#!/usr/bin/env python3

import sys, os, json, rpm, yaml, re

def mkdir_p(path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass

def split_rpm_filename(filename):
    if filename[-4:] == '.rpm':
        filename = filename[:-4]
    arch_index = filename.rfind('.')
    arch = filename[arch_index+1:]
    rel_index = filename[:arch_index].rfind('-')
    rel = filename[rel_index+1:arch_index]
    ver_index = filename[:rel_index].rfind('-')
    ver = filename[ver_index+1:rel_index]
    epochIndex = filename.find(':')
    if epochIndex == -1:
        epoch = ''
    else:
        epoch = filename[:epochIndex]
    name = filename[epochIndex + 1:ver_index]
    return name, ver, rel, epoch, arch

class SrpmMapping:
    def __init__(self, srpm_pkg_name, data):
        self.srpm_pkg_name = srpm_pkg_name
        self.recipe = None
        self.nonative = False
        self.subrecipes = {}
        self.rprovides = {}
        self.patch = None
        self.disabled = False
        self.rpms = []
        self.depends = []
        self.nodepends = []
        self.provides = []

        self.ldict = {}
        with open("license_dict.json", "r") as lconfig_f:
            self.ldict = json.load(lconfig_f)

        if "subrecipes" in data:
            subrecipes = data["subrecipes"]
            del data["subrecipes"]
            for subname, subdata in subrecipes.items():
                if subdata == None:
                    subdata = {}
                sub = SrpmMapping(srpm_pkg_name, subdata)
                # If no rpms are specified, assume it meant the one identical to the recipe name
                if len(sub.rpms) == 0:
                    sub.rpms.append(subname)
                self.subrecipes[subname] = sub

        for key in ["recipe", "patch", "disabled", "rpms", "depends", "nodepends", "rprovides", "provides", "nonative"]:
            if key in data:
                setattr(self, key, data[key])
                del data[key]

        for key in data.keys():
            print("WARNING: Unexpected srpm mapping key %s for srpm %s" % (key, srpm_pkg_name))


class Mapping:
    def __init__(self, data):
        self.preferred_packages = {}
        self.enabled_streams = []
        self.ignore_rpms = []
        self.srpms = {}

        if "srpms" in data:
            srpms = data["srpms"]
            del data["srpms"]
            for srpm_pkgname, srpm_data in srpms.items():
                srpm_mapping = SrpmMapping(srpm_pkgname, srpm_data)
                if srpm_pkgname in self.srpms:
                    print("WARNING: Duplicate srpms %s in mapping" % (srpm_pkgname))
                self.srpms[srpm_pkgname] = srpm_mapping

        for key in ["preferred-packages", "enabled-streams", "ignore-rpms"]:
            if key in data:
                setattr(self, key.replace("-", "_"), data[key])
                del data[key]

        for key in data.keys():
            print("WARNING: Unexpected global mapping key %s" % (key))

    def get_srpm_mapping(self, srpm_pkg_name):
        return mapping.srpms.get(srpm_pkg_name, SrpmMapping(srpm_pkg_name, {}))

    def get_recipe_name_for_srpm_pkg_name (self, srpm_pkg_name):
        srpm_mapping = self.get_srpm_mapping(srpm_pkg_name)
        if srpm_mapping.recipe:
            return srpm_mapping.recipe
        # default: Convert _ to -, and lowercase (yocto warns otherwise)
        return srpm_pkg_name.replace("_", "-").lower()

class RpmData:
    def __init__(self, rpm_name, data):
        self.rpm_name = rpm_name
        (pkg_name, ver, rel, epoch, arch) = split_rpm_filename(rpm_name)
        self.pkg_name = pkg_name
        self.evr = (epoch, ver, rel)
        self.arch = arch

        self.file_provides = []
        self.file_requires = []
        self.license = None
        self.modularitylabel = None
        self.provides = []
        self.requires = []
        self.srpm = None
        self.sha256: None
        self.url = None

        for key, val in data.items():
            setattr(self, key.replace("-", "_"), val)

        assert self.rpm_name == self.rpm # TODO: Stop generating self.rpm in json

        self.stream_id = None
        if self.modularitylabel:
            parts = self.modularitylabel.split(":")
            if len (parts) < 2:
                print ("WARNING: Unexpected modularity label format: %s" % (self.modularitylabel))
            else:
                self.stream_id = parts[0] + ":" + parts[1]

class RpmsData:
    def __init__(self, json_data, mapping):
        self.printed_prefers = {}
        self.non_modular_rpms = {}
        self.streams = {}

        for rpm_name, json_rpm_data in json_data.items():
            rpm_data = RpmData(rpm_name, json_rpm_data)
            pkg_name = rpm_data.pkg_name

            if pkg_name in mapping.ignore_rpms:
                continue

            if rpm_data.stream_id:
                stream_id = rpm_data.stream_id
                if not stream_id in self.streams:
                    self.streams[stream_id] = {}

                dest = self.streams[stream_id]
            else:
                dest = self.non_modular_rpms

            # Only enable the pkg_name with highest evr (for each stream)
            if pkg_name in dest:
                old_rpm_data = dest[pkg_name]
                if rpm.labelCompare(rpm_data.evr, old_rpm_data.evr) < 0:
                    continue # This is older than what we've seen, ignore
            dest[pkg_name] = rpm_data

        self.compute_enabled_rpms(mapping)
        self.compute_by_srpm()

        self.compute_providers()
        self.compute_rdepends(mapping)

    def compute_by_srpm(self):
        self.by_srpm = {}
        for pkg_name, rpm_data in self.rpms.items():
            srpm = rpm_data.srpm
            if not srpm in self.by_srpm:
                self.by_srpm[srpm] = []
            self.by_srpm[srpm].append(rpm_data)

    # Filter out the set of binary rpms that are not ignored, and
    # is enabled by the enabled module sets
    def compute_enabled_rpms(self, mapping):
        self.rpms = {}

        modular_rpms = {}
        # TODO: This just adds all the matching rpms for the stream, irrespective
        # of the NSVCA module version/context, and we let the NEVR sort handle duplicates
        # later. This isn't strictly correct, so this needs further work.
        for enabled in mapping.enabled_streams:
            if not enabled in self.streams:
                print ("WARNING: Specified enable stream '%s' does not exist" % (enabled))
                continue
            stream = self.streams[enabled]
            for pkg_name, rpm_data in stream.items():
                self.rpms[pkg_name] = rpm_data
                # If we used a pkg-name from a module, don't inherit it from base
                modular_rpms[pkg_name] = True

        for pkg_name, rpm_data in self.non_modular_rpms.items():
            if pkg_name in modular_rpms:
                continue
            self.rpms[pkg_name] = rpm_data

    # Generate reverse mapping from provide string to list of providers (by pkgname)
    def compute_providers(self):
        self.providers = {}
        for pkg_name, rpm_data in self.rpms.items():
            for prov in rpm_data.provides:
                prov = prov.split(" ")[0] # We ignore any versioning
                if not prov in self.providers:
                    self.providers[prov] = []
                if not pkg_name in self.providers[prov]:
                    self.providers[prov].append(pkg_name)
            for prov in rpm_data.file_provides:
                prov = prov.split("\t")[0] # We ignore these weird tab things
                if not prov in self.providers:
                    self.providers[prov] = []
                if not pkg_name in self.providers[prov]:
                    self.providers[prov].append(pkg_name)

    def find_provider_for_req(self, pkg_name, req, mapping):
        if req.startswith("rpmlib("): # These are handled internally
            return None
        if req.startswith("font("): # Font are best handled manually
            return None
        if req.startswith("("): # No support for optional requires
            return None
        req = req.split(" ")[0] # We ignore any versioning
        if not req in self.providers:
            print("WARNING: package %s requires %s, but it has no provider" % (pkg_name, req))
            return None
        providers = self.providers[req]
        if pkg_name in providers:
            return None # Some packages provider their own requires, in that case we don't add any deps

        if len(providers) == 1: # If there is only one, use that
            return providers[0]
        elif req in mapping.preferred_packages:
            return mapping.preferred_packages[req]
        elif req in providers: # Sometime things require a package name, but optionally other things supply it, go with the real thing
            return req

        # As fallback we pick the shortest named provider
        # This is stable over order-changes and prefers "foo" over "foo-special"
        provider = min(providers, key=len)

        providers_as_string = str(providers)
        printed_warning_already = providers_as_string in self.printed_prefers
        self.printed_prefers[providers_as_string] = True
        if not printed_warning_already:
            print("NOTE: package %s requires %s, but it has several providers: %s, picking shortest (%s)" % (pkg_name, req, providers, provider))

        return provider

    # Resolves rpm requires strings to real rpm pkg names
    def compute_rdepends(self, mapping):
        for pkg_name, rpm_data in self.rpms.items():
            rdepends = set()
            for req in rpm_data.requires:
                provider = self.find_provider_for_req(pkg_name, req, mapping)
                if provider:
                    rdepends.add(provider)
            rpm_data.rdepends = sorted(list(rdepends))

def extract_soname_dep(rpm_prov):
    if rpm_prov.startswith("/"): # It's a file provides (like "/bin/bash")
        return None
    parts = rpm_prov.split (" ")
    first = parts[0].split ("(")[0]
    if ".so" in first:
        return first
    return None

class Recipe:
    recipes = {}
    recipes_by_rpm = {}

    def __init__(self, recipe_name, srpm_pkg_name, mapping):
        if recipe_name in Recipe.recipes:
            print("Warning: recipe %s already exists" % (recipe_name))
            assert recipe_name not in Recipe.recipes
        self.recipe_name = recipe_name
        Recipe.recipes[recipe_name] = self
        self.srpm_pkg_name = srpm_pkg_name
        self.mapping = mapping
        self.srpm_full_names = []
        self.rpms_data = {} # pkg name to data
        self.subrecipes = {}
        self.depends = self.mapping.depends.copy()
        self.provides = self.mapping.provides.copy()

        for subrecipe, subrecipe_data in mapping.subrecipes.items():
            if subrecipe in Recipe.recipes:
                print("Warning: recipe %s split out from srpm %s already exists" % (subrecipe, srpm_pkg_name))
            self.subrecipes[subrecipe] = Recipe(subrecipe, srpm_pkg_name, subrecipe_data)

    def add_rpms(self, srpm_full_name, rpms_data):
        self.srpm_full_names.append(srpm_full_name)
        for rpm_data in rpms_data:
            target_recipe = self
            pkg_name = rpm_data.pkg_name
            for subrecipe_name, subrecipe in self.subrecipes.items():
                if pkg_name in subrecipe.mapping.rpms:
                    target_recipe = self.subrecipes[subrecipe_name]
                    break
            target_recipe.rpms_data[pkg_name] = rpm_data
            if pkg_name in Recipe.recipes_by_rpm:
                print("Warning: multiple recipes for package name %s" % (pkg_name))
            Recipe.recipes_by_rpm[pkg_name] = target_recipe


    def format(self):
        patch = ""
        provides = ""
        rpm_license = ""
        license = ""
        ltext = ""
        depends = ""
        per_package = ""
        urls = []
        sha256s = []
        arch = "allarch"
        native = 'BBCLASSEXTEND = "native"\n'
        if self.mapping.nonative:
            native = ""

        if self.mapping.patch:
            if type(self.mapping.patch) is list:
                patches = self.mapping.patch
            else:
                patches = [ self.mapping.patch ]
            patch = 'SRC_URI = "%s"\n' % (" ".join(map(lambda p: 'file://%s' % (p), patches)))

        if self.provides:
            provides = 'PROVIDES = "%s"\n' % (" ".join(sorted(self.provides)))

        if self.depends:
            depends = 'DEPENDS = "%s"\n' % (" ".join(sorted(self.depends)))

        for pkg_name in sorted(self.rpms_data.keys()):
            rpm_data = self.rpms_data[pkg_name]

            if rpm_data.arch != "noarch":
                arch = rpm_data.arch

            prov_sonames = set()
            req_sonames = set()
            rprovides = self.mapping.rprovides.get(pkg_name, [])

            urls.append(rpm_data.url)
            sha256s.append('SRC_URI[%s.sha256sum] = "%s"' % (pkg_name, rpm_data.sha256))

            #license map
            def lic_replace(ldict, ltext):
              regex = re.compile("|".join(map(re.escape, ldict.keys(  ))))
              return regex.sub(lambda match: ldict[match.group(0)], ltext)

            rpm_license = rpm_data.license
            license = lic_replace(self.mapping.ldict, rpm_license)

            locales = []
            for file in rpm_data.file_provides:
                if file.startswith ("/usr/lib/locale/") and file.endswith("LC_CTYPE"):
                    locale = os.path.basename(os.path.dirname(file))
                    if locale != "C" and not locale.startswith("C.") and locale.find("_") != -1:
                        l = tuple(locale.lower().split("_", 1))
                        locales.append(l)

            gconvs = []
            for file in rpm_data.file_provides:
                if (file.startswith ("/usr/lib/gconv/") or file.startswith ("/usr/lib64/gconv/")) and file.endswith(".so"):
                    gconv = os.path.basename(file)[:-3].lower()
                    gconvs.append(gconv)

            if pkg_name.endswith("-devel"):
                rprovides.append(pkg_name.replace("-devel", "-dev"))

            for locale in locales:
                rprovides.append("locale-base-%s-%s" % locale)
                rprovides.append("virtual-locale-%s-%s" % locale)
                rprovides.append("virtual-locale-%s" % (locale[0]))

            for gconv in gconvs:
                rprovides.append("glibc-gconv-%s" % gconv)

            for prov in rpm_data.provides:
                soname = extract_soname_dep(prov)
                if soname:
                    prov_sonames.add(soname)

            for req in rpm_data.requires:
                soname = extract_soname_dep(req)
                if soname:
                    req_sonames.add(soname)

            if prov_sonames:
                per_package = per_package + 'RPM_SONAME_PROV_%s = "%s"\n' % (pkg_name, " ".join(sorted(list(prov_sonames))))
            if req_sonames:
                per_package = per_package + 'RPM_SONAME_REQ_%s = "%s"\n' % (pkg_name, " ".join(sorted(list(req_sonames))))
            if rprovides:
                per_package = per_package + 'RPROVIDES_%s = "%s"\n' % (pkg_name, " ".join(map(lambda r: "%s (= %s)" % (r, rpm_data.evr[1]), sorted(rprovides))))
            if len(rpm_data.rdepends) > 0:
                per_package = per_package + 'RDEPENDS_%s = "%s"\n' % (pkg_name, " ".join(sorted(rpm_data.rdepends)))

        for pn in self.mapping.rprovides:
            if pn not in self.rpms_data:
                print("WARNING: %s specifies an rprovides for non-existing rpm %s" % (self.srpm_pkg_name, pn))

        return '''SUMMARY = "generated recipe based on {srpm} srpm"
DESCRIPTION = "Description"
LICENSE = "{license}"
RPM_LICENSE = "{rpm_license}"
LIC_FILES_CHKSUM = "file://${{COREBASE}}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "{arch}"
inherit rpmbased
{native}{provides}{depends}{per_package}
RPM_URI = "{urls} \\
          "
{patch}
{sha256s}
'''.format(srpm = self.srpm_pkg_name,
           urls = ' \\\n           '.join (sorted(urls)),
           sha256s = '\n'.join(sha256s),
           native = native,
           license = license,
           arch = arch,
           rpm_license = rpm_license,
           patch = patch,
           provides = provides,
           depends = depends,
           per_package = per_package,
           )


def compute_active_recipes(rpms_data, mapping):
    # Compute recipes for each srpm, renaming as necessary
    # Note that there may be multiple versions of the same srpm, either
    # because it was updated, or it was build with different options producing
    # different rpm names (e.g in modules)
    for srpm_name, srpm_rpms_data in rpms_data.by_srpm.items():
        (srpm_pkg_name, ver, rel, epoch, arch) = split_rpm_filename(srpm_name)
        recipe_name = mapping.get_recipe_name_for_srpm_pkg_name(srpm_pkg_name)

        if recipe_name in Recipe.recipes:
            # We have multiple srpms with the same pkg name (different versions), reuse Recipe
            recipe = Recipe.recipes[recipe_name]
        else:
            srpm_mapping = mapping.get_srpm_mapping(srpm_pkg_name)
            recipe = Recipe(recipe_name, srpm_pkg_name, srpm_mapping)

        recipe.add_rpms(srpm_name, srpm_rpms_data)


def compute_automatic_dependencies(rpms_data, mapping):
    # We try to auto-generate dependencies for devel packages.
    # For instance if libfoo-devel has a header that includes a header for libbar-devel
    # then libfoo needs to DEPENDS on libbar to get the header into the sysroot.
    # We don't really have this info, but we try to reconstruct it based on the
    # pkgconfig dependencies and manual dependencies from *-devel on another *-devel
    # To avoid some tricky loops we avoid handling dependencies for some core stuff
    for recipe_name, recipe in Recipe.recipes.items():
        if recipe_name in ["glibc", "binutils", "selinux-policy"]:
            continue
        has_pcfile = False
        for pkg_name, rpm_data in recipe.rpms_data.items():
            dep_providers = set()
            for req in rpm_data.requires:
                req_prefix = req.split(" ")[0].split("(")[0] # Strips version checks and ()
                if req.startswith("pkgconfig(") or (pkg_name.endswith("-devel") and req_prefix.endswith("-devel")):
                    provider = rpms_data.find_provider_for_req(pkg_name, req, mapping)
                    if provider:
                        dep_providers.add(provider)
                else:
                    soname = extract_soname_dep(req)
                    if soname:
                        provider = rpms_data.find_provider_for_req(pkg_name, req, mapping)
                        if provider:
                            # We don't create automatic dependencies between same srpms
                            # If its the same recipe (which is the default) that makes no sense,
                            # but if the srpm is split that adding dependencies between the two
                            # is prone to cycles, so we force the user to manually specify those deps
                            provider_data = rpms_data.rpms[provider]
                            if provider_data.srpm != rpm_data.srpm:
                                dep_providers.add(provider)

            for provider in dep_providers:
                dep_rec = Recipe.recipes_by_rpm[provider]
                if dep_rec and dep_rec.recipe_name != recipe_name and \
                   dep_rec.recipe_name not in ["kernel", "glibc", "gcc-runtime"] and \
                   dep_rec.recipe_name not in recipe.mapping.nodepends and \
                   dep_rec.recipe_name not in recipe.depends:
                    recipe.depends.append(dep_rec.recipe_name)

            for prov in rpm_data.provides:
                prov_prefix = prov.split(" ")[0].split("(")[0] # Strip version checks and ()
                if prov.startswith("pkgconfig("):
                    has_pcfile = True

        # Add pkg-config native dependency if the package has a pc-file so it can be used
        # We skip this for pkgconf and pkgconfig to avoid circular deps
        if "pkgconfig-native" not in recipe.depends and not recipe_name.startswith("pkgconf"):
            recipe.depends.append("pkgconfig-native")

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("ERROR: Not enough args, need json package file, json mapping file and dest dir")
        sys.exit(1)

    package_data_path = sys.argv[1]
    mapping_path = sys.argv[2]
    dest_dir = sys.argv[3]

    with open(mapping_path) as yaml_file:
        mapping_data = yaml.safe_load(yaml_file)

    mapping = Mapping(mapping_data)

    with open(package_data_path) as json_file:
        rpms_data = RpmsData(json.load(json_file), mapping)

    compute_active_recipes(rpms_data, mapping)

    compute_automatic_dependencies(rpms_data, mapping)

    # Write the recipes themselves
    mkdir_p(dest_dir)

    for recipe_name, recipe in Recipe.recipes.items():
        if recipe.mapping.disabled:
            continue

        content = recipe.format()
        path = os.path.join(dest_dir, recipe_name + ".bb")
        with open(path, 'w') as outfile:
            outfile.write(content)
