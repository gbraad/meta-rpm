#!/usr/bin/env python3

from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.parse import urljoin, urlparse
from urllib.request import urlretrieve
import sys, os, re, subprocess, hashlib, json
import argparse

def mkdir_p(path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass

def get_packages(url, arch):
    res = []
    print ("fetching %s" % (url))
    html_page = urlopen(url)
    soup = BeautifulSoup(html_page, features="lxml")
    for link in soup.findAll('a'):
        href = link.get('href')
        if href.endswith(".noarch.rpm") or href.endswith("." + arch + ".rpm"):
           res.append(urljoin (url, href))
    return res


def sha256_file(path):
    m = hashlib.sha256()
    with open(path,"rb") as f:
        for byte_block in iter(lambda: f.read(4096),b""):
            m.update(byte_block)
    return m.hexdigest()

def analyze_rpm(path):
    # Compute sha256 of binary
    sha256 = sha256_file(path)

    # Extract name of source rpm
    srpm = subprocess.check_output(["rpm", "-qp", "--qf", "%{SOURCERPM}", path], encoding = "utf8", stderr=subprocess.DEVNULL).strip()

    # Extract (optional) modularity label
    modularitylabel = subprocess.check_output(["rpm", "-qp", "--queryformat", "%{modularitylabel}", path], encoding = "utf8", stderr=subprocess.DEVNULL)
    if modularitylabel == "(none)" or modularitylabel == "":
        modularitylabel = None

    lic = subprocess.check_output(["rpm", "-qp", "--queryformat", "%{LICENSE}", path], encoding = "utf8", stderr=subprocess.DEVNULL)
    if lic == "(none)" or lic == "":
        lic = None

    # Extract prov/req
    provides = subprocess.check_output(["rpm", "-qp", "--provides", path], encoding = "utf8", stderr=subprocess.DEVNULL)
    file_provides = subprocess.check_output(["rpm", "-qp", "--fileprovide", path], encoding = "utf8", stderr=subprocess.DEVNULL)
    requires = subprocess.check_output(["rpm", "-qp", "--requires", path], encoding = "utf8", stderr=subprocess.DEVNULL)
    file_requires = subprocess.check_output(["rpm", "-qp", "--filerequire", path], encoding = "utf8", stderr=subprocess.DEVNULL)

    data = {
        "sha256": sha256,
        "srpm": srpm,
        "provides": list(map(lambda s: s.strip(), provides.splitlines())),
        "requires": list(map(lambda s: s.strip(), requires.splitlines())),
        "file-provides": list(map(lambda s: s.strip(), file_provides.splitlines())),
        "file-requires": list(map(lambda s: s.strip(), file_requires.splitlines())),
    }
    if modularitylabel:
        data["modularitylabel"] = modularitylabel
    if lic:
        data["license"] = lic

    return data

parser = argparse.ArgumentParser(description='Generate rpm package data.')
parser.add_argument('baseurls', type=str, nargs='+', help='base urls')
parser.add_argument('--workdir', type=str, default="rpms", help='download dir')
parser.add_argument('--output', type=str, default="package-data.json", help='Output filename')
parser.add_argument('--arch', type=str, default="x86_64", help='Architecture')

args = parser.parse_args()
print(args)

workdir = args.workdir

print(workdir)

# Ensure it exists
mkdir_p(workdir)

# Compute list of all binary rpms in the base uris
rpm_urls = []
for baseurl in args.baseurls:
    rpm_urls = rpm_urls + get_packages(baseurl, args.arch)

rpm_data = {}

# Download (if needed) and extract rpm data
for url in rpm_urls:
    filename = os.path.basename(urlparse (url).path)

    path = os.path.join(workdir, filename)
    if not os.path.isfile(path):
        print ("downloading %s" % (path))
        urlretrieve(url, path)

    print ("analyzing %s" % (filename))
    data = analyze_rpm(path)
    data["url"] = url
    data["rpm"] = filename
    rpm_data[filename] = data

with open(args.output, 'w') as outfile:
    json.dump(rpm_data, outfile, sort_keys=True, indent=4)
