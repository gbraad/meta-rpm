# meta-rpm

meta-rpm is a [yocto](https://www.yoctoproject.org/) layer that
replaces the "poky" base distribution by one based on pre-built rpms
from an existing rpm-based distribution. Currently only **Centos** is
supported, but the plan is to allow others too, like **Fedora**.

The goal of meta-rpm is to replace the core packages of the
distributions with an existing distribution while allowing you to
continue using your (and third party) existing Yocto layers on top of
it.  As such it tries to expose something that is as compatible as
possible with poky with things like recipe names, etc.

# How meta-rpm works

meta-rpm consists of a set of mostly auto-generated recipes that use
as sources the binary rpms that makes up a pre-existing
distribution. At build time the rpms are extracted to create
buildroots that can be used by yocto when it builds dependent recipes.

However, during the package generation phase these extracted
buildroots are not used. Instead a copy of the original rpm content is
produced, which will be used when composing the final image.

# Advantages

The advantage of the meta-rpm approach is that you can continue to use
yocto to build your software, but rely on a stable,
long-term-supported distribution with wide testing and deployment.

# Limitations

Due to the use of extracted pre-build rpms, meta-rpm does not support
cross compilation. This means you have to run the build on the same
architecture as the target system.

Additionally, while you can layer changes on top of the base
distribution you cannot change details affecting the build of base
packages (as they are pre-build), except by completely overriding
recipes.

There is currently no support for building a kernel in meta-rpm.

# Using meta-rpm

First install all the regular dependencies for yocto, as described at
https://docs.yoctoproject.org/ref-manual/system-requirements.html#required-packages-for-the-build-host.

On top of the yocto dependencies, meta-rpm requires these extra host packages:
 * patchelf
 * shadow-utils (uidmap on ubuntu)

Additionally, meta-rpm uses user namespaces which depends on the user
being granted access to some extra ids in /etc/subuid
/etc/subgid. Some distributions automatically do this for new users,
but if not add a line for your user giving access to at least 1024
extra ids.

Then check out meta-rpm itself checkout:

```
 $ git clone https://gitlab.com/fedora-iot/meta-rpm.git
 $ cd meta-rpm
 $ git submodule update --init
```

The create and set up a build directory:

```
 $ source oe-init-build-env # This will create a build subdirectory and cd to it
```

Then you can start a test build:

```
 $ bitbake centos8-minimal-image
```

Or for a larger image:

```
 $ bitbake centos8-test-image
```


Which will build an image in `tmp/deploy/images/rhx86-64/centos8-test-image-rhx86-64.tar.gz`

To test this you can run:
```
 $ mkdir test-rootfs
 $ tar xvf tmp/deploy/images/rhx86-64/centos8-test-image-rhx86-64.tar.gz -C test-rootfs
 $ bwrap --bind test-rootfs / --bind $HOME $HOME /bin/sh
 sh-4.4$
```

Now you should be in a shell in the image and can try an app, like this:

```
 $ bwrap --bind test-rootfs / --bind $HOME $HOME /bin/sh
 sh-4.4$ zenity --info --text="This is from meta-rpm"
```

# Contents of meta-rpm

## meta-rpm/{classes/, lib/}

These contains some code and bbclasses used by the meta-rpm
recipes. The main bbclass used by recipes is called `rpmbased.bbclass`
and the main image class is `rpm-image.bbclass`.

## package-data/

This directory has json files that contain extracted information about
all the rpms in the distribution. For instance, `centos8-x86_64.json`
has all the centos8 info.

There is a Makefile that allows you to re-generate this file, but that
will download a lot of data which is very slow.

## meta-rpm/recipes-core/{centos8, centos8-generated}

The `centos8` direcoty information of how to map the centos8 rpms to
yocto recipes. The primary content is the `centos8-mapping.yaml` which
describes the highlevel mapping. The `Makefile` in this directory
contains a rule that from this produces the `centos8-generated`
directory which contains the primary `*.bb` recipe files. (Note: This
directory is checked in to git, so you need only regenerate if the
mapping is changed).

In addition, the `centos8` directory contains additional `*.bb*` and
`*.bbappend` files that extend the auto-generated recipes in custom
ways as needed.

## meta-rpm/recipes-core/recipes-devtools

This contains some modified poky recipes that are needed during the
build. Primarily this contains required packages that are not packaged
in centos.

# rpmbased.bbclass internals

The core of meta-rpm is the rpmbased bbclass. It works by replacing
most of the steps of package.bbclass with different ones.

It does two major things, do_install() and do_package().

do_install() is supposed to do the "make install" part of the
configure/build/install cycle, but since we already built the code all
this does is extract the binary rpms. This is done with a custom
rpm2cpio implementation in python to avoid circular dependencies.

There are also some standard hacks done to the unpacked contents
before its put in the sysroot:

 * merged /usr is setup up before so unpack goes in the right place
 * Permissions are normalized
 * Any patches are applied
 * Build-id files are removed
 * Symlinks are made relative
 * pkg-config files are tweaked as needed (for -native packages)
 * The post_rpm_install function is called that can further tweaks

The results of do_install() are put in the "sysroot" (and the
sysroot-native for native build) and are used during build of further
recipes. It is not used in the final image.

do_package() normally generates the rpms from the files in do_install().
However instead we just spit out copies of the input binary rpms.

Additionally we call out to `rpm` to query things like dependencies
and provides from the rpms and inject these into the package
metadata that yocto otherwise generates when it analyzes the files during
packaging.
