SUMMARY = "generated recipe based on jakarta-commons-httpclient srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & (CLOSED | LGPL-2.0)"
RPM_LICENSE = "ASL 2.0 and (ASL 2.0 or LGPLv2+)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jakarta-commons-httpclient = "apache-commons-codec apache-commons-logging java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jakarta-commons-httpclient-demo = "jakarta-commons-httpclient"
RDEPENDS_jakarta-commons-httpclient-javadoc = "javapackages-filesystem"
RDEPENDS_jakarta-commons-httpclient-manual = "jakarta-commons-httpclient-javadoc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jakarta-commons-httpclient-3.1-28.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jakarta-commons-httpclient-demo-3.1-28.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jakarta-commons-httpclient-javadoc-3.1-28.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jakarta-commons-httpclient-manual-3.1-28.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jakarta-commons-httpclient.sha256sum] = "1847a98a85a5a95650966ffabced724b30f77b602a9acdd9eb2e7d97ac027805"
SRC_URI[jakarta-commons-httpclient-demo.sha256sum] = "32b94176fee058feea9b474237024e6b0e3771bc746d8969b84e25fa955c358f"
SRC_URI[jakarta-commons-httpclient-javadoc.sha256sum] = "bad9e58d2a2792064f001d00cb1c5fe25afcbce20fa25597f6aaa65ab5779dd4"
SRC_URI[jakarta-commons-httpclient-manual.sha256sum] = "b20ab463c0f1eb05af74679161292a1424d25451c028a99987e006e0a3ba01a8"
