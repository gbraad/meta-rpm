SUMMARY = "generated recipe based on libtevent srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libtalloc libxcrypt pkgconfig-native platform-python3"
RPM_SONAME_PROV_libtevent = "libtevent.so.0"
RPM_SONAME_REQ_libtevent = "libc.so.6 libcrypt.so.1 libdl.so.2 libpthread.so.0 libtalloc.so.2"
RDEPENDS_libtevent = "glibc libtalloc libxcrypt"
RPM_SONAME_REQ_libtevent-devel = "libtevent.so.0"
RPROVIDES_libtevent-devel = "libtevent-dev (= 0.10.0)"
RDEPENDS_libtevent-devel = "libtalloc-devel libtevent pkgconf-pkg-config"
RPM_SONAME_REQ_python3-tevent = "libc.so.6 libcrypt.so.1 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libtalloc.so.2 libtevent.so.0 libutil.so.1"
RDEPENDS_python3-tevent = "glibc libtalloc libtevent libxcrypt platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtevent-0.10.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtevent-devel-0.10.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-tevent-0.10.0-2.el8.x86_64.rpm \
          "

SRC_URI[libtevent.sha256sum] = "c4a35bc06bfc968f9f9afc56dca81b9943b7d7d674c3f72ce8f50cdc11f8d31b"
SRC_URI[libtevent-devel.sha256sum] = "fcfeb68064d84552dfbc22b9452b0bfe5b808354e4b3b0445833a45d9c8b91e0"
SRC_URI[python3-tevent.sha256sum] = "50b0c496b10751eae11b006d113218c30237cdad2ac94fe75b3d404b67e9e458"
