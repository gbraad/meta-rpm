SUMMARY = "generated recipe based on autoconf213 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_autoconf213 = "bash coreutils gawk info m4"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/autoconf213-2.13-39.el8.noarch.rpm \
          "

SRC_URI[autoconf213.sha256sum] = "5d2cdcf8c09d7b14a210e2b50f47f8d7e277bc65baba637f1a89a9a0c6098824"
