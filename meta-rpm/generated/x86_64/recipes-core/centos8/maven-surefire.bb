SUMMARY = "generated recipe based on maven-surefire srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & CPL-1.0"
RPM_LICENSE = "ASL 2.0 and CPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-failsafe-plugin = "apache-commons-io apache-commons-lang3 java-1.8.0-openjdk-headless javapackages-filesystem maven-shared-utils maven-surefire"
RDEPENDS_maven-surefire = "apache-commons-io apache-commons-lang3 java-1.8.0-openjdk-headless javapackages-filesystem maven-common-artifact-filters maven-lib maven-plugin-annotations maven-shared-utils maven-verifier plexus-languages procps-ng"
RDEPENDS_maven-surefire-javadoc = "javapackages-filesystem"
RDEPENDS_maven-surefire-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-surefire"
RDEPENDS_maven-surefire-provider-junit = "java-1.8.0-openjdk-headless javapackages-filesystem maven-shared-utils maven-surefire"
RDEPENDS_maven-surefire-provider-testng = "java-1.8.0-openjdk-headless javapackages-filesystem maven-shared-utils maven-surefire"
RDEPENDS_maven-surefire-report-parser = "java-1.8.0-openjdk-headless javapackages-filesystem maven-reporting-api maven-shared-utils maven-surefire"
RDEPENDS_maven-surefire-report-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-sitetools maven-lib maven-plugin-annotations maven-reporting-impl maven-surefire-report-parser plexus-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-failsafe-plugin-2.22.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-surefire-2.22.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-surefire-javadoc-2.22.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-surefire-plugin-2.22.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-surefire-provider-junit-2.22.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-surefire-provider-testng-2.22.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-surefire-report-parser-2.22.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-surefire-report-plugin-2.22.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-failsafe-plugin.sha256sum] = "ce7c7764659fd789908fea0b42db4d2ea64961405f6f528ce3c1763420ef2796"
SRC_URI[maven-surefire.sha256sum] = "d381f2a56b703904e27c584174f8f675f23186a85fdb0149fd2a6ccd4e3a743b"
SRC_URI[maven-surefire-javadoc.sha256sum] = "29248a03072fcc6dda0edd2b011925227559f7d20965284900d5d8a6e9feae46"
SRC_URI[maven-surefire-plugin.sha256sum] = "130f2285327c08ab098a5f08e8f94d67908bb3f7b5a989aca4050d80133a1910"
SRC_URI[maven-surefire-provider-junit.sha256sum] = "31e26644ed9dd3cb05f635ada0bbd4240c1e106514fa966d33236407dec741f2"
SRC_URI[maven-surefire-provider-testng.sha256sum] = "f375afd9cdde9c3d0a7485572ca1d73055d723bb1d8bf5692524be4a8052f9b9"
SRC_URI[maven-surefire-report-parser.sha256sum] = "33c456a17ffa667d773b8d6461a333ec6cdd73f58bab3161af403ab315cf7ce2"
SRC_URI[maven-surefire-report-plugin.sha256sum] = "7ae24e6f44e53e75ef414118a6ec99001fa9098b2b6015e302e75002c42bd12c"
