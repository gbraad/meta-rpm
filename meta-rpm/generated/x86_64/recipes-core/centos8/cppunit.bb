SUMMARY = "generated recipe based on cppunit srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_cppunit = "libcppunit-1.14.so.0"
RPM_SONAME_REQ_cppunit = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_cppunit = "glibc libgcc libstdc++"
RPM_SONAME_REQ_cppunit-devel = "libcppunit-1.14.so.0"
RPROVIDES_cppunit-devel = "cppunit-dev (= 1.14.0)"
RDEPENDS_cppunit-devel = "cppunit pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cppunit-1.14.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cppunit-devel-1.14.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cppunit-doc-1.14.0-4.el8.x86_64.rpm \
          "

SRC_URI[cppunit.sha256sum] = "343f7ea4d245906e6b46792fad475589783e5914bddb91006c8a647afb34f4df"
SRC_URI[cppunit-devel.sha256sum] = "c7dfaba8532b58c9a30fde72be362b667fc57488c07e0969a6f6b83da0dfcef2"
SRC_URI[cppunit-doc.sha256sum] = "f9461df88389cc827794a6be27fc5aca41b8180557ca85145e2f695f0ea0bb1f"
