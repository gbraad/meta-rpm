SUMMARY = "generated recipe based on perl-XML-Simple srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-Simple = "perl-Carp perl-Exporter perl-IO perl-PathTools perl-Scalar-List-Utils perl-Storable perl-XML-NamespaceSupport perl-XML-Parser perl-XML-SAX perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-XML-Simple-2.25-1.el8.noarch.rpm \
          "

SRC_URI[perl-XML-Simple.sha256sum] = "e487dcec4b6618964b7debbafa0c44023405d15336a75e6338896945a9b2d764"
