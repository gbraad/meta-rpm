SUMMARY = "generated recipe based on gtkmm30 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atkmm cairomm gdk-pixbuf glib-2.0 glibmm24 gtk+3 libgcc libsigc++20 pangomm pkgconfig-native"
RPM_SONAME_PROV_gtkmm30 = "libgdkmm-3.0.so.1 libgtkmm-3.0.so.1"
RPM_SONAME_REQ_gtkmm30 = "libatkmm-1.6.so.1 libc.so.6 libcairomm-1.0.so.1 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgiomm-2.4.so.1 libglib-2.0.so.0 libglibmm-2.4.so.1 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpangomm-1.4.so.1 libsigc-2.0.so.0 libstdc++.so.6"
RDEPENDS_gtkmm30 = "atkmm cairomm gdk-pixbuf2 glib2 glibc glibmm24 gtk3 libgcc libsigc++20 libstdc++ pangomm"
RPM_SONAME_REQ_gtkmm30-devel = "libgdkmm-3.0.so.1 libgtkmm-3.0.so.1"
RPROVIDES_gtkmm30-devel = "gtkmm30-dev (= 3.22.2)"
RDEPENDS_gtkmm30-devel = "atkmm-devel cairomm-devel gdk-pixbuf2-devel glibmm24-devel gtk3-devel gtkmm30 pangomm-devel pkgconf-pkg-config"
RDEPENDS_gtkmm30-doc = "glibmm24-doc gtkmm30"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gtkmm30-3.22.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gtkmm30-devel-3.22.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gtkmm30-doc-3.22.2-2.el8.noarch.rpm \
          "

SRC_URI[gtkmm30.sha256sum] = "6d531a17bcadfc45b094034fa620ac9206e9828f3128c94f75e601a635dae01d"
SRC_URI[gtkmm30-devel.sha256sum] = "3bcf06605ee0ef136bd24dab3ca7370d365252db13c496edf3643eb8c8c538ef"
SRC_URI[gtkmm30-doc.sha256sum] = "0f3a7f29d1db91459062de197733ce4ae63081a8d28ed75aaf6d5e915814448e"
