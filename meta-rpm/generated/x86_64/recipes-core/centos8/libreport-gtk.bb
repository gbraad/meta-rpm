SUMMARY = "generated recipe based on libreport srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk augeas cairo gdk-pixbuf glib-2.0 gtk+3 libtar pango pkgconfig-native satyr systemd-libs"
RPM_SONAME_PROV_libreport-gtk = "libreport-gtk.so.0"
RPM_SONAME_REQ_libreport-gtk = "libatk-1.0.so.0 libaugeas.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0 libtar.so.1 libutil.so.1"
RDEPENDS_libreport-gtk = "atk augeas-libs cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libreport libreport-plugin-reportuploader libtar pango satyr systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreport-gtk-2.9.5-10.el8.x86_64.rpm \
          "

SRC_URI[libreport-gtk.sha256sum] = "58bde9fb390417c6261376f8822d4089e52df0834152803dd6289632e6e61960"
