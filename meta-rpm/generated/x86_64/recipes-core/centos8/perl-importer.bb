SUMMARY = "generated recipe based on perl-Importer srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Importer = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Importer-0.025-1.el8.noarch.rpm \
          "

SRC_URI[perl-Importer.sha256sum] = "c5c677f3b13f4ebb407a0a240f31988410eebe35001be5bf87c1a4b1215b348d"
