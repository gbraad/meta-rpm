SUMMARY = "generated recipe based on mythes-en srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & ClArtistic"
RPM_LICENSE = "BSD and Artistic clarified"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-en = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-en-3.0-23.el8.noarch.rpm \
          "

SRC_URI[mythes-en.sha256sum] = "693ee181f3a242808775a9a71cad9ce0dab7783ffb5b1d41ddf608fe6db5c85a"
