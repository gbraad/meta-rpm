SUMMARY = "generated recipe based on perl-Locale-Codes srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Locale-Codes = "perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Locale-Codes-3.57-1.el8.noarch.rpm \
          "

SRC_URI[perl-Locale-Codes.sha256sum] = "9a0eb4dd3f86d90a74b14a54e9b1e73d5be002296e96cf65792d433bef9a070e"
