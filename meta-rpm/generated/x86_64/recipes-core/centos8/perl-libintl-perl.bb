SUMMARY = "generated recipe based on perl-libintl-perl srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & LGPL-2.0"
RPM_LICENSE = "GPLv3+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-libintl-perl = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-libintl-perl = "glibc perl-Carp perl-Encode perl-Exporter perl-IO perl-PathTools perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-libintl-perl-1.29-2.el8.x86_64.rpm \
          "

SRC_URI[perl-libintl-perl.sha256sum] = "8b8c1ce375e1d8dd73f905e99bd452243ec194dd707a36fa5bdea7a252165c60"
