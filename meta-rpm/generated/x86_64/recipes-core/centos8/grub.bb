SUMMARY = "generated recipe based on grub2 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "device-mapper-libs freetype pkgconfig-native rpm xz"
RDEPENDS_grub2-common = "bash"
RDEPENDS_grub2-efi-aa64-modules = "grub2-common"
RDEPENDS_grub2-efi-ia32 = "efi-filesystem grub2-common grub2-tools grub2-tools-extra grub2-tools-minimal"
RDEPENDS_grub2-efi-ia32-cdboot = "grub2-common"
RDEPENDS_grub2-efi-ia32-modules = "bash grub2-common"
RDEPENDS_grub2-efi-x64 = "efi-filesystem grub2-common grub2-tools grub2-tools-extra grub2-tools-minimal"
RDEPENDS_grub2-efi-x64-cdboot = "grub2-common"
RDEPENDS_grub2-efi-x64-modules = "grub2-common"
RDEPENDS_grub2-pc = "dracut file gettext grub2-common grub2-pc-modules grub2-tools grub2-tools-extra grub2-tools-minimal which"
RDEPENDS_grub2-pc-modules = "grub2-common"
RDEPENDS_grub2-ppc64le-modules = "grub2-common"
RPM_SONAME_REQ_grub2-tools = "libc.so.6 libdevmapper.so.1.02 liblzma.so.5 librpm.so.8"
RDEPENDS_grub2-tools = "bash device-mapper-libs dracut file gettext glibc grub2-common os-prober rpm-libs which xz-libs"
RPM_SONAME_REQ_grub2-tools-efi = "libc.so.6 libdevmapper.so.1.02"
RDEPENDS_grub2-tools-efi = "device-mapper-libs file gettext glibc grub2-common os-prober which"
RPM_SONAME_REQ_grub2-tools-extra = "libc.so.6 libdevmapper.so.1.02 libfreetype.so.6 liblzma.so.5"
RDEPENDS_grub2-tools-extra = "bash device-mapper-libs file freetype gettext glibc grub2-common grub2-tools-minimal os-prober which xz-libs"
RPM_SONAME_REQ_grub2-tools-minimal = "libc.so.6 libdevmapper.so.1.02 liblzma.so.5"
RDEPENDS_grub2-tools-minimal = "bash device-mapper-libs gettext glibc grub2-common xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-common-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-efi-aa64-modules-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-efi-ia32-2.02-87.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-efi-ia32-cdboot-2.02-87.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-efi-ia32-modules-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-efi-x64-2.02-87.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-efi-x64-cdboot-2.02-87.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-efi-x64-modules-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-pc-2.02-87.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-pc-modules-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-ppc64le-modules-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-tools-2.02-87.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-tools-efi-2.02-87.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-tools-extra-2.02-87.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/grub2-tools-minimal-2.02-87.el8_2.x86_64.rpm \
          "

SRC_URI[grub2-common.sha256sum] = "886fe30d2f7ab40f96a8b28a96b86079feab5f45f8fac818d02c17af03b5af05"
SRC_URI[grub2-efi-aa64-modules.sha256sum] = "7f209b336386a89100061a63fda2ddbf653052d050fb73caac97c9272a9f4f05"
SRC_URI[grub2-efi-ia32.sha256sum] = "11dbf709ff3a78ae07ead20cf1cce999c0aefebaaf32f370046fa0c8793b9d37"
SRC_URI[grub2-efi-ia32-cdboot.sha256sum] = "8cb849edd8ca6e53d2ccaacd995cc2b25478f0b8dc3ba5a9f80cedb563f809b7"
SRC_URI[grub2-efi-ia32-modules.sha256sum] = "230456edb514dee5f19d5e20b997c1e6c9536f60b3686b8d63d5dd4da75ffb15"
SRC_URI[grub2-efi-x64.sha256sum] = "93c479c81b1298b623b097477a5a5439079a4ef8f5e398ecd54fc556def6db61"
SRC_URI[grub2-efi-x64-cdboot.sha256sum] = "a1a189808af7ae1343ceba15e6bd53852ff187a34229e1fb199090543d12ebb9"
SRC_URI[grub2-efi-x64-modules.sha256sum] = "140e94fa5b2ce4f95f6caffb2ee25444f83e33cfd6d0690702ff2790fb35234c"
SRC_URI[grub2-pc.sha256sum] = "e53376a615e80105c074e7c27e1a8f6112fa8c723326cc4a7ff9b7b74a013941"
SRC_URI[grub2-pc-modules.sha256sum] = "8023853c359646ab9df45fdef5217ab842195626006e4eb6e8e026089d66ddb7"
SRC_URI[grub2-ppc64le-modules.sha256sum] = "266388983e0b88df3e47a856bbdb39e5ff2c0275b60d6691e89df79fa4625183"
SRC_URI[grub2-tools.sha256sum] = "1d2b7314f06c34c6c0211059941af1f40d45fb263d3ba8d2660bc2f749ec9ec2"
SRC_URI[grub2-tools-efi.sha256sum] = "f135a510a960ad5c8fd405cf3bd232f00ccf91e0d7401cd46f5c5affd15c9401"
SRC_URI[grub2-tools-extra.sha256sum] = "6de451052286cdfef832b386a849d2cd7b8838de979ccb85388fc9d3a0059cd4"
SRC_URI[grub2-tools-minimal.sha256sum] = "6ee2dad79fda67658703873b123bcdcd89be1c1e763e61947e188c0a8d192ad5"
