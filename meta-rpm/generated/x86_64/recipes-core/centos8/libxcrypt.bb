SUMMARY = "generated recipe based on libxcrypt srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & BSD & CLOSED"
RPM_LICENSE = "LGPLv2+ and BSD and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "virtual/crypt"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libxcrypt = "libcrypt.so.1"
RPM_SONAME_REQ_libxcrypt = "libc.so.6"
RDEPENDS_libxcrypt = "glibc"
RPM_SONAME_REQ_libxcrypt-devel = "libcrypt.so.1"
RPROVIDES_libxcrypt-devel = "libxcrypt-dev (= 4.1.1)"
RDEPENDS_libxcrypt-devel = "glibc-devel glibc-headers libxcrypt pkgconf-pkg-config"
RDEPENDS_libxcrypt-static = "glibc-static libxcrypt-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libxcrypt-4.1.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libxcrypt-devel-4.1.1-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libxcrypt-static-4.1.1-4.el8.x86_64.rpm \
          "

SRC_URI[libxcrypt.sha256sum] = "607344bcb45159a85fe83b691103e321e4169847abf197db6f3a8b223f6ba54d"
SRC_URI[libxcrypt-devel.sha256sum] = "fd32c56ba70c5aed1941dd6f42b8395afd28902639ea0dac6f68ab2936fe5b7f"
SRC_URI[libxcrypt-static.sha256sum] = "6cc9d9e34ce2009a43426a764edf9e042d324de17045288ed59104839cfc74a1"
