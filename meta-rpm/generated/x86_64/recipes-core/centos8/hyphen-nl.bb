SUMMARY = "generated recipe based on hyphen-nl srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-nl = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-nl-0.20050617-18.el8.noarch.rpm \
          "

SRC_URI[hyphen-nl.sha256sum] = "fdcfcbd9a086a67a46a153bc6559068d1970a5589edcaac7ad0825a8df4d7837"
