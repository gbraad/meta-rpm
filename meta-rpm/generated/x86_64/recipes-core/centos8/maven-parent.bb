SUMMARY = "generated recipe based on maven-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-parent = "apache-parent java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-parent-27-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-parent.sha256sum] = "6b59f8da0e62430c513007b16de50419a11cf8f5da2568c634731639ef870ae9"
