SUMMARY = "generated recipe based on libpq srpm"
DESCRIPTION = "Description"
LICENSE = "PostgreSQL"
RPM_LICENSE = "PostgreSQL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "krb5-libs openldap openssl pkgconfig-native"
RPM_SONAME_PROV_libpq = "libpq.so.5"
RPM_SONAME_REQ_libpq = "libc.so.6 libcrypto.so.1.1 libgssapi_krb5.so.2 libldap_r-2.4.so.2 libpthread.so.0 libssl.so.1.1"
RDEPENDS_libpq = "glibc krb5-libs openldap openssl-libs"
RPM_SONAME_REQ_libpq-devel = "libc.so.6 libpq.so.5 libpthread.so.0"
RPROVIDES_libpq-devel = "libpq-dev (= 12.4)"
RDEPENDS_libpq-devel = "glibc libpq pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpq-12.4-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpq-devel-12.4-1.el8_2.x86_64.rpm \
          "

SRC_URI[libpq.sha256sum] = "48f84ba42f787a3a9f3fea3f1448ad281377f783144373305a185b3baaf7c636"
SRC_URI[libpq-devel.sha256sum] = "e9ea2622c1963e09cf09f4afd9281cd2ce34ad6b997abbd1dcd0a60eea68a880"
