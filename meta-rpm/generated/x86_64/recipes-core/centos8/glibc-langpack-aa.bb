SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-aa = "locale-base-aa-dj (= 2.28) locale-base-aa-dj.utf8 (= 2.28) locale-base-aa-er (= 2.28) locale-base-aa-er@saaho (= 2.28) locale-base-aa-et (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa-dj (= 2.28) virtual-locale-aa-dj.utf8 (= 2.28) virtual-locale-aa-er (= 2.28) virtual-locale-aa-er@saaho (= 2.28) virtual-locale-aa-et (= 2.28)"
RDEPENDS_glibc-langpack-aa = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-aa-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-aa.sha256sum] = "a9d303942e4d002beeeaafc04512b1a3a4996ae5d0079f5541b818b421fbd97d"
