SUMMARY = "generated recipe based on centos-obsolete-packages srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/centos-obsolete-packages-8-4.noarch.rpm \
          "

SRC_URI[centos-obsolete-packages.sha256sum] = "387cec7987c0732dbc4196ee741ab47eebe5ffb84dc87ca59288bcf6c02be3dc"
