SUMMARY = "generated recipe based on ustr srpm"
DESCRIPTION = "Description"
LICENSE = "MIT | LGPL-2.0 | BSD"
RPM_LICENSE = "MIT or LGPLv2+ or BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_ustr = "libustr-1.0.so.1"
RPM_SONAME_REQ_ustr = "libc.so.6"
RDEPENDS_ustr = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ustr-1.0.4-26.el8.x86_64.rpm \
          "

SRC_URI[ustr.sha256sum] = "7ae4775813716773c36237c6ce58f3805266014dfb5c1060e16ed83e098b0694"
