SUMMARY = "generated recipe based on rtkit srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & BSD"
RPM_LICENSE = "GPLv3+ and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-libs libcap pkgconfig-native"
RPM_SONAME_REQ_rtkit = "libc.so.6 libcap.so.2 libdbus-1.so.3 libpthread.so.0 librt.so.1"
RDEPENDS_rtkit = "bash dbus dbus-libs glibc libcap polkit systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rtkit-0.11-19.el8.x86_64.rpm \
          "

SRC_URI[rtkit.sha256sum] = "4c3bcfe89f645a8a7a2519c10d62b4190435bc3b7cf88cc8c362670cea6d8596"
