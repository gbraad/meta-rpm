SUMMARY = "generated recipe based on ypbind srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libnsl2 libtirpc pkgconfig-native systemd-libs"
RPM_SONAME_REQ_ypbind = "libc.so.6 libnsl.so.2 libpthread.so.0 libsystemd.so.0 libtirpc.so.3"
RDEPENDS_ypbind = "bash glibc libnsl2 libtirpc nss_nis rpcbind systemd systemd-libs yp-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ypbind-2.5-2.el8.x86_64.rpm \
          "

SRC_URI[ypbind.sha256sum] = "0123e32a038943f1a435307cf4df5237ff2c553718a4b865ed56452edd7fc356"
