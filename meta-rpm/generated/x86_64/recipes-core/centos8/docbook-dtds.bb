SUMMARY = "generated recipe based on docbook-dtds srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Copyright only"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_docbook-dtds = "bash coreutils libxml2 sed sgml-common xml-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/docbook-dtds-1.0-69.el8.noarch.rpm \
          "

SRC_URI[docbook-dtds.sha256sum] = "e3b684be7d5bed0a8f3edaf49e0ff97abdbaaec2c84ece796a87377e0933e471"
