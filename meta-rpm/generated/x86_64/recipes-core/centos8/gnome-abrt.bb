SUMMARY = "generated recipe based on gnome-abrt srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "abrt atk cairo gdk-pixbuf glib-2.0 gtk+3 libreport libreport-gtk libx11 pango pkgconfig-native platform-python3"
RPM_SONAME_REQ_gnome-abrt = "libX11.so.6 libabrt.so.0 libabrt_gui.so.0 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdl.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libpython3.6m.so.1.0 libreport-gtk.so.0 libreport.so.0 libutil.so.1"
RDEPENDS_gnome-abrt = "abrt-gui-libs abrt-libs atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libX11 libreport libreport-gtk pango platform-python python3-dbus python3-gobject python3-humanize python3-inotify python3-libreport python3-libs xdg-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-abrt-1.2.6-5.el8.x86_64.rpm \
          "

SRC_URI[gnome-abrt.sha256sum] = "67c19df096865a57c9299e1ec57ea69c0d9d858c91382f4e640805ef29ced7bb"
