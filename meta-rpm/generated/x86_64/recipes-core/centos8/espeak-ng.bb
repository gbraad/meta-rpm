SUMMARY = "generated recipe based on espeak-ng srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pcaudiolib pkgconfig-native"
RPM_SONAME_PROV_espeak-ng = "libespeak-ng.so.1"
RPM_SONAME_REQ_espeak-ng = "libc.so.6 libm.so.6 libpcaudio.so.0 libpthread.so.0"
RDEPENDS_espeak-ng = "glibc pcaudiolib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/espeak-ng-1.49.2-4.el8.x86_64.rpm \
          "

SRC_URI[espeak-ng.sha256sum] = "68744ae281bd5456babc72150a17bc570779a46638103688983c25425d8abbb5"
