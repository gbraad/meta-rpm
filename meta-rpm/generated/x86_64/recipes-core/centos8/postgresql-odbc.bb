SUMMARY = "generated recipe based on postgresql-odbc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libpq pkgconfig-native unixodbc"
RPM_SONAME_REQ_postgresql-odbc = "libc.so.6 libodbcinst.so.2 libpq.so.5 libpthread.so.0"
RDEPENDS_postgresql-odbc = "glibc libpq unixODBC"
RDEPENDS_postgresql-odbc-tests = "bash gcc make postgresql-odbc unixODBC-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postgresql-odbc-10.03.0000-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/postgresql-odbc-tests-10.03.0000-2.el8.x86_64.rpm \
          "

SRC_URI[postgresql-odbc.sha256sum] = "dd9d9b7cbdd966d3282f97aed802feea243ad6c7d90ecf99fcd5fe61ce907ec5"
SRC_URI[postgresql-odbc-tests.sha256sum] = "05fac4d5d1a971236916edf05a01104be5d3aa476ae427be23ed282795ce02f9"
