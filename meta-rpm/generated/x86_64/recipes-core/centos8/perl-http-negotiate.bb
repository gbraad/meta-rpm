SUMMARY = "generated recipe based on perl-HTTP-Negotiate srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-HTTP-Negotiate = "perl-Exporter perl-HTTP-Message perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-HTTP-Negotiate-6.01-19.el8.noarch.rpm \
          "

SRC_URI[perl-HTTP-Negotiate.sha256sum] = "418e50e6a6f59a7ac47286566485f6ea59797a933772b57fa5a575580b81ee2f"
