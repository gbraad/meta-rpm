SUMMARY = "generated recipe based on libpciaccess srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libpciaccess = "libpciaccess.so.0"
RPM_SONAME_REQ_libpciaccess = "libc.so.6"
RDEPENDS_libpciaccess = "glibc hwdata"
RPM_SONAME_REQ_libpciaccess-devel = "libpciaccess.so.0"
RPROVIDES_libpciaccess-devel = "libpciaccess-dev (= 0.14)"
RDEPENDS_libpciaccess-devel = "libpciaccess pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpciaccess-0.14-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpciaccess-devel-0.14-1.el8.x86_64.rpm \
          "

SRC_URI[libpciaccess.sha256sum] = "759386be8f49257266ac614432b762b8e486a89aac5d5f7a581a0330efb59c77"
SRC_URI[libpciaccess-devel.sha256sum] = "d2ff03dbddaa93a30b38e2081609fe76dd18b79046088ba0eee983b0d5f802c6"
