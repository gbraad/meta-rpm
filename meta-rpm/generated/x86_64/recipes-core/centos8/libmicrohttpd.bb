SUMMARY = "generated recipe based on libmicrohttpd srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gnutls gnutls-libs pkgconfig-native"
RPM_SONAME_PROV_libmicrohttpd = "libmicrohttpd.so.12"
RPM_SONAME_REQ_libmicrohttpd = "libc.so.6 libgnutls.so.30 libpthread.so.0"
RDEPENDS_libmicrohttpd = "glibc gnutls info"
RPM_SONAME_REQ_libmicrohttpd-devel = "libmicrohttpd.so.12"
RPROVIDES_libmicrohttpd-devel = "libmicrohttpd-dev (= 0.9.59)"
RDEPENDS_libmicrohttpd-devel = "gnutls-devel libmicrohttpd pkgconf-pkg-config"
RDEPENDS_libmicrohttpd-doc = "bash libmicrohttpd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libmicrohttpd-0.9.59-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmicrohttpd-devel-0.9.59-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmicrohttpd-doc-0.9.59-2.el8.noarch.rpm \
          "

SRC_URI[libmicrohttpd.sha256sum] = "b0acbf526a1e8c2c736d3a08ca2745d7eb9b99f1b3338740a3fb04b63f0ffcc6"
SRC_URI[libmicrohttpd-devel.sha256sum] = "52bffef72204d1aab9157a69e775de3cb5c27390e399728a061bfb0f55d8ab0b"
SRC_URI[libmicrohttpd-doc.sha256sum] = "4d63c9b1646e7e6d6569c7fb5e801e78ac2e37fbe9c090ae40e9bd2d1e47c434"
