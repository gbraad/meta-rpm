SUMMARY = "generated recipe based on perl-Sub-Name srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sub-Name = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sub-Name = "glibc perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Sub-Name-0.21-7.el8.x86_64.rpm \
          "

SRC_URI[perl-Sub-Name.sha256sum] = "578cfd694606363eb39eaca2b927c9a0fc9c89ef8f01fbd3d0989057e001f634"
