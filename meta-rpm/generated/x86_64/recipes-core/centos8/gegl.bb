SUMMARY = "generated recipe based on gegl srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0 & GPL-3.0"
RPM_LICENSE = "LGPLv3+ and GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "babl cairo gdk-pixbuf glib-2.0 ilmbase jasper libgcc libjpeg-turbo libopenraw libpng librsvg libspiro openexr pango pkgconfig-native sdl suitesparse zlib"
RPM_SONAME_PROV_gegl = "libgegl-0.2.so.0"
RPM_SONAME_REQ_gegl = "libHalf.so.12 libIex-2_2.so.12 libIexMath-2_2.so.12 libIlmImf-2_2.so.22 libIlmThread-2_2.so.12 libImath-2_2.so.12 libSDL-1.2.so.0 libbabl-0.1.so.0 libc.so.6 libcairo.so.2 libgcc_s.so.1 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libjasper.so.4 libjpeg.so.62 libm.so.6 libmvec.so.1 libopenraw.so.7 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libpthread.so.0 librsvg-2.so.2 libspiro.so.0 libstdc++.so.6 libumfpack.so.5 libz.so.1"
RDEPENDS_gegl = "OpenEXR-libs SDL babl cairo dcraw gdk-pixbuf2 glib2 glibc ilmbase jasper-libs libgcc libjpeg-turbo libopenraw libpng librsvg2 libspiro libstdc++ pango suitesparse zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gegl-0.2.0-39.el8.x86_64.rpm \
          "

SRC_URI[gegl.sha256sum] = "13504fbf24db4d051909f24444c6667402597d3782d03c2cb33c2f262e626b26"
