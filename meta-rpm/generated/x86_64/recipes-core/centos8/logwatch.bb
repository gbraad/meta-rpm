SUMMARY = "generated recipe based on logwatch srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_logwatch = "bash crontabs grep mailx perl-Date-Manip perl-Exporter perl-File-Temp perl-Getopt-Long perl-Sys-CPU perl-Sys-MemInfo perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/logwatch-7.4.3-9.el8.noarch.rpm \
          "

SRC_URI[logwatch.sha256sum] = "a0b42e9dcf8d41ac91ab3e1a2a6f73f08ca5c1618e20196a5790fb1f925eccd9"
