SUMMARY = "generated recipe based on perl-Data-UUID srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & MIT"
RPM_LICENSE = "BSD and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Data-UUID = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Data-UUID = "glibc perl-Carp perl-Digest-MD5 perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Data-UUID-1.221-10.el8.x86_64.rpm \
          "

SRC_URI[perl-Data-UUID.sha256sum] = "68c133e5632a4b9fc83a951d706b798e4959d385b14133c60ee3404f12fcc00e"
