SUMMARY = "generated recipe based on libimobiledevice srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gnutls gnutls-libs libgcrypt libplist libtasn1 libusbmuxd pkgconfig-native"
RPM_SONAME_PROV_libimobiledevice = "libimobiledevice.so.6"
RPM_SONAME_REQ_libimobiledevice = "libc.so.6 libgcrypt.so.20 libgnutls.so.30 libplist.so.3 libpthread.so.0 libtasn1.so.6 libusbmuxd.so.4"
RDEPENDS_libimobiledevice = "glibc gnutls libgcrypt libplist libtasn1 libusbmuxd"
RPM_SONAME_REQ_libimobiledevice-devel = "libimobiledevice.so.6"
RPROVIDES_libimobiledevice-devel = "libimobiledevice-dev (= 1.2.0)"
RDEPENDS_libimobiledevice-devel = "gnutls-devel libimobiledevice libplist-devel libtasn1-devel libusbmuxd-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libimobiledevice-1.2.0-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libimobiledevice-devel-1.2.0-16.el8.x86_64.rpm \
          "

SRC_URI[libimobiledevice.sha256sum] = "b0f2567fb76cf97d118f17d50b072f0265e37daa45ef005b110fc2ed78739adb"
SRC_URI[libimobiledevice-devel.sha256sum] = "a054e6289da4cf9804856dea0a41940fd1181bcfcea1ff2392216ee4ce13143e"
