SUMMARY = "generated recipe based on perl-IO-Multiplex srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-Multiplex = "perl-Carp perl-IO perl-Socket perl-Time-HiRes perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-IO-Multiplex-1.16-9.el8.noarch.rpm \
          "

SRC_URI[perl-IO-Multiplex.sha256sum] = "3808d732d8749d24de315263b0cce5287880da4a3289c4933a3c5bc11c819f3e"
