SUMMARY = "generated recipe based on lohit-devanagari-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-devanagari-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lohit-devanagari-fonts-2.95.4-3.el8.noarch.rpm \
          "

SRC_URI[lohit-devanagari-fonts.sha256sum] = "308762b9a67166a418ee6a818cf6c76197731dea02d99cfd653c4130517a309f"
