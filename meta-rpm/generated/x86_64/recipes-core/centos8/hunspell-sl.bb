SUMMARY = "generated recipe based on hunspell-sl srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-2.0"
RPM_LICENSE = "GPL+ or LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-sl = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-sl-0.20070127-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-sl.sha256sum] = "3d807e4f0e387cd49b4337fa8034cd7002a19aa5f87413658fc72bbd9cb04dad"
