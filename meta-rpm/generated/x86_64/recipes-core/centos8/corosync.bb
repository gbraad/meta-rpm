SUMMARY = "generated recipe based on corosync srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libqb pkgconfig-native readline"
RPM_SONAME_REQ_corosync-vqsim = "libc.so.6 libcorosync_common.so.4 libpthread.so.0 libqb.so.0 libreadline.so.7"
RDEPENDS_corosync-vqsim = "corosynclib glibc libqb pkgconf-pkg-config readline"
RPM_SONAME_PROV_corosynclib = "libcfg.so.7 libcmap.so.4 libcorosync_common.so.4 libcpg.so.4 libquorum.so.5 libsam.so.4 libvotequorum.so.8"
RPM_SONAME_REQ_corosynclib = "libc.so.6 libpthread.so.0 libqb.so.0 librt.so.1"
RDEPENDS_corosynclib = "glibc libqb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/corosynclib-3.0.3-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/corosync-vqsim-3.0.3-2.el8.x86_64.rpm \
          "

SRC_URI[corosync-vqsim.sha256sum] = "05f21c7550e03bb13896f4d49dd1c2a82272b00542341e959622d4b3f1f94788"
SRC_URI[corosynclib.sha256sum] = "daaaf1f687ccc631fe45e8f21772848451119b00fe46cf211448f8509f0ced46"
