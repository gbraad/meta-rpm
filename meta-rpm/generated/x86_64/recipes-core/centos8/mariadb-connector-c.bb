SUMMARY = "generated recipe based on mariadb-connector-c srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "curl e2fsprogs krb5-libs openssl pkgconfig-native zlib"
RPM_SONAME_PROV_mariadb-connector-c = "libmariadb.so.3"
RPM_SONAME_REQ_mariadb-connector-c = "libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_mariadb-connector-c = "glibc krb5-libs libcom_err libcurl mariadb-connector-c-config openssl-libs zlib"
RPM_SONAME_REQ_mariadb-connector-c-devel = "libc.so.6 libmariadb.so.3"
RPROVIDES_mariadb-connector-c-devel = "mariadb-connector-c-dev (= 3.0.7)"
RDEPENDS_mariadb-connector-c-devel = "glibc mariadb-connector-c openssl-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mariadb-connector-c-3.0.7-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mariadb-connector-c-config-3.0.7-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mariadb-connector-c-devel-3.0.7-1.el8.x86_64.rpm \
          "

SRC_URI[mariadb-connector-c.sha256sum] = "739c49ae16e933746943042ba0fdf56cf5a811d4237e2b8d8183bed93de7a3f4"
SRC_URI[mariadb-connector-c-config.sha256sum] = "7f06cf0a160953b2e74b79b60d517147f41052e8ba343a1c759a49654bec9aba"
SRC_URI[mariadb-connector-c-devel.sha256sum] = "a337efd7df9c06134c2928db666b33c37bef7d68d82b367bc61b5e3eb897e09a"
