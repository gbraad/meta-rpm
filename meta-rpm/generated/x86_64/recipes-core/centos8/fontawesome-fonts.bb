SUMMARY = "generated recipe based on fontawesome-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1 & MIT"
RPM_LICENSE = "OFL and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_fontawesome-fonts = "fontpackages-filesystem"
RDEPENDS_fontawesome-fonts-web = "fontawesome-fonts"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fontawesome-fonts-4.7.0-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/fontawesome-fonts-web-4.7.0-4.el8.noarch.rpm \
          "

SRC_URI[fontawesome-fonts.sha256sum] = "6d02f30855c4b1dff0e2b01bedf192aff36bf18e3ccc79616b1b749356e8fdac"
SRC_URI[fontawesome-fonts-web.sha256sum] = "39fdb0774ad68fdb0396647e16a143778a9f02a658cea04c0ea48696327a74cb"
