SUMMARY = "generated recipe based on speech-dispatcher srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0"
RPM_LICENSE = "GPLv2+ and GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib dotconf espeak-ng glib-2.0 libao libsndfile2 libtool-ltdl pkgconfig-native pulseaudio"
RDEPENDS_python3-speechd = "platform-python python3-pyxdg speech-dispatcher"
RPM_SONAME_PROV_speech-dispatcher = "libspeechd.so.2"
RPM_SONAME_REQ_speech-dispatcher = "libao.so.4 libasound.so.2 libc.so.6 libdotconf.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgthread-2.0.so.0 libltdl.so.7 libm.so.6 libpthread.so.0 libpulse-simple.so.0 libpulse.so.0 libsndfile.so.1"
RDEPENDS_speech-dispatcher = "alsa-lib bash dotconf glib2 glibc libao libsndfile libtool-ltdl pulseaudio-libs speech-dispatcher-espeak-ng systemd"
RPM_SONAME_REQ_speech-dispatcher-devel = "libspeechd.so.2"
RPROVIDES_speech-dispatcher-devel = "speech-dispatcher-dev (= 0.8.8)"
RDEPENDS_speech-dispatcher-devel = "glib2-devel pkgconf-pkg-config speech-dispatcher"
RDEPENDS_speech-dispatcher-doc = "bash info speech-dispatcher"
RPM_SONAME_REQ_speech-dispatcher-espeak-ng = "libc.so.6 libdotconf.so.0 libespeak-ng.so.1 libglib-2.0.so.0 libgthread-2.0.so.0 libltdl.so.7 libm.so.6 libpthread.so.0 libsndfile.so.1"
RDEPENDS_speech-dispatcher-espeak-ng = "dotconf espeak-ng glib2 glibc libsndfile libtool-ltdl speech-dispatcher"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-speechd-0.8.8-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/speech-dispatcher-0.8.8-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/speech-dispatcher-espeak-ng-0.8.8-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/speech-dispatcher-devel-0.8.8-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/speech-dispatcher-doc-0.8.8-6.el8.noarch.rpm \
          "

SRC_URI[python3-speechd.sha256sum] = "b5abaa07b4e5e933b9b6a4d64b1525bb20c359ebce3783eefeff1e5a5f89cadf"
SRC_URI[speech-dispatcher.sha256sum] = "cf04480da615c7ad8f8b347759100bf2b5ed72a4e5ccdb326ac8f94a6a0ef93e"
SRC_URI[speech-dispatcher-devel.sha256sum] = "8d88d99fca21022797bf2834f2fb01a2d61c0f42552d790a5feb823fee83b06a"
SRC_URI[speech-dispatcher-doc.sha256sum] = "f457e2ad314f8b60b479f74de9b247ddeba4fb3ac5145fe6f9547c31e74df61c"
SRC_URI[speech-dispatcher-espeak-ng.sha256sum] = "b975e4d0ceec864ce2ceaf11d45f9c1af7e983b6a6f56cc607d01ca527bce7b5"
