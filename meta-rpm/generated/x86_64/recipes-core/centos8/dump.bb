SUMMARY = "generated recipe based on dump srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 e2fsprogs libselinux lzo ncurses pkgconfig-native readline zlib"
RPM_SONAME_REQ_dump = "libbz2.so.1 libc.so.6 libcom_err.so.2 libe2p.so.2 libext2fs.so.2 liblzo2.so.2 libreadline.so.7 libselinux.so.1 libtinfo.so.6 libz.so.1"
RDEPENDS_dump = "bzip2-libs e2fsprogs-libs glibc libcom_err libselinux lzo ncurses-libs readline rmt setup zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dump-0.4-0.36.b46.el8.x86_64.rpm \
          "

SRC_URI[dump.sha256sum] = "fc75b1ffc18a079f61bfdb0666cee55fb2d7d94f5653c7c4445dc23de3968c8c"
