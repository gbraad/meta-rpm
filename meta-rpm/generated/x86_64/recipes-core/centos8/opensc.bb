SUMMARY = "generated recipe based on opensc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native readline zlib"
RPM_SONAME_PROV_opensc = "libopensc.so.6 libsmm-local.so.6"
RPM_SONAME_REQ_opensc = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libpthread.so.0 libreadline.so.7 libz.so.1"
RDEPENDS_opensc = "bash glibc openssl-libs pcsc-lite pcsc-lite-libs pkgconf-pkg-config readline zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/opensc-0.19.0-7.el8.x86_64.rpm \
          "

SRC_URI[opensc.sha256sum] = "590a9642d5abff8c5ca57bdffed3bdaa96db84fa98a8273bb8c88800d6641be6"
