SUMMARY = "generated recipe based on open-sans-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_open-sans-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/open-sans-fonts-1.10-6.el8.noarch.rpm \
          "

SRC_URI[open-sans-fonts.sha256sum] = "ce7ef2eb3a548c0f137d10bed6799f855add5d76af08abb1dd7607d0a1f9c1ac"
