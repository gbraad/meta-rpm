SUMMARY = "generated recipe based on ethtool srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ethtool = "libc.so.6 libm.so.6"
RDEPENDS_ethtool = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ethtool-5.0-2.el8.x86_64.rpm \
          "

SRC_URI[ethtool.sha256sum] = "4a64f67b3f0f6932d003d403c0ecbcd91cf44e7a54f9fa7e68afbd1117f3f13c"
