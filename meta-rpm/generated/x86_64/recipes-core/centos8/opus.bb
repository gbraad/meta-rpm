SUMMARY = "generated recipe based on opus srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libogg pkgconfig-native"
RPM_SONAME_PROV_opus = "libopus.so.0"
RPM_SONAME_REQ_opus = "libc.so.6 libgcc_s.so.1 libm.so.6"
RDEPENDS_opus = "glibc libgcc"
RPM_SONAME_REQ_opus-devel = "libopus.so.0"
RPROVIDES_opus-devel = "opus-dev (= 1.3)"
RDEPENDS_opus-devel = "libogg-devel opus pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/opus-1.3-0.4.beta.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/opus-devel-1.3-0.4.beta.el8.x86_64.rpm \
          "

SRC_URI[opus.sha256sum] = "00512c56e8931eb0ab52de91d0272f00bf904d6f2042b580115edd7eb4a42df2"
SRC_URI[opus-devel.sha256sum] = "3d5185fe8a567f46f290bb48a1a7b5837458061e3eb10459ee6ab96e784c7054"
