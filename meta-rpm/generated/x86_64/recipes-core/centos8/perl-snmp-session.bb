SUMMARY = "generated recipe based on perl-SNMP_Session srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0"
RPM_LICENSE = "Artistic 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-SNMP_Session = "perl-Carp perl-Exporter perl-IO perl-Socket perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-SNMP_Session-1.13-17.el8.noarch.rpm \
          "

SRC_URI[perl-SNMP_Session.sha256sum] = "da8e5b112c54352ebf998ad29db99bc4a8832653abeddfd81ce17446584d3688"
