SUMMARY = "generated recipe based on brltty srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib at-spi2-core bluez dbus-libs espeak-ng glib-2.0 libx11 libxaw libxext libxt libxtst pkgconfig-native platform-python3 tcl"
RPM_SONAME_PROV_brlapi = "libbrlapi.so.0.6"
RPM_SONAME_REQ_brlapi = "libX11.so.6 libXext.so.6 libXtst.so.6 libbluetooth.so.3 libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_brlapi = "bash bluez-libs brltty coreutils glibc glibc-common libX11 libXext libXtst shadow-utils util-linux"
RPM_SONAME_REQ_brlapi-devel = "libbrlapi.so.0.6"
RPROVIDES_brlapi-devel = "brlapi-dev (= 0.6.7)"
RDEPENDS_brlapi-devel = "brlapi"
RPM_SONAME_REQ_brlapi-java = "libbrlapi.so.0.6 libc.so.6"
RDEPENDS_brlapi-java = "brlapi glibc"
RPM_SONAME_REQ_brltty = "ld-linux-x86-64.so.2 libasound.so.2 libbluetooth.so.3 libbrlapi.so.0.6 libc.so.6 libdbus-1.so.3 libdl.so.2 libpthread.so.0"
RDEPENDS_brltty = "alsa-lib bash bluez-libs brlapi coreutils dbus-libs glibc platform-python systemd"
RPM_SONAME_REQ_brltty-at-spi2 = "libatspi.so.0 libc.so.6 libdbus-1.so.3 libglib-2.0.so.0"
RDEPENDS_brltty-at-spi2 = "at-spi2-core brltty dbus-libs glib2 glibc"
RDEPENDS_brltty-docs = "brltty"
RDEPENDS_brltty-dracut = "bash brltty dracut"
RPM_SONAME_REQ_brltty-espeak-ng = "libc.so.6 libespeak-ng.so.1"
RDEPENDS_brltty-espeak-ng = "brltty espeak-ng glibc"
RPM_SONAME_REQ_brltty-xw = "libX11.so.6 libXaw.so.7 libXt.so.6 libc.so.6"
RDEPENDS_brltty-xw = "brltty glibc libX11 libXaw libXt ucs-miscfixed-fonts xorg-x11-fonts-misc"
RPM_SONAME_REQ_python3-brlapi = "libbrlapi.so.0.6 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-brlapi = "brlapi glibc platform-python python3-libs"
RPM_SONAME_REQ_tcl-brlapi = "libbrlapi.so.0.6 libc.so.6 libtcl8.6.so"
RDEPENDS_tcl-brlapi = "brlapi glibc tcl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brlapi-0.6.7-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brlapi-java-0.6.7-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brltty-5.6-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brltty-at-spi2-5.6-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brltty-docs-5.6-28.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brltty-dracut-5.6-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brltty-espeak-ng-5.6-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/brltty-xw-5.6-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-brlapi-0.6.7-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tcl-brlapi-0.6.7-28.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/brlapi-devel-0.6.7-28.el8.x86_64.rpm \
          "

SRC_URI[brlapi.sha256sum] = "7fe873c825dc2af6d4788860cf495ba58d698c528072f52bd8d8b8bea756c2a7"
SRC_URI[brlapi-devel.sha256sum] = "9d5318be5216d3299cde2b67b9789a0fab7f4745538d3a7ec21e16270dfc2664"
SRC_URI[brlapi-java.sha256sum] = "d3e51c66c35e74c9fb6d2ac9201c9168631c053c8f09ef2b5e8224552a8b81b4"
SRC_URI[brltty.sha256sum] = "3c38e1bd6c4f61ba4081d93c2deb91b7546acde4ea4dcc164f16d800b44e986d"
SRC_URI[brltty-at-spi2.sha256sum] = "7e71cd3c03a2934869211a3f6fbacd498be63b47cae52c96bac2956869109138"
SRC_URI[brltty-docs.sha256sum] = "3444eabb78ee71f992fde755b8a5d94c62844075723b755787976eecd753d535"
SRC_URI[brltty-dracut.sha256sum] = "49144139c514b807ef1d2ac47bdc566b7ac5e7c8109fd6733aabace6178b3c65"
SRC_URI[brltty-espeak-ng.sha256sum] = "d8b20f1df7e1f060c105499ddfb7090246f2baf21977734d62a9810bdffc6bce"
SRC_URI[brltty-xw.sha256sum] = "63038d26648b8c0bc715605ce6d9dadfb3b945f72acda3cee0c429a24ce86e13"
SRC_URI[python3-brlapi.sha256sum] = "1f531922e5cca8f7fa61457746b787b1d32ff092dc17766f32b1386040ca0878"
SRC_URI[tcl-brlapi.sha256sum] = "04d6b6ec52df77237b413f43be8f5bf07da7caf91ec991e8175b8d0998088c6d"
