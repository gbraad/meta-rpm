SUMMARY = "generated recipe based on libgdither srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libgdither = "libgdither.so.1"
RPM_SONAME_REQ_libgdither = "libc.so.6"
RDEPENDS_libgdither = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgdither-0.6-17.el8.x86_64.rpm \
          "

SRC_URI[libgdither.sha256sum] = "196f0aa1a5e007480b3984239814a51e39df7d826f9cf13da853900a1ac1e71b"
