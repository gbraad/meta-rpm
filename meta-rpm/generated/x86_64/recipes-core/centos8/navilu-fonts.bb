SUMMARY = "generated recipe based on navilu-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_navilu-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/navilu-fonts-1.2-11.el8.noarch.rpm \
          "

SRC_URI[navilu-fonts.sha256sum] = "2ebaa8202d37eaf0e604e42c6afb3b0d15d63edf332b1ba96f7cd4f2b21c6d0e"
