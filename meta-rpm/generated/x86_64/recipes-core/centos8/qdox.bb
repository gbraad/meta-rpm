SUMMARY = "generated recipe based on qdox srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_qdox = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_qdox-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qdox-2.0-3.M9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qdox-javadoc-2.0-3.M9.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[qdox.sha256sum] = "f34ffea318f51fc72263d35b50a791e1fff1fa17602cbf50baf3928ba00a361a"
SRC_URI[qdox-javadoc.sha256sum] = "abe26be249c76db001dfdc553cd15c54148804ebbf2f8729df55300ab67fed6c"
