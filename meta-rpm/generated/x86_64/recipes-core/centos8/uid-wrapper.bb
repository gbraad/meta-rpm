SUMMARY = "generated recipe based on uid_wrapper srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_uid_wrapper = "libuid_wrapper.so.0"
RPM_SONAME_REQ_uid_wrapper = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_uid_wrapper = "cmake-filesystem glibc pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/uid_wrapper-1.2.4-4.el8.x86_64.rpm \
          "

SRC_URI[uid_wrapper.sha256sum] = "5a67b86a019488291298ce39c883e7e686fd908c8a41456114d1a25900ab3cf8"
