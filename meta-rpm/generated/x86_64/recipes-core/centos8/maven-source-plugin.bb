SUMMARY = "generated recipe based on maven-source-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-source-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-archiver maven-lib plexus-archiver plexus-utils"
RDEPENDS_maven-source-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-source-plugin-3.0.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-source-plugin-javadoc-3.0.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-source-plugin.sha256sum] = "8d6c99cefbdf130f4c30b4c6bd6e13b792a47fc48f2a5e030566136c48a798e5"
SRC_URI[maven-source-plugin-javadoc.sha256sum] = "c94bb932a7eb87280656d15ac1573055f443a6a4e992a85d074784e5e61e3ea1"
