SUMMARY = "generated recipe based on LibRaw srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & (CDDL-1.0 | LGPL-2.0)"
RPM_LICENSE = "BSD and (CDDL or LGPLv2)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "jasper lcms2 libgcc libjpeg-turbo pkgconfig-native"
RPM_SONAME_PROV_LibRaw = "libraw.so.19 libraw_r.so.19"
RPM_SONAME_REQ_LibRaw = "libc.so.6 libgcc_s.so.1 libgomp.so.1 libjasper.so.4 libjpeg.so.62 liblcms2.so.2 libm.so.6 libstdc++.so.6"
RDEPENDS_LibRaw = "glibc jasper-libs lcms2 libgcc libgomp libjpeg-turbo libstdc++"
RPM_SONAME_REQ_LibRaw-devel = "libraw.so.19 libraw_r.so.19"
RPROVIDES_LibRaw-devel = "LibRaw-dev (= 0.19.5)"
RDEPENDS_LibRaw-devel = "LibRaw lcms2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/LibRaw-0.19.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/LibRaw-devel-0.19.5-1.el8.x86_64.rpm \
          "

SRC_URI[LibRaw.sha256sum] = "48684ef90485effe0b63c2d66ec8901f37c800536ad4d08ea6a2c3d40dc5165b"
SRC_URI[LibRaw-devel.sha256sum] = "04620cc19fded5d070ebb50b62cba1f26e7b51a559df92a873bf3dd896db63fd"
