SUMMARY = "generated recipe based on matchbox-window-manager srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "fontconfig freetype glib-2.0 libjpeg-turbo libmatchbox libpng libx11 libxcursor libxext libxfixes libxft pango pkgconfig-native zlib"
RPM_SONAME_REQ_matchbox-window-manager = "libX11.so.6 libXcursor.so.1 libXext.so.6 libXfixes.so.3 libXft.so.2 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libjpeg.so.62 libmb.so.1 libpango-1.0.so.0 libpangoft2-1.0.so.0 libpangoxft-1.0.so.0 libpng16.so.16 libz.so.1"
RDEPENDS_matchbox-window-manager = "filesystem fontconfig freetype glib2 glibc libX11 libXcursor libXext libXfixes libXft libjpeg-turbo libmatchbox libpng pango zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/matchbox-window-manager-1.2-23.20070628svn.el8.x86_64.rpm \
          "

SRC_URI[matchbox-window-manager.sha256sum] = "4ed33f2d6a6214974bea5629e93d857eea3920afd104751c5e136fc840efa23f"
