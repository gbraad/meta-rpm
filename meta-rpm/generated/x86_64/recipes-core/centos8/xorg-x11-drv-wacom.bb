SUMMARY = "generated recipe based on xorg-x11-drv-wacom srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 libxext libxi libxinerama libxrandr pkgconfig-native systemd-libs xorg-x11-server"
RPM_SONAME_REQ_xorg-x11-drv-wacom = "libX11.so.6 libXext.so.6 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libc.so.6 libm.so.6"
RDEPENDS_xorg-x11-drv-wacom = "glibc libX11 libXext libXi libXinerama libXrandr xorg-x11-drv-wacom-serial-support xorg-x11-server-Xorg"
RPM_SONAME_REQ_xorg-x11-drv-wacom-devel = "libc.so.6 libm.so.6"
RPROVIDES_xorg-x11-drv-wacom-devel = "xorg-x11-drv-wacom-dev (= 0.38.0)"
RDEPENDS_xorg-x11-drv-wacom-devel = "glibc pkgconf-pkg-config xorg-x11-server-devel"
RPM_SONAME_REQ_xorg-x11-drv-wacom-serial-support = "libc.so.6 libm.so.6 libudev.so.1"
RDEPENDS_xorg-x11-drv-wacom-serial-support = "glibc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-wacom-0.38.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-wacom-serial-support-0.38.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xorg-x11-drv-wacom-devel-0.38.0-1.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-wacom.sha256sum] = "266dbedaf42cbec318c7878ccfd5c2900ed29f6b08e7770c627017d110f4c2ca"
SRC_URI[xorg-x11-drv-wacom-devel.sha256sum] = "a3269f39a0967ebacb11eb6e447f83b11db09b869c530278982b9947e0a709ee"
SRC_URI[xorg-x11-drv-wacom-serial-support.sha256sum] = "b2bfb195f81727305f27fd4c5b3d4122be72f8c24d0dfa0dddad4cfa77faa06c"
