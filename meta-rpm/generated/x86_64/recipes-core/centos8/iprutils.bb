SUMMARY = "generated recipe based on iprutils srpm"
DESCRIPTION = "Description"
LICENSE = "CPL-1.0"
RPM_LICENSE = "CPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native zlib"
RPM_SONAME_REQ_iprutils = "libc.so.6 libform.so.6 libm.so.6 libmenu.so.6 libncurses.so.6 libpanel.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_iprutils = "bash glibc ncurses-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iprutils-2.4.18.1-1.el8.x86_64.rpm \
          "

SRC_URI[iprutils.sha256sum] = "6414052779692dca5cddde0da6e25aec7e8ae3b34bfcc5954883d03ada3f9aa1"
