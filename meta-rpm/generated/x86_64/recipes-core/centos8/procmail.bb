SUMMARY = "generated recipe based on procmail srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPLv2+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_procmail = "libc.so.6 libm.so.6"
RDEPENDS_procmail = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/procmail-3.22-47.el8.x86_64.rpm \
          "

SRC_URI[procmail.sha256sum] = "0dc0357451c6bb0b94392cf766eb7ab8ec2eefdeaa1a61a74ca0041d7c630256"
