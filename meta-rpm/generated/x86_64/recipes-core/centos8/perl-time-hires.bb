SUMMARY = "generated recipe based on perl-Time-HiRes srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Time-HiRes = "libc.so.6 libperl.so.5.26 libpthread.so.0 librt.so.1"
RDEPENDS_perl-Time-HiRes = "glibc perl-Carp perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Time-HiRes-1.9758-1.el8.x86_64.rpm \
          "

SRC_URI[perl-Time-HiRes.sha256sum] = "0eb39b280cfa4bd829182648b5f4b62e4684c975bc53d4bf7f92124d1aca6c08"
