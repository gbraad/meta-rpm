SUMMARY = "generated recipe based on glassfish-master-pom srpm"
DESCRIPTION = "Description"
LICENSE = "CDDL-1.0 | GPL-2.0"
RPM_LICENSE = "CDDL or GPLv2 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_glassfish-master-pom = "java-1.8.0-openjdk-headless javapackages-filesystem maven-compiler-plugin"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glassfish-master-pom-8-11.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[glassfish-master-pom.sha256sum] = "32d9043a5825b7cb0a03baff9465ca55bea4a95555e18e411e64711d66ee46e6"
