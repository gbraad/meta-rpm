SUMMARY = "generated recipe based on libsmbios srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 |  CLOSED"
RPM_LICENSE = "GPLv2+ or OSL 2.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libsmbios = "libsmbios_c.so.2"
RPM_SONAME_REQ_libsmbios = "libc.so.6"
RDEPENDS_libsmbios = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsmbios-2.4.1-2.el8.x86_64.rpm \
          "

SRC_URI[libsmbios.sha256sum] = "5092704c041bb246eaafd478bedface3f99ce8fa233ada5cf96807f67e980e42"
