SUMMARY = "generated recipe based on sip srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | GPL-3.0 & (GPL-3.0)"
RPM_LICENSE = "GPLv2 or GPLv3 and (GPLv3+ with exceptions)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native python3"
RPM_SONAME_REQ_python3-pyqt5-sip = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_python3-pyqt5-sip = "glibc libgcc libstdc++ platform-python"
RPROVIDES_python3-sip-devel = "python3-sip-dev (= 4.19.19)"
RDEPENDS_python3-sip-devel = "platform-python python36-devel sip"
RPM_SONAME_REQ_sip = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_sip = "bash glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pyqt5-sip-4.19.19-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sip-4.19.19-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-sip-devel-4.19.19-1.el8.x86_64.rpm \
          "

SRC_URI[python3-pyqt5-sip.sha256sum] = "a7b376b0978cf962672f02d5352dd40f3284d626f25bea8da77b000ce0b1c7b3"
SRC_URI[python3-sip-devel.sha256sum] = "869a840e6fc36a17424a0f2845f6c0074c4c49936803de167dd4512cdbc7c475"
SRC_URI[sip.sha256sum] = "5359e39439434a41036febf0689998ea53e894cecfe8692c102e9c983e107f97"
