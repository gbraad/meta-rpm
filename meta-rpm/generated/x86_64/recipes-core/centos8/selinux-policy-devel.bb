SUMMARY = "generated recipe based on selinux-policy srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native policycoreutils-devel"
RPROVIDES_selinux-policy-devel = "selinux-policy-dev (= 3.14.3)"
RDEPENDS_selinux-policy-devel = "bash checkpolicy m4 make policycoreutils-devel selinux-policy"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/selinux-policy-devel-3.14.3-41.el8_2.8.noarch.rpm \
          "

SRC_URI[selinux-policy-devel.sha256sum] = "3f11916b71892d95fd6da94d0f9b031e16c0c2948061a745aafb5ec86f5e0b6b"
