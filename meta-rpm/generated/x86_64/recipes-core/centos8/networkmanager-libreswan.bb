SUMMARY = "generated recipe based on NetworkManager-libreswan srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libgcc libnl libsecret network-manager-applet networkmanager pango pkgconfig-native"
RPM_SONAME_PROV_NetworkManager-libreswan = "libnm-vpn-plugin-libreswan.so"
RPM_SONAME_REQ_NetworkManager-libreswan = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libnl-3.so.200 libnm.so.0 libpthread.so.0 libutil.so.1"
RDEPENDS_NetworkManager-libreswan = "NetworkManager NetworkManager-libnm bash dbus glib2 glibc libgcc libnl3 libreswan"
RPM_SONAME_PROV_NetworkManager-libreswan-gnome = "libnm-vpn-plugin-libreswan-editor.so"
RPM_SONAME_REQ_NetworkManager-libreswan-gnome = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdl.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libnm.so.0 libnma.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsecret-1.so.0"
RDEPENDS_NetworkManager-libreswan-gnome = "NetworkManager-libnm NetworkManager-libreswan atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libnma libsecret pango shared-mime-info"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/NetworkManager-libreswan-1.2.10-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/NetworkManager-libreswan-gnome-1.2.10-4.el8.x86_64.rpm \
          "

SRC_URI[NetworkManager-libreswan.sha256sum] = "4bc9d04284e0aa6bc340b6b1342228f3cce3e6caaf24287fd6a360380976e329"
SRC_URI[NetworkManager-libreswan-gnome.sha256sum] = "d51b26e782934060dc6f7af48e4666753b4e6c7075706e7eebf7f3e1105a409f"
