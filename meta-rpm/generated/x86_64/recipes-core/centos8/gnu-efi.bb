SUMMARY = "generated recipe based on gnu-efi srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_gnu-efi-devel = "gnu-efi-dev (= 3.0.8)"
RDEPENDS_gnu-efi-devel = "gnu-efi"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gnu-efi-3.0.8-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gnu-efi-devel-3.0.8-4.el8.x86_64.rpm \
          "

SRC_URI[gnu-efi.sha256sum] = "f29c3bb888834361b6b2d1f2070d27d83f4a9513bc4727ed982936e0cece4a92"
SRC_URI[gnu-efi-devel.sha256sum] = "50f9dec7a2ae4926dbf7b736597fb36d7f996ce618718ea1f76e38ef04a01e49"
