SUMMARY = "generated recipe based on grilo srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 gtk+3 liboauth libsoup-2.4 libxml2 pkgconfig-native totem-pl-parser"
RPM_SONAME_PROV_grilo = "libgrilo-0.3.so.0 libgrlnet-0.3.so.0 libgrlpls-0.3.so.0"
RPM_SONAME_REQ_grilo = "libc.so.6 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 liboauth.so.0 libpthread.so.0 libsoup-2.4.so.1 libtotem-plparser.so.18"
RDEPENDS_grilo = "glib2 glibc gtk3 liboauth libsoup totem-pl-parser"
RPM_SONAME_REQ_grilo-devel = "libgrilo-0.3.so.0 libgrlnet-0.3.so.0 libgrlpls-0.3.so.0"
RPROVIDES_grilo-devel = "grilo-dev (= 0.3.6)"
RDEPENDS_grilo-devel = "glib2-devel grilo libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grilo-0.3.6-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/grilo-devel-0.3.6-2.el8.x86_64.rpm \
          "

SRC_URI[grilo.sha256sum] = "8e923effe7491b0c2bf99c1b3622e5f2bb63a3f1ae4f83663573dcb304990f62"
SRC_URI[grilo-devel.sha256sum] = "1666b879961a53952ba1a3d733bd478313718d888e149ac7e038f7b276abf84c"
