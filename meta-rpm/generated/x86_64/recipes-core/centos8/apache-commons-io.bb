SUMMARY = "generated recipe based on apache-commons-io srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-io = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-io-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-commons-io-2.6-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-commons-io-javadoc-2.6-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-io.sha256sum] = "462846316db5ca27d891900061f1276ffaa58562d8e53a98652c016e9578259f"
SRC_URI[apache-commons-io-javadoc.sha256sum] = "10f001da659de212aa55ecedbebb8387028d5accf2b835ba5ba522341bfa5a32"
