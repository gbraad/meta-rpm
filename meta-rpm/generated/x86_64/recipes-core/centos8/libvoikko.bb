SUMMARY = "generated recipe based on libvoikko srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libvoikko = "libvoikko.so.1"
RPM_SONAME_REQ_libvoikko = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libvoikko = "glibc libgcc libstdc++ malaga-suomi-voikko"
RDEPENDS_python3-libvoikko = "libvoikko platform-python"
RPM_SONAME_REQ_voikko-tools = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libvoikko.so.1"
RDEPENDS_voikko-tools = "glibc libgcc libstdc++ libvoikko"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvoikko-4.1.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-libvoikko-4.1.1-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/voikko-tools-4.1.1-1.el8.x86_64.rpm \
          "

SRC_URI[libvoikko.sha256sum] = "4218a75e19357411f2f03e0c27d42db44724f6984e3c4936e0b3b79ebb7dcd40"
SRC_URI[python3-libvoikko.sha256sum] = "6348e0bbe222b7c466e76429c2a9b70587e348768e0bcf9652f9df104dd88755"
SRC_URI[voikko-tools.sha256sum] = "06307532caa04c4c8816b3f084d890761e0cc83e6f8c7943ea22b66eb21f5b5d"
