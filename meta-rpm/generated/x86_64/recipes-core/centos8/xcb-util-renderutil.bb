SUMMARY = "generated recipe based on xcb-util-renderutil srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxcb pkgconfig-native"
RPM_SONAME_PROV_xcb-util-renderutil = "libxcb-render-util.so.0"
RPM_SONAME_REQ_xcb-util-renderutil = "libc.so.6 libxcb-render.so.0 libxcb.so.1"
RDEPENDS_xcb-util-renderutil = "glibc libxcb"
RPM_SONAME_REQ_xcb-util-renderutil-devel = "libxcb-render-util.so.0"
RPROVIDES_xcb-util-renderutil-devel = "xcb-util-renderutil-dev (= 0.3.9)"
RDEPENDS_xcb-util-renderutil-devel = "libxcb-devel pkgconf-pkg-config xcb-util-renderutil"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xcb-util-renderutil-0.3.9-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xcb-util-renderutil-devel-0.3.9-10.el8.x86_64.rpm \
          "

SRC_URI[xcb-util-renderutil.sha256sum] = "4b3778c183925b0796921bb7333340052c6e33651553e54c851e4a3513fdfcb6"
SRC_URI[xcb-util-renderutil-devel.sha256sum] = "483bc58c1b956d2392df6a31743167735933feb494022d36f726b1b8fef27e6d"
