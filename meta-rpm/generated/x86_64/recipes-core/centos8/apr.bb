SUMMARY = "generated recipe based on apr srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & CLOSED & ISC & BSD"
RPM_LICENSE = "ASL 2.0 and BSD with advertising and ISC and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libuuid libxcrypt pkgconfig-native"
RPM_SONAME_PROV_apr = "libapr-1.so.0"
RPM_SONAME_REQ_apr = "libc.so.6 libcrypt.so.1 libdl.so.2 libpthread.so.0 libuuid.so.1"
RDEPENDS_apr = "glibc libuuid libxcrypt"
RPM_SONAME_REQ_apr-devel = "libapr-1.so.0"
RPROVIDES_apr-devel = "apr-dev (= 1.6.3)"
RDEPENDS_apr-devel = "apr bash pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-1.6.3-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/apr-devel-1.6.3-9.el8.x86_64.rpm \
          "

SRC_URI[apr.sha256sum] = "3992efe4a4ebf8429a257a631a71525b09210431c37f35f92c599f8f4af7066a"
SRC_URI[apr-devel.sha256sum] = "39d23d596f37792e6e2319022054c28b6cbf43dfdb0d53b9221f8ef709c2a683"
