SUMMARY = "generated recipe based on perl-Module-Install srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Install = "perl-Archive-Zip perl-CPAN perl-CPAN-Meta perl-Carp perl-Devel-PPPort perl-ExtUtils-MakeMaker perl-ExtUtils-Manifest perl-File-Path perl-File-Remove perl-File-Temp perl-Module-Build perl-Module-CoreList perl-Module-ScanDeps perl-PathTools perl-Socket perl-YAML-Tiny perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Module-Install-1.19-2.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Install.sha256sum] = "3de42fa4fd1810d280fff5e8fb807e20fa243f12f175ac47337d0fbc66392c07"
