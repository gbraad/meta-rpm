SUMMARY = "generated recipe based on pakchois srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_pakchois = "libpakchois.so.0"
RPM_SONAME_REQ_pakchois = "libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_pakchois = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pakchois-0.4-17.el8.x86_64.rpm \
          "

SRC_URI[pakchois.sha256sum] = "4447cf5a75144622db9b54fabfa70b67a775f854f34bd884a864dfce59e00e88"
