SUMMARY = "generated recipe based on maven-resources-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-resources-plugin = "apache-commons-io java-1.8.0-openjdk-headless javapackages-filesystem maven-filtering maven-lib plexus-interpolation plexus-utils"
RDEPENDS_maven-resources-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resources-plugin-3.1.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resources-plugin-javadoc-3.1.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-resources-plugin.sha256sum] = "082cabbbb95e3847b9fce25e892df6e5514831403bb8e53b2c5f09e774556a8f"
SRC_URI[maven-resources-plugin-javadoc.sha256sum] = "8e035b8ca8db936494c177e8ec0a112a3cda4783566c02b86919208ac93f6497"
