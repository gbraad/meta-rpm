SUMMARY = "generated recipe based on mythes-pt srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-pt = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mythes-pt-0.20060817-19.el8.noarch.rpm \
          "

SRC_URI[mythes-pt.sha256sum] = "9e45e914159a7c2134304d654ff026844d3d84d60dd870c5410ca6ef21b443ce"
