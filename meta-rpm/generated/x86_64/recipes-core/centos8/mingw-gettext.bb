SUMMARY = "generated recipe based on mingw-gettext srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-gettext = "mingw32-crt mingw32-filesystem mingw32-gcc mingw32-gcc-c++ mingw32-termcap mingw32-win-iconv"
RDEPENDS_mingw32-gettext-static = "mingw32-gettext"
RDEPENDS_mingw64-gettext = "mingw64-crt mingw64-filesystem mingw64-gcc-c++ mingw64-termcap mingw64-win-iconv"
RDEPENDS_mingw64-gettext-static = "mingw64-gettext"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-gettext-0.19.7-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-gettext-static-0.19.7-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-gettext-0.19.7-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-gettext-static-0.19.7-5.el8.noarch.rpm \
          "

SRC_URI[mingw32-gettext.sha256sum] = "89162715f7df70eac71b14615e05f8483da6a89bc12b24dda8ef47df65cbd524"
SRC_URI[mingw32-gettext-static.sha256sum] = "a8359e8d9044a8f0674fe6fd36f72ac9642ca2100417c2cbb86b68cb0f1d2ba2"
SRC_URI[mingw64-gettext.sha256sum] = "5ceac6e15c46ca133b1bf48a4a02ee1d5b411a5650c0e4ec201cd8764686b1fa"
SRC_URI[mingw64-gettext-static.sha256sum] = "f592d74cc7d7d2a616b115c9b0e0cea6d73fce978e20756e6b35c129d8a0b452"
