SUMMARY = "generated recipe based on fetchmail srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & CLOSED"
RPM_LICENSE = "GPL+ and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "e2fsprogs krb5-libs libxcrypt openssl pkgconfig-native"
RPM_SONAME_REQ_fetchmail = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libcrypto.so.1.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libresolv.so.2 libssl.so.1.1"
RDEPENDS_fetchmail = "glibc krb5-libs libcom_err libxcrypt openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fetchmail-6.3.26-19.el8.x86_64.rpm \
          "

SRC_URI[fetchmail.sha256sum] = "0458e69f7e6bc303d0cc502c39d792deabd58a923e1048c3e525b532afd9e6ee"
