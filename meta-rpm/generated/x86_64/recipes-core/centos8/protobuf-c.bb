SUMMARY = "generated recipe based on protobuf-c srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native protobuf"
RPM_SONAME_PROV_protobuf-c = "libprotobuf-c.so.1"
RPM_SONAME_REQ_protobuf-c = "libc.so.6"
RDEPENDS_protobuf-c = "glibc"
RPM_SONAME_REQ_protobuf-c-compiler = "libc.so.6 libgcc_s.so.1 libm.so.6 libprotobuf.so.15 libprotoc.so.15 libpthread.so.0 libstdc++.so.6"
RDEPENDS_protobuf-c-compiler = "glibc libgcc libstdc++ protobuf protobuf-c protobuf-compiler"
RPM_SONAME_REQ_protobuf-c-devel = "libprotobuf-c.so.1"
RPROVIDES_protobuf-c-devel = "protobuf-c-dev (= 1.3.0)"
RDEPENDS_protobuf-c-devel = "pkgconf-pkg-config protobuf-c protobuf-c-compiler"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/protobuf-c-1.3.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/protobuf-c-compiler-1.3.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/protobuf-c-devel-1.3.0-4.el8.x86_64.rpm \
          "

SRC_URI[protobuf-c.sha256sum] = "da899211ac163dce2aa7e6acbfe50b7b9a9a801c06c84eef216c221122f0ba54"
SRC_URI[protobuf-c-compiler.sha256sum] = "66f99dbe12854f12f129cf442a787419001cecf42982db5ea801ab2958a45768"
SRC_URI[protobuf-c-devel.sha256sum] = "46fe444c2a60bc3eef3812b1f48b3a5a01e6578909be2eb88bf5adba3f9b74f8"
