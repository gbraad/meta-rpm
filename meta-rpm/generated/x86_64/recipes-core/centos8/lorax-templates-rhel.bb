SUMMARY = "generated recipe based on lorax-templates-rhel srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lorax-templates-rhel-8.2-6.el8.noarch.rpm \
          "

SRC_URI[lorax-templates-rhel.sha256sum] = "8f574eb259175ae0a3704e0796cb37ab1cba813fae20f80f27b575e4f3593577"
