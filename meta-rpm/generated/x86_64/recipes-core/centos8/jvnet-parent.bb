SUMMARY = "generated recipe based on jvnet-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jvnet-parent = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jvnet-parent-4-10.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jvnet-parent.sha256sum] = "150269f355136f7184134ae87e52a2343ccde9f83fead6f17b90fc1e87792b1d"
