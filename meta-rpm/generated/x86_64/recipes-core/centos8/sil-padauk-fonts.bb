SUMMARY = "generated recipe based on sil-padauk-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_sil-padauk-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sil-padauk-book-fonts-3.003-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sil-padauk-fonts-3.003-1.el8.noarch.rpm \
          "

SRC_URI[sil-padauk-book-fonts.sha256sum] = "b84c5addf70020e61ae19485f10c3e9a1df2776f6bf372c42bbaf3d30860adf2"
SRC_URI[sil-padauk-fonts.sha256sum] = "fa28fbd0bcd8f6ad8c16d8bc441f0525a8c4b292fe71a3e8409d7fec7e74b35d"
