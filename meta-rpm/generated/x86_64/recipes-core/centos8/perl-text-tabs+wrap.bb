SUMMARY = "generated recipe based on perl-Text-Tabs+Wrap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "TTWL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Text-Tabs+Wrap = "perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Text-Tabs+Wrap-2013.0523-395.el8.noarch.rpm \
          "

SRC_URI[perl-Text-Tabs+Wrap.sha256sum] = "7e50a5d0f2fbd8c95375f72f5772c7731186e999a447121b8247f448b065a4ef"
