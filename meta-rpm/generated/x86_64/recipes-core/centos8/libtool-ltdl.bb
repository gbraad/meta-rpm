SUMMARY = "generated recipe based on libtool srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libtool-ltdl = "libltdl.so.7"
RPM_SONAME_REQ_libtool-ltdl = "libc.so.6 libdl.so.2"
RDEPENDS_libtool-ltdl = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtool-ltdl-2.4.6-25.el8.x86_64.rpm \
          "

SRC_URI[libtool-ltdl.sha256sum] = "7dcd11f03fa0979841bf0afe0a2ac8f360502d0a2dee8322a39115595c2464ec"
