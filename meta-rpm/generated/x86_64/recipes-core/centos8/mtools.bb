SUMMARY = "generated recipe based on mtools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mtools = "libc.so.6"
RDEPENDS_mtools = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mtools-4.0.18-14.el8.x86_64.rpm \
          "

SRC_URI[mtools.sha256sum] = "f726efa5063fdb4b0bff847b20087a3286f9c069ce62f75561a6d1adee0dad5a"
