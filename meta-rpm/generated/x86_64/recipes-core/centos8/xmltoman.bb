SUMMARY = "generated recipe based on xmltoman srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_xmltoman = "perl-XML-Parser perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xmltoman-0.4-17.el8.noarch.rpm \
          "

SRC_URI[xmltoman.sha256sum] = "94abfc3a42672e25d6b12083116f2c0559659b86bbe76921f64525ebf4b97559"
