SUMMARY = "generated recipe based on cairomm srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo freetype libgcc libpng libsigc++20 libx11 libxext libxrender pkgconfig-native zlib"
RPM_SONAME_PROV_cairomm = "libcairomm-1.0.so.1"
RPM_SONAME_REQ_cairomm = "libX11.so.6 libXext.so.6 libXrender.so.1 libc.so.6 libcairo.so.2 libfreetype.so.6 libgcc_s.so.1 libm.so.6 libpng16.so.16 libsigc-2.0.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_cairomm = "cairo freetype glibc libX11 libXext libXrender libgcc libpng libsigc++20 libstdc++ zlib"
RPM_SONAME_REQ_cairomm-devel = "libcairomm-1.0.so.1"
RPROVIDES_cairomm-devel = "cairomm-dev (= 1.12.0)"
RDEPENDS_cairomm-devel = "cairo-devel cairomm libsigc++20-devel pkgconf-pkg-config"
RDEPENDS_cairomm-doc = "cairomm libsigc++20-doc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cairomm-1.12.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cairomm-devel-1.12.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cairomm-doc-1.12.0-7.el8.noarch.rpm \
          "

SRC_URI[cairomm.sha256sum] = "bebf00672b5b18914789e6a47a3b317709c753c34b032a9e74b92ff40773ea2d"
SRC_URI[cairomm-devel.sha256sum] = "8d7c0acf1242674b3c72beca29e2fdc6e4c939bb7457581ebbc38a78e7d918b1"
SRC_URI[cairomm-doc.sha256sum] = "0f6eab26a8ca223190f1ea4ad0321807b37c9805f2f1aa2633323aa78d47a531"
