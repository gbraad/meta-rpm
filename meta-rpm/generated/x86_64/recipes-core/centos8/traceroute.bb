SUMMARY = "generated recipe based on traceroute srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_traceroute = "libc.so.6 libm.so.6"
RDEPENDS_traceroute = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/traceroute-2.1.0-6.el8.x86_64.rpm \
          "

SRC_URI[traceroute.sha256sum] = "0c8c13de9ca90dcdc2722ab0d72691afc04e6d5823ad57709035d57a15e3689b"
