SUMMARY = "generated recipe based on abrt srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo dbus-libs gdk-pixbuf glib-2.0 gtk+3 json-c libcap libnotify libreport libreport-gtk libselinux pango pkgconfig-native platform-python3 polkit rpm satyr systemd-libs"
RPM_SONAME_REQ_abrt = "libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 libreport.so.0 librpm.so.8 librpmio.so.8 libsatyr.so.3"
RDEPENDS_abrt = "abrt-libs bash dmidecode glib2 glibc json-c libreport libreport-plugin-rhtsupport libreport-plugin-ureport platform-python python3-abrt python3-augeas python3-dbus rpm-libs satyr shadow-utils systemd"
RPM_SONAME_REQ_abrt-addon-ccpp = "libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0"
RDEPENDS_abrt-addon-ccpp = "abrt abrt-addon-coredump-helper abrt-libs bash cpio elfutils gdb-headless glib2 glibc libreport platform-python python3-libreport rpm satyr systemd-libs"
RPM_SONAME_REQ_abrt-addon-coredump-helper = "libabrt.so.0 libc.so.6 libcap.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libselinux.so.1"
RDEPENDS_abrt-addon-coredump-helper = "abrt-libs bash glib2 glibc libcap libreport libselinux satyr"
RPM_SONAME_REQ_abrt-addon-kerneloops = "libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0"
RDEPENDS_abrt-addon-kerneloops = "abrt abrt-libs bash curl glib2 glibc libreport satyr systemd-libs"
RPM_SONAME_REQ_abrt-addon-pstoreoops = "libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-addon-pstoreoops = "abrt abrt-addon-kerneloops abrt-libs bash glib2 glibc libreport platform-python satyr"
RDEPENDS_abrt-addon-vmcore = "abrt abrt-addon-kerneloops bash kexec-tools platform-python python3-abrt python3-augeas util-linux"
RPM_SONAME_REQ_abrt-addon-xorg = "libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3 libsystemd.so.0"
RDEPENDS_abrt-addon-xorg = "abrt abrt-libs bash curl glib2 glibc libreport satyr systemd-libs"
RDEPENDS_abrt-cli = "abrt abrt-addon-ccpp abrt-addon-kerneloops abrt-addon-pstoreoops abrt-addon-vmcore abrt-addon-xorg abrt-tui libreport-plugin-rhtsupport libreport-rhel python3-abrt-addon"
RDEPENDS_abrt-cli-ng = "abrt abrt-addon-ccpp abrt-dbus abrt-libs libreport-cli platform-python python3-abrt python3-argcomplete python3-argh python3-humanize"
RDEPENDS_abrt-console-notification = "abrt abrt-cli"
RPM_SONAME_REQ_abrt-dbus = "libabrt.so.0 libabrt_dbus.so.0 libc.so.6 libdbus-1.so.3 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-dbus = "abrt abrt-libs bash dbus-libs glib2 glibc libreport polkit-libs satyr"
RDEPENDS_abrt-desktop = "abrt abrt-addon-ccpp abrt-addon-kerneloops abrt-addon-pstoreoops abrt-addon-vmcore abrt-addon-xorg abrt-gui gdb-headless gnome-abrt libreport-plugin-rhtsupport libreport-rhel python3-abrt-addon"
RPM_SONAME_REQ_abrt-gui = "libabrt.so.0 libabrt_dbus.so.0 libabrt_gui.so.0 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpolkit-gobject-1.so.0 libreport-gtk.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-gui = "abrt abrt-dbus abrt-gui-libs abrt-libs atk bash cairo cairo-gobject gdk-pixbuf2 glib2 glibc gnome-abrt gsettings-desktop-schemas gtk3 libnotify libreport libreport-gtk pango polkit-libs satyr"
RPM_SONAME_PROV_abrt-gui-libs = "libabrt_gui.so.0"
RPM_SONAME_REQ_abrt-gui-libs = "libabrt.so.0 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-gui-libs = "abrt-libs atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libreport pango satyr"
RPM_SONAME_PROV_abrt-libs = "libabrt.so.0"
RPM_SONAME_REQ_abrt-libs = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-libs = "glib2 glibc libreport satyr"
RDEPENDS_abrt-plugin-machine-id = "abrt platform-python"
RDEPENDS_abrt-plugin-sosreport = "abrt sos"
RPM_SONAME_REQ_abrt-tui = "libabrt.so.0 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libreport.so.0 libsatyr.so.3"
RDEPENDS_abrt-tui = "abrt abrt-dbus abrt-libs glib2 glibc libreport libreport-cli polkit-libs satyr"
RPM_SONAME_REQ_python3-abrt = "libabrt.so.0 libc.so.6 libdl.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libreport.so.0 libsatyr.so.3 libutil.so.1"
RDEPENDS_python3-abrt = "abrt abrt-dbus abrt-libs glib2 glibc libreport platform-python python3-dbus python3-gobject-base python3-libreport python3-libs satyr"
RDEPENDS_python3-abrt-addon = "abrt bash platform-python python3-abrt python3-systemd"
RDEPENDS_python3-abrt-container-addon = "container-exception-logger platform-python"
RDEPENDS_python3-abrt-doc = "abrt platform-python python3-abrt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-addon-ccpp-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-addon-coredump-helper-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-addon-kerneloops-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-addon-pstoreoops-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-addon-vmcore-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-addon-xorg-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-cli-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-cli-ng-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-console-notification-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-dbus-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-desktop-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-gui-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-gui-libs-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-libs-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-plugin-machine-id-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-plugin-sosreport-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/abrt-tui-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-abrt-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-abrt-addon-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-abrt-container-addon-2.10.9-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-abrt-doc-2.10.9-11.el8.noarch.rpm \
          "

SRC_URI[abrt.sha256sum] = "9a138e3e7063e4412d3787c3139a4c133b43fa2a35a12fa8cb01c416e7fa730d"
SRC_URI[abrt-addon-ccpp.sha256sum] = "3a0d4d1de5eecc765561950465f4c8d9daec48fbef05ca4d7d22364251eb0741"
SRC_URI[abrt-addon-coredump-helper.sha256sum] = "4169675cb6a6ba5438bf251e490b34daa32158227bd300438667ff08982431e5"
SRC_URI[abrt-addon-kerneloops.sha256sum] = "0404b15082cc5bca54cacc760cf453da7fa6d34c8b8777f1f353726ac21dab18"
SRC_URI[abrt-addon-pstoreoops.sha256sum] = "0cc3d2a786fd51ebf7521b03c76c5be7f1795798691c4822351e47a35782b737"
SRC_URI[abrt-addon-vmcore.sha256sum] = "c7e647e7e8d65854509ce7d1be129fd9897863943a1f19d8f414802a31ae2a7d"
SRC_URI[abrt-addon-xorg.sha256sum] = "e82e3daa525b603a623747850120d31fa141ca66febea4e6ca0e6433877db94d"
SRC_URI[abrt-cli.sha256sum] = "bafea6e3b97913a1270aa0a3034a6ec6d5e230464f2a89fdea47796c7227d2a4"
SRC_URI[abrt-cli-ng.sha256sum] = "6700de88603d4636be1fc1a053ded51d9373bc65d4b46d6ccc0b365d787b9c05"
SRC_URI[abrt-console-notification.sha256sum] = "dfbf63a959d0a1750f9f4857826b1bf65a4bcee5b14f57220ac358a41457551a"
SRC_URI[abrt-dbus.sha256sum] = "271e67b7461b85b993c43839d2662586d06c0938df214400b11ab48cb9c0237e"
SRC_URI[abrt-desktop.sha256sum] = "840c1d2ce7c91785619c82b0adfd48bab5036004714c85ff81bbaa5f74eec663"
SRC_URI[abrt-gui.sha256sum] = "675b0ce828806292d3e7d867874260d4e71c4465b16c01db6de62f283135f475"
SRC_URI[abrt-gui-libs.sha256sum] = "075926124bb383592d1798bb86cc80c06d200c50e1f9ae295f42a12b313042c3"
SRC_URI[abrt-libs.sha256sum] = "71973726df29886e9b7ac3c69294b7e0baa84e7542b07f807b6e32c9d2bb1f0b"
SRC_URI[abrt-plugin-machine-id.sha256sum] = "d5af4f4fa8e76d72e92ac69f175e93d93f9834da76696c897fa2815c316e2fa0"
SRC_URI[abrt-plugin-sosreport.sha256sum] = "3b1b91c10f8f5c3875249bc86789c74cdcebf5e5139541080ec9dd6ce48589a2"
SRC_URI[abrt-tui.sha256sum] = "cf2b088158a76cb139a1829b424611dabda68f6cdd3ae7aa4f4dd0bccacbb5e0"
SRC_URI[python3-abrt.sha256sum] = "c889faecacdb66fc1b580899d7b97571568a761c3bfbc428639a007bf1819101"
SRC_URI[python3-abrt-addon.sha256sum] = "cdcde40550890d780244d1a12bccb12e66a8f84385d5b0321885df4262d26407"
SRC_URI[python3-abrt-container-addon.sha256sum] = "6bde68aeeb1966a3ef3af3428171bcebdd47f22f4d5ca73e04c67f2b889e6c11"
SRC_URI[python3-abrt-doc.sha256sum] = "5cb0ad44b193b226ba5b63f648127f5070478db1d30cb9cbc415930bd2283376"
