SUMMARY = "generated recipe based on perl-Unicode-EastAsianWidth srpm"
DESCRIPTION = "Description"
LICENSE = "CC0-1.0"
RPM_LICENSE = "CC0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Unicode-EastAsianWidth = "perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Unicode-EastAsianWidth-1.33-13.el8.noarch.rpm \
          "

SRC_URI[perl-Unicode-EastAsianWidth.sha256sum] = "ccd00c3f5a58084f75e461741350777158ca7e0cc6bd7f387cfe538fc9c7f5ea"
