SUMMARY = "generated recipe based on libuser srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs glib-2.0 libselinux libxcrypt openldap pam pkgconfig-native popt"
RPM_SONAME_PROV_libuser = "libuser.so.1 libuser_files.so libuser_ldap.so libuser_shadow.so"
RPM_SONAME_REQ_libuser = "libaudit.so.1 libc.so.6 libcrypt.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libldap-2.4.so.2 libpam.so.0 libpam_misc.so.0 libpopt.so.0 libpthread.so.0 libselinux.so.1"
RDEPENDS_libuser = "audit-libs glib2 glibc libselinux libxcrypt openldap pam popt"
RPM_SONAME_REQ_python3-libuser = "libaudit.so.1 libc.so.6 libcrypt.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libselinux.so.1 libuser.so.1"
RDEPENDS_python3-libuser = "audit-libs glib2 glibc libselinux libuser libxcrypt platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libuser-0.62-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libuser-0.62-23.el8.x86_64.rpm \
          "

SRC_URI[libuser.sha256sum] = "70b5e4e580da72969ad3bb144c15293910b7d679e195c118cee60d8320f01851"
SRC_URI[python3-libuser.sha256sum] = "a6d8806f3aed48e42dfb067fa0756ca9cb4600dd597552d8e3c6511e511b605a"
