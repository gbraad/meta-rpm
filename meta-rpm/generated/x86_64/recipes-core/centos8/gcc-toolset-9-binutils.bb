SUMMARY = "generated recipe based on gcc-toolset-9-binutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_gcc-toolset-9-binutils = "libbfd-2.32-17.el8_1.so libopcodes-2.32-17.el8_1.so"
RPM_SONAME_REQ_gcc-toolset-9-binutils = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-binutils = "bash chkconfig coreutils gcc-toolset-9-runtime glibc info libgcc libstdc++"
RPROVIDES_gcc-toolset-9-binutils-devel = "gcc-toolset-9-binutils-dev (= 2.32)"
RDEPENDS_gcc-toolset-9-binutils-devel = "coreutils gcc-toolset-9-binutils gcc-toolset-9-runtime zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-binutils-2.32-17.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-binutils-devel-2.32-17.el8_1.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-binutils.sha256sum] = "32dc97bbd2188680b97b416edc21daf808fd6a75b1dc4885482f3f6fd96e1993"
SRC_URI[gcc-toolset-9-binutils-devel.sha256sum] = "0b09b2aef2a2929a284d63fc7f4782324cd0093cac22c382432221cc9ca1885c"
