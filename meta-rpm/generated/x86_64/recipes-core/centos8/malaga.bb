SUMMARY = "generated recipe based on malaga srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 ncurses pango pkgconfig-native readline"
RPM_SONAME_PROV_libmalaga = "libmalaga.so.7"
RPM_SONAME_REQ_libmalaga = "libc.so.6 libglib-2.0.so.0 libm.so.6"
RDEPENDS_libmalaga = "glib2 glibc"
RPM_SONAME_REQ_malaga = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libmalaga.so.7 libncurses.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libreadline.so.7 libtinfo.so.6"
RDEPENDS_malaga = "atk bash cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 info libmalaga ncurses-libs pango readline"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmalaga-7.12-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/malaga-7.12-23.el8.x86_64.rpm \
          "

SRC_URI[libmalaga.sha256sum] = "543de24f9875f453aa6b04de66515a9c2cd622b37faa4ed2909d44bb651da9b4"
SRC_URI[malaga.sha256sum] = "f3c42c3650dc05d4006205bd504a628400b58f1f33d6aa27ac30f13c5b2f3d23"
