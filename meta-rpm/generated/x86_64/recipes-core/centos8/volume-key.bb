SUMMARY = "generated recipe based on volume_key srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & (MPL-1.1 | GPL-2.0 | LGPL-2.0)"
RPM_LICENSE = "GPLv2 and (MPLv1.1 or GPLv2 or LGPLv2)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cryptsetup-libs glib-2.0 gpgme libblkid nspr nss pkgconfig-native"
RPM_SONAME_REQ_volume_key = "libblkid.so.1 libc.so.6 libcryptsetup.so.12 libdl.so.2 libglib-2.0.so.0 libgpgme.so.11 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libvolume_key.so.1"
RDEPENDS_volume_key = "cryptsetup-libs glib2 glibc gpgme libblkid nspr nss nss-util volume_key-libs"
RPM_SONAME_REQ_volume_key-devel = "libvolume_key.so.1"
RPROVIDES_volume_key-devel = "volume_key-dev (= 0.3.11)"
RDEPENDS_volume_key-devel = "volume_key-libs"
RPM_SONAME_PROV_volume_key-libs = "libvolume_key.so.1"
RPM_SONAME_REQ_volume_key-libs = "libblkid.so.1 libc.so.6 libcryptsetup.so.12 libdl.so.2 libglib-2.0.so.0 libgpgme.so.11 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so"
RDEPENDS_volume_key-libs = "cryptsetup-libs glib2 glibc gnupg2 gpgme libblkid nspr nss nss-util"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/volume_key-0.3.11-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/volume_key-devel-0.3.11-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/volume_key-libs-0.3.11-5.el8.x86_64.rpm \
          "

SRC_URI[volume_key.sha256sum] = "3f8c5c72aa934832be3d8456d35cb71c8f324ad3ab63e24ed2b33bfb93370005"
SRC_URI[volume_key-devel.sha256sum] = "e10fbfa718211c2acc921861d391cf23f8e8151fe23606a3ccc868e3e77332e1"
SRC_URI[volume_key-libs.sha256sum] = "5c0cf0adf7a3ad24ddde58f298ebf4ce741bccdfc8eff0103644115c837f80d6"
