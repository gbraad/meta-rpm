SUMMARY = "generated recipe based on numpy srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & Python-2.0"
RPM_LICENSE = "BSD and Python"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openblas pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-numpy = "ld-linux-x86-64.so.2 libc.so.6 libm.so.6 libopenblas.so.0 libopenblasp.so.0 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-numpy = "glibc openblas openblas-threads platform-python python3-libs"
RDEPENDS_python3-numpy-f2py = "bash platform-python platform-python-devel python3-numpy python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-numpy-1.14.3-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-numpy-f2py-1.14.3-9.el8.x86_64.rpm \
          "

SRC_URI[python3-numpy.sha256sum] = "c6dc2cf6f1f4fc15c079a14818999907e3ea4856cde6d70df6fbc9aba575a40e"
SRC_URI[python3-numpy-f2py.sha256sum] = "9cc9d46a58d436f86f114481768d67a2a107b8ff478d401250b36213dc49bf26"
