SUMMARY = "generated recipe based on hyphen srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-2.0 | MPL-1.1"
RPM_LICENSE = "GPLv2 or LGPLv2+ or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_hyphen = "libhyphen.so.0"
RPM_SONAME_REQ_hyphen = "libc.so.6"
RDEPENDS_hyphen = "glibc"
RPM_SONAME_REQ_hyphen-devel = "libhyphen.so.0"
RPROVIDES_hyphen-devel = "hyphen-dev (= 2.8.8)"
RDEPENDS_hyphen-devel = "hyphen perl-interpreter"
RDEPENDS_hyphen-en = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-2.8.8-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-en-2.8.8-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/hyphen-devel-2.8.8-9.el8.x86_64.rpm \
          "

SRC_URI[hyphen.sha256sum] = "a263c425da99a445e874b42cd9d2f2661ce6b477d9c4d4251db8418dd8e5dd5c"
SRC_URI[hyphen-devel.sha256sum] = "6d52f941d9f9616b3b318ef6baf903c80c0e98d5f6ac841116951d7bea227b0e"
SRC_URI[hyphen-en.sha256sum] = "a00d285ed333b46d8e1fd44498c7ca079d5fc1e1e31b709a405bee28d424afe3"
