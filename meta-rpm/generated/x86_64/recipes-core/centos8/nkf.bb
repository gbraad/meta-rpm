SUMMARY = "generated recipe based on nkf srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_nkf = "libc.so.6"
RDEPENDS_nkf = "glibc"
RPM_SONAME_REQ_perl-NKF = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-NKF = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/nkf-2.1.4-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-NKF-2.1.4-8.el8.x86_64.rpm \
          "

SRC_URI[nkf.sha256sum] = "760e26275705c0958f0d89a297f33909e7fde39951fd828b775cd3638529941b"
SRC_URI[perl-NKF.sha256sum] = "26b15060fcd0cba696be42e1be34684f6c5dcd20da4d080deec54702c63b2fa7"
