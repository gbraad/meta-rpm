SUMMARY = "generated recipe based on perl-IO-String srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-String = "perl-Data-Dumper perl-IO perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-IO-String-1.08-31.el8.noarch.rpm \
          "

SRC_URI[perl-IO-String.sha256sum] = "97326e2416d5ccaa7e9cd36436c21619a23bad8de089f08a429a4e7fa4e01c2d"
