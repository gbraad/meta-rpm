SUMMARY = "generated recipe based on augeas srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libselinux libxml2 pkgconfig-native readline"
RPM_SONAME_REQ_augeas = "libaugeas.so.0 libc.so.6 libfa.so.1 libgcc_s.so.1 libreadline.so.7 libselinux.so.1 libxml2.so.2"
RDEPENDS_augeas = "augeas-libs glibc libgcc libselinux libxml2 readline"
RPM_SONAME_REQ_augeas-devel = "libaugeas.so.0 libfa.so.1"
RPROVIDES_augeas-devel = "augeas-dev (= 1.12.0)"
RDEPENDS_augeas-devel = "augeas-libs libselinux-devel libxml2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_augeas-libs = "libaugeas.so.0 libfa.so.1"
RPM_SONAME_REQ_augeas-libs = "libc.so.6 libselinux.so.1 libxml2.so.2"
RDEPENDS_augeas-libs = "glibc libselinux libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/augeas-1.12.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/augeas-libs-1.12.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/augeas-devel-1.12.0-5.el8.x86_64.rpm \
          "

SRC_URI[augeas.sha256sum] = "acaa073d975f1cb2754f066c9efb7d42b516011f1b06ddc71820f3b655133471"
SRC_URI[augeas-devel.sha256sum] = "5b25cff56656057aa035b3d5f7f0222e92d8ece87d4523dbe60e77e19fd03700"
SRC_URI[augeas-libs.sha256sum] = "fbef4adf76aaab78b737422c8fc5fa4b78949dbd66c326df23082aa89bc34675"
