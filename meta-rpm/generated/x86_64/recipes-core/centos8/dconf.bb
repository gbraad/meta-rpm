SUMMARY = "generated recipe based on dconf srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0 & GPL-3.0"
RPM_LICENSE = "LGPLv2+ and GPLv2+ and GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_dconf = "libdconf.so.1 libdconfsettings.so"
RPM_SONAME_REQ_dconf = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_dconf = "bash dbus glib2 glibc"
RPM_SONAME_REQ_dconf-devel = "libdconf.so.1"
RPROVIDES_dconf-devel = "dconf-dev (= 0.28.0)"
RDEPENDS_dconf-devel = "dconf glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dconf-0.28.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dconf-devel-0.28.0-3.el8.x86_64.rpm \
          "

SRC_URI[dconf.sha256sum] = "7a7f81d608673b64987e162663672189f2960f0f2e47356a3c2b4ba099f78115"
SRC_URI[dconf-devel.sha256sum] = "0c4e18ccda22bb4f2271c5bb518be3188db5673016bd5deebfaea16612909cd6"
