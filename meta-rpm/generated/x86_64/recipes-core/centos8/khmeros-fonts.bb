SUMMARY = "generated recipe based on khmeros-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_khmeros-base-fonts = "khmeros-fonts-common"
RDEPENDS_khmeros-battambang-fonts = "khmeros-fonts-common"
RDEPENDS_khmeros-bokor-fonts = "khmeros-fonts-common"
RDEPENDS_khmeros-fonts-common = "fontpackages-filesystem"
RDEPENDS_khmeros-handwritten-fonts = "khmeros-fonts-common"
RDEPENDS_khmeros-metal-chrieng-fonts = "khmeros-fonts-common"
RDEPENDS_khmeros-muol-fonts = "khmeros-fonts-common"
RDEPENDS_khmeros-siemreap-fonts = "khmeros-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/khmeros-base-fonts-5.0-25.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/khmeros-battambang-fonts-5.0-25.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/khmeros-bokor-fonts-5.0-25.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/khmeros-fonts-common-5.0-25.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/khmeros-handwritten-fonts-5.0-25.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/khmeros-metal-chrieng-fonts-5.0-25.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/khmeros-muol-fonts-5.0-25.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/khmeros-siemreap-fonts-5.0-25.el8.noarch.rpm \
          "

SRC_URI[khmeros-base-fonts.sha256sum] = "4a1ad228f71a2b3b2dee5c244aa4c50cd5700475a95bd6b432c89a237126b46d"
SRC_URI[khmeros-battambang-fonts.sha256sum] = "2c0fdfb9365e767721d615228646ec2ef800a03300e6104017c5137453b81f5c"
SRC_URI[khmeros-bokor-fonts.sha256sum] = "bb26db649aa2b9be0370289a20c6971b30daeffdfc3ae54e036d160079342e18"
SRC_URI[khmeros-fonts-common.sha256sum] = "daef36570013d7a828ada308aee74e010ac3d8c5d7e7a4e6a85d57a13d14f3a6"
SRC_URI[khmeros-handwritten-fonts.sha256sum] = "45c41630825122e8c0d4b003ada4c970749c92483c1d59ead48589a1cae1174f"
SRC_URI[khmeros-metal-chrieng-fonts.sha256sum] = "282ef869505fa22ebe9f7b5676826eeef73391ced57177cba4bca95a791e1371"
SRC_URI[khmeros-muol-fonts.sha256sum] = "e06b9975f42aa84fb50c296d85ade3582a4dc2c3742b03cb3d888a2a75a3c614"
SRC_URI[khmeros-siemreap-fonts.sha256sum] = "df4562add561abb34da174b3effe721b403c12a154037117ba040fcbca290dda"
