SUMMARY = "generated recipe based on perl-Test-Warnings srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Warnings = "perl-Carp perl-Exporter perl-Test-Simple perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Warnings-0.026-7.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Warnings.sha256sum] = "504c85177338c9f3f62e975e347b21f8dff488920cae01c7e70fdb559320416c"
