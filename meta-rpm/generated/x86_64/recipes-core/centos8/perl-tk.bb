SUMMARY = "generated recipe based on perl-Tk srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & SWL"
RPM_LICENSE = "(GPL+ or Artistic) and SWL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "fontconfig libjpeg-turbo libpng libx11 libxft perl pkgconfig-native zlib"
RPM_SONAME_REQ_perl-Tk = "libX11.so.6 libXft.so.2 libc.so.6 libfontconfig.so.1 libjpeg.so.62 libm.so.6 libperl.so.5.26 libpng16.so.16 libpthread.so.0 libz.so.1"
RDEPENDS_perl-Tk = "fontconfig glibc libX11 libXft libjpeg-turbo libpng perl-Carp perl-Encode perl-Exporter perl-Getopt-Long perl-IO perl-PathTools perl-Socket perl-Text-Tabs+Wrap perl-interpreter perl-libs perl-open zlib"
RPROVIDES_perl-Tk-devel = "perl-Tk-dev (= 804.034)"
RDEPENDS_perl-Tk-devel = "perl-Carp perl-Exporter perl-ExtUtils-MakeMaker perl-PathTools perl-Tk perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Tk-804.034-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Tk-devel-804.034-2.el8.x86_64.rpm \
          "

SRC_URI[perl-Tk.sha256sum] = "3f9467cd1857e75b6bd51d35184c9e76f8ef49fc063312e5afbfe62bce2c22bf"
SRC_URI[perl-Tk-devel.sha256sum] = "c4d4b02ed38305af728884274513865fd9445a1225495bb3d4f01c793400f726"
