SUMMARY = "generated recipe based on hunspell-qu srpm"
DESCRIPTION = "Description"
LICENSE = "AGPL-3.0"
RPM_LICENSE = "AGPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-qu = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-qu-0.9-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-qu.sha256sum] = "3e05ad8914e1af345cb3b958d9522e8b5f24fe6b9b15ab22bda3b70203f6510a"
