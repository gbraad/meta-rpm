SUMMARY = "generated recipe based on sound-theme-freedesktop srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0 & CC-BY-SA-1.0 & CC-BY-1.0 "
RPM_LICENSE = "GPLv2+ and LGPLv2+ and CC-BY-SA and CC-BY"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_sound-theme-freedesktop = "bash coreutils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sound-theme-freedesktop-0.8-9.el8.noarch.rpm \
          "

SRC_URI[sound-theme-freedesktop.sha256sum] = "8432cb92b20c99b7538cc95260c87ef32a0c3f8a95b2623368917fabaf61249b"
