SUMMARY = "generated recipe based on gtkspell3 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo enchant gdk-pixbuf glib-2.0 gtk+3 pango pkgconfig-native"
RPM_SONAME_PROV_gtkspell3 = "libgtkspell3-3.so.0"
RPM_SONAME_REQ_gtkspell3 = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libenchant.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_gtkspell3 = "atk cairo cairo-gobject enchant gdk-pixbuf2 glib2 glibc gtk3 iso-codes pango"
RPM_SONAME_REQ_gtkspell3-devel = "libgtkspell3-3.so.0"
RPROVIDES_gtkspell3-devel = "gtkspell3-dev (= 3.0.9)"
RDEPENDS_gtkspell3-devel = "enchant-devel glib2-devel gtk3-devel gtkspell3 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gtkspell3-3.0.9-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gtkspell3-devel-3.0.9-5.el8.x86_64.rpm \
          "

SRC_URI[gtkspell3.sha256sum] = "25e192eb0605316fb6b0aed9a9aa9baaee63e451ffd30009a9815e5240617ec1"
SRC_URI[gtkspell3-devel.sha256sum] = "57106c001f45ff1dc285e026eed4a84562ddd1c37799de5d80b5c00f8175a6b1"
