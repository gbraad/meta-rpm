SUMMARY = "generated recipe based on mingw-fontconfig srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-fontconfig = "mingw32-crt mingw32-expat mingw32-filesystem mingw32-freetype mingw32-gcc mingw32-pkg-config pkgconf-pkg-config"
RDEPENDS_mingw64-fontconfig = "mingw64-crt mingw64-expat mingw64-filesystem mingw64-freetype mingw64-gcc mingw64-pkg-config pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-fontconfig-2.12.6-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-fontconfig-2.12.6-3.el8.noarch.rpm \
          "

SRC_URI[mingw32-fontconfig.sha256sum] = "05495e16a0bc52d0d4cbfa0c209d31448a0d94ff768aa0cf4ba8c4c2c1601410"
SRC_URI[mingw64-fontconfig.sha256sum] = "a228dd951819964bfbc7bf1513e4ea2d505836dfec16a74114a4e1eb9f12fdeb"
