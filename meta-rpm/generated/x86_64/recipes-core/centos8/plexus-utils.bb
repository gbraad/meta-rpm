SUMMARY = "generated recipe based on plexus-utils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & CLOSED & xpp & BSD & CLOSED"
RPM_LICENSE = "ASL 1.1 and ASL 2.0 and xpp and BSD and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-utils = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_plexus-utils-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-utils-3.1.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-utils-javadoc-3.1.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-utils.sha256sum] = "4626d691e3b01d2ae2bb8ffb2d7ac324a821f90e36bd7f199930bfde0284bf22"
SRC_URI[plexus-utils-javadoc.sha256sum] = "02f9755d764022d96114d6fae665c5244fda6f83c57e824494eb2fbe0a832119"
