SUMMARY = "generated recipe based on imake srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_imake = "libc.so.6"
RDEPENDS_imake = "bash glibc perl-interpreter"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/imake-1.0.7-11.el8.x86_64.rpm \
          "

SRC_URI[imake.sha256sum] = "014af6759418ffcd9e3571b5112359c452f06df5118a7fd87ce7a62b31be14ab"
