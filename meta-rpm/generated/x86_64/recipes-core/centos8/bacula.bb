SUMMARY = "generated recipe based on bacula srpm"
DESCRIPTION = "Description"
LICENSE = "AGPL-3.0"
RPM_LICENSE = "AGPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl libcap libgcc libpq libxcrypt lzo mariadb-connector-c ncurses openssl pkgconfig-native readline sqlite3 zlib"
RPM_SONAME_REQ_bacula-client = "libacl.so.1 libbac-9.0.6.so libbaccfg-9.0.6.so libbacfind-9.0.6.so libc.so.6 libcap.so.2 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 liblzo2.so.2 libm.so.6 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_bacula-client = "bacula-common bacula-libs bash glibc libacl libcap libgcc libstdc++ lzo openssl-libs systemd zlib"
RDEPENDS_bacula-common = "bacula-libs bash shadow-utils"
RPM_SONAME_REQ_bacula-console = "libbac-9.0.6.so libbaccfg-9.0.6.so libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libreadline.so.7 libssl.so.1.1 libstdc++.so.6 libtinfo.so.6"
RDEPENDS_bacula-console = "bacula-libs glibc libgcc libstdc++ ncurses-libs openssl-libs readline"
RPM_SONAME_REQ_bacula-director = "libbac-9.0.6.so libbaccats-9.0.6.so libbaccfg-9.0.6.so libbacfind-9.0.6.so libbacsql-9.0.6.so libc.so.6 libcap.so.2 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_bacula-director = "bacula-common bacula-libs bacula-libs-sql bash glibc libcap libgcc libstdc++ openssl-libs perl-interpreter perl-libs systemd zlib"
RPM_SONAME_PROV_bacula-libs = "libbac-9.0.6.so libbaccfg-9.0.6.so libbacfind-9.0.6.so libbacsd-9.0.6.so"
RPM_SONAME_REQ_bacula-libs = "libc.so.6 libcap.so.2 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_bacula-libs = "glibc libcap libgcc libstdc++ openssl-libs zlib"
RPM_SONAME_PROV_bacula-libs-sql = "libbaccats-9.0.6.so libbacsql-9.0.6.so"
RPM_SONAME_REQ_bacula-libs-sql = "libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libmariadb.so.3 libpq.so.5 libpthread.so.0 libsqlite3.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_bacula-libs-sql = "bash glibc libgcc libpq libstdc++ libxcrypt mariadb-connector-c openssl-libs sqlite-libs zlib"
RDEPENDS_bacula-logwatch = "bacula-director logwatch perl-interpreter perl-libs"
RPM_SONAME_REQ_bacula-storage = "libbac-9.0.6.so libbaccats-9.0.6.so libbaccfg-9.0.6.so libbacfind-9.0.6.so libbacsd-9.0.6.so libbacsql-9.0.6.so libc.so.6 libcap.so.2 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 liblzo2.so.2 libm.so.6 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_bacula-storage = "bacula-common bacula-libs bacula-libs-sql bash glibc libcap libgcc libstdc++ lzo mt-st mtx openssl-libs systemd zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bacula-client-9.0.6-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bacula-common-9.0.6-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bacula-console-9.0.6-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bacula-director-9.0.6-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bacula-libs-9.0.6-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bacula-libs-sql-9.0.6-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bacula-logwatch-9.0.6-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bacula-storage-9.0.6-6.el8.x86_64.rpm \
          "

SRC_URI[bacula-client.sha256sum] = "5819bdba525d2ef93fa40a865cb30c130e4d646f1c901edbfa19d13e1257aaab"
SRC_URI[bacula-common.sha256sum] = "f4f250dc26003c4a4e04ba0d743d7f63437a550acfd4eb1b3ce94d1d80c03322"
SRC_URI[bacula-console.sha256sum] = "dddae9eb2fc5585443bd1c77d46a371c90834b1c9d73e70d03f468957dd6b524"
SRC_URI[bacula-director.sha256sum] = "d9b0953322b07f7fafab45e134d2d3b2b59741e194f19106d52aef1152b801b7"
SRC_URI[bacula-libs.sha256sum] = "92cc232871e46dcdbdec2d98d50a65dc6f8e1140156441e862b0109e3a24ee98"
SRC_URI[bacula-libs-sql.sha256sum] = "bfd9a6573f7945d45e2b0da0c612ba439e2746ef191f35706ade42b8b22b47ff"
SRC_URI[bacula-logwatch.sha256sum] = "a1580e52ea83a7075b87eb95fa62330a54a356ffc42cc5ea19a894a8179a49bb"
SRC_URI[bacula-storage.sha256sum] = "950ebac0ebaa46306532435e5f53c5675ccd930278d3b6132b9be403a2b711dc"
