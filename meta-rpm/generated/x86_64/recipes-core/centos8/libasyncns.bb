SUMMARY = "generated recipe based on libasyncns srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libasyncns = "libasyncns.so.0"
RPM_SONAME_REQ_libasyncns = "libc.so.6 libpthread.so.0 libresolv.so.2"
RDEPENDS_libasyncns = "glibc"
RPM_SONAME_REQ_libasyncns-devel = "libasyncns.so.0"
RPROVIDES_libasyncns-devel = "libasyncns-dev (= 0.8)"
RDEPENDS_libasyncns-devel = "libasyncns pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libasyncns-0.8-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libasyncns-devel-0.8-14.el8.x86_64.rpm \
          "

SRC_URI[libasyncns.sha256sum] = "bf545f886dbb4980e392d94a3e42c5ce0bfa5e4a29356ae1b7fcf825deab25cf"
SRC_URI[libasyncns-devel.sha256sum] = "21d5967a663490ecfa533cda3d9f46c06847cc3b83a4be9f4287c068ec94b36b"
