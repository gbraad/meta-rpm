SUMMARY = "generated recipe based on pyOpenSSL srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pyOpenSSL = "platform-python python3-cryptography"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pyOpenSSL-18.0.0-1.el8.noarch.rpm \
          "

SRC_URI[python3-pyOpenSSL.sha256sum] = "15d72644ce92ad261c4c6a97ebdd863bba4c5aa14fa50444873294cdbb612fff"
