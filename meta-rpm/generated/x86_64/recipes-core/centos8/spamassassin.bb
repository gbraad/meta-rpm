SUMMARY = "generated recipe based on spamassassin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native zlib"
RPM_SONAME_REQ_spamassassin = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libssl.so.1.1 libz.so.1"
RDEPENDS_spamassassin = "bash diffutils glibc gnupg2 openssl-libs perl-Archive-Tar perl-Carp perl-DB_File perl-Data-Dumper perl-Digest-MD5 perl-Digest-SHA perl-Encode-Detect perl-Errno perl-Exporter perl-File-Path perl-Getopt-Long perl-HTML-Parser perl-HTTP-Date perl-IO perl-IO-Socket-INET6 perl-IO-Socket-SSL perl-IO-Zlib perl-MIME-Base64 perl-Mail-DKIM perl-Mail-SPF perl-Net-DNS perl-NetAddr-IP perl-PathTools perl-Pod-Usage perl-Scalar-List-Utils perl-Socket perl-Sys-Syslog perl-Time-HiRes perl-Time-Local perl-constant perl-interpreter perl-libs perl-libwww-perl perl-version procmail systemd zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spamassassin-3.4.2-7.el8.x86_64.rpm \
          "

SRC_URI[spamassassin.sha256sum] = "0a07f74c0605ff169cf03dbf1390d40166d4a98eabf508a41620209bea930260"
