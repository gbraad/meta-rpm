SUMMARY = "generated recipe based on libXmu srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "xmu"
DEPENDS = "libx11 libxext libxt pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXmu = "libXmu.so.6 libXmuu.so.1"
RPM_SONAME_REQ_libXmu = "libX11.so.6 libXext.so.6 libXt.so.6 libc.so.6"
RDEPENDS_libXmu = "glibc libX11 libXext libXt"
RPM_SONAME_REQ_libXmu-devel = "libXmu.so.6 libXmuu.so.1"
RPROVIDES_libXmu-devel = "libXmu-dev (= 1.1.2)"
RDEPENDS_libXmu-devel = "libX11-devel libXext-devel libXmu libXt-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXmu-1.1.2-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXmu-devel-1.1.2-12.el8.x86_64.rpm \
          "

SRC_URI[libXmu.sha256sum] = "68cf1ec570bf8d877c577a801d162dc73f4662b066b0f7d07802793c422c3339"
SRC_URI[libXmu-devel.sha256sum] = "1b5ec6dfc283237e83aace1b943166a88e1417b543614add141925466e371b11"
