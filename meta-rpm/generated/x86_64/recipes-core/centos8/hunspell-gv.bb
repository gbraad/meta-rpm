SUMMARY = "generated recipe based on hunspell-gv srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-gv = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-gv-0.20040505-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-gv.sha256sum] = "23ce336db42aacc6ecd7f301da5f42e8dcb143784b926c01f9c7963c2247b5b1"
