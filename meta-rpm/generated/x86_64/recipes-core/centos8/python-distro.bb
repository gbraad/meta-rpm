SUMMARY = "generated recipe based on python-distro srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-distro = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-distro-1.4.0-2.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python3-distro.sha256sum] = "889be1e35217912c6b77551500f01850a589c5b7811aa5aceda16847677c8fd9"
