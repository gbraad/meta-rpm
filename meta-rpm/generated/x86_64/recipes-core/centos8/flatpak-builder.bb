SUMMARY = "generated recipe based on flatpak-builder srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "LGPLv2+ and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "curl elfutils glib-2.0 json-glib libgcc libsoup-2.4 libxml2 libyaml ostree pkgconfig-native"
RPM_SONAME_REQ_flatpak-builder = "libc.so.6 libcurl.so.4 libelf.so.1 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libostree-1.so.1 libpthread.so.0 libsoup-2.4.so.1 libxml2.so.2 libyaml-0.so.2"
RDEPENDS_flatpak-builder = "binutils bzip2 elfutils elfutils-libelf flatpak git-core glib2 glibc json-glib libcurl libgcc libsoup libxml2 libyaml ostree ostree-libs patch tar unzip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/flatpak-builder-1.0.9-2.el8.x86_64.rpm \
          "

SRC_URI[flatpak-builder.sha256sum] = "a2584c2c7184a97887c7fb4dfd59a8230c775df86f8bd760669712bf61182e3e"
