SUMMARY = "generated recipe based on fribidi srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & CLOSED"
RPM_LICENSE = "LGPLv2+ and UCD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_fribidi = "libfribidi.so.0"
RPM_SONAME_REQ_fribidi = "libc.so.6"
RDEPENDS_fribidi = "glibc"
RPM_SONAME_REQ_fribidi-devel = "libfribidi.so.0"
RPROVIDES_fribidi-devel = "fribidi-dev (= 1.0.4)"
RDEPENDS_fribidi-devel = "fribidi pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fribidi-1.0.4-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fribidi-devel-1.0.4-8.el8.x86_64.rpm \
          "

SRC_URI[fribidi.sha256sum] = "010fd82d27afefac9cb4451a30d48dd5035775983b709fac407896e5872ed8d8"
SRC_URI[fribidi-devel.sha256sum] = "ef868a322e55943aee702dd795deec3d5e6ba0d06e444295cbbe9929bc6ae434"
