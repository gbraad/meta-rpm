SUMMARY = "generated recipe based on python-asn1crypto srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-asn1crypto = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-asn1crypto-0.24.0-3.el8.noarch.rpm \
          "

SRC_URI[python3-asn1crypto.sha256sum] = "95155acf769b85643a43b670759ed7e0c14d8b2fbc2cc58f2cee5d38d78d12e8"
