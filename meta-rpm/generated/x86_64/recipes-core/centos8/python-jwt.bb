SUMMARY = "generated recipe based on python-jwt srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-jwt = "platform-python python3-cryptography"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-jwt-1.6.1-2.el8.noarch.rpm \
          "

SRC_URI[python3-jwt.sha256sum] = "ebeb05a4a9cdd5d060859eabac1349029c0120615c98fc939e26664c030655c1"
