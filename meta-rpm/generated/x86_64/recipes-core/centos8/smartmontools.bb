SUMMARY = "generated recipe based on smartmontools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libcap-ng libgcc libselinux pkgconfig-native"
RPM_SONAME_REQ_smartmontools = "libc.so.6 libcap-ng.so.0 libgcc_s.so.1 libm.so.6 libselinux.so.1 libstdc++.so.6"
RDEPENDS_smartmontools = "bash glibc libcap-ng libgcc libselinux libstdc++ systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/smartmontools-6.6-3.el8.x86_64.rpm \
          "

SRC_URI[smartmontools.sha256sum] = "3db81b4c33409d88a337b7ca26dd369c45ffd142096f8125b920a6236b585010"
