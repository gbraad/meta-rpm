SUMMARY = "generated recipe based on python-markdown srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-markdown = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-markdown-2.6.11-2.el8.noarch.rpm \
          "

SRC_URI[python3-markdown.sha256sum] = "fbe39a3c7ca604e0e6f938b2872af266871fded84d854e8ff3369588b8901ee5"
