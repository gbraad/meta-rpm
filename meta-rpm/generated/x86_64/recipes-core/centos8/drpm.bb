SUMMARY = "generated recipe based on drpm srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & BSD"
RPM_LICENSE = "LGPLv2+ and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 openssl pkgconfig-native rpm xz zlib"
RPM_SONAME_PROV_drpm = "libdrpm.so.0"
RPM_SONAME_REQ_drpm = "libbz2.so.1 libc.so.6 libcrypto.so.1.1 liblzma.so.5 librpm.so.8 librpmio.so.8 libz.so.1"
RDEPENDS_drpm = "bzip2-libs glibc openssl-libs rpm-libs xz-libs zlib"
RPM_SONAME_REQ_drpm-devel = "libdrpm.so.0"
RPROVIDES_drpm-devel = "drpm-dev (= 0.4.1)"
RDEPENDS_drpm-devel = "drpm pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/drpm-0.4.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/drpm-devel-0.4.1-1.el8.x86_64.rpm \
          "

SRC_URI[drpm.sha256sum] = "1a6f97a0143332d047c9fa00ff802453e09ecedaeb2537ef5db675050025fea1"
SRC_URI[drpm-devel.sha256sum] = "d0abcc6e52e5d904e815304ee9ab188a08969a2185bab2d4575363d6f0fb0174"
