SUMMARY = "generated recipe based on wsmancli srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openwsman pkgconfig-native"
RPM_SONAME_REQ_wsmancli = "libc.so.6 libwsman.so.1 libwsman_client.so.4 libwsman_curl_client_transport.so.1"
RDEPENDS_wsmancli = "curl glibc libwsman1"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/wsmancli-2.6.0-8.el8.x86_64.rpm \
          "

SRC_URI[wsmancli.sha256sum] = "e6d9e653f32ef18b34afdb0cccb7f25d875f3f477690b5834640d60f49eceb20"
