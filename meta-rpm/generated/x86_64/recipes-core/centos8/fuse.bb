SUMMARY = "generated recipe based on fuse srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_fuse = "libc.so.6 libpthread.so.0"
RDEPENDS_fuse = "fuse-common glibc which"
RPM_SONAME_REQ_fuse-devel = "libfuse.so.2 libulockmgr.so.1"
RPROVIDES_fuse-devel = "fuse-dev (= 2.9.7)"
RDEPENDS_fuse-devel = "fuse-libs pkgconf-pkg-config"
RPM_SONAME_PROV_fuse-libs = "libfuse.so.2 libulockmgr.so.1"
RPM_SONAME_REQ_fuse-libs = "libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_fuse-libs = "glibc"
RPM_SONAME_REQ_fuse3 = "libc.so.6"
RDEPENDS_fuse3 = "fuse-common glibc"
RPM_SONAME_REQ_fuse3-devel = "libfuse3.so.3"
RPROVIDES_fuse3-devel = "fuse3-dev (= 3.2.1)"
RDEPENDS_fuse3-devel = "fuse3-libs pkgconf-pkg-config"
RPM_SONAME_PROV_fuse3-libs = "libfuse3.so.3"
RPM_SONAME_REQ_fuse3-libs = "libc.so.6 libdl.so.2 libpthread.so.0 librt.so.1"
RDEPENDS_fuse3-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fuse-2.9.7-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fuse-common-3.2.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fuse-devel-2.9.7-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fuse-libs-2.9.7-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fuse3-3.2.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fuse3-devel-3.2.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fuse3-libs-3.2.1-12.el8.x86_64.rpm \
          "

SRC_URI[fuse.sha256sum] = "2465c0c3b3d9519a3f9ae2ffe3e2c0bc61dca6fcb6ae710a6c7951007f498864"
SRC_URI[fuse-common.sha256sum] = "3f947e1e56d0b0210f9ccbc4483f8b6bfb100cfd79ea1efac3336a8d624ec0d6"
SRC_URI[fuse-devel.sha256sum] = "9ca97a0f34f98803bdf8208c7b51e74c7ce9a8d816953ee77a7a52a3dd7862b2"
SRC_URI[fuse-libs.sha256sum] = "6c6c98e2ddc2210ca377b0ef0c6bb694abd23f33413acadaedc1760da5bcc079"
SRC_URI[fuse3.sha256sum] = "bef6876ead539276d4a46da8b7f9cc7f65fb665fcca9de184ff0bea4d7b9593b"
SRC_URI[fuse3-devel.sha256sum] = "1c432ade09df3d88f1864ec4db4332d8e911f9076406e724b01dd2e5a5b35cbe"
SRC_URI[fuse3-libs.sha256sum] = "0ef2747eb5a6008729aaf0c5c151d640918e660d8543ef6590b255dd9952613c"
