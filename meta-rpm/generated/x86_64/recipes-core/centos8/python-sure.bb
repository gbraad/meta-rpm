SUMMARY = "generated recipe based on python-sure srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-sure = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-sure-1.4.0-6.el8.noarch.rpm \
          "

SRC_URI[python3-sure.sha256sum] = "2c3a9ea913409d408dedb5bb7c409f4745471ecbc551cc138a589473bd554868"
