SUMMARY = "generated recipe based on colord srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 lcms2 libgcc libgudev libgusb pkgconfig-native polkit sqlite3 systemd-libs"
RPM_SONAME_PROV_colord = "libcolord_sensor_argyll.so libcolord_sensor_camera.so libcolord_sensor_colorhug.so libcolord_sensor_dtp94.so libcolord_sensor_dummy.so libcolord_sensor_huey.so libcolord_sensor_scanner.so"
RPM_SONAME_REQ_colord = "libc.so.6 libcolord.so.2 libcolordprivate.so.2 libcolorhug.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libgusb.so.2 liblcms2.so.2 libm.so.6 libpolkit-gobject-1.so.0 libpthread.so.0 libsqlite3.so.0 libsystemd.so.0"
RDEPENDS_colord = "bash color-filesystem colord-libs glib2 glibc lcms2 libgcc libgudev libgusb polkit-libs shadow-utils sqlite-libs systemd systemd-libs"
RPM_SONAME_REQ_colord-devel = "libcolord.so.2 libcolordprivate.so.2 libcolorhug.so.2"
RPROVIDES_colord-devel = "colord-dev (= 1.4.2)"
RDEPENDS_colord-devel = "colord colord-libs glib2-devel libgusb-devel pkgconf-pkg-config"
RDEPENDS_colord-devel-docs = "colord"
RPM_SONAME_PROV_colord-libs = "libcolord.so.2 libcolordprivate.so.2 libcolorhug.so.2"
RPM_SONAME_REQ_colord-libs = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgusb.so.2 liblcms2.so.2 libm.so.6 libudev.so.1"
RDEPENDS_colord-libs = "glib2 glibc lcms2 libgcc libgusb systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/colord-1.4.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/colord-libs-1.4.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/colord-devel-1.4.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/colord-devel-docs-1.4.2-1.el8.noarch.rpm \
          "

SRC_URI[colord.sha256sum] = "536a60240d63f7bba3900f45ab5f5053026b7a3aae39b96d8c06d5a7afc37e8d"
SRC_URI[colord-devel.sha256sum] = "7d16285fa9534cbb13890f08e9003b71fc9ff4181f4a5c300a837ba1ba2a2354"
SRC_URI[colord-devel-docs.sha256sum] = "b1fa7f4ace2f9b36988d1be8e950e32eb33bbc9aa08100f71198baf68a200ecf"
SRC_URI[colord-libs.sha256sum] = "96af3978c25911547aafb3031d1e144484a080ba5fbe960a65235e22beb4d2fc"
