SUMMARY = "generated recipe based on nghttp2 srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "c-ares libev libgcc libnghttp2 openssl pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_nghttp2 = "libc.so.6 libcares.so.2 libcrypto.so.1.1 libdl.so.2 libev.so.4 libgcc_s.so.1 libm.so.6 libnghttp2.so.14 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libsystemd.so.0 libz.so.1"
RDEPENDS_nghttp2 = "bash c-ares glibc libev libgcc libnghttp2 libstdc++ openssl-libs platform-python systemd systemd-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/nghttp2-1.33.0-3.el8_2.1.x86_64.rpm \
          "

SRC_URI[nghttp2.sha256sum] = "0a22c46d92998506b169a2b81a86482ce05b2810eac790ffe3d865c96af4f9da"
