SUMMARY = "generated recipe based on osgi-core srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_osgi-core = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_osgi-core-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/osgi-core-6.0.0-6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/osgi-core-javadoc-6.0.0-6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[osgi-core.sha256sum] = "f7497098209fa657cb76baa8441faee8c479e1d409d4e206aec0da0f75fe098e"
SRC_URI[osgi-core-javadoc.sha256sum] = "fd6c0a637dfcf73c39259e4027eb60372ba861df49ba43d43a6a90a1f2a83f7e"
