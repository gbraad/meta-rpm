SUMMARY = "generated recipe based on libXres srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXres = "libXRes.so.1"
RPM_SONAME_REQ_libXres = "libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXres = "glibc libX11 libXext"
RPM_SONAME_REQ_libXres-devel = "libXRes.so.1"
RPROVIDES_libXres-devel = "libXres-dev (= 1.2.0)"
RDEPENDS_libXres-devel = "libX11-devel libXext-devel libXres pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXres-1.2.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libXres-devel-1.2.0-4.el8.x86_64.rpm \
          "

SRC_URI[libXres.sha256sum] = "5d9d2d571446eb9ce858952dbd6518e66f7ada998a6d51e5ecbf3b8905273485"
SRC_URI[libXres-devel.sha256sum] = "a4804ef221f038a39bd0c8ebcc8c8fd4e2fd83c18ba8c9a5997a2f01349667d3"
