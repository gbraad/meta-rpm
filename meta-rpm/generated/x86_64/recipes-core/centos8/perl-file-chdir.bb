SUMMARY = "generated recipe based on perl-File-chdir srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-chdir = "perl-Carp perl-Exporter perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-File-chdir-0.1011-5.el8.noarch.rpm \
          "

SRC_URI[perl-File-chdir.sha256sum] = "f2eb0420ccad39db24dd8b1b32a7ec03b89256ab5c6150d9d1e5d34cfb2d90d8"
