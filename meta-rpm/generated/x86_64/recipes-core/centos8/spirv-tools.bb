SUMMARY = "generated recipe based on spirv-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_spirv-tools = "libSPIRV-Tools-link.so libSPIRV-Tools-opt.so libSPIRV-Tools-reduce.so libSPIRV-Tools.so libc.so.6 libgcc_s.so.1 libm.so.6 librt.so.1 libstdc++.so.6"
RDEPENDS_spirv-tools = "bash glibc libgcc libstdc++ spirv-tools-libs"
RPROVIDES_spirv-tools-devel = "spirv-tools-dev (= 2019.5)"
RDEPENDS_spirv-tools-devel = "cmake-filesystem pkgconf-pkg-config spirv-tools-libs"
RPM_SONAME_PROV_spirv-tools-libs = "libSPIRV-Tools-link.so libSPIRV-Tools-opt.so libSPIRV-Tools-reduce.so libSPIRV-Tools-shared.so libSPIRV-Tools.so"
RPM_SONAME_REQ_spirv-tools-libs = "libc.so.6 libgcc_s.so.1 libm.so.6 librt.so.1 libstdc++.so.6"
RDEPENDS_spirv-tools-libs = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spirv-tools-2019.5-2.20200421.git67f4838.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spirv-tools-libs-2019.5-2.20200421.git67f4838.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/spirv-tools-devel-2019.5-2.20200421.git67f4838.el8.x86_64.rpm \
          "

SRC_URI[spirv-tools.sha256sum] = "67c01524af6cf9b127f7db14be73977a3a7d5a81a5c8163c332220bc275e5807"
SRC_URI[spirv-tools-devel.sha256sum] = "605f3cba94ba5b70ffc13ce461f76a1ed8b84144c638db37842bb0e2cefdd9c3"
SRC_URI[spirv-tools-libs.sha256sum] = "82820519b2cbc593868dd2bcf67aa4c0708fe05ebed2ac3d1881782dd8e06e9d"
