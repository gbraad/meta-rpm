SUMMARY = "generated recipe based on numatop srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses numactl pkgconfig-native"
RPM_SONAME_REQ_numatop = "libc.so.6 libncurses.so.6 libncursesw.so.6 libnuma.so.1 libpthread.so.0 libtinfo.so.6"
RDEPENDS_numatop = "glibc ncurses-libs numactl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/numatop-2.1-3.el8.x86_64.rpm \
          "

SRC_URI[numatop.sha256sum] = "f32e01f0f42e54c7df2f2fb3a781615001714a1d5ce881492178dfe02105580d"
