SUMMARY = "generated recipe based on mingw-libjpeg-turbo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "wxWidgets"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-libjpeg-turbo = "mingw32-crt mingw32-filesystem"
RDEPENDS_mingw32-libjpeg-turbo-static = "mingw32-libjpeg-turbo"
RDEPENDS_mingw64-libjpeg-turbo = "mingw64-crt mingw64-filesystem"
RDEPENDS_mingw64-libjpeg-turbo-static = "mingw64-libjpeg-turbo"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-libjpeg-turbo-1.5.1-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-libjpeg-turbo-static-1.5.1-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-libjpeg-turbo-1.5.1-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-libjpeg-turbo-static-1.5.1-5.el8.noarch.rpm \
          "

SRC_URI[mingw32-libjpeg-turbo.sha256sum] = "68331387460403235d96953947021255b012204974c94fda89e76b02992fe9dc"
SRC_URI[mingw32-libjpeg-turbo-static.sha256sum] = "64d402c5b1577b6227d6950d3a3ee368f1f00b4069f19a35b11f7dda015b2dcb"
SRC_URI[mingw64-libjpeg-turbo.sha256sum] = "754dbfbe001c88ebce40e260a324b1f795e8f641df1615fd1e673d76a19920d6"
SRC_URI[mingw64-libjpeg-turbo-static.sha256sum] = "ed7fdd61122e12916af36defbbb30ebd254484336eb429cef01e69b971b9f1cc"
