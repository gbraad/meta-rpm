SUMMARY = "generated recipe based on sil-nuosu-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_sil-nuosu-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sil-nuosu-fonts-2.1.1-14.el8.noarch.rpm \
          "

SRC_URI[sil-nuosu-fonts.sha256sum] = "8fa61af2c45134b23a39d1e32738604ed55ba67a6a4633c64edfb39b8e19f036"
