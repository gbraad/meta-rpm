SUMMARY = "generated recipe based on plexus-containers srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & MIT & xpp"
RPM_LICENSE = "ASL 2.0 and MIT and xpp"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-containers = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-pom"
RDEPENDS_plexus-containers-component-annotations = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_plexus-containers-component-javadoc = "java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools"
RDEPENDS_plexus-containers-component-metadata = "apache-commons-cli java-1.8.0-openjdk-headless javapackages-filesystem jdom2 maven-lib maven-project objectweb-asm plexus-cli plexus-containers-component-annotations plexus-containers-container-default plexus-utils qdox"
RDEPENDS_plexus-containers-container-default = "guava20 java-1.8.0-openjdk-headless javapackages-filesystem objectweb-asm plexus-classworlds plexus-utils xbean"
RDEPENDS_plexus-containers-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-containers-1.7.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-containers-component-annotations-1.7.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-containers-component-javadoc-1.7.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-containers-component-metadata-1.7.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-containers-container-default-1.7.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-containers-javadoc-1.7.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-containers.sha256sum] = "5b3b686c55a4a938fe053450ce2f3ebda98b841f1fa851c2ade6a8fbaf6b9304"
SRC_URI[plexus-containers-component-annotations.sha256sum] = "4d62e0a529de4a3e36d7ef3e36cd410334c168d013ac52d4af8283b67a780602"
SRC_URI[plexus-containers-component-javadoc.sha256sum] = "4142dc08bc914810cf8281e2b8ce9c60f8f32cf69331ade5173257bcc43edb5c"
SRC_URI[plexus-containers-component-metadata.sha256sum] = "46b80945b7e381270aba593cf89b5c2619c765c8fd4abdc3a7b40cbfd4a43dce"
SRC_URI[plexus-containers-container-default.sha256sum] = "d36cc63ac3e00958fce0b8dc30b94b6cf99b7be4f5cd9b74bdb51878c6875284"
SRC_URI[plexus-containers-javadoc.sha256sum] = "69a45027daf4642b57814629bda9ae945645ce7ad53e071289ddc6c6937177f0"
