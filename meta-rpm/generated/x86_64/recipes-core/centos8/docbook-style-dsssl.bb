SUMMARY = "generated recipe based on docbook-style-dsssl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "DMIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_docbook-style-dsssl = "bash docbook-dtds openjade perl-interpreter sgml-common"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/docbook-style-dsssl-1.79-25.el8.noarch.rpm \
          "

SRC_URI[docbook-style-dsssl.sha256sum] = "25111d7b9585c9c5eda8c99d2a4236c3c0936ced9d6aeaf005e8db81398d2915"
