SUMMARY = "generated recipe based on perl-Taint-Runtime srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Taint-Runtime = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Taint-Runtime = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Taint-Runtime-0.03-32.el8.x86_64.rpm \
          "

SRC_URI[perl-Taint-Runtime.sha256sum] = "522e0223ffde1fb4daf28839570bff31a23fb30431a68dfc2dc18ba25cf99216"
