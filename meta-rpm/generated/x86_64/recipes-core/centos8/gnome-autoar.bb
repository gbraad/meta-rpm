SUMMARY = "generated recipe based on gnome-autoar srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libarchive libgcc pango pkgconfig-native"
RPM_SONAME_PROV_gnome-autoar = "libgnome-autoar-0.so.0 libgnome-autoar-gtk-0.so.0"
RPM_SONAME_REQ_gnome-autoar = "libarchive.so.13 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_gnome-autoar = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libarchive libgcc pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-autoar-0.2.3-1.el8.x86_64.rpm \
          "

SRC_URI[gnome-autoar.sha256sum] = "890dda52b746089985a073d45c42f503044c32b5a2de5675b2d9033a19ff07e5"
