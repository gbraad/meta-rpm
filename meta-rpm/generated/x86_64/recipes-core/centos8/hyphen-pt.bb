SUMMARY = "generated recipe based on hyphen-pt srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-pt = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-pt-0.20021021-17.el8.noarch.rpm \
          "

SRC_URI[hyphen-pt.sha256sum] = "96a43dbb37393a10f3b7c36063f1f7a64504f824796ee805bf00bcaba000aa3f"
