SUMMARY = "generated recipe based on perl-Module-Implementation srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0"
RPM_LICENSE = "Artistic 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Implementation = "perl-Carp perl-Module-Runtime perl-Try-Tiny perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Module-Implementation-0.09-15.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Implementation.sha256sum] = "f91716b2780593bea26a14889785ff076e8990ad2bd1ab3dc285ffaed04c33b1"
