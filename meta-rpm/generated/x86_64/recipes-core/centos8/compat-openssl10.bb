SUMMARY = "generated recipe based on compat-openssl10 srpm"
DESCRIPTION = "Description"
LICENSE = "OpenSSL"
RPM_LICENSE = "OpenSSL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_compat-openssl10 = "libcrypto.so.10 libssl.so.10"
RPM_SONAME_REQ_compat-openssl10 = "libc.so.6 libdl.so.2 libz.so.1"
RDEPENDS_compat-openssl10 = "coreutils crypto-policies glibc make zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/compat-openssl10-1.0.2o-3.el8.x86_64.rpm \
          "

SRC_URI[compat-openssl10.sha256sum] = "2baa59d2c0c03ce262a5188596c8f0e4b90c307f81236746a5e831d4e17a18ca"
