SUMMARY = "generated recipe based on hyphen-sl srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-sl = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-sl-0.20070127-16.el8.noarch.rpm \
          "

SRC_URI[hyphen-sl.sha256sum] = "f3f4b57315656ac604a1828cb9bca0589022c8fcefe02219b35f77f8a266c542"
