SUMMARY = "generated recipe based on libpcap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "BSD with advertising"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libpcap = "libpcap.so.1"
RPM_SONAME_REQ_libpcap = "libc.so.6"
RDEPENDS_libpcap = "glibc"
RPM_SONAME_REQ_libpcap-devel = "libpcap.so.1"
RPROVIDES_libpcap-devel = "libpcap-dev (= 1.9.0)"
RDEPENDS_libpcap-devel = "bash libpcap pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libpcap-1.9.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libpcap-devel-1.9.0-3.el8.x86_64.rpm \
          "

SRC_URI[libpcap.sha256sum] = "59d387adafdee640abc4639bdde9e2cde08db8a4ae88a8cfe981556f7d9127d9"
SRC_URI[libpcap-devel.sha256sum] = "c441cdd440ffb69cfca2cd3b9654a6e4156989251049030d9ae5ceb66f21567e"
