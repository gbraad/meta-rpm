SUMMARY = "generated recipe based on python-schedutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-schedutils = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-schedutils = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-schedutils-0.6-6.el8.x86_64.rpm \
          "

SRC_URI[python3-schedutils.sha256sum] = "bafc2680bd298f0fac70854224ef03b7098d4c46ee35e270f6fd58d2e812ff1b"
