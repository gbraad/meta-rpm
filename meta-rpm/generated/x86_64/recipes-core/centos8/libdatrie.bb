SUMMARY = "generated recipe based on libdatrie srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdatrie = "libdatrie.so.1"
RPM_SONAME_REQ_libdatrie = "libc.so.6"
RDEPENDS_libdatrie = "glibc"
RPM_SONAME_REQ_libdatrie-devel = "libc.so.6 libdatrie.so.1"
RPROVIDES_libdatrie-devel = "libdatrie-dev (= 0.2.9)"
RDEPENDS_libdatrie-devel = "glibc libdatrie pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libdatrie-0.2.9-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdatrie-devel-0.2.9-7.el8.x86_64.rpm \
          "

SRC_URI[libdatrie.sha256sum] = "7d43fda5ced8faf64d09cb3c47dcb6c9aa1fd936fc49f8609af29780c7a75f90"
SRC_URI[libdatrie-devel.sha256sum] = "d597fe80015f634226f15513d0d3309749294eba8eeaaf3e5dd8fd2072f8e6ef"
