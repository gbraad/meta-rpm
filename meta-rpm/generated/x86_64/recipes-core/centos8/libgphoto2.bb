SUMMARY = "generated recipe based on libgphoto2 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0"
RPM_LICENSE = "GPLv2+ and GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gd libexif libjpeg-turbo libtool-ltdl libusb1 libxml2 lockdev pkgconfig-native"
RPM_SONAME_PROV_libgphoto2 = "libgphoto2.so.6 libgphoto2_port.so.12"
RPM_SONAME_REQ_libgphoto2 = "libc.so.6 libexif.so.12 libgd.so.3 libjpeg.so.62 liblockdev.so.1 libltdl.so.7 libm.so.6 libpthread.so.0 libusb-1.0.so.0 libxml2.so.2"
RDEPENDS_libgphoto2 = "bash gd glibc libexif libjpeg-turbo libtool-ltdl libusbx libxml2 lockdev"
RPM_SONAME_REQ_libgphoto2-devel = "libgphoto2.so.6 libgphoto2_port.so.12"
RPROVIDES_libgphoto2-devel = "libgphoto2-dev (= 2.5.16)"
RDEPENDS_libgphoto2-devel = "bash libexif-devel libgphoto2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgphoto2-2.5.16-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgphoto2-devel-2.5.16-3.el8.x86_64.rpm \
          "

SRC_URI[libgphoto2.sha256sum] = "703e0e0ac3040da8506435860da5c5f6322c7b1987c27f3b51dcd92151efdeb0"
SRC_URI[libgphoto2-devel.sha256sum] = "8239c5231ac5ba20e491301a6dddb7b379098044840fbc363c7fcc17c3445ec9"
