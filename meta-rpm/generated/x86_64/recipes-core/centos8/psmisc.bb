SUMMARY = "generated recipe based on psmisc srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libselinux ncurses pkgconfig-native"
RPM_SONAME_REQ_psmisc = "libc.so.6 libselinux.so.1 libtinfo.so.6"
RDEPENDS_psmisc = "glibc libselinux ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/psmisc-23.1-4.el8.x86_64.rpm \
          "

SRC_URI[psmisc.sha256sum] = "e12b1fbdfb203c0c925eab4f90c2a49ed80b40ff7e1daa698671f0a29a1b7316"
