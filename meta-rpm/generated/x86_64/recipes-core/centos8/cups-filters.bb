SUMMARY = "generated recipe based on cups-filters srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & MIT"
RPM_LICENSE = "LGPLv2 and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "avahi avahi-libs cups-libs dbus-libs e2fsprogs fontconfig freetype glib-2.0 krb5-libs lcms2 libgcc libjpeg-turbo libpng libxcrypt pkgconfig-native poppler qpdf tiff zlib"
RPM_SONAME_REQ_cups-filters = "libavahi-client.so.3 libavahi-common.so.3 libavahi-glib.so.1 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupsfilters.so.1 libcupsimage.so.2 libdbus-1.so.3 libdl.so.2 libfontconfig.so.1 libfontembed.so.1 libfreetype.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libjpeg.so.62 libk5crypto.so.3 libkrb5.so.3 liblcms2.so.2 libm.so.6 libpng16.so.16 libpoppler.so.78 libpthread.so.0 libqpdf.so.18 libstdc++.so.6 libtiff.so.5 libz.so.1"
RDEPENDS_cups-filters = "avahi-glib avahi-libs bash bc cups cups-filesystem cups-filters-libs cups-libs dbus-libs fontconfig freetype ghostscript glib2 glibc grep krb5-libs lcms2 libcom_err liberation-mono-fonts libgcc libjpeg-turbo libpng libstdc++ libtiff libxcrypt poppler poppler-utils qpdf-libs sed systemd which zlib"
RPM_SONAME_REQ_cups-filters-devel = "libcupsfilters.so.1 libfontembed.so.1"
RPROVIDES_cups-filters-devel = "cups-filters-dev (= 1.20.0)"
RDEPENDS_cups-filters-devel = "cups-filters-libs pkgconf-pkg-config"
RPM_SONAME_PROV_cups-filters-libs = "libcupsfilters.so.1 libfontembed.so.1"
RPM_SONAME_REQ_cups-filters-libs = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupsimage.so.2 libdbus-1.so.3 libdl.so.2 libgssapi_krb5.so.2 libjpeg.so.62 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpng16.so.16 libpthread.so.0 libtiff.so.5 libz.so.1"
RDEPENDS_cups-filters-libs = "cups-libs dbus-libs glibc krb5-libs libcom_err libjpeg-turbo libpng libtiff libxcrypt zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cups-filters-1.20.0-19.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cups-filters-libs-1.20.0-19.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cups-filters-devel-1.20.0-19.el8.0.1.x86_64.rpm \
          "

SRC_URI[cups-filters.sha256sum] = "ec2de809ffd2e72678bfc2c4c239833e2feee1e25a07f4a37d8b753d393868af"
SRC_URI[cups-filters-devel.sha256sum] = "7440ecfe6355c9c0ba352d696980906399c3dc99bb6985a63cc7fcd50aeeaa84"
SRC_URI[cups-filters-libs.sha256sum] = "0b058161ac571fabc1ab7435edfad11440264019c737a973c4182875a0162f7d"
