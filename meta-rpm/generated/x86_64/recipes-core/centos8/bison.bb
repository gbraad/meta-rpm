SUMMARY = "generated recipe based on bison srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_bison = "libc.so.6"
RDEPENDS_bison = "bash glibc info m4"
RPROVIDES_bison-devel = "bison-dev (= 3.0.4)"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bison-3.0.4-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bison-runtime-3.0.4-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/bison-devel-3.0.4-10.el8.x86_64.rpm \
          "

SRC_URI[bison.sha256sum] = "2357b5cb84974f2cdfd4d9b4a1da8af70a73e50a70c4f118c173dc7a9df21550"
SRC_URI[bison-devel.sha256sum] = "338e9073c73252ab8fb0919dfaede6609be600f239b833db7bae4dda37b840f4"
SRC_URI[bison-runtime.sha256sum] = "ebe200f02da979fe22792f7ddeeb3b38d80812a7c17d6e63696ad3cf7f63f38a"
