SUMMARY = "generated recipe based on go-srpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/go-srpm-macros-2-16.el8.noarch.rpm \
          "

SRC_URI[go-srpm-macros.sha256sum] = "a7c54270d5d486df52b4c82b86028cfaa07f71216130d38d87606a4893af6a57"
