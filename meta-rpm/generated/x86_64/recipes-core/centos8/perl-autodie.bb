SUMMARY = "generated recipe based on perl-autodie srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-autodie = "perl-Carp perl-Exporter perl-IPC-System-Simple perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-autodie-2.29-396.el8.noarch.rpm \
          "

SRC_URI[perl-autodie.sha256sum] = "1bbb80b975dfc090996c9c73a50bd0285883e866b12f2b86cbc1e498f6e026d4"
