SUMMARY = "generated recipe based on lvm2 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libblkid libgcc libselinux libsepol pkgconfig-native systemd-libs"
RPM_SONAME_REQ_device-mapper-event = "libblkid.so.1 libc.so.6 libdevmapper-event.so.1.02 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libselinux.so.1 libsepol.so.1 libudev.so.1"
RDEPENDS_device-mapper-event = "bash device-mapper device-mapper-event-libs device-mapper-libs glibc libblkid libgcc libselinux libsepol systemd systemd-libs"
RPM_SONAME_REQ_device-mapper-event-devel = "libdevmapper-event.so.1.02"
RPROVIDES_device-mapper-event-devel = "device-mapper-event-dev (= 1.02.169)"
RDEPENDS_device-mapper-event-devel = "device-mapper-event device-mapper-event-libs pkgconf-pkg-config"
RPM_SONAME_PROV_device-mapper-event-libs = "libdevmapper-event.so.1.02"
RPM_SONAME_REQ_device-mapper-event-libs = "libblkid.so.1 libc.so.6 libdevmapper.so.1.02 libm.so.6 libpthread.so.0 libselinux.so.1 libsepol.so.1 libudev.so.1"
RDEPENDS_device-mapper-event-libs = "device-mapper-libs glibc libblkid libselinux libsepol systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/device-mapper-event-1.02.169-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/device-mapper-event-libs-1.02.169-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/device-mapper-event-devel-1.02.169-3.el8.x86_64.rpm \
          "

SRC_URI[device-mapper-event.sha256sum] = "493a8bf9dee7dc30f2e18f549e415f926e6afb20a2f8d585b62e43087302b6dd"
SRC_URI[device-mapper-event-devel.sha256sum] = "f1a30dc21ff184f21191709e6fcac064a8117caae45026f265c6d941f0f0b4cb"
SRC_URI[device-mapper-event-libs.sha256sum] = "6b43414c6de73973ff04228ca5693e4a8550b66f8fcc84df2f061a79508e0db3"
