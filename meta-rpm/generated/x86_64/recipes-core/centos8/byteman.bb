SUMMARY = "generated recipe based on byteman srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_byteman = "bash java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_byteman-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/byteman-4.0.4-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/byteman-javadoc-4.0.4-2.el8.noarch.rpm \
          "

SRC_URI[byteman.sha256sum] = "8436a627532bba16146066a3d0803a12106b959f2e6d0f1cfba350a4ba4d0b22"
SRC_URI[byteman-javadoc.sha256sum] = "8d46bbf0a52d4dc65a1a98c0bb90da9834b9cddd8548f3c825416a94e8382a32"
