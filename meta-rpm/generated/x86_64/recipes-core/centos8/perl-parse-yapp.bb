SUMMARY = "generated recipe based on perl-Parse-Yapp srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Parse-Yapp = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Parse-Yapp-1.21-2.el8.noarch.rpm \
          "

SRC_URI[perl-Parse-Yapp.sha256sum] = "b43fe7a5ae6bd0ef3dd3fb37cf7d736ed15e65a299c15ddc3990de24f9b5e11c"
