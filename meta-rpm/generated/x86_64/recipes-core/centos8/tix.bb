SUMMARY = "generated recipe based on tix srpm"
DESCRIPTION = "Description"
LICENSE = "tcl"
RPM_LICENSE = "TCL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 pkgconfig-native"
RPM_SONAME_PROV_tix = "libTix.so"
RPM_SONAME_REQ_tix = "libX11.so.6 libc.so.6"
RDEPENDS_tix = "glibc libX11 tcl tk"
RPROVIDES_tix-devel = "tix-dev (= 8.4.3)"
RDEPENDS_tix-devel = "tix"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tix-8.4.3-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/tix-devel-8.4.3-23.el8.x86_64.rpm \
          "

SRC_URI[tix.sha256sum] = "dfd51ed2ccf089a0d52af7183636ff66b938827024da0746d023e7450318af71"
SRC_URI[tix-devel.sha256sum] = "d04d7dfc7508a8ce6c34d9d026ca5ec97fa729ceef251dc4350b923315a378cb"
