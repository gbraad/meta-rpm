SUMMARY = "generated recipe based on perl-Encode srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Encode = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Encode = "glibc perl-Carp perl-Exporter perl-Getopt-Long perl-MIME-Base64 perl-Storable perl-constant perl-interpreter perl-libs perl-parent"
RPROVIDES_perl-Encode-devel = "perl-Encode-dev (= 2.97)"
RDEPENDS_perl-Encode-devel = "perl-Encode perl-constant perl-interpreter perl-libs"
RDEPENDS_perl-encoding = "perl-Carp perl-Encode perl-Filter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Encode-devel-2.97-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-encoding-2.22-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Encode-2.97-3.el8.x86_64.rpm \
          "

SRC_URI[perl-Encode.sha256sum] = "d2b0e4b28a5aac754f6caa119d5479a64816f93c059e0ac564e46391264e2234"
SRC_URI[perl-Encode-devel.sha256sum] = "c06fce94103d5a06b9824e663af228cb6ddd4fecbf59b4a265a5e7da611aed3d"
SRC_URI[perl-encoding.sha256sum] = "81944f4e69960fb11958390fb4a35b0b06016863a08a71b276f6cd5369d4f5eb"
