SUMMARY = "generated recipe based on shared-mime-info srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libxml2 pkgconfig-native"
RPM_SONAME_REQ_shared-mime-info = "libc.so.6 libglib-2.0.so.0 libxml2.so.2"
RDEPENDS_shared-mime-info = "bash coreutils glib2 glibc libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/shared-mime-info-1.9-3.el8.x86_64.rpm \
          "

SRC_URI[shared-mime-info.sha256sum] = "3f3cced089849779b46d7b1f27ae1b73e0b1144eca15716e4c19e4b54bb16f6c"
