SUMMARY = "generated recipe based on system-config-printer srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cups-libs glib-2.0 libusb1 pkgconfig-native systemd-libs"
RDEPENDS_system-config-printer-libs = "bash gobject-introspection gtk3 platform-python python3-cups python3-dbus python3-gobject python3-pycurl python3-requests"
RPM_SONAME_REQ_system-config-printer-udev = "libc.so.6 libcups.so.2 libglib-2.0.so.0 libudev.so.1 libusb-1.0.so.0"
RDEPENDS_system-config-printer-udev = "cups-libs glib2 glibc libusbx platform-python system-config-printer-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/system-config-printer-libs-1.5.11-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/system-config-printer-udev-1.5.11-13.el8.x86_64.rpm \
          "

SRC_URI[system-config-printer-libs.sha256sum] = "bbf858e39acba6a9c722d038c2ba9389756e2a034abd4e82aa89c0dacfa0bdfd"
SRC_URI[system-config-printer-udev.sha256sum] = "d7a2bd6c2de99d5aaf49a086c145535724810a6edcea927b71ed1d51923c4624"
