SUMMARY = "generated recipe based on libepubgen srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-2.0"
RPM_LICENSE = "MPLv2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc librevenge pkgconfig-native"
RPM_SONAME_PROV_libepubgen = "libepubgen-0.1.so.1"
RPM_SONAME_REQ_libepubgen = "libc.so.6 libgcc_s.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6"
RDEPENDS_libepubgen = "glibc libgcc librevenge libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libepubgen-0.1.0-2.el8.x86_64.rpm \
          "

SRC_URI[libepubgen.sha256sum] = "e278f07758a681e1f9e19addee6ab5e3064ff1192b89abcb4ca60575aad50242"
