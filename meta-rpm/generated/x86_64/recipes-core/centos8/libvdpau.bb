SUMMARY = "generated recipe based on libvdpau srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libx11 libxext pkgconfig-native"
RPM_SONAME_PROV_libvdpau = "libvdpau.so.1 libvdpau_trace.so.1"
RPM_SONAME_REQ_libvdpau = "libXext.so.6 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libvdpau = "glibc libXext libgcc libstdc++"
RPM_SONAME_REQ_libvdpau-devel = "libvdpau.so.1"
RPROVIDES_libvdpau-devel = "libvdpau-dev (= 1.1.1)"
RDEPENDS_libvdpau-devel = "libX11-devel libvdpau pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvdpau-1.1.1-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libvdpau-devel-1.1.1-7.el8.x86_64.rpm \
          "

SRC_URI[libvdpau.sha256sum] = "ff79461f253b73795ecc57a354a30bcb9f4522c156eee5ac027718fe457f927b"
SRC_URI[libvdpau-devel.sha256sum] = "96f1972c8fb3899259092a37dae866a6a55c47a2c0d6cf08e6f157b62f0e8b9c"
