SUMMARY = "generated recipe based on gnome-bluetooth srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 gtk+3 libcanberra libnotify pkgconfig-native systemd systemd-libs"
RPM_SONAME_REQ_gnome-bluetooth = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgnome-bluetooth.so.13 libgobject-2.0.so.0 libgtk-3.so.0"
RDEPENDS_gnome-bluetooth = "bluez bluez-obexd glib2 glibc gnome-bluetooth-libs gtk3 pulseaudio-module-bluetooth"
RPM_SONAME_PROV_gnome-bluetooth-libs = "libgnome-bluetooth.so.13"
RPM_SONAME_REQ_gnome-bluetooth-libs = "libc.so.6 libcanberra-gtk3.so.0 libcanberra.so.0 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libnotify.so.4 libpthread.so.0 libudev.so.1"
RDEPENDS_gnome-bluetooth-libs = "glib2 glibc gtk3 libcanberra libcanberra-gtk3 libnotify systemd-libs"
RPM_SONAME_REQ_gnome-bluetooth-libs-devel = "libgnome-bluetooth.so.13"
RPROVIDES_gnome-bluetooth-libs-devel = "gnome-bluetooth-libs-dev (= 3.28.2)"
RDEPENDS_gnome-bluetooth-libs-devel = "glib2-devel gnome-bluetooth gnome-bluetooth-libs gtk3-devel pkgconf-pkg-config systemd-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-bluetooth-3.28.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-bluetooth-libs-3.28.2-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gnome-bluetooth-libs-devel-3.28.2-2.el8.x86_64.rpm \
          "

SRC_URI[gnome-bluetooth.sha256sum] = "db6adba9998429e7b48340008034f3069f8b2ad596e4909742c6beac0ae083ec"
SRC_URI[gnome-bluetooth-libs.sha256sum] = "5b8f483986a47cdd9bbbb97112c8f98a9df02da329057fbfa4426730a0e93fe3"
SRC_URI[gnome-bluetooth-libs-devel.sha256sum] = "bd2b64937aeb01148d7ad67391b6ddf0fbf30703c7d872ba670623336d0f6802"
