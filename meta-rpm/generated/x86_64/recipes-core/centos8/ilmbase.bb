SUMMARY = "generated recipe based on ilmbase srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd mesa-libglu pkgconfig-native"
RPM_SONAME_PROV_ilmbase = "libHalf.so.12 libIex-2_2.so.12 libIexMath-2_2.so.12 libIlmThread-2_2.so.12 libImath-2_2.so.12"
RPM_SONAME_REQ_ilmbase = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_ilmbase = "glibc libgcc libstdc++"
RPM_SONAME_REQ_ilmbase-devel = "libHalf.so.12 libIex-2_2.so.12 libIexMath-2_2.so.12 libIlmThread-2_2.so.12 libImath-2_2.so.12"
RPROVIDES_ilmbase-devel = "ilmbase-dev (= 2.2.0)"
RDEPENDS_ilmbase-devel = "ilmbase libglvnd-devel mesa-libGLU-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ilmbase-2.2.0-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ilmbase-devel-2.2.0-11.el8.x86_64.rpm \
          "

SRC_URI[ilmbase.sha256sum] = "ed2e48ad367e08c5f6d0e79c405ff38967506895c0e39209d301d56529f6ddaa"
SRC_URI[ilmbase-devel.sha256sum] = "51d0541f4233f2b70531c31a8c5e47ea84a2661d84d5351d140c5c9526e68a6a"
