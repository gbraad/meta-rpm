SUMMARY = "generated recipe based on libconfig srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libconfig = "libconfig++.so.9 libconfig.so.9"
RPM_SONAME_REQ_libconfig = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libconfig = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libconfig-devel = "libconfig++.so.9 libconfig.so.9"
RPROVIDES_libconfig-devel = "libconfig-dev (= 1.5)"
RDEPENDS_libconfig-devel = "bash info libconfig pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libconfig-1.5-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libconfig-devel-1.5-9.el8.x86_64.rpm \
          "

SRC_URI[libconfig.sha256sum] = "a4a2c7c0e2f454abae61dddbf4286a0b3617a8159fd20659bddbcedd8eaaa80c"
SRC_URI[libconfig-devel.sha256sum] = "b2b1cb9863c88110f2620cf982f5640f33225936115c634e91b4f77b5a66d918"
