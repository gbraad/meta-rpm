SUMMARY = "generated recipe based on fprintd srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-glib dbus-libs glib-2.0 libfprint libgcc pam pkgconfig-native polkit"
RPM_SONAME_REQ_fprintd = "libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libfprint.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0"
RDEPENDS_fprintd = "dbus-glib dbus-libs glib2 glibc libfprint libgcc polkit-libs"
RPM_SONAME_REQ_fprintd-pam = "libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libpam.so.0"
RDEPENDS_fprintd-pam = "authselect-compat bash dbus-glib dbus-libs fprintd glib2 glibc pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fprintd-1.90.0-0.20191121gitf022902.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/fprintd-pam-1.90.0-0.20191121gitf022902.el8.x86_64.rpm \
          "

SRC_URI[fprintd.sha256sum] = "e326438f01adf1cd24844542f5682531938e17027606f1238f22495f31826763"
SRC_URI[fprintd-pam.sha256sum] = "cefdbf0655294ec9ce7b510f63e3dfdc2f46637ae2ae434f3f034a9f9399e3fe"
