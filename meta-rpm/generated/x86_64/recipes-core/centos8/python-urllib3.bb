SUMMARY = "generated recipe based on python-urllib3 srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-urllib3 = "ca-certificates platform-python python3-pysocks python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-urllib3-1.24.2-4.el8.noarch.rpm \
          "

SRC_URI[python3-urllib3.sha256sum] = "8ad17175e3284a002b0a4ddca6d7d1e612ae7b7ab847fa491d16874be837ffae"
