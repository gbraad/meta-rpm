SUMMARY = "generated recipe based on hdparm srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_hdparm = "libc.so.6"
RDEPENDS_hdparm = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/hdparm-9.54-2.el8.x86_64.rpm \
          "

SRC_URI[hdparm.sha256sum] = "ab5da35c911ef902526a11b7e04604732b1e1733213ea061e320615007632dc1"
