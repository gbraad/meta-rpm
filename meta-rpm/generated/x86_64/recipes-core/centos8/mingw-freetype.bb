SUMMARY = "generated recipe based on mingw-freetype srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED | GPL-2.0"
RPM_LICENSE = "FTL or GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-freetype = "mingw32-bzip2 mingw32-crt mingw32-filesystem mingw32-libpng mingw32-pkg-config mingw32-zlib"
RDEPENDS_mingw32-freetype-static = "mingw32-freetype"
RDEPENDS_mingw64-freetype = "mingw64-bzip2 mingw64-crt mingw64-filesystem mingw64-libpng mingw64-pkg-config mingw64-zlib"
RDEPENDS_mingw64-freetype-static = "mingw64-freetype"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-freetype-2.8-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-freetype-static-2.8-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-freetype-2.8-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-freetype-static-2.8-3.el8.noarch.rpm \
          "

SRC_URI[mingw32-freetype.sha256sum] = "a24635fa105f78021acdb07d67fa75db37c72bfaabbd18a49b33a3ded7b32b4e"
SRC_URI[mingw32-freetype-static.sha256sum] = "fea02b4120c06f4ca945c6150f9e7716042612e05c990e40dd3ca5435ff8245f"
SRC_URI[mingw64-freetype.sha256sum] = "3265ff0edade8f83feea320cd38ee845b25699965c6d1d164657243c3f534ec1"
SRC_URI[mingw64-freetype-static.sha256sum] = "c640e63321f2a312a366e819f5cf1fe5116cfc9872baed021575d97cf9a7bf01"
