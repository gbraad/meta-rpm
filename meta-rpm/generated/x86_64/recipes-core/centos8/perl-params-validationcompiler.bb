SUMMARY = "generated recipe based on perl-Params-ValidationCompiler srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0"
RPM_LICENSE = "Artistic 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Params-ValidationCompiler = "perl-Carp perl-Eval-Closure perl-Exception-Class perl-Exporter perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Params-ValidationCompiler-0.27-1.el8.noarch.rpm \
          "

SRC_URI[perl-Params-ValidationCompiler.sha256sum] = "e5f776a979e71b0cb6866aece89fbe3dcc6043cc4d0902dd3f9b8294565b445e"
