SUMMARY = "generated recipe based on perl-IO-Socket-INET6 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-Socket-INET6 = "perl-Carp perl-Errno perl-IO perl-Socket perl-Socket6 perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-IO-Socket-INET6-2.72-12.el8.noarch.rpm \
          "

SRC_URI[perl-IO-Socket-INET6.sha256sum] = "89a59ba1db080c02dad27d435d09439447662e972ad51d46ebfc0c01372db047"
