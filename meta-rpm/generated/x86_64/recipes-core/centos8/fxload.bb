SUMMARY = "generated recipe based on fxload srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_fxload = "libc.so.6"
RDEPENDS_fxload = "glibc systemd-udev"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fxload-2008_10_13-10.el8.x86_64.rpm \
          "

SRC_URI[fxload.sha256sum] = "de5b1a025910415516b851755eba0e58435e7af7d39fcd10d7ed261c7443a770"
