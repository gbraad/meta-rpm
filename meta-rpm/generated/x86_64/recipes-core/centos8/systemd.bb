SUMMARY = "generated recipe based on systemd srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "udev"
DEPENDS = "acl audit-libs bzip2 cryptsetup-libs curl dbus-libs elfutils glib-2.0 gnutls-libs iptables kmod libblkid libcap libgcc libgcrypt libgpg-error libidn2 libmicrohttpd libmount libpcre2 libseccomp libselinux libxcrypt lz4 pam pkgconfig-native systemd-libs xz zlib"
RPM_SONAME_PROV_systemd = "libsystemd-shared-239.so"
RPM_SONAME_REQ_systemd = "ld-linux-x86-64.so.2 libacl.so.1 libaudit.so.1 libblkid.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypt.so.1 libcryptsetup.so.12 libdl.so.2 libdw.so.1 libelf.so.1 libgcc_s.so.1 libgcrypt.so.20 libgnutls.so.30 libgpg-error.so.0 libidn2.so.0 libip4tc.so.2 libkmod.so.2 liblz4.so.1 liblzma.so.5 libm.so.6 libmount.so.1 libpam.so.0 libpcre2-8.so.0 libpthread.so.0 librt.so.1 libseccomp.so.2 libselinux.so.1 libz.so.1"
RDEPENDS_systemd = "acl audit-libs bash bzip2-libs coreutils cryptsetup-libs dbus elfutils-libelf elfutils-libs glibc glibc-common gnutls grep iptables-libs kmod-libs libacl libblkid libcap libgcc libgcrypt libgpg-error libidn2 libmount libseccomp libselinux libxcrypt lz4-libs pam pcre2 sed shadow-utils systemd-libs systemd-pam util-linux xz-libs zlib"
RPM_SONAME_PROV_systemd-container = "libnss_mymachines.so.2"
RPM_SONAME_REQ_systemd-container = "ld-linux-x86-64.so.2 libacl.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcurl.so.4 libgcc_s.so.1 libgcrypt.so.20 liblzma.so.5 libmount.so.1 libpthread.so.0 librt.so.1 libseccomp.so.2 libselinux.so.1 libsystemd-shared-239.so libz.so.1"
RDEPENDS_systemd-container = "bzip2-libs glibc libacl libcap libcurl libgcc libgcrypt libmount libseccomp libselinux systemd xz-libs zlib"
RPM_SONAME_REQ_systemd-devel = "libsystemd.so.0 libudev.so.1"
RPROVIDES_systemd-devel = "systemd-dev (= 239)"
RDEPENDS_systemd-devel = "systemd-libs systemd-pam"
RPM_SONAME_REQ_systemd-journal-remote = "libc.so.6 libcurl.so.4 libgcc_s.so.1 libgnutls.so.30 libmicrohttpd.so.12 libpthread.so.0 libsystemd-shared-239.so"
RDEPENDS_systemd-journal-remote = "bash firewalld-filesystem glibc glibc-common gnutls libcurl libgcc libmicrohttpd systemd"
RPM_SONAME_REQ_systemd-pam = "ld-linux-x86-64.so.2 libc.so.6 libcap.so.2 libgcc_s.so.1 libmount.so.1 libpam.so.0 libpam_misc.so.0 libpthread.so.0 librt.so.1"
RDEPENDS_systemd-pam = "glibc libcap libgcc libmount pam systemd"
RPM_SONAME_REQ_systemd-tests = "ld-linux-x86-64.so.2 libacl.so.1 libaudit.so.1 libblkid.so.1 libc.so.6 libcap.so.2 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libgcrypt.so.20 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libkmod.so.2 liblz4.so.1 libm.so.6 libmount.so.1 libpam.so.0 libpthread.so.0 librt.so.1 libseccomp.so.2 libselinux.so.1 libsystemd-shared-239.so libsystemd.so.0 libudev.so.1 libz.so.1"
RDEPENDS_systemd-tests = "audit-libs dbus-libs glib2 glibc kmod-libs libacl libblkid libcap libgcc libgcrypt libgpg-error libmount libseccomp libselinux lz4-libs pam systemd systemd-libs zlib"
RPM_SONAME_REQ_systemd-udev = "libacl.so.1 libblkid.so.1 libc.so.6 libcryptsetup.so.12 libgcc_s.so.1 libkmod.so.2 libpthread.so.0 libsystemd-shared-239.so"
RPROVIDES_systemd-udev = "udev (= 239)"
RDEPENDS_systemd-udev = "bash cryptsetup-libs glibc grep kmod kmod-libs libacl libblkid libgcc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/systemd-239-31.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/systemd-container-239-31.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/systemd-devel-239-31.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/systemd-journal-remote-239-31.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/systemd-pam-239-31.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/systemd-tests-239-31.el8_2.2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/systemd-udev-239-31.el8_2.2.x86_64.rpm \
          "

SRC_URI[systemd.sha256sum] = "80d2799020796ef45087c4d8b70fa16d266e52f6db496e50f30bfc41b7fa8345"
SRC_URI[systemd-container.sha256sum] = "f7b48b797a200fe54ef3a525489a9b9dfa69206c72455634fc73f64d6ebaeaf3"
SRC_URI[systemd-devel.sha256sum] = "703a19dddf4131cebc6f3a64386e5c86d984e2e34f972cdbdab86a492eeed973"
SRC_URI[systemd-journal-remote.sha256sum] = "731e8f44374c5d13022814518978db74f8a6beb34a1641661f523fdd9be93bdf"
SRC_URI[systemd-pam.sha256sum] = "989592241cbb0bf9945455cba1aa807bf5db3aa9796c04b24c2a6c5b886ad2d3"
SRC_URI[systemd-tests.sha256sum] = "ab0a274f7c2240d14a23c1b27b3ee83dac7ff4908ac95af7072e4fb87d245f5a"
SRC_URI[systemd-udev.sha256sum] = "dcc023bee498da55c94e520fff057d3a0ab251eb67322426fff84a6f3e707005"
