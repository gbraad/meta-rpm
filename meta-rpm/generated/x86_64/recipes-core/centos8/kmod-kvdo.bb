SUMMARY = "generated recipe based on kmod-kvdo srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_kmod-kvdo = "bash kernel kmod"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kmod-kvdo-6.2.2.117-65.el8.x86_64.rpm \
          "

SRC_URI[kmod-kvdo.sha256sum] = "840ab02a50aecec0722a91c7e08fb7f543b972667d34ae4172b5b9dcec30413d"
