SUMMARY = "generated recipe based on foomatic srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxml2 pkgconfig-native xz zlib"
RPM_SONAME_REQ_foomatic = "libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_foomatic = "bash colord coreutils cups cups-filters dbus foomatic-db ghostscript glibc libxml2 perl-Data-Dumper perl-Encode perl-Exporter perl-Getopt-Long perl-PathTools perl-interpreter perl-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/foomatic-4.0.12-23.el8.x86_64.rpm \
          "

SRC_URI[foomatic.sha256sum] = "7c106d5747f14f6de16079c68887db79c2b894efa43a9e0b0da524a65dc1769f"
