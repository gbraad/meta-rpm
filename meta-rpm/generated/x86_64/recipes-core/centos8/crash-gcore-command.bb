SUMMARY = "generated recipe based on crash-gcore-command srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_crash-gcore-command = "libc.so.6"
RDEPENDS_crash-gcore-command = "crash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/crash-gcore-command-1.3.1-4.el8.x86_64.rpm \
          "

SRC_URI[crash-gcore-command.sha256sum] = "5a6f2684fa258244b2212c3599e8854d821b8df2b93cd770fc42ec33c0c0a606"
