SUMMARY = "generated recipe based on perl-DateTime-Format-Builder srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0 & (GPL-2.0 | Artistic-1.0)"
RPM_LICENSE = "Artistic 2.0 and (GPL+ or Artistic)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-Format-Builder = "perl-Carp perl-Class-Factory-Util perl-DateTime perl-DateTime-Format-Strptime perl-Params-Validate perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-DateTime-Format-Builder-0.8100-15.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-Format-Builder.sha256sum] = "a16ceaa32e5b75b9b721312cccfb651e144f763f5d84897de2f3cd0c448782f3"
