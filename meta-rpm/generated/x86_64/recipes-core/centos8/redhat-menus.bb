SUMMARY = "generated recipe based on redhat-menus srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_redhat-menus = "bash"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redhat-menus-12.0.2-12.el8.noarch.rpm \
          "

SRC_URI[redhat-menus.sha256sum] = "2ed86255c30bf6fcc921c496d9ca046e2f5e6d8c3eb3d2119ca48cf2a86c8bb5"
