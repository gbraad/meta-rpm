SUMMARY = "generated recipe based on perl-Module-Install-ReadmeFromPod srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Install-ReadmeFromPod = "perl-Capture-Tiny perl-IO-All perl-Module-Install perl-Pod-Html perl-Pod-Markdown perl-interpreter perl-libs perl-podlators"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Module-Install-ReadmeFromPod-0.30-4.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Install-ReadmeFromPod.sha256sum] = "df8eadaba086ab92b2c14519bbd181a7960b90b4b82bc3f33622e638648a090e"
