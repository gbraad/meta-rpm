SUMMARY = "generated recipe based on mingw-gstreamer1 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-gstreamer1 = "mingw32-crt mingw32-filesystem mingw32-gcc mingw32-gettext mingw32-glib2 mingw32-pkg-config mingw32-winpthreads"
RDEPENDS_mingw64-gstreamer1 = "mingw64-crt mingw64-filesystem mingw64-gcc mingw64-gettext mingw64-glib2 mingw64-pkg-config mingw64-winpthreads"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-gstreamer1-1.14.1-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-gstreamer1-1.14.1-2.el8.noarch.rpm \
          "

SRC_URI[mingw32-gstreamer1.sha256sum] = "0791b9cc05737c8ed2cda6dc0d4b276733acb65cc2592d9bcf1590b9220743ca"
SRC_URI[mingw64-gstreamer1.sha256sum] = "92973b733f9752c311b21928d7188e9a5c9069cdb38a5f2b77d7c91a00fb7efc"
