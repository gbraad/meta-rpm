SUMMARY = "generated recipe based on libguestfs-winsupport srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_libguestfs-winsupport = "libguestfs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libguestfs-winsupport-8.0-4.module_el8.2.0+320+13f867d7.x86_64.rpm \
          "

SRC_URI[libguestfs-winsupport.sha256sum] = "a077cf5772c5e1f1356663997e82bb354dbd9bbf38a5986dac32fa555a15a6f1"
