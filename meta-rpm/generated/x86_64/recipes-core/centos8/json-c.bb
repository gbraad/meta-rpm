SUMMARY = "generated recipe based on json-c srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_json-c = "libjson-c.so.4"
RPM_SONAME_REQ_json-c = "ld-linux-x86-64.so.2 libc.so.6"
RDEPENDS_json-c = "glibc"
RPM_SONAME_REQ_json-c-devel = "libjson-c.so.4"
RPROVIDES_json-c-devel = "json-c-dev (= 0.13.1)"
RDEPENDS_json-c-devel = "json-c pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/json-c-0.13.1-0.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/json-c-devel-0.13.1-0.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/json-c-doc-0.13.1-0.2.el8.noarch.rpm \
          "

SRC_URI[json-c.sha256sum] = "2cc5d8593583fa7d474dd6beef46e229a06a50ff5c132040a188fb573eec0a95"
SRC_URI[json-c-devel.sha256sum] = "d0696c5f8f1934764461c5f57b99674da178646c7b2ae00f5524e6718e23002d"
SRC_URI[json-c-doc.sha256sum] = "11bcee8c836e2108fca403808472fb66a8e3fb37eacff061abab366d4ddd9c65"
