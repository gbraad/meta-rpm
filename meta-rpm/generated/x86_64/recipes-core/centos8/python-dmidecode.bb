SUMMARY = "generated recipe based on python-dmidecode srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxml2 pkgconfig-native platform-python3 xz zlib"
RPM_SONAME_REQ_python3-dmidecode = "libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libxml2.so.2 libxml2mod.so libz.so.1"
RDEPENDS_python3-dmidecode = "glibc libxml2 platform-python python3-libs python3-libxml2 xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-dmidecode-3.12.2-15.el8.x86_64.rpm \
          "

SRC_URI[python3-dmidecode.sha256sum] = "c39a53566ad66d6db9e101ca9a6db316843a1d1f6f5ac5f2286c6028638a8ab7"
