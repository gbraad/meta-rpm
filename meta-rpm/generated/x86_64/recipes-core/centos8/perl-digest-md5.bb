SUMMARY = "generated recipe based on perl-Digest-MD5 srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & BSD"
RPM_LICENSE = "(GPL+ or Artistic) and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Digest-MD5 = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Digest-MD5 = "glibc perl-Digest perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Digest-MD5-2.55-396.el8.x86_64.rpm \
          "

SRC_URI[perl-Digest-MD5.sha256sum] = "41d5ae2f1b8516aab4fe6e42c85ed8af2959f0a1e7993f21856e86042feefa3c"
