SUMMARY = "generated recipe based on sos srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_sos = "bzip2 platform-python python3-libxml2 python3-six xz"
RDEPENDS_sos-audit = "bash"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sos-3.8-7.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sos-audit-3.8-7.el8_2.noarch.rpm \
          "

SRC_URI[sos.sha256sum] = "19657a9e55148d03bef42b0ffc5c9a5a332e54c6ba8560b418d581d9929b5bc4"
SRC_URI[sos-audit.sha256sum] = "d47723b109252b8e71f5a5e582eb5c1bc07decd453b42c6a31c1d092b0f9ae50"
