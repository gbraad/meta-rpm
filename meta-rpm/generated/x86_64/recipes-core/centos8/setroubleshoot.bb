SUMMARY = "generated recipe based on setroubleshoot srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs dbus-libs libcap-ng libselinux pkgconfig-native"
RDEPENDS_setroubleshoot = "dbus desktop-file-utils gtk3 libnotify libreport-gtk platform-python python3-gobject python3-libreport python3-pydbus setroubleshoot-server xdg-utils"
RPM_SONAME_REQ_setroubleshoot-server = "libauparse.so.0 libc.so.6 libcap-ng.so.0 libdbus-1.so.3 libselinux.so.1"
RDEPENDS_setroubleshoot-server = "audit audit-libs bash dbus dbus-libs glibc initscripts libcap-ng libselinux platform-python policycoreutils-python-utils polkit python3-audit python3-dbus python3-gobject python3-libselinux python3-libxml2 python3-rpm python3-slip-dbus python3-systemd setroubleshoot-plugins shadow-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/setroubleshoot-3.3.22-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/setroubleshoot-server-3.3.22-2.el8.x86_64.rpm \
          "

SRC_URI[setroubleshoot.sha256sum] = "781cc799af62c75a37478fc3110f79641cdd9adbc65b7de777d88c6b1e2d2568"
SRC_URI[setroubleshoot-server.sha256sum] = "830464042bdf1943e982ef84f64bcbd8a8c2079455a4dd0c95278fcbefee5d61"
