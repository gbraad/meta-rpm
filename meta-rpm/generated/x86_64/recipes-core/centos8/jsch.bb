SUMMARY = "generated recipe based on jsch srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jsch = "java-1.8.0-openjdk-headless javapackages-filesystem jzlib"
RDEPENDS_jsch-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jsch-0.1.54-6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jsch-javadoc-0.1.54-6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jsch.sha256sum] = "36b651d30379008fa0be4c641b2895eb2676aa457d57072e8fc720798794ced5"
SRC_URI[jsch-javadoc.sha256sum] = "db9a1272e77495cb70fc6f3cbfb25a546883226a60d72a5e82108a1d94dd861d"
