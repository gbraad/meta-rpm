SUMMARY = "generated recipe based on libmaxminddb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & BSD"
RPM_LICENSE = "ASL 2.0 and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmaxminddb = "libmaxminddb.so.0"
RPM_SONAME_REQ_libmaxminddb = "libc.so.6"
RDEPENDS_libmaxminddb = "glibc"
RPM_SONAME_REQ_libmaxminddb-devel = "libc.so.6 libm.so.6 libmaxminddb.so.0"
RPROVIDES_libmaxminddb-devel = "libmaxminddb-dev (= 1.2.0)"
RDEPENDS_libmaxminddb-devel = "glibc libmaxminddb pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmaxminddb-1.2.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmaxminddb-devel-1.2.0-7.el8.x86_64.rpm \
          "

SRC_URI[libmaxminddb.sha256sum] = "e471366ad3cb10727abdb4f9a1e084216e6e676525e2db2aee08c8dca80742be"
SRC_URI[libmaxminddb-devel.sha256sum] = "dc20c9e7872e2e58aede43450d51aba6c56bf96c3a93a3c853a793f813600a6f"
