SUMMARY = "generated recipe based on hyphen-sa srpm"
DESCRIPTION = "Description"
LICENSE = "LPPL-1.0"
RPM_LICENSE = "LPPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-sa = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-sa-0.20110915-13.el8.noarch.rpm \
          "

SRC_URI[hyphen-sa.sha256sum] = "104f184ed2fe094006f8016c79f1a1547fe4e3fb040c7e8e56f439844efcd127"
