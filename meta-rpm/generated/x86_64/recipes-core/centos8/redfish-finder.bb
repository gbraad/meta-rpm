SUMMARY = "generated recipe based on redfish-finder srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_redfish-finder = "NetworkManager bash dmidecode platform-python python36 systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/redfish-finder-0.3-4.el8.noarch.rpm \
          "

SRC_URI[redfish-finder.sha256sum] = "201f1d7d180a72b7064b8bd38c71ea31d31b1b45ce66a1bafdad14c6b5348410"
