SUMMARY = "generated recipe based on perftest srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | BSD"
RPM_LICENSE = "GPLv2 or BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native rdma-core"
RPM_SONAME_REQ_perftest = "libc.so.6 libibumad.so.3 libibverbs.so.1 libm.so.6 libpthread.so.0 librdmacm.so.1"
RDEPENDS_perftest = "glibc libibumad libibverbs librdmacm"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perftest-4.2-2.el8.x86_64.rpm \
          "

SRC_URI[perftest.sha256sum] = "e0c7cba27102ac4096341cba06d476ebbcb3a759b9b4a3ace54f75a34afaedab"
