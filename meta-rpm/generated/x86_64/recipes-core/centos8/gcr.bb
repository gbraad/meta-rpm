SUMMARY = "generated recipe based on gcr srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libgcrypt libgpg-error p11-kit pango pkgconfig-native"
RPM_SONAME_PROV_gcr = "libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1"
RPM_SONAME_REQ_gcr = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdl.so.2 libgcrypt.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libp11-kit.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_gcr = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libgcrypt libgpg-error p11-kit pango"
RPM_SONAME_REQ_gcr-devel = "libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1"
RPROVIDES_gcr-devel = "gcr-dev (= 3.28.0)"
RDEPENDS_gcr-devel = "gcr glib2-devel gtk3-devel p11-kit-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcr-3.28.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcr-devel-3.28.0-1.el8.x86_64.rpm \
          "

SRC_URI[gcr.sha256sum] = "c17db9834c01fd17da226ce387fb9a616e67ac28e4625724adc89ca52e2497a4"
SRC_URI[gcr-devel.sha256sum] = "ebea91f4b3ded70f2b73c049c06a2e04071bf97444454fe5f3225e30c2222db5"
