SUMMARY = "generated recipe based on cdrkit srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 cdparanoia file libcap pkgconfig-native zlib"
RDEPENDS_dirsplit = "genisoimage perl-File-Path perl-Getopt-Long perl-PathTools perl-Scalar-List-Utils perl-interpreter perl-libs"
RPM_SONAME_REQ_genisoimage = "libbz2.so.1 libc.so.6 libmagic.so.1 libpthread.so.0 librols.so.0 libusal.so.0 libz.so.1"
RDEPENDS_genisoimage = "bash bzip2-libs chkconfig coreutils file-libs glibc libusal zlib"
RPM_SONAME_REQ_icedax = "libc.so.6 libcdda_interface.so.0 libcdda_paranoia.so.0 librols.so.0 libusal.so.0"
RDEPENDS_icedax = "bash cdparanoia cdparanoia-libs chkconfig coreutils glibc libusal vorbis-tools"
RPM_SONAME_PROV_libusal = "librols.so.0 libusal.so.0"
RPM_SONAME_REQ_libusal = "libc.so.6"
RDEPENDS_libusal = "glibc"
RPM_SONAME_REQ_wodim = "libc.so.6 libcap.so.2 libpthread.so.0 librols.so.0 libusal.so.0"
RDEPENDS_wodim = "bash chkconfig coreutils glibc libcap libusal"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dirsplit-1.1.11-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/genisoimage-1.1.11-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/icedax-1.1.11-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libusal-1.1.11-39.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/wodim-1.1.11-39.el8.x86_64.rpm \
          "

SRC_URI[dirsplit.sha256sum] = "5111c816ec8918fe1d635713fd2acdde61ac723c3c2741ee399fc7bce863961a"
SRC_URI[genisoimage.sha256sum] = "f98e67e6ed49e1ff2f4c1d8dea7aa139aaff69020013e458d2f3d8bd9d2c91b2"
SRC_URI[icedax.sha256sum] = "2cd1c87d57f622e9f74b8e68c75e7b9a247f05bb9ad778b72325632dac28bd7d"
SRC_URI[libusal.sha256sum] = "0b2b79d9f8cd01090816386ad89852662b5489bbd43fbd04760f0e57c28bce4c"
SRC_URI[wodim.sha256sum] = "daa25adbdf03383b8d48f1c623082cb17aea01acde2b9ec6e4d9afe1a0eff893"
