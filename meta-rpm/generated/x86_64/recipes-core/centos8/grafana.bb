SUMMARY = "generated recipe based on grafana srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_grafana = "libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_grafana = "bash glibc shadow-utils systemd"
RDEPENDS_grafana-azure-monitor = "grafana"
RDEPENDS_grafana-cloudwatch = "grafana"
RDEPENDS_grafana-elasticsearch = "grafana"
RDEPENDS_grafana-graphite = "grafana"
RDEPENDS_grafana-influxdb = "grafana"
RDEPENDS_grafana-loki = "grafana"
RDEPENDS_grafana-mssql = "grafana"
RDEPENDS_grafana-mysql = "grafana"
RDEPENDS_grafana-opentsdb = "grafana"
RDEPENDS_grafana-postgres = "grafana"
RDEPENDS_grafana-prometheus = "grafana"
RDEPENDS_grafana-stackdriver = "grafana"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-azure-monitor-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-cloudwatch-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-elasticsearch-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-graphite-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-influxdb-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-loki-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-mssql-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-mysql-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-opentsdb-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-postgres-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-prometheus-6.3.6-2.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/grafana-stackdriver-6.3.6-2.el8_2.x86_64.rpm \
          "

SRC_URI[grafana.sha256sum] = "13933da67131105faf8c6b77fefb9ac96b2db63edc137592b1475ba022eb5f9a"
SRC_URI[grafana-azure-monitor.sha256sum] = "50656457cd7883299baa7aa2501ae62e16cea0575ffd887879b5aefd59903cd3"
SRC_URI[grafana-cloudwatch.sha256sum] = "f72fac4a6c2f20af3d927421912c22890b1d2a3ce2374ac27cdbfc261f1b6435"
SRC_URI[grafana-elasticsearch.sha256sum] = "42eadcd8498c3242cf37c9f8342339e0269e9ceaf7be7aea9a722480332948ae"
SRC_URI[grafana-graphite.sha256sum] = "17dccfd81f56da40d2daf222046bc191399024ea64055f7c89ad14fa1ce3cfe0"
SRC_URI[grafana-influxdb.sha256sum] = "ab9dcb34001b2a4c075ea3b5d2186773042ba136f19ef8598e7f25a7357bc3db"
SRC_URI[grafana-loki.sha256sum] = "fb5d7cc42118c86bcebc021df55349f776ec032e63fbd47d2872476ccee091e8"
SRC_URI[grafana-mssql.sha256sum] = "4a0a14c487a148b4c0f93e4174ff3ef688220238d507b92162b72a1b19c7d2a7"
SRC_URI[grafana-mysql.sha256sum] = "84874c47df9eed7164da22f33fdcfc748bdbf30d4cea882f092ba13ea5d26889"
SRC_URI[grafana-opentsdb.sha256sum] = "e78fd3a89fe718dc9c7c83f8c4bb66d7db6b2fe983b144239f232c109efd0a19"
SRC_URI[grafana-postgres.sha256sum] = "6d64046228719ce098a9b8e3a14225e38a8380a8e04da487aeb2363c8801065d"
SRC_URI[grafana-prometheus.sha256sum] = "3394813d07d6f5f1838d4f79058b82da03c9ce1ee977b61fe1b5637a371e27f3"
SRC_URI[grafana-stackdriver.sha256sum] = "3576fae3d5d08dc42f78181681d444644adc4689263e0f78bebfe03eb246327c"
