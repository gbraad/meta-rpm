SUMMARY = "generated recipe based on libmpc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0 & GFDL-1.1"
RPM_LICENSE = "LGPLv3+ and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gmp mpfr pkgconfig-native"
RPM_SONAME_PROV_libmpc = "libmpc.so.3"
RPM_SONAME_REQ_libmpc = "libc.so.6 libgmp.so.10 libm.so.6 libmpfr.so.4"
RDEPENDS_libmpc = "glibc gmp mpfr"
RPM_SONAME_REQ_libmpc-devel = "libmpc.so.3"
RPROVIDES_libmpc-devel = "libmpc-dev (= 1.0.2)"
RDEPENDS_libmpc-devel = "bash gmp-devel libmpc mpfr-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmpc-1.0.2-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmpc-devel-1.0.2-9.el8.x86_64.rpm \
          "

SRC_URI[libmpc.sha256sum] = "93569a603e41ccd8f59682518a1318464857229ac3a2e1c7e7e5413e3c988b90"
SRC_URI[libmpc-devel.sha256sum] = "84c657ab9b3a9effb7b36c8ca2012b3bed6012ba5c0130c0fd0dd46c58862be3"
