SUMMARY = "generated recipe based on qt5-qtsensors srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtsensors = "libQt5Sensors.so.5"
RPM_SONAME_REQ_qt5-qtsensors = "libGL.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_qt5-qtsensors = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtsensors-devel = "libQt5Sensors.so.5"
RPROVIDES_qt5-qtsensors-devel = "qt5-qtsensors-dev (= 5.12.5)"
RDEPENDS_qt5-qtsensors-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtsensors"
RPM_SONAME_PROV_qt5-qtsensors-examples = "libdeclarative_explorer.so libdeclarative_grue.so libgruesensor.so.1 libqtsensors_grue.so"
RPM_SONAME_REQ_qt5-qtsensors-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Sensors.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtsensors-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtsensors"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtsensors-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtsensors-devel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtsensors-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtsensors.sha256sum] = "9125518d22369e21e54dd53dc5a857ae824f2954bb78f8e5d8eac4587f607f1a"
SRC_URI[qt5-qtsensors-devel.sha256sum] = "f82977e66de337b54c6f52b88984bb2a1e8021eb85ac6aaf279c2f31a8f48d1b"
SRC_URI[qt5-qtsensors-examples.sha256sum] = "b508b5bf04732cb53e184ce883ca6f272cdbf83663a92f7d47b838a8ba6f2b74"
