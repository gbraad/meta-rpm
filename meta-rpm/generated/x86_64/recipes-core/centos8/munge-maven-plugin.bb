SUMMARY = "generated recipe based on munge-maven-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CDDL-1.0"
RPM_LICENSE = "CDDL-1.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_munge-maven-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib"
RDEPENDS_munge-maven-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/munge-maven-plugin-1.0-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/munge-maven-plugin-javadoc-1.0-11.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[munge-maven-plugin.sha256sum] = "8d82c19728be97d5b0bb22b69577924c08f902715a4f1acc737052d0407b422c"
SRC_URI[munge-maven-plugin-javadoc.sha256sum] = "3c3584763a3e419487ea3b0cae3b8b5a8c0908459c456398492b24eb30c7ddc6"
