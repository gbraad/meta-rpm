SUMMARY = "generated recipe based on usbutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libusb1 pkgconfig-native systemd-libs"
RPM_SONAME_REQ_usbutils = "libc.so.6 libudev.so.1 libusb-1.0.so.0"
RDEPENDS_usbutils = "bash glibc hwdata libusbx platform-python systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/usbutils-010-3.el8.x86_64.rpm \
          "

SRC_URI[usbutils.sha256sum] = "9c6c29c409d5867a39c376aa5990650a90ff08748b30d79d85b0792368a5e426"
