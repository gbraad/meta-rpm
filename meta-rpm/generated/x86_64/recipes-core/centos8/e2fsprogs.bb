SUMMARY = "generated recipe based on e2fsprogs srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "fuse libblkid libuuid pkgconfig-native"
RPM_SONAME_REQ_e2fsprogs = "libblkid.so.1 libc.so.6 libcom_err.so.2 libdl.so.2 libe2p.so.2 libext2fs.so.2 libfuse.so.2 libss.so.2 libuuid.so.1"
RDEPENDS_e2fsprogs = "e2fsprogs-libs fuse-libs glibc libblkid libcom_err libss libuuid"
RPM_SONAME_REQ_e2fsprogs-devel = "libe2p.so.2 libext2fs.so.2"
RPROVIDES_e2fsprogs-devel = "e2fsprogs-dev (= 1.45.4)"
RDEPENDS_e2fsprogs-devel = "bash e2fsprogs-libs gawk info libcom_err-devel pkgconf-pkg-config"
RPM_SONAME_PROV_e2fsprogs-libs = "libe2p.so.2 libext2fs.so.2"
RPM_SONAME_REQ_e2fsprogs-libs = "libc.so.6 libcom_err.so.2"
RDEPENDS_e2fsprogs-libs = "glibc libcom_err"
RPM_SONAME_PROV_libcom_err = "libcom_err.so.2"
RPM_SONAME_REQ_libcom_err = "ld-linux-x86-64.so.2 libc.so.6 libpthread.so.0"
RDEPENDS_libcom_err = "glibc"
RPM_SONAME_REQ_libcom_err-devel = "libcom_err.so.2"
RPROVIDES_libcom_err-devel = "libcom_err-dev (= 1.45.4)"
RDEPENDS_libcom_err-devel = "bash libcom_err pkgconf-pkg-config"
RPM_SONAME_PROV_libss = "libss.so.2"
RPM_SONAME_REQ_libss = "libc.so.6 libcom_err.so.2 libdl.so.2"
RDEPENDS_libss = "glibc libcom_err"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/e2fsprogs-1.45.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/e2fsprogs-devel-1.45.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/e2fsprogs-libs-1.45.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcom_err-1.45.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcom_err-devel-1.45.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libss-1.45.4-3.el8.x86_64.rpm \
          "

SRC_URI[e2fsprogs.sha256sum] = "9a38b4692f9c2fb81b89ba23654756cf25d9bf9c795790dedeb863f1a0fbb769"
SRC_URI[e2fsprogs-devel.sha256sum] = "1d8199ee5ce285ef5860086253e003e0c51ef986fb21c9668c8c360e2e4d56cb"
SRC_URI[e2fsprogs-libs.sha256sum] = "920fb9cea74b1380ff05bd580728d17123f70ccad0e2596ae7a42e401e4d70f5"
SRC_URI[libcom_err.sha256sum] = "88fe10942ad145fa8e9a18d62ac3f90542ff3ba8bec2ee7fe23b8c289a394c8c"
SRC_URI[libcom_err-devel.sha256sum] = "bdaedb1b19d0c8a4e31bdaad8f87a1c8d6d10add8d00ebc3a85619ec6e582a09"
SRC_URI[libss.sha256sum] = "653debd905563e7b5a2314fa0cd8e905b75e84abdb9165a5d330055c322b22ec"
