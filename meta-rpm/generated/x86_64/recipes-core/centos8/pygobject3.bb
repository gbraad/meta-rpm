SUMMARY = "generated recipe based on pygobject3 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & MIT"
RPM_LICENSE = "LGPLv2+ and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo glib-2.0 gobject-introspection libffi pkgconfig-native"
RPROVIDES_pygobject3-devel = "pygobject3-dev (= 3.28.3)"
RDEPENDS_pygobject3-devel = "glib2-devel gobject-introspection-devel libffi-devel pkgconf-pkg-config python3-gobject"
RPM_SONAME_REQ_python3-gobject = "libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgirepository-1.0.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_python3-gobject = "cairo cairo-gobject glib2 glibc gobject-introspection platform-python python3-cairo python3-gobject-base"
RPM_SONAME_REQ_python3-gobject-base = "libc.so.6 libffi.so.6 libgirepository-1.0.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_python3-gobject-base = "glib2 glibc gobject-introspection libffi platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-gobject-3.28.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-gobject-base-3.28.3-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/pygobject3-devel-3.28.3-1.el8.x86_64.rpm \
          "

SRC_URI[pygobject3-devel.sha256sum] = "f39882040097629bd90a4249ff727c2fba9e2b254e4b4d5388d494a563b6b660"
SRC_URI[python3-gobject.sha256sum] = "5ed7ec6b3bf7c44e668972b7eeb5ea0dda436f822484310379d7e9c82eda37de"
SRC_URI[python3-gobject-base.sha256sum] = "4b178450925489a4635b1ef005523d1b754d3a5c32fe569ffcb7f4d97f6ca6fd"
