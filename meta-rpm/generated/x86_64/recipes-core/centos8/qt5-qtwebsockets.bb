SUMMARY = "generated recipe based on qt5-qtwebsockets srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtwebsockets = "libQt5WebSockets.so.5"
RPM_SONAME_REQ_qt5-qtwebsockets = "libQt5Core.so.5 libQt5Network.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtwebsockets = "glibc libgcc libstdc++ qt5-qtbase"
RPM_SONAME_REQ_qt5-qtwebsockets-devel = "libQt5Core.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5WebSockets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_qt5-qtwebsockets-devel = "qt5-qtwebsockets-dev (= 5.12.5)"
RDEPENDS_qt5-qtwebsockets-devel = "cmake-filesystem glibc libgcc libstdc++ pkgconf-pkg-config qt5-qtbase qt5-qtbase-devel qt5-qtdeclarative qt5-qtwebsockets"
RPM_SONAME_REQ_qt5-qtwebsockets-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5WebSockets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtwebsockets-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtwebsockets"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtwebsockets-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtwebsockets-devel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtwebsockets-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtwebsockets.sha256sum] = "6dead848553762422964c6ad7c6d52619037fb53e3714030430265173255a57b"
SRC_URI[qt5-qtwebsockets-devel.sha256sum] = "e9bd9eab91b7d3843956545eb538b3d0f48ca83f298a3fc28f007b7ed9e7f730"
SRC_URI[qt5-qtwebsockets-examples.sha256sum] = "7247891db5b7e7448603261082cd0eac9c4a225f9c00dd78f41fd5a65b094597"
