SUMMARY = "generated recipe based on ipset srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libmnl pkgconfig-native"
RPM_SONAME_REQ_ipset = "libc.so.6 libipset.so.13"
RDEPENDS_ipset = "bash glibc ipset-libs"
RPM_SONAME_REQ_ipset-devel = "libipset.so.13"
RPROVIDES_ipset-devel = "ipset-dev (= 7.1)"
RDEPENDS_ipset-devel = "ipset-libs libmnl-devel pkgconf-pkg-config"
RPM_SONAME_PROV_ipset-libs = "libipset.so.13"
RPM_SONAME_REQ_ipset-libs = "libc.so.6 libdl.so.2 libmnl.so.0"
RDEPENDS_ipset-libs = "glibc libmnl"
RDEPENDS_ipset-service = "bash ipset iptables-services systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ipset-7.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ipset-libs-7.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ipset-service-7.1-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ipset-devel-7.1-1.el8.x86_64.rpm \
          "

SRC_URI[ipset.sha256sum] = "82e27237686171b812aee7903c11ec4f8dbdb6544e419758fb0fbd61731044b0"
SRC_URI[ipset-devel.sha256sum] = "6cac1fe6cdfb36ca3929d2b7687172ed80ae5462b71d80c3c01cb929014811ce"
SRC_URI[ipset-libs.sha256sum] = "6f699a1e24a6bb76904ef1a588e3b3ef116cad07a5cbad86c6cdb522c00670dd"
SRC_URI[ipset-service.sha256sum] = "2fea3dcc8b91ebda627d9c4da9c93db70fd53a4f062b1923a880f1006ee29516"
