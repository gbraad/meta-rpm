SUMMARY = "generated recipe based on cairo srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | MPL-1.1"
RPM_LICENSE = "LGPLv2 or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "fontconfig freetype glib-2.0 libpng libx11 libxcb libxext libxrender pixman pkgconfig-native zlib"
RPM_SONAME_PROV_cairo = "libcairo-script-interpreter.so.2 libcairo.so.2"
RPM_SONAME_REQ_cairo = "libX11.so.6 libXext.so.6 libXrender.so.1 libc.so.6 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libm.so.6 libpixman-1.so.0 libpng16.so.16 libpthread.so.0 librt.so.1 libxcb-render.so.0 libxcb-shm.so.0 libxcb.so.1 libz.so.1"
RDEPENDS_cairo = "fontconfig freetype glib2 glibc libX11 libXext libXrender libpng libxcb pixman zlib"
RPM_SONAME_REQ_cairo-devel = "libcairo-script-interpreter.so.2 libcairo.so.2"
RPROVIDES_cairo-devel = "cairo-dev (= 1.15.12)"
RDEPENDS_cairo-devel = "cairo fontconfig-devel freetype-devel glib2-devel libX11-devel libXext-devel libXrender-devel libpng-devel libxcb-devel pixman-devel pkgconf-pkg-config"
RPM_SONAME_PROV_cairo-gobject = "libcairo-gobject.so.2"
RPM_SONAME_REQ_cairo-gobject = "libX11.so.6 libXext.so.6 libXrender.so.1 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpixman-1.so.0 libpng16.so.16 libpthread.so.0 librt.so.1 libxcb-render.so.0 libxcb-shm.so.0 libxcb.so.1 libz.so.1"
RDEPENDS_cairo-gobject = "cairo fontconfig freetype glib2 glibc libX11 libXext libXrender libpng libxcb pixman zlib"
RPM_SONAME_REQ_cairo-gobject-devel = "libcairo-gobject.so.2"
RPROVIDES_cairo-gobject-devel = "cairo-gobject-dev (= 1.15.12)"
RDEPENDS_cairo-gobject-devel = "cairo-devel cairo-gobject glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cairo-1.15.12-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cairo-devel-1.15.12-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cairo-gobject-1.15.12-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cairo-gobject-devel-1.15.12-3.el8.x86_64.rpm \
          "

SRC_URI[cairo.sha256sum] = "2fcd7a063cab2e103fd4fdf8f4c63d09b9f3d60759c3b0982c75ed9a9e57bdf8"
SRC_URI[cairo-devel.sha256sum] = "a4471cb4d00ee86cb449ee40a283e5735b19f6c4fd496568856baf5111d46c77"
SRC_URI[cairo-gobject.sha256sum] = "ee4413fda60078f82c3b209557f0fc730871bb2a24cc380db6d01b511322ac85"
SRC_URI[cairo-gobject-devel.sha256sum] = "5ea5dc56ac53e3e2e2fae90b2893a7feac41fea8d4beb565cd66b730253c34c8"
