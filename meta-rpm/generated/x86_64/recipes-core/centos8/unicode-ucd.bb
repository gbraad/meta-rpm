SUMMARY = "generated recipe based on unicode-ucd srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/unicode-ucd-11.0.0-1.el8.noarch.rpm \
          "

SRC_URI[unicode-ucd.sha256sum] = "fb302385185ba0c39e8d13cc886128d90dd2efca807cf053222fff23d8dcdf8e"
