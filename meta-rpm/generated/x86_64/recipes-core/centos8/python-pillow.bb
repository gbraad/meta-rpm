SUMMARY = "generated recipe based on python-pillow srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "freetype lcms2 libjpeg-turbo libwebp openjpeg2 pkgconfig-native platform-python3 tiff zlib"
RPM_SONAME_REQ_python3-pillow = "libc.so.6 libfreetype.so.6 libjpeg.so.62 liblcms2.so.2 libopenjp2.so.7 libpthread.so.0 libpython3.6m.so.1.0 libtiff.so.5 libwebp.so.7 libwebpdemux.so.2 libwebpmux.so.3 libz.so.1"
RDEPENDS_python3-pillow = "freetype glibc lcms2 libjpeg-turbo libtiff libwebp openjpeg2 platform-python python3-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pillow-5.1.1-12.el8_2.x86_64.rpm \
          "

SRC_URI[python3-pillow.sha256sum] = "c47f2f21ea49bc22ba773f3c62b936db2f5f1d882fc16e6cdf97a1a206524e0c"
