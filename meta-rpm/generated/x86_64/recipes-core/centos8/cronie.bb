SUMMARY = "generated recipe based on cronie srpm"
DESCRIPTION = "Description"
LICENSE = "MIT & BSD & ISC & GPL-2.0"
RPM_LICENSE = "MIT and BSD and ISC and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs libselinux pam pkgconfig-native"
RPM_SONAME_REQ_cronie = "libaudit.so.1 libc.so.6 libdl.so.2 libpam.so.0 libselinux.so.1"
RDEPENDS_cronie = "audit-libs bash coreutils cronie-anacron glibc libselinux pam sed systemd"
RPM_SONAME_REQ_cronie-anacron = "libaudit.so.1 libc.so.6 libdl.so.2 libpam.so.0 libselinux.so.1"
RDEPENDS_cronie-anacron = "audit-libs bash coreutils cronie crontabs glibc libselinux pam"
RDEPENDS_cronie-noanacron = "cronie crontabs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cronie-1.5.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cronie-anacron-1.5.2-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cronie-noanacron-1.5.2-4.el8.x86_64.rpm \
          "

SRC_URI[cronie.sha256sum] = "63a6b7765828081cc88b36d10c68621acbebf02a7c2e7d7f1a1217c8742ea6ae"
SRC_URI[cronie-anacron.sha256sum] = "71fcbbec3aa152bc1427cb4d597375b008438f6259b20cfcb68fff21eef80f91"
SRC_URI[cronie-noanacron.sha256sum] = "a49900bf1d1c0823df8a806738aa085ec50c6ebbc2a2b77fc917d5f84e85d008"
