SUMMARY = "generated recipe based on attr srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_attr = "libattr.so.1 libc.so.6"
RDEPENDS_attr = "glibc libattr"
RPM_SONAME_PROV_libattr = "libattr.so.1"
RPM_SONAME_REQ_libattr = "libc.so.6"
RDEPENDS_libattr = "glibc"
RPM_SONAME_REQ_libattr-devel = "libattr.so.1"
RPROVIDES_libattr-devel = "libattr-dev (= 2.4.48)"
RDEPENDS_libattr-devel = "glibc-headers libattr pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/attr-2.4.48-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libattr-2.4.48-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libattr-devel-2.4.48-3.el8.x86_64.rpm \
          "

SRC_URI[attr.sha256sum] = "da1464c73554bd77756428d592f0cb9a8f65604c22c3b3b2b7db14b35f5ad178"
SRC_URI[libattr.sha256sum] = "a02e1344ccde1747501ceeeff37df4f18149fb79b435aa22add08cff6bab3a5a"
SRC_URI[libattr-devel.sha256sum] = "cc850b04377ee9ecd864534cb27e9e4a99dd40b35ce2335bd8d1d3292d38d2c2"
