SUMMARY = "generated recipe based on krb5 srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "e2fsprogs keyutils krb5-libs libselinux libverto openldap openssl pam pkgconfig-native"
RPM_SONAME_REQ_krb5-pkinit = "libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 libkrb5support.so.0 libresolv.so.2"
RDEPENDS_krb5-pkinit = "glibc keyutils-libs krb5-libs libcom_err openssl-libs"
RPM_SONAME_REQ_krb5-server = "libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libgssapi_krb5.so.2 libgssrpc.so.4 libk5crypto.so.3 libkadm5clnt_mit.so.11 libkadm5srv_mit.so.11 libkdb5.so.9 libkeyutils.so.1 libkrad.so.0 libkrb5.so.3 libkrb5support.so.0 libpthread.so.0 libresolv.so.2 libselinux.so.1 libss.so.2 libutil.so.1 libverto.so.1"
RDEPENDS_krb5-server = "bash glibc keyutils-libs krb5-libs libcom_err libkadm5 libselinux libss libverto libverto-libevent logrotate openssl-libs systemd words"
RPM_SONAME_PROV_krb5-server-ldap = "libkdb_ldap.so.1"
RPM_SONAME_REQ_krb5-server-ldap = "libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libgssapi_krb5.so.2 libgssrpc.so.4 libk5crypto.so.3 libkadm5srv_mit.so.11 libkdb5.so.9 libkeyutils.so.1 libkrb5.so.3 libkrb5support.so.0 liblber-2.4.so.2 libldap-2.4.so.2 libpthread.so.0 libresolv.so.2 libselinux.so.1"
RDEPENDS_krb5-server-ldap = "glibc keyutils-libs krb5-libs krb5-server libcom_err libkadm5 libselinux openldap openssl-libs"
RPM_SONAME_REQ_krb5-workstation = "libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libgssapi_krb5.so.2 libgssrpc.so.4 libk5crypto.so.3 libkadm5clnt_mit.so.11 libkadm5srv_mit.so.11 libkdb5.so.9 libkeyutils.so.1 libkrb5.so.3 libkrb5support.so.0 libpam.so.0 libresolv.so.2 libselinux.so.1 libss.so.2"
RDEPENDS_krb5-workstation = "bash glibc keyutils-libs krb5-libs libcom_err libkadm5 libselinux libss openssl-libs pam"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/krb5-pkinit-1.17-18.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/krb5-server-1.17-18.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/krb5-server-ldap-1.17-18.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/krb5-workstation-1.17-18.el8.x86_64.rpm \
          "

SRC_URI[krb5-pkinit.sha256sum] = "2caf0098686641ece88efd4c4a6fe6feac5f48870a143eec72dd544e2850a2b0"
SRC_URI[krb5-server.sha256sum] = "fad8d607134ff4c97ddeac5c578c9067abe3c660cdd36250519336285529b333"
SRC_URI[krb5-server-ldap.sha256sum] = "e46fd08e5f68cefb11e775cab8188e74f644cba7025d83b47048c962b0f95285"
SRC_URI[krb5-workstation.sha256sum] = "f625955d853bf9e83038f7edf47a296410c35e4a81a2698aeba4f3e594c77cbc"
