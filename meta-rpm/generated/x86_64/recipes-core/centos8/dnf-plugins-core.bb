SUMMARY = "generated recipe based on dnf-plugins-core srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_dnf-plugins-core = "python3-dnf-plugins-core"
RDEPENDS_python3-dnf-plugin-post-transaction-actions = "platform-python python3-dnf-plugins-core"
RDEPENDS_python3-dnf-plugin-versionlock = "platform-python python3-dnf-plugins-core"
RDEPENDS_python3-dnf-plugins-core = "platform-python python3-dateutil python3-dnf python3-hawkey"
RDEPENDS_yum-utils = "dnf dnf-plugins-core platform-python python3-dnf"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dnf-plugins-core-4.0.12-4.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-dnf-plugin-post-transaction-actions-4.0.12-4.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-dnf-plugin-versionlock-4.0.12-4.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-dnf-plugins-core-4.0.12-4.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/yum-utils-4.0.12-4.el8_2.noarch.rpm \
          "

SRC_URI[dnf-plugins-core.sha256sum] = "19a220d0e5f2089a679dffd6dfbb9598ef0e8ce86b429e385ccbeec34cd298dd"
SRC_URI[python3-dnf-plugin-post-transaction-actions.sha256sum] = "99ef8678ff4fee92b50b2baf7a76302b327606fd8eb82e3b2f4d87463f29979b"
SRC_URI[python3-dnf-plugin-versionlock.sha256sum] = "3d82e7fa0fc150a71c9034c58dbd0cc0bfba1ba5e54c880b8d3ff5c6ba3b98da"
SRC_URI[python3-dnf-plugins-core.sha256sum] = "b54e3e1728a5745773c13f1520a024aa518c40b37087c4b7d25e09143e35c7c4"
SRC_URI[yum-utils.sha256sum] = "22d8674d7bc788300cf3caf361acfbff228fe1c070e74703f91e67e9efad3c04"
