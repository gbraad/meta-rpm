SUMMARY = "generated recipe based on hexchat srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-glib gdk-pixbuf glib-2.0 gtk2 libcanberra libnotify libproxy libx11 lua openssl pango pciutils perl pkgconfig-native platform-python3"
RPM_SONAME_REQ_hexchat = "libX11.so.6 libc.so.6 libcanberra.so.0 libcrypto.so.1.1 libdbus-glib-1.so.2 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 liblua-5.3.so libnotify.so.4 libpango-1.0.so.0 libpci.so.3 libperl.so.5.26 libproxy.so.1 libpthread.so.0 libpython3.6m.so.1.0 libssl.so.1.1"
RDEPENDS_hexchat = "dbus-glib gdk-pixbuf2 glib2 glibc gtk2 libX11 libcanberra libnotify libproxy lua-libs openssl-libs pango pciutils-libs perl-libs python3-libs"
RPROVIDES_hexchat-devel = "hexchat-dev (= 2.14.1)"
RDEPENDS_hexchat-devel = "hexchat pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hexchat-2.14.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hexchat-devel-2.14.1-2.el8.x86_64.rpm \
          "

SRC_URI[hexchat.sha256sum] = "50e0fa0ade475d917538aeadf24d9f4ba4fff19e9b110f92060314e7bef2a5da"
SRC_URI[hexchat-devel.sha256sum] = "46dfa3ff879cc75be2ee166d689310b6a81afd3aeb01c6f674ca87776d1090b0"
