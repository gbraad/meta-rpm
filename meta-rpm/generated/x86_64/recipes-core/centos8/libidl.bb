SUMMARY = "generated recipe based on libIDL srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_libIDL = "libIDL-2.so.0"
RPM_SONAME_REQ_libIDL = "libc.so.6 libglib-2.0.so.0"
RDEPENDS_libIDL = "glib2 glibc"
RPM_SONAME_REQ_libIDL-devel = "libIDL-2.so.0"
RPROVIDES_libIDL-devel = "libIDL-dev (= 0.8.14)"
RDEPENDS_libIDL-devel = "bash glib2-devel info libIDL pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libIDL-0.8.14-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libIDL-devel-0.8.14-15.el8.x86_64.rpm \
          "

SRC_URI[libIDL.sha256sum] = "68ece9441dce15624d49353c0686a6c0d71c60b7ecd550401185e8c052d7222e"
SRC_URI[libIDL-devel.sha256sum] = "ec5cbbbf94f9de4c6eb7c8d4f781b7b9b51c3cabbef69363235f98f9b5d95b88"
