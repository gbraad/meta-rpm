SUMMARY = "generated recipe based on perl-List-MoreUtils-XS srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-List-MoreUtils-XS = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-List-MoreUtils-XS = "glibc perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-List-MoreUtils-XS-0.428-3.el8.x86_64.rpm \
          "

SRC_URI[perl-List-MoreUtils-XS.sha256sum] = "e410289cb6768c77590f7bcdba2faa993c4156f61d067f4c66b309ead1e99452"
