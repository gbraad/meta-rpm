SUMMARY = "generated recipe based on libmodman srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libmodman = "libmodman.so.1"
RPM_SONAME_REQ_libmodman = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libmodman = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libmodman-2.0.1-17.el8.x86_64.rpm \
          "

SRC_URI[libmodman.sha256sum] = "c3b8c553b166491d3114793e198cd1aad95e494d177af8d0dc7180b8b841124d"
