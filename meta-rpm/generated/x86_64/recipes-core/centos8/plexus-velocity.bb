SUMMARY = "generated recipe based on plexus-velocity srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-velocity = "apache-commons-collections java-1.8.0-openjdk-headless javapackages-filesystem plexus-containers-container-default velocity"
RDEPENDS_plexus-velocity-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-velocity-1.2-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-velocity-javadoc-1.2-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-velocity.sha256sum] = "8ef78de064a3f115050a731552cc8d7661c9b3350727a627026785bd8ce2a51d"
SRC_URI[plexus-velocity-javadoc.sha256sum] = "d5106773e36faba67bc58ee4e91325c6f2298cfbf779a4d6ca3f0d579fcee710"
