SUMMARY = "generated recipe based on mod_md srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "apr apr-util curl expat jansson libxcrypt openssl pkgconfig-native"
RPM_SONAME_REQ_mod_md = "libapr-1.so.0 libaprutil-1.so.0 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libexpat.so.1 libjansson.so.4 libpthread.so.0 libssl.so.1.1"
RDEPENDS_mod_md = "apr apr-util expat glibc httpd jansson libcurl libxcrypt mod_ssl openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_md-2.0.8-7.module_el8.2.0+307+4d18d695.x86_64.rpm \
          "

SRC_URI[mod_md.sha256sum] = "b91cccf8c417db0c81ec194916135637a3bd3826ed69fa92753ac8ae7c9fb6db"
