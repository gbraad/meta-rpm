SUMMARY = "generated recipe based on celt051 srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libogg pkgconfig-native"
RPM_SONAME_PROV_celt051 = "libcelt051.so.0"
RPM_SONAME_REQ_celt051 = "libc.so.6 libm.so.6 libogg.so.0"
RDEPENDS_celt051 = "glibc libogg"
RPM_SONAME_REQ_celt051-devel = "libcelt051.so.0"
RPROVIDES_celt051-devel = "celt051-dev (= 0.5.1.3)"
RDEPENDS_celt051-devel = "celt051 libogg-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/celt051-0.5.1.3-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/celt051-devel-0.5.1.3-15.el8.x86_64.rpm \
          "

SRC_URI[celt051.sha256sum] = "f689f4c20fb5de0e9c39b9c5f81e44fe89833aead1597de6454c2b459a2d1742"
SRC_URI[celt051-devel.sha256sum] = "b7355119431047ee7de70622bb616b69bb1d1e2d681ac6a8a9c2da2debcffdd3"
