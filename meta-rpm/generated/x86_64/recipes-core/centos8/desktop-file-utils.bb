SUMMARY = "generated recipe based on desktop-file-utils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_desktop-file-utils = "libc.so.6 libglib-2.0.so.0"
RDEPENDS_desktop-file-utils = "bash emacs-filesystem glib2 glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/desktop-file-utils-0.23-8.el8.x86_64.rpm \
          "

SRC_URI[desktop-file-utils.sha256sum] = "f341d768bdcc8ab517ae50943242de4f9997c6cbfc7a26fa5fc5ae11db245902"
