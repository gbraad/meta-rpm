SUMMARY = "generated recipe based on cglib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & BSD"
RPM_LICENSE = "ASL 2.0 and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_cglib = "java-1.8.0-openjdk-headless javapackages-filesystem objectweb-asm"
RDEPENDS_cglib-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cglib-3.2.4-7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/cglib-javadoc-3.2.4-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[cglib.sha256sum] = "cafa6da0979cc82954f23b9e1a89152d6c3d39761531750d7d6c1b153d55a652"
SRC_URI[cglib-javadoc.sha256sum] = "4474b23c6c8f02a4be2c847f2920eb516d89fbb4db21791ac0249cbbc14dac96"
