SUMMARY = "generated recipe based on apache-commons-compress srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-compress = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-compress-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-commons-compress-1.18-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-commons-compress-javadoc-1.18-1.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-compress.sha256sum] = "b9edab1d683d17243d723f6351e5f3d4c85e11d754674e88d8e0f2f20c375de5"
SRC_URI[apache-commons-compress-javadoc.sha256sum] = "29fe46b87b6e5b9ce3c24309956fd5648863bba2e1dd8948254d1bc40a7cebe9"
