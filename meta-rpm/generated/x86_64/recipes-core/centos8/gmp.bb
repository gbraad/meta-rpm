SUMMARY = "generated recipe based on gmp srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0 | GPL-2.0"
RPM_LICENSE = "LGPLv3+ or GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_gmp = "libgmp.so.10"
RPM_SONAME_REQ_gmp = "libc.so.6"
RDEPENDS_gmp = "glibc"
RPM_SONAME_PROV_gmp-c++ = "libgmpxx.so.4"
RPM_SONAME_REQ_gmp-c++ = "libc.so.6 libgcc_s.so.1 libgmp.so.10 libstdc++.so.6"
RDEPENDS_gmp-c++ = "glibc gmp libgcc libstdc++"
RPM_SONAME_REQ_gmp-devel = "libgmp.so.10 libgmpxx.so.4"
RPROVIDES_gmp-devel = "gmp-dev (= 6.1.2)"
RDEPENDS_gmp-devel = "bash gmp gmp-c++ info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gmp-6.1.2-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gmp-c++-6.1.2-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gmp-devel-6.1.2-10.el8.x86_64.rpm \
          "

SRC_URI[gmp.sha256sum] = "3b96e2c7d5cd4b49bfde8e52c8af6ff595c91438e50856e468f14a049d8511e2"
SRC_URI[gmp-c++.sha256sum] = "c16c0fd39bc0a0fce7a75b9584a6f04db7e141a22af69f5dfb4644718a25063a"
SRC_URI[gmp-devel.sha256sum] = "fce02741fc828b95f0d73c5ae9633d00f8b7de6ed843198c89854dffdc006e50"
