SUMMARY = "generated recipe based on glew srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & MIT"
RPM_LICENSE = "BSD and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libglvnd libx11 mesa-libglu pkgconfig-native"
RPM_SONAME_REQ_glew-devel = "libGLEW.so.2.0"
RPROVIDES_glew-devel = "glew-dev (= 2.0.0)"
RDEPENDS_glew-devel = "libGLEW mesa-libGLU-devel pkgconf-pkg-config"
RPM_SONAME_PROV_libGLEW = "libGLEW.so.2.0"
RPM_SONAME_REQ_libGLEW = "libGL.so.1 libX11.so.6 libc.so.6"
RDEPENDS_libGLEW = "glibc libX11 libglvnd-glx"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glew-devel-2.0.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libGLEW-2.0.0-6.el8.x86_64.rpm \
          "

SRC_URI[glew-devel.sha256sum] = "15ad9d300113d295b22a611640d130483f14d75b47bf5e6b09e9d1a13ea72fc2"
SRC_URI[libGLEW.sha256sum] = "e9f5ee8f643b3d1905fb9a63a462bad40db5a6e9048a9902c7e7bc80b3c9556a"
