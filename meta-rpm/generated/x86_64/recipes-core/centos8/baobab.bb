SUMMARY = "generated recipe based on baobab srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GFDL-1.1"
RPM_LICENSE = "GPLv2+ and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo glib-2.0 gtk+3 pango pkgconfig-native"
RPM_SONAME_REQ_baobab = "libc.so.6 libcairo.so.2 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0"
RDEPENDS_baobab = "cairo glib2 glibc gtk3 pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/baobab-3.28.0-4.el8.x86_64.rpm \
          "

SRC_URI[baobab.sha256sum] = "c5461fbdfbca2fd840c7c0cc0af3965e48ab22016bef9829448f2d7693e993c0"
