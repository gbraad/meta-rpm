SUMMARY = "generated recipe based on c-ares srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_c-ares = "libcares.so.2"
RPM_SONAME_REQ_c-ares = "libc.so.6"
RDEPENDS_c-ares = "glibc"
RPM_SONAME_REQ_c-ares-devel = "libcares.so.2"
RPROVIDES_c-ares-devel = "c-ares-dev (= 1.13.0)"
RDEPENDS_c-ares-devel = "c-ares pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/c-ares-1.13.0-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/c-ares-devel-1.13.0-5.el8.x86_64.rpm \
          "

SRC_URI[c-ares.sha256sum] = "9d160cdfba107ddc0613e169311bf57077c04e71a052a57704f3bdb64729d565"
SRC_URI[c-ares-devel.sha256sum] = "a2a6d81b2304ee677581024e59e52dbccbf5914458df2c57535e4953dce76ffa"
