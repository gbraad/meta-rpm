SUMMARY = "generated recipe based on libomp srpm"
DESCRIPTION = "Description"
LICENSE = "NCSA"
RPM_LICENSE = "NCSA"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "clang elfutils libffi libgcc pkgconfig-native"
RPM_SONAME_PROV_libomp = "libomp.so libomptarget.rtl.x86_64.so libomptarget.so"
RPM_SONAME_REQ_libomp = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libelf.so.1 libffi.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libomp = "elfutils-libelf glibc libffi libgcc libstdc++"
RPROVIDES_libomp-devel = "libomp-dev (= 9.0.1)"
RDEPENDS_libomp-devel = "clang-devel"
RDEPENDS_libomp-test = "bash clang gcc gcc-c++ libomp libomp-devel llvm python3-lit"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libomp-9.0.1-1.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libomp-devel-9.0.1-1.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libomp-test-9.0.1-1.module_el8.2.0+309+0c7b6b03.x86_64.rpm \
          "

SRC_URI[libomp.sha256sum] = "2b114449ece182e9c76446fcbf23ce6eea9a280852aa2efb2064b926d6aabd05"
SRC_URI[libomp-devel.sha256sum] = "cc6633cdb12fdf431535f58f0b6f8224f90fa24382413c18d77a062dfbfe72fb"
SRC_URI[libomp-test.sha256sum] = "5a815944cf9ea053bfab488bcff7e25e225bff7bfe6757251a4527c9f927bc25"
