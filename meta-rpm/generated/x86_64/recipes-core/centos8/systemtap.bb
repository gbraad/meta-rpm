SUMMARY = "generated recipe based on systemtap srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "avahi-libs boost dyninst elfutils json-c libgcc libselinux libvirt libxml2 ncurses nspr nss pkgconfig-native platform-python3 readline rpm sqlite3"
RDEPENDS_systemtap = "bash systemtap-client systemtap-devel"
RPM_SONAME_REQ_systemtap-client = "libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libreadline.so.7 librpm.so.8 librpmio.so.8 libsmime3.so libsqlite3.so.0 libssl3.so libstdc++.so.6"
RDEPENDS_systemtap-client = "avahi-libs bash coreutils elfutils-libelf elfutils-libs glibc grep libgcc libstdc++ mokutil nspr nss nss-util openssh-clients perl-interpreter readline rpm-libs sed sqlite-libs systemtap-runtime unzip zip"
RPM_SONAME_PROV_systemtap-devel = "libHelperSDT_amd64.so"
RPM_SONAME_REQ_systemtap-devel = "libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libreadline.so.7 librpm.so.8 librpmio.so.8 libsmime3.so libsqlite3.so.0 libssl3.so libstdc++.so.6"
RPROVIDES_systemtap-devel = "systemtap-dev (= 4.2)"
RDEPENDS_systemtap-devel = "avahi-libs bash elfutils-libelf elfutils-libs gcc glibc libgcc libstdc++ make nspr nss nss-util readline rpm-libs sqlite-libs"
RDEPENDS_systemtap-exporter = "bash platform-python systemtap-runtime"
RDEPENDS_systemtap-initscript = "bash systemd systemtap"
RPM_SONAME_REQ_systemtap-runtime = "libboost_system.so.1.66.0 libc.so.6 libdl.so.2 libdyninstAPI.so.10.1 libelf.so.1 libgcc_s.so.1 libjson-c.so.4 libm.so.6 libncurses.so.6 libnspr4.so libnss3.so libnssutil3.so libpanel.so.6 libplc4.so libplds4.so libpthread.so.0 libselinux.so.1 libsmime3.so libssl3.so libstdc++.so.6 libsymtabAPI.so.10.1 libtinfo.so.6"
RDEPENDS_systemtap-runtime = "bash boost-system dyninst elfutils-libelf glibc json-c libgcc libselinux libstdc++ ncurses-libs nspr nss nss-util shadow-utils"
RPM_SONAME_PROV_systemtap-runtime-java = "libHelperSDT_amd64.so"
RPM_SONAME_REQ_systemtap-runtime-java = "libc.so.6"
RDEPENDS_systemtap-runtime-java = "bash byteman glibc iproute java-1.8.0-openjdk-devel systemtap-runtime"
RPM_SONAME_REQ_systemtap-runtime-python3 = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_systemtap-runtime-python3 = "glibc platform-python python3-libs systemtap-runtime"
RDEPENDS_systemtap-runtime-virtguest = "bash coreutils findutils grep systemtap-runtime"
RPM_SONAME_REQ_systemtap-runtime-virthost = "libc.so.6 libvirt.so.0 libxml2.so.2"
RDEPENDS_systemtap-runtime-virthost = "glibc libvirt libvirt-libs libxml2"
RPM_SONAME_REQ_systemtap-server = "libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libstdc++.so.6"
RDEPENDS_systemtap-server = "avahi-libs bash coreutils glibc libgcc libstdc++ nspr nss nss-util openssl shadow-utils systemd systemtap-devel unzip zip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-4.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-client-4.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-devel-4.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-exporter-4.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-initscript-4.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-runtime-4.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-runtime-java-4.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-runtime-python3-4.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-runtime-virtguest-4.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-runtime-virthost-4.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/systemtap-server-4.2-6.el8.x86_64.rpm \
          "

SRC_URI[systemtap.sha256sum] = "b2ff8976430e736fac2ff1c593e34cac34739c6edf83129e64cc7189466ea133"
SRC_URI[systemtap-client.sha256sum] = "7371ba0c0e830aa9480e071bd8a8bfeece77ea386fd6bcde101453374887e05b"
SRC_URI[systemtap-devel.sha256sum] = "f6e52c5c6eb02ba1a8639248a3bd636c86bf2f6af6355196cf7fd16aa3ece2f0"
SRC_URI[systemtap-exporter.sha256sum] = "34b819e6b760f7d6b868e43cff8ddd4f43810d05b6add2236ab8cd1886474afe"
SRC_URI[systemtap-initscript.sha256sum] = "7e99af70bd610f217b0cce2c7703f47afe606f768c7e9fe7d66b4d0b2e91d167"
SRC_URI[systemtap-runtime.sha256sum] = "4edf0e79d0a2ddb0c7e899db1bb213205341e6b7a684da2e7d68338d277a14c2"
SRC_URI[systemtap-runtime-java.sha256sum] = "07f7283424ada96e23f96c3249f35d43eff013bf41ab319d0b0243e7f5b7aa0d"
SRC_URI[systemtap-runtime-python3.sha256sum] = "a9c057c7633d027ee4195afdaef4cad33a8119a0714aef45b833e78d39ad81e6"
SRC_URI[systemtap-runtime-virtguest.sha256sum] = "984fab3424b041adcc0972bb779b214e2f129284cd807233ff70aa8cd555f5c7"
SRC_URI[systemtap-runtime-virthost.sha256sum] = "44633cf46e09a94b82330951455924a0d6aa6dc68baa0cd7d4b9f0e48acc7abb"
SRC_URI[systemtap-server.sha256sum] = "f18005aed4045851d8e033b455313f11d829776ba9ae84152753c8e9547708cf"
