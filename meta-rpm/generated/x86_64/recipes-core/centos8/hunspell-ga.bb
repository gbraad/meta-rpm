SUMMARY = "generated recipe based on hunspell-ga srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ga = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ga-5.0-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-ga.sha256sum] = "abb14a0631e1f56e241f281fcd1633dcc502cd3d0a99a893778145d4e2f1d75a"
