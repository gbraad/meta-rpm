SUMMARY = "generated recipe based on libpng12 srpm"
DESCRIPTION = "Description"
LICENSE = "Zlib"
RPM_LICENSE = "zlib"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_libpng12 = "libpng12.so.0"
RPM_SONAME_REQ_libpng12 = "libc.so.6 libm.so.6 libz.so.1"
RDEPENDS_libpng12 = "glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpng12-1.2.57-5.el8.x86_64.rpm \
          "

SRC_URI[libpng12.sha256sum] = "c64d1fb02125b87433cf1b00de1de3d751da24ebc589573608453fdfe259261a"
