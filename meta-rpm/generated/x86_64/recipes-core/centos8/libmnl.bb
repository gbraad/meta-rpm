SUMMARY = "generated recipe based on libmnl srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmnl = "libmnl.so.0"
RPM_SONAME_REQ_libmnl = "libc.so.6"
RDEPENDS_libmnl = "glibc"
RPM_SONAME_REQ_libmnl-devel = "libmnl.so.0"
RPROVIDES_libmnl-devel = "libmnl-dev (= 1.0.4)"
RDEPENDS_libmnl-devel = "libmnl pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libmnl-1.0.4-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmnl-devel-1.0.4-6.el8.x86_64.rpm \
          "

SRC_URI[libmnl.sha256sum] = "30fab73ee155f03dbbd99c1e30fe59dfba4ae8fdb2e7213451ccc36d6918bfcc"
SRC_URI[libmnl-devel.sha256sum] = "1c92eb3e72e39fe4caddd13f92c252edad807a2c5af157cae68a7cea5dbccc8c"
