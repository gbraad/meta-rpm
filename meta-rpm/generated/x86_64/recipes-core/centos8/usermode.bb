SUMMARY = "generated recipe based on usermode srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 libblkid libice libselinux libsm libuser pam pango pkgconfig-native startup-notification"
RPM_SONAME_REQ_usermode = "libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libpam.so.0 libpam_misc.so.0 libpthread.so.0 libselinux.so.1 libuser.so.1"
RDEPENDS_usermode = "glib2 glibc libselinux libuser pam passwd util-linux"
RPM_SONAME_REQ_usermode-gtk = "libICE.so.6 libSM.so.6 libatk-1.0.so.0 libblkid.so.1 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libstartup-notification-1.so.0"
RDEPENDS_usermode-gtk = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 libICE libSM libblkid pango startup-notification usermode"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/usermode-gtk-1.113-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/usermode-1.113-1.el8.x86_64.rpm \
          "

SRC_URI[usermode.sha256sum] = "eec162a2757ee2ffbbee95e1dbcd33318119e5113e53af877ce9416529a19082"
SRC_URI[usermode-gtk.sha256sum] = "5b6045c4f08b8989b16c6f27b7f4ed3bbae1b93510f7eeb792095eeb506bf04d"
