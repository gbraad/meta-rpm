SUMMARY = "generated recipe based on libgexiv2 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "compat-exiv2-026 exiv2 glib-2.0 libgcc pkgconfig-native"
RPM_SONAME_PROV_libgexiv2 = "libgexiv2.so.2"
RPM_SONAME_REQ_libgexiv2 = "libc.so.6 libexiv2.so.26 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libstdc++.so.6"
RDEPENDS_libgexiv2 = "compat-exiv2-026 glib2 glibc libgcc libstdc++"
RPM_SONAME_REQ_libgexiv2-devel = "libgexiv2.so.2"
RPROVIDES_libgexiv2-devel = "libgexiv2-dev (= 0.10.8)"
RDEPENDS_libgexiv2-devel = "exiv2-devel glib2-devel libgexiv2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libgexiv2-0.10.8-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgexiv2-devel-0.10.8-4.el8.x86_64.rpm \
          "

SRC_URI[libgexiv2.sha256sum] = "e06ba3904e30d464b2d1e40e75c4ed5e0a81101de9f9c214d0da764fbbe3a3d8"
SRC_URI[libgexiv2-devel.sha256sum] = "aef434524b7d7f0bfd662aad2d0f9b26fdca4860978f3a1ae0faf01099fcd0eb"
