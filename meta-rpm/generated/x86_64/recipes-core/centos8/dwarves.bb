SUMMARY = "generated recipe based on dwarves srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "elfutils pkgconfig-native zlib"
RPM_SONAME_REQ_dwarves = "libc.so.6 libdw.so.1 libdwarves.so.1 libdwarves_emit.so.1 libdwarves_reorganize.so.1 libelf.so.1 libz.so.1"
RDEPENDS_dwarves = "bash elfutils-libelf elfutils-libs glibc libdwarves1 platform-python zlib"
RPM_SONAME_PROV_libdwarves1 = "libdwarves.so.1 libdwarves_emit.so.1 libdwarves_reorganize.so.1"
RPM_SONAME_REQ_libdwarves1 = "libc.so.6 libdw.so.1 libelf.so.1 libz.so.1"
RDEPENDS_libdwarves1 = "elfutils-libelf elfutils-libs glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dwarves-1.15-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libdwarves1-1.15-5.el8.x86_64.rpm \
          "

SRC_URI[dwarves.sha256sum] = "9fe8edc61011a14ee412409883d510f93c4c6c76be9ee6a7d3cfacc45a9e26b4"
SRC_URI[libdwarves1.sha256sum] = "66808ed97c80fc0fd5f5bf9b54141774f978907d587c585de1d3cbb196177a67"
