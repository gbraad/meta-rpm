SUMMARY = "generated recipe based on glassfish-annotation-api srpm"
DESCRIPTION = "Description"
LICENSE = "CDDL-1.0 | GPL-2.0"
RPM_LICENSE = "CDDL or GPLv2 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_glassfish-annotation-api = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_glassfish-annotation-api-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glassfish-annotation-api-1.2-13.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glassfish-annotation-api-javadoc-1.2-13.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[glassfish-annotation-api.sha256sum] = "a9db076252fe0be3c32158bd5f48fcc9a2e72ba2f4505d37abe2b6dbc7f3c696"
SRC_URI[glassfish-annotation-api-javadoc.sha256sum] = "52e16c38617ceb6525e5760994b2b75fd6b7d454a876e7b34a18b4e4805b1454"
