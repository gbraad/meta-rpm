SUMMARY = "generated recipe based on libgusb srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libusb1 pkgconfig-native"
RPM_SONAME_PROV_libgusb = "libgusb.so.2"
RPM_SONAME_REQ_libgusb = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libusb-1.0.so.0"
RDEPENDS_libgusb = "glib2 glibc libusbx"
RPM_SONAME_REQ_libgusb-devel = "libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgusb.so.2"
RPROVIDES_libgusb-devel = "libgusb-dev (= 0.3.0)"
RDEPENDS_libgusb-devel = "glib2 glib2-devel glibc libgusb libusbx-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgusb-0.3.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libgusb-devel-0.3.0-1.el8.x86_64.rpm \
          "

SRC_URI[libgusb.sha256sum] = "ab7bc1a828168006b88934bea949ab2b29b837b0a431f7da1a12147f64f6ddb5"
SRC_URI[libgusb-devel.sha256sum] = "3dbfdcb83a1e77a78d5c4c04168a29be441c96e48b35b59926c1b598843f3ed1"
