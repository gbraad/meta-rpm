SUMMARY = "generated recipe based on fwupdate srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "efivar libsmbios pkgconfig-native popt"
RPM_SONAME_REQ_fwupdate = "libc.so.6 libefiboot.so.1 libefivar.so.1 libfwup.so.1 libpopt.so.0 libpthread.so.0 libsmbios_c.so.2"
RDEPENDS_fwupdate = "bash efivar-libs fwupdate-libs glibc libsmbios popt"
RDEPENDS_fwupdate-efi = "fwupdate-libs"
RPM_SONAME_PROV_fwupdate-libs = "libfwup.so.1"
RPM_SONAME_REQ_fwupdate-libs = "libc.so.6 libefiboot.so.1 libefivar.so.1 libpthread.so.0 libsmbios_c.so.2"
RDEPENDS_fwupdate-libs = "bash efivar-libs fwupdate-efi glibc libsmbios shim-x64"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fwupdate-11-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fwupdate-efi-11-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/fwupdate-libs-11-3.el8.x86_64.rpm \
          "

SRC_URI[fwupdate.sha256sum] = "58ea1b70825d3f7948c745a4dd1827819a6089762716ed77d1ac05014b990ff3"
SRC_URI[fwupdate-efi.sha256sum] = "93b050a8bcf2370337bbfe9f3be22aac2c48e1a2feac6e43f02af86b1691b505"
SRC_URI[fwupdate-libs.sha256sum] = "350c0b52575f83b4b0a13a075a30122224b0cc745170234e429f652d399c2126"
