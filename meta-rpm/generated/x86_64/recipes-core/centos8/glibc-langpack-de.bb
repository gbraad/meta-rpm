SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-de = "locale-base-de-at (= 2.28) locale-base-de-at.utf8 (= 2.28) locale-base-de-at@euro (= 2.28) locale-base-de-be (= 2.28) locale-base-de-be.utf8 (= 2.28) locale-base-de-be@euro (= 2.28) locale-base-de-ch (= 2.28) locale-base-de-ch.utf8 (= 2.28) locale-base-de-de (= 2.28) locale-base-de-de.utf8 (= 2.28) locale-base-de-de@euro (= 2.28) locale-base-de-it (= 2.28) locale-base-de-it.utf8 (= 2.28) locale-base-de-li.utf8 (= 2.28) locale-base-de-lu (= 2.28) locale-base-de-lu.utf8 (= 2.28) locale-base-de-lu@euro (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de (= 2.28) virtual-locale-de-at (= 2.28) virtual-locale-de-at.utf8 (= 2.28) virtual-locale-de-at@euro (= 2.28) virtual-locale-de-be (= 2.28) virtual-locale-de-be.utf8 (= 2.28) virtual-locale-de-be@euro (= 2.28) virtual-locale-de-ch (= 2.28) virtual-locale-de-ch.utf8 (= 2.28) virtual-locale-de-de (= 2.28) virtual-locale-de-de.utf8 (= 2.28) virtual-locale-de-de@euro (= 2.28) virtual-locale-de-it (= 2.28) virtual-locale-de-it.utf8 (= 2.28) virtual-locale-de-li.utf8 (= 2.28) virtual-locale-de-lu (= 2.28) virtual-locale-de-lu.utf8 (= 2.28) virtual-locale-de-lu@euro (= 2.28)"
RDEPENDS_glibc-langpack-de = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-de-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-de.sha256sum] = "0df280a5f02946a34ae4bbdc7e188251f5e7e246ccb41187a8a0786378ccd91a"
