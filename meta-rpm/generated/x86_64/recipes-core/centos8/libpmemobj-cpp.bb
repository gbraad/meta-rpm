SUMMARY = "generated recipe based on libpmemobj-cpp srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native pmdk"
RPROVIDES_libpmemobj++-devel = "libpmemobj++-dev (= 1.6)"
RDEPENDS_libpmemobj++-devel = "libpmemobj-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmemobj++-devel-1.6-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libpmemobj++-doc-1.6-2.el8.x86_64.rpm \
          "

SRC_URI[libpmemobj++-devel.sha256sum] = "8ee6e79c1e7b4394c5504ae35013a9e709107cf054e2fb02692fa1fcbd54d994"
SRC_URI[libpmemobj++-doc.sha256sum] = "9a14b81f314aa7177403cc8d7eb117122eeb6905ae030c19993d7f98b3f15958"
