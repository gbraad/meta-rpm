SUMMARY = "generated recipe based on sane-frontends srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+ and GPLv2+ with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 pango pkgconfig-native sane-backends"
RPM_SONAME_REQ_sane-frontends = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libsane.so.1"
RDEPENDS_sane-frontends = "atk cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 pango sane-backends-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sane-frontends-1.0.14-30.el8.x86_64.rpm \
          "

SRC_URI[sane-frontends.sha256sum] = "a69e70779a616bafd971a4feefac46e3109d667b73fecaba15874068b3437873"
