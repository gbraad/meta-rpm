SUMMARY = "generated recipe based on rasdaemon srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native sqlite3"
RPM_SONAME_REQ_rasdaemon = "libc.so.6 libpthread.so.0 libsqlite3.so.0"
RDEPENDS_rasdaemon = "dmidecode glibc hwdata perl-DBD-SQLite perl-Getopt-Long perl-constant perl-interpreter perl-libs sqlite-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rasdaemon-0.6.1-3.el8.x86_64.rpm \
          "

SRC_URI[rasdaemon.sha256sum] = "e037403e2c7a1547d6f5629c04e5b8b7b08b0af1e35fad479de7149ea55b5250"
