SUMMARY = "generated recipe based on sil-abyssinica-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_sil-abyssinica-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sil-abyssinica-fonts-1.200-13.el8.noarch.rpm \
          "

SRC_URI[sil-abyssinica-fonts.sha256sum] = "34a928cd01a998db63cbf930bea8c9f246848e479498866b7c026c3cc3c55778"
