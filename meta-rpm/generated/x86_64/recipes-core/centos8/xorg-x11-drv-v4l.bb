SUMMARY = "generated recipe based on xorg-x11-drv-v4l srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-drv-v4l = "libc.so.6"
RDEPENDS_xorg-x11-drv-v4l = "glibc xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-v4l-0.3.0-2.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-v4l.sha256sum] = "11e570035b9d599123dd6b928fe0732915e8367393cd75d5287ef9edda22fdec"
