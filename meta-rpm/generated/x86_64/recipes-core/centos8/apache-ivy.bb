SUMMARY = "generated recipe based on apache-ivy srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-ivy = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-ivy-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-ivy-2.4.0-14.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-ivy-javadoc-2.4.0-14.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-ivy.sha256sum] = "da2f0ea124362ff003a48fb7c76ceb62f5ab9e5f8f0dd6fbb55399c087071ca8"
SRC_URI[apache-ivy-javadoc.sha256sum] = "489ecad6224b5b95f68b8cf965fa33bdb3ec7230005b00cbac6c024256ee33c0"
