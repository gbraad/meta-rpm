SUMMARY = "generated recipe based on whois srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libidn2 pkgconfig-native"
RPM_SONAME_REQ_whois = "libc.so.6 libidn2.so.0"
RDEPENDS_whois = "bash chkconfig glibc libidn2 whois-nls"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/whois-5.5.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/whois-nls-5.5.1-2.el8.noarch.rpm \
          "

SRC_URI[whois.sha256sum] = "aa1e99415effc907e402a739e98e0c468258d9cc098ed7d83dc82b0bcea27545"
SRC_URI[whois-nls.sha256sum] = "93f5ad20f63fd1cf4681c22afe470166dbd40b75f442cb368b0155262c7afbf0"
