SUMMARY = "generated recipe based on netcf srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "augeas libgcrypt libgpg-error libnl libxml2 libxslt pkgconfig-native readline"
RPM_SONAME_REQ_netcf = "libaugeas.so.0 libc.so.6 libdl.so.2 libexslt.so.0 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libnetcf.so.1 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 libreadline.so.7 libxml2.so.2 libxslt.so.1"
RDEPENDS_netcf = "augeas-libs glibc libgcrypt libgpg-error libnl3 libxml2 libxslt netcf-libs readline systemd"
RPM_SONAME_REQ_netcf-devel = "libnetcf.so.1"
RPROVIDES_netcf-devel = "netcf-dev (= 0.2.8)"
RDEPENDS_netcf-devel = "netcf-libs pkgconf-pkg-config"
RPM_SONAME_PROV_netcf-libs = "libnetcf.so.1"
RPM_SONAME_REQ_netcf-libs = "libaugeas.so.0 libc.so.6 libdl.so.2 libexslt.so.0 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 libxml2.so.2 libxslt.so.1"
RDEPENDS_netcf-libs = "augeas-libs bash glibc libgcrypt libgpg-error libnl3 libxml2 libxslt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/netcf-0.2.8-12.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/netcf-devel-0.2.8-12.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/netcf-libs-0.2.8-12.module_el8.2.0+320+13f867d7.x86_64.rpm \
          "

SRC_URI[netcf.sha256sum] = "1212022026e06c9cabff9253c1af1ebb39dad0f84f245c82ee7f22c6dc9e61f6"
SRC_URI[netcf-devel.sha256sum] = "014acb2ff6ffbe86a65828a97e461e58f72b3f65b77f818fc6be0e526f9225ea"
SRC_URI[netcf-libs.sha256sum] = "e53071cb8530bdbc9b1013ceefd955c3fc094010f794bb2f986e746325b352ae"
