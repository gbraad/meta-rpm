SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gd libpng pkgconfig-native zlib"
RPM_SONAME_REQ_glibc-utils = "libc.so.6 libgd.so.3 libm.so.6 libpng16.so.16 libz.so.1"
RDEPENDS_glibc-utils = "bash gd glibc libpng perl-interpreter zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/glibc-utils-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-utils.sha256sum] = "2e2c400ccf7fc39cd25d9238dfae313741e467e1491d9ed39d42b91435578d88"
