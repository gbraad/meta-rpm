SUMMARY = "generated recipe based on xorg-x11-drv-intel srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo libdrm libpciaccess libx11 libxcb libxcursor libxdamage libxext libxfixes libxinerama libxrandr libxrender libxscrnsaver libxtst libxv libxvmc pixman pkgconfig-native systemd-libs xcb-util"
RPM_SONAME_REQ_intel-gpu-tools = "libX11.so.6 libXcursor.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXinerama.so.1 libXrandr.so.2 libXrender.so.1 libXss.so.1 libXtst.so.6 libXv.so.1 libc.so.6 libcairo.so.2 libdrm.so.2 libdrm_intel.so.1 libpciaccess.so.0 libpixman-1.so.0 librt.so.1 libudev.so.1"
RDEPENDS_intel-gpu-tools = "bash cairo glibc libX11 libXScrnSaver libXcursor libXdamage libXext libXfixes libXinerama libXrandr libXrender libXtst libXv libdrm libpciaccess pixman systemd-libs"
RPM_SONAME_PROV_xorg-x11-drv-intel = "libIntelXvMC.so.1"
RPM_SONAME_REQ_xorg-x11-drv-intel = "libX11-xcb.so.1 libX11.so.6 libXv.so.1 libXvMC.so.1 libc.so.6 libdrm.so.2 libdrm_intel.so.1 libm.so.6 libpciaccess.so.0 libpixman-1.so.0 libpthread.so.0 libudev.so.1 libxcb-dri2.so.0 libxcb-util.so.1 libxcb.so.1"
RDEPENDS_xorg-x11-drv-intel = "glibc libX11 libX11-xcb libXv libXvMC libdrm libpciaccess libxcb pixman polkit systemd-libs xcb-util xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/intel-gpu-tools-2.99.917-38.20180618.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-intel-2.99.917-38.20180618.el8.x86_64.rpm \
          "

SRC_URI[intel-gpu-tools.sha256sum] = "801320844c51ecab2426e75858dce61f41f6d8ba61849cffc5fdde185c797522"
SRC_URI[xorg-x11-drv-intel.sha256sum] = "ec96a9880b12ec8fa9656cf1f3d72ff4b8de9a32878defb0ad5c1bbc03fba79b"
