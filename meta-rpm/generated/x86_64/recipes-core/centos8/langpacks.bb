SUMMARY = "generated recipe based on langpacks srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-af-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-am-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ar-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-as-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ast-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-be-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-bg-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-bn-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-br-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-bs-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ca-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-cs-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-cy-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-da-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-de-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-el-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-en-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-en_GB-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-es-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-et-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-eu-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-fa-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-fi-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-fr-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ga-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-gl-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-gu-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-he-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-hi-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-hr-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-hu-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ia-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-id-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-is-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-it-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ja-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-kk-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-kn-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ko-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-lt-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-lv-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-mai-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-mk-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ml-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-mr-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ms-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-nb-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ne-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-nl-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-nn-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-nr-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-nso-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-or-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-pa-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-pl-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-pt-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-pt_BR-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ro-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ru-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-si-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-sk-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-sl-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-sq-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-sr-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ss-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-sv-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ta-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-te-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-th-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-tn-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-tr-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ts-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-uk-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ur-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-ve-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-vi-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-xh-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-zh_CN-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-zh_TW-1.0-12.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/langpacks-zu-1.0-12.el8.noarch.rpm \
          "

SRC_URI[langpacks-af.sha256sum] = "166fa73151697f0c73de87b5a04dd8c58a244a5eccb8d52e058e022356331d34"
SRC_URI[langpacks-am.sha256sum] = "cee36c16b1adcb271deb242dfa4acc8d78c328cfaab4c49b8ccf09430df6ceb9"
SRC_URI[langpacks-ar.sha256sum] = "d4185726354b6a26ddb72f19d508e99158de68020f39e372019517c84668adda"
SRC_URI[langpacks-as.sha256sum] = "dd61dd45494c0adf9b60bb5ce38294a778d5cfa19c1ebbc1217f629762241d3a"
SRC_URI[langpacks-ast.sha256sum] = "0722dd4019c3f7043c5dfcb2b19dd7a12aba2c895ffb336ca82ad0f4449ef097"
SRC_URI[langpacks-be.sha256sum] = "1d6de0f7ff3847ad2bc65ab3fda84819021ea5812972431947d2c68e17e26391"
SRC_URI[langpacks-bg.sha256sum] = "6dec392a662f9379b75439c4cf6aabf8301cb37370db6cc15153ee76821429d8"
SRC_URI[langpacks-bn.sha256sum] = "1b90987be7f1c4dd0d83b0cdcfe9d20212c42734afe054d32c41994a89b17fb7"
SRC_URI[langpacks-br.sha256sum] = "849b766fbafa8bd8930364ddc27a567896696cc5d353b0925d48e98cd93de02f"
SRC_URI[langpacks-bs.sha256sum] = "e209b004ab9d85a5708cfdd17c82b551eba4b08e0095dbd20b959e8d90baa973"
SRC_URI[langpacks-ca.sha256sum] = "cf9f48424ad24857324e84c82487959adbc28c9bd8560b78b23bd88f366ab0d7"
SRC_URI[langpacks-cs.sha256sum] = "a78bbd8ce2ca9ed1b3758044f63cee9824a8bf31e0c6ad1bc98bf811129655bd"
SRC_URI[langpacks-cy.sha256sum] = "aaa3b8d2cb37e02e56ade9eb355ca5e31509e290b2d7e9f2a2f10753bffb6e0f"
SRC_URI[langpacks-da.sha256sum] = "f6d8ac3de41311728f2d7e5bbf6f8c1b7ba9faee4ce6070be361f871cdd479cd"
SRC_URI[langpacks-de.sha256sum] = "ff839f9b20fc04a97bb97225b54c0923534870357169b35edfc6c3271939287e"
SRC_URI[langpacks-el.sha256sum] = "868f6a9ccaec28706d5cabe0aa5297528127843ea47a2e2a83b34e71ed06b488"
SRC_URI[langpacks-en.sha256sum] = "c99db07c2548246f6b2ec98c1d2b536e40b2f4dbba39f3430044868a27985732"
SRC_URI[langpacks-en_GB.sha256sum] = "2692746e369d1fcc1e85f6abc8a2afcf5c190ab60d7d77d7f878e38c32c40f64"
SRC_URI[langpacks-es.sha256sum] = "83463efc09a1777eefb7e6d9d1e5e5a1e56f15e29aa46d492971c2118add009d"
SRC_URI[langpacks-et.sha256sum] = "d6b99d0c7afdd5b4168c1b45944679af5d015377020df3a7291fb4d3637d42d5"
SRC_URI[langpacks-eu.sha256sum] = "c4a2e5ab68684770c45af25d55e3950bff61e8cc9fc7a6c715052105b23aa59e"
SRC_URI[langpacks-fa.sha256sum] = "d5c48283a18c9f3521518273d3ee9d2fc2ebfd9db3072cf827f63a2eb10f1380"
SRC_URI[langpacks-fi.sha256sum] = "45d643c5cf74ba452483fc21551560fbe6fced753a303b8a3e3ad898fbbe6bb4"
SRC_URI[langpacks-fr.sha256sum] = "49ad45284f01eef531bb7c09bb2cbe01c2082676ce7e48b75885aa30efa0533a"
SRC_URI[langpacks-ga.sha256sum] = "096bcbcbb66a6c34b5c14ecff80c5ec78eb9576ba6f210154bc145f859483834"
SRC_URI[langpacks-gl.sha256sum] = "9bfd4b5f9dbf9d00de2ced89c5eb74f09ecc2ddc59da35245bc467b46d464d3b"
SRC_URI[langpacks-gu.sha256sum] = "ea82e75291b5755ec16284160c9cdfb5f213f71a2e5445c4c75419a5b0a125dd"
SRC_URI[langpacks-he.sha256sum] = "c86e188e0908229c6294fb7ce85c8293245ffaac0b2bc7f98ad654ebc05c3da3"
SRC_URI[langpacks-hi.sha256sum] = "d503fc60112c7002d6bc053dc7b5ba53a1b87ce31a506ae567178195956b7fde"
SRC_URI[langpacks-hr.sha256sum] = "4c26f2912051b290f366746ab50fb602bcab45111454b7b01163a0bd326c4a18"
SRC_URI[langpacks-hu.sha256sum] = "06564219ad7f5866cc956f37f944647e7cc9f610ac3989bf46065b49b62e65b6"
SRC_URI[langpacks-ia.sha256sum] = "d4519ec8f1ee2e1c26b53082cefdb91f3fc2991e2b8cf1240bfeae41895eb65a"
SRC_URI[langpacks-id.sha256sum] = "520252d7f923d75065b127e012f7b37074ad6b5309ca90abf5ca8cf150f10b6b"
SRC_URI[langpacks-is.sha256sum] = "f04570a7a7f55e9522947d2505a3acb97a92bd2c78460ef6e9dd24225decd479"
SRC_URI[langpacks-it.sha256sum] = "9d4454c66809de2fd526745b007489d0d795125bc91b68e761383e6f71099242"
SRC_URI[langpacks-ja.sha256sum] = "7a535f2fe4467d2dd66de80becdbc5df7b6e806e4b6c4c9672864273b740ab8e"
SRC_URI[langpacks-kk.sha256sum] = "b54097beef2b07b342fbbe37ab583b35ebc0cfee9456f2b16ff6f6bc4b43db5f"
SRC_URI[langpacks-kn.sha256sum] = "02e2ca19a527a9604586c29bfb218bf88a5bf05123460b1e827b1f5e918e2653"
SRC_URI[langpacks-ko.sha256sum] = "70813af6cf9395d00e23606722d600268560e949f2090090edf7cdade469c9b3"
SRC_URI[langpacks-lt.sha256sum] = "a4d32980eeec5882749aed07012d9c5a65e85c7557d65bfb39303d38011e58d3"
SRC_URI[langpacks-lv.sha256sum] = "9140eab7df9f993de106f53879bad72d63db0f40a7ea7855a921d31dc8664be2"
SRC_URI[langpacks-mai.sha256sum] = "3a2a290319245e1d67c579e6c8ffceae56a4ef621182880e28775fc005254a60"
SRC_URI[langpacks-mk.sha256sum] = "25b4e1e4d18bc9a0d567ca41f28024daff21d915a41770e777c5921aeca0381f"
SRC_URI[langpacks-ml.sha256sum] = "5d188717864e01faa1804903b17418457f607e41e9bc0c9b1adc447add15cff6"
SRC_URI[langpacks-mr.sha256sum] = "4dbf836a63f7ca2575a2bab90b4de38d68e44ca29078cde12d46de27b0278d8b"
SRC_URI[langpacks-ms.sha256sum] = "c190fb7acb2c936c7a4725a49fdc7bf877c559c780878b02b73890dfe5bbf0a5"
SRC_URI[langpacks-nb.sha256sum] = "f8f75a8934089fc25937cc73667212eaf5689f3f54e60c9b8f6de4470a015405"
SRC_URI[langpacks-ne.sha256sum] = "cfdbd1a146799feb0bf14c41effed55dc89921f9c900f1fd31b7f5dcfa63acf6"
SRC_URI[langpacks-nl.sha256sum] = "55c43f29fdd64ef8eb13b2c011a62879fb05dcf9d19ad86922fd014d8ad44fbf"
SRC_URI[langpacks-nn.sha256sum] = "57462cbc02de192df75e688bd76e20ec7e47ffca4e007140c923806f0f514d1a"
SRC_URI[langpacks-nr.sha256sum] = "be232413f3a48d766212e0bd35de44bdfa8174dc3a450e78e10c8bbce3f00bce"
SRC_URI[langpacks-nso.sha256sum] = "219e3bc9236bfed42557fbe8a83c657d0a3f2a4a138ca890993ac32216168899"
SRC_URI[langpacks-or.sha256sum] = "d539ac32289fd2592583d6c4ea818673a3dec34962553a8a0c89fabcb1d4212c"
SRC_URI[langpacks-pa.sha256sum] = "43fd5e63f05fb7a33cba30e70fe1d467b049a953317ae8edf86b064163a17904"
SRC_URI[langpacks-pl.sha256sum] = "e500c77b8487cc808e5f706e57ac535560e0771fe9830f1f287511e4268fc7ce"
SRC_URI[langpacks-pt.sha256sum] = "c704f978a4ca05e4540c2227ae46dba3bf063ca6c7d720476a01574df9757e7d"
SRC_URI[langpacks-pt_BR.sha256sum] = "ad5777654d6a8a89d4ae3e60ed6f2b199e43c00f09e8d23398ef9e1b0c51a7f3"
SRC_URI[langpacks-ro.sha256sum] = "0f5d3206922686668c8e2b7749044bfd67974e7f622c2907273f5e78066988ca"
SRC_URI[langpacks-ru.sha256sum] = "d65dfeac0cb49e086c2786a72245dca05901247ebb14ed8860ec6bf26cd7af64"
SRC_URI[langpacks-si.sha256sum] = "d462fd4ef5e7915c1ea5eabf523eb4424a47af44cb12949506705c8424650d4e"
SRC_URI[langpacks-sk.sha256sum] = "9c3c361b455154974c1bba9d7fb6243117e3ce7f308c38dfd3484ee07602ae87"
SRC_URI[langpacks-sl.sha256sum] = "ca4b70a43cfafeb971fffc9a1a8692c4bdb2bc2c5c55366b2acf592f813bbcca"
SRC_URI[langpacks-sq.sha256sum] = "abbdf0e82efae69e14d9e0e389ce637c9f88e06f4efa4f52ea79f3c094861cd4"
SRC_URI[langpacks-sr.sha256sum] = "759c3b07ecc496177317c50a802c03051e01a08f45327dfa3161169581e8df47"
SRC_URI[langpacks-ss.sha256sum] = "0aa0a830e4e05998e1074600150323e320a9e032dea010ec87bc678eefc5a710"
SRC_URI[langpacks-sv.sha256sum] = "d5776a54760a45980a6d3dc2561ab9988e8431a578ab991b5f904624d47aedb0"
SRC_URI[langpacks-ta.sha256sum] = "22b6a132f73d894b2bd072e216bb96806d940c63a4b5a1ea3543c0b3f050c917"
SRC_URI[langpacks-te.sha256sum] = "7e1ad3de668d1c8d616d0db2d3b7eeefab5e47dfdaa27029c66fde60d75177f1"
SRC_URI[langpacks-th.sha256sum] = "424e1bfe957268cb6ef6e2debba6580bde1e83c2115b5a397986787767423bd3"
SRC_URI[langpacks-tn.sha256sum] = "d618f7118875c9d3e89cc56f11e6a27e231e38d589cdd4beb45d71c936d68937"
SRC_URI[langpacks-tr.sha256sum] = "2dfc2f6677d4b9603e9b4be0d8a049bce9f07913e89be133cb467ecbdefd5894"
SRC_URI[langpacks-ts.sha256sum] = "f781c1d0f7c717626c12a152a409be342e5ef18d056c338caf8a31eede4dfaa1"
SRC_URI[langpacks-uk.sha256sum] = "286060358650fae249753ef4c60cc5a8209ff4af3ce0152c162e8a3ca2189964"
SRC_URI[langpacks-ur.sha256sum] = "33b10d3249a909d78e4503603f8b67800734d40c89f2714136c9d8896bfb6670"
SRC_URI[langpacks-ve.sha256sum] = "9369ce68ed46f270085e485516eda1e9764d3019b0c902b335ed30e41ea00117"
SRC_URI[langpacks-vi.sha256sum] = "9920086bcfac102901e085e2b9d980a69b2b2b4a205b4166d929f606653f5960"
SRC_URI[langpacks-xh.sha256sum] = "4268e496fb516820900024eb49ab6b88f33bf9f943bed50451a19b33887680d6"
SRC_URI[langpacks-zh_CN.sha256sum] = "d2f44ffa2510cea72a5b1e95bb2b16473573efa6b52c3d3090e52867c0d6d6bc"
SRC_URI[langpacks-zh_TW.sha256sum] = "1ce4eaf84778930cd2c32911e271cfbcd4f7611d49e16f6519816a9f394ac7c4"
SRC_URI[langpacks-zu.sha256sum] = "c85bfc18f2e5bbda6e0b470025be84ee455837673539bdf28ffde681da0cf0fc"
