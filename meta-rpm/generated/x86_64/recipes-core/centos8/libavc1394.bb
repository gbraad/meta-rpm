SUMMARY = "generated recipe based on libavc1394 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libraw1394 pkgconfig-native"
RPM_SONAME_PROV_libavc1394 = "libavc1394.so.0 librom1394.so.0"
RPM_SONAME_REQ_libavc1394 = "libc.so.6 libm.so.6 libraw1394.so.11"
RDEPENDS_libavc1394 = "glibc libraw1394"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libavc1394-0.5.4-7.el8.x86_64.rpm \
          "

SRC_URI[libavc1394.sha256sum] = "e001e30d5c1c740237ed5e119727fad55a32e222a001985637bacab191171061"
