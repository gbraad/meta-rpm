SUMMARY = "generated recipe based on libXtst srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "xtst"
DEPENDS = "libx11 libxext libxi pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXtst = "libXtst.so.6"
RPM_SONAME_REQ_libXtst = "libX11.so.6 libXext.so.6 libXi.so.6 libc.so.6"
RDEPENDS_libXtst = "glibc libX11 libXext libXi"
RPM_SONAME_REQ_libXtst-devel = "libXtst.so.6"
RPROVIDES_libXtst-devel = "libXtst-dev (= 1.2.3)"
RDEPENDS_libXtst-devel = "libX11-devel libXext-devel libXi-devel libXtst pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXtst-1.2.3-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXtst-devel-1.2.3-7.el8.x86_64.rpm \
          "

SRC_URI[libXtst.sha256sum] = "a19ecb3f3814649210b4667cf82ebca98b0d00e1b8222bc2f5aca2cc062999e6"
SRC_URI[libXtst-devel.sha256sum] = "39d8797cb21a5e22c57140ac46d9000f1224ab6aea707408bc58741614123af2"
