SUMMARY = "generated recipe based on java-11-openjdk srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & CLOSED & BSD & CLOSED & GPL-2.0 & GPL-2.0 & GPL-2.0 & CLOSED & LGPL-2.0 & MIT & MPL-2.0 & CLOSED & W3C & Zlib & ISC & CLOSED & RSA"
RPM_LICENSE = "ASL 1.1 and ASL 2.0 and BSD and BSD with advertising and GPL+ and GPLv2 and GPLv2 with exceptions and IJG and LGPLv2+ and MIT and MPLv2.0 and Public Domain and W3C and zlib and ISC and FTL and RSA"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib freetype giflib lcms2 libgcc libjpeg-turbo libpng libx11 libxext libxi libxrender libxtst pkgconfig-native zlib"
RPM_SONAME_REQ_java-11-openjdk = "libX11.so.6 libXext.so.6 libXi.so.6 libXrender.so.1 libXtst.so.6 libc.so.6 libdl.so.2 libgif.so.7 libjpeg.so.62 libm.so.6 libpng16.so.16 libpthread.so.0 libz.so.1"
RDEPENDS_java-11-openjdk = "bash fontconfig giflib glibc java-11-openjdk-headless libX11 libXcomposite libXext libXi libXrender libXtst libjpeg-turbo libpng xorg-x11-fonts-Type1 zlib"
RDEPENDS_java-11-openjdk-demo = "java-11-openjdk"
RPM_SONAME_REQ_java-11-openjdk-devel = "libc.so.6 libdl.so.2 libpthread.so.0 libz.so.1"
RPROVIDES_java-11-openjdk-devel = "java-11-openjdk-dev (= 11.0.9.11)"
RDEPENDS_java-11-openjdk-devel = "bash chkconfig glibc java-11-openjdk zlib"
RPM_SONAME_REQ_java-11-openjdk-headless = "ld-linux-x86-64.so.2 libasound.so.2 libc.so.6 libdl.so.2 libfreetype.so.6 libgcc_s.so.1 libjpeg.so.62 liblcms2.so.2 libm.so.6 libpthread.so.0 libstdc++.so.6 libthread_db.so.1 libz.so.1"
RDEPENDS_java-11-openjdk-headless = "alsa-lib bash ca-certificates chkconfig copy-jdk-configs cups-libs freetype glibc javapackages-filesystem lcms2 libgcc libjpeg-turbo libstdc++ lksctp-tools tzdata-java zlib"
RDEPENDS_java-11-openjdk-javadoc = "bash chkconfig javapackages-filesystem"
RDEPENDS_java-11-openjdk-javadoc-zip = "bash chkconfig javapackages-filesystem"
RDEPENDS_java-11-openjdk-jmods = "java-11-openjdk-devel"
RDEPENDS_java-11-openjdk-src = "java-11-openjdk-headless"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/java-11-openjdk-11.0.9.11-0.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/java-11-openjdk-demo-11.0.9.11-0.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/java-11-openjdk-devel-11.0.9.11-0.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/java-11-openjdk-headless-11.0.9.11-0.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/java-11-openjdk-javadoc-11.0.9.11-0.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/java-11-openjdk-javadoc-zip-11.0.9.11-0.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/java-11-openjdk-jmods-11.0.9.11-0.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/java-11-openjdk-src-11.0.9.11-0.el8_2.x86_64.rpm \
          "

SRC_URI[java-11-openjdk.sha256sum] = "e81b95a853c594edb43ddd9fad1d7920279419209251726cf81c9eb339fcc1be"
SRC_URI[java-11-openjdk-demo.sha256sum] = "f0fb9d94d05ec4b97ef132f58265fdfa88a5f8f9f861a0dac6465ecf77f5f87e"
SRC_URI[java-11-openjdk-devel.sha256sum] = "fafa750e5977c8111c5cc9b0f675265f03b5ebcadda3028beeb4d913b04d3540"
SRC_URI[java-11-openjdk-headless.sha256sum] = "5f7e1c006050cfc44675be0687a8f858a3421c40cf6db23e8acc55ed1f94f06d"
SRC_URI[java-11-openjdk-javadoc.sha256sum] = "9affce3eaf064ec74d878644063571ba191dd2c94a451c292677e0867166e5ea"
SRC_URI[java-11-openjdk-javadoc-zip.sha256sum] = "1ed54e38df3af0ce73bf41293ddc3820ffe467bd6ccf019b71c89b23f9a07014"
SRC_URI[java-11-openjdk-jmods.sha256sum] = "972a902fa347e7fd40bbc72785ce74b6b868f0e462df8aca8005dcf518b08b94"
SRC_URI[java-11-openjdk-src.sha256sum] = "0f3f4f732b0c0e279a6603b02e33b57c3e5daf1eba4b17b2b8a9f0a3def73a5b"
