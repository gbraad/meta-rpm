SUMMARY = "generated recipe based on python-flask srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-flask = "platform-python python3-click python3-itsdangerous python3-jinja2 python3-werkzeug"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-flask-0.12.2-4.el8.noarch.rpm \
          "

SRC_URI[python3-flask.sha256sum] = "0a57b21c725f927ea9193ec873917ca91f60cb25f2ca6cb0d97b0e6edd9f3ce1"
