SUMMARY = "generated recipe based on oddjob srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-libs libselinux libxml2 pam pkgconfig-native"
RPM_SONAME_REQ_oddjob = "libc.so.6 libdbus-1.so.3 libselinux.so.1 libxml2.so.2"
RDEPENDS_oddjob = "bash dbus dbus-libs glibc libselinux libxml2 psmisc systemd"
RPM_SONAME_REQ_oddjob-mkhomedir = "libc.so.6 libdbus-1.so.3 libpam.so.0 libselinux.so.1"
RDEPENDS_oddjob-mkhomedir = "bash dbus-libs dbus-tools glibc grep libselinux oddjob pam psmisc sed"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/oddjob-0.34.4-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/oddjob-mkhomedir-0.34.4-7.el8.x86_64.rpm \
          "

SRC_URI[oddjob.sha256sum] = "19732823550aa1d0408f834127b5fd73f4ea3ba2f4d4f223f0d3abae4c8402f4"
SRC_URI[oddjob-mkhomedir.sha256sum] = "c36fea457421a7fbf00362001a0645e74f7f4af413b36c294248c6daf5c44154"
