SUMMARY = "generated recipe based on hunspell-uz srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-uz = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-uz-0.6-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-uz.sha256sum] = "ea1f33d70e7107351024363351a6e8ab4f151602f2ca728aa6f90b7e975c5d18"
