SUMMARY = "generated recipe based on libkcapi srpm"
DESCRIPTION = "Description"
LICENSE = "BSD | GPL-2.0"
RPM_LICENSE = "BSD or GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libkcapi = "libkcapi.so.1"
RPM_SONAME_REQ_libkcapi = "libc.so.6"
RDEPENDS_libkcapi = "glibc systemd"
RPM_SONAME_REQ_libkcapi-hmaccalc = "libc.so.6 libdl.so.2 libkcapi.so.1"
RDEPENDS_libkcapi-hmaccalc = "glibc libkcapi"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libkcapi-1.1.1-16_1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libkcapi-hmaccalc-1.1.1-16_1.el8.x86_64.rpm \
          "

SRC_URI[libkcapi.sha256sum] = "5ad05773c3f244d697024367129b2fffdc639f34e298806215f894bb06a233a1"
SRC_URI[libkcapi-hmaccalc.sha256sum] = "ace3fc0e68568a10d8d64711eb62c06f7334333b5b13cec8d9ee7e7f8ae3aaed"
