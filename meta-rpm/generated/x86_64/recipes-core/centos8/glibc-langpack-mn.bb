SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-mn = "locale-base-mn-mn (= 2.28) virtual-locale-mn (= 2.28) virtual-locale-mn-mn (= 2.28)"
RDEPENDS_glibc-langpack-mn = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-mn-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-mn.sha256sum] = "8d5a35be063f697a4d550b9a6a0b075350a718b8e1b9c4c86adc5a8c48dbe565"
