SUMMARY = "generated recipe based on isomd5sum srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native popt"
RPM_SONAME_REQ_isomd5sum = "libc.so.6 libpopt.so.0"
RDEPENDS_isomd5sum = "glibc popt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/isomd5sum-1.2.3-3.el8.x86_64.rpm \
          "

SRC_URI[isomd5sum.sha256sum] = "f8ab9e51914b89f1639f238f4e13cbeccd0847edb8975fee556b0d2208cd18e5"
