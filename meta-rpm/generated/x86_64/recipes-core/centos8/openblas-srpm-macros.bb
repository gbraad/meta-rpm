SUMMARY = "generated recipe based on openblas-srpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/openblas-srpm-macros-2-2.el8.noarch.rpm \
          "

SRC_URI[openblas-srpm-macros.sha256sum] = "e551afb45f48dcd047181fea948b9a643c10de10b8869688171d4572794cd4d1"
