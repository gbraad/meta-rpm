SUMMARY = "generated recipe based on expect srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native tcl"
RPM_SONAME_PROV_expect = "libexpect5.45.4.so"
RPM_SONAME_REQ_expect = "libc.so.6 libdl.so.2 libm.so.6 libtcl8.6.so libutil.so.1"
RDEPENDS_expect = "bash glibc tcl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/expect-5.45.4-5.el8.x86_64.rpm \
          "

SRC_URI[expect.sha256sum] = "a7daab0d5163c8777b75acda81f1ed0ea0bb2f9ae11bd9cd5960629d4630047f"
