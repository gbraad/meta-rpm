SUMMARY = "generated recipe based on perl-Net-DNS srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & MIT"
RPM_LICENSE = "(GPL+ or Artistic) and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Net-DNS = "perl-Carp perl-Data-Dumper perl-Digest-HMAC perl-Digest-MD5 perl-Digest-SHA perl-Encode perl-Exporter perl-IO perl-MIME-Base64 perl-Time-Local perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Net-DNS-1.15-1.el8.noarch.rpm \
          "

SRC_URI[perl-Net-DNS.sha256sum] = "2c0c55297ab15537c140e25b468c66dc87e8b62a75ffd87703e023d921f7ea81"
