SUMMARY = "generated recipe based on qt5 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native qt5-qt3d qt5-qtbase qt5-qtconnectivity qt5-qtdeclarative qt5-qtlocation qt5-qtmultimedia qt5-qtscript qt5-qtsensors qt5-qtserialport qt5-qtsvg qt5-qttools qt5-qtwayland qt5-qtwebchannel qt5-qtwebsockets qt5-qtx11extras qt5-qtxmlpatterns"
RPROVIDES_qt5-devel = "qt5-dev (= 5.12.5)"
RDEPENDS_qt5-devel = "qt5-designer qt5-doctools qt5-linguist qt5-qt3d-devel qt5-qtbase-devel qt5-qtbase-static qt5-qtconnectivity-devel qt5-qtdeclarative-devel qt5-qtdeclarative-static qt5-qtlocation-devel qt5-qtmultimedia-devel qt5-qtscript-devel qt5-qtsensors-devel qt5-qtserialport-devel qt5-qtsvg-devel qt5-qttools-devel qt5-qttools-static qt5-qtwayland-devel qt5-qtwebchannel-devel qt5-qtwebsockets-devel qt5-qtx11extras-devel qt5-qtxmlpatterns-devel qt5-rpm-macros"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qt5-devel-5.12.5-3.el8.noarch.rpm \
          "

SRC_URI[qt5-devel.sha256sum] = "17e5f0eb737e57912c09752854d6d81ea2e283d11b1268b27a6703e2193a2eb8"
