SUMMARY = "generated recipe based on gupnp-av srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libxml2 pkgconfig-native"
RPM_SONAME_PROV_gupnp-av = "libgupnp-av-1.0.so.2"
RPM_SONAME_REQ_gupnp-av = "libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libxml2.so.2"
RDEPENDS_gupnp-av = "glib2 glibc libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gupnp-av-0.12.10-6.el8.x86_64.rpm \
          "

SRC_URI[gupnp-av.sha256sum] = "3dfa986e489ffcdc69684b13ce28b9f6e3f776f58e6cf3b8af03f4f1ad7ae7d7"
