SUMMARY = "generated recipe based on libvirt-dbus srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libgcc libvirt libvirt-glib pkgconfig-native"
RPM_SONAME_REQ_libvirt-dbus = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libvirt-glib-1.0.so.0 libvirt.so.0"
RDEPENDS_libvirt-dbus = "bash dbus glib2 glibc libgcc libvirt-glib libvirt-libs polkit shadow-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvirt-dbus-1.2.0-3.module_el8.2.0+320+13f867d7.x86_64.rpm \
          "

SRC_URI[libvirt-dbus.sha256sum] = "174f7e6da61621c992a056649314554eb445764981a4d1f8cc6e123cb8d091a9"
