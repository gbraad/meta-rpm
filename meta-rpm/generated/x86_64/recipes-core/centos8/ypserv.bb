SUMMARY = "generated recipe based on ypserv srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libnsl2 libtirpc libxcrypt pkgconfig-native systemd-libs tokyocabinet"
RPM_SONAME_REQ_ypserv = "libc.so.6 libcrypt.so.1 libnsl.so.2 libsystemd.so.0 libtirpc.so.3 libtokyocabinet.so.9"
RDEPENDS_ypserv = "bash gawk glibc hostname libnsl2 libtirpc libxcrypt make rpcbind systemd systemd-libs tokyocabinet"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ypserv-4.0-6.20170331git5bfba76.el8.x86_64.rpm \
          "

SRC_URI[ypserv.sha256sum] = "1256b691e1a033b1ec2c6a580c98203a35e0f1d37d68279337250a404652efb0"
