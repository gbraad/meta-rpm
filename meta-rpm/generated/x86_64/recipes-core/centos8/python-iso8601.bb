SUMMARY = "generated recipe based on python-iso8601 srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python2-iso8601 = "platform-python"
RDEPENDS_python3-iso8601 = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python2-iso8601-0.1.11-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-iso8601-0.1.11-9.el8.noarch.rpm \
          "

SRC_URI[python2-iso8601.sha256sum] = "85f8d0f47740d2dea34d2385fcd7715dba9ad8681e92fdadf6d12afc2a2d98f6"
SRC_URI[python3-iso8601.sha256sum] = "440cf4e541f6a66840c4cccb0313c2e6d54edb3b7175266b3ef750b64bcfd59a"
