SUMMARY = "generated recipe based on libXv srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXv = "libXv.so.1"
RPM_SONAME_REQ_libXv = "libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXv = "glibc libX11 libXext"
RPM_SONAME_REQ_libXv-devel = "libXv.so.1"
RPROVIDES_libXv-devel = "libXv-dev (= 1.0.11)"
RDEPENDS_libXv-devel = "libX11-devel libXext-devel libXv pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXv-1.0.11-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXv-devel-1.0.11-7.el8.x86_64.rpm \
          "

SRC_URI[libXv.sha256sum] = "e04aeb7921dc1864379f670172c69d2e6241c0ca602b7bdee42079596910a4c3"
SRC_URI[libXv-devel.sha256sum] = "a1d8d34b2982edbafa03f61bfa729150159fbdefbafa94c964b0633740aa9806"
