SUMMARY = "generated recipe based on pmix srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libevent munge pkgconfig-native"
RPM_SONAME_PROV_pmix = "libpmi.so.1 libpmi2.so.1 libpmix.so.2"
RPM_SONAME_REQ_pmix = "libc.so.6 libdl.so.2 libevent-2.1.so.6 libevent_pthreads-2.1.so.6 libm.so.6 libmunge.so.2 libpthread.so.0"
RDEPENDS_pmix = "environment-modules glibc libevent munge-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pmix-2.1.1-1.el8.x86_64.rpm \
          "

SRC_URI[pmix.sha256sum] = "c5ec68ffea1deb9225f0f881c9ef7b9c2da8a45b90dc3d25bb905261436abdc8"
