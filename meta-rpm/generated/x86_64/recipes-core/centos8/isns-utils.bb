SUMMARY = "generated recipe based on isns-utils srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_isns-utils = "libc.so.6 libcrypto.so.1.1 libisns.so.0"
RDEPENDS_isns-utils = "bash glibc isns-utils-libs openssl-libs systemd"
RPM_SONAME_REQ_isns-utils-devel = "libisns.so.0"
RPROVIDES_isns-utils-devel = "isns-utils-dev (= 0.99)"
RDEPENDS_isns-utils-devel = "isns-utils-libs"
RPM_SONAME_PROV_isns-utils-libs = "libisns.so.0"
RPM_SONAME_REQ_isns-utils-libs = "libc.so.6 libcrypto.so.1.1"
RDEPENDS_isns-utils-libs = "glibc openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/isns-utils-0.99-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/isns-utils-devel-0.99-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/isns-utils-libs-0.99-1.el8.x86_64.rpm \
          "

SRC_URI[isns-utils.sha256sum] = "c3ce93139952196a1bc5e92ac592ab257af8b65170e032d8cd9d3994fa0913b2"
SRC_URI[isns-utils-devel.sha256sum] = "7bfb1644943cd830f7aa6827e3950e7ba0173e4d70bbcd65615f7987f7810d2c"
SRC_URI[isns-utils-libs.sha256sum] = "5830a9484eb786849dd73fce6f2b20d5d42e779d687842f60c8b588c962e5e40"
