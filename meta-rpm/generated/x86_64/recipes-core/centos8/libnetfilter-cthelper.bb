SUMMARY = "generated recipe based on libnetfilter_cthelper srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libmnl pkgconfig-native"
RPM_SONAME_PROV_libnetfilter_cthelper = "libnetfilter_cthelper.so.0"
RPM_SONAME_REQ_libnetfilter_cthelper = "libc.so.6 libmnl.so.0"
RDEPENDS_libnetfilter_cthelper = "glibc libmnl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libnetfilter_cthelper-1.0.0-15.el8.x86_64.rpm \
          "

SRC_URI[libnetfilter_cthelper.sha256sum] = "1ff19864aecd9d21527e14cd1a254a42eb2296967544a03e2572358fcd9a7912"
