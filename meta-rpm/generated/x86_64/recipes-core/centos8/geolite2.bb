SUMMARY = "generated recipe based on geolite2 srpm"
DESCRIPTION = "Description"
LICENSE = "CC-BY-SA-1.0"
RPM_LICENSE = "CC-BY-SA"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/geolite2-city-20180605-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/geolite2-country-20180605-1.el8.noarch.rpm \
          "

SRC_URI[geolite2-city.sha256sum] = "cad86777bcb265db0da766952ff63e8d33f0fd4f3b90b22d0a1da587a785db44"
SRC_URI[geolite2-country.sha256sum] = "67419432fe7d373f8e5ddaa7b0dd1c25389608bf2f4ee76dbabcc2d96eb8c9d0"
