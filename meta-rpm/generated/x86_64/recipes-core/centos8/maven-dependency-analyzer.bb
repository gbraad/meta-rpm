SUMMARY = "generated recipe based on maven-dependency-analyzer srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-dependency-analyzer = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-project objectweb-asm plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-dependency-analyzer-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-dependency-analyzer-1.8-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-dependency-analyzer-javadoc-1.8-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-dependency-analyzer.sha256sum] = "bd711203ac11d9e72cfb6128aafb57199f1838f53dd100cd016cc9e062784319"
SRC_URI[maven-dependency-analyzer-javadoc.sha256sum] = "425ccf248d10e1e823a0d1ea58894fc6e61b231e492f64b55b1318ee9d57906e"
