SUMMARY = "generated recipe based on memcached srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cyrus-sasl-lib libevent pkgconfig-native"
RPM_SONAME_REQ_memcached = "libc.so.6 libevent-2.1.so.6 libpthread.so.0 libsasl2.so.3"
RDEPENDS_memcached = "bash cyrus-sasl-lib glibc libevent perl-IO perl-interpreter perl-libs shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/memcached-1.5.9-3.el8.x86_64.rpm \
          "

SRC_URI[memcached.sha256sum] = "5232b245bbcd1ef7264c78f21144e1fa6575de1f24f9115dc69fa34022fad6e9"
