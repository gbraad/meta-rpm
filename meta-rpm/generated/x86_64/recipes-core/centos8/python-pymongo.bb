SUMMARY = "generated recipe based on python-pymongo srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & MIT"
RPM_LICENSE = "ASL 2.0 and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-bson = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-bson = "glibc platform-python python3-libs"
RPM_SONAME_REQ_python3-pymongo = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-pymongo = "glibc platform-python python3-bson python3-libs"
RDEPENDS_python3-pymongo-gridfs = "platform-python python3-pymongo"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python-pymongo-doc-3.6.1-11.module_el8.1.0+245+c39af44f.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-bson-3.6.1-11.module_el8.1.0+245+c39af44f.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pymongo-3.6.1-11.module_el8.1.0+245+c39af44f.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pymongo-gridfs-3.6.1-11.module_el8.1.0+245+c39af44f.x86_64.rpm \
          "

SRC_URI[python-pymongo-doc.sha256sum] = "bdbe6138d3b5e266be82dc935a32d4a80eb85849c5b035cd5a3de410d0227fea"
SRC_URI[python3-bson.sha256sum] = "63525a4fd0b0d392ff1164fe17cc28569e48d692122660b45050be040f3c7c5e"
SRC_URI[python3-pymongo.sha256sum] = "35b43dd16ac3856df8019453a954e0e462aeb9888bc8089495ff1e48f0e3162c"
SRC_URI[python3-pymongo-gridfs.sha256sum] = "442b74f274ded0b07a4f78abfbf8613f92bb13438b13e44efcbfbc294a21f5e3"
