SUMMARY = "generated recipe based on gnome-video-effects srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_gnome-video-effects = "frei0r-plugins pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-video-effects-0.4.3-3.el8.noarch.rpm \
          "

SRC_URI[gnome-video-effects.sha256sum] = "65009f27aa6df6d284851feff1a51832a7c33dc589636f2eced2cadb041ff830"
