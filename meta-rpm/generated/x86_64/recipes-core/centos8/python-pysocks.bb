SUMMARY = "generated recipe based on python-pysocks srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pysocks = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-pysocks-1.6.8-3.el8.noarch.rpm \
          "

SRC_URI[python3-pysocks.sha256sum] = "7f506879c64e0bb4d98782d025f46fb9a07517487ae4cbebbb3985a98f4e2154"
