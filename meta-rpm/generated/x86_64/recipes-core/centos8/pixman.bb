SUMMARY = "generated recipe based on pixman srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_pixman = "libpixman-1.so.0"
RPM_SONAME_REQ_pixman = "ld-linux-x86-64.so.2 libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_pixman = "glibc"
RPM_SONAME_REQ_pixman-devel = "libpixman-1.so.0"
RPROVIDES_pixman-devel = "pixman-dev (= 0.38.4)"
RDEPENDS_pixman-devel = "pixman pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pixman-0.38.4-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pixman-devel-0.38.4-1.el8.x86_64.rpm \
          "

SRC_URI[pixman.sha256sum] = "ddbbf3a8191dbc1a9fcb67ccf9cea0d34dbe9bbb74780e1359933cd03ee24451"
SRC_URI[pixman-devel.sha256sum] = "2594005ae46ee2ca21da2bc824acfd791a61e01144339ab68fde4c71a68f0339"
