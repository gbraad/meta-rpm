SUMMARY = "generated recipe based on openjade srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "DMIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc opensp pkgconfig-native"
RPM_SONAME_PROV_openjade = "libogrove.so.0 libospgrove.so.0 libostyle.so.0"
RPM_SONAME_REQ_openjade = "libc.so.6 libgcc_s.so.1 libm.so.6 libosp.so.5 libstdc++.so.6"
RDEPENDS_openjade = "bash glibc libgcc libstdc++ opensp sgml-common"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/openjade-1.3.2-57.el8.x86_64.rpm \
          "

SRC_URI[openjade.sha256sum] = "e42451774d22516bbfe9bc8686c1ff3388a30cc8424c109153378aa2b9a2dc27"
