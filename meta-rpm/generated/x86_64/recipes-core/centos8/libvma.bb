SUMMARY = "generated recipe based on libvma srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | BSD"
RPM_LICENSE = "GPLv2 or BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libnl pkgconfig-native rdma-core"
RPM_SONAME_PROV_libvma = "libvma.so.8"
RPM_SONAME_REQ_libvma = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libibverbs.so.1 libm.so.6 libmlx5.so.1 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 librdmacm.so.1 librt.so.1 libstdc++.so.6"
RDEPENDS_libvma = "bash glibc libgcc libibverbs libnl3 librdmacm libstdc++ pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libvma-8.9.5-1.el8.x86_64.rpm \
          "

SRC_URI[libvma.sha256sum] = "3959ccd90abc2aa5b6327e80994dacbe2da40306b40987551be0705403816aa4"
