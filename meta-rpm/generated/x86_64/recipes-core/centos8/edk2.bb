SUMMARY = "generated recipe based on edk2 srpm"
DESCRIPTION = "Description"
LICENSE = "BSD-2-Clause-Patent & OpenSSL"
RPM_LICENSE = "BSD-2-Clause-Patent and OpenSSL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/edk2-ovmf-20190829git37eef91017ad-9.el8.noarch.rpm \
          "

SRC_URI[edk2-ovmf.sha256sum] = "d5d2d8f5e3bfac8671276e38ab99aea1eb935cfa8e5a03e96f35a5461a300f72"
