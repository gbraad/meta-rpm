SUMMARY = "generated recipe based on tpm2-tss srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcrypt pkgconfig-native"
RPM_SONAME_PROV_tpm2-tss = "libtss2-esys.so.0 libtss2-mu.so.0 libtss2-sys.so.0 libtss2-tcti-device.so.0 libtss2-tcti-mssim.so.0"
RPM_SONAME_REQ_tpm2-tss = "libc.so.6 libdl.so.2 libgcrypt.so.20"
RDEPENDS_tpm2-tss = "glibc libgcrypt"
RPM_SONAME_REQ_tpm2-tss-devel = "libtss2-esys.so.0 libtss2-mu.so.0 libtss2-sys.so.0 libtss2-tcti-device.so.0 libtss2-tcti-mssim.so.0"
RPROVIDES_tpm2-tss-devel = "tpm2-tss-dev (= 2.0.0)"
RDEPENDS_tpm2-tss-devel = "pkgconf-pkg-config tpm2-tss"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tpm2-tss-2.0.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tpm2-tss-devel-2.0.0-4.el8.x86_64.rpm \
          "

SRC_URI[tpm2-tss.sha256sum] = "3a1851014a93a3ea959241b9e04e5a2be29b03b83d3c29ab0c432130319b71ca"
SRC_URI[tpm2-tss-devel.sha256sum] = "b4521228d9470b53a94579842278253c66dff44b1d9ae9df41e0aa1e897f4919"
