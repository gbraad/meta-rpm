SUMMARY = "generated recipe based on perl-Canary-Stability srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Canary-Stability = "perl-ExtUtils-MakeMaker perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Canary-Stability-2012-5.el8.noarch.rpm \
          "

SRC_URI[perl-Canary-Stability.sha256sum] = "1ea3a39e5d463ded78b898767b1d78871855b4e69585937c05634b6fff1743ed"
