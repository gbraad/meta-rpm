SUMMARY = "generated recipe based on libproxy srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libgcc pkgconfig-native webkit2gtk3"
RPM_SONAME_REQ_libproxy-webkitgtk4 = "libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libjavascriptcoregtk-4.0.so.18 libm.so.6 libproxy.so.1 libstdc++.so.6"
RDEPENDS_libproxy-webkitgtk4 = "glib2 glibc libgcc libproxy libstdc++ webkit2gtk3-jsc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libproxy-webkitgtk4-0.4.15-5.2.el8.x86_64.rpm \
          "

SRC_URI[libproxy-webkitgtk4.sha256sum] = "3ad2c9ed573e020cfbaa1b7eb6c6e345bb46cd2e4c1809f5cc5166df4bfb57ad"
