SUMMARY = "generated recipe based on maven-plugin-testing srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-plugin-testing = "java-1.8.0-openjdk-headless javapackages-filesystem maven-parent"
RDEPENDS_maven-plugin-testing-harness = "apache-commons-io java-1.8.0-openjdk-headless javapackages-filesystem plexus-archiver"
RDEPENDS_maven-plugin-testing-javadoc = "javapackages-filesystem"
RDEPENDS_maven-plugin-testing-tools = "java-1.8.0-openjdk-headless javapackages-filesystem maven-invoker"
RDEPENDS_maven-test-tools = "easymock java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-plugin-testing-3.3.0-12.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-plugin-testing-harness-3.3.0-12.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-plugin-testing-javadoc-3.3.0-12.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-plugin-testing-tools-3.3.0-12.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-test-tools-3.3.0-12.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-plugin-testing.sha256sum] = "a15585947d11ccbfa476e979be19bdf904f149431c6a42ef5655febc75fe2fd2"
SRC_URI[maven-plugin-testing-harness.sha256sum] = "fb5532d0dd805eb905b397d602a85c1457c48641c9611fac014dc897090ea4c3"
SRC_URI[maven-plugin-testing-javadoc.sha256sum] = "2b5bbe5b2b42d9ddbcaaa29d9cf92db81755de293a6ded585ed98ec314c50b96"
SRC_URI[maven-plugin-testing-tools.sha256sum] = "e8a2bd5653d6661fc4321f52c9e076c84698ae5e81e3462a4d35218d02f11629"
SRC_URI[maven-test-tools.sha256sum] = "d33295d62ddf44a275032b42a75472b228e132476a4c10e70e7dc4e49a9e63be"
