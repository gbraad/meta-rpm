SUMMARY = "generated recipe based on apache-commons-lang3 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-lang3 = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-lang3-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-commons-lang3-3.7-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-commons-lang3-javadoc-3.7-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-lang3.sha256sum] = "87bddb6fdf0309dbd3a4f1f7583ff1f9617e18bfafd8ef136316360671f48509"
SRC_URI[apache-commons-lang3-javadoc.sha256sum] = "1ba336398d1d3c62ffc701f2736c3ef63d97a65c86b7081bf3ce2f91dac6bd1d"
