SUMMARY = "generated recipe based on python-justbytes srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-justbytes = "platform-python python3-justbases python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-justbytes-0.11-2.el8.noarch.rpm \
          "

SRC_URI[python3-justbytes.sha256sum] = "a8da0db210f0fb058dceb8d3fcca698de432b5874c37731e961fca2a0ca99d51"
