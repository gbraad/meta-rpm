SUMMARY = "generated recipe based on rpm srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
DEPENDS = "acl audit-libs bzip2 db elfutils file ima-evm-utils libarchive libcap lua openssl pkgconfig-native platform-python3 popt xz zlib zstd"
RPM_SONAME_REQ_python3-rpm = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libdw.so.1 libelf.so.1 libimaevm.so.0 liblua-5.3.so liblzma.so.5 libm.so.6 libmagic.so.1 libpopt.so.0 libpthread.so.0 libpython3.6m.so.1.0 librpm.so.8 librpmbuild.so.8 librpmio.so.8 librpmsign.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_python3-rpm = "audit-libs bzip2-libs elfutils-libelf elfutils-libs file-libs glibc ima-evm-utils libacl libcap libdb libzstd lua-libs openssl-libs platform-python popt python3-libs rpm-build-libs rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm = "libacl.so.1 libarchive.so.13 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm = "audit-libs bash bzip2-libs coreutils curl elfutils-libelf glibc libacl libarchive libcap libdb libdb-utils libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_PROV_rpm-build-libs = "librpmbuild.so.8 librpmsign.so.8"
RPM_SONAME_REQ_rpm-build-libs = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libdw.so.1 libelf.so.1 libimaevm.so.0 liblua-5.3.so liblzma.so.5 libm.so.6 libmagic.so.1 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-build-libs = "audit-libs bzip2-libs elfutils-libelf elfutils-libs file-libs glibc gnupg2 ima-evm-utils libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RDEPENDS_rpm-cron = "bash crontabs logrotate rpm"
RPM_SONAME_PROV_rpm-libs = "librpm.so.8 librpmio.so.8"
RPM_SONAME_REQ_rpm-libs = "ld-linux-x86-64.so.2 libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-libs = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm xz-libs zlib"
RPM_SONAME_REQ_rpm-sign = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 libimaevm.so.0 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 librpmsign.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-sign = "audit-libs bzip2-libs elfutils-libelf glibc ima-evm-utils libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-build-libs rpm-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-rpm-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-apidocs-4.14.2-37.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-build-libs-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-cron-4.14.2-37.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-libs-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-sign-4.14.2-37.el8.x86_64.rpm \
          "

SRC_URI[python3-rpm.sha256sum] = "a5a9510004eb53f67f5efc3788e5cfba675f09e56cd419b33eaaae7033bcf90e"
SRC_URI[rpm.sha256sum] = "7d8d850130c6772c0921693233aada04b42fa9d71bb2a8c41ea0ab42dda2b81b"
SRC_URI[rpm-apidocs.sha256sum] = "f36fd2c3094c35fe1f75e2991d33e598991810aea9135d60e32b264c17b34739"
SRC_URI[rpm-build-libs.sha256sum] = "712a169d7a35a5c14bee17f492f594465fcdd604295ac75642531f1f64d589f7"
SRC_URI[rpm-cron.sha256sum] = "601f9565125f201503dbba97accc4215c157b29ecf8087124b02a7b1e4cf073d"
SRC_URI[rpm-libs.sha256sum] = "29ac185f858263a69104489c09f240eca59625acf5e2d239303ff720cebad90e"
SRC_URI[rpm-sign.sha256sum] = "bc94e1ee80be917814c312419a907eba81c81a4c77aeee54e4cab76a27d14db5"
