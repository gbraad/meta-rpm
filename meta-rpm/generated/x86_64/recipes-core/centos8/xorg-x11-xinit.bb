SUMMARY = "generated recipe based on xorg-x11-xinit srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-xinit = "libX11.so.6 libc.so.6"
RDEPENDS_xorg-x11-xinit = "bash coreutils glibc libX11 xorg-x11-server-utils xorg-x11-xauth"
RDEPENDS_xorg-x11-xinit-session = "bash"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-xinit-1.3.4-18.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-xinit-session-1.3.4-18.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-xinit.sha256sum] = "1c77aa7bbca7567cd1783b61274a087dbcdbc87d8ee4d07b9e42c7a048bf7ee6"
SRC_URI[xorg-x11-xinit-session.sha256sum] = "498b515d4efa8b9e0e1734ac6718252e6f9b78b30d822baeae5fedcad495ea26"
