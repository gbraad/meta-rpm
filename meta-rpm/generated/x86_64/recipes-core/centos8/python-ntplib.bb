SUMMARY = "generated recipe based on python-ntplib srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-ntplib = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-ntplib-0.3.3-10.el8.noarch.rpm \
          "

SRC_URI[python3-ntplib.sha256sum] = "89ced6781ae54983063c73fde6699f0d1d8997704c6c8cc6edc5c980cecb2352"
