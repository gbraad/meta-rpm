SUMMARY = "generated recipe based on sanlock srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2 and GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libaio libblkid libuuid pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-sanlock = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 libsanlock.so.1"
RDEPENDS_python3-sanlock = "glibc platform-python python3-libs sanlock-lib"
RPM_SONAME_REQ_sanlk-reset = "libc.so.6 libsanlock.so.1 libwdmd.so.1"
RDEPENDS_sanlk-reset = "glibc sanlock sanlock-lib"
RPM_SONAME_REQ_sanlock = "libaio.so.1 libblkid.so.1 libc.so.6 libpthread.so.0 librt.so.1 libsanlock.so.1 libuuid.so.1 libwdmd.so.1"
RDEPENDS_sanlock = "bash glibc libaio libblkid libuuid sanlock-lib shadow-utils systemd"
RPM_SONAME_REQ_sanlock-devel = "libsanlock.so.1 libsanlock_client.so.1 libwdmd.so.1"
RPROVIDES_sanlock-devel = "sanlock-dev (= 3.8.0)"
RDEPENDS_sanlock-devel = "pkgconf-pkg-config sanlock-lib"
RPM_SONAME_PROV_sanlock-lib = "libsanlock.so.1 libsanlock_client.so.1 libwdmd.so.1"
RPM_SONAME_REQ_sanlock-lib = "libaio.so.1 libblkid.so.1 libc.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_sanlock-lib = "glibc libaio libblkid"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-sanlock-3.8.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sanlk-reset-3.8.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sanlock-3.8.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sanlock-lib-3.8.0-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/sanlock-devel-3.8.0-2.el8.x86_64.rpm \
          "

SRC_URI[python3-sanlock.sha256sum] = "ef6eb9096fc1cbf45e6220f6c52bab5a73dddbdc7633bdcc7acde5642e8c970f"
SRC_URI[sanlk-reset.sha256sum] = "834b191bc8b2767714b4630219ba27d198af7caf15bbfe9a4658a1910e5b6124"
SRC_URI[sanlock.sha256sum] = "f6b760fd5eda09ce692f27b15d76817599191d7afaf6a6326a723c839c16fcf2"
SRC_URI[sanlock-devel.sha256sum] = "e621f0746206aa4c55bbbdef48ed0f37f93bf74fe3cb75cdf7689e3c84bc68e1"
SRC_URI[sanlock-lib.sha256sum] = "bf3cf656c6d61193d8372478c4149ee440e83588c692e8de21e72a58724402bb"
