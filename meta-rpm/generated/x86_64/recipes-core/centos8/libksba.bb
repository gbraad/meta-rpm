SUMMARY = "generated recipe based on libksba srpm"
DESCRIPTION = "Description"
LICENSE = "(LGPL-3.0 | GPL-2.0) & GPL-3.0"
RPM_LICENSE = "(LGPLv3+ or GPLv2+) and GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgpg-error pkgconfig-native"
RPM_SONAME_PROV_libksba = "libksba.so.8"
RPM_SONAME_REQ_libksba = "libc.so.6 libgpg-error.so.0"
RDEPENDS_libksba = "glibc libgpg-error"
RPM_SONAME_REQ_libksba-devel = "libksba.so.8"
RPROVIDES_libksba-devel = "libksba-dev (= 1.3.5)"
RDEPENDS_libksba-devel = "bash info libksba"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libksba-1.3.5-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libksba-devel-1.3.5-7.el8.x86_64.rpm \
          "

SRC_URI[libksba.sha256sum] = "e6d3476e9996fb49632744be169f633d92900f5b7151db233501167a9018d240"
SRC_URI[libksba-devel.sha256sum] = "2130da2762cdaba5f37330a5585c6dcfa7db23f174ad229fa0ac1f2d99a258f5"
