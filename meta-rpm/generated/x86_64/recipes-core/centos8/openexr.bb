SUMMARY = "generated recipe based on OpenEXR srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ilmbase libgcc pkgconfig-native zlib"
RPM_SONAME_REQ_OpenEXR-devel = "libIlmImf-2_2.so.22 libIlmImfUtil-2_2.so.22"
RPROVIDES_OpenEXR-devel = "OpenEXR-dev (= 2.2.0)"
RDEPENDS_OpenEXR-devel = "OpenEXR-libs ilmbase-devel pkgconf-pkg-config"
RPM_SONAME_PROV_OpenEXR-libs = "libIlmImf-2_2.so.22 libIlmImfUtil-2_2.so.22"
RPM_SONAME_REQ_OpenEXR-libs = "libHalf.so.12 libIex-2_2.so.12 libIexMath-2_2.so.12 libIlmThread-2_2.so.12 libImath-2_2.so.12 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6 libz.so.1"
RDEPENDS_OpenEXR-libs = "glibc ilmbase libgcc libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/OpenEXR-libs-2.2.0-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/OpenEXR-devel-2.2.0-11.el8.x86_64.rpm \
          "

SRC_URI[OpenEXR-devel.sha256sum] = "5908473f3e90c1c7c01838efe11a72a54fb89315510ee5b08795cbb62b365883"
SRC_URI[OpenEXR-libs.sha256sum] = "db2fc6fba78f9be40cb7d4f21d69ba339f17573647d41e8a6ddbfc644a8ed80c"
