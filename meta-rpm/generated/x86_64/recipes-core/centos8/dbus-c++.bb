SUMMARY = "generated recipe based on dbus-c++ srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus dbus-libs expat glib-2.0 libgcc pkgconfig-native"
RPM_SONAME_PROV_dbus-c++ = "libdbus-c++-1.so.0"
RPM_SONAME_REQ_dbus-c++ = "libc.so.6 libdbus-1.so.3 libexpat.so.1 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_dbus-c++ = "dbus-libs expat glibc libgcc libstdc++"
RPM_SONAME_REQ_dbus-c++-devel = "libdbus-c++-1.so.0 libdbus-c++-glib-1.so.0"
RPROVIDES_dbus-c++-devel = "dbus-c++-dev (= 0.9.0)"
RDEPENDS_dbus-c++-devel = "dbus-c++ dbus-c++-glib dbus-devel pkgconf-pkg-config"
RPM_SONAME_PROV_dbus-c++-glib = "libdbus-c++-glib-1.so.0"
RPM_SONAME_REQ_dbus-c++-glib = "libc.so.6 libdbus-1.so.3 libgcc_s.so.1 libglib-2.0.so.0 libm.so.6 libstdc++.so.6"
RDEPENDS_dbus-c++-glib = "dbus-c++ dbus-libs glib2 glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dbus-c++-0.9.0-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dbus-c++-devel-0.9.0-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dbus-c++-glib-0.9.0-17.el8.x86_64.rpm \
          "

SRC_URI[dbus-c++.sha256sum] = "2dcba819a3bb952d9fff38f810741ccae6ba2dcc5226b5e63c74b15b5c3da782"
SRC_URI[dbus-c++-devel.sha256sum] = "286dcd0c152280e096f6f9adb6fe7434df5f6122fed28fb3c42269b18ead7165"
SRC_URI[dbus-c++-glib.sha256sum] = "262aa0f8b3d669e797346b8ace167e5ad02c407f3d44ff83cbdd67d3bf77634b"
