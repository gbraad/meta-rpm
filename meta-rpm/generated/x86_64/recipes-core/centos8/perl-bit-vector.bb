SUMMARY = "generated recipe based on perl-Bit-Vector srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & LGPL-2.0"
RPM_LICENSE = "(GPLv2+ or Artistic) and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Bit-Vector = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Bit-Vector = "glibc perl-Carp-Clan perl-Exporter perl-Storable perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Bit-Vector-7.4-11.el8.x86_64.rpm \
          "

SRC_URI[perl-Bit-Vector.sha256sum] = "20231426f025b3aff9a9ed5aec8bbb3a85de60ff495663e94f393281cf9d84fd"
