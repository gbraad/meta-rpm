SUMMARY = "generated recipe based on userspace-rcu srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_userspace-rcu = "liburcu-bp.so.6 liburcu-cds.so.6 liburcu-common.so.6 liburcu-mb.so.6 liburcu-qsbr.so.6 liburcu-signal.so.6 liburcu.so.6"
RPM_SONAME_REQ_userspace-rcu = "ld-linux-x86-64.so.2 libc.so.6 libpthread.so.0"
RDEPENDS_userspace-rcu = "glibc"
RPM_SONAME_REQ_userspace-rcu-devel = "liburcu-bp.so.6 liburcu-cds.so.6 liburcu-common.so.6 liburcu-mb.so.6 liburcu-qsbr.so.6 liburcu-signal.so.6 liburcu.so.6"
RPROVIDES_userspace-rcu-devel = "userspace-rcu-dev (= 0.10.1)"
RDEPENDS_userspace-rcu-devel = "pkgconf-pkg-config userspace-rcu"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/userspace-rcu-0.10.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/userspace-rcu-devel-0.10.1-2.el8.x86_64.rpm \
          "

SRC_URI[userspace-rcu.sha256sum] = "a2114670da656776e3666e8957b12f193a586eb38cf137d5504354b2c43a6c3a"
SRC_URI[userspace-rcu-devel.sha256sum] = "883fb56b3cf0a8fff08e02a2d00223bbc33bbb2279afb90da5bf9f0e60ffedb5"
