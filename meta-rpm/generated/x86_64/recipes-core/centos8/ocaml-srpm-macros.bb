SUMMARY = "generated recipe based on ocaml-srpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ocaml-srpm-macros-5-4.el8.noarch.rpm \
          "

SRC_URI[ocaml-srpm-macros.sha256sum] = "8d29881a811766472437504526b14d5f681c965c5a04526f97ad91e360aa2ad9"
