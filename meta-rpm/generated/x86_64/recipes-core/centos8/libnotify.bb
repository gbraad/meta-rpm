SUMMARY = "generated recipe based on libnotify srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gdk-pixbuf glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_libnotify = "libnotify.so.4"
RPM_SONAME_REQ_libnotify = "libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_libnotify = "gdk-pixbuf2 glib2 glibc"
RPM_SONAME_REQ_libnotify-devel = "libnotify.so.4"
RPROVIDES_libnotify-devel = "libnotify-dev (= 0.7.7)"
RDEPENDS_libnotify-devel = "gdk-pixbuf2-devel glib2-devel libnotify pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libnotify-0.7.7-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libnotify-devel-0.7.7-5.el8.x86_64.rpm \
          "

SRC_URI[libnotify.sha256sum] = "120ce19ff29974e0f1fd42667a41f39d21a25e8a2614b29be1f2e5baa1cda680"
SRC_URI[libnotify-devel.sha256sum] = "26d9106504a4a4da68df9aa4bec8b2b150a2fdfae2fbe739804b45dd1c3100dd"
