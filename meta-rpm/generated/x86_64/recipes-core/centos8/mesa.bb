SUMMARY = "generated recipe based on mesa srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "libegl libgles3-mesa-dev mesa-dev mesa-megadriver virtual/egl virtual/libgbm virtual/libgl virtual/libgles2 virtual/mesa"
DEPENDS = "elfutils expat libdrm libgcc libglvnd libselinux libx11 libxcb libxdamage libxext libxfixes libxshmfence libxxf86vm llvm pkgconfig-native vulkan-loader wayland zlib"
RPM_SONAME_PROV_mesa-dri-drivers = "libgallium_dri.so libmesa_dri_drivers.so"
RPM_SONAME_REQ_mesa-dri-drivers = "ld-linux-x86-64.so.2 libLLVM-9.so libc.so.6 libdl.so.2 libdrm.so.2 libdrm_amdgpu.so.1 libdrm_nouveau.so.2 libdrm_radeon.so.1 libelf.so.1 libexpat.so.1 libgcc_s.so.1 libglapi.so.0 libm.so.6 libpthread.so.0 libselinux.so.1 libstdc++.so.6 libz.so.1"
RDEPENDS_mesa-dri-drivers = "elfutils-libelf expat glibc libdrm libgcc libselinux libstdc++ llvm-libs mesa-filesystem mesa-libglapi zlib"
RPM_SONAME_PROV_mesa-libEGL = "libEGL_mesa.so.0"
RPM_SONAME_REQ_mesa-libEGL = "libX11-xcb.so.1 libc.so.6 libdl.so.2 libdrm.so.2 libexpat.so.1 libgbm.so.1 libglapi.so.0 libm.so.6 libpthread.so.0 libwayland-client.so.0 libwayland-server.so.0 libxcb-dri2.so.0 libxcb-dri3.so.0 libxcb-present.so.0 libxcb-sync.so.1 libxcb-xfixes.so.0 libxcb.so.1 libxshmfence.so.1"
RPROVIDES_mesa-libEGL = "libegl (= 19.3.4)"
RDEPENDS_mesa-libEGL = "expat glibc libX11-xcb libdrm libglvnd-egl libwayland-client libwayland-server libxcb libxshmfence mesa-libgbm mesa-libglapi"
RPROVIDES_mesa-libEGL-devel = "mesa-libEGL-dev (= 19.3.4)"
RDEPENDS_mesa-libEGL-devel = "libglvnd-devel mesa-libEGL"
RPM_SONAME_PROV_mesa-libGL = "libGLX_mesa.so.0"
RPM_SONAME_REQ_mesa-libGL = "libX11-xcb.so.1 libX11.so.6 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXxf86vm.so.1 libc.so.6 libdl.so.2 libdrm.so.2 libexpat.so.1 libglapi.so.0 libm.so.6 libpthread.so.0 libxcb-dri2.so.0 libxcb-dri3.so.0 libxcb-glx.so.0 libxcb-present.so.0 libxcb-sync.so.1 libxcb.so.1 libxshmfence.so.1"
RDEPENDS_mesa-libGL = "expat glibc libX11 libX11-xcb libXdamage libXext libXfixes libXxf86vm libdrm libglvnd-glx libxcb libxshmfence mesa-libglapi"
RPM_SONAME_REQ_mesa-libGL-devel = "libglapi.so.0"
RPROVIDES_mesa-libGL-devel = "mesa-libGL-dev (= 19.3.4)"
RDEPENDS_mesa-libGL-devel = "libdrm-devel libglvnd-devel mesa-libGL mesa-libglapi pkgconf-pkg-config"
RPM_SONAME_PROV_mesa-libOSMesa = "libOSMesa.so.8"
RPM_SONAME_REQ_mesa-libOSMesa = "libLLVM-9.so libc.so.6 libgcc_s.so.1 libglapi.so.0 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_mesa-libOSMesa = "glibc libgcc libstdc++ llvm-libs mesa-libglapi zlib"
RPM_SONAME_REQ_mesa-libOSMesa-devel = "libOSMesa.so.8"
RPROVIDES_mesa-libOSMesa-devel = "mesa-libOSMesa-dev (= 19.3.4)"
RDEPENDS_mesa-libOSMesa-devel = "mesa-libOSMesa pkgconf-pkg-config"
RPM_SONAME_PROV_mesa-libgbm = "libgbm.so.1"
RPM_SONAME_REQ_mesa-libgbm = "libc.so.6 libdl.so.2 libdrm.so.2 libexpat.so.1 libm.so.6 libpthread.so.0 libwayland-server.so.0"
RDEPENDS_mesa-libgbm = "expat glibc libdrm libwayland-server"
RPM_SONAME_REQ_mesa-libgbm-devel = "libgbm.so.1"
RPROVIDES_mesa-libgbm-devel = "mesa-libgbm-dev (= 19.3.4)"
RDEPENDS_mesa-libgbm-devel = "mesa-libgbm pkgconf-pkg-config"
RPM_SONAME_PROV_mesa-libglapi = "libglapi.so.0"
RPM_SONAME_REQ_mesa-libglapi = "libc.so.6 libpthread.so.0 libselinux.so.1"
RDEPENDS_mesa-libglapi = "glibc libselinux"
RPM_SONAME_PROV_mesa-libxatracker = "libxatracker.so.2"
RPM_SONAME_REQ_mesa-libxatracker = "libLLVM-9.so libc.so.6 libdl.so.2 libdrm.so.2 libdrm_nouveau.so.2 libexpat.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_mesa-libxatracker = "expat glibc libdrm libgcc libstdc++ llvm-libs zlib"
RPM_SONAME_PROV_mesa-vdpau-drivers = "libvdpau_gallium.so.1.0.0"
RPM_SONAME_REQ_mesa-vdpau-drivers = "libLLVM-9.so libX11-xcb.so.1 libc.so.6 libdl.so.2 libdrm.so.2 libdrm_amdgpu.so.1 libdrm_nouveau.so.2 libdrm_radeon.so.1 libelf.so.1 libexpat.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libxcb-dri2.so.0 libxcb-dri3.so.0 libxcb-present.so.0 libxcb-sync.so.1 libxcb-xfixes.so.0 libxcb.so.1 libxshmfence.so.1 libz.so.1"
RDEPENDS_mesa-vdpau-drivers = "elfutils-libelf expat glibc libX11-xcb libdrm libgcc libstdc++ libxcb libxshmfence llvm-libs mesa-filesystem zlib"
RPROVIDES_mesa-vulkan-devel = "mesa-vulkan-dev (= 19.3.4)"
RDEPENDS_mesa-vulkan-devel = "mesa-vulkan-drivers vulkan-loader-devel"
RPM_SONAME_PROV_mesa-vulkan-drivers = "libvulkan_intel.so libvulkan_radeon.so"
RPM_SONAME_REQ_mesa-vulkan-drivers = "ld-linux-x86-64.so.2 libLLVM-9.so libX11-xcb.so.1 libc.so.6 libdl.so.2 libdrm.so.2 libdrm_amdgpu.so.1 libelf.so.1 libexpat.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libwayland-client.so.0 libxcb-dri3.so.0 libxcb-present.so.0 libxcb-randr.so.0 libxcb-sync.so.1 libxcb.so.1 libxshmfence.so.1 libz.so.1"
RDEPENDS_mesa-vulkan-drivers = "elfutils-libelf expat glibc libX11-xcb libdrm libgcc libstdc++ libwayland-client libxcb libxshmfence llvm-libs vulkan-loader zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-dri-drivers-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-filesystem-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libEGL-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libEGL-devel-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libGL-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libGL-devel-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libOSMesa-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libgbm-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libglapi-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-libxatracker-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-vdpau-drivers-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-vulkan-devel-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mesa-vulkan-drivers-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mesa-libOSMesa-devel-19.3.4-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mesa-libgbm-devel-19.3.4-2.el8.x86_64.rpm \
          "

SRC_URI[mesa-dri-drivers.sha256sum] = "be71092b3aa0b67673ac9396e755c99e324864dc72a366728dc6f3d2c552fe5f"
SRC_URI[mesa-filesystem.sha256sum] = "b3b4dee5702ddbbfa718f3ce30b25bf6920e0cb25e4282e3122a5f24a1f4a86e"
SRC_URI[mesa-libEGL.sha256sum] = "ffb556f44f72658168d80880c543752bee41ef018f95f2403364b49ab2935c51"
SRC_URI[mesa-libEGL-devel.sha256sum] = "ce47997e0703da15682b02e96f970001271c30c3868540d0b7a84afc79266dea"
SRC_URI[mesa-libGL.sha256sum] = "92bef171ca1d4cec25e6e6c8b0f69ae1892490ef1f7e6513fe5f676ed8ea13fe"
SRC_URI[mesa-libGL-devel.sha256sum] = "ea2de5d606335096698b3eda4751240f183d9380bdd005c63742fc79dfccfe33"
SRC_URI[mesa-libOSMesa.sha256sum] = "4341f1ad3b7b5ca9013c57d7f999fad5206db26bcb111db1af5e735362b7b2b4"
SRC_URI[mesa-libOSMesa-devel.sha256sum] = "c4ddb8dea070ca98e740da9990715e758f09e82fc659ff51d96aa98f6e97f1cb"
SRC_URI[mesa-libgbm.sha256sum] = "dd87af9c37c34335f94166f3539858dc21fd3ecfe6e4e0cd6a5ffb5b1cb84506"
SRC_URI[mesa-libgbm-devel.sha256sum] = "f11a18fbc2df03ddabcab6825d9234ace3647a37f409d2c969cd647a535209ee"
SRC_URI[mesa-libglapi.sha256sum] = "d36d02e4a8e7bbf478d4613ef7988ae89a617db065031c2620b43e8edf1ad055"
SRC_URI[mesa-libxatracker.sha256sum] = "f82cb4fe10a2415ac89457e189b52f4da2ec12bdca53269e3fd887e8cb903d8e"
SRC_URI[mesa-vdpau-drivers.sha256sum] = "a83b257157c03a0a4141f439f7eb794398a907b9ed6a2ac863b6c16cac31d0e3"
SRC_URI[mesa-vulkan-devel.sha256sum] = "1d13cb4fdd2da8aa54dd55df3d3bad4721ef1709efd83c2029459c4d3e01a43c"
SRC_URI[mesa-vulkan-drivers.sha256sum] = "7ed75928f940f71b2ea7fd10d952226717af8d7454595c09bdac86495d995e70"
