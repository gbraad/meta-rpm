SUMMARY = "generated recipe based on hyphen-es srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0 | GPL-3.0 | MPL-1.1"
RPM_LICENSE = "LGPLv3+ or GPLv3+ or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-es = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-es-2.3-2.el8.noarch.rpm \
          "

SRC_URI[hyphen-es.sha256sum] = "8d69453d336fda4f1cf11ef5dfbf95f8ad142e747efb8d932ee89905093225f6"
