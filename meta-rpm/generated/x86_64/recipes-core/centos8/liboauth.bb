SUMMARY = "generated recipe based on liboauth srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "curl nspr nss pkgconfig-native"
RPM_SONAME_PROV_liboauth = "liboauth.so.0"
RPM_SONAME_REQ_liboauth = "libc.so.6 libcurl.so.4 libdl.so.2 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so"
RDEPENDS_liboauth = "glibc libcurl nspr nss nss-util"
RPM_SONAME_REQ_liboauth-devel = "liboauth.so.0"
RPROVIDES_liboauth-devel = "liboauth-dev (= 1.0.3)"
RDEPENDS_liboauth-devel = "libcurl-devel liboauth nss-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liboauth-1.0.3-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liboauth-devel-1.0.3-9.el8.x86_64.rpm \
          "

SRC_URI[liboauth.sha256sum] = "9b14112748aed608f6eb66c39a658d77068f40e05a60be8d36690085483a128b"
SRC_URI[liboauth-devel.sha256sum] = "7bcb87ad0d7f3b3c79fa8b52179b0e5a9f5cf95ce3b9a34cbd7ba08ebe6e6b26"
