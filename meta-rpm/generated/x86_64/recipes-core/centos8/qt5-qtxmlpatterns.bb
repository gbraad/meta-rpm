SUMMARY = "generated recipe based on qt5-qtxmlpatterns srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtxmlpatterns = "libQt5XmlPatterns.so.5 libqmlxmllistmodelplugin.so"
RPM_SONAME_REQ_qt5-qtxmlpatterns = "libQt5Core.so.5 libQt5Network.so.5 libQt5Qml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtxmlpatterns = "glibc libgcc libstdc++ qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtxmlpatterns-devel = "libQt5Core.so.5 libQt5Network.so.5 libQt5XmlPatterns.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_qt5-qtxmlpatterns-devel = "qt5-qtxmlpatterns-dev (= 5.12.5)"
RDEPENDS_qt5-qtxmlpatterns-devel = "cmake-filesystem glibc libgcc libstdc++ pkgconf-pkg-config qt5-qtbase qt5-qtbase-devel qt5-qtxmlpatterns"
RPM_SONAME_REQ_qt5-qtxmlpatterns-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Widgets.so.5 libQt5XmlPatterns.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtxmlpatterns-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtxmlpatterns"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtxmlpatterns-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtxmlpatterns-devel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtxmlpatterns-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtxmlpatterns.sha256sum] = "bf140a50d5146ba17c736baca53fdf284ce33b825411865f25a3e1fd7e8604dd"
SRC_URI[qt5-qtxmlpatterns-devel.sha256sum] = "66fcf56a10d7dbfeb384647b46f3564f0c74390c00194ddfe770efc3083c34a6"
SRC_URI[qt5-qtxmlpatterns-examples.sha256sum] = "cf0ebfd88403d732198bdc99a79cfde02a1f3197bebf0b82bec2c0332c3583a0"
