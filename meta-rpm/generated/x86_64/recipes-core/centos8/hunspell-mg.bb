SUMMARY = "generated recipe based on hunspell-mg srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mg = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-mg-0.20050109-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-mg.sha256sum] = "c129e3a33767401de6faf78eacd4cc0f877ca37ae9afa37a1743926d26b96bde"
