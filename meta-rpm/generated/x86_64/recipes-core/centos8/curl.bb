SUMMARY = "generated recipe based on curl srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "brotli e2fsprogs krb5-libs libidn2 libmetalink libnghttp2 libpsl libssh openldap openssl pkgconfig-native zlib"
RPM_SONAME_REQ_curl = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libmetalink.so.3 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_curl = "glibc libcurl libmetalink openssl-libs zlib"
RPM_SONAME_PROV_libcurl = "libcurl.so.4"
RPM_SONAME_REQ_libcurl = "libbrotlidec.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libgssapi_krb5.so.2 libidn2.so.0 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libnghttp2.so.14 libpsl.so.5 libpthread.so.0 libssh.so.4 libssl.so.1.1 libz.so.1"
RDEPENDS_libcurl = "brotli glibc krb5-libs libcom_err libidn2 libnghttp2 libpsl libssh openldap openssl-libs zlib"
RPM_SONAME_REQ_libcurl-devel = "libcurl.so.4"
RPROVIDES_libcurl-devel = "libcurl-dev (= 7.61.1)"
RDEPENDS_libcurl-devel = "bash libcurl pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/curl-7.61.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcurl-7.61.1-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcurl-devel-7.61.1-12.el8.x86_64.rpm \
          "

SRC_URI[curl.sha256sum] = "c9b792f4f44699f5ab991cab72ba44b4c88c9ffc1284b0219cb13cb9a436a4c4"
SRC_URI[libcurl.sha256sum] = "72b75aafd9eddbff4a4cc7ce5b4ffcf15ab549ca4d2cc085226e3742d4bc5337"
SRC_URI[libcurl-devel.sha256sum] = "7a518fee41a66d86fcc8e2de802fd941aa84721c8dae8ed73559b131d0f1acdc"
