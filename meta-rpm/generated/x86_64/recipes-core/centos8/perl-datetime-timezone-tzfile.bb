SUMMARY = "generated recipe based on perl-DateTime-TimeZone-Tzfile srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-TimeZone-Tzfile = "perl-Carp perl-Date-ISO8601 perl-DateTime-TimeZone-SystemV perl-IO perl-Params-Classify perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-DateTime-TimeZone-Tzfile-0.011-3.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-TimeZone-Tzfile.sha256sum] = "9b64f3d5106e7f0204dd20d6c1f7af63c493ff85a894ebba261444a5904d2e5c"
