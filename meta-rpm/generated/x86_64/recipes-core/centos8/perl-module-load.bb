SUMMARY = "generated recipe based on perl-Module-Load srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Load = "perl-PathTools perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Module-Load-0.32-395.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Load.sha256sum] = "b9231ba23f032fe210cd63e2e3f63e6020be05f8b7e977c616d37adf0413268c"
