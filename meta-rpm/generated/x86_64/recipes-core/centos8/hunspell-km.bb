SUMMARY = "generated recipe based on hunspell-km srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-km = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-km-1.82-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-km.sha256sum] = "20aff0cd537badc4c2a5ac1eae93cad07dea49d4cc06a7201245a3d80dfe89cd"
