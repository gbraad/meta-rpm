SUMMARY = "generated recipe based on python-suds srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-suds = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-suds-0.7-0.8.94664ddd46a6.el8.noarch.rpm \
          "

SRC_URI[python3-suds.sha256sum] = "3b258785f81a8be7c351957d9c0f8e87a55828ea2123105d0abc513e4fa9aba4"
