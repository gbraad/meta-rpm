SUMMARY = "generated recipe based on star srpm"
DESCRIPTION = "Description"
LICENSE = "CDDL-1.0"
RPM_LICENSE = "CDDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl attr libselinux pkgconfig-native"
RPM_SONAME_REQ_rmt = "libc.so.6"
RDEPENDS_rmt = "glibc"
RPM_SONAME_REQ_spax = "libacl.so.1 libattr.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_spax = "bash chkconfig glibc libacl libattr libselinux"
RPM_SONAME_REQ_star = "libacl.so.1 libattr.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_star = "glibc libacl libattr libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rmt-1.5.3-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/spax-1.5.3-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/star-1.5.3-13.el8.x86_64.rpm \
          "

SRC_URI[rmt.sha256sum] = "bd21dc28136c8e8d2ba3bc6e391ee3de683388a832d214994733b75371ce656c"
SRC_URI[spax.sha256sum] = "01f4555b80ec18be171ddc770ff5cb9037d7cd368facb412d14fccdb3f537c3d"
SRC_URI[star.sha256sum] = "d33c842e20930dc0635a483495736c660354c6e4bf2902a5ad481a57fcc9fd29"
