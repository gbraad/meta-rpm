SUMMARY = "generated recipe based on metacity srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libcanberra libgtop2 libice libsm libx11 libxcomposite libxcursor libxdamage libxext libxfixes libxinerama libxrandr libxrender pango pkgconfig-native startup-notification"
RPM_SONAME_PROV_metacity = "libmetacity.so.1"
RPM_SONAME_REQ_metacity = "libICE.so.6 libSM.so.6 libX11.so.6 libXcomposite.so.1 libXcursor.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXinerama.so.1 libXrandr.so.2 libXrender.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcanberra-gtk3.so.0 libcanberra.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libgtop-2.0.so.11 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libstartup-notification-1.so.0"
RDEPENDS_metacity = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gsettings-desktop-schemas gtk3 libICE libSM libX11 libXcomposite libXcursor libXdamage libXext libXfixes libXinerama libXrandr libXrender libcanberra libcanberra-gtk3 libgtop2 pango startup-notification zenity"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/metacity-3.28.0-1.el8.x86_64.rpm \
          "

SRC_URI[metacity.sha256sum] = "4b14a9d863f5755a65b423edde8e57c74158233d944937a497ccd6c8ecee5ef4"
