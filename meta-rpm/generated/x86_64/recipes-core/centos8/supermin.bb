SUMMARY = "generated recipe based on supermin srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "e2fsprogs pkgconfig-native rpm"
RPM_SONAME_REQ_supermin = "libc.so.6 libcom_err.so.2 libdl.so.2 libext2fs.so.2 libm.so.6 librpm.so.8 librpmio.so.8"
RDEPENDS_supermin = "cpio dnf dnf-plugins-core e2fsprogs e2fsprogs-libs findutils glibc libcom_err rpm rpm-libs tar util-linux"
RPROVIDES_supermin-devel = "supermin-dev (= 5.1.19)"
RDEPENDS_supermin-devel = "bash rpm-build supermin"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/supermin-5.1.19-9.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/supermin-devel-5.1.19-9.module_el8.2.0+320+13f867d7.x86_64.rpm \
          "

SRC_URI[supermin.sha256sum] = "185edf702c977bcc71206bd2ed75cbf460d7bfe084c35afbc19513fe91d29398"
SRC_URI[supermin-devel.sha256sum] = "182936f17c58f87e77396833a2af4f358cd5accb0a397963fa71fe173287e9a2"
