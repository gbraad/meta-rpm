SUMMARY = "generated recipe based on tpm2-abrmd srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-libs glib-2.0 pkgconfig-native tpm2-tss"
RPM_SONAME_PROV_tpm2-abrmd = "libtss2-tcti-tabrmd.so.0"
RPM_SONAME_REQ_tpm2-abrmd = "libc.so.6 libdbus-1.so.3 libdl.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0 libtss2-mu.so.0 libtss2-sys.so.0"
RDEPENDS_tpm2-abrmd = "bash dbus-libs glib2 glibc systemd tpm2-tss"
RPM_SONAME_REQ_tpm2-abrmd-devel = "libtss2-tcti-tabrmd.so.0"
RPROVIDES_tpm2-abrmd-devel = "tpm2-abrmd-dev (= 2.1.1)"
RDEPENDS_tpm2-abrmd-devel = "glib2-devel pkgconf-pkg-config tpm2-abrmd tpm2-tss-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tpm2-abrmd-2.1.1-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/tpm2-abrmd-devel-2.1.1-3.el8.x86_64.rpm \
          "

SRC_URI[tpm2-abrmd.sha256sum] = "2d134bc0af9b5c98381dd74689d60475e252d808da71b59bb9640b59c7c39fd2"
SRC_URI[tpm2-abrmd-devel.sha256sum] = "31fc70b635e557b5ffaca32526fd80be74ed99640152f6e57a244862a327ca73"
