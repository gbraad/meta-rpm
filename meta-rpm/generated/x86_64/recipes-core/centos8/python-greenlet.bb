SUMMARY = "generated recipe based on python-greenlet srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-greenlet = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-greenlet = "glibc platform-python python3-libs"
RPROVIDES_python3-greenlet-devel = "python3-greenlet-dev (= 0.4.13)"
RDEPENDS_python3-greenlet-devel = "python3-greenlet"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-greenlet-0.4.13-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-greenlet-devel-0.4.13-4.el8.x86_64.rpm \
          "

SRC_URI[python3-greenlet.sha256sum] = "071b6303f0e418a42b247f72b2839571629812367fc3b23de832c4011db4f390"
SRC_URI[python3-greenlet-devel.sha256sum] = "e8c83ce7a75965c6270874df045509c41f5c6020393951a0fe05dae7e8743228"
