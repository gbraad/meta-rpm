SUMMARY = "generated recipe based on rpm srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
DEPENDS = "acl audit-libs bzip2 db dbus-libs elfutils libcap libselinux lua openssl pkgconfig-native popt xz zlib zstd"
RPM_SONAME_REQ_rpm-plugin-ima = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-ima = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-prioreset = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-prioreset = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-selinux = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-selinux = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libselinux libzstd lua-libs openssl-libs popt rpm-libs selinux-policy-minimum xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-syslog = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-syslog = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-systemd-inhibit = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdbus-1.so.3 libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-systemd-inhibit = "audit-libs bzip2-libs dbus-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-plugin-ima-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-plugin-prioreset-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-plugin-selinux-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-plugin-syslog-4.14.2-37.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rpm-plugin-systemd-inhibit-4.14.2-37.el8.x86_64.rpm \
          "

SRC_URI[rpm-plugin-ima.sha256sum] = "48f8e6928715f6d35549e61f65bcbf939d6a0e47a70aa2be47281fbd237b4b03"
SRC_URI[rpm-plugin-prioreset.sha256sum] = "b1f678ed2ab302d076a86c067feef55d595456d137a1b9f7eb253397e65d6f04"
SRC_URI[rpm-plugin-selinux.sha256sum] = "e53bcd420a2480eb24b81e5d006ced844f9ef01c69f1032f8a2ae9b118c2fb47"
SRC_URI[rpm-plugin-syslog.sha256sum] = "b2d2a32e0d64afa24f48190eba6bb816fdc6e7a5a20b67561fa2ab15f61a69ba"
SRC_URI[rpm-plugin-systemd-inhibit.sha256sum] = "b2513e90289244083d5657c35c2a82fc51b0f59d0aa03dab26a250663294c621"
