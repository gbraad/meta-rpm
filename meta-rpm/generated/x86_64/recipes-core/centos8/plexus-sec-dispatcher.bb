SUMMARY = "generated recipe based on plexus-sec-dispatcher srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-sec-dispatcher = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-cipher plexus-utils"
RDEPENDS_plexus-sec-dispatcher-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-sec-dispatcher-1.4-26.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-sec-dispatcher-javadoc-1.4-26.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-sec-dispatcher.sha256sum] = "e9c709d6a1b59ae5de97b5af1c916554129a4c77a311b806d94fa9037708c747"
SRC_URI[plexus-sec-dispatcher-javadoc.sha256sum] = "543db8a284b9b84c1224969b636e0bf3b0263efed6e1350301d9b88cfaac8503"
