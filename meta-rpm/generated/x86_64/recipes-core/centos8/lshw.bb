SUMMARY = "generated recipe based on lshw srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 libgcc pango pkgconfig-native sqlite3"
RPM_SONAME_REQ_lshw = "libc.so.6 libgcc_s.so.1 libm.so.6 libresolv.so.2 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_lshw = "glibc hwdata libgcc libstdc++ sqlite-libs"
RPM_SONAME_REQ_lshw-gui = "libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libresolv.so.2 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_lshw-gui = "atk bash cairo fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 libgcc libstdc++ lshw pango polkit sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lshw-gui-B.02.18-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/lshw-B.02.18-23.el8.x86_64.rpm \
          "

SRC_URI[lshw.sha256sum] = "4e27df03218d89dd89c98ebba421e5e8b8b76638ece086337f842ff686f31129"
SRC_URI[lshw-gui.sha256sum] = "05f9fef53273858c68be71110e1f9a5a33185309a99a6b4d924ba5fbb704edd4"
