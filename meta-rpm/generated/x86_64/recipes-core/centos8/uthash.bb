SUMMARY = "generated recipe based on uthash srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_uthash-devel = "uthash-dev (= 2.0.2)"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/uthash-devel-2.0.2-4.el8.noarch.rpm \
          "

SRC_URI[uthash-devel.sha256sum] = "a5e721485190c3d9599300adaf6dcd2cf0a2a4741ecab4ebd488424946e89e17"
