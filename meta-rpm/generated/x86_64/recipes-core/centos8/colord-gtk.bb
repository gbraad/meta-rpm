SUMMARY = "generated recipe based on colord-gtk srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo colord gdk-pixbuf glib-2.0 gtk+3 lcms2 pango pkgconfig-native"
RPM_SONAME_PROV_colord-gtk = "libcolord-gtk.so.1"
RPM_SONAME_REQ_colord-gtk = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcolord.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 liblcms2.so.2 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_colord-gtk = "atk cairo cairo-gobject colord-libs gdk-pixbuf2 glib2 glibc gtk3 lcms2 pango"
RPM_SONAME_REQ_colord-gtk-devel = "libcolord-gtk.so.1"
RPROVIDES_colord-gtk-devel = "colord-gtk-dev (= 0.1.26)"
RDEPENDS_colord-gtk-devel = "colord-devel colord-gtk gtk3-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/colord-gtk-0.1.26-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/colord-gtk-devel-0.1.26-8.el8.x86_64.rpm \
          "

SRC_URI[colord-gtk.sha256sum] = "5d201d25607fa11974e1121d339ff24ac7425ae4ba8999a3d9fabbb4a27ed689"
SRC_URI[colord-gtk-devel.sha256sum] = "36533e0dadce0302d56d04fce15074d71ab9098edaf2eab3cd3612c68b93b64a"
