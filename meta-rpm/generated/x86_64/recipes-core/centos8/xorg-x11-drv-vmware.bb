SUMMARY = "generated recipe based on xorg-x11-drv-vmware srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libdrm mesa pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-drv-vmware = "libc.so.6 libdrm.so.2 libxatracker.so.2"
RDEPENDS_xorg-x11-drv-vmware = "glibc libdrm mesa-libxatracker xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-vmware-13.2.1-8.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-vmware.sha256sum] = "92cd121f4f3e4d4ee795beea0262e412455bba39b105d0ab0cb5d6822523084c"
