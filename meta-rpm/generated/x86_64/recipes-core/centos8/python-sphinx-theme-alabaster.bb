SUMMARY = "generated recipe based on python-sphinx-theme-alabaster srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-sphinx-theme-alabaster = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python3-sphinx-theme-alabaster-0.7.9-7.el8.noarch.rpm \
          "

SRC_URI[python3-sphinx-theme-alabaster.sha256sum] = "e7b1e8c31a95318548cc80efa9c054972d3de9b82716d594b74c2a077e5f3f2f"
