SUMMARY = "generated recipe based on libsemanage srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs bzip2 libselinux libsepol pkgconfig-native platform-python3"
RPM_SONAME_PROV_libsemanage = "libsemanage.so.1"
RPM_SONAME_REQ_libsemanage = "libaudit.so.1 libbz2.so.1 libc.so.6 libselinux.so.1 libsepol.so.1"
RDEPENDS_libsemanage = "audit-libs bzip2-libs glibc libselinux libsepol"
RPM_SONAME_REQ_python3-libsemanage = "libc.so.6 libpython3.6m.so.1.0 libsemanage.so.1"
RDEPENDS_python3-libsemanage = "glibc libsemanage platform-python python3-libs python3-libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsemanage-2.9-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-libsemanage-2.9-2.el8.x86_64.rpm \
          "

SRC_URI[libsemanage.sha256sum] = "488201cb92e713dcd27322b5d44aa62de84e7a9ce6b1132dd8e309bc128d19fd"
SRC_URI[python3-libsemanage.sha256sum] = "987a2c5a0bbfc8f93fd92e6fa37657d0007f654e3967e82b382637d6736efd47"
