SUMMARY = "generated recipe based on libXxf86vm srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXxf86vm = "libXxf86vm.so.1"
RPM_SONAME_REQ_libXxf86vm = "libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXxf86vm = "glibc libX11 libXext"
RPM_SONAME_REQ_libXxf86vm-devel = "libXxf86vm.so.1"
RPROVIDES_libXxf86vm-devel = "libXxf86vm-dev (= 1.1.4)"
RDEPENDS_libXxf86vm-devel = "libX11-devel libXext-devel libXxf86vm pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXxf86vm-1.1.4-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXxf86vm-devel-1.1.4-9.el8.x86_64.rpm \
          "

SRC_URI[libXxf86vm.sha256sum] = "5813a48905fafc027e4b71b8113e654f23c963a9526015ec4fd0738b68de264a"
SRC_URI[libXxf86vm-devel.sha256sum] = "9213c3cf032fc5da6f52a1cc8ae607ad8ddece80e80f212407c19dfd57742ace"
