SUMMARY = "generated recipe based on hunspell-ku srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 | LGPL-3.0 | MPL-1.1"
RPM_LICENSE = "GPLv3 or LGPLv3 or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ku = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ku-0.21-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-ku.sha256sum] = "b2507c0335eb46735652c16036654f77992bfe39c059dd99807c8962dbad7ccc"
