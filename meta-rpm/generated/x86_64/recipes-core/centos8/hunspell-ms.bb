SUMMARY = "generated recipe based on hunspell-ms srpm"
DESCRIPTION = "Description"
LICENSE = "GFDL-1.1 & GPL-2.0"
RPM_LICENSE = "GFDL and GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ms = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-ms-0.20050117-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-ms.sha256sum] = "75920fb9fae649ce4818b3399faf688758b3253a9ae8d8c98637bf0c9deb9842"
