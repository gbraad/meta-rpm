SUMMARY = "generated recipe based on gsl srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GFDL-1.1 & BSD"
RPM_LICENSE = "GPLv3 and GFDL and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_gsl = "libgsl.so.23 libgslcblas.so.0"
RPM_SONAME_REQ_gsl = "libc.so.6 libm.so.6"
RDEPENDS_gsl = "glibc"
RPM_SONAME_REQ_gsl-devel = "libgsl.so.23 libgslcblas.so.0"
RPROVIDES_gsl-devel = "gsl-dev (= 2.5)"
RDEPENDS_gsl-devel = "automake bash gsl info pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gsl-2.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gsl-devel-2.5-1.el8.x86_64.rpm \
          "

SRC_URI[gsl.sha256sum] = "1488f74a3782acf7e4a59d67e1b645adbc1ad86121bbe05d4a6fe45f613a60a3"
SRC_URI[gsl-devel.sha256sum] = "dbc1085fbe370a3af864edf5586812472ff899b67008889c8720e75332400451"
