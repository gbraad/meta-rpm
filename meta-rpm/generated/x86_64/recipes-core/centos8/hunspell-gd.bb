SUMMARY = "generated recipe based on hunspell-gd srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-3.0"
RPM_LICENSE = "GPLv2+ and GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-gd = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-gd-2.6-10.el8.noarch.rpm \
          "

SRC_URI[hunspell-gd.sha256sum] = "06724f4346dcedc4bcf6d5e5abbd7b7a201b6b5caf3458c753b79bad64def8a7"
