SUMMARY = "generated recipe based on hunspell-hil srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-hil = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-hil-0.14-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-hil.sha256sum] = "e2eaadadb688a35d06ca7de4537bafa2c98d45804f45669f1f9b30dcca00d6eb"
