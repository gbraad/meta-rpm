SUMMARY = "generated recipe based on rdma-core srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | BSD"
RPM_LICENSE = "GPLv2 or BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libnl pkgconfig-native systemd-libs"
RPM_SONAME_PROV_ibacm = "libibacmp.so"
RPM_SONAME_REQ_ibacm = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libibumad.so.3 libibverbs.so.1 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0 libsystemd.so.0"
RDEPENDS_ibacm = "bash glibc libibumad libibverbs libnl3 rdma-core systemd systemd-libs"
RPM_SONAME_PROV_infiniband-diags = "libibmad.so.5 libibnetdisc.so.5"
RPM_SONAME_REQ_infiniband-diags = "libc.so.6 libibumad.so.3"
RDEPENDS_infiniband-diags = "bash glibc libibumad perl-interpreter"
RPM_SONAME_REQ_iwpmd = "libc.so.6 libnl-3.so.200 libpthread.so.0 libsystemd.so.0"
RDEPENDS_iwpmd = "bash glibc libnl3 rdma-core systemd systemd-libs"
RPM_SONAME_PROV_libibumad = "libibumad.so.3"
RPM_SONAME_REQ_libibumad = "libc.so.6"
RDEPENDS_libibumad = "bash glibc rdma-core"
RPM_SONAME_PROV_libibverbs = "libbnxt_re-rdmav25.so libcxgb4-rdmav25.so libhfi1verbs-rdmav25.so libhns-rdmav25.so libi40iw-rdmav25.so libibverbs.so.1 libmlx4.so.1 libmlx5.so.1 libqedr-rdmav25.so librxe-rdmav25.so libsiw-rdmav25.so libvmw_pvrdma-rdmav25.so"
RPM_SONAME_REQ_libibverbs = "libc.so.6 libdl.so.2 libnl-3.so.200 libnl-route-3.so.200 libpthread.so.0"
RDEPENDS_libibverbs = "bash glibc libnl3 perl-interpreter rdma-core"
RPM_SONAME_REQ_libibverbs-utils = "libc.so.6 libibverbs.so.1"
RDEPENDS_libibverbs-utils = "glibc libibverbs"
RPM_SONAME_PROV_librdmacm = "librdmacm.so.1 librspreload.so"
RPM_SONAME_REQ_librdmacm = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libibverbs.so.1 libnl-3.so.200 libpthread.so.0"
RDEPENDS_librdmacm = "bash glibc libibverbs libnl3 rdma-core"
RPM_SONAME_REQ_librdmacm-utils = "libc.so.6 libibverbs.so.1 libpthread.so.0 librdmacm.so.1"
RDEPENDS_librdmacm-utils = "glibc libibverbs librdmacm"
RPM_SONAME_REQ_rdma-core = "libc.so.6 libnl-3.so.200 libsystemd.so.0 libudev.so.1"
RDEPENDS_rdma-core = "bash dracut glibc kmod libnl3 pciutils systemd systemd-libs"
RPM_SONAME_REQ_rdma-core-devel = "libibmad.so.5 libibnetdisc.so.5 libibumad.so.3 libibverbs.so.1 libmlx4.so.1 libmlx5.so.1 librdmacm.so.1"
RPROVIDES_rdma-core-devel = "rdma-core-dev (= 26.0)"
RDEPENDS_rdma-core-devel = "ibacm infiniband-diags libibumad libibverbs librdmacm pkgconf-pkg-config rdma-core"
RPM_SONAME_REQ_srp_daemon = "libc.so.6 libibumad.so.3 libibverbs.so.1 libpthread.so.0"
RDEPENDS_srp_daemon = "bash glibc libibumad libibverbs rdma-core systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/ibacm-26.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/infiniband-diags-26.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwpmd-26.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libibumad-26.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libibverbs-26.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libibverbs-utils-26.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/librdmacm-26.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/librdmacm-utils-26.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rdma-core-26.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rdma-core-devel-26.0-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/srp_daemon-26.0-8.el8.x86_64.rpm \
          "

SRC_URI[ibacm.sha256sum] = "8133f589e31b797821e641317441fa8a316ad8c930bdd32e5e953957972cd3e0"
SRC_URI[infiniband-diags.sha256sum] = "94f0eab6728e688d36a48c9264ed07b8efa2abe6884ef997f57637b5a15e97bd"
SRC_URI[iwpmd.sha256sum] = "0755f99407a160390e4fe3033ef9b2a3f1f9c81d8303c7341c459ac60cd0b117"
SRC_URI[libibumad.sha256sum] = "6f6c525112c80601e2a5e2cae2eac6d043674c843573057c5580d23c547e1b9e"
SRC_URI[libibverbs.sha256sum] = "7b30c72fa1c49f017eb77bfb0b4a424b8ae7b9937adbc46ab81d50e9d31fc5f2"
SRC_URI[libibverbs-utils.sha256sum] = "aa781ce965720949dcc681d7436ca457b6bdf6152f0097b835f4705979bf5bbb"
SRC_URI[librdmacm.sha256sum] = "241e2a8857b7e3f4321eb106bbe003c2aa68283ebe955dc9c373669b9b1e0bfd"
SRC_URI[librdmacm-utils.sha256sum] = "954c2b2d1f1a8af4aeaac035794ed8432e64b31bb96648095ba1aef31595c14a"
SRC_URI[rdma-core.sha256sum] = "3f5bcad11918fd7c0b5d9dd02d2f68002be2b8db7142452bd7875a1ea384d6e9"
SRC_URI[rdma-core-devel.sha256sum] = "b78e88157768de5d7b402c3560f672fd7be8609949953779bab2578b6e6688d5"
SRC_URI[srp_daemon.sha256sum] = "3ae9245eb7b7d19527700250ec02c04830f2bccdea595f0a99988c5d882f5259"
