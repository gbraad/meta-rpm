SUMMARY = "generated recipe based on spice srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "celt051 cyrus-sasl-lib glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base libcacard libjpeg-turbo lz4 openssl opus orc pixman pkgconfig-native spice-protocol zlib"
RPM_SONAME_PROV_spice-server = "libspice-server.so.1"
RPM_SONAME_REQ_spice-server = "libc.so.6 libcelt051.so.0 libcrypto.so.1.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgthread-2.0.so.0 libjpeg.so.62 liblz4.so.1 libm.so.6 libopus.so.0 liborc-0.4.so.0 libpixman-1.so.0 libpthread.so.0 librt.so.1 libsasl2.so.3 libssl.so.1.1 libz.so.1"
RDEPENDS_spice-server = "celt051 cyrus-sasl-lib glib2 glibc gstreamer1 gstreamer1-plugins-base libjpeg-turbo lz4-libs openssl-libs opus orc pixman zlib"
RPM_SONAME_REQ_spice-server-devel = "libspice-server.so.1"
RPROVIDES_spice-server-devel = "spice-server-dev (= 0.14.2)"
RDEPENDS_spice-server-devel = "glib2-devel libcacard-devel openssl-devel pixman-devel pkgconf-pkg-config spice-protocol spice-server"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/spice-server-0.14.2-1.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/spice-server-devel-0.14.2-1.el8_2.1.x86_64.rpm \
          "

SRC_URI[spice-server.sha256sum] = "5a32e54d3437e8576723353abaf653118701e61c007fba16018d77c53ba615b2"
SRC_URI[spice-server-devel.sha256sum] = "1c1d57c99bce7c9fb0dbb57141f11b1c61ebf150b240743e0f2f3cc1c28d4579"
