SUMMARY = "generated recipe based on mtdev srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_mtdev = "libmtdev.so.1"
RPM_SONAME_REQ_mtdev = "libc.so.6"
RDEPENDS_mtdev = "glibc"
RPM_SONAME_REQ_mtdev-devel = "libc.so.6 libmtdev.so.1"
RPROVIDES_mtdev-devel = "mtdev-dev (= 1.1.5)"
RDEPENDS_mtdev-devel = "glibc mtdev pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mtdev-1.1.5-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mtdev-devel-1.1.5-12.el8.x86_64.rpm \
          "

SRC_URI[mtdev.sha256sum] = "b32a8bcf7cc4ef8ac853b33106e565a094d0c4ddfd3fba6c90e41dbe297906f6"
SRC_URI[mtdev-devel.sha256sum] = "9d22d921285b51d7e2736f40e08ba6ea646a469cb6d92f8e1d7a009fab84e5a1"
