SUMMARY = "generated recipe based on tracker-miners srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo exempi flac giflib glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base icu libarchive libexif libgexiv2 libgsf libgxps libiptcdata libjpeg-turbo libosinfo libpng libseccomp libvorbis libxml2 pkgconfig-native poppler taglib tiff totem-pl-parser tracker upower zlib"
RPM_SONAME_PROV_tracker-miners = "libextract-abw.so libextract-bmp.so libextract-dummy.so libextract-dvi.so libextract-epub.so libextract-flac.so libextract-gif.so libextract-gstreamer.so libextract-html.so libextract-icon.so libextract-iso.so libextract-jpeg.so libextract-msoffice-xml.so libextract-msoffice.so libextract-oasis.so libextract-pdf.so libextract-playlist.so libextract-png.so libextract-ps.so libextract-raw.so libextract-text.so libextract-tiff.so libextract-vorbis.so libextract-xmp.so libextract-xps.so libtracker-extract.so.0 libwriteback-taglib.so libwriteback-xmp.so"
RPM_SONAME_REQ_tracker-miners = "libFLAC.so.8 libarchive.so.13 libc.so.6 libcairo.so.2 libexempi.so.3 libexif.so.12 libgexiv2.so.2 libgif.so.7 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgsf-1.so.114 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgsttag-1.0.so.0 libgxps.so.2 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libiptcdata.so.0 libjpeg.so.62 libm.so.6 libosinfo-1.0.so.0 libpng16.so.16 libpoppler-glib.so.8 libpthread.so.0 libseccomp.so.2 libtag.so.1 libtag_c.so.0 libtiff.so.5 libtotem-plparser.so.18 libtracker-miner-2.0.so.0 libtracker-sparql-2.0.so.0 libupower-glib.so.3 libvorbisfile.so.3 libxml2.so.2 libz.so.1"
RDEPENDS_tracker-miners = "bash cairo exempi flac-libs giflib glib2 glibc gstreamer1 gstreamer1-plugins-base libarchive libexif libgexiv2 libgsf libgxps libicu libiptcdata libjpeg-turbo libosinfo libpng libseccomp libtiff libvorbis libxml2 poppler-glib systemd taglib totem-pl-parser tracker upower zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tracker-miners-2.1.5-1.el8.x86_64.rpm \
          "

SRC_URI[tracker-miners.sha256sum] = "0fe5211a71ecae07fb8cf45dd1a183d33aa8c54bff6e836913fb89c198883d73"
