SUMMARY = "generated recipe based on lua-posix srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxcrypt ncurses pkgconfig-native"
RPM_SONAME_REQ_lua-posix = "libc.so.6 libcrypt.so.1 libncursesw.so.6 librt.so.1 libtinfo.so.6"
RDEPENDS_lua-posix = "glibc libxcrypt lua-libs ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/lua-posix-33.3.1-9.el8.x86_64.rpm \
          "

SRC_URI[lua-posix.sha256sum] = "295a5364acc7c4f543471ccd39506efa62c535766586b519f8b184bd7e8e457d"
