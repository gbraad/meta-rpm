SUMMARY = "generated recipe based on perl-CPAN-Meta srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-CPAN-Meta = "perl-CPAN-Meta-Requirements perl-CPAN-Meta-YAML perl-Carp perl-Encode perl-Exporter perl-JSON-PP perl-Scalar-List-Utils perl-libs perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-CPAN-Meta-2.150010-396.el8.noarch.rpm \
          "

SRC_URI[perl-CPAN-Meta.sha256sum] = "cc1ad1f38602e92325040215ed2017b93eceac18823d9b5b1a1cbf1da292f587"
