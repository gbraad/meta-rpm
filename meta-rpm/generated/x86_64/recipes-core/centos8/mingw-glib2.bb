SUMMARY = "generated recipe based on mingw-glib2 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mingw32-glib2 = "mingw32-crt mingw32-filesystem mingw32-gcc mingw32-gettext mingw32-libffi mingw32-pcre mingw32-pkg-config mingw32-zlib python36"
RDEPENDS_mingw32-glib2-static = "mingw32-gettext-static mingw32-glib2"
RDEPENDS_mingw64-glib2 = "mingw64-crt mingw64-filesystem mingw64-gettext mingw64-libffi mingw64-pcre mingw64-pkg-config mingw64-zlib python36"
RDEPENDS_mingw64-glib2-static = "mingw64-gettext-static mingw64-glib2"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-glib2-2.56.1-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-glib2-static-2.56.1-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-glib2-2.56.1-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-glib2-static-2.56.1-2.el8.noarch.rpm \
          "

SRC_URI[mingw32-glib2.sha256sum] = "58161d10bea4a7c705aa5891f949b80c04bc1742d125a85162347ab62d7f6b44"
SRC_URI[mingw32-glib2-static.sha256sum] = "99ec225e88294f7b4685a2fd8e79dda46c8a6789228ef86082f4b9fc53acd414"
SRC_URI[mingw64-glib2.sha256sum] = "715899e08edb1576bcd5b4ee1f6c705390bd304c722c0706bb2736f911abcacf"
SRC_URI[mingw64-glib2-static.sha256sum] = "8002d34bbb5b6c1595282670b2e232957ce3239ff889c0835b214a65bb72c818"
