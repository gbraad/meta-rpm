SUMMARY = "generated recipe based on qt5-qtdeclarative srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase"
RPM_SONAME_PROV_qt5-qtdeclarative = "libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickParticles.so.5 libQt5QuickShapes.so.5 libQt5QuickTest.so.5 libQt5QuickWidgets.so.5 libqmldbg_debugger.so libqmldbg_inspector.so libqmldbg_local.so libqmldbg_messages.so libqmldbg_native.so libqmldbg_nativedebugger.so libqmldbg_preview.so libqmldbg_profiler.so libqmldbg_quickprofiler.so libqmldbg_server.so libqmldbg_tcp.so"
RPM_SONAME_REQ_qt5-qtdeclarative = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Sql.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_qt5-qtdeclarative = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_REQ_qt5-qtdeclarative-devel = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickParticles.so.5 libQt5QuickShapes.so.5 libQt5QuickTest.so.5 libQt5QuickWidgets.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_qt5-qtdeclarative-devel = "qt5-qtdeclarative-dev (= 5.12.5)"
RDEPENDS_qt5-qtdeclarative-devel = "cmake-filesystem glibc libgcc libglvnd-glx libstdc++ pkgconf-pkg-config qt5-qtbase qt5-qtbase-devel qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtdeclarative-examples = "libchartsplugin.so libqmlimageproviderplugin.so libqmlimageresponseproviderplugin.so libqmlqtimeexampleplugin.so libqmltextballoonplugin.so"
RPM_SONAME_REQ_qt5-qtdeclarative-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickTest.so.5 libQt5QuickWidgets.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtdeclarative-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RDEPENDS_qt5-qtdeclarative-static = "qt5-qtdeclarative-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtdeclarative-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtdeclarative-devel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtdeclarative-examples-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qt5-qtdeclarative-static-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtdeclarative.sha256sum] = "6ab8e608de797d1fae70fdedfc5adb0de8bc0b0e94d51d8288a59db8f79fbf40"
SRC_URI[qt5-qtdeclarative-devel.sha256sum] = "6853c3b12a1deecf66446936a1a22afe5420225870f3d4b5dce6236461fde76a"
SRC_URI[qt5-qtdeclarative-examples.sha256sum] = "26cd7d6e09f7ca3470cb8d9598f47572af3c7657ca4b1b78a57da158db307731"
SRC_URI[qt5-qtdeclarative-static.sha256sum] = "4e430fc627279531fcabb8683031eae06ec58a63addaf38357bcb8457ba32d65"
