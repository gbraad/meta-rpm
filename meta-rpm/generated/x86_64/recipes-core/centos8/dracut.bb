SUMMARY = "generated recipe based on dracut srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "kmod libgcc pkgconfig-native"
RPM_SONAME_REQ_dracut = "libc.so.6 libgcc_s.so.1 libkmod.so.2"
RDEPENDS_dracut = "bash coreutils cpio filesystem findutils glibc grep gzip kmod kmod-libs libgcc libkcapi-hmaccalc procps-ng sed systemd systemd-udev util-linux xz"
RDEPENDS_dracut-caps = "bash dracut libcap"
RDEPENDS_dracut-config-generic = "dracut"
RDEPENDS_dracut-config-rescue = "bash dracut"
RDEPENDS_dracut-live = "bash coreutils curl device-mapper dracut dracut-network gzip tar"
RDEPENDS_dracut-network = "bash dhcp-client dracut iproute iputils"
RDEPENDS_dracut-squash = "bash dracut squashfs-tools"
RDEPENDS_dracut-tools = "bash dracut"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dracut-049-70.git20200228.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dracut-caps-049-70.git20200228.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dracut-config-generic-049-70.git20200228.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dracut-config-rescue-049-70.git20200228.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dracut-live-049-70.git20200228.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dracut-network-049-70.git20200228.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dracut-squash-049-70.git20200228.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dracut-tools-049-70.git20200228.el8.x86_64.rpm \
          "

SRC_URI[dracut.sha256sum] = "3ae4feb6ec3dc11d217e408697f0e35d9d4bd0cd7b4bc273853db1c37eece208"
SRC_URI[dracut-caps.sha256sum] = "145e81903988ec4a8f49a150d47e0b1af5903c2be14fb397c1f46dca9f9b4fa7"
SRC_URI[dracut-config-generic.sha256sum] = "7d80eaa21617742c9f17791e7a4f92f98ba7bf9b10f65ca4e3118f0d917369e8"
SRC_URI[dracut-config-rescue.sha256sum] = "82665ef6e174a1eed4d34236bce5af3cecc9e6f09ac8663f49ca9dd629e126f5"
SRC_URI[dracut-live.sha256sum] = "cdeb81f7252b329f974a27f75fdd640194d38896914790d3b36050e06cc643ba"
SRC_URI[dracut-network.sha256sum] = "9fac65c38bf2c4183e067f19463760c9173deb6cecace2e3883e85a265f28037"
SRC_URI[dracut-squash.sha256sum] = "b94fb7dd3b48e61359b4544c2cbded5d2554b104e64f0618be4b2c85f509744e"
SRC_URI[dracut-tools.sha256sum] = "a02d05ab732a853f28ffdfb91d9a7dedca2cefbcdc35c173b449425f1dff381a"
