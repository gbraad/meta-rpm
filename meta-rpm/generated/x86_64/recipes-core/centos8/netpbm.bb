SUMMARY = "generated recipe based on netpbm srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & GPL-2.0 & CLOSED & MIT & CLOSED"
RPM_LICENSE = "BSD and GPLv2 and IJG and MIT and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "jasper jbigkit libjpeg-turbo libpng libx11 libxml2 pkgconfig-native tiff zlib"
RPM_SONAME_PROV_netpbm = "libnetpbm.so.11"
RPM_SONAME_REQ_netpbm = "libc.so.6 libm.so.6"
RDEPENDS_netpbm = "glibc"
RPROVIDES_netpbm-devel = "netpbm-dev (= 10.82.00)"
RDEPENDS_netpbm-devel = "netpbm"
RDEPENDS_netpbm-doc = "netpbm-progs"
RPM_SONAME_REQ_netpbm-progs = "libX11.so.6 libc.so.6 libjasper.so.4 libjbig.so.2.1 libjpeg.so.62 libm.so.6 libnetpbm.so.11 libpng16.so.16 libtiff.so.5 libxml2.so.2 libz.so.1"
RDEPENDS_netpbm-progs = "bash ghostscript glibc jasper-libs jbigkit-libs libX11 libjpeg-turbo libpng libtiff libxml2 netpbm perl-Errno perl-File-Temp perl-Getopt-Long perl-PathTools perl-interpreter perl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/netpbm-10.82.00-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/netpbm-progs-10.82.00-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/netpbm-devel-10.82.00-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/netpbm-doc-10.82.00-6.el8.x86_64.rpm \
          "

SRC_URI[netpbm.sha256sum] = "eb52f8909a4d32cd145eaa52bafaa74311d39febd27f21296365e08548759adf"
SRC_URI[netpbm-devel.sha256sum] = "725672f24ff482079eff5b8b37c873f3ee876fe2ba40cb44e158e8447927ff9e"
SRC_URI[netpbm-doc.sha256sum] = "283a45bd53f41359035fbf6c5db3ef5ef6268fdd30361addfec892f8745f79ec"
SRC_URI[netpbm-progs.sha256sum] = "1d7c2205add9cf4963bf1fb5dd229a7a5aada451358858a0dccc9afdb7ca4e19"
