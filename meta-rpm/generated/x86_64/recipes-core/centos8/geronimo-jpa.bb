SUMMARY = "generated recipe based on geronimo-jpa srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_geronimo-jpa = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_geronimo-jpa-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/geronimo-jpa-1.1.1-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/geronimo-jpa-javadoc-1.1.1-21.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[geronimo-jpa.sha256sum] = "6581827e72913a5cab0ce01fac245e3b448b2b1a25fe2c089373b7e95e502c9d"
SRC_URI[geronimo-jpa-javadoc.sha256sum] = "bc557d4cb71c3bc808ddd007fc8bc6a2f6b990ab71e5489f6cf3c503fb9a560b"
