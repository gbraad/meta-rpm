SUMMARY = "generated recipe based on initial-setup srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_initial-setup = "anaconda-tui bash platform-python python3-libreport systemd util-linux"
RDEPENDS_initial-setup-gui = "anaconda-gui bash gtk3 initial-setup metacity platform-python xorg-x11-server-Xorg xorg-x11-xinit"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/initial-setup-0.3.62.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/initial-setup-gui-0.3.62.1-1.el8.x86_64.rpm \
          "

SRC_URI[initial-setup.sha256sum] = "08c15406bd0d40deddc136ba4c26c34195073ee2686b7e94fdc919acb6ce239f"
SRC_URI[initial-setup-gui.sha256sum] = "58c1e8ce35fb07566d5f5175e41e229e2ffbe5cb15387c63e6937a6cc5e3786f"
