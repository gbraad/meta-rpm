SUMMARY = "generated recipe based on hunspell-th srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-th = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-th-0.20061212-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-th.sha256sum] = "9a854c983b21ac7af600c1de8c025703af596e0be3bc52910230e4c227f2bbb2"
