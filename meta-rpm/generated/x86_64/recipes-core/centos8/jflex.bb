SUMMARY = "generated recipe based on jflex srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jflex = "ant-lib bash java-1.8.0-openjdk-headless java_cup javapackages-filesystem javapackages-tools"
RDEPENDS_jflex-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jflex-1.6.1-12.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jflex-javadoc-1.6.1-12.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jflex.sha256sum] = "a32af7d639b8cadfd26f322aef426a69a6056320e878a5fb61cbf25d786e7dd5"
SRC_URI[jflex-javadoc.sha256sum] = "1693a5e871af990544a8ec6c9ce1d28a17be88258a42cac2f4a1661904535348"
