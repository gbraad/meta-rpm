SUMMARY = "generated recipe based on ibus-hangul srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 ibus libhangul pkgconfig-native"
RPM_SONAME_REQ_ibus-hangul = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libhangul.so.1 libibus-1.0.so.5 libpthread.so.0"
RDEPENDS_ibus-hangul = "bash glib2 glibc ibus ibus-libs libhangul platform-python python3-gobject"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ibus-hangul-1.5.1-4.el8.x86_64.rpm \
          "

SRC_URI[ibus-hangul.sha256sum] = "b7ed5779e1719981c80268a73fe6898937de77fba137a5d6c0813f489adb9763"
