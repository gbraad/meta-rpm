SUMMARY = "generated recipe based on pyatspi srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "LGPLv2 and GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pyatspi = "at-spi2-core platform-python python3-gobject"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-pyatspi-2.26.0-6.el8.noarch.rpm \
          "

SRC_URI[python3-pyatspi.sha256sum] = "be3ee68852bc003d3bb01dfd27fcace3a19c6d136a2e09330ebe76656e0ee73a"
