SUMMARY = "generated recipe based on lmdb srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "OpenLDAP"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_lmdb-libs = "liblmdb.so.0.0.0"
RPM_SONAME_REQ_lmdb-libs = "libc.so.6 libpthread.so.0"
RDEPENDS_lmdb-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lmdb-libs-0.9.23-5.el8.x86_64.rpm \
          "

SRC_URI[lmdb-libs.sha256sum] = "7b1d4049622b3f76fa13fe7991cae24a191e68e99a8d76c5daa59449cb65dab3"
