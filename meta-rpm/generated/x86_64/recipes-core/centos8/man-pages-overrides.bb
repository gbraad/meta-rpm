SUMMARY = "generated recipe based on man-pages-overrides srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0 & BSD & MIT & CLOSED & CLOSED"
RPM_LICENSE = "GPL+ and GPLv2+ and BSD and MIT and Copyright only and IEEE"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/man-pages-overrides-8.2.0.2-1.el8.noarch.rpm \
          "

SRC_URI[man-pages-overrides.sha256sum] = "bd95e3803014c672a0bcf022745dc36bab477e78f154e8a49eac72aa6589b0ab"
