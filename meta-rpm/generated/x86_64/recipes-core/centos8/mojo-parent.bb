SUMMARY = "generated recipe based on mojo-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mojo-parent = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mojo-parent-40-6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[mojo-parent.sha256sum] = "6d8e7b6d03e7367c6e1dcfce31a2a98cf7009ab3630dfcc8426ff49bc85a645b"
