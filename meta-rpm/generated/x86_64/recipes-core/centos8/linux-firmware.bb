SUMMARY = "generated recipe based on linux-firmware srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0 & MIT & CLOSED"
RPM_LICENSE = "GPL+ and GPLv2+ and MIT and Redistributable, no modification permitted"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl100-firmware-39.31.5.1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl1000-firmware-39.31.5.1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl105-firmware-18.168.6.1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl135-firmware-18.168.6.1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl2000-firmware-18.168.6.1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl2030-firmware-18.168.6.1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl3160-firmware-25.30.13.0-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl3945-firmware-15.32.2.9-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl4965-firmware-228.61.2.24-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl5000-firmware-8.83.5.1_1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl5150-firmware-8.24.2.2-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl6000-firmware-9.221.4.1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl6000g2a-firmware-18.168.6.1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl6000g2b-firmware-18.168.6.1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl6050-firmware-41.28.5.1-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/iwl7260-firmware-25.30.13.0-97.el8.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libertas-sd8686-firmware-20191202-97.gite8a0f4c9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libertas-sd8787-firmware-20191202-97.gite8a0f4c9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libertas-usb8388-firmware-20191202-97.gite8a0f4c9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libertas-usb8388-olpc-firmware-20191202-97.gite8a0f4c9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/linux-firmware-20191202-97.gite8a0f4c9.el8.noarch.rpm \
          "

SRC_URI[iwl100-firmware.sha256sum] = "c492514ba313f2cac38dbb0c25f82d4b3e084d69800d24ad2ffec956de1278c4"
SRC_URI[iwl1000-firmware.sha256sum] = "ea319049861c519be6968579b5401d062b4017d20676a8f6c53e2b29eea57d4c"
SRC_URI[iwl105-firmware.sha256sum] = "ec36085c8596eeb4a32619c271cdce639fb26e229818d32efddc93079ef6657a"
SRC_URI[iwl135-firmware.sha256sum] = "6398f5bfa1988d8d7d4429d3051a0e6381a65110c3d05b956a95cc326555902f"
SRC_URI[iwl2000-firmware.sha256sum] = "dd0e8d44a9df8462310c2074b60e5e6010be69d36986a0b67d16eaf4e10abd12"
SRC_URI[iwl2030-firmware.sha256sum] = "2c0cc4da474fff409759c26cb9dc17e03ea85702168fbc047ce81539820622c9"
SRC_URI[iwl3160-firmware.sha256sum] = "0909cc0bcb824518e28ae27bbce7979f4f4b2d70aff383b14c47eab341ef62ff"
SRC_URI[iwl3945-firmware.sha256sum] = "87d7ef155821573b9b57c3cb4dec9c4bed973652ee3565ec5711b2df107de017"
SRC_URI[iwl4965-firmware.sha256sum] = "e9fde7151958e31bdb3398c110b00ebcf6d5ba5eef23da91fca7e6e78625d4b4"
SRC_URI[iwl5000-firmware.sha256sum] = "9c5728ac01473c2d657dd5e72c1867f45f24f1b0795cea1a211b5a3ff85bd9cb"
SRC_URI[iwl5150-firmware.sha256sum] = "4d89b6e16b752ade7e46e43ff7620d9eb223efe4606ed4cf90e07bd8cbb9b284"
SRC_URI[iwl6000-firmware.sha256sum] = "3fd73c541f8baad415a5917bc5c7704888661721d592d8171d3c02ca7249e013"
SRC_URI[iwl6000g2a-firmware.sha256sum] = "04ba6040201b21aa9ffab5991507f494ea9bed6c1a3f4970dfee3b117b45edde"
SRC_URI[iwl6000g2b-firmware.sha256sum] = "174de0f8599906fcbcdeddbab40c4869db74215288c3b13d3cdf2e2f072223c2"
SRC_URI[iwl6050-firmware.sha256sum] = "cfe55e4ebf87cc4bbc79d89f67854daafc7f572f9639ca0d1115de651a80061b"
SRC_URI[iwl7260-firmware.sha256sum] = "07107cc57d199c1de7ab684b3797886c0543e8e1e2482ec27a31330f338d820f"
SRC_URI[libertas-sd8686-firmware.sha256sum] = "ac6ada1e04080c0e79af2179397d2f213cee0bbe248c994f683513cfe3194d93"
SRC_URI[libertas-sd8787-firmware.sha256sum] = "a73e7abc497e6353fb42f34d4a7c8fd5380709ceff38c1bc2e2113fe3f00e42e"
SRC_URI[libertas-usb8388-firmware.sha256sum] = "e7706484ed6f4158a921f2a8e56921a60a6db6e5a1797df1d752e49787fff13a"
SRC_URI[libertas-usb8388-olpc-firmware.sha256sum] = "ba07103fddbc5375384485d0dc7504d9cc4dd319a36a5f66eecd9e786dad2d1b"
SRC_URI[linux-firmware.sha256sum] = "0ff5707e1fd27ff7932698b7ae711e02f5f50471f656ed8850280f26f3717d9f"
