SUMMARY = "generated recipe based on scotch srpm"
DESCRIPTION = "Description"
LICENSE = "CeCILL-C"
RPM_LICENSE = "CeCILL-C"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 mpich openmpi pkgconfig-native zlib"
RPM_SONAME_PROV_ptscotch-mpich = "libptesmumps.so.0 libptscotch.so.0 libptscotcherr.so.0 libptscotcherrexit.so.0 libptscotchparmetis.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0"
RPM_SONAME_REQ_ptscotch-mpich = "libbz2.so.1 libc.so.6 libm.so.6 libmpi.so.12 libpthread.so.0 librt.so.1 libz.so.1"
RDEPENDS_ptscotch-mpich = "bzip2-libs glibc mpich zlib"
RPM_SONAME_REQ_ptscotch-mpich-devel = "libptesmumps.so.0 libptscotch.so.0 libptscotcherr.so.0 libptscotcherrexit.so.0 libptscotchparmetis.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0"
RPROVIDES_ptscotch-mpich-devel = "ptscotch-mpich-dev (= 6.0.5)"
RDEPENDS_ptscotch-mpich-devel = "ptscotch-mpich"
RDEPENDS_ptscotch-mpich-devel-parmetis = "ptscotch-mpich-devel"
RPM_SONAME_PROV_ptscotch-openmpi = "libptesmumps.so.0 libptscotch.so.0 libptscotcherr.so.0 libptscotcherrexit.so.0 libptscotchparmetis.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0"
RPM_SONAME_REQ_ptscotch-openmpi = "libbz2.so.1 libc.so.6 libm.so.6 libmpi.so.40 libpthread.so.0 librt.so.1 libz.so.1"
RDEPENDS_ptscotch-openmpi = "bzip2-libs glibc openmpi zlib"
RPM_SONAME_REQ_ptscotch-openmpi-devel = "libptesmumps.so.0 libptscotch.so.0 libptscotcherr.so.0 libptscotcherrexit.so.0 libptscotchparmetis.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0"
RPROVIDES_ptscotch-openmpi-devel = "ptscotch-openmpi-dev (= 6.0.5)"
RDEPENDS_ptscotch-openmpi-devel = "ptscotch-openmpi"
RPM_SONAME_PROV_scotch = "libesmumps.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0 libscotchmetis.so.0"
RPM_SONAME_REQ_scotch = "libbz2.so.1 libc.so.6 libm.so.6 libpthread.so.0 librt.so.1 libz.so.1"
RDEPENDS_scotch = "bzip2-libs glibc zlib"
RPM_SONAME_REQ_scotch-devel = "libesmumps.so.0 libscotch.so.0 libscotcherr.so.0 libscotcherrexit.so.0 libscotchmetis.so.0"
RPROVIDES_scotch-devel = "scotch-dev (= 6.0.5)"
RDEPENDS_scotch-devel = "scotch"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ptscotch-mpich-6.0.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ptscotch-mpich-devel-6.0.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ptscotch-mpich-devel-parmetis-6.0.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ptscotch-openmpi-6.0.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ptscotch-openmpi-devel-6.0.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/scotch-6.0.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/scotch-devel-6.0.5-1.el8.x86_64.rpm \
          "

SRC_URI[ptscotch-mpich.sha256sum] = "1a5bce26c6f0ffbed519579f8fc67d5076a6a5c5e69dc87887924900056fa20f"
SRC_URI[ptscotch-mpich-devel.sha256sum] = "ec30aea01af88df640e3a0fe3aaf6e68e93a3d1c05e523474ec596c440f75a2b"
SRC_URI[ptscotch-mpich-devel-parmetis.sha256sum] = "7c9019b7abdaca74031654bf2b45c3a74d04c7e6522d8efc01ea2917ba0c7d5f"
SRC_URI[ptscotch-openmpi.sha256sum] = "37da5be8022f78721dc3bea7dcabe5c2e8380a2f6c29dfad8551661f653ba729"
SRC_URI[ptscotch-openmpi-devel.sha256sum] = "7e33716d9d306fe00e8fa49727019090663c7171b8c0bc65fd9ec9b4790618b0"
SRC_URI[scotch.sha256sum] = "3d2f85940a230fd8d689aac47cda6d7d56b482baf5fb8451bed591d385eeba54"
SRC_URI[scotch-devel.sha256sum] = "fbd9126f3c2576b2517cd612e8ac9fa7bec3758b915446dc684bca1a7ad3f198"
