SUMMARY = "generated recipe based on guava20 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & CC0-1.0"
RPM_LICENSE = "ASL 2.0 and CC0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_guava20 = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_guava20-javadoc = "javapackages-filesystem"
RDEPENDS_guava20-testlib = "guava20 java-1.8.0-openjdk-headless javapackages-filesystem jsr-305 junit"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/guava20-20.0-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/guava20-javadoc-20.0-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/guava20-testlib-20.0-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[guava20.sha256sum] = "7b6eedfbf2df94708dc9666bdaa3eb29952211bfb1d1f124da3948011a56b0fb"
SRC_URI[guava20-javadoc.sha256sum] = "c3e4f763f5e9b1e8ab0462cd1e898562fd81f26237490f9f1b6983b64ecc26b2"
SRC_URI[guava20-testlib.sha256sum] = "ed58feca37977a4cc27bfb6e399a4fc99f23e78127f72ff95f6be5070fb20407"
