SUMMARY = "generated recipe based on perl-File-Find-Rule srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-Find-Rule = "perl-Carp perl-Number-Compare perl-PathTools perl-Text-Glob perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-File-Find-Rule-0.34-8.el8.noarch.rpm \
          "

SRC_URI[perl-File-Find-Rule.sha256sum] = "b767b9a30d271b18bd1e85d8902cdcb918a31197f91d67a0bdd169ed286cd3fd"
