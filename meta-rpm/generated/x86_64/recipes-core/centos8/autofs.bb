SUMMARY = "generated recipe based on autofs srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cyrus-sasl-lib e2fsprogs krb5-libs libgcc libnsl2 libtirpc libxml2 openldap pkgconfig-native systemd-libs xz zlib"
RPM_SONAME_REQ_autofs = "libc.so.6 libcom_err.so.2 libdl.so.2 libgcc_s.so.1 libk5crypto.so.3 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 liblzma.so.5 libm.so.6 libnsl.so.2 libpthread.so.0 libresolv.so.2 librt.so.1 libsasl2.so.3 libsystemd.so.0 libtirpc.so.3 libxml2.so.2 libz.so.1"
RDEPENDS_autofs = "bash coreutils cyrus-sasl-lib gawk glibc grep kmod krb5-libs libcom_err libgcc libnsl2 libtirpc libxml2 openldap procps-ng sed systemd systemd-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/autofs-5.1.4-40.el8.x86_64.rpm \
          "

SRC_URI[autofs.sha256sum] = "d9dee8f8b48fdbaa5f60ec67565c0f2623e923b7f990eaed6906a72304e46339"
