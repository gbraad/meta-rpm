SUMMARY = "generated recipe based on libeasyfc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "fontconfig freetype glib-2.0 harfbuzz libxml2 pkgconfig-native"
RPM_SONAME_PROV_libeasyfc = "libeasyfc.so.0"
RPM_SONAME_REQ_libeasyfc = "libc.so.6 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libharfbuzz.so.0 libxml2.so.2"
RDEPENDS_libeasyfc = "fontconfig freetype glib2 glibc harfbuzz libxml2"
RPM_SONAME_PROV_libeasyfc-gobject = "libeasyfc-gobject.so.0"
RPM_SONAME_REQ_libeasyfc-gobject = "libc.so.6 libeasyfc.so.0 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libharfbuzz.so.0 libxml2.so.2"
RDEPENDS_libeasyfc-gobject = "fontconfig freetype glib2 glibc harfbuzz libeasyfc libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libeasyfc-0.14.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libeasyfc-gobject-0.14.0-1.el8.x86_64.rpm \
          "

SRC_URI[libeasyfc.sha256sum] = "d827eb1a6691477360f06c5152c8ac21cbebe9b35827efc11e3bc9e2f8a0bf50"
SRC_URI[libeasyfc-gobject.sha256sum] = "cda1e3a82dc7a47b13eb20687791b3f7d012f65f8c33fcde27e8507ed2cbf71c"
