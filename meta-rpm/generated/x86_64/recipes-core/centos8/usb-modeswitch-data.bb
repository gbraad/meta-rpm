SUMMARY = "generated recipe based on usb_modeswitch-data srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_usb_modeswitch-data = "bash systemd usb_modeswitch"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/usb_modeswitch-data-20191128-1.el8.noarch.rpm \
          "

SRC_URI[usb_modeswitch-data.sha256sum] = "f2497737973339d6e84bf663ffa8dedb77a0eabb22bc36c242724f50e31b593e"
