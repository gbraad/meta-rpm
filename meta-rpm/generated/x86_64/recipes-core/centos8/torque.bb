SUMMARY = "generated recipe based on torque srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & CLOSED"
RPM_LICENSE = "OpenPBS and TORQUEv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libxml2 munge openssl pkgconfig-native xz zlib"
RPM_SONAME_REQ_torque = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 liblzma.so.5 libm.so.6 libmunge.so.2 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6 libtorque.so.2 libxml2.so.2 libz.so.1"
RDEPENDS_torque = "bash coreutils glibc grep libgcc libstdc++ libxml2 munge munge-libs openssl-libs setup systemd torque-libs xz-libs zlib"
RPM_SONAME_REQ_torque-devel = "libtorque.so.2"
RPROVIDES_torque-devel = "torque-dev (= 4.2.10)"
RDEPENDS_torque-devel = "bash torque-libs"
RPM_SONAME_PROV_torque-libs = "libtorque.so.2"
RPM_SONAME_REQ_torque-libs = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 liblzma.so.5 libm.so.6 libmunge.so.2 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_torque-libs = "glibc libgcc libstdc++ libxml2 munge munge-libs openssl-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/torque-libs-4.2.10-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/torque-4.2.10-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/torque-devel-4.2.10-17.el8.x86_64.rpm \
          "

SRC_URI[torque.sha256sum] = "fa014600bc82fa31b5b657f11f89307e8d5ec332298e9ae1d0c2e9b0fa7edddb"
SRC_URI[torque-devel.sha256sum] = "6b0523eaf43ec4be211b2972a3fd9e09eafae735649fd7513b658580a43c102e"
SRC_URI[torque-libs.sha256sum] = "6000ca615d896ab1d5f31f593eb7fd1db1ecd7d1f90909754dd984d78b3c04fc"
