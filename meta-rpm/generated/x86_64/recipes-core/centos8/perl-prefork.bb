SUMMARY = "generated recipe based on perl-prefork srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-prefork = "perl-Carp perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-prefork-1.04-26.el8.noarch.rpm \
          "

SRC_URI[perl-prefork.sha256sum] = "641ef99930c7fdd36e297e1618fd4ffa934743f15e303221be7b8411bd5b7c44"
