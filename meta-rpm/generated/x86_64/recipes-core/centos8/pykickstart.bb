SUMMARY = "generated recipe based on pykickstart srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & MIT"
RPM_LICENSE = "GPLv2 and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_pykickstart = "platform-python python3-kickstart"
RDEPENDS_python3-kickstart = "platform-python python3-ordered-set python3-requests python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pykickstart-3.16.10-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-kickstart-3.16.10-1.el8.noarch.rpm \
          "

SRC_URI[pykickstart.sha256sum] = "c98bfb552662cbebdd3a4d340bd4846312ce09e72d8be30484787eda741dde9a"
SRC_URI[python3-kickstart.sha256sum] = "9f3790da60a8737464315f66084dcee0ff7cdf27cb10a1fc34125753255b8517"
