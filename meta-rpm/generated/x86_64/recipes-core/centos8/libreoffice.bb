SUMMARY = "generated recipe based on libreoffice srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-2.0"
RPM_LICENSE = "MPLv2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk boost cairo clucene cups-libs curl dbus-glib dbus-libs dconf expat fontconfig freetype gdk-pixbuf glib-2.0 gpgme graphite2 gstreamer1.0 gstreamer1.0-plugins-base gtk+3 gtk2 harfbuzz hunspell hyphen icu java-1.8.0-openjdk lcms2 libabw libcdr libcmis libepoxy libepubgen libetonyek libexttextcat libfreehand libgcc libice libjpeg-turbo liblangtag libmspub libmwaw libodfgen liborcus libpagemaker libpng libqxp librevenge libsm libstaroffice libtool-ltdl libvisio libwpd libwpg libwps libx11 libxext libxinerama libxml2 libxrandr libxrender libxslt libzmf lpsolve mythes neon nspr nss openldap openssl pango pkgconfig-native platform-python3 poppler raptor2 redland xmlsec1 zlib"
RPM_SONAME_PROV_libreoffice-base = "libabplo.so libdbplo.so libhsqldb.so librptlo.so librptuilo.so librptxmllo.so"
RPM_SONAME_REQ_libreoffice-base = "libc.so.6 libcomphelper.so libdbtoolslo.so libdbulo.so libeditenglo.so libforlo.so libforuilo.so libfwelo.so libgcc_s.so.1 libi18nlangtag.so libjvmfwklo.so libm.so.6 libsaxlo.so libsfxlo.so libsotlo.so libstdc++.so.6 libsvllo.so libsvtlo.so libsvxcorelo.so libsvxlo.so libtklo.so libtllo.so libucbhelper.so libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libuno_salhelpergcc3.so.3 libutllo.so libvcllo.so libxolo.so"
RDEPENDS_libreoffice-base = "bash glibc libgcc libreoffice-calc libreoffice-core libreoffice-data libreoffice-pyuno libreoffice-ure libstdc++ pentaho-reporting-flow-engine postgresql-jdbc"
RPM_SONAME_PROV_libreoffice-calc = "libanalysislo.so libcalclo.so libdatelo.so libforlo.so libforuilo.so libpricinglo.so libscdlo.so libscfiltlo.so libsclo.so libscuilo.so libsolverlo.so libvbaobjlo.so libwpftcalclo.so"
RPM_SONAME_REQ_libreoffice-calc = "ld-linux-x86-64.so.2 libavmedialo.so libbasegfxlo.so libboost_filesystem.so.1.66.0 libboost_iostreams.so.1.66.0 libc.so.6 libclewlo.so libcomphelper.so libdbtoolslo.so libdrawinglayerlo.so libeditenglo.so libetonyek-0.1.so.1 libfilelo.so libgcc_s.so.1 libi18nlangtag.so libi18nutil.so libicui18n.so.60 libicuuc.so.60 liblcms2.so.2 liblpsolve55.so libm.so.6 libmsfilterlo.so libmwaw-0.3.so.3 libodfgen-0.1.so.1 libooxlo.so libopencllo.so liborcus-0.13.so.0 liborcus-parser-0.13.so.0 libpthread.so.0 librevenge-0.0.so.0 librevenge-stream-0.0.so.0 librt.so.1 libsaxlo.so libsblo.so libsfxlo.so libsotlo.so libstaroffice-0.0.so.0 libstdc++.so.6 libsvllo.so libsvtlo.so libsvxcorelo.so libsvxlo.so libtklo.so libtllo.so libucbhelper.so libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libuno_salhelpergcc3.so.3 libutllo.so libvbahelperlo.so libvcllo.so libwps-0.4.so.4 libwriterperfectlo.so libxml2.so.2 libxolo.so libz.so.1"
RDEPENDS_libreoffice-calc = "bash boost-filesystem boost-iostreams glibc lcms2 libetonyek libgcc libicu libmwaw libodfgen liborcus libreoffice-core libreoffice-data libreoffice-pyuno libreoffice-ure librevenge libstaroffice libstdc++ libwps libxml2 lpsolve zlib"
RPM_SONAME_PROV_libreoffice-core = "libacclo.so libanimcorelo.so libavmediagst.so libavmedialo.so libbasctllo.so libbasegfxlo.so libbasprovlo.so libbiblo.so libcached1.so libcairocanvaslo.so libcanvasfactorylo.so libcanvastoolslo.so libchartcontrollerlo.so libchartcorelo.so libchartopengllo.so libclewlo.so libcmdmaillo.so libcollator_data.so libcomphelper.so libconfigmgrlo.so libcppcanvaslo.so libctllo.so libcuilo.so libdbalo.so libdbaselo.so libdbaxmllo.so libdbmmlo.so libdbpool2.so libdbtoolslo.so libdbulo.so libdeployment.so libdeploymentgui.so libdeploymentmisclo.so libdesktop_detectorlo.so libdesktopbe1lo.so libdict_ja.so libdict_zh.so libdlgprovlo.so libdrawinglayerlo.so libeditenglo.so libembobj.so libemboleobj.so libemfiolo.so libevoablo.so libevtattlo.so libexpwraplo.so libfilelo.so libfilterconfiglo.so libflatlo.so libfps_officelo.so libfrmlo.so libfsstoragelo.so libfwelo.so libfwilo.so libfwklo.so libfwllo.so libfwmlo.so libgielo.so libguesslanglo.so libhelplinkerlo.so libhyphenlo.so libi18npoollo.so libi18nsearchlo.so libi18nutil.so libicglo.so libindex_data.so libjdbclo.so libldapbe2lo.so liblnglo.so liblnthlo.so liblocalebe1lo.so liblocaledata_en.so liblocaledata_es.so liblocaledata_euro.so liblocaledata_others.so libloglo.so liblosessioninstalllo.so libmcnttype.so libmigrationoo2lo.so libmigrationoo3lo.so libmorklo.so libmozbootstraplo.so libmsfilterlo.so libmsformslo.so libmtfrendererlo.so libmysqllo.so libodbclo.so libodfflatxmllo.so liboffacclo.so liboglcanvaslo.so libooxlo.so libopencllo.so libpackage2.so libpasswordcontainerlo.so libpcrlo.so libpdffilterlo.so libprotocolhandlerlo.so librecentfile.so libsaxlo.so libsblo.so libscnlo.so libscriptframe.so libsdbc2.so libsdbtlo.so libsddlo.so libsdfiltlo.so libsdlo.so libsduilo.so libsfxlo.so libsimplecanvaslo.so libslideshowlo.so libsofficeapp.so libsotlo.so libspelllo.so libspllo.so libsrtrs1.so libstoragefdlo.so libstringresourcelo.so libsvgiolo.so libsvllo.so libsvtlo.so libsvxcorelo.so libsvxlo.so libswdlo.so libswlo.so libsysshlo.so libtextconv_dict.so libtextconversiondlgslo.so libtextfdlo.so libtklo.so libtllo.so libucb1.so libucbhelper.so libucpchelp1.so libucpcmis1lo.so libucpdav1.so libucpexpand1lo.so libucpextlo.so libucpfile1.so libucpftp1.so libucpgio1lo.so libucphier1.so libucpimagelo.so libucppkg1.so libucptdoc1lo.so libunopkgapp.so libunordflo.so libunoxmllo.so libupdatefeedlo.so libutllo.so libuuilo.so libvbaeventslo.so libvbahelperlo.so libvclcanvaslo.so libvcllo.so libwriterperfectlo.so libxmlfalo.so libxmlfdlo.so libxmlscriptlo.so libxmlsecurity.so libxoflo.so libxolo.so libxsec_fw.so libxsec_xmlsec.so libxsltdlglo.so libxsltfilterlo.so libxstor.so"
RPM_SONAME_REQ_libreoffice-core = "ld-linux-x86-64.so.2 libICE.so.6 libSM.so.6 libX11.so.6 libXext.so.6 libXinerama.so.1 libatk-1.0.so.0 libboost_date_time.so.1.66.0 libboost_locale.so.1.66.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libclucene-contribs-lib.so.1 libclucene-core.so.1 libclucene-shared.so.1 libcmis-0.5.so.5 libcrypto.so.1.1 libcups.so.2 libcurl.so.4 libdbus-1.so.3 libdbus-glib-1.so.2 libdconf.so.1 libdl.so.2 libepoxy.so.0 libexpat.so.1 libexslt.so.0 libexttextcat-2.0.so.0 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgpgmepp.so.6 libgraphite2.so.3 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libgtk-3.so.0 libharfbuzz-icu.so.0 libharfbuzz.so.0 libhunspell-1.6.so.0 libhyphen.so.0 libi18nlangtag.so libicui18n.so.60 libicuuc.so.60 libjpeg.so.62 libjvmaccesslo.so libjvmfwklo.so liblber-2.4.so.2 liblcms2.so.2 libldap-2.4.so.2 libltdl.so.7 libm.so.6 libmythes-1.2.so.0 libneon.so.27 libnspr4.so libnss3.so libnssutil3.so libodfgen-0.1.so.1 liborcus-0.13.so.0 liborcus-parser-0.13.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libplc4.so libplds4.so libpng16.so.16 libpthread.so.0 libraptor2.so.0 librdf.so.0 librevenge-0.0.so.0 librevenge-stream-0.0.so.0 librt.so.1 libsmime3.so libssl.so.1.1 libssl3.so libstdc++.so.6 libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libuno_salhelpergcc3.so.3 libxml2.so.2 libxmlreaderlo.so libxmlsec1-nss.so.1 libxmlsec1.so.1 libxslt.so.1 libz.so.1"
RDEPENDS_libreoffice-core = "atk bash boost-date-time boost-locale cairo cairo-gobject clucene-contribs-lib clucene-core cups-libs dbus-glib dbus-libs dconf dejavu-sans-fonts dejavu-sans-mono-fonts dejavu-serif-fonts expat fontconfig freetype gdk-pixbuf2 glib2 glibc google-crosextra-caladea-fonts google-crosextra-carlito-fonts gpgmepp graphite2 gstreamer1 gstreamer1-plugins-base gtk3 harfbuzz harfbuzz-icu hunspell hyphen java-1.8.0-openjdk-headless lcms2 libICE libSM libX11 libXext libXinerama libcmis libcurl libepoxy liberation-mono-fonts liberation-sans-fonts liberation-serif-fonts libexttextcat libgcc libicu libjpeg-turbo libodfgen liborcus libpng libreoffice-data libreoffice-gtk3 libreoffice-langpack-en libreoffice-opensymbol-fonts libreoffice-ure librevenge libstdc++ libtool-ltdl libxml2 libxslt mythes neon nspr nss nss-util openldap openssl-libs pango raptor2 redland xmlsec1 xmlsec1-nss zlib"
RDEPENDS_libreoffice-data = "bash"
RDEPENDS_libreoffice-draw = "bash libreoffice-core libreoffice-data libreoffice-graphicfilter libreoffice-pdfimport libreoffice-pyuno libreoffice-ure"
RDEPENDS_libreoffice-emailmerge = "libreoffice-pyuno libreoffice-writer"
RDEPENDS_libreoffice-filters = "libreoffice-calc libreoffice-core libreoffice-draw libreoffice-graphicfilter libreoffice-impress libreoffice-math libreoffice-ure libreoffice-writer libreoffice-xsltfilter"
RDEPENDS_libreoffice-gdb-debug-support = "gdb libreoffice-core python3-six"
RPM_SONAME_PROV_libreoffice-graphicfilter = "libflashlo.so libgraphicfilterlo.so libsvgfilterlo.so libwpftdrawlo.so"
RPM_SONAME_REQ_libreoffice-graphicfilter = "libbasegfxlo.so libc.so.6 libcdr-0.1.so.1 libcomphelper.so libeditenglo.so libfreehand-0.1.so.1 libgcc_s.so.1 libi18nlangtag.so libicui18n.so.60 libicuuc.so.60 liblcms2.so.2 libm.so.6 libmspub-0.1.so.1 libmwaw-0.3.so.3 libodfgen-0.1.so.1 libpagemaker-0.0.so.0 libpng16.so.16 libqxp-0.0.so.0 librevenge-0.0.so.0 librevenge-stream-0.0.so.0 libsaxlo.so libsfxlo.so libsotlo.so libstaroffice-0.0.so.0 libstdc++.so.6 libsvllo.so libsvtlo.so libsvxcorelo.so libsvxlo.so libtklo.so libtllo.so libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libuno_salhelpergcc3.so.3 libutllo.so libvcllo.so libvisio-0.1.so.1 libwpd-0.10.so.10 libwpg-0.3.so.3 libwriterperfectlo.so libxml2.so.2 libxolo.so libz.so.1 libzmf-0.0.so.0"
RDEPENDS_libreoffice-graphicfilter = "glibc lcms2 libcdr libfreehand libgcc libicu libmspub libmwaw libodfgen libpagemaker libpng libqxp libreoffice-core libreoffice-data libreoffice-ure librevenge libstaroffice libstdc++ libvisio libwpd libwpg libxml2 libzmf zlib"
RPM_SONAME_PROV_libreoffice-gtk2 = "libvclplug_gtklo.so"
RPM_SONAME_REQ_libreoffice-gtk2 = "ld-linux-x86-64.so.2 libICE.so.6 libSM.so.6 libX11.so.6 libXext.so.6 libatk-1.0.so.0 libbasegfxlo.so libc.so.6 libcairo.so.2 libcomphelper.so libdbus-1.so.3 libdbus-glib-1.so.2 libdl.so.2 libepoxy.so.0 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-x11-2.0.so.0 libi18nlangtag.so libi18nutil.so libicuuc.so.60 libjvmaccesslo.so libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libsotlo.so libstdc++.so.6 libtllo.so libucbhelper.so libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libutllo.so libvcllo.so libvclplug_genlo.so"
RDEPENDS_libreoffice-gtk2 = "atk cairo dbus-glib dbus-libs fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 libICE libSM libX11 libXext libepoxy libgcc libicu libreoffice-core libreoffice-ure libreoffice-x11 libstdc++ pango"
RPM_SONAME_PROV_libreoffice-gtk3 = "libvclplug_gtk3lo.so"
RPM_SONAME_REQ_libreoffice-gtk3 = "ld-linux-x86-64.so.2 libICE.so.6 libSM.so.6 libX11.so.6 libXext.so.6 libatk-1.0.so.0 libbasegfxlo.so libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcomphelper.so libdbus-1.so.3 libdbus-glib-1.so.2 libdl.so.2 libepoxy.so.0 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-3.so.0 libi18nlangtag.so libi18nutil.so libjvmaccesslo.so libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsotlo.so libstdc++.so.6 libtllo.so libucbhelper.so libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libutllo.so libvcllo.so"
RDEPENDS_libreoffice-gtk3 = "atk cairo cairo-gobject dbus-glib dbus-libs gdk-pixbuf2 glib2 glibc gstreamer1-plugins-good-gtk gtk3 libICE libSM libX11 libXext libepoxy libgcc libreoffice-core libreoffice-ure libstdc++ pango"
RDEPENDS_libreoffice-help-ar = "libreoffice-core"
RDEPENDS_libreoffice-help-bg = "libreoffice-core"
RDEPENDS_libreoffice-help-bn = "libreoffice-core"
RDEPENDS_libreoffice-help-ca = "libreoffice-core"
RDEPENDS_libreoffice-help-cs = "libreoffice-core"
RDEPENDS_libreoffice-help-da = "libreoffice-core"
RDEPENDS_libreoffice-help-de = "libreoffice-core"
RDEPENDS_libreoffice-help-dz = "libreoffice-core"
RDEPENDS_libreoffice-help-el = "libreoffice-core"
RDEPENDS_libreoffice-help-en = "libreoffice-core"
RDEPENDS_libreoffice-help-es = "libreoffice-core"
RDEPENDS_libreoffice-help-et = "libreoffice-core"
RDEPENDS_libreoffice-help-eu = "libreoffice-core"
RDEPENDS_libreoffice-help-fi = "libreoffice-core"
RDEPENDS_libreoffice-help-fr = "libreoffice-core"
RDEPENDS_libreoffice-help-gl = "libreoffice-core"
RDEPENDS_libreoffice-help-gu = "libreoffice-core"
RDEPENDS_libreoffice-help-he = "libreoffice-core"
RDEPENDS_libreoffice-help-hi = "libreoffice-core"
RDEPENDS_libreoffice-help-hr = "libreoffice-core"
RDEPENDS_libreoffice-help-hu = "libreoffice-core"
RDEPENDS_libreoffice-help-id = "libreoffice-core"
RDEPENDS_libreoffice-help-it = "libreoffice-core"
RDEPENDS_libreoffice-help-ja = "libreoffice-core"
RDEPENDS_libreoffice-help-ko = "libreoffice-core"
RDEPENDS_libreoffice-help-lt = "libreoffice-core"
RDEPENDS_libreoffice-help-lv = "libreoffice-core"
RDEPENDS_libreoffice-help-nb = "libreoffice-core"
RDEPENDS_libreoffice-help-nl = "libreoffice-core"
RDEPENDS_libreoffice-help-nn = "libreoffice-core"
RDEPENDS_libreoffice-help-pl = "libreoffice-core"
RDEPENDS_libreoffice-help-pt-BR = "libreoffice-core"
RDEPENDS_libreoffice-help-pt-PT = "libreoffice-core"
RDEPENDS_libreoffice-help-ro = "libreoffice-core"
RDEPENDS_libreoffice-help-ru = "libreoffice-core"
RDEPENDS_libreoffice-help-si = "libreoffice-core"
RDEPENDS_libreoffice-help-sk = "libreoffice-core"
RDEPENDS_libreoffice-help-sl = "libreoffice-core"
RDEPENDS_libreoffice-help-sv = "libreoffice-core"
RDEPENDS_libreoffice-help-ta = "libreoffice-core"
RDEPENDS_libreoffice-help-tr = "libreoffice-core"
RDEPENDS_libreoffice-help-uk = "libreoffice-core"
RDEPENDS_libreoffice-help-zh-Hans = "libreoffice-core"
RDEPENDS_libreoffice-help-zh-Hant = "libreoffice-core"
RPM_SONAME_PROV_libreoffice-impress = "libPresentationMinimizerlo.so libPresenterScreenlo.so libwpftimpresslo.so"
RPM_SONAME_REQ_libreoffice-impress = "libc.so.6 libcomphelper.so libetonyek-0.1.so.1 libgcc_s.so.1 libm.so.6 libmwaw-0.3.so.3 libodfgen-0.1.so.1 librevenge-0.0.so.0 librevenge-stream-0.0.so.0 libsotlo.so libstaroffice-0.0.so.0 libstdc++.so.6 libsvxlo.so libtllo.so libucbhelper.so libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libutllo.so libvcllo.so libwriterperfectlo.so libxml2.so.2 libxolo.so libz.so.1"
RDEPENDS_libreoffice-impress = "bash glibc libetonyek libgcc libmwaw libodfgen libreoffice-core libreoffice-data libreoffice-graphicfilter libreoffice-pyuno libreoffice-ure librevenge libstaroffice libstdc++ libxml2 zlib"
RDEPENDS_libreoffice-langpack-af = "autocorr-af hunspell-af hyphen-af libreoffice-core"
RDEPENDS_libreoffice-langpack-ar = "hunspell-ar libreoffice-core libreoffice-help-ar"
RDEPENDS_libreoffice-langpack-as = "hunspell-as hyphen-as libreoffice-core"
RDEPENDS_libreoffice-langpack-bg = "autocorr-bg hunspell-bg hyphen-bg libreoffice-core libreoffice-help-bg mythes-bg"
RDEPENDS_libreoffice-langpack-bn = "hunspell-bn hyphen-bn libreoffice-core libreoffice-help-bn"
RDEPENDS_libreoffice-langpack-br = "hunspell-br libreoffice-core"
RDEPENDS_libreoffice-langpack-ca = "autocorr-ca hunspell-ca hyphen-ca libreoffice-core libreoffice-help-ca mythes-ca"
RDEPENDS_libreoffice-langpack-cs = "autocorr-cs hunspell-cs hyphen-cs libreoffice-core libreoffice-help-cs mythes-cs"
RDEPENDS_libreoffice-langpack-cy = "hunspell-cy hyphen-cy libreoffice-core"
RDEPENDS_libreoffice-langpack-da = "autocorr-da hunspell-da hyphen-da libreoffice-core libreoffice-help-da mythes-da"
RDEPENDS_libreoffice-langpack-de = "autocorr-de hunspell-de hyphen-de libreoffice-core libreoffice-help-de mythes-de"
RDEPENDS_libreoffice-langpack-dz = "libreoffice-core libreoffice-help-dz"
RDEPENDS_libreoffice-langpack-el = "hunspell-el hyphen-el libreoffice-core libreoffice-help-el mythes-el"
RDEPENDS_libreoffice-langpack-en = "autocorr-en hunspell-en-US hyphen-en libreoffice-core libreoffice-help-en mythes-en"
RDEPENDS_libreoffice-langpack-es = "autocorr-es hunspell-es hyphen-es libreoffice-core libreoffice-help-es mythes-es"
RDEPENDS_libreoffice-langpack-et = "hunspell-et hyphen-et libreoffice-core libreoffice-help-et"
RDEPENDS_libreoffice-langpack-eu = "hunspell-eu hyphen-eu libreoffice-core libreoffice-help-eu"
RDEPENDS_libreoffice-langpack-fa = "autocorr-fa hunspell-fa hyphen-fa libreoffice-core"
RDEPENDS_libreoffice-langpack-fi = "autocorr-fi libreoffice-core libreoffice-help-fi libreoffice-voikko"
RDEPENDS_libreoffice-langpack-fr = "autocorr-fr hunspell-fr hyphen-fr libreoffice-core libreoffice-help-fr mythes-fr"
RDEPENDS_libreoffice-langpack-ga = "autocorr-ga hunspell-ga hyphen-ga libreoffice-core mythes-ga"
RDEPENDS_libreoffice-langpack-gl = "hunspell-gl hyphen-gl libreoffice-core libreoffice-help-gl"
RDEPENDS_libreoffice-langpack-gu = "hunspell-gu hyphen-gu libreoffice-core libreoffice-help-gu"
RDEPENDS_libreoffice-langpack-he = "hunspell-he libreoffice-core libreoffice-help-he"
RDEPENDS_libreoffice-langpack-hi = "hunspell-hi hyphen-hi libreoffice-core libreoffice-help-hi"
RDEPENDS_libreoffice-langpack-hr = "autocorr-hr hunspell-hr hyphen-hr libreoffice-core libreoffice-help-hr"
RDEPENDS_libreoffice-langpack-hu = "autocorr-hu hunspell-hu hyphen-hu libreoffice-core libreoffice-help-hu mythes-hu"
RDEPENDS_libreoffice-langpack-id = "hunspell-id hyphen-id libreoffice-core libreoffice-help-id"
RDEPENDS_libreoffice-langpack-it = "autocorr-it hunspell-it hyphen-it libreoffice-core libreoffice-help-it mythes-it"
RDEPENDS_libreoffice-langpack-ja = "autocorr-ja libreoffice-core libreoffice-help-ja"
RDEPENDS_libreoffice-langpack-kk = "hunspell-kk libreoffice-core"
RDEPENDS_libreoffice-langpack-kn = "hunspell-kn hyphen-kn libreoffice-core"
RDEPENDS_libreoffice-langpack-ko = "autocorr-ko hunspell-ko libreoffice-core libreoffice-help-ko"
RDEPENDS_libreoffice-langpack-lt = "autocorr-lt hunspell-lt hyphen-lt libreoffice-core libreoffice-help-lt"
RDEPENDS_libreoffice-langpack-lv = "hunspell-lv hyphen-lv libreoffice-core libreoffice-help-lv mythes-lv"
RDEPENDS_libreoffice-langpack-mai = "libreoffice-core"
RDEPENDS_libreoffice-langpack-ml = "hunspell-ml hyphen-ml libreoffice-core"
RDEPENDS_libreoffice-langpack-mr = "hunspell-mr hyphen-mr libreoffice-core"
RDEPENDS_libreoffice-langpack-nb = "hunspell-nb hyphen-nb libreoffice-core libreoffice-help-nb mythes-nb"
RDEPENDS_libreoffice-langpack-nl = "autocorr-nl hunspell-nl hyphen-nl libreoffice-core libreoffice-help-nl mythes-nl"
RDEPENDS_libreoffice-langpack-nn = "hunspell-nn hyphen-nn libreoffice-core libreoffice-help-nn mythes-nn"
RDEPENDS_libreoffice-langpack-nr = "hunspell-nr libreoffice-core"
RDEPENDS_libreoffice-langpack-nso = "hunspell-nso libreoffice-core"
RDEPENDS_libreoffice-langpack-or = "hunspell-or hyphen-or libreoffice-core"
RDEPENDS_libreoffice-langpack-pa = "hunspell-pa hyphen-pa libreoffice-core"
RDEPENDS_libreoffice-langpack-pl = "autocorr-pl hunspell-pl hyphen-pl libreoffice-core libreoffice-help-pl mythes-pl"
RDEPENDS_libreoffice-langpack-pt-BR = "autocorr-pt hunspell-pt hyphen-pt libreoffice-core libreoffice-help-pt-BR mythes-pt"
RDEPENDS_libreoffice-langpack-pt-PT = "autocorr-pt hunspell-pt hyphen-pt libreoffice-core libreoffice-help-pt-PT mythes-pt"
RDEPENDS_libreoffice-langpack-ro = "autocorr-ro hunspell-ro hyphen-ro libreoffice-core libreoffice-help-ro mythes-ro"
RDEPENDS_libreoffice-langpack-ru = "autocorr-ru hunspell-ru hyphen-ru libreoffice-core libreoffice-help-ru mythes-ru"
RDEPENDS_libreoffice-langpack-si = "hunspell-si libreoffice-core libreoffice-help-si"
RDEPENDS_libreoffice-langpack-sk = "autocorr-sk hunspell-sk hyphen-sk libreoffice-core libreoffice-help-sk mythes-sk"
RDEPENDS_libreoffice-langpack-sl = "autocorr-sl hunspell-sl hyphen-sl libreoffice-core libreoffice-help-sl mythes-sl"
RDEPENDS_libreoffice-langpack-sr = "autocorr-sr hunspell-sr hyphen-sr libreoffice-core"
RDEPENDS_libreoffice-langpack-ss = "hunspell-ss libreoffice-core"
RDEPENDS_libreoffice-langpack-st = "hunspell-st libreoffice-core"
RDEPENDS_libreoffice-langpack-sv = "autocorr-sv hunspell-sv hyphen-sv libreoffice-core libreoffice-help-sv mythes-sv"
RDEPENDS_libreoffice-langpack-ta = "hunspell-ta hyphen-ta libreoffice-core libreoffice-help-ta"
RDEPENDS_libreoffice-langpack-te = "hunspell-te hyphen-te libreoffice-core"
RDEPENDS_libreoffice-langpack-th = "hunspell-th libreoffice-core"
RDEPENDS_libreoffice-langpack-tn = "hunspell-tn libreoffice-core"
RDEPENDS_libreoffice-langpack-tr = "autocorr-tr libreoffice-core libreoffice-help-tr"
RDEPENDS_libreoffice-langpack-ts = "hunspell-ts libreoffice-core"
RDEPENDS_libreoffice-langpack-uk = "hunspell-uk hyphen-uk libreoffice-core libreoffice-help-uk mythes-uk"
RDEPENDS_libreoffice-langpack-ve = "hunspell-ve libreoffice-core"
RDEPENDS_libreoffice-langpack-xh = "hunspell-xh libreoffice-core"
RDEPENDS_libreoffice-langpack-zh-Hans = "autocorr-zh libreoffice-core libreoffice-help-zh-Hans"
RDEPENDS_libreoffice-langpack-zh-Hant = "autocorr-zh libreoffice-core libreoffice-help-zh-Hant"
RDEPENDS_libreoffice-langpack-zu = "hunspell-zu hyphen-zu libreoffice-core"
RPM_SONAME_PROV_libreoffice-math = "libsmdlo.so libsmlo.so"
RPM_SONAME_REQ_libreoffice-math = "libc.so.6 libcomphelper.so libeditenglo.so libgcc_s.so.1 libi18nlangtag.so libi18nutil.so libm.so.6 libmsfilterlo.so libooxlo.so libsaxlo.so libsfxlo.so libsotlo.so libstdc++.so.6 libsvllo.so libsvtlo.so libsvxcorelo.so libsvxlo.so libtklo.so libtllo.so libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libutllo.so libvcllo.so libxolo.so"
RDEPENDS_libreoffice-math = "bash glibc libgcc libreoffice-core libreoffice-pyuno libreoffice-ure libstdc++"
RPM_SONAME_PROV_libreoffice-ogltrans = "libOGLTranslo.so"
RPM_SONAME_REQ_libreoffice-ogltrans = "libbasegfxlo.so libc.so.6 libcanvastoolslo.so libcomphelper.so libepoxy.so.0 libgcc_s.so.1 libm.so.6 libstdc++.so.6 libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libvcllo.so"
RDEPENDS_libreoffice-ogltrans = "glibc libepoxy libgcc libreoffice-core libreoffice-impress libreoffice-ure libstdc++"
RDEPENDS_libreoffice-opensymbol-fonts = "fontpackages-filesystem"
RPM_SONAME_PROV_libreoffice-pdfimport = "libpdfimportlo.so"
RPM_SONAME_REQ_libreoffice-pdfimport = "libbasegfxlo.so libc.so.6 libcomphelper.so libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libm.so.6 libpoppler.so.78 libstdc++.so.6 libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libvcllo.so libz.so.1"
RDEPENDS_libreoffice-pdfimport = "fontconfig freetype glibc libgcc libreoffice-core libreoffice-draw libreoffice-ure libstdc++ poppler zlib"
RPM_SONAME_PROV_libreoffice-pyuno = "libpythonloaderlo.so libpyuno.so"
RPM_SONAME_REQ_libreoffice-pyuno = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libstdc++.so.6 libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libuno_salhelpergcc3.so.3 libutil.so.1"
RDEPENDS_libreoffice-pyuno = "glibc libgcc libreoffice-core libreoffice-ure libstdc++ platform-python python3-libs"
RPM_SONAME_REQ_libreoffice-sdk = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libreglo.so libstdc++.so.6 libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_purpenvhelpergcc3.so.3 libuno_sal.so.3 libuno_salhelpergcc3.so.3 libunoidllo.so"
RDEPENDS_libreoffice-sdk = "bash glibc java-1.8.0-openjdk-devel libgcc libreoffice-core libreoffice-ure libstdc++ unzip"
RPM_SONAME_PROV_libreoffice-ure = "libaffine_uno_uno.so libbinaryurplo.so libbootstraplo.so libgcc3_uno.so libi18nlangtag.so libintrospectionlo.so libinvocadaptlo.so libinvocationlo.so libiolo.so libjava_uno.so libjavaloaderlo.so libjavavmlo.so libjpipe.so libjuh.so libjuhx.so libjvmaccesslo.so libjvmfwklo.so liblog_uno_uno.so libnamingservicelo.so libproxyfaclo.so libreflectionlo.so libreglo.so libsal_textenclo.so libstocserviceslo.so libstorelo.so libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_purpenvhelpergcc3.so.3 libuno_sal.so.3 libuno_salhelpergcc3.so.3 libunoidllo.so libunsafe_uno_uno.so libuuresolverlo.so libxmlreaderlo.so"
RPM_SONAME_REQ_libreoffice-ure = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libicuuc.so.60 libjvm.so liblangtag.so.1 libm.so.6 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6 libxml2.so.2"
RDEPENDS_libreoffice-ure = "bash glibc java-1.8.0-openjdk-headless libgcc libicu liblangtag libreoffice-ure-common libstdc++ libxml2 openssl-libs unzip"
RDEPENDS_libreoffice-wiki-publisher = "libreoffice-core libreoffice-ure libreoffice-writer"
RPM_SONAME_PROV_libreoffice-writer = "libhwplo.so liblwpftlo.so libmswordlo.so libswuilo.so libt602filterlo.so libvbaswobjlo.so libwpftwriterlo.so libwriterfilterlo.so libwriterlo.so"
RPM_SONAME_REQ_libreoffice-writer = "libabw-0.1.so.1 libbasegfxlo.so libc.so.6 libcomphelper.so libdbtoolslo.so libdrawinglayerlo.so libeditenglo.so libepubgen-0.1.so.1 libetonyek-0.1.so.1 libfilelo.so libgcc_s.so.1 libi18nlangtag.so libi18nutil.so libicui18n.so.60 libicuuc.so.60 liblangtag.so.1 libm.so.6 libmsfilterlo.so libmwaw-0.3.so.3 libodfgen-0.1.so.1 libooxlo.so librevenge-0.0.so.0 librevenge-stream-0.0.so.0 libsaxlo.so libsblo.so libsfxlo.so libsotlo.so libstaroffice-0.0.so.0 libstdc++.so.6 libsvllo.so libsvtlo.so libsvxcorelo.so libsvxlo.so libswlo.so libtklo.so libtllo.so libucbhelper.so libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libuno_salhelpergcc3.so.3 libutllo.so libvbahelperlo.so libvcllo.so libwpd-0.10.so.10 libwpg-0.3.so.3 libwps-0.4.so.4 libwriterperfectlo.so libxml2.so.2 libxolo.so libz.so.1"
RDEPENDS_libreoffice-writer = "bash glibc libabw libepubgen libetonyek libgcc libicu liblangtag libmwaw libodfgen libreoffice-core libreoffice-data libreoffice-pyuno libreoffice-ure librevenge libstaroffice libstdc++ libwpd libwpg libwps libxml2 zlib"
RPM_SONAME_PROV_libreoffice-x11 = "libvclplug_genlo.so"
RPM_SONAME_REQ_libreoffice-x11 = "libICE.so.6 libSM.so.6 libX11.so.6 libXext.so.6 libXinerama.so.1 libXrandr.so.2 libXrender.so.1 libbasegfxlo.so libc.so.6 libcairo.so.2 libcomphelper.so libdl.so.2 libepoxy.so.0 libgcc_s.so.1 libgraphite2.so.3 libharfbuzz-icu.so.0 libharfbuzz.so.0 libi18nlangtag.so libi18nutil.so libicuuc.so.60 libjvmaccesslo.so libm.so.6 libpthread.so.0 libsotlo.so libstdc++.so.6 libtllo.so libucbhelper.so libuno_cppu.so.3 libuno_cppuhelpergcc3.so.3 libuno_sal.so.3 libutllo.so libvcllo.so"
RDEPENDS_libreoffice-x11 = "cairo glibc graphite2 harfbuzz harfbuzz-icu libICE libSM libX11 libXext libXinerama libXrandr libXrender libepoxy libgcc libicu libreoffice-core libreoffice-ure libstdc++"
RDEPENDS_libreoffice-xsltfilter = "libreoffice-core"
RPM_SONAME_PROV_libreofficekit = "liblibreofficekitgtk.so"
RPM_SONAME_REQ_libreofficekit = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdl.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libreofficekit = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libgcc libstdc++ pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-af-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-bg-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-ca-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-cs-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-da-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-de-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-en-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-es-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-fa-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-fi-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-fr-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-ga-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-hr-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-hu-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-is-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-it-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-ja-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-ko-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-lb-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-lt-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-mn-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-nl-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-pl-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-pt-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-ro-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-ru-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-sk-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-sl-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-sr-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-sv-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-tr-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-vi-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/autocorr-zh-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-base-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-calc-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-core-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-data-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-draw-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-emailmerge-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-filters-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-gdb-debug-support-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-graphicfilter-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-gtk2-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-gtk3-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-ar-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-bg-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-bn-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-ca-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-cs-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-da-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-de-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-dz-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-el-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-en-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-es-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-et-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-eu-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-fi-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-fr-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-gl-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-gu-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-he-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-hi-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-hr-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-hu-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-id-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-it-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-ja-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-ko-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-lt-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-lv-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-nb-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-nl-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-nn-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-pl-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-pt-BR-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-pt-PT-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-ro-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-ru-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-si-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-sk-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-sl-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-sv-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-ta-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-tr-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-uk-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-zh-Hans-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-help-zh-Hant-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-impress-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-af-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ar-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-as-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-bg-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-bn-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-br-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ca-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-cs-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-cy-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-da-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-de-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-dz-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-el-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-en-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-es-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-et-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-eu-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-fa-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-fi-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-fr-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ga-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-gl-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-gu-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-he-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-hi-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-hr-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-hu-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-id-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-it-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ja-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-kk-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-kn-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ko-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-lt-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-lv-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-mai-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ml-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-mr-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-nb-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-nl-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-nn-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-nr-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-nso-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-or-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-pa-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-pl-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-pt-BR-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-pt-PT-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ro-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ru-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-si-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-sk-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-sl-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-sr-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ss-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-st-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-sv-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ta-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-te-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-th-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-tn-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-tr-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ts-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-uk-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-ve-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-xh-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-zh-Hans-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-zh-Hant-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-langpack-zu-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-math-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-ogltrans-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-opensymbol-fonts-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-pdfimport-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-pyuno-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-ure-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-ure-common-6.0.6.1-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-wiki-publisher-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-writer-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-x11-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreoffice-xsltfilter-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libreofficekit-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libreoffice-sdk-6.0.6.1-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libreoffice-sdk-doc-6.0.6.1-20.el8.x86_64.rpm \
          "

SRC_URI[autocorr-af.sha256sum] = "7140b0c46ae869271e49aa8327e90e0723f3f1269cd5ed368d9ce864d6f079c0"
SRC_URI[autocorr-bg.sha256sum] = "c6059aaa8ee397ed3b63d9b0e4b9ece322079cdb3a4b8148823f79c87e026e3e"
SRC_URI[autocorr-ca.sha256sum] = "10c0bea9da19afc73dc56af822d4b9e483d15684ef4f6c503ca33e8cbec048fb"
SRC_URI[autocorr-cs.sha256sum] = "bab47acae32e103ea868895c9781e6a8532eb11b6291ed7b6daf52911e7ddb3d"
SRC_URI[autocorr-da.sha256sum] = "0bbd3ff0f6b885f569d83343aa813bbed6b4663ffe53e7797757455a299ba52b"
SRC_URI[autocorr-de.sha256sum] = "6a096a51208294e7a53a08bb7d5ce6137face62483cbb488a2945ae253ac20a0"
SRC_URI[autocorr-en.sha256sum] = "b28376fd2596914ef2a198d0dfa3ca699afaecfc35778be36d3b65567b25ca10"
SRC_URI[autocorr-es.sha256sum] = "5e1863c4db4de3323162989f1003c6f4d629116c8ebb9effd5edaa1dcb3eb3e2"
SRC_URI[autocorr-fa.sha256sum] = "d6dee205cbf7d2d2b190b4a7840a4b90020fc0238ca912243a5f7d0651e9cb46"
SRC_URI[autocorr-fi.sha256sum] = "2d0bd6117635d286a63ab840de3ca0be20b5b8837a717df753c27b35c3dc2fc9"
SRC_URI[autocorr-fr.sha256sum] = "cf83e60daa666fdd1ce579264b22df068bfd7cdbde7236f429a746b54d65025d"
SRC_URI[autocorr-ga.sha256sum] = "69277a705ad5899c77a32b2066a1fe76480b203d644f9e633c257954a14b0756"
SRC_URI[autocorr-hr.sha256sum] = "f257c29605ff20e972636258248e56ce728e83958a0821855afe242f14c61286"
SRC_URI[autocorr-hu.sha256sum] = "c8d9b30d9d5dc25182603a8d65bb8382336b3eaf4cf32d3e7b6ee85b4e2b7b74"
SRC_URI[autocorr-is.sha256sum] = "ddefdc2a7e54370a62007b723f961f5773751f33ce13bbb9162579cf194aa113"
SRC_URI[autocorr-it.sha256sum] = "8b2b74b8d7abd04032999fa4eaa1bb20fb22f5ae0245af2e5387053427d98339"
SRC_URI[autocorr-ja.sha256sum] = "0ffb2d29315bc5fcde427ca69a60c3230c09d192156dd656f1a000c9a5f2cd78"
SRC_URI[autocorr-ko.sha256sum] = "f0a16a93e8e1af580abc43931271f562719e6a6a4ce22a52473e149fe8ad3cfb"
SRC_URI[autocorr-lb.sha256sum] = "646b96af9a4ac1321926347e298c3361e2316afbcff7a8b23360a1985fe39006"
SRC_URI[autocorr-lt.sha256sum] = "dc361a7aaef246c88aa76c6be2629954eb0a0b712fb26eef1c9ca8ba07620f34"
SRC_URI[autocorr-mn.sha256sum] = "6b9b6d07d41206a27867f01cc7708a050c24bbc17acf848b3167bb7a725d6219"
SRC_URI[autocorr-nl.sha256sum] = "db2cfaeae23d2f2e4a3c154905743853fc23c6c29759b9f87ceb68f84b9fa650"
SRC_URI[autocorr-pl.sha256sum] = "667b6cbbf06b6e3cd5910d2911c425409276f397c1ac27ee981a2aee6faba981"
SRC_URI[autocorr-pt.sha256sum] = "92d78120e1b99b78e01bbfca170952e783fae0b1177fd757134135ad7275d778"
SRC_URI[autocorr-ro.sha256sum] = "8f0c9df09e92a917379c77767d1f03a6343dc41f89de6782493da7e0cbf84fbc"
SRC_URI[autocorr-ru.sha256sum] = "bb9a6eec5e36e01653a5208aaa1818be5d1f2185b292f6cd4e75012183f63ead"
SRC_URI[autocorr-sk.sha256sum] = "b5109dbb20f0a04027167b3afea2d45e0ca88d1011cc6300c23d68edbd190f28"
SRC_URI[autocorr-sl.sha256sum] = "6b61fb8d697f6948e23d1964800497a8df8e4d7f9d4c0324ffb6980d6da9e0bf"
SRC_URI[autocorr-sr.sha256sum] = "8afd6e1a69dc0a3c577070f61ef8116ce02b75ff8795d5fbbd67910a755b11f0"
SRC_URI[autocorr-sv.sha256sum] = "fd1095023f6f526d0e3ddc9d75a91de8c8af11bdaf74771cb2bb91a3f846f5bb"
SRC_URI[autocorr-tr.sha256sum] = "7f81d63d189b0fd03e2fd927eb71fc34694d44f5ea96322fdc069558b480a484"
SRC_URI[autocorr-vi.sha256sum] = "cb684e2c9efc7763c3cd92c3e21ed0738f68fe4d0daa3ad418bf9b6149dfd3aa"
SRC_URI[autocorr-zh.sha256sum] = "eb2c234de5b374add5b0b6428c764fdeffa430a420005512e15d84a24d22306c"
SRC_URI[libreoffice-base.sha256sum] = "a4ebaca0a655cf6a21aa6df357f442b16f98457ed2e1dfe3bd34f06e78d7bf54"
SRC_URI[libreoffice-calc.sha256sum] = "6262754ea762bc009e59d1f49bb7fcf0415159ef60723a968c48d9267d4b5237"
SRC_URI[libreoffice-core.sha256sum] = "0d07e1aaae34ba3c9ad3fe1ef4d58921fb2a86c9341f35ffe4d1d98197a073da"
SRC_URI[libreoffice-data.sha256sum] = "9f598d1ff83f6ab4c92b5aa95fd4d3c4bcbb0fa62458eb55e587ac603b0216e2"
SRC_URI[libreoffice-draw.sha256sum] = "aca7cbe57da7677866c9e297770811e2c42b4fc310c445444e586d35c5e50002"
SRC_URI[libreoffice-emailmerge.sha256sum] = "9d7360251c2e7f4304bd4d5c6a04cf1e42e5e29e9072354e3bc7ae43ee02eb67"
SRC_URI[libreoffice-filters.sha256sum] = "d9214d9b344851368fd1392bf523eef2165f6ed7d27aad9a864a1add677709d2"
SRC_URI[libreoffice-gdb-debug-support.sha256sum] = "413663fe982df3964a7ec577dcfbedd0910c4e9c5b8039db0ea2fd9f6095c175"
SRC_URI[libreoffice-graphicfilter.sha256sum] = "133404b279bfb78f2b8f4a73d9b2949ec009a979428138e84c4f9ab6897a9685"
SRC_URI[libreoffice-gtk2.sha256sum] = "8fffaa7b632ef023663a87b3b029521aaff1210cca0eff97a2d027551debfda2"
SRC_URI[libreoffice-gtk3.sha256sum] = "2d5421e2c9fa603c47a16d7fb9c56931d3728308aefa9a53dd9ded239d89a118"
SRC_URI[libreoffice-help-ar.sha256sum] = "caca9bb45c68f3df20832721b04dce2939565a70232e1faaa8c6265b3af414ce"
SRC_URI[libreoffice-help-bg.sha256sum] = "452f30c83492f8c4299ff3d20774dc61f60dbc9c7b60d9e9d2212062fd4a75ba"
SRC_URI[libreoffice-help-bn.sha256sum] = "6b396c3c7777cf093579cf866daa54749b83daa7b57b9b614573b08e63785cf5"
SRC_URI[libreoffice-help-ca.sha256sum] = "523e76f7e023b88909346d89944469c90a5f1ea57661bd8eb0163d174281361f"
SRC_URI[libreoffice-help-cs.sha256sum] = "5948e902a3b7ad317b76cffcfd48dd507d1c1048a908887012474d1f5f5a84cd"
SRC_URI[libreoffice-help-da.sha256sum] = "821f6d54d8bd74c38a884361d9d1e9fba6164df8f13326d837b72470dd53e10d"
SRC_URI[libreoffice-help-de.sha256sum] = "b682371d7755d4c46f07d91fee101af8b6e4664f7caa1c7596878f8d1fbd3f3e"
SRC_URI[libreoffice-help-dz.sha256sum] = "476fea3c929932ee3d21a302a7ecd96857b0361b7ee0d3c6c74cca228ad18331"
SRC_URI[libreoffice-help-el.sha256sum] = "c60e09c3a11d680ff5950bae09611466c65425ebfd0fbd2884a99808d7fd9e4e"
SRC_URI[libreoffice-help-en.sha256sum] = "62ea0e8bee8b8ed67f8c0f43afb28a101d8de35dce2f29381b26abc71d58b4b4"
SRC_URI[libreoffice-help-es.sha256sum] = "4fad80d6ecc12eee0cc928f9e9142b66c2e21a236ff7afb7dafe700ad4f1796a"
SRC_URI[libreoffice-help-et.sha256sum] = "8e7a51286a6389400384461300a782fe623a501524c065ec9d43ec844d84a834"
SRC_URI[libreoffice-help-eu.sha256sum] = "3b80855af5221bbb92be7217125fea7a4ee56b3d1c6025cc00c83436e94beeec"
SRC_URI[libreoffice-help-fi.sha256sum] = "3470da1b172538ff24416db7e3ea9f40793ef05d252c1eb9214901d164781b19"
SRC_URI[libreoffice-help-fr.sha256sum] = "44762cc82daea2dc97d54ffff6a594cfbf866780a5b5795cd1ef9537e7c4ea49"
SRC_URI[libreoffice-help-gl.sha256sum] = "1dabdef15a0b0a1a6cb2cecf4bb93e4a2bc9a46c3de6385e41715094da10a4c8"
SRC_URI[libreoffice-help-gu.sha256sum] = "02ee1c0c786f778e5671f7a6ffbbc94d32d546ddba06159121e09b4356d1cdbe"
SRC_URI[libreoffice-help-he.sha256sum] = "871e53171d1a16099f2814eeab3b56259f80ba11aa5bc01a8ed967d98d4bc734"
SRC_URI[libreoffice-help-hi.sha256sum] = "0d2115febc98fc718de56b55e77061e84ce0a3384bee691384b55f85d2b394f9"
SRC_URI[libreoffice-help-hr.sha256sum] = "a07d4a06ef03e6a638edbfb141906148d6429283ca630b0f1aacbe8eefc058f2"
SRC_URI[libreoffice-help-hu.sha256sum] = "2ef73272d57d6a91e80b6ea038779ad76a6399a270dd39990c85689e5276e2d7"
SRC_URI[libreoffice-help-id.sha256sum] = "e6e489f0fe608d3775ea0070534ccbb5b808041e45cc2a9c489ac478e5d8e0c3"
SRC_URI[libreoffice-help-it.sha256sum] = "7533cdca9c0608f034fd6919c0cec4f22834eae509ddeb00938ba64d3df36b19"
SRC_URI[libreoffice-help-ja.sha256sum] = "51d5635debf362bf17a1b7d4a49c80543089623694f026708d267c34d5d442f2"
SRC_URI[libreoffice-help-ko.sha256sum] = "8d369b409afc95db295152d240bfb12adea404a796fcdc078de9cf6c6b3526b2"
SRC_URI[libreoffice-help-lt.sha256sum] = "fdc84c74463049fbe1bb32cf0b84470a9229883fb7dd4b6de7456587008c8163"
SRC_URI[libreoffice-help-lv.sha256sum] = "7b023e440b01b1b597475c598248245f4c1055e9261f82958c833392617c04d9"
SRC_URI[libreoffice-help-nb.sha256sum] = "0988088b71f1a5b1192c07c9505eb9020a9d49198c1455438f993e111e6bb515"
SRC_URI[libreoffice-help-nl.sha256sum] = "9e732ef982d1d9bb06c43135ee3726c8d8d0966040ee1a6903c56a97057da693"
SRC_URI[libreoffice-help-nn.sha256sum] = "c33b0061d0ceab3862c28322ee8d8ff804d5042da0d1533b8eb7229b0f92c79b"
SRC_URI[libreoffice-help-pl.sha256sum] = "dc0a9b6bcfdf69e8eb29b896e624d52d9f6996cc6dc0aaab5481f7827585a5b3"
SRC_URI[libreoffice-help-pt-BR.sha256sum] = "89057c289ce1e0e4b0bc7621143b0fe32b9a6b12c5f11d0a58019220a67b19d1"
SRC_URI[libreoffice-help-pt-PT.sha256sum] = "24b0cfcce9a1e83af15e3a706f9b7b643b6eee01f973889311e77a5080834f8f"
SRC_URI[libreoffice-help-ro.sha256sum] = "49c41d5c031decffe242f3b4ac25884a93733d5b622852314d36fc16bbf9c66f"
SRC_URI[libreoffice-help-ru.sha256sum] = "7667717813db937730394b40eac33849a9f0dccce4fdd8423cc40488f2faa4a2"
SRC_URI[libreoffice-help-si.sha256sum] = "74c1205d38cc99f2911ded4e79f823bcb103b888e78e2ad7c4edbd236f6da2a6"
SRC_URI[libreoffice-help-sk.sha256sum] = "bf0788bbaf3c97c98d993a6747d09686d44fa78c57d6ae314430100e521842a1"
SRC_URI[libreoffice-help-sl.sha256sum] = "cec1438bce6a780afc3a3d489a9eb8606632ade41d7cb85fc5b6be31842f1e27"
SRC_URI[libreoffice-help-sv.sha256sum] = "a14f0e46d38cf5bd3ad583860d9859ae1d86b5f993b08326095675404ecadb3a"
SRC_URI[libreoffice-help-ta.sha256sum] = "046758aea0c6bf9ad8d880be4d9b8c0c2dac5de81198e0b85de8e789b0076718"
SRC_URI[libreoffice-help-tr.sha256sum] = "daa9ef43eb880d829607b19b617889c7de8f63886b0eee59ef12ca94199eb926"
SRC_URI[libreoffice-help-uk.sha256sum] = "ca29424580689c33d5c902da3452346ebe6b23cd535a4adc1901b86af2991ead"
SRC_URI[libreoffice-help-zh-Hans.sha256sum] = "91d48b70ee878f486baeda4be08f7065f662c4d1e43cff5b6db5933103d44913"
SRC_URI[libreoffice-help-zh-Hant.sha256sum] = "65a32e1e9b7cfb2579b290b0d1da316ecd761ed392307c1483478ec26dfe062c"
SRC_URI[libreoffice-impress.sha256sum] = "f422c35a889401c4ebede7928f38ec1107f0af07e048f01d1111e5046df0682a"
SRC_URI[libreoffice-langpack-af.sha256sum] = "2f1fb9b1fd1df99018c4782a2f983279b747030b7f54623ecbc1adcc9fd0a982"
SRC_URI[libreoffice-langpack-ar.sha256sum] = "b11642210c043680e955add4d37d22834ecd29d509c9f0eee6f13aafccb93531"
SRC_URI[libreoffice-langpack-as.sha256sum] = "3a05f17f47fbcf9541c298bdc9103f76febf235c11063ee4ba79387983d8e757"
SRC_URI[libreoffice-langpack-bg.sha256sum] = "8b88f5289ebebb1ed4a4183244d164c9dd797162f263be89535564a78bda31ea"
SRC_URI[libreoffice-langpack-bn.sha256sum] = "f26e58faccd25537b31bcd9dd6546cb48759df8afb8381b754f9cfc656bf1f52"
SRC_URI[libreoffice-langpack-br.sha256sum] = "0aec2b072cf59ab45fa584a0f588a70ec6dccd03883ca9461482bb7ae0269f64"
SRC_URI[libreoffice-langpack-ca.sha256sum] = "ddc74d29c2302dbe1734ecc680dab84ead6468f830f9df7d12fb26bf739a49e9"
SRC_URI[libreoffice-langpack-cs.sha256sum] = "f1bbe417b51c45161db3c64143b38cd8d33ba5107a63985dfb32821752ead15c"
SRC_URI[libreoffice-langpack-cy.sha256sum] = "2639435b62d483b16ae5d95410bc6c79b82e23f4bb5d3472aba4153fa3384b86"
SRC_URI[libreoffice-langpack-da.sha256sum] = "e5081b897e59848512bd52947f10d59fc024c28205f09a033ada30223a834ec4"
SRC_URI[libreoffice-langpack-de.sha256sum] = "c3fa03cbde482a7f040d85fee566e76734fc7c30cee55762c9db9f2cf1bd0a47"
SRC_URI[libreoffice-langpack-dz.sha256sum] = "4c284fa67883b06cb2c4b0f0b9ba706c60c27e7c4c7339fbe0b93718b10ada0c"
SRC_URI[libreoffice-langpack-el.sha256sum] = "6005a9fee495c658bdece55dfb3e79416e3ea1eeaf5a3f40d5247aa9c3e71874"
SRC_URI[libreoffice-langpack-en.sha256sum] = "6cb1dceaa1929f6ed2afa1a8a2ecc5cece3a5527e1287ea41e52b947f69b8659"
SRC_URI[libreoffice-langpack-es.sha256sum] = "ccaf66c3047ad5df5fed8edfa695c670648f7d4c5b3bf1cbc983d1180bad42d2"
SRC_URI[libreoffice-langpack-et.sha256sum] = "ea64f269357393ce24b798ad575cd0c3ef1f208733f7ea0d951e09a23597aaf6"
SRC_URI[libreoffice-langpack-eu.sha256sum] = "d1eb19dc68315e446262888f277fb77ccad6ac8bea3b8b790f914a027e015479"
SRC_URI[libreoffice-langpack-fa.sha256sum] = "aadff17487e4d980893e60ec1ad893d037327a8a4be30f76411268a100f828d8"
SRC_URI[libreoffice-langpack-fi.sha256sum] = "fb495b7a635f59bab2fdd096f92269af5c40808263e1945b6290b23a9e21a1b6"
SRC_URI[libreoffice-langpack-fr.sha256sum] = "01fe914e8a3fa5c14242791c4f486eb73d5b810250b7b2df36fd3acd7ed1f0ca"
SRC_URI[libreoffice-langpack-ga.sha256sum] = "b047b55cf0d2c7ea07630f71e522e4fba11d19014b6cf791d267f06e71045167"
SRC_URI[libreoffice-langpack-gl.sha256sum] = "a9b15ad9e34276d595bed0326afd9f6f7a1711c852ee688597d43dc3d2f9ad7f"
SRC_URI[libreoffice-langpack-gu.sha256sum] = "3400c8ec30657a4704a13a0eb8afc980db59cee00aa7456731196cb611265253"
SRC_URI[libreoffice-langpack-he.sha256sum] = "6ff7320bdbe70e1e6b4c84484ab30dd6542e43221b4e69b1170246ef8a95bf64"
SRC_URI[libreoffice-langpack-hi.sha256sum] = "01a77c66c14437eec351c62106b1e928976cfb1b90ad5601e214319cb3f86028"
SRC_URI[libreoffice-langpack-hr.sha256sum] = "f037e169e0683213b9584d1ec08e8765bdf66410d96539af79905bd0f9b99705"
SRC_URI[libreoffice-langpack-hu.sha256sum] = "d991b2e5a45f611a125f6e1e3cd08393981534c22be3f4299ce0c35042dcb2fd"
SRC_URI[libreoffice-langpack-id.sha256sum] = "2a0948329045884a26118ac1675a33658d043e9501b3ea11f9e721bdc12ff270"
SRC_URI[libreoffice-langpack-it.sha256sum] = "d0911b505273003fa6bac7c83bc06e84b4746c3f53daa24388ce649b6f32a62b"
SRC_URI[libreoffice-langpack-ja.sha256sum] = "cbe3d00538f07e284931714b5a1c84264e306c2be5c4bf685920d3827b34b85c"
SRC_URI[libreoffice-langpack-kk.sha256sum] = "87326e1fbe5c1d9feeaa18710b504e1ae909c00ee1bf762b9e2d3470e0aa27d5"
SRC_URI[libreoffice-langpack-kn.sha256sum] = "cd5decc736b5457b6fd564ddb48882e47ab70e494bfe78b31cb8a5d04b9cbce5"
SRC_URI[libreoffice-langpack-ko.sha256sum] = "cf8d50e9492943187e8c2a9126dd051a53f2ca002a0aadcf2c616c111a4c4f10"
SRC_URI[libreoffice-langpack-lt.sha256sum] = "7b4e6a4f194c50b18745022edbc4d4c63481242aab6ada48818614f395d9cb6a"
SRC_URI[libreoffice-langpack-lv.sha256sum] = "2f3772b5eb4831e1e11fb70b93eceaeca580a87c38cc5922f97525c55c6597ef"
SRC_URI[libreoffice-langpack-mai.sha256sum] = "b92a80b91c499c654d9931bea2e167260306d2f5593f305acb0d9f6360301452"
SRC_URI[libreoffice-langpack-ml.sha256sum] = "39cdaa2878561fc27dffa542fea8d1431486262d44d77a76d6522a275874b680"
SRC_URI[libreoffice-langpack-mr.sha256sum] = "bf77264f14dffbca6a2dc02605d8bed50f0eb2bd5c14a90657ad8058fd4a17ac"
SRC_URI[libreoffice-langpack-nb.sha256sum] = "5f0904dfb78a8e29645ba3a0c3741452805df2f3f520d1602a5a389bf520d358"
SRC_URI[libreoffice-langpack-nl.sha256sum] = "4ee6a9c1ad0c569001af1211cc0c14bca5e9ee4fc3e7fb31c2ca98f9e7bc527e"
SRC_URI[libreoffice-langpack-nn.sha256sum] = "f77de39c56c87eb6f474de869df4f0d8dec8278b29c56150602df6abfa03e7df"
SRC_URI[libreoffice-langpack-nr.sha256sum] = "a1beaea487e78572ed9c103f870a17e3dbb6d378ce137a26c102bd06e90b9fff"
SRC_URI[libreoffice-langpack-nso.sha256sum] = "ea9e351afb8d22ed650ed8f237702c09ba750c6d420bab6d0003d03079d995c0"
SRC_URI[libreoffice-langpack-or.sha256sum] = "6ad30ed929e95365e42505b3fda2fb461c533b7635b9a03731f9c263a3acdaf0"
SRC_URI[libreoffice-langpack-pa.sha256sum] = "aaf0047900fb51a6dabc136f64e91ad2139bb69405d349ef5f3d26a957ad5583"
SRC_URI[libreoffice-langpack-pl.sha256sum] = "9e2cac3b146ffc17e99ad540f7deb59bee58a2ded57fd2aacd5edee2dcf187e4"
SRC_URI[libreoffice-langpack-pt-BR.sha256sum] = "4e52e62f4926a683d0f4fe42ef9975002a649ef6882fc3f14fbbc6ff39baa849"
SRC_URI[libreoffice-langpack-pt-PT.sha256sum] = "10d801b0a1ff779efdbea1a96fa1288fdbb0691b2c5db6ead5401d3e55a7c189"
SRC_URI[libreoffice-langpack-ro.sha256sum] = "db90d2e04efd1021eae8c21c963f5b6de995646494eca248996a09b4bf7cbeea"
SRC_URI[libreoffice-langpack-ru.sha256sum] = "8ef9f0891f58fad0582af9d252b93ec17d0932a4ed200a16594a52a65ba4acc9"
SRC_URI[libreoffice-langpack-si.sha256sum] = "91162f6a324160a81a0441d79eb03362878cdd2c1294f1a0c6f260ad9d72a597"
SRC_URI[libreoffice-langpack-sk.sha256sum] = "7f000a295c315183f6366ea82afb3741a5dd1e2d90886419917033bcb329fe8c"
SRC_URI[libreoffice-langpack-sl.sha256sum] = "75f8c7102f70882f064bd28495a8ccaf73ecf0dfdaa40ddc706ebba17dc71c64"
SRC_URI[libreoffice-langpack-sr.sha256sum] = "31988fe13015c30bd84fcabd97bb66f44c02469568a1984b72f263735bdd5518"
SRC_URI[libreoffice-langpack-ss.sha256sum] = "8816620cc1c033a37fe52a8d2548a021dbe32c913843456177f50476b1051840"
SRC_URI[libreoffice-langpack-st.sha256sum] = "92c09373fb424390633d5ad1658644acfc9bc51936951f4b28d3b01f4bba0bfd"
SRC_URI[libreoffice-langpack-sv.sha256sum] = "b4ebe59ea3aba13f18395bddeffcfd395abf01fd3b111512c7177d19f66a2b54"
SRC_URI[libreoffice-langpack-ta.sha256sum] = "799f90bda0694a217a64015935c6e2d2923a18d6656f56d7794571f10da45bcd"
SRC_URI[libreoffice-langpack-te.sha256sum] = "eb8879ec43f22eb4b39a5e31e482bc9088e782efd4aed173bb45d36fe997d0fa"
SRC_URI[libreoffice-langpack-th.sha256sum] = "bf6290103f02b1a7f0f0cda4c2bf2ffa7fd1957faa27a62a23a11443ef974e89"
SRC_URI[libreoffice-langpack-tn.sha256sum] = "59056f451c6c86c3092e6b781da6b31758df6e42c3f1bf2d1a2e684040846fe7"
SRC_URI[libreoffice-langpack-tr.sha256sum] = "dbfde11deb5d672cdfed22abbf352101e58f6ecaeded28c9ed98924a1c1df797"
SRC_URI[libreoffice-langpack-ts.sha256sum] = "4f88e36291d9f6da970508c85a2dc68548af57349960f2853e1834b8e2c49068"
SRC_URI[libreoffice-langpack-uk.sha256sum] = "17bf7ef787a3f5879a040a59f34688936a3af679a20bbcaf7ec9a6201f09980a"
SRC_URI[libreoffice-langpack-ve.sha256sum] = "06dd504227050fd52cb9431921429863a11b4a287d5500fdafb2d15b3420dc4c"
SRC_URI[libreoffice-langpack-xh.sha256sum] = "57d0baab4789ce588a761f887114eac3ff90511b459e35d0dd4bc0788502c00b"
SRC_URI[libreoffice-langpack-zh-Hans.sha256sum] = "35eff82995a6990369e2e4df6e088bd1d727558d6311e4091050baedf1d65ceb"
SRC_URI[libreoffice-langpack-zh-Hant.sha256sum] = "666cb7a52cb653b6f5fb7a78d1ff79be2388c6568df51d9a2f8da54932bba919"
SRC_URI[libreoffice-langpack-zu.sha256sum] = "72b8dbda492c4bafbbec916d099b9b8350a6796969300f57c9c7a4b39967646e"
SRC_URI[libreoffice-math.sha256sum] = "72dd05e34516009923fcc14c771042540cc6ce8b6eff45d847663dbe9e6ad602"
SRC_URI[libreoffice-ogltrans.sha256sum] = "bddf69f9b0411c18b41deba4d70c05985341dca63ef66c0f173c64662fe41499"
SRC_URI[libreoffice-opensymbol-fonts.sha256sum] = "492ce95c0e0292c62d3542b2b6565fb603d2b877f19f753a82d97353a27c581d"
SRC_URI[libreoffice-pdfimport.sha256sum] = "c637f51c0e4f62f8db8ab0ddf61e9dec8d5f296b544b1cb62b3a6ec8a9441eff"
SRC_URI[libreoffice-pyuno.sha256sum] = "727ffd73dd239784deac7da794b62f917d16fe5ae0221f555b5061eb233175a8"
SRC_URI[libreoffice-sdk.sha256sum] = "88f50a12813b0827a5e1bf13537c5aa1a5f7dc12807931d83bf640ccdd1b40f8"
SRC_URI[libreoffice-sdk-doc.sha256sum] = "48223ffc94a644c5485bdc194a2913315357f723557b01bc6dd53e80feb88db4"
SRC_URI[libreoffice-ure.sha256sum] = "7ecfb67248dca29acf261cd61a8d2fbd27ebc4f7ffe727dc4d69583edb24a10c"
SRC_URI[libreoffice-ure-common.sha256sum] = "ee3b74a399f13e07286a0396a1fad08afd3d7322b7ced3fe22c1ad962ab171ac"
SRC_URI[libreoffice-wiki-publisher.sha256sum] = "9ce075df94303817319db9a48b114f6e22774514adcf762caca066e9210fdc7c"
SRC_URI[libreoffice-writer.sha256sum] = "f4d7ddaa63fcc365bf5c5295fc757574ce9556d9f294c1436fe11d84b8e1544b"
SRC_URI[libreoffice-x11.sha256sum] = "1ce903e150c9ddb29324756428e0b566295763fa6af9be3438c4d3d0981f6f4e"
SRC_URI[libreoffice-xsltfilter.sha256sum] = "431e0329a0230fe8da7cc4ace8a8085785eb44987010547d56a56bb1cfd65b28"
SRC_URI[libreofficekit.sha256sum] = "a88b83a3c8756b49624e814d49b9a3662a3440a43bfb4954d6b4ab5e5f4eed41"
