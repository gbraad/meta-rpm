SUMMARY = "generated recipe based on python-markupsafe srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-markupsafe = "libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-markupsafe = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-markupsafe-0.23-19.el8.x86_64.rpm \
          "

SRC_URI[python3-markupsafe.sha256sum] = "9c7a5a6f73c8e32559226c54d229cb25304a81a853af22d9f829b9bb09b21f53"
