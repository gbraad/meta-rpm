SUMMARY = "generated recipe based on perl-Test-Differences srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Differences = "perl-Carp perl-Data-Dumper perl-Exporter perl-Text-Diff perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Differences-0.6400-8.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Differences.sha256sum] = "3670247bf83dc5475c2f7c85b26e6b9959d96acaadf09a7f4819a24fc455f7bc"
