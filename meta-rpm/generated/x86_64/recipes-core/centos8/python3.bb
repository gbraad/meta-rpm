SUMMARY = "generated recipe based on python36 srpm"
DESCRIPTION = "Description"
LICENSE = "Python-2.0"
RPM_LICENSE = "Python"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPROVIDES_python36 = "python3 (= 3.6.8)"
RDEPENDS_python36 = "bash chkconfig platform-python python3-pip python3-setuptools"
RPROVIDES_python36-devel = "python3-dev (= 3.6.8) python36-dev (= 3.6.8)"
RDEPENDS_python36-devel = "bash platform-python-devel python36"
RDEPENDS_python36-rpm-macros = "python3-rpm-macros"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python36-3.6.8-2.module_el8.1.0+245+c39af44f.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python36-devel-3.6.8-2.module_el8.1.0+245+c39af44f.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python36-rpm-macros-3.6.8-2.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python36.sha256sum] = "35fe02c1239fc2990db4b92a0230636ae5ab7bc31c03ec5d9e16762a7b736530"
SRC_URI[python36-devel.sha256sum] = "8c44b5d68bb29f9702230931c2754030d1c92a4ee38f83f3a3ab709d7942d965"
SRC_URI[python36-rpm-macros.sha256sum] = "a0da789613911ef63eb47d159a589fe23d2f9945392dbd87ce822dedc9a03ecd"
