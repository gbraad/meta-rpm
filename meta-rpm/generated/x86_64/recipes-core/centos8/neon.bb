SUMMARY = "generated recipe based on neon srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "LGPLv2+ and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "e2fsprogs expat krb5-libs libproxy openssl pakchois pkgconfig-native zlib"
RPM_SONAME_PROV_neon = "libneon.so.27"
RPM_SONAME_REQ_neon = "libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libexpat.so.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libpakchois.so.0 libproxy.so.1 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_neon = "ca-certificates expat glibc krb5-libs libcom_err libproxy openssl-libs pakchois zlib"
RPM_SONAME_REQ_neon-devel = "libneon.so.27"
RPROVIDES_neon-devel = "neon-dev (= 0.30.2)"
RDEPENDS_neon-devel = "bash expat-devel neon openssl-devel pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/neon-0.30.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/neon-devel-0.30.2-6.el8.x86_64.rpm \
          "

SRC_URI[neon.sha256sum] = "e69e650a1776329872fea251e4af6cf10ddb2e65325d71fe6771c574cd1fce5b"
SRC_URI[neon-devel.sha256sum] = "9bf880441841cbcdce2d34c80e1ef18dd4e68e8336838e09b07353ed1c7fe4f8"
