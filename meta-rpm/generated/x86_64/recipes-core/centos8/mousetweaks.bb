SUMMARY = "generated recipe based on mousetweaks srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GFDL-1.1"
RPM_LICENSE = "GPLv3 and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libx11 libxcursor libxfixes libxtst pango pkgconfig-native"
RPM_SONAME_REQ_mousetweaks = "libX11.so.6 libXcursor.so.1 libXfixes.so.3 libXtst.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_mousetweaks = "GConf2 atk bash cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libX11 libXcursor libXfixes libXtst pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mousetweaks-3.12.0-11.el8.x86_64.rpm \
          "

SRC_URI[mousetweaks.sha256sum] = "a24394a99fa431cf83d8ebf005ed887fe230ccd9b4b315895d6cf06829c5602f"
