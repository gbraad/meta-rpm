SUMMARY = "generated recipe based on hyphen-mr srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-mr = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-mr-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-mr.sha256sum] = "3542ec1bd57272b335b6043da1cbe810ed52b45cefb1e644de3ba5eef0b923db"
