SUMMARY = "generated recipe based on perl-MRO-Compat srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-MRO-Compat = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-MRO-Compat-0.13-4.el8.noarch.rpm \
          "

SRC_URI[perl-MRO-Compat.sha256sum] = "7d4bf25fe0e682be066c01634f1b517cd672516dda33ab4edbbc7f5ab53a002a"
