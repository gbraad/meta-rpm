SUMMARY = "generated recipe based on libselinux srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native ruby"
RPM_SONAME_REQ_libselinux-ruby = "libc.so.6 libruby.so.2.5 libselinux.so.1"
RDEPENDS_libselinux-ruby = "glibc libselinux ruby-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libselinux-ruby-2.9-3.el8.x86_64.rpm \
          "

SRC_URI[libselinux-ruby.sha256sum] = "9e06feae2af91eff9de5ecde844e3f99824c6c9ea30c2c87e66a2c92b1f06a7d"
