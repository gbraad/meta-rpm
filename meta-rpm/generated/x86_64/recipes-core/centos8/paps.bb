SUMMARY = "generated recipe based on paps srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cups-libs e2fsprogs fontconfig freetype glib-2.0 krb5-libs libxcrypt pango pkgconfig-native zlib"
RPM_SONAME_REQ_paps = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpango-1.0.so.0 libpangoft2-1.0.so.0 libpaps.so.0 libpthread.so.0 libz.so.1"
RDEPENDS_paps = "cups-filesystem cups-libs fontconfig fontpackages-filesystem freetype glib2 glibc krb5-libs libcom_err libxcrypt pango paps-libs zlib"
RPM_SONAME_PROV_paps-libs = "libpaps.so.0"
RPM_SONAME_REQ_paps-libs = "libc.so.6 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libpango-1.0.so.0 libpangoft2-1.0.so.0"
RDEPENDS_paps-libs = "fontconfig freetype glib2 glibc pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/paps-0.6.8-41.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/paps-libs-0.6.8-41.el8.x86_64.rpm \
          "

SRC_URI[paps.sha256sum] = "5a0af326f357193b8b6b69d5dff8297d743b25ae5720ca07f5849c0bb7903ff4"
SRC_URI[paps-libs.sha256sum] = "f267b15ff2724e2a2623018083176834a30ff63582892368f4ffe3fe1c451c5f"
