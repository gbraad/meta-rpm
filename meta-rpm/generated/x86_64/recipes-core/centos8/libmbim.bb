SUMMARY = "generated recipe based on libmbim srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libgudev pkgconfig-native"
RPM_SONAME_PROV_libmbim = "libmbim-glib.so.4"
RPM_SONAME_REQ_libmbim = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0"
RDEPENDS_libmbim = "glib2 glibc libgudev"
RPM_SONAME_REQ_libmbim-utils = "libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libmbim-glib.so.4"
RDEPENDS_libmbim-utils = "bash glib2 glibc libgudev libmbim"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libmbim-1.20.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libmbim-utils-1.20.2-1.el8.x86_64.rpm \
          "

SRC_URI[libmbim.sha256sum] = "e2549a249d537f80f6595816a1094532c3feb0a730585d102c580583cc1dfde4"
SRC_URI[libmbim-utils.sha256sum] = "f790391bcc29a68970a4a2ce6138ca835e88c2006a411d069127f1bee9e15045"
