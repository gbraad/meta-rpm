SUMMARY = "generated recipe based on qt5-qtscript srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase"
RPM_SONAME_PROV_qt5-qtscript = "libQt5Script.so.5 libQt5ScriptTools.so.5"
RPM_SONAME_REQ_qt5-qtscript = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtscript = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_REQ_qt5-qtscript-devel = "libQt5Script.so.5 libQt5ScriptTools.so.5"
RPROVIDES_qt5-qtscript-devel = "qt5-qtscript-dev (= 5.12.5)"
RDEPENDS_qt5-qtscript-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtscript"
RPM_SONAME_REQ_qt5-qtscript-examples = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Script.so.5 libQt5ScriptTools.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtscript-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtscript"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtscript-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtscript-devel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtscript-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtscript.sha256sum] = "79248f7b256293517ba9f4f6f0e6dfa65cf68414a8c844f7826fca397bd900b4"
SRC_URI[qt5-qtscript-devel.sha256sum] = "ca5577e4b596a184e4e1990edb2eb79872baa73acda83e8cfc22de80439ab5d0"
SRC_URI[qt5-qtscript-examples.sha256sum] = "c529dd5b4c7a2f3404dc14bb957e330c1cc8fff01245d4d061dfdd5841247ba4"
