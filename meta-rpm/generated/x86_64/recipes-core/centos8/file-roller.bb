SUMMARY = "generated recipe based on file-roller srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo file gdk-pixbuf glib-2.0 gtk+3 json-glib libarchive libnotify pango pkgconfig-native"
RPM_SONAME_REQ_file-roller = "libarchive.so.13 libc.so.6 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjson-glib-1.0.so.0 libmagic.so.1 libnotify.so.4 libpango-1.0.so.0 libpthread.so.0"
RDEPENDS_file-roller = "binutils bzip2 cairo cpio desktop-file-utils file-libs gdk-pixbuf2 genisoimage glib2 glibc gtk3 gzip json-glib libarchive libnotify lzop ncompress pango tar unzip xz zip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/file-roller-3.28.1-2.el8.x86_64.rpm \
          "

SRC_URI[file-roller.sha256sum] = "4c08d3512319aa1ccb3348bf3437ea00c36f05cac0248b68414236477280f413"
