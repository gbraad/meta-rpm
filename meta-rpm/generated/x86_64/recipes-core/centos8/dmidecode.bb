SUMMARY = "generated recipe based on dmidecode srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_dmidecode = "libc.so.6"
RDEPENDS_dmidecode = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dmidecode-3.2-5.el8.x86_64.rpm \
          "

SRC_URI[dmidecode.sha256sum] = "ab4eda7a8142f4b09b3d084902128fda3472aba66de68f40412f8dfc8d221294"
