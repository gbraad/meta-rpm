SUMMARY = "generated recipe based on dblatex srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0 & LPPL-1.0 & CLOSED & CLOSED"
RPM_LICENSE = "GPLv2+ and GPLv2 and LPPL and DMIT and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_dblatex = "docbook-dtds libxslt platform-python texlive-anysize texlive-appendix texlive-base texlive-bibtopic texlive-changebar texlive-collection-fontsrecommended texlive-collection-htmlxml texlive-collection-latex texlive-collection-xetex texlive-ec texlive-fancybox texlive-jknapltx texlive-kpathsea texlive-multirow texlive-overpic texlive-passivetex texlive-pdfpages texlive-stmaryrd texlive-subfigure texlive-wasysym texlive-xmltex"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/dblatex-0.3.10-8.el8.noarch.rpm \
          "

SRC_URI[dblatex.sha256sum] = "d615df9315dcee36b69729849cdda6c00945c608db0f71a0a8bc1e60349df429"
