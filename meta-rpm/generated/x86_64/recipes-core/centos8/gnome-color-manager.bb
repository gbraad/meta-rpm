SUMMARY = "generated recipe based on gnome-color-manager srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo colord colord-gtk compat-exiv2-026 gdk-pixbuf glib-2.0 gtk+3 lcms2 libcanberra libexif libgcc pango pkgconfig-native tiff vte291"
RPM_SONAME_REQ_gnome-color-manager = "libc.so.6 libcairo.so.2 libcanberra-gtk3.so.0 libcanberra.so.0 libcolord-gtk.so.1 libcolord.so.2 libexif.so.12 libexiv2.so.26 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 liblcms2.so.2 libm.so.6 libpango-1.0.so.0 libstdc++.so.6 libtiff.so.5 libvte-2.91.so.0"
RDEPENDS_gnome-color-manager = "cairo colord-gtk colord-libs compat-exiv2-026 gdk-pixbuf2 glib2 glibc gtk3 lcms2 libcanberra libcanberra-gtk3 libexif libgcc libstdc++ libtiff pango shared-mime-info vte291"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-color-manager-3.28.0-3.el8.x86_64.rpm \
          "

SRC_URI[gnome-color-manager.sha256sum] = "509e7b30bc292568ccf0841443f5907cb06f25bb4ec7685c89e86857feb665d3"
