SUMMARY = "generated recipe based on hunspell-cop srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-cop = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-cop-0.3-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-cop.sha256sum] = "7bc1fd0dd67f6ed3b246b537997e741904b72b18240613c67b2eb182d324037a"
