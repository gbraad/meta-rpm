SUMMARY = "generated recipe based on liblangtag srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0 | MPL-2.0"
RPM_LICENSE = "LGPLv3+ or MPLv2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libxml2 pkgconfig-native"
RPM_SONAME_PROV_liblangtag = "liblangtag-ext-ldml-t.so liblangtag-ext-ldml-u.so liblangtag.so.1"
RPM_SONAME_REQ_liblangtag = "libc.so.6 libdl.so.2 libxml2.so.2"
RDEPENDS_liblangtag = "glibc liblangtag-data libxml2"
RPM_SONAME_REQ_liblangtag-devel = "liblangtag-gobject.so.0 liblangtag.so.1"
RPROVIDES_liblangtag-devel = "liblangtag-dev (= 0.6.2)"
RDEPENDS_liblangtag-devel = "glib2-devel liblangtag liblangtag-gobject libxml2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_liblangtag-gobject = "liblangtag-gobject.so.0"
RPM_SONAME_REQ_liblangtag-gobject = "libc.so.6 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 liblangtag.so.1 libxml2.so.2"
RDEPENDS_liblangtag-gobject = "glib2 glibc liblangtag libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liblangtag-0.6.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liblangtag-data-0.6.2-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/liblangtag-devel-0.6.2-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/liblangtag-doc-0.6.2-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/liblangtag-gobject-0.6.2-5.el8.x86_64.rpm \
          "

SRC_URI[liblangtag.sha256sum] = "bcf522116ca81a2cefab84209e73b0a7920a9300247fc6caf328955b574eaa44"
SRC_URI[liblangtag-data.sha256sum] = "ae50b9fd0e6b204c8c8f3ac0ede8985f550f3e368fa1ed8c5d607007f256191b"
SRC_URI[liblangtag-devel.sha256sum] = "d9ace9a69b9d8fee00a7ede61ea95d948c3a965d66065d36085bd0de6b17541c"
SRC_URI[liblangtag-doc.sha256sum] = "5a5942091765b2c01b88537a9b6aa9e5c86c8cef3256ea7ce1a2cb300d8aa989"
SRC_URI[liblangtag-gobject.sha256sum] = "5347241234d3d65f4ebdb8abf4396c89302ee2c02075f7c769c5c0eee291b4b4"
