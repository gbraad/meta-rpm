SUMMARY = "generated recipe based on qt5-qttools srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0 | LGPL-2.0"
RPM_LICENSE = "LGPLv3 or LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "clang libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-assistant = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Help.so.5 libQt5Network.so.5 libQt5PrintSupport.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-assistant = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common qt5-qttools-libs-help"
RPM_SONAME_REQ_qt5-designer = "libGL.so.1 libQt5Core.so.5 libQt5Designer.so.5 libQt5DesignerComponents.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5PrintSupport.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-designer = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-libs-designer qt5-qttools-libs-designercomponents"
RPM_SONAME_REQ_qt5-doctools = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Help.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libclang.so.9 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-doctools = "clang-libs glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qttools-libs-help"
RPM_SONAME_REQ_qt5-linguist = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5PrintSupport.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-linguist = "cmake-filesystem glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common"
RPM_SONAME_REQ_qt5-qdbusviewer = "libGL.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qdbusviewer = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common"
RPM_SONAME_REQ_qt5-qttools = "libQt5Core.so.5 libQt5DBus.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qttools = "glibc libgcc libstdc++ qt5-qtbase qt5-qttools-common"
RPM_SONAME_REQ_qt5-qttools-devel = "libGL.so.1 libQt5Core.so.5 libQt5Designer.so.5 libQt5DesignerComponents.so.5 libQt5Gui.so.5 libQt5Help.so.5 libQt5Network.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_qt5-qttools-devel = "qt5-qttools-dev (= 5.12.5)"
RDEPENDS_qt5-qttools-devel = "cmake-filesystem glibc libgcc libglvnd-glx libstdc++ pkgconf-pkg-config qt5-designer qt5-doctools qt5-linguist qt5-qtbase qt5-qtbase-devel qt5-qtbase-gui qt5-qttools qt5-qttools-libs-designer qt5-qttools-libs-designercomponents qt5-qttools-libs-help"
RPM_SONAME_PROV_qt5-qttools-examples = "libcontainerextension.so libcustomwidgetplugin.so libqquickwidget.so libtaskmenuextension.so libworldtimeclockplugin.so"
RPM_SONAME_REQ_qt5-qttools-examples = "libGL.so.1 libQt5Core.so.5 libQt5Designer.so.5 libQt5Gui.so.5 libQt5Help.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickWidgets.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qttools-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qttools-common qt5-qttools-libs-designer qt5-qttools-libs-help"
RPM_SONAME_PROV_qt5-qttools-libs-designer = "libQt5Designer.so.5"
RPM_SONAME_REQ_qt5-qttools-libs-designer = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qttools-libs-designer = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common"
RPM_SONAME_PROV_qt5-qttools-libs-designercomponents = "libQt5DesignerComponents.so.5"
RPM_SONAME_REQ_qt5-qttools-libs-designercomponents = "libGL.so.1 libQt5Core.so.5 libQt5Designer.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qttools-libs-designercomponents = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common qt5-qttools-libs-designer"
RPM_SONAME_PROV_qt5-qttools-libs-help = "libQt5Help.so.5"
RPM_SONAME_REQ_qt5-qttools-libs-help = "libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qttools-libs-help = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qttools-common"
RDEPENDS_qt5-qttools-static = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qttools-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-assistant-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-designer-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-doctools-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-linguist-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qdbusviewer-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qttools-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qttools-common-5.12.5-1.el8.0.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qttools-devel-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qttools-examples-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qttools-libs-designer-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qttools-libs-designercomponents-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qttools-libs-help-5.12.5-1.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/qt5-qttools-static-5.12.5-1.el8.0.1.x86_64.rpm \
          "

SRC_URI[qt5-assistant.sha256sum] = "a6b6adeae3a927aab0b82df4990b5b09725cdc0536637087534b1a51e780e5df"
SRC_URI[qt5-designer.sha256sum] = "7d3c7f271dbcd9626ef1d10471c50388331f8a6ac742583e39bc95fc920c2bce"
SRC_URI[qt5-doctools.sha256sum] = "3ab25e97e3e8b421dcf315598a07d484ad7912bc99c17364225ec4625d9d192b"
SRC_URI[qt5-linguist.sha256sum] = "f33638703f05e0a1dbb19546923c97e515a685c72d25b5a5a69bd5a77240b36d"
SRC_URI[qt5-qdbusviewer.sha256sum] = "93ad4647ad86b4305986e5215c6fdd6f97b6541d1052427af5ffecc15b01b27b"
SRC_URI[qt5-qttools.sha256sum] = "685ddc10654ff90ee7fdee7ca9b447efc23a6f9e6ca0cb1804760e3f2c06871d"
SRC_URI[qt5-qttools-common.sha256sum] = "edaa0000c5ed9f5e19d25951eb7025cb84fd154bd2a4e4a93f347b1b3bbe89ba"
SRC_URI[qt5-qttools-devel.sha256sum] = "c862ae591939fee5d9c5c780c9423d914657be0a63d143aca2090062b2b244a2"
SRC_URI[qt5-qttools-examples.sha256sum] = "7d4ed2c2666753380c229c99d909c59488dc46bdd02c361fa8ebcacfd90755d3"
SRC_URI[qt5-qttools-libs-designer.sha256sum] = "75acde0b0e687b646d59f8c50ad689305bd9742a15e5b91daad4a7423623c527"
SRC_URI[qt5-qttools-libs-designercomponents.sha256sum] = "b0c37fdc903558effeac3f6e7eb78d5b6c40258cd9bca522950e325847717fc1"
SRC_URI[qt5-qttools-libs-help.sha256sum] = "552e2381171b7146fb96b9c550ed45e3c3752f29f0a3942044aceb1181b6bc9d"
SRC_URI[qt5-qttools-static.sha256sum] = "368e78e69c8c0134aa4017ccb16e0848235c564c28545ef190043e05e2a6c7dc"
