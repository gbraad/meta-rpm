SUMMARY = "generated recipe based on ucs-miscfixed-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/ucs-miscfixed-fonts-0.3-17.el8.noarch.rpm \
          "

SRC_URI[ucs-miscfixed-fonts.sha256sum] = "a7f997099d4d0c085e5e360b7e278062ea1fe35ee7504530a68ad10797f9d459"
