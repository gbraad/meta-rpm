SUMMARY = "generated recipe based on anaconda srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & MIT"
RPM_LICENSE = "GPLv2+ and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk audit-libs cairo gdk-pixbuf glade glib-2.0 gtk+3 libarchive libxklavier libxml2 pango pkgconfig-native platform-python3 rpm"
RDEPENDS_anaconda = "anaconda-core anaconda-gui anaconda-install-env-deps anaconda-tui bash"
RPM_SONAME_REQ_anaconda-core = "libaudit.so.1 libc.so.6 libpython3.6m.so.1.0"
RDEPENDS_anaconda-core = "NetworkManager NetworkManager-libnm NetworkManager-team anaconda-tui audit-libs bash chrony cracklib-dicts dhcp-client glibc glibc-langpack-en kbd langtable-data libreport-anaconda platform-python platform-python-coverage python3-blivet python3-blockdev python3-dbus python3-dnf python3-gobject-base python3-kickstart python3-langtable python3-libs python3-libselinux python3-meh python3-ntplib python3-ordered-set python3-pid python3-productmd python3-pwquality python3-pydbus python3-pyparted python3-pytz python3-requests python3-requests-file python3-requests-ftp python3-rpm python3-syspurpose python3-systemd subscription-manager systemd teamd usermode util-linux"
RPM_SONAME_REQ_anaconda-dracut = "libarchive.so.13 libc.so.6 librpm.so.8 librpmio.so.8"
RDEPENDS_anaconda-dracut = "bash dracut dracut-live dracut-network glibc libarchive platform-python python3-kickstart rpm-libs xz"
RDEPENDS_anaconda-gui = "NetworkManager-wifi adwaita-icon-theme anaconda-core anaconda-user-help anaconda-widgets centos-logos keybinder3 libgnomekbd libtimezonemap libxklavier nm-connection-editor platform-python python3-meh-gui tigervnc-server-minimal yelp zenity"
RDEPENDS_anaconda-install-env-deps = "createrepo_c fcoe-utils gdb isomd5sum kexec-tools libblockdev-plugins-all realmd rsync tmux udisks2-iscsi"
RDEPENDS_anaconda-tui = "anaconda-core platform-python python3-simpleline"
RPM_SONAME_PROV_anaconda-widgets = "libAnacondaWidgets.so.4"
RPM_SONAME_REQ_anaconda-widgets = "libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgladeui-2.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libxklavier.so.16 libxml2.so.2"
RDEPENDS_anaconda-widgets = "atk cairo cairo-gobject gdk-pixbuf2 glade-libs glib2 glibc gtk3 libxklavier libxml2 pango platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/anaconda-29.19.2.17-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/anaconda-core-29.19.2.17-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/anaconda-dracut-29.19.2.17-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/anaconda-gui-29.19.2.17-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/anaconda-install-env-deps-29.19.2.17-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/anaconda-tui-29.19.2.17-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/anaconda-widgets-29.19.2.17-1.el8.x86_64.rpm \
          "

SRC_URI[anaconda.sha256sum] = "18595bdeb81177d98cdcf0fe2a88bb3a7b65773194fdfe2ee0d2e90527956acd"
SRC_URI[anaconda-core.sha256sum] = "0b11c02e170dca65250b902fd930b7b84fb2bbe877ec62229ad76a74378d7c9e"
SRC_URI[anaconda-dracut.sha256sum] = "fa3fb07436dbfa40b528b33f5413510e3c4404782e313c9b5b72d564ac2362ce"
SRC_URI[anaconda-gui.sha256sum] = "b310f659923785c83efddaed59199d723d33a9172201f90e03b7a8bc1774731c"
SRC_URI[anaconda-install-env-deps.sha256sum] = "ae658ac9b85211b287c207132df88dca310ea49c4bc07d47e4fe4f918790863d"
SRC_URI[anaconda-tui.sha256sum] = "6a2bd710e4790718cd75ad4808da2e459903120aa60be45310d0b745913b74b4"
SRC_URI[anaconda-widgets.sha256sum] = "08d7f8628d9504d9480273dee27e22a445d6ae41124d433ba47d7444e796ea10"
