SUMMARY = "generated recipe based on libxslt srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcrypt libgpg-error libxml2 pkgconfig-native"
RPM_SONAME_PROV_libxslt = "libexslt.so.0 libxslt.so.1"
RPM_SONAME_REQ_libxslt = "libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0 libm.so.6 libxml2.so.2"
RDEPENDS_libxslt = "glibc libgcrypt libgpg-error libxml2"
RPM_SONAME_REQ_libxslt-devel = "libexslt.so.0 libxslt.so.1"
RPROVIDES_libxslt-devel = "libxslt-dev (= 1.1.32)"
RDEPENDS_libxslt-devel = "bash libgcrypt-devel libgpg-error-devel libxml2-devel libxslt pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxslt-devel-1.1.32-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libxslt-1.1.32-4.el8.x86_64.rpm \
          "

SRC_URI[libxslt.sha256sum] = "90df25088318ca476c498e47764881eae576c79c815494ef447671b0a0cec117"
SRC_URI[libxslt-devel.sha256sum] = "b35424fa90d2b7e92b0f60579c6dc483e4f54781738911326c052e24723bf5b0"
