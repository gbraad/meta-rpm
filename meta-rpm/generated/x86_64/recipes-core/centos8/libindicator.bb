SUMMARY = "generated recipe based on libindicator srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gdk-pixbuf glib-2.0 gtk+3 pkgconfig-native"
RPM_SONAME_PROV_libindicator-gtk3 = "libindicator3.so.7"
RPM_SONAME_REQ_libindicator-gtk3 = "libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpthread.so.0"
RDEPENDS_libindicator-gtk3 = "gdk-pixbuf2 glib2 glibc gtk3"
RPM_SONAME_REQ_libindicator-gtk3-devel = "libindicator3.so.7"
RPROVIDES_libindicator-gtk3-devel = "libindicator-gtk3-dev (= 12.10.1)"
RDEPENDS_libindicator-gtk3-devel = "gtk3-devel libindicator-gtk3 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libindicator-gtk3-12.10.1-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libindicator-gtk3-devel-12.10.1-14.el8.x86_64.rpm \
          "

SRC_URI[libindicator-gtk3.sha256sum] = "4943aaefbe693035b7feae5353919bea0bf66fac38ea388e0ee0b11adb95288b"
SRC_URI[libindicator-gtk3-devel.sha256sum] = "bf1d21092800cac087cf36a632c1f37683fee35dccabb88618130cd5a05a8a92"
