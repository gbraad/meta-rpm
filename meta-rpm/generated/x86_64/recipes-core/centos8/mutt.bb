SUMMARY = "generated recipe based on mutt srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & CLOSED"
RPM_LICENSE = "GPLv2+ and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cyrus-sasl-lib e2fsprogs gnutls-libs gpgme krb5-libs libidn2 ncurses pkgconfig-native tokyocabinet"
RPM_SONAME_REQ_mutt = "libc.so.6 libcom_err.so.2 libgnutls.so.30 libgpgme.so.11 libgssapi_krb5.so.2 libidn2.so.0 libk5crypto.so.3 libkrb5.so.3 libncursesw.so.6 libsasl2.so.3 libtinfo.so.6 libtokyocabinet.so.9"
RDEPENDS_mutt = "cyrus-sasl-lib glibc gnutls gpgme krb5-libs libcom_err libidn2 mailcap ncurses-libs perl-File-Temp perl-Time-Local perl-interpreter perl-libs tokyocabinet urlview"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mutt-1.10.1-2.el8.x86_64.rpm \
          "

SRC_URI[mutt.sha256sum] = "37557b77841b4b2ecc346f39b74639da43f7415c3c388f068d75d351d88fc6b1"
