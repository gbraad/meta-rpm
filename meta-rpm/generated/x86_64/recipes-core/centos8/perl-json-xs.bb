SUMMARY = "generated recipe based on perl-JSON-XS srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-JSON-XS = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-JSON-XS = "glibc perl-Encode perl-Exporter perl-Getopt-Long perl-Storable perl-Types-Serialiser perl-common-sense perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-JSON-XS-3.04-3.el8.x86_64.rpm \
          "

SRC_URI[perl-JSON-XS.sha256sum] = "7bd8bd90efdb853e76b8527b1163af06fdac69bfc7c1c441190f12ff6a308ceb"
