SUMMARY = "generated recipe based on openldap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "OpenLDAP"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cyrus-sasl-lib openssl pkgconfig-native"
RPM_SONAME_PROV_openldap = "liblber-2.4.so.2 libldap-2.4.so.2 libldap_r-2.4.so.2 libslapi-2.4.so.2"
RPM_SONAME_REQ_openldap = "libc.so.6 libcrypto.so.1.1 libpthread.so.0 libresolv.so.2 libsasl2.so.3 libssl.so.1.1"
RDEPENDS_openldap = "cyrus-sasl-lib glibc openssl-libs"
RPM_SONAME_REQ_openldap-clients = "libc.so.6 liblber-2.4.so.2 libldap-2.4.so.2 libsasl2.so.3"
RDEPENDS_openldap-clients = "cyrus-sasl-lib glibc openldap"
RPM_SONAME_REQ_openldap-devel = "liblber-2.4.so.2 libldap-2.4.so.2 libldap_r-2.4.so.2 libslapi-2.4.so.2"
RPROVIDES_openldap-devel = "openldap-dev (= 2.4.46)"
RDEPENDS_openldap-devel = "cyrus-sasl-devel openldap"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openldap-2.4.46-11.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openldap-clients-2.4.46-11.el8_1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openldap-devel-2.4.46-11.el8_1.x86_64.rpm \
          "

SRC_URI[openldap.sha256sum] = "2f6ce52d64a906f93a8a91f71c5a42f5ff945fc197d8e9a8b4a32d72ca75173b"
SRC_URI[openldap-clients.sha256sum] = "895b807897748073176ee6f4eff685faf8ebcdfdf369cd4e2f63db6e7f9beb19"
SRC_URI[openldap-devel.sha256sum] = "7bcf0c358335fb34fd446506be141dd791aae4bc0a1388e363fded1915453341"
