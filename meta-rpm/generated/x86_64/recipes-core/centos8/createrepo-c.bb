SUMMARY = "generated recipe based on createrepo_c srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 curl drpm expat file glib-2.0 libgcc libxml2 openssl pkgconfig-native platform-python3 rpm rpm-devel sqlite3 xz zlib"
RPM_SONAME_REQ_createrepo_c = "libbz2.so.1 libc.so.6 libcreaterepo_c.so.0 libcrypto.so.1.1 libcurl.so.4 libdrpm.so.0 libexpat.so.1 libgcc_s.so.1 libglib-2.0.so.0 libgthread-2.0.so.0 liblzma.so.5 libmagic.so.1 librpm.so.8 librpmio.so.8 libsqlite3.so.0 libssl.so.1.1 libxml2.so.2 libz.so.1"
RDEPENDS_createrepo_c = "bzip2-libs createrepo_c-libs drpm expat file-libs glib2 glibc libcurl libgcc libxml2 openssl-libs rpm rpm-libs sqlite-libs xz-libs zlib"
RPM_SONAME_REQ_createrepo_c-devel = "libcreaterepo_c.so.0"
RPROVIDES_createrepo_c-devel = "createrepo_c-dev (= 0.15.1)"
RDEPENDS_createrepo_c-devel = "createrepo_c-libs glib2-devel libcurl-devel libxml2-devel pkgconf-pkg-config rpm-devel sqlite-devel zlib-devel"
RPM_SONAME_PROV_createrepo_c-libs = "libcreaterepo_c.so.0"
RPM_SONAME_REQ_createrepo_c-libs = "libbz2.so.1 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdrpm.so.0 libexpat.so.1 libgcc_s.so.1 libglib-2.0.so.0 liblzma.so.5 libmagic.so.1 librpm.so.8 librpmio.so.8 libsqlite3.so.0 libssl.so.1.1 libxml2.so.2 libz.so.1"
RDEPENDS_createrepo_c-libs = "bzip2-libs drpm expat file-libs glib2 glibc libcurl libgcc libxml2 openssl-libs rpm-libs sqlite-libs xz-libs zlib"
RPM_SONAME_REQ_python3-createrepo_c = "libbz2.so.1 libc.so.6 libcreaterepo_c.so.0 libcrypto.so.1.1 libcurl.so.4 libdrpm.so.0 libexpat.so.1 libglib-2.0.so.0 liblzma.so.5 libmagic.so.1 libpython3.6m.so.1.0 librpm.so.8 librpmio.so.8 libsqlite3.so.0 libssl.so.1.1 libxml2.so.2 libz.so.1"
RDEPENDS_python3-createrepo_c = "bzip2-libs createrepo_c-libs drpm expat file-libs glib2 glibc libcurl libxml2 openssl-libs platform-python python3-libs rpm-libs sqlite-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/createrepo_c-0.15.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/createrepo_c-devel-0.15.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/createrepo_c-libs-0.15.1-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-createrepo_c-0.15.1-2.el8.x86_64.rpm \
          "

SRC_URI[createrepo_c.sha256sum] = "63a9c69b7467ec2cedad03f85d2553e3a06f8f25af225a85b65b2cf2be9384c8"
SRC_URI[createrepo_c-devel.sha256sum] = "532bace4dd2729be2ea018de89543df08a5e74283d09d3454a7fc2be9d654299"
SRC_URI[createrepo_c-libs.sha256sum] = "0913053a78c2937049be619d33e90fe82652570c94f4b20cfb78f40c84341885"
SRC_URI[python3-createrepo_c.sha256sum] = "cbbef8c6ba85081f1b7cb7d40e6ef7cd2c2569b4395d878d25230630af1d1a4d"
