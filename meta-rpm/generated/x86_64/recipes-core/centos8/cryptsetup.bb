SUMMARY = "generated recipe based on cryptsetup srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cryptsetup-libs libblkid libpwquality libuuid pkgconfig-native popt"
RPM_SONAME_REQ_cryptsetup = "libblkid.so.1 libc.so.6 libcryptsetup.so.12 libm.so.6 libpopt.so.0 libpthread.so.0 libpwquality.so.1 libuuid.so.1"
RDEPENDS_cryptsetup = "cryptsetup-libs glibc libblkid libpwquality libuuid popt"
RPM_SONAME_REQ_cryptsetup-devel = "libcryptsetup.so.12"
RPROVIDES_cryptsetup-devel = "cryptsetup-dev (= 2.2.2)"
RDEPENDS_cryptsetup-devel = "cryptsetup cryptsetup-libs pkgconf-pkg-config"
RPM_SONAME_REQ_cryptsetup-reencrypt = "libblkid.so.1 libc.so.6 libcryptsetup.so.12 libm.so.6 libpopt.so.0 libpthread.so.0 libpwquality.so.1 libuuid.so.1"
RDEPENDS_cryptsetup-reencrypt = "cryptsetup-libs glibc libblkid libpwquality libuuid popt"
RPM_SONAME_REQ_integritysetup = "libblkid.so.1 libc.so.6 libcryptsetup.so.12 libm.so.6 libpopt.so.0 libpthread.so.0 libuuid.so.1"
RDEPENDS_integritysetup = "cryptsetup-libs glibc libblkid libuuid popt"
RPM_SONAME_REQ_veritysetup = "libblkid.so.1 libc.so.6 libcryptsetup.so.12 libm.so.6 libpopt.so.0 libpthread.so.0"
RDEPENDS_veritysetup = "cryptsetup-libs glibc libblkid popt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cryptsetup-devel-2.2.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cryptsetup-2.2.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cryptsetup-reencrypt-2.2.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/integritysetup-2.2.2-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/veritysetup-2.2.2-1.el8.x86_64.rpm \
          "

SRC_URI[cryptsetup.sha256sum] = "8071e10e876ad0e00298eba4802bba8a4b505999dc7e6c1ba2ab4171217c5915"
SRC_URI[cryptsetup-devel.sha256sum] = "a773227310eebc8503e0e949d905fd78fe1fd37bee166e40033624aeace207df"
SRC_URI[cryptsetup-reencrypt.sha256sum] = "4416c7e843daf76f0bcece32a88c88c03f902af023054c72eb96af8fc0c3ae9c"
SRC_URI[integritysetup.sha256sum] = "f68b0c9e5bb45e3b72bb40b6e8a39cf80aaf53d2d47a28ab40b6f9ab090705d0"
SRC_URI[veritysetup.sha256sum] = "cb1c66dbc86ec29219679d98d6673dd12069ef6d114694c5d527f41bea693e78"
