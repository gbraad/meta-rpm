SUMMARY = "generated recipe based on perl-DateTime-Format-Mail srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-Format-Mail = "perl-Carp perl-DateTime perl-Params-Validate perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-DateTime-Format-Mail-0.403-6.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-Format-Mail.sha256sum] = "6a49fd169ab0673f9ef0b2d9a61adfdabebb8f6bcebccd04e443f880325b9cad"
