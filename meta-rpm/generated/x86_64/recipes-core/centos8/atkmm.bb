SUMMARY = "generated recipe based on atkmm srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk glib-2.0 glibmm24 libgcc libsigc++20 pkgconfig-native"
RPM_SONAME_PROV_atkmm = "libatkmm-1.6.so.1"
RPM_SONAME_REQ_atkmm = "libatk-1.0.so.0 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libglibmm-2.4.so.1 libgobject-2.0.so.0 libm.so.6 libsigc-2.0.so.0 libstdc++.so.6"
RDEPENDS_atkmm = "atk glib2 glibc glibmm24 libgcc libsigc++20 libstdc++"
RPM_SONAME_REQ_atkmm-devel = "libatkmm-1.6.so.1"
RPROVIDES_atkmm-devel = "atkmm-dev (= 2.24.2)"
RDEPENDS_atkmm-devel = "atk-devel atkmm glibmm24-devel pkgconf-pkg-config"
RDEPENDS_atkmm-doc = "atkmm glibmm24-doc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/atkmm-2.24.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/atkmm-devel-2.24.2-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/atkmm-doc-2.24.2-6.el8.noarch.rpm \
          "

SRC_URI[atkmm.sha256sum] = "6a7886f54b5551e7676676c9754b4945499ef414c8af1aa73b9b547547dc826e"
SRC_URI[atkmm-devel.sha256sum] = "df175f6441a07185240e2868a24d3817606a822297cede322428d6c3e726c0ca"
SRC_URI[atkmm-doc.sha256sum] = "b5d885e85fc899b5cf4ca1eddea143afeb6db471bbaf3360d68abcc9d4005437"
