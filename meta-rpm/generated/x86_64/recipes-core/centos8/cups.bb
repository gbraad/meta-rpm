SUMMARY = "generated recipe based on cups srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0 & CLOSED"
RPM_LICENSE = "GPLv2+ and LGPLv2 with exceptions and AML"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl avahi-libs cups-libs dbus-libs e2fsprogs gnutls-libs krb5-libs libgcc libusb1 libxcrypt pam pkgconfig-native systemd-libs zlib"
RPM_SONAME_REQ_cups = "libacl.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupscgi.so.1 libcupsimage.so.2 libcupsmime.so.1 libcupsppdc.so.1 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpam.so.0 libpthread.so.0 libstdc++.so.6 libsystemd.so.0 libusb-1.0.so.0 libz.so.1"
RDEPENDS_cups = "acl avahi-libs bash cups-client cups-filesystem cups-filters cups-libs dbus dbus-libs glibc gnutls grep krb5-libs libacl libcom_err libgcc libstdc++ libusbx libxcrypt pam sed systemd systemd-libs zlib"
RPM_SONAME_REQ_cups-client = "libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libgnutls.so.30 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_cups-client = "avahi-libs bash chkconfig cups-libs glibc gnutls krb5-libs libcom_err libxcrypt zlib"
RPM_SONAME_REQ_cups-ipptool = "libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libgnutls.so.30 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_cups-ipptool = "avahi-libs cups-libs glibc gnutls krb5-libs libcom_err libxcrypt zlib"
RPM_SONAME_REQ_cups-lpd = "libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libgnutls.so.30 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_cups-lpd = "avahi-libs bash cups cups-libs glibc gnutls krb5-libs libcom_err libxcrypt zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cups-2.2.6-33.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cups-client-2.2.6-33.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cups-filesystem-2.2.6-33.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cups-ipptool-2.2.6-33.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cups-lpd-2.2.6-33.el8.x86_64.rpm \
          "

SRC_URI[cups.sha256sum] = "88a1545a423d119553c9f84a333028a7c94e2edca0d45a319d43540e8c14998d"
SRC_URI[cups-client.sha256sum] = "20b64040003d1daa25e946028778fa132a5ec5eec09895b0922fb0bc17ec85d3"
SRC_URI[cups-filesystem.sha256sum] = "d3de38fb431b6e3525bb5314983f6df75a9109785ee7895a5377cbcb900e6a2a"
SRC_URI[cups-ipptool.sha256sum] = "bf73afda8aa0bf9afe649c44fda310c461d59e1d71d7b7f57cb8d26878014710"
SRC_URI[cups-lpd.sha256sum] = "699ba5b0c94505de59fca80405b984ce5321ecee21efe960e2f6674bd93d3961"
