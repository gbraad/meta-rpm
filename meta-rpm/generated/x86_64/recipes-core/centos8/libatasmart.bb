SUMMARY = "generated recipe based on libatasmart srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native systemd-libs"
RPM_SONAME_PROV_libatasmart = "libatasmart.so.4"
RPM_SONAME_REQ_libatasmart = "libc.so.6 libudev.so.1"
RDEPENDS_libatasmart = "glibc systemd-libs"
RPM_SONAME_REQ_libatasmart-devel = "libatasmart.so.4"
RPROVIDES_libatasmart-devel = "libatasmart-dev (= 0.19)"
RDEPENDS_libatasmart-devel = "libatasmart pkgconf-pkg-config vala"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libatasmart-0.19-14.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libatasmart-devel-0.19-14.el8.x86_64.rpm \
          "

SRC_URI[libatasmart.sha256sum] = "e1133a81512bfba8d4bf06bc12aafee7fe13d6319fc8690b81e6f46d978930eb"
SRC_URI[libatasmart-devel.sha256sum] = "fa20a17cdcf015d0c809702c9e278ec747f578a19092e6a26f44041aa1c4a6e6"
