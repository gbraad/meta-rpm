SUMMARY = "generated recipe based on parted srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "device-mapper-libs libblkid libselinux libsepol libuuid ncurses pkgconfig-native readline"
RPM_SONAME_PROV_parted = "libparted-fs-resize.so.0 libparted.so.2"
RPM_SONAME_REQ_parted = "libblkid.so.1 libc.so.6 libdevmapper.so.1.02 libdl.so.2 libreadline.so.7 libselinux.so.1 libsepol.so.1 libtinfo.so.6 libuuid.so.1"
RDEPENDS_parted = "bash device-mapper-libs glibc info libblkid libselinux libsepol libuuid ncurses-libs readline"
RPM_SONAME_REQ_parted-devel = "libparted-fs-resize.so.0 libparted.so.2"
RPROVIDES_parted-devel = "parted-dev (= 3.2)"
RDEPENDS_parted-devel = "parted pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/parted-3.2-38.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/parted-devel-3.2-38.el8.x86_64.rpm \
          "

SRC_URI[parted.sha256sum] = "4c5c7f3773c634c054b0a5fc1b40d0a8448b44bb5aff410bfa88facf9e2059ff"
SRC_URI[parted-devel.sha256sum] = "935cc7a4e94b09e5ca88a93c621bb24b2f82f96c30f09ed952ee6021a2d6e101"
