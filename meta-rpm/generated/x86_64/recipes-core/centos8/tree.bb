SUMMARY = "generated recipe based on tree srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_tree = "libc.so.6"
RDEPENDS_tree = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/tree-1.7.0-15.el8.x86_64.rpm \
          "

SRC_URI[tree.sha256sum] = "b5969c16b012fc777922ca87069d03f3f9e12486498e8beb969f4aeb9f51e12b"
