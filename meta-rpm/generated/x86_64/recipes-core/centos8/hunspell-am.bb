SUMMARY = "generated recipe based on hunspell-am srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-am = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-am-0.20090704-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-am.sha256sum] = "c24c1637bc6b94148dffbe7363c670cdd3ab202fa789a85730af5c4a60b311cc"
