SUMMARY = "generated recipe based on docbook5-schemas srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Freely redistributable without restriction"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_docbook5-schemas = "bash libxml2 perl-interpreter perl-libs xml-common"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/docbook5-schemas-5.0-17.el8.noarch.rpm \
          "

SRC_URI[docbook5-schemas.sha256sum] = "1d09e96991aebd1042013d51497283356eb6e4d75754c2056e0ecadd7487e58a"
