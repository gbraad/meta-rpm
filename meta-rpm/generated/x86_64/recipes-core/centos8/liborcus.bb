SUMMARY = "generated recipe based on liborcus srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-2.0"
RPM_LICENSE = "MPLv2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "boost libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_liborcus = "liborcus-0.13.so.0 liborcus-mso-0.13.so.0 liborcus-parser-0.13.so.0"
RPM_SONAME_REQ_liborcus = "libboost_filesystem.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_liborcus = "boost-filesystem boost-iostreams boost-system glibc libgcc libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liborcus-0.13.4-2.el8.x86_64.rpm \
          "

SRC_URI[liborcus.sha256sum] = "8b7a3ca661850b976caa4b8fd89f226bcc7ccad58eb41009ef84cb4b23389ac6"
