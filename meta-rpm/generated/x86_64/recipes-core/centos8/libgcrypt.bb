SUMMARY = "generated recipe based on libgcrypt srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "LGPLv2+ and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgpg-error pkgconfig-native"
RPM_SONAME_PROV_libgcrypt = "libgcrypt.so.20"
RPM_SONAME_REQ_libgcrypt = "libc.so.6 libdl.so.2 libgpg-error.so.0"
RDEPENDS_libgcrypt = "glibc libgpg-error"
RPM_SONAME_REQ_libgcrypt-devel = "libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0"
RPROVIDES_libgcrypt-devel = "libgcrypt-dev (= 1.8.3)"
RDEPENDS_libgcrypt-devel = "bash glibc info libgcrypt libgpg-error libgpg-error-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgcrypt-1.8.3-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libgcrypt-devel-1.8.3-4.el8.x86_64.rpm \
          "

SRC_URI[libgcrypt.sha256sum] = "fb886c8806902ca77fd5c37e447282ec2394b50903b41fac2a178d326652a9b1"
SRC_URI[libgcrypt-devel.sha256sum] = "b8d309e1492eea5ae4f40e9d9c0fbcb6188e1f8908614b39f2bbe9316c7f17a3"
