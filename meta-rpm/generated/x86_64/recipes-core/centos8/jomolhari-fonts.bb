SUMMARY = "generated recipe based on jomolhari-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jomolhari-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/jomolhari-fonts-0.003-24.el8.noarch.rpm \
          "

SRC_URI[jomolhari-fonts.sha256sum] = "513be5f20a0ee9a3dfedf7cdfec47369078f269006149f255ae9d6fbc9fc649a"
