SUMMARY = "generated recipe based on protobuf srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_protobuf = "libprotobuf.so.15"
RPM_SONAME_REQ_protobuf = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_protobuf = "glibc libgcc libstdc++ zlib"
RPM_SONAME_PROV_protobuf-compiler = "libprotoc.so.15"
RPM_SONAME_REQ_protobuf-compiler = "libc.so.6 libgcc_s.so.1 libm.so.6 libprotobuf.so.15 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_protobuf-compiler = "glibc libgcc libstdc++ protobuf zlib"
RPM_SONAME_REQ_protobuf-devel = "libprotobuf.so.15 libprotoc.so.15"
RPROVIDES_protobuf-devel = "protobuf-dev (= 3.5.0)"
RDEPENDS_protobuf-devel = "pkgconf-pkg-config protobuf protobuf-compiler zlib-devel"
RPM_SONAME_PROV_protobuf-lite = "libprotobuf-lite.so.15"
RPM_SONAME_REQ_protobuf-lite = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_protobuf-lite = "glibc libgcc libstdc++ zlib"
RDEPENDS_python3-protobuf = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/protobuf-3.5.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/protobuf-lite-3.5.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-protobuf-3.5.0-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/protobuf-compiler-3.5.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/protobuf-devel-3.5.0-7.el8.x86_64.rpm \
          "

SRC_URI[protobuf.sha256sum] = "8ec633b452edd527d7eed77b5ea9335a3813b2651bf41cc06f60a7d719c058f6"
SRC_URI[protobuf-compiler.sha256sum] = "5a3f05a294df363c40ffe47ca847d51e2e4bf89d34f098db30903c13600a1caf"
SRC_URI[protobuf-devel.sha256sum] = "1efb0feb8cf2fb0c32ff90426fb0e78e039914742c8b3a07ac1d6094f9fefac1"
SRC_URI[protobuf-lite.sha256sum] = "f7f7debfe7f39f0184beae0e5f26a3dbf5e0071cdb3e64d8fc3feee62e051149"
SRC_URI[python3-protobuf.sha256sum] = "cec600b0e7b474e1d60501a011603ff9dfb3ab8b98b4f369fb578e81698a7560"
