SUMMARY = "generated recipe based on asciidoc srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0"
RPM_LICENSE = "GPL+ and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_asciidoc = "docbook-style-xsl graphviz libxslt platform-python source-highlight vim-filesystem"
RDEPENDS_asciidoc-doc = "asciidoc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/asciidoc-8.6.10-0.5.20180627gitf7c2274.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/asciidoc-doc-8.6.10-0.5.20180627gitf7c2274.el8.noarch.rpm \
          "

SRC_URI[asciidoc.sha256sum] = "f83bd76a75e8bc36f23388d609b4f45c1fbad711ff246206d5a9c6bfe78c3fe2"
SRC_URI[asciidoc-doc.sha256sum] = "b7b4f02a70b484ff107da86baa0ca690577ce2e78ddb84899335d38beb7df69d"
