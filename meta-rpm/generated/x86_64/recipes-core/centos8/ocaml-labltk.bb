SUMMARY = "generated recipe based on ocaml-labltk srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+ with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 pkgconfig-native tcl tk"
RPM_SONAME_REQ_ocaml-labltk = "libX11.so.6 libc.so.6 libtcl8.6.so libtk8.6.so"
RDEPENDS_ocaml-labltk = "glibc libX11 ocaml-runtime tcl tk"
RPROVIDES_ocaml-labltk-devel = "ocaml-labltk-dev (= 8.06.4)"
RDEPENDS_ocaml-labltk-devel = "bash ocaml-labltk ocaml-runtime"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-labltk-8.06.4-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ocaml-labltk-devel-8.06.4-7.el8.x86_64.rpm \
          "

SRC_URI[ocaml-labltk.sha256sum] = "63d1b2c7d225f22342fbc3c9b80091edf28d5f244fc9311a4c6fb755baa30ab3"
SRC_URI[ocaml-labltk-devel.sha256sum] = "8032ed8df8a69f9f6a78bc5063cf7976611acebbd26959e44f1c247669d9d812"
