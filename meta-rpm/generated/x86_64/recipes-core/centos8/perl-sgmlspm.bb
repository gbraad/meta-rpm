SUMMARY = "generated recipe based on perl-SGMLSpm srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-SGMLSpm = "openjade perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-SGMLSpm-1.03ii-42.el8.noarch.rpm \
          "

SRC_URI[perl-SGMLSpm.sha256sum] = "cb805d5dcc47f666c22fd57c3a1af0c2c6d3bbd17e7907b469b1baa3636427d4"
