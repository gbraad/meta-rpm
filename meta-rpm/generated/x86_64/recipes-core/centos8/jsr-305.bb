SUMMARY = "generated recipe based on jsr-305 srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & CC-BY-1.0 "
RPM_LICENSE = "BSD and CC-BY"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jsr-305 = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jsr-305-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jsr-305-0-0.22.20130910svn.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jsr-305-javadoc-0-0.22.20130910svn.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jsr-305.sha256sum] = "2eb88aa980050304c6b76d93df2eccee605ead9c6358c8d450e2724e60a79d59"
SRC_URI[jsr-305-javadoc.sha256sum] = "37fbcab1a21d1d68bbbaa80c2fd4fb0a17393b3f699778365871960472c7ebc8"
