SUMMARY = "generated recipe based on tex-fonts-hebrew srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LPPL-1.0"
RPM_LICENSE = "GPL+ and LPPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_tex-fonts-hebrew = "bash culmus-aharoni-clm-fonts culmus-caladings-clm-fonts culmus-david-clm-fonts culmus-drugulin-clm-fonts culmus-ellinia-clm-fonts culmus-frank-ruehl-clm-fonts culmus-hadasim-clm-fonts culmus-miriam-clm-fonts culmus-miriam-mono-clm-fonts culmus-nachlieli-clm-fonts culmus-simple-clm-fonts culmus-stamashkenaz-clm-fonts culmus-stamsefarad-clm-fonts culmus-yehuda-clm-fonts texlive-base texlive-kpathsea texlive-tetex texlive-texconfig"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tex-fonts-hebrew-0.1-28.el8.noarch.rpm \
          "

SRC_URI[tex-fonts-hebrew.sha256sum] = "0dd78321a42f1d5e80db61489559ed967c41ebb6415555b227934bc22e89be88"
