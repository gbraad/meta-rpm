SUMMARY = "generated recipe based on mod_security_crs srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mod_security_crs = "mod_security"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/mod_security_crs-3.0.0-5.el8.noarch.rpm \
          "

SRC_URI[mod_security_crs.sha256sum] = "786640bb3fb6cf7ca7311ddb5de030edbda7e3a069a5744303399a2f4224a82d"
