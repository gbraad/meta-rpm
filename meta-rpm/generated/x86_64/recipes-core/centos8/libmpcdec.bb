SUMMARY = "generated recipe based on libmpcdec srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libmpcdec = "libmpcdec.so.5"
RPM_SONAME_REQ_libmpcdec = "libc.so.6"
RDEPENDS_libmpcdec = "glibc"
RPM_SONAME_REQ_libmpcdec-devel = "libmpcdec.so.5"
RPROVIDES_libmpcdec-devel = "libmpcdec-dev (= 1.2.6)"
RDEPENDS_libmpcdec-devel = "libmpcdec"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libmpcdec-1.2.6-20.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libmpcdec-devel-1.2.6-20.el8.x86_64.rpm \
          "

SRC_URI[libmpcdec.sha256sum] = "a7d59ce8a7319cb67aea5a63ab40d3381809fc0830ab1ec71c2a92e32757405e"
SRC_URI[libmpcdec-devel.sha256sum] = "4511433f55f9ffba5608a8ffe51054de6158a5c7a37f81c45c10e806275ccb48"
