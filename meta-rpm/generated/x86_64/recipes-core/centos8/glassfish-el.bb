SUMMARY = "generated recipe based on glassfish-el srpm"
DESCRIPTION = "Description"
LICENSE = "CDDL-1.1 | GPL-2.0"
RPM_LICENSE = "CDDL-1.1 or GPLv2 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_glassfish-el = "glassfish-el-api java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_glassfish-el-api = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_glassfish-el-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glassfish-el-3.0.1-0.7.b08.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glassfish-el-api-3.0.1-0.7.b08.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/glassfish-el-javadoc-3.0.1-0.7.b08.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[glassfish-el.sha256sum] = "ad5f334fc6ebee7caf0416ee9aa6523d833a9146953bbe72d4d9aee9458fdfbe"
SRC_URI[glassfish-el-api.sha256sum] = "e769fc592f663eb5ff7b705eadee7c175cad044921b8023c2e8852b2590e55b6"
SRC_URI[glassfish-el-javadoc.sha256sum] = "0069a627f50953a80c45095ae3afc61d45e8e5f0acc2717b68e4fd0a5686720f"
