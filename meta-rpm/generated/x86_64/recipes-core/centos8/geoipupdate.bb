SUMMARY = "generated recipe based on geoipupdate srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "curl pkgconfig-native zlib"
RPM_SONAME_REQ_geoipupdate = "libc.so.6 libcurl.so.4 libz.so.1"
RDEPENDS_geoipupdate = "glibc libcurl zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/geoipupdate-2.5.0-2.el8.x86_64.rpm \
          "

SRC_URI[geoipupdate.sha256sum] = "ee63f0811fbfd09a5a2f4fb09bbfd0680dd19eeac5ea7516ad6fba06d511cd8f"
