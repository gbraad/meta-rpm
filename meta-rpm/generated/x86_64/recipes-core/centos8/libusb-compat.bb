SUMMARY = "generated recipe based on libusb srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libusb1 pkgconfig-native"
RPM_SONAME_PROV_libusb = "libusb-0.1.so.4"
RPM_SONAME_REQ_libusb = "libc.so.6 libusb-1.0.so.0"
RDEPENDS_libusb = "glibc libusbx"
RPM_SONAME_REQ_libusb-devel = "libusb-0.1.so.4"
RPROVIDES_libusb-devel = "libusb-dev (= 0.1.5)"
RDEPENDS_libusb-devel = "bash libusb libusbx-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libusb-0.1.5-12.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libusb-devel-0.1.5-12.el8.x86_64.rpm \
          "

SRC_URI[libusb.sha256sum] = "aa8d1f77e2afea5018f4df761a5cffe7ab70e341e51cbf4c57ea53233b081ffc"
SRC_URI[libusb-devel.sha256sum] = "7671ab09c92c2c79ea85578ea0854f9506e74218d02bf78b3ec954c74a665ef1"
