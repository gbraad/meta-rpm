SUMMARY = "generated recipe based on perl-IPC-System-Simple srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IPC-System-Simple = "perl-Carp perl-Exporter perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-IPC-System-Simple-1.25-17.el8.noarch.rpm \
          "

SRC_URI[perl-IPC-System-Simple.sha256sum] = "3bb97849ad44d6889629ae8aa672479f1cbc4472030d26d4f532c1faeadd8063"
