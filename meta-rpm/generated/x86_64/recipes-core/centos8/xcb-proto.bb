SUMMARY = "generated recipe based on xcb-proto srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_xcb-proto = "pkgconf-pkg-config platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xcb-proto-1.13-4.el8.noarch.rpm \
          "

SRC_URI[xcb-proto.sha256sum] = "07270a71e3c3195f44b6f82e9b2f02ac9788d70dfa9aeb9188d221fb8aabcfee"
