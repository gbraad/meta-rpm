SUMMARY = "generated recipe based on tk srpm"
DESCRIPTION = "Description"
LICENSE = "tcl"
RPM_LICENSE = "TCL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "fontconfig freetype libx11 libxft pkgconfig-native tcl zlib"
RPM_SONAME_PROV_tk = "libtk8.6.so"
RPM_SONAME_REQ_tk = "libX11.so.6 libXft.so.2 libc.so.6 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libm.so.6 libpthread.so.0 libtcl8.6.so libz.so.1"
RDEPENDS_tk = "bash fontconfig freetype glibc libX11 libXft tcl zlib"
RPROVIDES_tk-devel = "tk-dev (= 8.6.8)"
RDEPENDS_tk-devel = "libX11-devel libXft-devel pkgconf-pkg-config tcl-devel tk"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tk-8.6.8-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tk-devel-8.6.8-1.el8.x86_64.rpm \
          "

SRC_URI[tk.sha256sum] = "41e7e8341bed3479582cc4c36a46a715802b69ca404eb5ace57efd072f68532d"
SRC_URI[tk-devel.sha256sum] = "f61ef7e75593978afce3f8b3e1c3a56430bb2148bb8c595fd69e682e5434585c"
