SUMMARY = "generated recipe based on OpenIPMI srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0 | BSD"
RPM_LICENSE = "LGPLv2+ and GPLv2+ or BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gdbm glib-2.0 ncurses net-snmp openssl pkgconfig-native popt readline"
RPM_SONAME_REQ_OpenIPMI = "libOpenIPMI.so.0 libOpenIPMIcmdlang.so.0 libOpenIPMIglib.so.0 libOpenIPMIposix.so.0 libOpenIPMIui.so.1 libOpenIPMIutils.so.0 libc.so.6 libgdbm.so.6 libglib-2.0.so.0 libgthread-2.0.so.0 libncurses.so.6 libnetsnmp.so.35 libpthread.so.0 libreadline.so.7"
RDEPENDS_OpenIPMI = "OpenIPMI-libs bash gdbm-libs glib2 glibc ncurses-libs net-snmp-libs readline systemd"
RPM_SONAME_REQ_OpenIPMI-devel = "libIPMIlanserv.so.0 libOpenIPMI.so.0 libOpenIPMIcmdlang.so.0 libOpenIPMIglib.so.0 libOpenIPMIposix.so.0 libOpenIPMIpthread.so.0 libOpenIPMIui.so.1 libOpenIPMIutils.so.0"
RPROVIDES_OpenIPMI-devel = "OpenIPMI-dev (= 2.0.27)"
RDEPENDS_OpenIPMI-devel = "OpenIPMI OpenIPMI-lanserv OpenIPMI-libs ncurses-devel pkgconf-pkg-config"
RPM_SONAME_PROV_OpenIPMI-lanserv = "libIPMIlanserv.so.0"
RPM_SONAME_REQ_OpenIPMI-lanserv = "libOpenIPMIposix.so.0 libOpenIPMIutils.so.0 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpopt.so.0 libpthread.so.0"
RDEPENDS_OpenIPMI-lanserv = "OpenIPMI OpenIPMI-libs glibc openssl-libs popt"
RPM_SONAME_PROV_OpenIPMI-libs = "libOpenIPMI.so.0 libOpenIPMIcmdlang.so.0 libOpenIPMIglib.so.0 libOpenIPMIposix.so.0 libOpenIPMIpthread.so.0 libOpenIPMIui.so.1 libOpenIPMIutils.so.0"
RPM_SONAME_REQ_OpenIPMI-libs = "libc.so.6 libcrypto.so.1.1 libgdbm.so.6 libglib-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libpthread.so.0"
RDEPENDS_OpenIPMI-libs = "gdbm-libs glib2 glibc openssl-libs"
RPM_SONAME_PROV_OpenIPMI-perl = "libOpenIPMI.so.0"
RPM_SONAME_REQ_OpenIPMI-perl = "libc.so.6"
RDEPENDS_OpenIPMI-perl = "OpenIPMI-libs glibc perl-Exporter perl-interpreter perl-libs"
RPM_SONAME_REQ_python3-openipmi = "libOpenIPMI.so.0 libOpenIPMIcmdlang.so.0 libOpenIPMIglib.so.0 libOpenIPMIpthread.so.0 libOpenIPMIutils.so.0 libc.so.6 libpthread.so.0"
RDEPENDS_python3-openipmi = "OpenIPMI-libs glibc platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/OpenIPMI-2.0.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/OpenIPMI-lanserv-2.0.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/OpenIPMI-libs-2.0.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/OpenIPMI-perl-2.0.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-openipmi-2.0.27-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/OpenIPMI-devel-2.0.27-1.el8.x86_64.rpm \
          "

SRC_URI[OpenIPMI.sha256sum] = "e3e00cf2a987b8135ddb362eed8e027e2385336e7eeaed1a42db725c0be838ad"
SRC_URI[OpenIPMI-devel.sha256sum] = "48ca5aa16feeca54c0e5fe84345319ebbe70c079c03ee212c99aa9b626b3592b"
SRC_URI[OpenIPMI-lanserv.sha256sum] = "1168014176d5623583e79cbd2566bfb63c6c5875d5ccd3be6c1b0c6c6100a9f3"
SRC_URI[OpenIPMI-libs.sha256sum] = "ed0ee70de8d733d2d7823f71d4d70dd13f9dc44abfaa903ce24cde60aba5d462"
SRC_URI[OpenIPMI-perl.sha256sum] = "2ee31a1dc936e5f623537c8cbaa77bd52eac0fc3c61e6f054eb09c56f748ce42"
SRC_URI[python3-openipmi.sha256sum] = "04fb1984e4a373753f64ff4c882aaa194ee006a132868c4a855f62b70f675d4e"
