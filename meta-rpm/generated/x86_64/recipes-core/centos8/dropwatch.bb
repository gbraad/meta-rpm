SUMMARY = "generated recipe based on dropwatch srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libnl pkgconfig-native readline zlib"
RPM_SONAME_REQ_dropwatch = "libc.so.6 libdl.so.2 libnl-3.so.200 libnl-genl-3.so.200 libreadline.so.7 libz.so.1"
RDEPENDS_dropwatch = "glibc libnl3 readline zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dropwatch-1.5-1.el8.x86_64.rpm \
          "

SRC_URI[dropwatch.sha256sum] = "d3099aa8c7dd8503a6140f448bdad70a153ac3e84c43abaa7aab2f01a3acf06b"
