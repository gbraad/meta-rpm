SUMMARY = "generated recipe based on perl-IO-Socket-SSL srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & MPL-2.0"
RPM_LICENSE = "(GPL+ or Artistic) and MPLv2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-Socket-SSL = "openssl perl-Carp perl-Errno perl-Exporter perl-HTTP-Tiny perl-IO perl-IO-Socket-IP perl-Net-SSLeay perl-Socket perl-URI perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-IO-Socket-SSL-2.066-4.el8.noarch.rpm \
          "

SRC_URI[perl-IO-Socket-SSL.sha256sum] = "2fa90d5bd159498310c25c82ca7b58c9fc7c6d6dbd6b13f3e5cc8e7570e2821b"
