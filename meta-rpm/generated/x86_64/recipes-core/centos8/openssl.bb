SUMMARY = "generated recipe based on openssl srpm"
DESCRIPTION = "Description"
LICENSE = "OpenSSL"
RPM_LICENSE = "OpenSSL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "krb5-libs pkgconfig-native zlib"
RPM_SONAME_REQ_openssl = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_openssl = "bash coreutils glibc openssl-libs zlib"
RPM_SONAME_REQ_openssl-devel = "libcrypto.so.1.1 libssl.so.1.1"
RPROVIDES_openssl-devel = "openssl-dev (= 1.1.1c)"
RDEPENDS_openssl-devel = "krb5-devel openssl-libs pkgconf-pkg-config zlib-devel"
RPM_SONAME_PROV_openssl-libs = "libcrypto.so.1.1 libssl.so.1.1"
RPM_SONAME_REQ_openssl-libs = "libc.so.6 libdl.so.2 libpthread.so.0 libz.so.1"
RPROVIDES_openssl-libs = "libssl (= 1.1.1c)"
RDEPENDS_openssl-libs = "ca-certificates crypto-policies glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssl-1.1.1c-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssl-devel-1.1.1c-15.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssl-libs-1.1.1c-15.el8.x86_64.rpm \
          "

SRC_URI[openssl.sha256sum] = "0ea161cbcbbf5db609bb4d7524750c0a6e57dc279282aa021aabf63a7cc63e79"
SRC_URI[openssl-devel.sha256sum] = "7c1cdec5e46fb029dc086cc88d6467674acaf8f1b7fb70f11f109d7acb85d6ac"
SRC_URI[openssl-libs.sha256sum] = "212def76c7dcc2cc8422b9ebcb9a485050c32f68cebabf66012bca3936cf79c3"
