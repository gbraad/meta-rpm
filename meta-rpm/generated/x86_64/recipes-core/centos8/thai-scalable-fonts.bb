SUMMARY = "generated recipe based on thai-scalable-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & BitstreamVera"
RPM_LICENSE = "GPLv2+ and Bitstream Vera"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_thai-scalable-fonts-common = "fontpackages-filesystem"
RDEPENDS_thai-scalable-garuda-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-kinnari-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-laksaman-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-loma-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-norasi-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-purisa-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-sawasdee-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-tlwgmono-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-tlwgtypewriter-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-tlwgtypist-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-tlwgtypo-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-umpush-fonts = "thai-scalable-fonts-common"
RDEPENDS_thai-scalable-waree-fonts = "thai-scalable-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-fonts-common-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-garuda-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-kinnari-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-laksaman-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-loma-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-norasi-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-purisa-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-sawasdee-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-tlwgmono-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-tlwgtypewriter-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-tlwgtypist-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-tlwgtypo-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-umpush-fonts-0.6.5-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/thai-scalable-waree-fonts-0.6.5-1.el8.noarch.rpm \
          "

SRC_URI[thai-scalable-fonts-common.sha256sum] = "591dc3a182d85005fd5fc5bea2ff45616d77f775b7f6384cbd90a1a6707c3f34"
SRC_URI[thai-scalable-garuda-fonts.sha256sum] = "daea35c1cc76c942b918400e95608b0ff9114972aafa5649698620a6062c6212"
SRC_URI[thai-scalable-kinnari-fonts.sha256sum] = "486ca9c8c307ec1d7451032673b152606b330a015973a69ed6287af957609abf"
SRC_URI[thai-scalable-laksaman-fonts.sha256sum] = "20de8d954f5652be1c12eb52f35a59b80383fc59da4f62d9f40c4aff23c35a94"
SRC_URI[thai-scalable-loma-fonts.sha256sum] = "0775e1da2d676696c81ec04c0fe40f00ea1db164cd97256910e1a2ff65c2fca2"
SRC_URI[thai-scalable-norasi-fonts.sha256sum] = "80b1e50a4786f49421084bc8372ba5c6e605f543e2c1108195038aec60b4c096"
SRC_URI[thai-scalable-purisa-fonts.sha256sum] = "5188087f200713dfe256d76439db9e441a5b6c42d2a6d356207062be0ef56a54"
SRC_URI[thai-scalable-sawasdee-fonts.sha256sum] = "3c28b812b894880fc736be27f256b8cebb06c0845f0972803a1849eef5f99060"
SRC_URI[thai-scalable-tlwgmono-fonts.sha256sum] = "3c6746e8db45624fe25c497985872a169372924e1914ebbe221408ab03901c67"
SRC_URI[thai-scalable-tlwgtypewriter-fonts.sha256sum] = "a40345d88f864ab7eb0ad969cd32eeb15cb6537c61b8956b391a19f869bb2cbe"
SRC_URI[thai-scalable-tlwgtypist-fonts.sha256sum] = "7dd37d054709e683cff4bcfd36c3a287bfca07152326f0a099f1f8197400229b"
SRC_URI[thai-scalable-tlwgtypo-fonts.sha256sum] = "0b6d0d4d05e3a6ee52de9b053bb11ff4762b795fb4016dbcf572c51c0b6a78cc"
SRC_URI[thai-scalable-umpush-fonts.sha256sum] = "881115ba5f0090b6aaf1ed22326fefe1110d40b17a374aaa4b5a41d7843ec0d7"
SRC_URI[thai-scalable-waree-fonts.sha256sum] = "d110c5b4658d5f8fc205d040aedd3388866aeaa02729d291a5e48fb8fba11de1"
