SUMMARY = "generated recipe based on gamin srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_gamin = "libfam.so.0 libgamin-1.so.0"
RPM_SONAME_REQ_gamin = "libc.so.6 libglib-2.0.so.0"
RDEPENDS_gamin = "glib2 glibc"
RPM_SONAME_REQ_gamin-devel = "libfam.so.0 libgamin-1.so.0"
RPROVIDES_gamin-devel = "gamin-dev (= 0.1.10)"
RDEPENDS_gamin-devel = "gamin pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gamin-0.1.10-31.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gamin-devel-0.1.10-31.el8.x86_64.rpm \
          "

SRC_URI[gamin.sha256sum] = "f05fbbd7bec5201911d5420403caae920697ee8f4222730c3e23ff9b1e0fb15e"
SRC_URI[gamin-devel.sha256sum] = "be5d5645d2fd9235ab3c0f08c58d513976b271355f425e9bffcad0a821ca7dc9"
