SUMMARY = "generated recipe based on subscription-manager srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 json-c libdnf librepo openssl pkgconfig-native rpm sqlite3 zlib"
RPM_SONAME_REQ_dnf-plugin-subscription-manager = "libc.so.6 libcrypto.so.1.1 libdnf.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-c.so.4 librepo.so.0 librpm.so.8 librpmio.so.8 libsqlite3.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_dnf-plugin-subscription-manager = "dnf glib2 glibc json-c libdnf librepo openssl-libs platform-python python3-dnf-plugins-core python3-librepo rpm-libs sqlite-libs zlib"
RDEPENDS_python3-syspurpose = "platform-python"
RDEPENDS_rhsm-gtk = "gtk3 librsvg2 platform-python python3-gobject rarian-compat rhsm-icons usermode-gtk"
RPM_SONAME_REQ_subscription-manager = "libc.so.6 libglib-2.0.so.0"
RDEPENDS_subscription-manager = "bash dnf-plugin-subscription-manager glib2 glibc platform-python platform-python-setuptools python3-dateutil python3-dbus python3-decorator python3-dmidecode python3-ethtool python3-gobject-base python3-iniparse python3-inotify python3-six python3-subscription-manager-rhsm python3-syspurpose systemd usermode virt-what"
RDEPENDS_subscription-manager-cockpit = "cockpit-bridge cockpit-system cockpit-ws rhsm-icons subscription-manager"
RDEPENDS_subscription-manager-initial-setup-addon = "initial-setup-gui rhsm-gtk"
RDEPENDS_subscription-manager-migration = "platform-python subscription-manager subscription-manager-migration-data"
RDEPENDS_subscription-manager-plugin-ostree = "platform-python python3-gobject-base python3-iniparse subscription-manager"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rhsm-gtk-1.26.20-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/subscription-manager-initial-setup-addon-1.26.20-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/subscription-manager-migration-1.26.20-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/dnf-plugin-subscription-manager-1.26.20-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-syspurpose-1.26.20-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/rhsm-icons-1.26.20-1.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/subscription-manager-1.26.20-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/subscription-manager-cockpit-1.26.20-1.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/subscription-manager-plugin-ostree-1.26.20-1.el8_2.x86_64.rpm \
          "

SRC_URI[dnf-plugin-subscription-manager.sha256sum] = "527a03078b4d997d3fbd07fca304a29e480ee9182942fb076318ef6489931264"
SRC_URI[python3-syspurpose.sha256sum] = "baa1ae813062151a4f6cb91ff5564b76f14ff662f1ab4294bb4293de50a548cb"
SRC_URI[rhsm-gtk.sha256sum] = "8809c85efd184d16c34b51f962d2e2db04cd2fa119d1b92c02d7ff80ca664521"
SRC_URI[rhsm-icons.sha256sum] = "d9b35dfb9ed2cba066c711c522758b83372c8b47c09cb26498c411d35ea3b7a6"
SRC_URI[subscription-manager.sha256sum] = "5c563600173a33890ae1f96addc54dc7f3e25ffba53181b148d93c009e69c32b"
SRC_URI[subscription-manager-cockpit.sha256sum] = "c63098a1ee8701758f070df20fd3ca899e820605a9e5c481df5f5f9fcd5da41e"
SRC_URI[subscription-manager-initial-setup-addon.sha256sum] = "1607b1a535aa6aa13113067f61a937ed41823419805445e07bac4cefb9d06211"
SRC_URI[subscription-manager-migration.sha256sum] = "75bfc98ee5b5e0bc7fe4f3c781deaf9ac6b634c2a71bf2d8dca0ea474acd2553"
SRC_URI[subscription-manager-plugin-ostree.sha256sum] = "0cc59fb40a81481ed95f6be60b76be57c73b15c45cf3639828043084888fd4b6"
