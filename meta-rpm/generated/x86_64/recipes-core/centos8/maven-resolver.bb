SUMMARY = "generated recipe based on maven-resolver srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-resolver = "java-1.8.0-openjdk-headless javapackages-filesystem maven-jar-plugin maven-parent"
RDEPENDS_maven-resolver-api = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_maven-resolver-connector-basic = "java-1.8.0-openjdk-headless javapackages-filesystem maven-resolver-api maven-resolver-spi maven-resolver-util"
RDEPENDS_maven-resolver-impl = "java-1.8.0-openjdk-headless javapackages-filesystem maven-resolver-api maven-resolver-spi maven-resolver-util"
RDEPENDS_maven-resolver-javadoc = "javapackages-filesystem"
RDEPENDS_maven-resolver-spi = "java-1.8.0-openjdk-headless javapackages-filesystem maven-resolver-api"
RDEPENDS_maven-resolver-test-util = "java-1.8.0-openjdk-headless javapackages-filesystem maven-resolver-api maven-resolver-spi"
RDEPENDS_maven-resolver-transport-classpath = "java-1.8.0-openjdk-headless javapackages-filesystem maven-resolver-api maven-resolver-spi maven-resolver-util"
RDEPENDS_maven-resolver-transport-file = "java-1.8.0-openjdk-headless javapackages-filesystem maven-resolver-api maven-resolver-spi"
RDEPENDS_maven-resolver-transport-http = "httpcomponents-client httpcomponents-core java-1.8.0-openjdk-headless javapackages-filesystem jcl-over-slf4j maven-resolver-api maven-resolver-spi maven-resolver-util"
RDEPENDS_maven-resolver-transport-wagon = "java-1.8.0-openjdk-headless javapackages-filesystem maven-resolver-api maven-resolver-spi maven-resolver-util maven-wagon-provider-api"
RDEPENDS_maven-resolver-util = "java-1.8.0-openjdk-headless javapackages-filesystem maven-resolver-api"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-api-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-connector-basic-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-impl-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-javadoc-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-spi-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-test-util-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-transport-classpath-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-transport-file-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-transport-http-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-transport-wagon-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-resolver-util-1.1.1-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-resolver.sha256sum] = "45ef4c61d13aac875fcf7ed56ad41bd84bdbe32f01cf306a364fcb5e34049a48"
SRC_URI[maven-resolver-api.sha256sum] = "2f0abcc170709f6b384e6fbd94a6217f5e1ca2bd1e40bcc7452fb82a730f1857"
SRC_URI[maven-resolver-connector-basic.sha256sum] = "625d5ee7a3c21c8a9bc154de9bdb58b401a135041b94738703639430273816ad"
SRC_URI[maven-resolver-impl.sha256sum] = "745720e1947dfa04b5aec31c7ecc975fdbbf59c91a6e681e20a6f36d938e906b"
SRC_URI[maven-resolver-javadoc.sha256sum] = "10ece58ea45b09e477add7633d7e4d5c2bd8b91720530ff8328e42991a6635e4"
SRC_URI[maven-resolver-spi.sha256sum] = "b0fab6cdc639cee68cfe78fca3047d6fc42c1fb816d70ea296fa486305ebf82c"
SRC_URI[maven-resolver-test-util.sha256sum] = "1e3af875815bc269a9363663c29e5bb772a3d0e33351f7486edec50818cf5e44"
SRC_URI[maven-resolver-transport-classpath.sha256sum] = "f3c8ae422a59910b4f984cfaf2d387e1d2d7521ef876f107b3afd5a7dcce722b"
SRC_URI[maven-resolver-transport-file.sha256sum] = "6b250863b83222699761fb7f612ff40654055062009e24f886c8f89446a98325"
SRC_URI[maven-resolver-transport-http.sha256sum] = "bd245c236be6b09a614f9c54d77689ef0c3da9f3a83e4c3e5d0757b0b3a915d5"
SRC_URI[maven-resolver-transport-wagon.sha256sum] = "3d85d64bf5ded697ef12531797ebb9fb2ab673b4dcc7d258e667ceb537ca4844"
SRC_URI[maven-resolver-util.sha256sum] = "412dc1155fbfc827321d934db35c4a347d4bca5c2982de20ad862225bda425f5"
