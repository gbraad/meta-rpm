SUMMARY = "generated recipe based on perl-experimental srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-experimental = "perl-Carp perl-interpreter perl-libs perl-version"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-experimental-0.019-2.el8.noarch.rpm \
          "

SRC_URI[perl-experimental.sha256sum] = "8f1e70e23e11e689b3ce917ef56c5b63794d636d29f12db4c905e3b46e5df036"
