SUMMARY = "generated recipe based on acl srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "attr pkgconfig-native"
RPM_SONAME_REQ_acl = "libacl.so.1 libattr.so.1 libc.so.6"
RDEPENDS_acl = "glibc libacl libattr"
RPM_SONAME_PROV_libacl = "libacl.so.1"
RPM_SONAME_REQ_libacl = "libattr.so.1 libc.so.6"
RDEPENDS_libacl = "glibc libattr"
RPM_SONAME_REQ_libacl-devel = "libacl.so.1"
RPROVIDES_libacl-devel = "libacl-dev (= 2.2.53)"
RDEPENDS_libacl-devel = "libacl libattr-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/acl-2.2.53-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libacl-2.2.53-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libacl-devel-2.2.53-1.el8.x86_64.rpm \
          "

SRC_URI[acl.sha256sum] = "227de6071cd3aeca7e10ad386beaf38737d081e06350d02208a3f6a2c9710385"
SRC_URI[libacl.sha256sum] = "4973664648b7ed9278bf29074ec6a60a9f660aa97c23a283750483f64429d5bb"
SRC_URI[libacl-devel.sha256sum] = "c910937153e1fa7c824945d74841431fe81ab2078bd58f6d87569d4d7396fce6"
