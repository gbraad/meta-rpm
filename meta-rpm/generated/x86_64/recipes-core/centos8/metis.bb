SUMMARY = "generated recipe based on metis srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & BSD & LGPL-2.0"
RPM_LICENSE = "ASL 2.0 and BSD and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_metis = "libmetis.so.0"
RPM_SONAME_REQ_metis = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_metis = "glibc libgcc libgomp"
RPM_SONAME_REQ_metis-devel = "libmetis.so.0"
RPROVIDES_metis-devel = "metis-dev (= 5.1.0)"
RDEPENDS_metis-devel = "metis"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/metis-5.1.0-17.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/metis-devel-5.1.0-17.el8.x86_64.rpm \
          "

SRC_URI[metis.sha256sum] = "e31e7976dcc5227eb58ede7688e0b258986be2e2658d9345f6fd70d047e648f4"
SRC_URI[metis-devel.sha256sum] = "b7813cd37854eb7fcca28522fc7703c39e3baa6fbd5f9803ac9bd0f4b8eae116"
