SUMMARY = "generated recipe based on maven-shade-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-shade-plugin = "apache-commons-io guava20 java-1.8.0-openjdk-headless javapackages-filesystem jdependency jdom maven-artifact-transfer maven-dependency-tree maven-lib objectweb-asm plexus-utils"
RDEPENDS_maven-shade-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-shade-plugin-3.1.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-shade-plugin-javadoc-3.1.0-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-shade-plugin.sha256sum] = "658ae16695ca366e23de438ca0199b520399f6b0c0bb24ef741041c2330c74ec"
SRC_URI[maven-shade-plugin-javadoc.sha256sum] = "9edf8524acd049270c8a046b820d424725e55fae099a9b864662810c672f6946"
