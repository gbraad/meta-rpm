SUMMARY = "generated recipe based on boost srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & MIT & Python-2.0"
RPM_LICENSE = "Boost and MIT and Python"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 icu libgcc mpich openmpi pkgconfig-native platform-python3 zlib"
RDEPENDS_boost = "boost-atomic boost-chrono boost-container boost-context boost-coroutine boost-date-time boost-fiber boost-filesystem boost-graph boost-iostreams boost-locale boost-log boost-math boost-program-options boost-random boost-regex boost-serialization boost-signals boost-stacktrace boost-system boost-test boost-thread boost-timer boost-type_erasure boost-wave"
RPM_SONAME_PROV_boost-atomic = "libboost_atomic.so.1.66.0"
RPM_SONAME_REQ_boost-atomic = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-atomic = "glibc libgcc libstdc++"
RDEPENDS_boost-build = "boost-jam platform-python"
RPM_SONAME_PROV_boost-chrono = "libboost_chrono.so.1.66.0"
RPM_SONAME_REQ_boost-chrono = "libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-chrono = "boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-container = "libboost_container.so.1.66.0"
RPM_SONAME_REQ_boost-container = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-container = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-context = "libboost_context.so.1.66.0"
RPM_SONAME_REQ_boost-context = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-context = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-coroutine = "libboost_coroutine.so.1.66.0"
RPM_SONAME_REQ_boost-coroutine = "libboost_chrono.so.1.66.0 libboost_context.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-coroutine = "boost-chrono boost-context boost-system boost-thread glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-date-time = "libboost_date_time.so.1.66.0"
RPM_SONAME_REQ_boost-date-time = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-date-time = "glibc libgcc libstdc++"
RPM_SONAME_REQ_boost-devel = "libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_container.so.1.66.0 libboost_context.so.1.66.0 libboost_coroutine.so.1.66.0 libboost_date_time.so.1.66.0 libboost_fiber.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_graph.so.1.66.0 libboost_iostreams.so.1.66.0 libboost_locale.so.1.66.0 libboost_log.so.1.66.0 libboost_log_setup.so.1.66.0 libboost_math_c99.so.1.66.0 libboost_math_c99f.so.1.66.0 libboost_math_c99l.so.1.66.0 libboost_math_tr1.so.1.66.0 libboost_math_tr1f.so.1.66.0 libboost_math_tr1l.so.1.66.0 libboost_prg_exec_monitor.so.1.66.0 libboost_program_options.so.1.66.0 libboost_random.so.1.66.0 libboost_regex.so.1.66.0 libboost_serialization.so.1.66.0 libboost_signals.so.1.66.0 libboost_stacktrace_addr2line.so.1.66.0 libboost_stacktrace_basic.so.1.66.0 libboost_stacktrace_noop.so.1.66.0 libboost_system.so.1.66.0 libboost_timer.so.1.66.0 libboost_type_erasure.so.1.66.0 libboost_unit_test_framework.so.1.66.0 libboost_wave.so.1.66.0 libboost_wserialization.so.1.66.0"
RPROVIDES_boost-devel = "boost-dev (= 1.66.0)"
RDEPENDS_boost-devel = "boost boost-atomic boost-chrono boost-container boost-context boost-coroutine boost-date-time boost-fiber boost-filesystem boost-graph boost-iostreams boost-locale boost-log boost-math boost-program-options boost-random boost-regex boost-serialization boost-signals boost-stacktrace boost-system boost-test boost-timer boost-type_erasure boost-wave libicu-devel libquadmath-devel"
RDEPENDS_boost-examples = "boost-devel"
RPM_SONAME_PROV_boost-fiber = "libboost_fiber.so.1.66.0"
RPM_SONAME_REQ_boost-fiber = "ld-linux-x86-64.so.2 libboost_context.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-fiber = "boost-context boost-filesystem boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-filesystem = "libboost_filesystem.so.1.66.0"
RPM_SONAME_REQ_boost-filesystem = "libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-filesystem = "boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-graph = "libboost_graph.so.1.66.0"
RPM_SONAME_REQ_boost-graph = "libboost_regex.so.1.66.0 libc.so.6 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-graph = "boost-regex glibc libgcc libicu libstdc++"
RPM_SONAME_PROV_boost-graph-mpich = "libboost_graph_parallel.so.1.66.0"
RPM_SONAME_REQ_boost-graph-mpich = "libboost_mpi.so.1.66.0 libboost_serialization.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-graph-mpich = "boost-mpich boost-serialization glibc libgcc libstdc++ mpich"
RPM_SONAME_PROV_boost-graph-openmpi = "libboost_graph_parallel.so.1.66.0"
RPM_SONAME_REQ_boost-graph-openmpi = "libboost_mpi.so.1.66.0 libboost_serialization.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.40 libmpi_cxx.so.40 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-graph-openmpi = "boost-openmpi boost-serialization glibc libgcc libstdc++ openmpi"
RPM_SONAME_PROV_boost-iostreams = "libboost_iostreams.so.1.66.0"
RPM_SONAME_REQ_boost-iostreams = "libbz2.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6 libz.so.1"
RDEPENDS_boost-iostreams = "bzip2-libs glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_boost-jam = "libc.so.6"
RDEPENDS_boost-jam = "glibc"
RPM_SONAME_PROV_boost-locale = "libboost_locale.so.1.66.0"
RPM_SONAME_REQ_boost-locale = "libboost_chrono.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-locale = "boost-chrono boost-system boost-thread glibc libgcc libicu libstdc++"
RPM_SONAME_PROV_boost-log = "libboost_log.so.1.66.0 libboost_log_setup.so.1.66.0"
RPM_SONAME_REQ_boost-log = "ld-linux-x86-64.so.2 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_regex.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-log = "boost-atomic boost-chrono boost-date-time boost-filesystem boost-regex boost-system boost-thread glibc libgcc libicu libstdc++"
RPM_SONAME_PROV_boost-math = "libboost_math_c99.so.1.66.0 libboost_math_c99f.so.1.66.0 libboost_math_c99l.so.1.66.0 libboost_math_tr1.so.1.66.0 libboost_math_tr1f.so.1.66.0 libboost_math_tr1l.so.1.66.0"
RPM_SONAME_REQ_boost-math = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-math = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-mpich = "libboost_mpi.so.1.66.0"
RPM_SONAME_REQ_boost-mpich = "libboost_serialization.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-mpich = "boost-serialization glibc libgcc libstdc++ mpich"
RPM_SONAME_REQ_boost-mpich-devel = "libboost_graph_parallel.so.1.66.0 libboost_mpi.so.1.66.0"
RPROVIDES_boost-mpich-devel = "boost-mpich-dev (= 1.66.0)"
RDEPENDS_boost-mpich-devel = "boost-devel boost-graph-mpich boost-mpich"
RPM_SONAME_PROV_boost-mpich-python3 = "libboost_mpi_python3.so.1.66.0"
RPM_SONAME_REQ_boost-mpich-python3 = "libboost_mpi.so.1.66.0 libboost_python3.so.1.66.0 libboost_serialization.so.1.66.0 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 libpython3.6m.so.1.0 librt.so.1 libstdc++.so.6 libutil.so.1"
RDEPENDS_boost-mpich-python3 = "boost-mpich boost-python3 boost-serialization glibc libgcc libstdc++ mpich platform-python python3-libs python3-mpich"
RPM_SONAME_PROV_boost-numpy3 = "libboost_numpy3.so.1.66.0"
RPM_SONAME_REQ_boost-numpy3 = "libboost_python3.so.1.66.0 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 librt.so.1 libstdc++.so.6 libutil.so.1"
RDEPENDS_boost-numpy3 = "boost-python3 glibc libgcc libstdc++ python3-libs python3-numpy"
RPM_SONAME_PROV_boost-openmpi = "libboost_mpi.so.1.66.0"
RPM_SONAME_REQ_boost-openmpi = "libboost_serialization.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.40 libmpi_cxx.so.40 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-openmpi = "boost-serialization glibc libgcc libstdc++ openmpi"
RPM_SONAME_REQ_boost-openmpi-devel = "libboost_graph_parallel.so.1.66.0 libboost_mpi.so.1.66.0"
RPROVIDES_boost-openmpi-devel = "boost-openmpi-dev (= 1.66.0)"
RDEPENDS_boost-openmpi-devel = "boost-devel boost-graph-openmpi boost-openmpi"
RPM_SONAME_PROV_boost-openmpi-python3 = "libboost_mpi_python3.so.1.66.0"
RPM_SONAME_REQ_boost-openmpi-python3 = "libboost_mpi.so.1.66.0 libboost_python3.so.1.66.0 libboost_serialization.so.1.66.0 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libmpi.so.40 libmpi_cxx.so.40 libpthread.so.0 libpython3.6m.so.1.0 librt.so.1 libstdc++.so.6 libutil.so.1"
RDEPENDS_boost-openmpi-python3 = "boost-openmpi boost-python3 boost-serialization glibc libgcc libstdc++ openmpi platform-python python3-libs python3-openmpi"
RPM_SONAME_PROV_boost-program-options = "libboost_program_options.so.1.66.0"
RPM_SONAME_REQ_boost-program-options = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-program-options = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-python3 = "libboost_python3.so.1.66.0"
RPM_SONAME_REQ_boost-python3 = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 librt.so.1 libstdc++.so.6 libutil.so.1"
RDEPENDS_boost-python3 = "glibc libgcc libstdc++ python3-libs"
RPM_SONAME_REQ_boost-python3-devel = "libboost_numpy3.so.1.66.0 libboost_python3.so.1.66.0"
RPROVIDES_boost-python3-devel = "boost-python3-dev (= 1.66.0)"
RDEPENDS_boost-python3-devel = "boost-devel boost-numpy3 boost-python3"
RPM_SONAME_PROV_boost-random = "libboost_random.so.1.66.0"
RPM_SONAME_REQ_boost-random = "libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-random = "boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-regex = "libboost_regex.so.1.66.0"
RPM_SONAME_REQ_boost-regex = "libc.so.6 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-regex = "glibc libgcc libicu libstdc++"
RPM_SONAME_PROV_boost-serialization = "libboost_serialization.so.1.66.0 libboost_wserialization.so.1.66.0"
RPM_SONAME_REQ_boost-serialization = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-serialization = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-signals = "libboost_signals.so.1.66.0"
RPM_SONAME_REQ_boost-signals = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-signals = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-stacktrace = "libboost_stacktrace_addr2line.so.1.66.0 libboost_stacktrace_basic.so.1.66.0 libboost_stacktrace_noop.so.1.66.0"
RPM_SONAME_REQ_boost-stacktrace = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-stacktrace = "glibc libgcc libstdc++"
RDEPENDS_boost-static = "boost-devel"
RPM_SONAME_PROV_boost-system = "libboost_system.so.1.66.0"
RPM_SONAME_REQ_boost-system = "libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-system = "glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-test = "libboost_prg_exec_monitor.so.1.66.0 libboost_unit_test_framework.so.1.66.0"
RPM_SONAME_REQ_boost-test = "libboost_system.so.1.66.0 libboost_timer.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-test = "boost-system boost-timer glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-thread = "libboost_thread.so.1.66.0"
RPM_SONAME_REQ_boost-thread = "libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-thread = "boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-timer = "libboost_timer.so.1.66.0"
RPM_SONAME_REQ_boost-timer = "libboost_chrono.so.1.66.0 libboost_system.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-timer = "boost-chrono boost-system glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-type_erasure = "libboost_type_erasure.so.1.66.0"
RPM_SONAME_REQ_boost-type_erasure = "libboost_chrono.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-type_erasure = "boost-chrono boost-system boost-thread glibc libgcc libstdc++"
RPM_SONAME_PROV_boost-wave = "libboost_wave.so.1.66.0"
RPM_SONAME_REQ_boost-wave = "libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_boost-wave = "boost-chrono boost-date-time boost-filesystem boost-system boost-thread glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-atomic-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-chrono-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-container-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-context-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-coroutine-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-date-time-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-devel-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-fiber-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-filesystem-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-graph-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-iostreams-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-locale-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-log-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-math-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-program-options-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-random-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-regex-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-serialization-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-signals-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-stacktrace-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-system-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-test-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-thread-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-timer-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-type_erasure-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/boost-wave-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-build-1.66.0-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-doc-1.66.0-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-examples-1.66.0-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-graph-mpich-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-graph-openmpi-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-jam-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-mpich-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-mpich-devel-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-mpich-python3-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-numpy3-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-openmpi-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-openmpi-devel-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-openmpi-python3-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-python3-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-python3-devel-1.66.0-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/boost-static-1.66.0-7.el8.x86_64.rpm \
          "

SRC_URI[boost.sha256sum] = "abb4dc9261d54637c3353ba93713964c2f29a86385b4129864571dfa54f2e8cf"
SRC_URI[boost-atomic.sha256sum] = "97175ba28bef8067089ff7bd18b940909947f42e9ac600bf5165e3bba7df78eb"
SRC_URI[boost-build.sha256sum] = "620180b4ace4b81b9b4c349eae2c1163fb036b847b9c5bb6f4a8f681b4375955"
SRC_URI[boost-chrono.sha256sum] = "4d6a4c7d367db31a7645956b9aa0635f38ce1c2de9c2dcd72b300f47ab7197b8"
SRC_URI[boost-container.sha256sum] = "23a06863cbb2b41fe18129d3f182b6f61b23fa09b9ebc02db46bae803c9911f8"
SRC_URI[boost-context.sha256sum] = "b2c0179af8eea4bec2acecb17d6e2362309bc2b52e6cf97f405a547ace1de5e8"
SRC_URI[boost-coroutine.sha256sum] = "f6ca808158c3693466cc3cae88286195d32996383c849c29732f1c633781e594"
SRC_URI[boost-date-time.sha256sum] = "7ae5b783864c234802c4dab96ee51bc05be3555dd4331842d2279ecf9119cc0e"
SRC_URI[boost-devel.sha256sum] = "995494d5cbc165ef07ac8f69699ec9a5ede8a47c212b12bd508e3385805834b8"
SRC_URI[boost-doc.sha256sum] = "debee86c107bf8f326a71d189cae237e806d3b993dcb6b2d319ad098c730f832"
SRC_URI[boost-examples.sha256sum] = "c8961375f400f1a1aa53c4676c9f59ac95cc758332f94e8c2521b661fb2a7c35"
SRC_URI[boost-fiber.sha256sum] = "388912c4b653181816849647619dba8f0ff74b376afe2237aae2fc72f8a7ad27"
SRC_URI[boost-filesystem.sha256sum] = "b0cb39e967bfde775821466a4dfc76d8fae1e6f3f48c0d9d3c70a12285e4f22c"
SRC_URI[boost-graph.sha256sum] = "0d6db5d471c4fcd240e8a532ed4b6dac190d55ffae79592e2696b1dcdfd97e37"
SRC_URI[boost-graph-mpich.sha256sum] = "841572d85ac7f55a9cef427492ceeb30dc8082bbe890cba852382888e9ad3579"
SRC_URI[boost-graph-openmpi.sha256sum] = "43965c5c5314776d4796347ac6ec663b909cf96e47636923ae37425f45d7ea16"
SRC_URI[boost-iostreams.sha256sum] = "8753bc32ede4aad0c61d817f065b32b77ca8c07448ed4f0f27e396de60843249"
SRC_URI[boost-jam.sha256sum] = "bad09187480e31ecae6721f4b26ded7723f7cde64cd93ecc0901b0cce943ed7e"
SRC_URI[boost-locale.sha256sum] = "c4e4997805f19c36c732a41c566307f079010f6ea31095fe1fdebbb7c01c8997"
SRC_URI[boost-log.sha256sum] = "5a81171b0e3f50ea419ae3426fc346be07ddee8d07b39932a87f5b1840663066"
SRC_URI[boost-math.sha256sum] = "1eebf2172fb47821a9944165c99997609f8ed88c81ad5fc453110ffa21381797"
SRC_URI[boost-mpich.sha256sum] = "650da27233c0903598f358a02b658cd9ccef5e93d94e44c584b87b325233f791"
SRC_URI[boost-mpich-devel.sha256sum] = "aa25546dfe0ca6dfe07a86ba7f9ea8eace6c09c722e143c16bf154bb358cb5cd"
SRC_URI[boost-mpich-python3.sha256sum] = "64869f3dfa06af394c53deb29d17d35ece9a21cfac853568d660a465d932c5dd"
SRC_URI[boost-numpy3.sha256sum] = "99b9bbfd86098b739ae50b7c4baa2898b3be45f95d1f5ef72f0febcd29ea95b3"
SRC_URI[boost-openmpi.sha256sum] = "2ccd0982e94f8a550bcfaba408ca3d0517d606ec9ff4db94830634c1fa925851"
SRC_URI[boost-openmpi-devel.sha256sum] = "3bb2f28a7cded59327b763c71f1052c47abe0f27a6c04c0e046373e7aba9836a"
SRC_URI[boost-openmpi-python3.sha256sum] = "c3a3049722ac6a33dbe09ca4a179cfc3615da2e7beb3d8b3e7bb81da1e71f7e8"
SRC_URI[boost-program-options.sha256sum] = "a0514f24c2bbc8ea14a13703acb5f35b1b5ba1dfb97397e6776d522c8ea60331"
SRC_URI[boost-python3.sha256sum] = "54acc521fe0b0c4e139c76c2ea75f2cb0616d2f142b3be158539659b3dc42c32"
SRC_URI[boost-python3-devel.sha256sum] = "742fc773854b58da3a1111d3763448e8b89d7d52aa461653e23445343ecdb40d"
SRC_URI[boost-random.sha256sum] = "ce42866a1ba717acf396d07d696b028c89ac6b331d071f3d15a0ca21c26ca0e5"
SRC_URI[boost-regex.sha256sum] = "1fc49859936514495a21e612b9dafad31a12c2a6f53f50da6da1400de1b95c3b"
SRC_URI[boost-serialization.sha256sum] = "523188bfea35c3c0dcfe7eeec65489aabc4a1ed73f8eda9d8f9563cf43a23114"
SRC_URI[boost-signals.sha256sum] = "ca8cc89da95365cb5546121bfa74fb77e2a01306d965fb38a1c7768298b46bd0"
SRC_URI[boost-stacktrace.sha256sum] = "7b38d6be65f72ef54ffc0a51e425b30ad61d4ab763a6ef7efbd097da54bc6125"
SRC_URI[boost-static.sha256sum] = "9c23a36b4a3829bf573af682b90b29ce847f4e0cae9ede9ab56e1d63614766a3"
SRC_URI[boost-system.sha256sum] = "562d5c9c55eb3ad34b4be1a98167d080c69232216884c4ca7d0a5fbc5c704ed0"
SRC_URI[boost-test.sha256sum] = "5bc3d395ae820ad9899a6213dbb2156089df79575d9e232d0de845c253274b7f"
SRC_URI[boost-thread.sha256sum] = "de7a578e3acdcaca58822ae58fe8152ba97867626d454380e26a3bfa23d4db2a"
SRC_URI[boost-timer.sha256sum] = "1e282495ba415e8c45c2ae5813350df87df098986845bbedc69f29cd032b28b6"
SRC_URI[boost-type_erasure.sha256sum] = "693b8036b965820603e095860e17e52ed002a441a39932d395698db580954d0e"
SRC_URI[boost-wave.sha256sum] = "43c196894042293a927a617b704b6dac047ecf94a7859e517b689a0bd16499ee"
