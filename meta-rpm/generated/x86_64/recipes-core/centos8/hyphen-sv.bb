SUMMARY = "generated recipe based on hyphen-sv srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-2.0"
RPM_LICENSE = "LGPLv2+ or GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-sv = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-sv-1.00.1-18.el8.noarch.rpm \
          "

SRC_URI[hyphen-sv.sha256sum] = "128f9952afd15c611d12af4986e70269ea3c0ca548939f5dee7a53a4ba14681a"
