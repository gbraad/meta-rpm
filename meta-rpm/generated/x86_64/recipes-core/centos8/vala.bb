SUMMARY = "generated recipe based on vala srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & BSD"
RPM_LICENSE = "LGPLv2+ and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_vala = "libvala-0.40.so.0 libvalaccodegen.so"
RPM_SONAME_REQ_vala = "libc.so.6 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_vala = "bash chkconfig glib2 glibc gobject-introspection-devel pkgconf-pkg-config"
RPM_SONAME_REQ_vala-devel = "libvala-0.40.so.0"
RPROVIDES_vala-devel = "vala-dev (= 0.40.19)"
RDEPENDS_vala-devel = "glib2-devel pkgconf-pkg-config vala"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/vala-0.40.19-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/vala-devel-0.40.19-1.el8.x86_64.rpm \
          "

SRC_URI[vala.sha256sum] = "77a51d0fb3fc5786c812ca1c02b042e32991b8ce28d77459429505725048ffa9"
SRC_URI[vala-devel.sha256sum] = "24b25696662de9e1861ea8ecffbd7fe6ec4bd7338f4ee394af6983bca676d302"
