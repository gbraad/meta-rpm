SUMMARY = "generated recipe based on hunspell-kn srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-2.0 | MPL-1.1"
RPM_LICENSE = "GPLv2+ or LGPLv2+ or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-kn = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-kn-1.0.3-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-kn.sha256sum] = "94759ede0d759f16db8fd9e5fe98424b2da1a2d5fcc4c7eccab21a7787c5b1d9"
