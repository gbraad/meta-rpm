SUMMARY = "generated recipe based on libXrandr srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 libxext libxrender pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXrandr = "libXrandr.so.2"
RPM_SONAME_REQ_libXrandr = "libX11.so.6 libXext.so.6 libXrender.so.1 libc.so.6"
RDEPENDS_libXrandr = "glibc libX11 libXext libXrender"
RPM_SONAME_REQ_libXrandr-devel = "libXrandr.so.2"
RPROVIDES_libXrandr-devel = "libXrandr-dev (= 1.5.1)"
RDEPENDS_libXrandr-devel = "libX11-devel libXext-devel libXrandr libXrender-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXrandr-1.5.1-7.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libXrandr-devel-1.5.1-7.el8.x86_64.rpm \
          "

SRC_URI[libXrandr.sha256sum] = "a12be80122704be667f2fa73f8b7a6ba7e2b225d44b082518aa9e6697c817a60"
SRC_URI[libXrandr-devel.sha256sum] = "05b57c3566697324b28c37345f18cab1ff471d285c8015c1a1161ab8e81983f3"
