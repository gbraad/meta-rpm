SUMMARY = "generated recipe based on liboggz srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libogg pkgconfig-native"
RPM_SONAME_PROV_liboggz = "liboggz.so.2"
RPM_SONAME_REQ_liboggz = "libc.so.6 libm.so.6 libogg.so.0"
RDEPENDS_liboggz = "bash glibc libogg"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/liboggz-1.1.1-14.el8.x86_64.rpm \
          "

SRC_URI[liboggz.sha256sum] = "4e902cd9611e5760bf7871596d673eeacbd7be5e6e7e6779b6066232f8c61025"
