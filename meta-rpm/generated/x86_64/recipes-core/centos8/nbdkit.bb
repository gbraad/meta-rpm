SUMMARY = "generated recipe based on nbdkit srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gnutls-libs libgcc libselinux pkgconfig-native platform-python3 xz zlib"
RPM_SONAME_REQ_nbdkit = "libc.so.6 libdl.so.2 libgcc_s.so.1 libgnutls.so.30 libpthread.so.0 libselinux.so.1"
RDEPENDS_nbdkit = "glibc gnutls libgcc libselinux"
RDEPENDS_nbdkit-bash-completion = "bash-completion nbdkit"
RPM_SONAME_REQ_nbdkit-basic-plugins = "libc.so.6 libselinux.so.1"
RDEPENDS_nbdkit-basic-plugins = "glibc libselinux nbdkit"
RPROVIDES_nbdkit-devel = "nbdkit-dev (= 1.4.2)"
RDEPENDS_nbdkit-devel = "nbdkit pkgconf-pkg-config"
RPM_SONAME_REQ_nbdkit-example-plugins = "libc.so.6 libselinux.so.1"
RDEPENDS_nbdkit-example-plugins = "glibc libselinux nbdkit"
RPM_SONAME_REQ_nbdkit-plugin-gzip = "libc.so.6 libselinux.so.1 libz.so.1"
RDEPENDS_nbdkit-plugin-gzip = "glibc libselinux nbdkit zlib"
RDEPENDS_nbdkit-plugin-python-common = "nbdkit"
RPM_SONAME_REQ_nbdkit-plugin-python3 = "libc.so.6 libpython3.6m.so.1.0 libselinux.so.1"
RDEPENDS_nbdkit-plugin-python3 = "glibc libselinux nbdkit nbdkit-plugin-python-common python3-libs"
RPM_SONAME_REQ_nbdkit-plugin-vddk = "libc.so.6 libdl.so.2 libselinux.so.1"
RDEPENDS_nbdkit-plugin-vddk = "glibc libselinux nbdkit"
RPM_SONAME_REQ_nbdkit-plugin-xz = "libc.so.6 liblzma.so.5 libselinux.so.1"
RDEPENDS_nbdkit-plugin-xz = "glibc libselinux nbdkit xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nbdkit-1.4.2-5.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nbdkit-bash-completion-1.4.2-5.module_el8.2.0+320+13f867d7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nbdkit-basic-plugins-1.4.2-5.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nbdkit-devel-1.4.2-5.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nbdkit-example-plugins-1.4.2-5.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nbdkit-plugin-gzip-1.4.2-5.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nbdkit-plugin-python-common-1.4.2-5.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nbdkit-plugin-python3-1.4.2-5.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nbdkit-plugin-vddk-1.4.2-5.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/nbdkit-plugin-xz-1.4.2-5.module_el8.2.0+320+13f867d7.x86_64.rpm \
          "

SRC_URI[nbdkit.sha256sum] = "4b5ca293bde37b13ed645fcb1b5c37ef85a568bbe4508846a6edd5fd89db50f7"
SRC_URI[nbdkit-bash-completion.sha256sum] = "57cb9d10781f50348ce36f5c858ad77079382011c7e4ac8300da1fada17eadaf"
SRC_URI[nbdkit-basic-plugins.sha256sum] = "7dc10dd7a014c9e87affd311142956933547bb6a823f2eeee0006cbb339d75f5"
SRC_URI[nbdkit-devel.sha256sum] = "100a351f0af3875fd08c0ca74a823444e851601ba1f26e8c82b07cd390fd0f09"
SRC_URI[nbdkit-example-plugins.sha256sum] = "227aa9afb670ba67ad2c2e204e2c5ecadd4591ecd6dc44a9dff49f502da688c5"
SRC_URI[nbdkit-plugin-gzip.sha256sum] = "7d9e39edc9d1540beff94dfa753151074b3e929981221db03d949f0eab6b4a96"
SRC_URI[nbdkit-plugin-python-common.sha256sum] = "f248c295fe64f63bfb4695e33e9483269a830938b090949a967cb16928a74875"
SRC_URI[nbdkit-plugin-python3.sha256sum] = "fea3a92890a22e8e500ca2ebc8089410ac2c87e16e71ad0f03b9c4f8bf3017f3"
SRC_URI[nbdkit-plugin-vddk.sha256sum] = "674a1d8b37e61b3b7712485f452765a173a7ab7943dc2a6c673e6ed1d87276c3"
SRC_URI[nbdkit-plugin-xz.sha256sum] = "2cb6f1a126c1f11d9433b14e0d36665d26e380918db9bb9dc01a139d2385b51e"
