SUMMARY = "generated recipe based on perl-Filter srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Filter = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Filter = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Filter-1.58-2.el8.x86_64.rpm \
          "

SRC_URI[perl-Filter.sha256sum] = "758a793abe09e11c66fb56945e9e342f8521a9beadbf95d65033899f1facaf9a"
