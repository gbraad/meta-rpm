SUMMARY = "generated recipe based on perl-Test-Requires srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Requires = "perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Requires-0.10-10.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Requires.sha256sum] = "4319669bc00fc8b49a3216ac4e2985b17377e69edff65d65d061357f5e3b0b03"
