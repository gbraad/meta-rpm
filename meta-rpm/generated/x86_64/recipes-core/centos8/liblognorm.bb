SUMMARY = "generated recipe based on liblognorm srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libestr libfastjson libpcre pkgconfig-native"
RPM_SONAME_PROV_liblognorm = "liblognorm.so.5"
RPM_SONAME_REQ_liblognorm = "libc.so.6 libestr.so.0 libfastjson.so.4 libpcre.so.1"
RDEPENDS_liblognorm = "glibc libestr libfastjson pcre"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liblognorm-2.0.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/liblognorm-doc-2.0.5-1.el8.x86_64.rpm \
          "

SRC_URI[liblognorm.sha256sum] = "e0fc85915200c25a06090e09cf0742c9755d49c3bfb830554b0b3c8ceb98985f"
SRC_URI[liblognorm-doc.sha256sum] = "f6f34361fcd491235fda795630b3253331a0c1002e0f5eef9a1d98043592790d"
