SUMMARY = "generated recipe based on libsecret srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libgcrypt libgpg-error pkgconfig-native"
RPM_SONAME_PROV_libsecret = "libsecret-1.so.0"
RPM_SONAME_REQ_libsecret = "libc.so.6 libdl.so.2 libgcrypt.so.20 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libpthread.so.0"
RDEPENDS_libsecret = "glib2 glibc libgcrypt libgpg-error"
RPM_SONAME_REQ_libsecret-devel = "libsecret-1.so.0"
RPROVIDES_libsecret-devel = "libsecret-dev (= 0.18.6)"
RDEPENDS_libsecret-devel = "glib2-devel libsecret pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsecret-0.18.6-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libsecret-devel-0.18.6-1.el8.x86_64.rpm \
          "

SRC_URI[libsecret.sha256sum] = "f8d06e0c4b3f4a2c75f8ee6ffafb6a06fbbe1ecc5f3ee87d6e6df12c3c590c5b"
SRC_URI[libsecret-devel.sha256sum] = "f7b99ce32cf19e4c3791fcbd78b6fb0b69329e2c381076bae59add95a0af5725"
