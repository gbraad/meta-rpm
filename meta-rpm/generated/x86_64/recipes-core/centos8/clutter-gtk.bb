SUMMARY = "generated recipe based on clutter-gtk srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo clutter cogl gdk-pixbuf glib-2.0 gtk+3 json-glib libdrm libglvnd libx11 libxcomposite libxdamage libxext libxfixes libxi libxkbcommon libxrandr mesa pango pkgconfig-native wayland"
RPM_SONAME_PROV_clutter-gtk = "libclutter-gtk-1.0.so.0"
RPM_SONAME_REQ_clutter-gtk = "libEGL.so.1 libX11.so.6 libXcomposite.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXrandr.so.2 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libclutter-1.0.so.0 libcogl-pango.so.20 libcogl-path.so.20 libcogl.so.20 libdrm.so.2 libgbm.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjson-glib-1.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libwayland-server.so.0 libxkbcommon.so.0"
RDEPENDS_clutter-gtk = "atk cairo cairo-gobject clutter cogl gdk-pixbuf2 glib2 glibc gtk3 json-glib libX11 libXcomposite libXdamage libXext libXfixes libXi libXrandr libdrm libglvnd-egl libwayland-client libwayland-cursor libwayland-egl libwayland-server libxkbcommon mesa-libgbm pango"
RPM_SONAME_REQ_clutter-gtk-devel = "libclutter-gtk-1.0.so.0"
RPROVIDES_clutter-gtk-devel = "clutter-gtk-dev (= 1.8.4)"
RDEPENDS_clutter-gtk-devel = "clutter-devel clutter-gtk gtk3-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/clutter-gtk-1.8.4-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/clutter-gtk-devel-1.8.4-3.el8.x86_64.rpm \
          "

SRC_URI[clutter-gtk.sha256sum] = "80c6f380531dc0290d755363bce3d71251bf7f4caa6ca85cb908fb60717970d8"
SRC_URI[clutter-gtk-devel.sha256sum] = "033c7d629378cd96df5dfc00489d971cde3952d2135ef405e9915dd532429d7f"
