SUMMARY = "generated recipe based on mcstrans srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libcap libpcre libselinux pkgconfig-native"
RPM_SONAME_REQ_mcstrans = "libc.so.6 libcap.so.2 libpcre.so.1 libselinux.so.1"
RDEPENDS_mcstrans = "bash glibc libcap libselinux pcre systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/mcstrans-2.9-2.el8.x86_64.rpm \
          "

SRC_URI[mcstrans.sha256sum] = "a5ae4b95f76a15f8c93512e60db987e48cd14e1115957c0a19c53d93bbabfd2d"
