SUMMARY = "generated recipe based on libidn2 srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | LGPL-3.0) & GPL-3.0"
RPM_LICENSE = "(GPLv2+ or LGPLv3+) and GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libunistring pkgconfig-native"
RPM_SONAME_REQ_idn2 = "libc.so.6 libidn2.so.0 libunistring.so.2"
RDEPENDS_idn2 = "glibc libidn2 libunistring"
RPM_SONAME_PROV_libidn2 = "libidn2.so.0"
RPM_SONAME_REQ_libidn2 = "libc.so.6 libunistring.so.2"
RDEPENDS_libidn2 = "glibc libunistring"
RPM_SONAME_REQ_libidn2-devel = "libidn2.so.0"
RPROVIDES_libidn2-devel = "libidn2-dev (= 2.2.0)"
RDEPENDS_libidn2-devel = "libidn2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/idn2-2.2.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libidn2-2.2.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libidn2-devel-2.2.0-1.el8.x86_64.rpm \
          "

SRC_URI[idn2.sha256sum] = "4494cef305d09a9c0c288b714f39ea7cb93a56e39164a4854a6b01942af6481f"
SRC_URI[libidn2.sha256sum] = "7e08785bd3cc0e09f9ab4bf600b98b705203d552cbb655269a939087987f1694"
SRC_URI[libidn2-devel.sha256sum] = "a84668ad2f76396dc8b6e00ccc2589e9fb1aa602eb73a0d6123a0f6cdf658ab4"
