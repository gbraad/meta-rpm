SUMMARY = "generated recipe based on perl-Error srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & MIT"
RPM_LICENSE = "(GPL+ or Artistic) and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Error = "perl-Carp perl-Exporter perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Error-0.17025-2.el8.noarch.rpm \
          "

SRC_URI[perl-Error.sha256sum] = "de4051d5d3f1f14a1fea06e6e5fbe9f47ff39ed051f345e500dcd0b2c684616c"
