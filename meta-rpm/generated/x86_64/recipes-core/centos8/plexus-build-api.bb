SUMMARY = "generated recipe based on plexus-build-api srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-build-api = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-utils"
RDEPENDS_plexus-build-api-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-build-api-0.0.7-20.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-build-api-javadoc-0.0.7-20.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-build-api.sha256sum] = "568d55a7fa7ff183e1c58d08c8c662e7578838c87400781f4cb89d6d5f051a25"
SRC_URI[plexus-build-api-javadoc.sha256sum] = "3480a528ccbd7cf000c53823bcae506559da748d1b6a34e2504df01125008e39"
