SUMMARY = "generated recipe based on hyphen-ku srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-2.0"
RPM_LICENSE = "GPLv2+ or LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-ku = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-ku-1.71.2-14.el8.noarch.rpm \
          "

SRC_URI[hyphen-ku.sha256sum] = "42e50a953337612cfc1eb89fbcd23f4265e146e62c772b1a3de0a5b78f6cd1cd"
