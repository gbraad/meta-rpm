SUMMARY = "generated recipe based on cpio srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_cpio = "libc.so.6"
RDEPENDS_cpio = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/cpio-2.12-8.el8.x86_64.rpm \
          "

SRC_URI[cpio.sha256sum] = "c5bf206709fc7895b1ba51073d5984d3df562b94b80e0144a809a930284f0288"
