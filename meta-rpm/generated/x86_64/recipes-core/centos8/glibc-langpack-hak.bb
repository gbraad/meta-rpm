SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-hak = "locale-base-hak-tw (= 2.28) virtual-locale-hak (= 2.28) virtual-locale-hak-tw (= 2.28)"
RDEPENDS_glibc-langpack-hak = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-hak-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-hak.sha256sum] = "e5e2c0f0b7222801c0901c59890bcc9aff6860c0c9639ffb35c46ebf882720cf"
