SUMMARY = "generated recipe based on perl-IPC-SysV srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-IPC-SysV = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-IPC-SysV = "glibc perl-Carp perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-IPC-SysV-2.07-397.el8.x86_64.rpm \
          "

SRC_URI[perl-IPC-SysV.sha256sum] = "a4575502e1513f395b1961928312550d39411fb1458b445eb68cb30e1b4128b9"
