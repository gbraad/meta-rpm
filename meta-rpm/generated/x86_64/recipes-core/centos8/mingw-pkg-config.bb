SUMMARY = "generated recipe based on mingw-pkg-config srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_mingw32-pkg-config = "libc.so.6 libglib-2.0.so.0"
RDEPENDS_mingw32-pkg-config = "glib2 glibc mingw32-filesystem"
RPM_SONAME_REQ_mingw64-pkg-config = "libc.so.6 libglib-2.0.so.0"
RDEPENDS_mingw64-pkg-config = "glib2 glibc mingw64-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw32-pkg-config-0.28-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/mingw64-pkg-config-0.28-11.el8.x86_64.rpm \
          "

SRC_URI[mingw32-pkg-config.sha256sum] = "4f6b568cedfb2d4263dc2339dbae578fb6d94ea81124323862fe092592019c1e"
SRC_URI[mingw64-pkg-config.sha256sum] = "e9f732abd1a33c42f0db57ba3b853cb5bcf6dddc027516213c4d3b2731ca1b65"
