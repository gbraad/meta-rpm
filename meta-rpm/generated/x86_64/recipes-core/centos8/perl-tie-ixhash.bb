SUMMARY = "generated recipe based on perl-Tie-IxHash srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Tie-IxHash = "perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Tie-IxHash-1.23-13.el8.noarch.rpm \
          "

SRC_URI[perl-Tie-IxHash.sha256sum] = "3a7bd2a6ad613c1e22c23c96383f4875bed860898f0d38e6728078ef6f5658ae"
