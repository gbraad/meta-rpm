SUMMARY = "generated recipe based on make srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "virtual/make"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_make = "libc.so.6 libdl.so.2"
RDEPENDS_make = "bash glibc info"
RPROVIDES_make-devel = "make-dev (= 4.2.1)"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/make-4.2.1-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/make-devel-4.2.1-10.el8.x86_64.rpm \
          "

SRC_URI[make.sha256sum] = "838ccf69e4e4f7e0685ae5883827b403c672bb32e1cafee8e023e238aa650fb6"
SRC_URI[make-devel.sha256sum] = "0d94938fd72d13f520a79e776de75f0a5b903b6556d9075d9273b10a9ce81e22"
