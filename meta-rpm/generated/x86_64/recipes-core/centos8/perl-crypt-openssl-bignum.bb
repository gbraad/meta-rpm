SUMMARY = "generated recipe based on perl-Crypt-OpenSSL-Bignum srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl perl pkgconfig-native"
RPM_SONAME_REQ_perl-Crypt-OpenSSL-Bignum = "libc.so.6 libcrypto.so.1.1 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Crypt-OpenSSL-Bignum = "glibc openssl-libs perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Crypt-OpenSSL-Bignum-0.09-5.el8.x86_64.rpm \
          "

SRC_URI[perl-Crypt-OpenSSL-Bignum.sha256sum] = "133d4a7db450be596504a81605546d81badcd35c4975bd54dab675a184d738a6"
