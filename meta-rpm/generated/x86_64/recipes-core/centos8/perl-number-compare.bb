SUMMARY = "generated recipe based on perl-Number-Compare srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Number-Compare = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Number-Compare-0.03-19.el8.noarch.rpm \
          "

SRC_URI[perl-Number-Compare.sha256sum] = "a70013793d7440970c0944a43c9ba68380f5411a22b943afa1fc902a38714eb5"
