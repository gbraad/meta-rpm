SUMMARY = "generated recipe based on setup srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_setup = "centos-release"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/setup-2.12.2-5.el8.noarch.rpm \
          "

SRC_URI[setup.sha256sum] = "73db3670ce0ce861a10d759e593d469c09d209dc703551935e2c0d6ede9215cf"
