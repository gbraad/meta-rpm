SUMMARY = "generated recipe based on ninja-build srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_ninja-build = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_ninja-build = "emacs-filesystem glibc libgcc libstdc++ vim-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/ninja-build-1.8.2-1.el8.x86_64.rpm \
          "

SRC_URI[ninja-build.sha256sum] = "d6c320f3e82a483c505265d57cdb722bf70a0b7efdc1be016a5538eebb15c9b4"
