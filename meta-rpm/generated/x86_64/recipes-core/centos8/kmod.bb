SUMMARY = "generated recipe based on kmod srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc openssl pkgconfig-native xz zlib"
RPM_SONAME_REQ_kmod = "libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 liblzma.so.5 libz.so.1"
RDEPENDS_kmod = "bash glibc libgcc openssl-libs xz-libs zlib"
RPM_SONAME_REQ_kmod-devel = "libkmod.so.2"
RPROVIDES_kmod-devel = "kmod-dev (= 25)"
RDEPENDS_kmod-devel = "kmod-libs pkgconf-pkg-config"
RPM_SONAME_PROV_kmod-libs = "libkmod.so.2"
RPM_SONAME_REQ_kmod-libs = "libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 liblzma.so.5 libssl.so.1.1 libz.so.1"
RDEPENDS_kmod-libs = "glibc libgcc openssl-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kmod-25-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/kmod-libs-25-16.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/kmod-devel-25-16.el8.x86_64.rpm \
          "

SRC_URI[kmod.sha256sum] = "7b867d450b934e960d40ec9a5e1ac98cad3339aea15266d54586e5773eb25dc6"
SRC_URI[kmod-devel.sha256sum] = "44b1769685dd030429a44faf1107a7705ce32ae6628590e113f31c35254857c1"
SRC_URI[kmod-libs.sha256sum] = "f79730f28af5d2bc3bdcd82f5f061de702f4d9129eee977415d4df0aaf7c34bb"
