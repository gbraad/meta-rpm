SUMMARY = "generated recipe based on openhpi srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "curl glib-2.0 json-c libgcc libgcrypt librabbitmq libuuid libxml2 net-snmp openssl pkgconfig-native sysfsutils"
RPM_SONAME_PROV_openhpi = "libdyn_simulator.so.3 libilo2_ribcl.so.3 libipmidirect.so.3 liboa_soap.so.3 libov_rest.so.3 libsimulator.so.3 libslave.so.3 libsnmp_bc.so.3 libsysfs2hpi.so.3 libtest_agent.so.3 libwatchdog.so.3"
RPM_SONAME_REQ_openhpi = "libc.so.6 libcrypto.so.1.1 libcurl.so.4 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgmodule-2.0.so.0 libgthread-2.0.so.0 libjson-c.so.4 libm.so.6 libnetsnmp.so.35 libopenhpi.so.3 libopenhpi_snmp.so.3 libopenhpi_ssl.so.3 libopenhpimarshal.so.3 libopenhpitransport.so.3 libopenhpiutils.so.3 libpthread.so.0 librabbitmq.so.4 libssl.so.1.1 libstdc++.so.6 libsysfs.so.2 libuuid.so.1 libxml2.so.2"
RDEPENDS_openhpi = "bash glib2 glibc json-c libcurl libgcc libgcrypt librabbitmq libstdc++ libsysfs libuuid libxml2 net-snmp-libs openhpi-libs openssl-libs systemd"
RPM_SONAME_PROV_openhpi-libs = "libopenhpi.so.3 libopenhpi_snmp.so.3 libopenhpi_ssl.so.3 libopenhpimarshal.so.3 libopenhpitransport.so.3 libopenhpiutils.so.3"
RPM_SONAME_REQ_openhpi-libs = "libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 libgcrypt.so.20 libglib-2.0.so.0 libgthread-2.0.so.0 libm.so.6 libnetsnmp.so.35 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libuuid.so.1"
RDEPENDS_openhpi-libs = "glib2 glibc libgcc libgcrypt libstdc++ libuuid net-snmp-libs openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openhpi-3.8.0-9.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openhpi-libs-3.8.0-9.el8.x86_64.rpm \
          "

SRC_URI[openhpi.sha256sum] = "7eb8fbdd05d3a3b0f6ea40769b0abba520cb53d158d4f824c4780c480aad1f86"
SRC_URI[openhpi-libs.sha256sum] = "1431564928f8de3477db5cfca7dc53e085f876634238d979ddcd0fb06de08522"
