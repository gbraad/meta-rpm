SUMMARY = "generated recipe based on tinyxml2 srpm"
DESCRIPTION = "Description"
LICENSE = "Zlib"
RPM_LICENSE = "zlib"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_tinyxml2 = "libtinyxml2.so.6"
RPM_SONAME_REQ_tinyxml2 = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_tinyxml2 = "glibc libgcc libstdc++"
RPM_SONAME_REQ_tinyxml2-devel = "libtinyxml2.so.6"
RPROVIDES_tinyxml2-devel = "tinyxml2-dev (= 6.0.0)"
RDEPENDS_tinyxml2-devel = "cmake-filesystem pkgconf-pkg-config tinyxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/tinyxml2-6.0.0-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/tinyxml2-devel-6.0.0-3.el8.x86_64.rpm \
          "

SRC_URI[tinyxml2.sha256sum] = "cd0139f12d041b69d498a79254124c83f2b819a0d3b5e95c9f33d8305a69977c"
SRC_URI[tinyxml2-devel.sha256sum] = "a38ebaf8875c6293bf1896a29ac95a10ae83ad1ae481861e286381607800cfdc"
