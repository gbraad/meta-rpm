SUMMARY = "generated recipe based on google-noto-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_google-noto-kufi-arabic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-mono-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-naskh-arabic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-naskh-arabic-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-nastaliq-urdu-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-armenian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-avestan-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-balinese-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-bamum-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-batak-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-bengali-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-bengali-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-brahmi-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-buginese-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-buhid-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-canadian-aboriginal-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-carian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-cham-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-cherokee-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-coptic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-cuneiform-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-cypriot-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-deseret-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-devanagari-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-devanagari-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-egyptian-hieroglyphs-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-ethiopic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-georgian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-glagolitic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-gothic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-gujarati-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-gujarati-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-gurmukhi-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-gurmukhi-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-hanunoo-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-hebrew-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-imperial-aramaic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-inscriptional-pahlavi-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-inscriptional-parthian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-javanese-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-kaithi-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-kannada-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-kannada-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-kayah-li-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-kharoshthi-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-khmer-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-khmer-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-lao-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-lao-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-lepcha-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-limbu-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-linear-b-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-lisu-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-lycian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-lydian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-malayalam-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-malayalam-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-mandaic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-meetei-mayek-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-mongolian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-myanmar-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-myanmar-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-new-tai-lue-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-nko-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-ogham-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-ol-chiki-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-old-italic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-old-persian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-old-south-arabian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-old-turkic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-oriya-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-oriya-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-osmanya-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-phags-pa-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-phoenician-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-rejang-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-runic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-samaritan-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-saurashtra-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-shavian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-sinhala-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-sundanese-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-syloti-nagri-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-symbols-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-syriac-eastern-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-syriac-estrangela-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-syriac-western-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-tagalog-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-tagbanwa-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-tai-le-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-tai-tham-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-tai-viet-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-tamil-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-tamil-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-telugu-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-telugu-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-thaana-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-thai-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-thai-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-tibetan-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-tifinagh-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-ugaritic-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-ui-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-vai-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-sans-yi-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-armenian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-bengali-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-devanagari-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-georgian-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-gujarati-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-kannada-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-khmer-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-lao-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-malayalam-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-tamil-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-telugu-fonts = "bash fontpackages-filesystem google-noto-fonts-common"
RDEPENDS_google-noto-serif-thai-fonts = "bash fontpackages-filesystem google-noto-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-fonts-common-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-kufi-arabic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-mono-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-naskh-arabic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-naskh-arabic-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-nastaliq-urdu-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-armenian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-avestan-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-balinese-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-bamum-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-batak-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-bengali-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-bengali-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-brahmi-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-buginese-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-buhid-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-canadian-aboriginal-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-carian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-cham-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-cherokee-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-coptic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-cuneiform-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-cypriot-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-deseret-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-devanagari-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-devanagari-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-egyptian-hieroglyphs-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-ethiopic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-georgian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-glagolitic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-gothic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-gujarati-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-gujarati-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-gurmukhi-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-gurmukhi-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-hanunoo-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-hebrew-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-imperial-aramaic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-inscriptional-pahlavi-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-inscriptional-parthian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-javanese-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-kaithi-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-kannada-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-kannada-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-kayah-li-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-kharoshthi-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-khmer-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-khmer-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-lao-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-lao-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-lepcha-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-limbu-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-linear-b-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-lisu-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-lycian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-lydian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-malayalam-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-malayalam-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-mandaic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-meetei-mayek-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-mongolian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-myanmar-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-myanmar-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-new-tai-lue-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-nko-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-ogham-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-ol-chiki-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-old-italic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-old-persian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-old-south-arabian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-old-turkic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-oriya-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-oriya-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-osmanya-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-phags-pa-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-phoenician-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-rejang-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-runic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-samaritan-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-saurashtra-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-shavian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-sinhala-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-sundanese-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-syloti-nagri-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-symbols-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-syriac-eastern-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-syriac-estrangela-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-syriac-western-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-tagalog-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-tagbanwa-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-tai-le-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-tai-tham-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-tai-viet-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-tamil-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-tamil-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-telugu-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-telugu-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-thaana-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-thai-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-thai-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-tibetan-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-tifinagh-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-ugaritic-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-ui-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-vai-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-sans-yi-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-armenian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-bengali-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-devanagari-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-georgian-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-gujarati-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-kannada-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-khmer-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-lao-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-malayalam-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-tamil-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-telugu-fonts-20161022-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/google-noto-serif-thai-fonts-20161022-7.el8.noarch.rpm \
          "

SRC_URI[google-noto-fonts-common.sha256sum] = "1dba391cb282a891c9185382b3fa453941678a7290b3a79cf55a6e470d0b7791"
SRC_URI[google-noto-kufi-arabic-fonts.sha256sum] = "0bf9b78c8266589a4807da0f41bd9892d8e10b159dc8183303cbcf04ef57295c"
SRC_URI[google-noto-mono-fonts.sha256sum] = "71e232065f91454f955ff548d0ce6a3f28bef8d2c8f8bdaf3729b62e005270f6"
SRC_URI[google-noto-naskh-arabic-fonts.sha256sum] = "aed145ca3272ec30bc6e6570399845a17da0eeb439046e90adfb03ff47ca775c"
SRC_URI[google-noto-naskh-arabic-ui-fonts.sha256sum] = "9111e9195c94888c2ce01c2eb54013d9c1c7a0a0dc4ef0a7ac2d84c219389bda"
SRC_URI[google-noto-nastaliq-urdu-fonts.sha256sum] = "0088249206446827485e3dc4bb2bcb875e89a7e208ecfe2eb28cd4e4f831aa27"
SRC_URI[google-noto-sans-armenian-fonts.sha256sum] = "6de6c220a96fb90b1756ed9a7b5cb0be6d2add7bc75170248f656a08a824ba08"
SRC_URI[google-noto-sans-avestan-fonts.sha256sum] = "3d8ed8c86f3f41126b957664cbfd8ef141bef77dfd7890b2b1cbd93235770751"
SRC_URI[google-noto-sans-balinese-fonts.sha256sum] = "306a5334988bd892d8abb905a7e968bc166989fb557a342e49b76f9b2773ef53"
SRC_URI[google-noto-sans-bamum-fonts.sha256sum] = "0d7dde92281be315a51bbf1be4de4610ad24286f716162995641129d2e5d9668"
SRC_URI[google-noto-sans-batak-fonts.sha256sum] = "1fde6a1ccd5d33d133a6f63035247ca50d0d1b69e994f31cc8fe10a5f3288dc7"
SRC_URI[google-noto-sans-bengali-fonts.sha256sum] = "e69c5262fdada6eebcb1ed690ef8db288b4b8b3985dbe7e02da685e64245643a"
SRC_URI[google-noto-sans-bengali-ui-fonts.sha256sum] = "19b104edbc5ab1c560480b55b16c45cf46c16c29195d286a072fb2ba782e96d6"
SRC_URI[google-noto-sans-brahmi-fonts.sha256sum] = "829aa3b873fd4e5cb01b27c25510c529cce879f27a1ebbc4ac6afa20ce7b2117"
SRC_URI[google-noto-sans-buginese-fonts.sha256sum] = "19aa992f335d9340969cd0e24cb3919e40d2b555e7f547e42d1b1312d4bec4cb"
SRC_URI[google-noto-sans-buhid-fonts.sha256sum] = "da26b9a6bddb11ac5e298a7aee18812035fa2a2813d1a15768491780cce30805"
SRC_URI[google-noto-sans-canadian-aboriginal-fonts.sha256sum] = "a7f515e43ed8f100a048d1f585b3a83cdb4eda56c4a5809dd7e6a03c72e94a3f"
SRC_URI[google-noto-sans-carian-fonts.sha256sum] = "88c3b20674e7b4b037c5c5d6124cd8838eb28122588f88d087bd248667ec6332"
SRC_URI[google-noto-sans-cham-fonts.sha256sum] = "2ae71379200a1770c869a4ad5f8de0e3cb9ee2f7b8ccf5b9d5296667e79a7e59"
SRC_URI[google-noto-sans-cherokee-fonts.sha256sum] = "ec5a5c7b715403c0bf8391b2b86ee2345542036dbca6f6683904581a8ceac6a1"
SRC_URI[google-noto-sans-coptic-fonts.sha256sum] = "cf267742294327b0c739cef8cde2cd365b19a3848f4199d4f9c2fe3f636f4418"
SRC_URI[google-noto-sans-cuneiform-fonts.sha256sum] = "ea28ad90832db8b90b5cb56999fb24cf2054bea503d5313f75d8c054097ef7c5"
SRC_URI[google-noto-sans-cypriot-fonts.sha256sum] = "5a6b4edf1181d6d0f1d256b9e6d2ae669b86c7add5dc1f7194b098e112bdc9ff"
SRC_URI[google-noto-sans-deseret-fonts.sha256sum] = "9b05f880fb8dedc1d8dadc1fe2240fbf656b97ff00023b073c35343081a7607c"
SRC_URI[google-noto-sans-devanagari-fonts.sha256sum] = "d08c1ddbd6224a512bcecb44bd1531c4b295fd2137d252f2253ce282c3bdf56b"
SRC_URI[google-noto-sans-devanagari-ui-fonts.sha256sum] = "b4f597d20590f657bcd160c38ecb997d99e0764ec928e35753c98363ebfe7ed2"
SRC_URI[google-noto-sans-egyptian-hieroglyphs-fonts.sha256sum] = "f0d95ac3c943521fc030f50171d54bb6fbc694232a11e8cc58c11ea1d788baee"
SRC_URI[google-noto-sans-ethiopic-fonts.sha256sum] = "660e1c4e632ff99107ea745c72be326197dfcac4dd9a153e8232cc7bca10dd6f"
SRC_URI[google-noto-sans-fonts.sha256sum] = "bff65a406594f8dcc6703158869f8888bab8fd2ba2bbb79efee47f269708a9fd"
SRC_URI[google-noto-sans-georgian-fonts.sha256sum] = "e65385e882f34372927523fde9cdc750173db5ec8dfb29e5b642b154daf4c255"
SRC_URI[google-noto-sans-glagolitic-fonts.sha256sum] = "d0847ee8ac1267241767a9e83e1d678e55bba05450bcec35d3b499079c8acdb5"
SRC_URI[google-noto-sans-gothic-fonts.sha256sum] = "c56effb33ff7466ba8bdb1cc7a3160e477bf8a343d069f6e60c246387ad18dfe"
SRC_URI[google-noto-sans-gujarati-fonts.sha256sum] = "e42c0f8a92f42c422c14b76b37db5862d15108fc9e9effb64035f1e14d5c22c7"
SRC_URI[google-noto-sans-gujarati-ui-fonts.sha256sum] = "ecad8054099ec3e1fc6fb5174e12b082a1d7bca390142048f3653f20ecdd693a"
SRC_URI[google-noto-sans-gurmukhi-fonts.sha256sum] = "61309432c0e0a25771c851e5af0d986add73f747c82b9ec9db9cf283620806c6"
SRC_URI[google-noto-sans-gurmukhi-ui-fonts.sha256sum] = "57ca1338025858cc1acd12faf920234524bb12788e9cf1f67873d50f218e5995"
SRC_URI[google-noto-sans-hanunoo-fonts.sha256sum] = "0442eb7ca997161455c909e4bcb1d8c20583df13c47426abad25aec55e97b521"
SRC_URI[google-noto-sans-hebrew-fonts.sha256sum] = "7eeb6cf16f06f92219df1e165b13626f29c7345edde576f07e84dbc7d8bcc68a"
SRC_URI[google-noto-sans-imperial-aramaic-fonts.sha256sum] = "2c2f415d082353ff57797e2a13a75d8c86ce9cea1a2df76bb8ec819723530337"
SRC_URI[google-noto-sans-inscriptional-pahlavi-fonts.sha256sum] = "4a58c1ec4f6c6e7d07a93a61627560798046c29295ecb7a96faead20fb38c3d6"
SRC_URI[google-noto-sans-inscriptional-parthian-fonts.sha256sum] = "9e2097b2319c9b3fbfd2e0238875a54e3299f6f320e8496122a328c55bb4f8e5"
SRC_URI[google-noto-sans-javanese-fonts.sha256sum] = "9cb3042db167e24ba48d52fae0345fddda9c87de47da3a9733ad9462c326ca2f"
SRC_URI[google-noto-sans-kaithi-fonts.sha256sum] = "5fa48db9848fccd1c77ecc6474df2bafe2b189cbf950349afe611455068ee77d"
SRC_URI[google-noto-sans-kannada-fonts.sha256sum] = "f6c8a8a5d4554ec4ae523d564ec85c8016431dc1de9fb6f8026102620a1173ca"
SRC_URI[google-noto-sans-kannada-ui-fonts.sha256sum] = "80d4b165a0b802f89438e9025fed02e68f57a3182241e734be108e1c0e19db4e"
SRC_URI[google-noto-sans-kayah-li-fonts.sha256sum] = "63d9791d62587e53f9a2eea9a6126f048eaf0c5811465a9abe764d97b869540c"
SRC_URI[google-noto-sans-kharoshthi-fonts.sha256sum] = "7b0a12506b190ef1e11afbcd3a25f6f15a15dd32de5fbfc725031e04c6ee40eb"
SRC_URI[google-noto-sans-khmer-fonts.sha256sum] = "5439ffab26a98f65b21d5e041367f98f261698f3c8235bb9dea842cb6de911b4"
SRC_URI[google-noto-sans-khmer-ui-fonts.sha256sum] = "85507716aa8440c07b38516b066dcbda9fab136a4edea720a986d06f3991a492"
SRC_URI[google-noto-sans-lao-fonts.sha256sum] = "2040872e4287b05908c1c969772ab6af99a5ce186930aa99edfba8506c8aef00"
SRC_URI[google-noto-sans-lao-ui-fonts.sha256sum] = "ecaec03333ced94a42971766beaf82ba484a7e98d669f3c1d40fbfca677d8722"
SRC_URI[google-noto-sans-lepcha-fonts.sha256sum] = "236fcf1fb6a56a633d63eab24a86acb9c25d1493f9de641951a995c571eb45a3"
SRC_URI[google-noto-sans-limbu-fonts.sha256sum] = "a5e1ee55a92d23bc1c8ab47ab9b520bcba8692d181037c80a48adfce87d3afa4"
SRC_URI[google-noto-sans-linear-b-fonts.sha256sum] = "bd87bdf3f1d904adf2b0ba68a29a044915d129ebfa7ef904e03414bfcdd57cf6"
SRC_URI[google-noto-sans-lisu-fonts.sha256sum] = "cd118d008b7aec1a8fe5f33c1d1847a5d61d44aaf014643c29878bf7f732a438"
SRC_URI[google-noto-sans-lycian-fonts.sha256sum] = "9a4423054bb36e3eabe678730362e983f61981532ea53aaf508ff5ca34b6bc3d"
SRC_URI[google-noto-sans-lydian-fonts.sha256sum] = "73cfce169cc961c67688a4f9f953aaa1591a2be2270354d602cf31c2210ccce0"
SRC_URI[google-noto-sans-malayalam-fonts.sha256sum] = "de2ef8d821c68da46ed0aba0a4752a9bdde3c8892e413b406401fc8dfc900a46"
SRC_URI[google-noto-sans-malayalam-ui-fonts.sha256sum] = "523200dc53054e8ac78c3184da9a223be95a48ff116757e64f058514255b1949"
SRC_URI[google-noto-sans-mandaic-fonts.sha256sum] = "7e0207f724231b2bd6e7ff89f4d5c319c46ec7c245d5d771123ca97ae2e65ec8"
SRC_URI[google-noto-sans-meetei-mayek-fonts.sha256sum] = "497378c89b6fb23c5db835b3b608de4e44b6fa270591700170940fae55db3a0c"
SRC_URI[google-noto-sans-mongolian-fonts.sha256sum] = "70ac135b9116a277a5234066ee4eb03987c31268a9a7adcd4985975cee301325"
SRC_URI[google-noto-sans-myanmar-fonts.sha256sum] = "041c916920df05b0d79d5804908ddd7d7fe16a8c0a1a0103c2ed4038d1a2a856"
SRC_URI[google-noto-sans-myanmar-ui-fonts.sha256sum] = "acc1d7ce0f04c1261bce2b0873e04527dd801815f36adbb42e4cd97254252275"
SRC_URI[google-noto-sans-new-tai-lue-fonts.sha256sum] = "826d59b13af68dc228c39b54c320bfa1b2738839059cd2a943d0ed421d360e0e"
SRC_URI[google-noto-sans-nko-fonts.sha256sum] = "91ca3e9b2784ee9d44c73d96b7fdde89efab8225e8f6d18d7bc0f019f0b248d0"
SRC_URI[google-noto-sans-ogham-fonts.sha256sum] = "5638de3ad05c90c84f059be29479e4106ebda6fe3adc35e13f8a3c5ce6fcb4ab"
SRC_URI[google-noto-sans-ol-chiki-fonts.sha256sum] = "788c18f088c16d34a702fc4cd2df659b8aa8b5e4f61411c943a972525c46bc7c"
SRC_URI[google-noto-sans-old-italic-fonts.sha256sum] = "2a8888758d9bcbd896d2fc210f356f3603669e76f658736df0012570fc1554d9"
SRC_URI[google-noto-sans-old-persian-fonts.sha256sum] = "49ffcbc44c2c74470f80649e75d5bbb899e5bab56c5a753214072ce698a4ba6a"
SRC_URI[google-noto-sans-old-south-arabian-fonts.sha256sum] = "b5138f41f35c3e1563e0eb1ea600a78af247a9f264aa5148b116d4ec3372ca2a"
SRC_URI[google-noto-sans-old-turkic-fonts.sha256sum] = "ca03af4411911ff7209441b768b3317134145c2eb2294d8269a02933659491e6"
SRC_URI[google-noto-sans-oriya-fonts.sha256sum] = "0694971556a169fa2bc48ce198691119cce3fa0c7fa5103c257bf1e8ec98649a"
SRC_URI[google-noto-sans-oriya-ui-fonts.sha256sum] = "3cecbb9e86eaaa92bdd014c4dee468ec01ae082e55c68eb1ca14486d61d78f06"
SRC_URI[google-noto-sans-osmanya-fonts.sha256sum] = "d7facd68a2d2edb48d03f2c5e9f007df5cb9373dcec06907904da296760987c2"
SRC_URI[google-noto-sans-phags-pa-fonts.sha256sum] = "0db9e3f8520e8bd60d29c6dcd81c250dc21b3e21a906a6b9021e47e7325519f0"
SRC_URI[google-noto-sans-phoenician-fonts.sha256sum] = "e48aec0f7b4111cf68339a843c0ade85abc64b353d1665b4e362f9e385d1993e"
SRC_URI[google-noto-sans-rejang-fonts.sha256sum] = "6b97a45b2b2b0704653cef6ce48680ceda2a985556f9a07595d567c5e5b9b3ca"
SRC_URI[google-noto-sans-runic-fonts.sha256sum] = "8d14a56fd70e2ea30d00bfa2c320ab20ed82e707f5a9ac2d99c9e93c1f8ab183"
SRC_URI[google-noto-sans-samaritan-fonts.sha256sum] = "ea9e8c4405c2233853456f40b34fb4da8e46e94d9d487006baa9d7f47061bce0"
SRC_URI[google-noto-sans-saurashtra-fonts.sha256sum] = "4ad8584987f8a621495dde35b9b49aabdff4a9fbba3f005ac290ea097a4e56ef"
SRC_URI[google-noto-sans-shavian-fonts.sha256sum] = "ff8d94a6db644a702ad918a5fd0bfed5ee0bfea2381c647acdb3518e9ff90dea"
SRC_URI[google-noto-sans-sinhala-fonts.sha256sum] = "7dfc6eb8185fd161e98d6b179bfcd17e16b629eec59f92459a84fb89077a1cc9"
SRC_URI[google-noto-sans-sundanese-fonts.sha256sum] = "3354e83045b3fd8485b06c097745b03091f08cf935e7b7069d44b4cbd50d1342"
SRC_URI[google-noto-sans-syloti-nagri-fonts.sha256sum] = "73f7f30b004e0568ad3f8988dfa85a1ab0f5e6377dfa1009a78de72b127c75e6"
SRC_URI[google-noto-sans-symbols-fonts.sha256sum] = "cf148a0d4e01e6b50a86dede0e58e1b608b8692a6f2833850cfe9f76ea573023"
SRC_URI[google-noto-sans-syriac-eastern-fonts.sha256sum] = "064b7e561eff493269fbd0ecb39147ef98b4dfc7a2846ed7b9397529edd3d5b2"
SRC_URI[google-noto-sans-syriac-estrangela-fonts.sha256sum] = "599789951a91681276e06c4ec18e4700fca7dd210543d9a313d20ea5352ad661"
SRC_URI[google-noto-sans-syriac-western-fonts.sha256sum] = "7cc02b29b310d114b8c530908d641a1e22314ba777bfca22070618cf395feed4"
SRC_URI[google-noto-sans-tagalog-fonts.sha256sum] = "704b11fc663911ad73697c7887d14b308ee9a5c07b49216120ca300b805f6dd2"
SRC_URI[google-noto-sans-tagbanwa-fonts.sha256sum] = "1f20c622670123785d34a6b91c6167030179ed69b4d92f3da99eac16ffb2afc2"
SRC_URI[google-noto-sans-tai-le-fonts.sha256sum] = "ffee88fed26fcbeced34c2ff1effdc3e48a4b28846897f4dada65a1aee8e07e5"
SRC_URI[google-noto-sans-tai-tham-fonts.sha256sum] = "90cf90056c1e244297c06065a948db3140f2a242aeeb39e6620a549d77bbbbb9"
SRC_URI[google-noto-sans-tai-viet-fonts.sha256sum] = "222324c1f7308768ca44fb32187329be1ae385f75b025ad5f37b694d93e41e45"
SRC_URI[google-noto-sans-tamil-fonts.sha256sum] = "7e2978e6a4f21c23ff90cb83f859ab1326c5cfbbd862c99dafaa785cf52fffbf"
SRC_URI[google-noto-sans-tamil-ui-fonts.sha256sum] = "38bd635f9a0cd1379ae3c0f676ba85b564468a87a1e5b0bac2881a6ecaaa9317"
SRC_URI[google-noto-sans-telugu-fonts.sha256sum] = "544ac4c7d2c8c7ee3b7abc5a5662997a1783e7472e9eb50284894607c69f0dca"
SRC_URI[google-noto-sans-telugu-ui-fonts.sha256sum] = "643c79559ee330e2b372a58348433276c831c33abf14cceb7a145e37b922085b"
SRC_URI[google-noto-sans-thaana-fonts.sha256sum] = "1e4529a4c05420c0dcd7dd8c3cb8b890ee92de258f2862fdbf4f6dd7c30aa2af"
SRC_URI[google-noto-sans-thai-fonts.sha256sum] = "774689e2e79449c8eb6fe4ec14d3de4b02ff91b694654b1d1a923ac1170b7ea0"
SRC_URI[google-noto-sans-thai-ui-fonts.sha256sum] = "b65734e3bf48d7e0eb196c44e94bdc7f764ea3412b2bd15b4abba7635adceff6"
SRC_URI[google-noto-sans-tibetan-fonts.sha256sum] = "3d7ee036b9949d2750706b355eb77bae86d0c5ee36de1a59808259fbbbc05ca2"
SRC_URI[google-noto-sans-tifinagh-fonts.sha256sum] = "14ef6ca46dd9ae580f070f6ac67ffd69a7777b8b2b6376a88ca166b0280ad7f9"
SRC_URI[google-noto-sans-ugaritic-fonts.sha256sum] = "80b1fcce60f021a96642d39005f88c3d27b696ca249b9fde0305eee930352e88"
SRC_URI[google-noto-sans-ui-fonts.sha256sum] = "588b2b793afed5c89a917f645ca093f190f986b9db95d00b1b6952995b37ee21"
SRC_URI[google-noto-sans-vai-fonts.sha256sum] = "61320e909b1918f89e011b537d4ae73e45e8da8e94273234176d29788612f029"
SRC_URI[google-noto-sans-yi-fonts.sha256sum] = "994e0ec42f6ea594f490894b60e17dcec3e46e9f4f42a177e341775f857f60f7"
SRC_URI[google-noto-serif-armenian-fonts.sha256sum] = "cb23339ead60b578029541fb8e2c7d6fe31d7011e9eb6a9c18389aefbc28526f"
SRC_URI[google-noto-serif-bengali-fonts.sha256sum] = "d79995de09d9b9c13748da711264a9e278d4d213d97ed19ee7f8989e16cff112"
SRC_URI[google-noto-serif-devanagari-fonts.sha256sum] = "58d4881f2cc6c9fed5ae1b225c19dfd06db0a9a5c22661601509ee322da0b1d3"
SRC_URI[google-noto-serif-fonts.sha256sum] = "7602deeca5af308d96bd0dbe6349f95205a2bf2a8f8d6190ce8b9fb25e83b3b9"
SRC_URI[google-noto-serif-georgian-fonts.sha256sum] = "dbf728dcb7b5cbb50daf8ca4716f26a7549efaf52b0aaa9dd2dca6da2889a58c"
SRC_URI[google-noto-serif-gujarati-fonts.sha256sum] = "a4c8889771209d7e6d82bb3634c9c44fe962dc3555cac01f5c523e97eb83ace5"
SRC_URI[google-noto-serif-kannada-fonts.sha256sum] = "14329640b70076446ad3037167bf02e5fa5b6a0a10170af08dec2706e4294395"
SRC_URI[google-noto-serif-khmer-fonts.sha256sum] = "dd7c7877e37ea743086d1e65f6032823b04b2328be59bcad5259336f16c164c7"
SRC_URI[google-noto-serif-lao-fonts.sha256sum] = "b395b7986973e73cfe3a7ad5a29dd113b884cd1643b574bf9d67a6c6bb1231d8"
SRC_URI[google-noto-serif-malayalam-fonts.sha256sum] = "331fd77b3be18df62f8b00451ab9870e146cf738c4f3aeec028b4c06abcf32b1"
SRC_URI[google-noto-serif-tamil-fonts.sha256sum] = "de3721dc3f4d19c3cb2e301db420e223c89c43485953172e63a7a5af1c1cfc23"
SRC_URI[google-noto-serif-telugu-fonts.sha256sum] = "eeb1bd7dd1fdac9dcfb965ba7821018f2c3faf8fd3bacc697bbb1bf15b3bcda4"
SRC_URI[google-noto-serif-thai-fonts.sha256sum] = "1a85de32c75e9aa30ee550c5ea9430ed2839fe048e5c14b3522597fb464ea018"
