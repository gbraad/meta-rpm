SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-it = "locale-base-it-ch (= 2.28) locale-base-it-ch.utf8 (= 2.28) locale-base-it-it (= 2.28) locale-base-it-it.utf8 (= 2.28) locale-base-it-it@euro (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it (= 2.28) virtual-locale-it-ch (= 2.28) virtual-locale-it-ch.utf8 (= 2.28) virtual-locale-it-it (= 2.28) virtual-locale-it-it.utf8 (= 2.28) virtual-locale-it-it@euro (= 2.28)"
RDEPENDS_glibc-langpack-it = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-it-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-it.sha256sum] = "1aa7ef66a44e8bf962d4d1e30f19717d589048b478140844a359ab95b8e74e6a"
