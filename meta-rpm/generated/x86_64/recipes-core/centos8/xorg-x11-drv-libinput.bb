SUMMARY = "generated recipe based on xorg-x11-drv-libinput srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libinput pkgconfig-native"
RPM_SONAME_PROV_xorg-x11-drv-libinput = "libinput_drv.so"
RPM_SONAME_REQ_xorg-x11-drv-libinput = "libc.so.6 libinput.so.10 libm.so.6"
RDEPENDS_xorg-x11-drv-libinput = "glibc libinput xkeyboard-config xorg-x11-server-Xorg"
RPROVIDES_xorg-x11-drv-libinput-devel = "xorg-x11-drv-libinput-dev (= 0.29.0)"
RDEPENDS_xorg-x11-drv-libinput-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-libinput-0.29.0-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xorg-x11-drv-libinput-devel-0.29.0-1.el8.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-libinput.sha256sum] = "04d6c225cec082dfd887283ebd97d0c9187e2e0ffd39db7399612c7e06ef1269"
SRC_URI[xorg-x11-drv-libinput-devel.sha256sum] = "227d11f83d3203c32ec20a1557076ee2e375a33ef02afeb9654aff506d3d8179"
