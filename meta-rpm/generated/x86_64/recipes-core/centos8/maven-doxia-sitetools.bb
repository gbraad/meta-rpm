SUMMARY = "generated recipe based on maven-doxia-sitetools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-doxia-sitetools = "apache-commons-collections apache-commons-io apache-commons-lang3 java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-doxia-core maven-doxia-logging-api maven-doxia-module-apt maven-doxia-module-fml maven-doxia-module-xdoc maven-doxia-module-xhtml maven-doxia-sink-api maven-lib maven-model maven-reporting-api plexus-containers-component-annotations plexus-containers-container-default plexus-i18n plexus-interpolation plexus-utils plexus-velocity velocity xalan-j2 xml-commons-apis"
RDEPENDS_maven-doxia-sitetools-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-sitetools-1.7.5-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-doxia-sitetools-javadoc-1.7.5-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-doxia-sitetools.sha256sum] = "b0b063d2349575251a70ef9fe42faefa0e52014c710e34bc1297c981f6fef51b"
SRC_URI[maven-doxia-sitetools-javadoc.sha256sum] = "88e193bc1fdaca3d84b0e05daf5a655b462ca78b3c7bbaeac4fce6ab6dcc9442"
