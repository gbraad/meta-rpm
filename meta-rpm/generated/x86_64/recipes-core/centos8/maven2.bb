SUMMARY = "generated recipe based on maven2 srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-artifact = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-utils"
RDEPENDS_maven-artifact-manager = "java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-lib maven-wagon-provider-api plexus-containers-container-default plexus-utils"
RDEPENDS_maven-model = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-utils"
RDEPENDS_maven-monitor = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_maven-plugin-descriptor = "java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-lib plexus-containers-container-default"
RDEPENDS_maven-plugin-registry = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-containers-container-default plexus-utils"
RDEPENDS_maven-profile = "java-1.8.0-openjdk-headless javapackages-filesystem maven-model plexus-containers-container-default plexus-interpolation plexus-utils"
RDEPENDS_maven-project = "java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-artifact-manager maven-model maven-plugin-registry maven-profile maven-settings plexus-containers-container-default plexus-interpolation plexus-utils"
RDEPENDS_maven-settings = "java-1.8.0-openjdk-headless javapackages-filesystem maven-model plexus-containers-container-default plexus-interpolation plexus-utils"
RDEPENDS_maven-toolchain = "java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-lib"
RDEPENDS_maven2-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-artifact-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-artifact-manager-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-model-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-monitor-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-plugin-descriptor-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-plugin-registry-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-profile-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-project-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-settings-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-toolchain-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven2-javadoc-2.2.1-59.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-artifact.sha256sum] = "c25d4876e275ef78532eb774ef945568ca2a6cd6ec8c03de29235c96ef3a8d33"
SRC_URI[maven-artifact-manager.sha256sum] = "7161e82cede7c20dfa82855fa8de5d899560206d70201ccf97ff118acf60a504"
SRC_URI[maven-model.sha256sum] = "aadff86144432bf5f9c72a1b5e60db28d775a009cce193b6e2ce8619059697f0"
SRC_URI[maven-monitor.sha256sum] = "6fbe3974bd52459fbcdf4ec3975598c40773aed5e45ff2c0c922d92d7eb5ac36"
SRC_URI[maven-plugin-descriptor.sha256sum] = "0061e75e186e42c7d1dbbca169b6a8cdd8f6e1b728912da42d542451858cd6e5"
SRC_URI[maven-plugin-registry.sha256sum] = "1b9f6da84220ea53c6daaa3742e99adfcdd1e4cc7bb9b74b2e5408aa8bf555e7"
SRC_URI[maven-profile.sha256sum] = "62535483b3d36e9d6baaf1cad5929c8f72dfcdc42e5d2a0ac754d3e0b2230523"
SRC_URI[maven-project.sha256sum] = "716a85c4accc290406c3bb437833285c6d4b527ab1d762e44f1c0a98a4e2b616"
SRC_URI[maven-settings.sha256sum] = "e74229a7f920de86b8971fcbf97135e6afab1d3353605a41c488e0b3aa451489"
SRC_URI[maven-toolchain.sha256sum] = "62213b975394df5c2ca3545c753f6299ce9c3d8e3518bd1fffca069e1556bb8c"
SRC_URI[maven2-javadoc.sha256sum] = "97e2ff36a95c9189ecf2dca508a53718d3f8575e5998be1061669e3a01c18651"
