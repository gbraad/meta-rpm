SUMMARY = "generated recipe based on jasper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "JasPer"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libjpeg-turbo pkgconfig-native"
RPM_SONAME_REQ_jasper-devel = "libjasper.so.4"
RPROVIDES_jasper-devel = "jasper-dev (= 2.0.14)"
RDEPENDS_jasper-devel = "jasper-libs libjpeg-turbo-devel pkgconf-pkg-config"
RPM_SONAME_PROV_jasper-libs = "libjasper.so.4"
RPM_SONAME_REQ_jasper-libs = "libc.so.6 libjpeg.so.62 libm.so.6"
RDEPENDS_jasper-libs = "glibc libjpeg-turbo"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/jasper-libs-2.0.14-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/jasper-devel-2.0.14-4.el8.x86_64.rpm \
          "

SRC_URI[jasper-devel.sha256sum] = "14bf8b2e45e6802f062e7d8051d91ec7ba674325092ab3774a55915e089e9325"
SRC_URI[jasper-libs.sha256sum] = "b659bd24d32ba0c1b578cfa367ff451f0cdafa1dbb9a803241d942c9cd7d78cc"
