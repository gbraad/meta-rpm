SUMMARY = "generated recipe based on libcroco srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libxml2 pkgconfig-native"
RPM_SONAME_PROV_libcroco = "libcroco-0.6.so.3"
RPM_SONAME_REQ_libcroco = "libc.so.6 libglib-2.0.so.0 libxml2.so.2"
RDEPENDS_libcroco = "glib2 glibc libxml2"
RPM_SONAME_REQ_libcroco-devel = "libcroco-0.6.so.3"
RPROVIDES_libcroco-devel = "libcroco-dev (= 0.6.12)"
RDEPENDS_libcroco-devel = "bash glib2-devel libcroco libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libcroco-0.6.12-4.el8_2.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libcroco-devel-0.6.12-4.el8_2.1.x86_64.rpm \
          "

SRC_URI[libcroco.sha256sum] = "87f2a4d80cf4f6a958f3662c6a382edefc32a5ad2c364a7f3c40337cf2b1e8ba"
SRC_URI[libcroco-devel.sha256sum] = "33f882c8513648a89058a281b37af6cb91938837311f4457c0159e153e3a188b"
