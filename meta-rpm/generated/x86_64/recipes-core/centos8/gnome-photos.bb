SUMMARY = "generated recipe based on gnome-photos srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & LGPL-2.0"
RPM_LICENSE = "GPLv3+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk babl cairo gdk-pixbuf gegl04 geocode-glib gfbgraph glib-2.0 gnome-online-accounts grilo gtk+3 json-glib libdazzle libgcc libgdata libgexiv2 libjpeg-turbo libpng libsoup-2.4 libxml2 pango pkgconfig-native rest tracker zlib"
RPM_SONAME_REQ_gnome-photos = "libatk-1.0.so.0 libbabl-0.1.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdazzle-1.0.so.0 libgcc_s.so.1 libgdata.so.22 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgegl-0.4.so.0 libgegl-npd-0.4.so libgeocode-glib.so.0 libgexiv2.so.2 libgfbgraph-0.2.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgoa-1.0.so.0 libgobject-2.0.so.0 libgrilo-0.3.so.0 libgtk-3.so.0 libjpeg.so.62 libjson-glib-1.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libpthread.so.0 librest-0.7.so.0 librt.so.1 libsoup-2.4.so.1 libtracker-control-2.0.so.0 libtracker-sparql-2.0.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_gnome-photos = "atk babl baobab cairo cairo-gobject dleyna-renderer gdk-pixbuf2 gegl04 geocode-glib gettext-libs gfbgraph glib2 glibc gnome-online-accounts gnome-online-miners gnome-settings-daemon grilo gtk3 json-glib libdazzle libgcc libgdata libgexiv2 libjpeg-turbo libpng libsoup libxml2 pango rest tracker tracker-miners zlib"
RDEPENDS_gnome-photos-tests = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-photos-3.28.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-photos-tests-3.28.1-1.el8.x86_64.rpm \
          "

SRC_URI[gnome-photos.sha256sum] = "3449e931237398554c5f5613a9ea6de28fc94c303f18f7ee41275232c9bfbe5a"
SRC_URI[gnome-photos-tests.sha256sum] = "305cc1b562b63f56a595f0cbedcdbbdbce35e5c1849c29971639f309349f7241"
