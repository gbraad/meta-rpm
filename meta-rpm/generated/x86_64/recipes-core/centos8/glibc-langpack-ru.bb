SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-ru = "locale-base-ru-ru (= 2.28) locale-base-ru-ru.koi8r (= 2.28) locale-base-ru-ru.utf8 (= 2.28) locale-base-ru-ua (= 2.28) locale-base-ru-ua.utf8 (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru (= 2.28) virtual-locale-ru-ru (= 2.28) virtual-locale-ru-ru.koi8r (= 2.28) virtual-locale-ru-ru.utf8 (= 2.28) virtual-locale-ru-ua (= 2.28) virtual-locale-ru-ua.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ru = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ru-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-ru.sha256sum] = "3fe2e69f825be18a410e71d6cadc90ce0f76c80daa2a034c2096f71d8100e31c"
