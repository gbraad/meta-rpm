SUMMARY = "generated recipe based on perl-Class-Data-Inheritable srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Data-Inheritable = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Class-Data-Inheritable-0.08-27.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Data-Inheritable.sha256sum] = "c7875018c5aec5f8a6f8f3e8076072b3c84bff4ef930cac692b95811c92bebe6"
