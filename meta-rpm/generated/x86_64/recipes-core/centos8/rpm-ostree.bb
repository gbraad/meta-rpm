SUMMARY = "generated recipe based on rpm-ostree srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "curl glib-2.0 gpgme json-c json-glib libarchive libcap libgcc libgpg-error libmodulemd librepo libsolv openssl ostree pkgconfig-native polkit rpm sqlite3 systemd-libs util-linux"
RPM_SONAME_REQ_rpm-ostree = "libarchive.so.13 libc.so.6 libcap.so.2 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libjson-c.so.4 libjson-glib-1.0.so.0 libm.so.6 libmodulemd.so.1 libostree-1.so.1 libpolkit-gobject-1.so.0 libpthread.so.0 librepo.so.0 librpm.so.8 librpmio.so.8 librpmostree-1.so.1 libsmartcols.so.1 libsolv.so.1 libsolvext.so.1 libsqlite3.so.0 libssl.so.1.1 libstdc++.so.6 libsystemd.so.0"
RDEPENDS_rpm-ostree = "bash bubblewrap fuse glib2 glibc gpgme json-c json-glib libarchive libcap libcurl libgcc libgpg-error libmodulemd1 librepo libsmartcols libsolv libstdc++ openssl-libs ostree ostree-libs polkit-libs rpm-libs rpm-ostree-libs sqlite-libs systemd-libs"
RPM_SONAME_PROV_rpm-ostree-libs = "librpmostree-1.so.1"
RPM_SONAME_REQ_rpm-ostree-libs = "ld-linux-x86-64.so.2 libarchive.so.13 libc.so.6 libcap.so.2 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libm.so.6 libostree-1.so.1 libpolkit-gobject-1.so.0 libpthread.so.0 librepo.so.0 librpm.so.8 librpmio.so.8 libsolv.so.1 libsystemd.so.0"
RDEPENDS_rpm-ostree-libs = "glib2 glibc json-glib libarchive libcap libgcc librepo libsolv ostree-libs polkit-libs rpm-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rpm-ostree-2019.6-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/rpm-ostree-libs-2019.6-8.el8.x86_64.rpm \
          "

SRC_URI[rpm-ostree.sha256sum] = "dd63929e8e9ef824bc15fad345bd6176b2101266ba550b8b17294085cba83b2c"
SRC_URI[rpm-ostree-libs.sha256sum] = "bf65d730ca2f93f72ce7fd7070d0153f335bd2ca8f8f636963d9a4d01f602c81"
