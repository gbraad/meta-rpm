SUMMARY = "generated recipe based on hunspell-mn srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mn = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-mn-0.20080709-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-mn.sha256sum] = "3bbef5700a4b1ce15a476e7ded49e1ceb16bcba584d00c327d184722324330a3"
