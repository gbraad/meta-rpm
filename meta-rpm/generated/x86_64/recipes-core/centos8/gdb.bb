SUMMARY = "generated recipe based on gdb srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GPL-3.0 & GPL-2.0 & GPL-2.0 & GPL-2.0 & LGPL-2.0 & LGPL-3.0 & BSD & CLOSED & GFDL-1.1"
RPM_LICENSE = "GPLv3+ and GPLv3+ with exceptions and GPLv2+ and GPLv2+ with exceptions and GPL+ and LGPLv2+ and LGPLv3+ and BSD and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "babeltrace expat guile libgcc libipt libselinux mpfr ncurses pkgconfig-native platform-python3 readline xz zlib"
RDEPENDS_gdb = "bash gdb-headless"
RPM_SONAME_PROV_gdb-gdbserver = "libinproctrace.so"
RPM_SONAME_REQ_gdb-gdbserver = "libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libselinux.so.1 libstdc++.so.6"
RDEPENDS_gdb-gdbserver = "glibc libgcc libselinux libstdc++"
RPM_SONAME_REQ_gdb-headless = "libbabeltrace-ctf.so.1 libbabeltrace.so.1 libc.so.6 libdl.so.2 libexpat.so.1 libgcc_s.so.1 libguile-2.0.so.22 libipt.so.1 liblzma.so.5 libm.so.6 libmpfr.so.4 libncursesw.so.6 libpthread.so.0 libpython3.6m.so.1.0 libreadline.so.7 libselinux.so.1 libstdc++.so.6 libtinfo.so.6 libutil.so.1 libz.so.1"
RDEPENDS_gdb-headless = "bash expat glibc guile libbabeltrace libgcc libipt libselinux libstdc++ mpfr ncurses-libs python3-libs readline xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gdb-8.2-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gdb-doc-8.2-11.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gdb-gdbserver-8.2-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gdb-headless-8.2-11.el8.x86_64.rpm \
          "

SRC_URI[gdb.sha256sum] = "fd3483a468ff376b9ba71db139a2f673c6e91c9eb792dc20d7d62699a0a2ef5d"
SRC_URI[gdb-doc.sha256sum] = "3e2f532cf9dcac9f35559bf821545ce1ef378a53be9a76042e28dd63511ae397"
SRC_URI[gdb-gdbserver.sha256sum] = "66680c789c4588784a3cb5fc2c70c83cfa1700b29f547aabec35208b18ca97b7"
SRC_URI[gdb-headless.sha256sum] = "1e4e76da5353a46ddc70cdcb17d1658c0a358b4e4dab1ceea163ba832b6bb94f"
