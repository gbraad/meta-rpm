SUMMARY = "generated recipe based on libexif srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libexif = "libexif.so.12"
RPM_SONAME_REQ_libexif = "libc.so.6 libm.so.6"
RDEPENDS_libexif = "glibc"
RPM_SONAME_REQ_libexif-devel = "libexif.so.12"
RPROVIDES_libexif-devel = "libexif-dev (= 0.6.21)"
RDEPENDS_libexif-devel = "libexif pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libexif-0.6.21-17.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libexif-devel-0.6.21-17.el8_2.x86_64.rpm \
          "

SRC_URI[libexif.sha256sum] = "b1a88e0494b613c4b0e5999bdc3d8fc50f65a7d1f002ef78c19c423ef0232149"
SRC_URI[libexif-devel.sha256sum] = "4912029cd2b2712323981b444e49b3f61c1fbd364962f2b5c274cee63ce122c9"
