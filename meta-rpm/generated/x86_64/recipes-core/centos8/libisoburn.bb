SUMMARY = "generated recipe based on libisoburn srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl libburn libisofs pkgconfig-native readline zlib"
RPM_SONAME_PROV_libisoburn = "libisoburn.so.1"
RPM_SONAME_REQ_libisoburn = "libacl.so.1 libburn.so.4 libc.so.6 libisofs.so.6 libpthread.so.0 libreadline.so.7 libz.so.1"
RDEPENDS_libisoburn = "glibc libacl libburn libisofs readline zlib"
RPM_SONAME_REQ_libisoburn-devel = "libisoburn.so.1"
RPROVIDES_libisoburn-devel = "libisoburn-dev (= 1.4.8)"
RDEPENDS_libisoburn-devel = "libisoburn pkgconf-pkg-config"
RPM_SONAME_REQ_xorriso = "libacl.so.1 libburn.so.4 libc.so.6 libisoburn.so.1 libisofs.so.6 libpthread.so.0 libreadline.so.7 libz.so.1"
RDEPENDS_xorriso = "bash chkconfig coreutils glibc info libacl libburn libisoburn libisofs readline zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libisoburn-1.4.8-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorriso-1.4.8-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libisoburn-devel-1.4.8-4.el8.x86_64.rpm \
          "

SRC_URI[libisoburn.sha256sum] = "7aa030310250b462d90895d8c04ce47695722d86f5470930fdf8bfba0570c4dc"
SRC_URI[libisoburn-devel.sha256sum] = "926dfb03cebbbd0cec886f6c7b33a9039fc9b81930652880d0978ae52782c161"
SRC_URI[xorriso.sha256sum] = "3a232d848da1ace286efef6c8c9cf0fcfab2c47dd58968ddb6a24718629a6220"
