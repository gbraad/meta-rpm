SUMMARY = "generated recipe based on plexus-resources srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-resources = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-containers-container-default plexus-utils"
RDEPENDS_plexus-resources-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-resources-1.0-0.23.a7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/plexus-resources-javadoc-1.0-0.23.a7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-resources.sha256sum] = "e1dc83c1269b00b24d89f9b0dc6ae0b873e6bcae53f7a8d8e1989f576b69c871"
SRC_URI[plexus-resources-javadoc.sha256sum] = "13a723ed5d531bcc17317dcb31ca0fd3ff377c57f1571708b8215337ca85ee2c"
