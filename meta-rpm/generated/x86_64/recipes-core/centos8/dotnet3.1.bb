SUMMARY = "generated recipe based on dotnet3.1 srpm"
DESCRIPTION = "Description"
LICENSE = "MIT & CLOSED & BSD"
RPM_LICENSE = "MIT and ASL 2.0 and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "curl krb5-libs libgcc lttng-ust openssl pkgconfig-native zlib"
RDEPENDS_aspnetcore-runtime-3.1 = "dotnet-runtime-3.1"
RDEPENDS_aspnetcore-targeting-pack-3.1 = "dotnet-host"
RDEPENDS_dotnet = "dotnet-sdk-3.1"
RPM_SONAME_PROV_dotnet-apphost-pack-3.1 = "libnethost.so"
RPM_SONAME_REQ_dotnet-apphost-pack-3.1 = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_dotnet-apphost-pack-3.1 = "dotnet-host glibc libgcc libstdc++"
RPM_SONAME_REQ_dotnet-host = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_dotnet-host = "glibc libgcc libstdc++"
RPM_SONAME_REQ_dotnet-hostfxr-3.1 = "ld-linux-x86-64.so.2 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_dotnet-hostfxr-3.1 = "dotnet-host glibc libgcc libstdc++"
RPM_SONAME_REQ_dotnet-runtime-3.1 = "ld-linux-x86-64.so.2 libc.so.6 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgssapi_krb5.so.2 liblttng-ust.so.0 libm.so.6 libpthread.so.0 librt.so.1 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_dotnet-runtime-3.1 = "dotnet-hostfxr-3.1 glibc krb5-libs libcurl libgcc libicu libstdc++ lttng-ust openssl-libs zlib"
RPM_SONAME_REQ_dotnet-sdk-3.1 = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_dotnet-sdk-3.1 = "aspnetcore-runtime-3.1 aspnetcore-targeting-pack-3.1 dotnet-apphost-pack-3.1 dotnet-runtime-3.1 dotnet-targeting-pack-3.1 dotnet-templates-3.1 glibc libgcc libstdc++ netstandard-targeting-pack-2.1"
RDEPENDS_dotnet-targeting-pack-3.1 = "dotnet-host"
RDEPENDS_dotnet-templates-3.1 = "dotnet-host"
RDEPENDS_netstandard-targeting-pack-2.1 = "dotnet-host"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/aspnetcore-runtime-3.1-3.1.7-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/aspnetcore-targeting-pack-3.1-3.1.7-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-3.1.107-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-apphost-pack-3.1-3.1.7-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-host-3.1.7-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-hostfxr-3.1-3.1.7-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-runtime-3.1-3.1.7-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-sdk-3.1-3.1.107-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-targeting-pack-3.1-3.1.7-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/dotnet-templates-3.1-3.1.107-1.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/netstandard-targeting-pack-2.1-3.1.107-1.el8_2.x86_64.rpm \
          "

SRC_URI[aspnetcore-runtime-3.1.sha256sum] = "eea6e8cdc8579c56ab94e5a8f29ea9a30801cd152766490d59394c29e50f2828"
SRC_URI[aspnetcore-targeting-pack-3.1.sha256sum] = "12b0440d699f808a3caf8431937f3dfb24654d77f6e91bdf7a08c82a1ea56d5f"
SRC_URI[dotnet.sha256sum] = "3195f27c0575ab94091a948e1f86dfa85aa07a284ee10fec1db49829f8df5039"
SRC_URI[dotnet-apphost-pack-3.1.sha256sum] = "79127071185576f8da36b76e6e77c2500785b35e132e09e06ef5d01bba0536d0"
SRC_URI[dotnet-host.sha256sum] = "4217036b1fdaf579bff2763115cb56c2514146f597a62576278d211e8b727762"
SRC_URI[dotnet-hostfxr-3.1.sha256sum] = "dd049cb1935c6d29a1e198e5b38d480eeb19bbc8c2eeb79f058b4011d8baadcb"
SRC_URI[dotnet-runtime-3.1.sha256sum] = "5f5f810c3a2e4d547adf470a97e33812f64008d889f3e572cbe7b6179cc92505"
SRC_URI[dotnet-sdk-3.1.sha256sum] = "71891d111f99284fdec01deb008a402b59cecc46d17678611095b54c5b3a1881"
SRC_URI[dotnet-targeting-pack-3.1.sha256sum] = "9797a060f3cdc1d6a6f611a377decf5c34663c1ddc39df71494141587662708a"
SRC_URI[dotnet-templates-3.1.sha256sum] = "d6b21c7aa752a715b695108f37e72e1cf9e944ac44a5fec2dffb8101927edab7"
SRC_URI[netstandard-targeting-pack-2.1.sha256sum] = "2853ee2dee81db9f18ed311f949de77a12171d2f76f89404c61f8e99784f8b25"
