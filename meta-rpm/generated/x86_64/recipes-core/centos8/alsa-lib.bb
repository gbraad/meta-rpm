SUMMARY = "generated recipe based on alsa-lib srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_alsa-lib = "libasound.so.2 libatopology.so.2"
RPM_SONAME_REQ_alsa-lib = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_alsa-lib = "coreutils glibc"
RPM_SONAME_REQ_alsa-lib-devel = "libasound.so.2 libatopology.so.2"
RPROVIDES_alsa-lib-devel = "alsa-lib-dev (= 1.2.1.2)"
RDEPENDS_alsa-lib-devel = "alsa-lib pkgconf-pkg-config"
RDEPENDS_alsa-ucm = "alsa-lib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-lib-1.2.1.2-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-lib-devel-1.2.1.2-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/alsa-ucm-1.2.1.2-3.el8.noarch.rpm \
          "

SRC_URI[alsa-lib.sha256sum] = "92f7444be324be0dcb1bcbbebcba2929d44263ee6dc8a1c69ad98c9abb5a2aa9"
SRC_URI[alsa-lib-devel.sha256sum] = "f99199809fc3de2eca49f1e7b72e1cb9d397ef6878f764e392d2a2265b61ec55"
SRC_URI[alsa-ucm.sha256sum] = "daba0567491b6fe0ab3714fbb9fa0024fdfe40206e4b228e1869274650867298"
