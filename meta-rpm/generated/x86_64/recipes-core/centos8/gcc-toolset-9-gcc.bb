SUMMARY = "generated recipe based on gcc-toolset-9-gcc srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GPL-3.0 & GPL-2.0 & LGPL-2.0 & BSD"
RPM_LICENSE = "GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gmp libgcc libmpc mpfr pkgconfig-native zlib"
RPM_SONAME_PROV_gcc-toolset-9-gcc = "liblto_plugin.so.0"
RPM_SONAME_REQ_gcc-toolset-9-gcc = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_gcc-toolset-9-gcc = "bash binutils gcc-toolset-9-runtime glibc glibc-devel gmp libgcc libgomp libmpc mpfr zlib"
RPM_SONAME_REQ_gcc-toolset-9-gcc-c++ = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_gcc-toolset-9-gcc-c++ = "gcc-toolset-9-gcc gcc-toolset-9-libstdc++-devel gcc-toolset-9-runtime glibc gmp libmpc libstdc++ mpfr zlib"
RPM_SONAME_PROV_gcc-toolset-9-gcc-gdb-plugin = "libcc1.so.0 libcc1plugin.so.0 libcp1plugin.so.0"
RPM_SONAME_REQ_gcc-toolset-9-gcc-gdb-plugin = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-gcc-gdb-plugin = "gcc-toolset-9-gcc gcc-toolset-9-runtime glibc libgcc libstdc++"
RPM_SONAME_REQ_gcc-toolset-9-gcc-gfortran = "ld-linux-x86-64.so.2 libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpc.so.3 libmpfr.so.4 libz.so.1"
RDEPENDS_gcc-toolset-9-gcc-gfortran = "bash gcc-toolset-9-gcc gcc-toolset-9-libquadmath-devel gcc-toolset-9-runtime glibc gmp libgfortran libmpc mpfr zlib"
RPM_SONAME_REQ_gcc-toolset-9-gcc-plugin-devel = "libc.so.6 libm.so.6"
RPROVIDES_gcc-toolset-9-gcc-plugin-devel = "gcc-toolset-9-gcc-plugin-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-gcc-plugin-devel = "gcc-toolset-9-gcc gcc-toolset-9-runtime glibc gmp-devel libmpc-devel mpfr-devel"
RPROVIDES_gcc-toolset-9-libasan-devel = "gcc-toolset-9-libasan-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libasan-devel = "gcc-toolset-9-runtime libasan"
RPROVIDES_gcc-toolset-9-libatomic-devel = "gcc-toolset-9-libatomic-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libatomic-devel = "gcc-toolset-9-runtime libatomic"
RPROVIDES_gcc-toolset-9-libitm-devel = "gcc-toolset-9-libitm-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libitm-devel = "gcc-toolset-9-gcc gcc-toolset-9-runtime libitm"
RPROVIDES_gcc-toolset-9-liblsan-devel = "gcc-toolset-9-liblsan-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-liblsan-devel = "gcc-toolset-9-runtime liblsan"
RPROVIDES_gcc-toolset-9-libquadmath-devel = "gcc-toolset-9-libquadmath-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libquadmath-devel = "gcc-toolset-9-gcc gcc-toolset-9-runtime libquadmath"
RPROVIDES_gcc-toolset-9-libstdc++-devel = "gcc-toolset-9-libstdc++-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libstdc++-devel = "gcc-toolset-9-runtime libstdc++"
RDEPENDS_gcc-toolset-9-libstdc++-docs = "gcc-toolset-9-runtime"
RPROVIDES_gcc-toolset-9-libtsan-devel = "gcc-toolset-9-libtsan-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libtsan-devel = "gcc-toolset-9-runtime libtsan"
RPROVIDES_gcc-toolset-9-libubsan-devel = "gcc-toolset-9-libubsan-dev (= 9.2.1)"
RDEPENDS_gcc-toolset-9-libubsan-devel = "gcc-toolset-9-runtime libubsan"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-gcc-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-gcc-c++-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-gcc-gdb-plugin-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-gcc-gfortran-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-libasan-devel-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-libatomic-devel-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-libitm-devel-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-liblsan-devel-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-libquadmath-devel-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-libstdc++-devel-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-libstdc++-docs-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-libtsan-devel-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-libubsan-devel-9.2.1-2.2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gcc-toolset-9-gcc-plugin-devel-9.2.1-2.2.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-gcc.sha256sum] = "ed73e173f0d49f2334cf9ede7e401b08dc9f2146c521711292498b495b9529e0"
SRC_URI[gcc-toolset-9-gcc-c++.sha256sum] = "13a504ea83d4b4496b740c2a1baede3f730079daf74a886ace8bd66d871016f3"
SRC_URI[gcc-toolset-9-gcc-gdb-plugin.sha256sum] = "1d289d18f9b167133f730f83e9b82ce92ec45b5c8c0f5ebdecc36efcd325d919"
SRC_URI[gcc-toolset-9-gcc-gfortran.sha256sum] = "b0322c71e74e7e29d246fe454a72f94f012a11a0450e6d064c00128b2f0cfffc"
SRC_URI[gcc-toolset-9-gcc-plugin-devel.sha256sum] = "3d9c3bfff446996a7f23b080397463dd7f7701905e43276a9f33042b4dcc415c"
SRC_URI[gcc-toolset-9-libasan-devel.sha256sum] = "3b766006dc01c485d7745c97680127abe99c2463c093aff270197ab3527d7257"
SRC_URI[gcc-toolset-9-libatomic-devel.sha256sum] = "4d29a476649e90c9639caf1f9fbac26f6ad689fa2e754923ebf5bbfc7c495d79"
SRC_URI[gcc-toolset-9-libitm-devel.sha256sum] = "3e632310eb8d6234be7c1105bd7b5ed515dd25844109c2aeb8f5f7ca139fdc1b"
SRC_URI[gcc-toolset-9-liblsan-devel.sha256sum] = "34001992bd68ffd26b65e9251746a59c7bc3890a13ab10abe0afa65a2705460d"
SRC_URI[gcc-toolset-9-libquadmath-devel.sha256sum] = "1dee23a0323b992b05c2013243cd5101c1c03c6e6c053b5b7923fc4c153389d8"
SRC_URI[gcc-toolset-9-libstdc++-devel.sha256sum] = "e858f8f33163c12c85fb33d9f4e7b572555ebd7dc1da94d1be83ed6cbeb2fb68"
SRC_URI[gcc-toolset-9-libstdc++-docs.sha256sum] = "00ab8d684dc3a1605a766d5f1be7bc311b01a9910f2cc92797e2b6b6b62d0a12"
SRC_URI[gcc-toolset-9-libtsan-devel.sha256sum] = "8c84ed60947714549141f7a48164f75eb6cba468f3f1c4bec3984411a84d4622"
SRC_URI[gcc-toolset-9-libubsan-devel.sha256sum] = "79ab31ac5e377cce42e12a093653fa5c625ad4b609e54567b33ff36cdafe79b0"
