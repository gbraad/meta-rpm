SUMMARY = "generated recipe based on junit srpm"
DESCRIPTION = "Description"
LICENSE = "EPL-1.0"
RPM_LICENSE = "EPL-1.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_junit = "hamcrest-core java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_junit-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/junit-4.12-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/junit-javadoc-4.12-9.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/junit-manual-4.12-9.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[junit.sha256sum] = "71d689bfa96d24f660ec9c6e3809c9604063c690f5a13d18bd9cf887cd6b00b4"
SRC_URI[junit-javadoc.sha256sum] = "ff0a4dd44f9c12112f81cf89e79b9827b84c1b0b1b930786cfd619dba94c6040"
SRC_URI[junit-manual.sha256sum] = "323461a46040d0326e9970fa7b318fcd15d693ba45a77fdfb5c35c5c97149760"
