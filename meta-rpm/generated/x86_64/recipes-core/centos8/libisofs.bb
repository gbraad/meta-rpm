SUMMARY = "generated recipe based on libisofs srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl pkgconfig-native zlib"
RPM_SONAME_PROV_libisofs = "libisofs.so.6"
RPM_SONAME_REQ_libisofs = "libacl.so.1 libc.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_libisofs = "glibc libacl zlib"
RPM_SONAME_REQ_libisofs-devel = "libisofs.so.6"
RPROVIDES_libisofs-devel = "libisofs-dev (= 1.4.8)"
RDEPENDS_libisofs-devel = "libisofs pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libisofs-1.4.8-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libisofs-devel-1.4.8-3.el8.x86_64.rpm \
          "

SRC_URI[libisofs.sha256sum] = "66b7bcc256b62736f7b3d33fa65c6a91a17e08c61484a7c3748f4f86b4589bc7"
SRC_URI[libisofs-devel.sha256sum] = "25c56b9f9fdcdc6172a07d7ec013c416f3e421e89caaa09ff8d965fbe52d1571"
