SUMMARY = "generated recipe based on libyami srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libva libx11 pkgconfig-native"
RPM_SONAME_PROV_libyami = "libyami.so.1"
RPM_SONAME_REQ_libyami = "libX11.so.6 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libva-drm.so.2 libva-x11.so.2 libva.so.2"
RDEPENDS_libyami = "glibc libX11 libX11-devel libgcc libstdc++ libva libva-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libyami-1.3.1-1.el8.x86_64.rpm \
          "

SRC_URI[libyami.sha256sum] = "555504fba7d4e79730a9214832e839f3f2e90f2901d354067a0ee75240b19e50"
