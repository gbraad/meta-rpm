SUMMARY = "generated recipe based on libappstream-glib srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gdk-pixbuf glib-2.0 libarchive libgcc libsoup-2.4 libstemmer libuuid pkgconfig-native"
RPM_SONAME_PROV_libappstream-glib = "libappstream-glib.so.8"
RPM_SONAME_REQ_libappstream-glib = "libarchive.so.13 libc.so.6 libgcc_s.so.1 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libsoup-2.4.so.1 libstemmer.so.0 libuuid.so.1"
RDEPENDS_libappstream-glib = "gdk-pixbuf2 glib2 glibc json-glib libarchive libgcc libsoup libstemmer libuuid"
RPM_SONAME_REQ_libappstream-glib-devel = "libappstream-glib.so.8"
RPROVIDES_libappstream-glib-devel = "libappstream-glib-dev (= 0.7.14)"
RDEPENDS_libappstream-glib-devel = "gdk-pixbuf2-devel glib2-devel libappstream-glib libarchive-devel libuuid-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libappstream-glib-0.7.14-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/libappstream-glib-devel-0.7.14-3.el8.x86_64.rpm \
          "

SRC_URI[libappstream-glib.sha256sum] = "230820d3f9dbaa9dcd013836f4ba56d0514946bc05cac7727ef2aaff3a3dde26"
SRC_URI[libappstream-glib-devel.sha256sum] = "e23cd3d3ad849284bc405e892d731fb8a5e0b8d54a436f6570a803a43f9be596"
