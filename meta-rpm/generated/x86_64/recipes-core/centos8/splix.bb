SUMMARY = "generated recipe based on splix srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cups-libs e2fsprogs jbigkit krb5-libs libgcc libxcrypt pkgconfig-native zlib"
RPM_SONAME_REQ_splix = "libc.so.6 libcom_err.so.2 libcrypt.so.1 libcups.so.2 libcupsimage.so.2 libgcc_s.so.1 libgssapi_krb5.so.2 libjbig85.so.2.1 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_splix = "cups cups-libs glibc jbigkit-libs krb5-libs libcom_err libgcc libstdc++ libxcrypt zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/splix-2.0.1-0.36.20130902svn.el8.x86_64.rpm \
          "

SRC_URI[splix.sha256sum] = "69ee3b1d129d66599d5a86b1c5b7b6ee6fad3c0f16ad762765875200c441ec37"
