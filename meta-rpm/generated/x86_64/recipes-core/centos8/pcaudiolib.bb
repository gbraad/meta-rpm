SUMMARY = "generated recipe based on pcaudiolib srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib pkgconfig-native pulseaudio"
RPM_SONAME_PROV_pcaudiolib = "libpcaudio.so.0"
RPM_SONAME_REQ_pcaudiolib = "libasound.so.2 libc.so.6 libpulse-simple.so.0 libpulse.so.0"
RDEPENDS_pcaudiolib = "alsa-lib glibc pulseaudio-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/pcaudiolib-1.1-2.el8.x86_64.rpm \
          "

SRC_URI[pcaudiolib.sha256sum] = "16a81cded9109ccf06b70ae7d6b716b745ba8d843f8041c42836fe6fc45aa293"
