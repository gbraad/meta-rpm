SUMMARY = "generated recipe based on gnome-initial-setup srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "accountsservice atk cairo cheese clutter cogl e2fsprogs fontconfig freetype gdk-pixbuf gdm geoclue2 geocode-glib glib-2.0 gnome-desktop3 gnome-online-accounts gstreamer1.0 gtk+3 ibus json-glib krb5-libs libdrm libgcc libglvnd libgweather libpwquality libsecret libsoup-2.4 libx11 libxcomposite libxcrypt libxdamage libxext libxfixes libxi libxkbcommon libxml2 libxrandr mesa network-manager-applet networkmanager packagekit pango pkgconfig-native polkit rest sqlite3 wayland webkit2gtk3"
RPM_SONAME_REQ_gnome-initial-setup = "libEGL.so.1 libX11.so.6 libXcomposite.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXrandr.so.2 libaccountsservice.so.0 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcheese-gtk.so.25 libcheese.so.8 libclutter-1.0.so.0 libcogl-pango.so.20 libcogl-path.so.20 libcogl.so.20 libcom_err.so.2 libcrypt.so.1 libdrm.so.2 libfontconfig.so.1 libfreetype.so.6 libgbm.so.1 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgdm.so.1 libgeoclue-2.so.0 libgeocode-glib.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnome-desktop-3.so.17 libgoa-1.0.so.0 libgoa-backend-1.0.so.1 libgobject-2.0.so.0 libgstreamer-1.0.so.0 libgtk-3.so.0 libgweather-3.so.15 libibus-1.0.so.5 libjavascriptcoregtk-4.0.so.18 libjson-glib-1.0.so.0 libk5crypto.so.3 libkrb5.so.3 libm.so.6 libnm.so.0 libnma.so.0 libpackagekit-glib2.so.18 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libpwquality.so.1 librest-0.7.so.0 libsecret-1.so.0 libsoup-2.4.so.1 libsqlite3.so.0 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libwayland-server.so.0 libwebkit2gtk-4.0.so.37 libxkbcommon.so.0 libxml2.so.2"
RDEPENDS_gnome-initial-setup = "NetworkManager-libnm PackageKit-glib accountsservice-libs atk bash cairo cairo-gobject cheese-libs clutter cogl fontconfig freetype gdk-pixbuf2 gdm geoclue2-libs geocode-glib glib2 glibc gnome-desktop3 gnome-online-accounts gstreamer1 gtk3 ibus-libs json-glib krb5-libs libX11 libXcomposite libXdamage libXext libXfixes libXi libXrandr libcom_err libdrm libgcc libglvnd-egl libgnomekbd libgweather libnma libpwquality libsecret libsoup libwayland-client libwayland-cursor libwayland-egl libwayland-server libxcrypt libxkbcommon libxml2 mesa-libgbm pango polkit polkit-libs rest shadow-utils sqlite-libs webkit2gtk3 webkit2gtk3-jsc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-initial-setup-3.28.0-8.el8.x86_64.rpm \
          "

SRC_URI[gnome-initial-setup.sha256sum] = "07907fc3dad61df3d280710b0786c97d0d7025fa4995d69bad8724e5cc2bc8c9"
