SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-el = "locale-base-el-cy (= 2.28) locale-base-el-cy.utf8 (= 2.28) locale-base-el-gr (= 2.28) locale-base-el-gr.utf8 (= 2.28) locale-base-el-gr@euro (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el-cy (= 2.28) virtual-locale-el-cy.utf8 (= 2.28) virtual-locale-el-gr (= 2.28) virtual-locale-el-gr.utf8 (= 2.28) virtual-locale-el-gr@euro (= 2.28)"
RDEPENDS_glibc-langpack-el = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-el-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-el.sha256sum] = "98a9e500a13979cc6856aab213a011f53e37466b75a3ce72f6e743a468b9d38a"
