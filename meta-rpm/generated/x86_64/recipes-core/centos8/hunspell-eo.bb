SUMMARY = "generated recipe based on hunspell-eo srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-eo = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-eo-1.0-0.15.dev.el8.noarch.rpm \
          "

SRC_URI[hunspell-eo.sha256sum] = "03fe42828fa9e5ab63fb24704cea27dae115c328c09d91a8cbbc9e63730fbf7b"
