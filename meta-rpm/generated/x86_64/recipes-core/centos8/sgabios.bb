SUMMARY = "generated recipe based on sgabios srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_sgabios = "sgabios-bin"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sgabios-0.20170427git-3.module_el8.2.0+320+13f867d7.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sgabios-bin-0.20170427git-3.module_el8.2.0+320+13f867d7.noarch.rpm \
          "

SRC_URI[sgabios.sha256sum] = "86373294cc4e759d3a511b3f4c063dbaf39af89a74528f1c94789533f374ff00"
SRC_URI[sgabios-bin.sha256sum] = "36701dc06633632f14af0450e481909226547f7c321d6151b5dc515b5f227242"
