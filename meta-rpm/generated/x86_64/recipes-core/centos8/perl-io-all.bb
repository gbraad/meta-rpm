SUMMARY = "generated recipe based on perl-IO-All srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-All = "perl-Carp perl-File-MimeInfo perl-File-Path perl-File-ReadBackwards perl-IO perl-PathTools perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-IO-All-0.87-6.el8.noarch.rpm \
          "

SRC_URI[perl-IO-All.sha256sum] = "989810bfadb0bc1fc15dd000d7002af357b2dc8044708feca0ecf0fd8c9adc3d"
