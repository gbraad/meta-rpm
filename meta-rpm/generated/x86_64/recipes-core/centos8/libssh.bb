SUMMARY = "generated recipe based on libssh srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "e2fsprogs krb5-libs openssl pkgconfig-native zlib"
RPM_SONAME_PROV_libssh = "libssh.so.4 libssh_threads.so libssh_threads.so.4"
RPM_SONAME_REQ_libssh = "ld-linux-x86-64.so.2 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 librt.so.1 libz.so.1"
RDEPENDS_libssh = "crypto-policies glibc krb5-libs libcom_err libssh-config openssl-libs zlib"
RPM_SONAME_REQ_libssh-devel = "libssh.so.4"
RPROVIDES_libssh-devel = "libssh-dev (= 0.9.0)"
RDEPENDS_libssh-devel = "cmake-filesystem libssh pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libssh-devel-0.9.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libssh-0.9.0-4.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libssh-config-0.9.0-4.el8.noarch.rpm \
          "

SRC_URI[libssh.sha256sum] = "689f6b3b25b7e2db8ab581914eb906f0b42f114a9f18b0401506732dff9257f4"
SRC_URI[libssh-config.sha256sum] = "de84d1439aba91eb9a6de521a0beb3bc6cbf7686f56a622aa289c398b2b0a28f"
SRC_URI[libssh-devel.sha256sum] = "55aa9aad7f9cd8101e861397086d4cf82f6726c7df79434605a04f30245e9e78"
