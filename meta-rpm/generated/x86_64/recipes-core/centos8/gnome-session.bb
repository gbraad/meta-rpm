SUMMARY = "generated recipe based on gnome-session srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 gnome-desktop3 gtk+3 json-glib libepoxy libgcc libglvnd libice libsm libx11 libxcomposite pkgconfig-native systemd-libs"
RPM_SONAME_REQ_gnome-session = "libEGL.so.1 libGL.so.1 libGLESv2.so.2 libICE.so.6 libSM.so.6 libX11.so.6 libXcomposite.so.1 libc.so.6 libepoxy.so.0 libgcc_s.so.1 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgtk-3.so.0 libjson-glib-1.0.so.0 libsystemd.so.0"
RDEPENDS_gnome-session = "bash centos-backgrounds centos-logos dbus-x11 dconf glib2 glibc gnome-control-center-filesystem gnome-desktop3 gsettings-desktop-schemas gtk3 json-glib libICE libSM libX11 libXcomposite libepoxy libgcc libglvnd-egl libglvnd-gles libglvnd-glx systemd-libs"
RDEPENDS_gnome-session-wayland-session = "gnome-session xorg-x11-server-Xwayland"
RDEPENDS_gnome-session-xsession = "gnome-session"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-session-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-session-wayland-session-3.28.1-8.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnome-session-xsession-3.28.1-8.el8.x86_64.rpm \
          "

SRC_URI[gnome-session.sha256sum] = "20779749b5588028439fa01c0edb5358d1d30953f9af0cefa246673e1f3f4f4b"
SRC_URI[gnome-session-wayland-session.sha256sum] = "3b9f66a6ac92860347a083e3d8dcae578be7ac068c559d5b2ad61b67adfff549"
SRC_URI[gnome-session-xsession.sha256sum] = "7ac049276058c3cc939735f7bacc81e74371104c786f219ad7265a71284f6549"
