SUMMARY = "generated recipe based on libtasn1 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libtasn1 = "libtasn1.so.6"
RPM_SONAME_REQ_libtasn1 = "libc.so.6"
RDEPENDS_libtasn1 = "glibc"
RPM_SONAME_REQ_libtasn1-devel = "libtasn1.so.6"
RPROVIDES_libtasn1-devel = "libtasn1-dev (= 4.13)"
RDEPENDS_libtasn1-devel = "bash info libtasn1 pkgconf-pkg-config"
RPM_SONAME_REQ_libtasn1-tools = "libc.so.6 libtasn1.so.6"
RDEPENDS_libtasn1-tools = "glibc libtasn1"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libtasn1-devel-4.13-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libtasn1-tools-4.13-3.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/libtasn1-4.13-3.el8.x86_64.rpm \
          "

SRC_URI[libtasn1.sha256sum] = "e8d9697a8914226a2d3ed5a4523b85e8e70ac09cf90aae05395e6faee9858534"
SRC_URI[libtasn1-devel.sha256sum] = "b07a871eb9fbc8f24c4335c0bbf89c58837eac0d502cf7094fb8dd5997af9de1"
SRC_URI[libtasn1-tools.sha256sum] = "ef6bb7c3a4bcd36c8888d8bdf726498a837e2e0992ddd847aff1c6f9239bc10c"
