SUMMARY = "generated recipe based on python-cups srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cups-libs pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-cups = "libc.so.6 libcups.so.2 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-cups = "cups-libs glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-cups-1.9.72-21.el8.0.1.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/python-cups-doc-1.9.72-21.el8.0.1.x86_64.rpm \
          "

SRC_URI[python-cups-doc.sha256sum] = "f407740b0532c4e90b188afe192b0e7413005318b1d5d931639509dc52041c14"
SRC_URI[python3-cups.sha256sum] = "4c3ff5efd2e48d2dc230201e6442c667290da8a743ec51edec11f169ce7bb941"
