SUMMARY = "generated recipe based on libfonts srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & CLOSED"
RPM_LICENSE = "LGPLv2 and UCD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_libfonts = "java-1.8.0-openjdk-headless javapackages-tools libloader"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libfonts-1.1.3-21.el8.noarch.rpm \
          "

SRC_URI[libfonts.sha256sum] = "0db6b2c2baf4de7e4978b8e79dc0d73692999412ee00c441cec61c165db6e0db"
