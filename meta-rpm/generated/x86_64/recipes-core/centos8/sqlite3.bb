SUMMARY = "generated recipe based on sqlite srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native readline zlib"
RPM_SONAME_REQ_lemon = "libc.so.6"
RDEPENDS_lemon = "glibc"
RPM_SONAME_REQ_sqlite = "libc.so.6 libdl.so.2 libm.so.6 libncurses.so.6 libpthread.so.0 libreadline.so.7 libtinfo.so.6 libz.so.1"
RDEPENDS_sqlite = "glibc ncurses-libs readline sqlite-libs zlib"
RPM_SONAME_REQ_sqlite-devel = "libsqlite3.so.0"
RPROVIDES_sqlite-devel = "sqlite-dev (= 3.26.0)"
RDEPENDS_sqlite-devel = "pkgconf-pkg-config sqlite sqlite-libs"
RPM_SONAME_PROV_sqlite-libs = "libsqlite3.so.0"
RPM_SONAME_REQ_sqlite-libs = "libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_sqlite-libs = "glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lemon-3.26.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sqlite-3.26.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sqlite-devel-3.26.0-6.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sqlite-doc-3.26.0-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sqlite-libs-3.26.0-6.el8.x86_64.rpm \
          "

SRC_URI[lemon.sha256sum] = "c927bd49922c34bc6c8c34593bf793af9e38c53781ef72bfc25cbb42bce50bf2"
SRC_URI[sqlite.sha256sum] = "83ca2d0caf2e7aecf3cfb3253e37320095518954246567ff1647b20577713abf"
SRC_URI[sqlite-devel.sha256sum] = "b5a67a63747dc011c21c7c71ade8b2e294ab7403610bb6afe93234a32fe5648d"
SRC_URI[sqlite-doc.sha256sum] = "c83764b7de6eac97675c533dd6220028ceed2d9ceb8f68da2999ad4a56afbb19"
SRC_URI[sqlite-libs.sha256sum] = "b537558593911388d78f4169136147c23c34a5acef3c6273409c30504ccdac9f"
