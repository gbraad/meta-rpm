SUMMARY = "generated recipe based on gedit srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GFDL-1.1"
RPM_LICENSE = "GPLv2+ and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo enchant2 gdk-pixbuf glib-2.0 gobject-introspection gspell gtk+3 gtksourceview3 libpeas libx11 libxml2 pango pkgconfig-native"
RPM_SONAME_PROV_gedit = "libdocinfo.so libfilebrowser.so libgedit.so libmodelines.so libsort.so libspell.so libtime.so"
RPM_SONAME_REQ_gedit = "libX11.so.6 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libenchant-2.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgspell-1.so.2 libgtk-3.so.0 libgtksourceview-3.0.so.1 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0 libxml2.so.2"
RDEPENDS_gedit = "atk bash cairo cairo-gobject desktop-file-utils enchant2 gdk-pixbuf2 glib2 glibc gobject-introspection gsettings-desktop-schemas gspell gtk3 gtksourceview3 gvfs libX11 libpeas libpeas-gtk libpeas-loader-python3 libxml2 pango platform-python python3-gobject zenity"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gedit-3.28.1-3.el8.x86_64.rpm \
          "

SRC_URI[gedit.sha256sum] = "684389f329026d8b6a10bdedaddf6b1a29da052d105262f4605bc97d8ed67546"
