SUMMARY = "generated recipe based on poppler srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | GPL-3.0) & GPL-2.0 & LGPL-2.0 & MIT"
RPM_LICENSE = "(GPLv2 or GPLv3) and GPLv2+ and LGPLv2+ and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo fontconfig freetype glib-2.0 lcms2 libgcc libjpeg-turbo libpng nspr nss openjpeg2 pkgconfig-native qt5-qtbase tiff"
RPM_SONAME_PROV_poppler = "libpoppler.so.78"
RPM_SONAME_REQ_poppler = "libc.so.6 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libjpeg.so.62 liblcms2.so.2 libm.so.6 libnspr4.so libnss3.so libopenjp2.so.7 libpng16.so.16 libpthread.so.0 libsmime3.so libstdc++.so.6 libtiff.so.5"
RDEPENDS_poppler = "fontconfig freetype glibc lcms2 libgcc libjpeg-turbo libpng libstdc++ libtiff nspr nss openjpeg2 poppler-data"
RPM_SONAME_PROV_poppler-cpp = "libpoppler-cpp.so.0"
RPM_SONAME_REQ_poppler-cpp = "libc.so.6 libgcc_s.so.1 libpoppler.so.78 libstdc++.so.6"
RDEPENDS_poppler-cpp = "glibc libgcc libstdc++ poppler"
RPM_SONAME_REQ_poppler-cpp-devel = "libpoppler-cpp.so.0"
RPROVIDES_poppler-cpp-devel = "poppler-cpp-dev (= 0.66.0)"
RDEPENDS_poppler-cpp-devel = "pkgconf-pkg-config poppler-cpp poppler-devel"
RPM_SONAME_REQ_poppler-devel = "libpoppler.so.78"
RPROVIDES_poppler-devel = "poppler-dev (= 0.66.0)"
RDEPENDS_poppler-devel = "pkgconf-pkg-config poppler"
RPM_SONAME_PROV_poppler-glib = "libpoppler-glib.so.8"
RPM_SONAME_REQ_poppler-glib = "libc.so.6 libcairo.so.2 libfreetype.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpoppler.so.78 libpthread.so.0 libstdc++.so.6"
RDEPENDS_poppler-glib = "cairo freetype glib2 glibc libgcc libstdc++ poppler"
RPM_SONAME_REQ_poppler-glib-devel = "libpoppler-glib.so.8"
RPROVIDES_poppler-glib-devel = "poppler-glib-dev (= 0.66.0)"
RDEPENDS_poppler-glib-devel = "cairo-devel glib2-devel pkgconf-pkg-config poppler-devel poppler-glib"
RPM_SONAME_PROV_poppler-qt5 = "libpoppler-qt5.so.1"
RPM_SONAME_REQ_poppler-qt5 = "libQt5Core.so.5 libQt5Gui.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libpoppler.so.78 libstdc++.so.6"
RDEPENDS_poppler-qt5 = "glibc libgcc libstdc++ poppler qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_REQ_poppler-qt5-devel = "libpoppler-qt5.so.1"
RPROVIDES_poppler-qt5-devel = "poppler-qt5-dev (= 0.66.0)"
RDEPENDS_poppler-qt5-devel = "pkgconf-pkg-config poppler-devel poppler-qt5 qt5-qtbase-devel"
RPM_SONAME_REQ_poppler-utils = "libc.so.6 libcairo.so.2 libfreetype.so.6 libgcc_s.so.1 liblcms2.so.2 libm.so.6 libpoppler.so.78 libpthread.so.0 libstdc++.so.6"
RDEPENDS_poppler-utils = "cairo freetype glibc lcms2 libgcc libstdc++ poppler"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/poppler-0.66.0-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/poppler-glib-0.66.0-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/poppler-utils-0.66.0-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/poppler-cpp-0.66.0-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/poppler-cpp-devel-0.66.0-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/poppler-devel-0.66.0-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/poppler-glib-devel-0.66.0-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/poppler-qt5-0.66.0-26.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/poppler-qt5-devel-0.66.0-26.el8.x86_64.rpm \
          "

SRC_URI[poppler.sha256sum] = "050101c7c1cdc1098946c42a02ec1551042afe19bd37e231e7fd61d73e368455"
SRC_URI[poppler-cpp.sha256sum] = "97c33c9b1176262dbc815978129fa1a312f4c7d50ccb14c339f41533db18e2a7"
SRC_URI[poppler-cpp-devel.sha256sum] = "dcaa1af79326ba8f6edc532bb42f260838afbfa2330de66cec2c4920b73318c4"
SRC_URI[poppler-devel.sha256sum] = "b72fa8848ebf41be9f2eace6834f1a36ffa220111b51957ca6fb7c44fd8de595"
SRC_URI[poppler-glib.sha256sum] = "db3935b0bd058f97d926133ef7c8dd0991de2c0b2e745b1ea4d9d49f31fd16f9"
SRC_URI[poppler-glib-devel.sha256sum] = "561a2fde4f9e802deeb8f8ac4ca28abc6744e202ec3f468e2ea1711e55558f8e"
SRC_URI[poppler-qt5.sha256sum] = "11c7de861230e4a8e79efabae31c395a06c6759eaf58dadb1e6de8b88ea6fc1d"
SRC_URI[poppler-qt5-devel.sha256sum] = "990f70549244738e3eb56928922d0ea54c3e2bc703c69e081c90221b8e1a75ec"
SRC_URI[poppler-utils.sha256sum] = "3cc3eca17b79efd010376e4aef82e8912b2477d6edf2de55a80746d267a25ba9"
