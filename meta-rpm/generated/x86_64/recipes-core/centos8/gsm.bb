SUMMARY = "generated recipe based on gsm srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_gsm = "libgsm.so.1"
RPM_SONAME_REQ_gsm = "libc.so.6"
RDEPENDS_gsm = "glibc"
RPM_SONAME_REQ_gsm-devel = "libgsm.so.1"
RPROVIDES_gsm-devel = "gsm-dev (= 1.0.17)"
RDEPENDS_gsm-devel = "gsm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gsm-1.0.17-5.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gsm-devel-1.0.17-5.el8.x86_64.rpm \
          "

SRC_URI[gsm.sha256sum] = "46176d2f6ca0a6b48719c1b2fc8c26c23687f854e03d6cd377ae7758d3f71245"
SRC_URI[gsm-devel.sha256sum] = "c11c0075ddef5595aef884455bead782c299882ef8e97f6a81e151ab6e76888d"
