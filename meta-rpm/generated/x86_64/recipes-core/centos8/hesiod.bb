SUMMARY = "generated recipe based on hesiod srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libidn pkgconfig-native"
RPM_SONAME_PROV_hesiod = "libhesiod.so.0"
RPM_SONAME_REQ_hesiod = "libc.so.6 libidn.so.11 libresolv.so.2"
RDEPENDS_hesiod = "glibc libidn"
RPM_SONAME_REQ_hesiod-devel = "libhesiod.so.0"
RPROVIDES_hesiod-devel = "hesiod-dev (= 3.2.1)"
RDEPENDS_hesiod-devel = "hesiod pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hesiod-3.2.1-11.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/hesiod-devel-3.2.1-11.el8.x86_64.rpm \
          "

SRC_URI[hesiod.sha256sum] = "b77aa8f05f54889217cfa5fcefd65ce1319903d7d6c20fc6c159fc31e6371abd"
SRC_URI[hesiod-devel.sha256sum] = "98b8ce23d361bd449640fccaa29b9672bf0971bb5cbdc840ba2b3e1ea811aadf"
