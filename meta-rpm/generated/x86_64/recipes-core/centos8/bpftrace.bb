SUMMARY = "generated recipe based on bpftrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bcc clang elfutils libgcc llvm pkgconfig-native"
RPM_SONAME_REQ_bpftrace = "libLLVM-9.so libbcc.so.0 libc.so.6 libclang.so.9 libelf.so.1 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_bpftrace = "bcc clang-libs elfutils-libelf glibc libgcc libstdc++ llvm-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bpftrace-0.9.2-1.el8.x86_64.rpm \
          "

SRC_URI[bpftrace.sha256sum] = "0ae991250faeeeac0b41a694afc5482224d6ceec67c2dbcf093c89e1d260ceae"
