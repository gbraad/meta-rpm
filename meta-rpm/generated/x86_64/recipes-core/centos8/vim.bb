SUMMARY = "generated recipe based on vim srpm"
DESCRIPTION = "Description"
LICENSE = "vim & MIT"
RPM_LICENSE = "Vim and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl cairo gdk-pixbuf glib-2.0 gpm gtk+3 libice libselinux libsm libx11 libxt ncurses pango pkgconfig-native"
RPM_SONAME_REQ_vim-X11 = "libICE.so.6 libSM.so.6 libX11.so.6 libXt.so.6 libacl.so.1 libc.so.6 libcairo.so.2 libdl.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpm.so.2 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libselinux.so.1 libtinfo.so.6"
RDEPENDS_vim-X11 = "bash cairo gdk-pixbuf2 glib2 glibc gpm-libs gtk3 hicolor-icon-theme libICE libSM libX11 libXt libacl libattr libselinux ncurses-libs pango perl-libs vim-common"
RPM_SONAME_REQ_vim-common = "libc.so.6"
RDEPENDS_vim-common = "bash glibc vim-filesystem"
RPM_SONAME_REQ_vim-enhanced = "libacl.so.1 libc.so.6 libdl.so.2 libgpm.so.2 libm.so.6 libpthread.so.0 libselinux.so.1 libtinfo.so.6"
RDEPENDS_vim-enhanced = "bash glibc gpm-libs libacl libselinux ncurses-libs vim-common which"
RPM_SONAME_REQ_vim-minimal = "libacl.so.1 libc.so.6 libselinux.so.1 libtinfo.so.6"
RDEPENDS_vim-minimal = "glibc libacl libselinux ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vim-X11-8.0.1763-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vim-common-8.0.1763-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vim-enhanced-8.0.1763-13.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/vim-filesystem-8.0.1763-13.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/vim-minimal-8.0.1763-13.el8.x86_64.rpm \
          "

SRC_URI[vim-X11.sha256sum] = "656f74bc450f1138fc4ef8e53d4a02996022a7eee99daea4e486669971ab2a8e"
SRC_URI[vim-common.sha256sum] = "cb2448c905f3b0c0dc583a09b211ddd8e7b47e8df7f432e13208786dbc0264f7"
SRC_URI[vim-enhanced.sha256sum] = "50f871e1d2b24ca92f327268887e70da8e364ddc2124947e5afa37f0a035f8eb"
SRC_URI[vim-filesystem.sha256sum] = "e61f185f6fcce2740e2f92f963b069695dd0120b4aa82343458c43cf4ba95faf"
SRC_URI[vim-minimal.sha256sum] = "2bf56823ac206cd882724d132af6465e0ae44c1631f84baffe8c6df80569b81e"
