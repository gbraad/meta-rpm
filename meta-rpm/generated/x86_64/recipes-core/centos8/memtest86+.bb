SUMMARY = "generated recipe based on memtest86+ srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_memtest86+ = "bash coreutils grubby sed util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/memtest86+-5.01-19.el8.x86_64.rpm \
          "

SRC_URI[memtest86+.sha256sum] = "230cf894f28b8aeee24f4a70cc54391c8382f00b8f377b7c8672dd5e2e22e757"
