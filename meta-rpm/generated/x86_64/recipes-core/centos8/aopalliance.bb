SUMMARY = "generated recipe based on aopalliance srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_aopalliance = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_aopalliance-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/aopalliance-1.0-17.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/aopalliance-javadoc-1.0-17.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[aopalliance.sha256sum] = "ad404540d7b8c97b99b535a4354823431009f243fa9679a9a7fa896c226a9c8d"
SRC_URI[aopalliance-javadoc.sha256sum] = "d5e7d2fd4f9d66fe0d28fb3a5b34cf83451b3bd4d51cd483c8ce3c1d5d9d3d20"
