SUMMARY = "generated recipe based on teckit srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | CPL-1.0"
RPM_LICENSE = "LGPLv2+ or CPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "expat libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_teckit = "libTECkit.so.0 libTECkit_Compiler.so.0"
RPM_SONAME_REQ_teckit = "libc.so.6 libexpat.so.1 libgcc_s.so.1 libm.so.6 libstdc++.so.6 libz.so.1"
RDEPENDS_teckit = "expat glibc libgcc libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/teckit-2.5.8-1.el8.x86_64.rpm \
          "

SRC_URI[teckit.sha256sum] = "5288b9d2ab4cd7ed5b852cfeca26d75492aaa176416ab8dfbda890fa1a675a7a"
