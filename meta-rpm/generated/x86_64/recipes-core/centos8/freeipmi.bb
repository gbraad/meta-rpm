SUMMARY = "generated recipe based on freeipmi srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcrypt pkgconfig-native"
RPM_SONAME_PROV_freeipmi = "libfreeipmi.so.17 libipmiconsole.so.2 libipmidetect.so.0 libipmimonitoring.so.6"
RPM_SONAME_REQ_freeipmi = "libc.so.6 libgcrypt.so.20 libm.so.6 libpthread.so.0"
RDEPENDS_freeipmi = "bash glibc libgcrypt systemd"
RPM_SONAME_REQ_freeipmi-bmc-watchdog = "libc.so.6 libfreeipmi.so.17 libgcrypt.so.20 libm.so.6 libpthread.so.0"
RDEPENDS_freeipmi-bmc-watchdog = "bash freeipmi glibc libgcrypt"
RPM_SONAME_REQ_freeipmi-devel = "libfreeipmi.so.17 libipmiconsole.so.2 libipmidetect.so.0 libipmimonitoring.so.6"
RPROVIDES_freeipmi-devel = "freeipmi-dev (= 1.6.1)"
RDEPENDS_freeipmi-devel = "freeipmi pkgconf-pkg-config"
RPM_SONAME_REQ_freeipmi-ipmidetectd = "libc.so.6 libfreeipmi.so.17 libgcrypt.so.20 libm.so.6 libpthread.so.0"
RDEPENDS_freeipmi-ipmidetectd = "bash freeipmi glibc libgcrypt"
RPM_SONAME_REQ_freeipmi-ipmiseld = "libc.so.6 libfreeipmi.so.17 libgcrypt.so.20 libm.so.6 libpthread.so.0"
RDEPENDS_freeipmi-ipmiseld = "bash freeipmi glibc libgcrypt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/freeipmi-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/freeipmi-bmc-watchdog-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/freeipmi-ipmidetectd-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/freeipmi-ipmiseld-1.6.1-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/freeipmi-devel-1.6.1-1.el8.x86_64.rpm \
          "

SRC_URI[freeipmi.sha256sum] = "79e0eb53dfb53d373f5139a75c7ed2739d513b78828d287bccf30172b66e4bd8"
SRC_URI[freeipmi-bmc-watchdog.sha256sum] = "7582024262fb0b8277987e520f31f20e1a2b3ebbca358bf5bcbf17f51bbfd13d"
SRC_URI[freeipmi-devel.sha256sum] = "62147ea82f98f69d16bdc21e3ee615470ca2579891d7c85305139e8017ec2d77"
SRC_URI[freeipmi-ipmidetectd.sha256sum] = "d587c54c5a94be9000e200ca154f37d910ce516e14eb76cbcf874c8bd925cb18"
SRC_URI[freeipmi-ipmiseld.sha256sum] = "5e37e788ca2f8cd7ba8b4bf5a372d5d16e0123873e02d59d3d61a6438a63ea8c"
