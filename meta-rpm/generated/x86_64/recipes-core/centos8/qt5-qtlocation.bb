SUMMARY = "generated recipe based on qt5-qtlocation srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "icu libgcc libglvnd openssl pkgconfig-native qt5-qtbase qt5-qtdeclarative zlib"
RPM_SONAME_PROV_qt5-qtlocation = "libQt5Location.so.5 libQt5Positioning.so.5 libQt5PositioningQuick.so.5"
RPM_SONAME_REQ_qt5-qtlocation = "ld-linux-x86-64.so.2 libGL.so.1 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Sql.so.5 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_qt5-qtlocation = "glibc libgcc libglvnd-glx libicu libstdc++ openssl-libs qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative zlib"
RPM_SONAME_REQ_qt5-qtlocation-devel = "libQt5Location.so.5 libQt5Positioning.so.5 libQt5PositioningQuick.so.5"
RPROVIDES_qt5-qtlocation-devel = "qt5-qtlocation-dev (= 5.12.5)"
RDEPENDS_qt5-qtlocation-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtlocation"
RPM_SONAME_REQ_qt5-qtlocation-examples = "libGL.so.1 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5Gui.so.5 libQt5Location.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5Positioning.so.5 libQt5PositioningQuick.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtlocation-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtlocation"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtlocation-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtlocation-devel-5.12.5-1.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/qt5-qtlocation-examples-5.12.5-1.el8.x86_64.rpm \
          "

SRC_URI[qt5-qtlocation.sha256sum] = "73274479fe9c3bddd63179bb410688bc5f0e44b7343c36d8b785d8cb255c0656"
SRC_URI[qt5-qtlocation-devel.sha256sum] = "6feacb7cd7966fc5faa97345044b0538a0e7e229aabafd9e1ecb452afb38802a"
SRC_URI[qt5-qtlocation-examples.sha256sum] = "69d8aabe99c35f6963b3c2ecbe82ff550dadbb751344ec5eb5e1563033bfc2d0"
