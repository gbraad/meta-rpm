SUMMARY = "generated recipe based on woff2 srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "brotli libgcc pkgconfig-native"
RPM_SONAME_PROV_woff2 = "libwoff2common.so.1.0.2 libwoff2dec.so.1.0.2 libwoff2enc.so.1.0.2"
RPM_SONAME_REQ_woff2 = "libbrotlidec.so.1 libbrotlienc.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_woff2 = "brotli glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/woff2-1.0.2-4.el8.x86_64.rpm \
          "

SRC_URI[woff2.sha256sum] = "13453b217a743afeb8789860ae11c8688420a1aa82e77c351a08c3eccc3290cc"
