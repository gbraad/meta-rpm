SUMMARY = "generated recipe based on iio-sensor-proxy srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libgcc libgudev pkgconfig-native"
RPM_SONAME_REQ_iio-sensor-proxy = "libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libm.so.6"
RDEPENDS_iio-sensor-proxy = "bash glib2 glibc libgcc libgudev systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/iio-sensor-proxy-2.4-3.el8.x86_64.rpm \
          "

SRC_URI[iio-sensor-proxy.sha256sum] = "fb99ca631f5e8cf0fd1bbf8f7079057802ca77214b3b0bfcead9f5b3d02507a4"
