SUMMARY = "generated recipe based on perl-Params-Util srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Params-Util = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Params-Util = "glibc perl-Exporter perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Params-Util-1.07-22.el8.x86_64.rpm \
          "

SRC_URI[perl-Params-Util.sha256sum] = "7e557daaa734b8f9a1ada5508f5f9f916bb3f1787d7f1f488fb6643694d744cb"
