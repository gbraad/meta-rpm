SUMMARY = "generated recipe based on cloud-utils-growpart srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_cloud-utils-growpart = "bash gawk util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cloud-utils-growpart-0.29-3.el8.noarch.rpm \
          "

SRC_URI[cloud-utils-growpart.sha256sum] = "ce137030e5e2ed2be9238c97448da5189b80d14a5e694b336942dcc6ef1797c7"
