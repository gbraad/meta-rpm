SUMMARY = "generated recipe based on xcb-util srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxcb pkgconfig-native"
RPM_SONAME_PROV_xcb-util = "libxcb-util.so.1"
RPM_SONAME_REQ_xcb-util = "libc.so.6 libxcb.so.1"
RDEPENDS_xcb-util = "glibc libxcb"
RPM_SONAME_REQ_xcb-util-devel = "libxcb-util.so.1"
RPROVIDES_xcb-util-devel = "xcb-util-dev (= 0.4.0)"
RDEPENDS_xcb-util-devel = "libxcb-devel pkgconf-pkg-config xcb-util"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xcb-util-0.4.0-10.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xcb-util-devel-0.4.0-10.el8.x86_64.rpm \
          "

SRC_URI[xcb-util.sha256sum] = "6abd77ede2437765354a3c79711625183e5b7aff57e527841060edc4f343b98b"
SRC_URI[xcb-util-devel.sha256sum] = "0d9d58f0faddd136828a03190d6d5c6990e496eb85d7f81edb6cf109710e6bcd"
