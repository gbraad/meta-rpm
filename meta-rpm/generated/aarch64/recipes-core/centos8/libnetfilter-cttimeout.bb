SUMMARY = "generated recipe based on libnetfilter_cttimeout srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libmnl pkgconfig-native"
RPM_SONAME_PROV_libnetfilter_cttimeout = "libnetfilter_cttimeout.so.1"
RPM_SONAME_REQ_libnetfilter_cttimeout = "ld-linux-aarch64.so.1 libc.so.6 libmnl.so.0"
RDEPENDS_libnetfilter_cttimeout = "glibc libmnl"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnetfilter_cttimeout-1.0.0-11.el8.aarch64.rpm \
          "

SRC_URI[libnetfilter_cttimeout.sha256sum] = "f6b9506cfd29cfe742339918e5d6cde84afdc574630d8b59991d1ffdbff386cf"
