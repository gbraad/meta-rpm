SUMMARY = "generated recipe based on gnome-menus srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_gnome-menus = "libgnome-menu-3.so.0"
RPM_SONAME_REQ_gnome-menus = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_gnome-menus = "glib2 glibc"
RPM_SONAME_REQ_gnome-menus-devel = "libgnome-menu-3.so.0"
RPROVIDES_gnome-menus-devel = "gnome-menus-dev (= 3.13.3)"
RDEPENDS_gnome-menus-devel = "glib2-devel gnome-menus pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-menus-3.13.3-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gnome-menus-devel-3.13.3-11.el8.aarch64.rpm \
          "

SRC_URI[gnome-menus.sha256sum] = "b14d7566daba8b8580475814dc155634c1d10ea8f4a4bbb7c1e60f91518e4610"
SRC_URI[gnome-menus-devel.sha256sum] = "b3d46afb182c54127489ef531424ca0ce0d0034704b3ed7a02b7e8326efa857c"
