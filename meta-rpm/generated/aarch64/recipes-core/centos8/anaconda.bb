SUMMARY = "generated recipe based on anaconda srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & MIT"
RPM_LICENSE = "GPLv2+ and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk audit-libs cairo gdk-pixbuf glade glib-2.0 gtk+3 libarchive libxklavier libxml2 pango pkgconfig-native platform-python3 rpm"
RDEPENDS_anaconda = "anaconda-core anaconda-gui anaconda-install-env-deps anaconda-tui"
RPM_SONAME_REQ_anaconda-core = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libpython3.6m.so.1.0"
RDEPENDS_anaconda-core = "NetworkManager NetworkManager-libnm NetworkManager-team anaconda-tui audit-libs bash chrony cracklib-dicts dhcp-client glibc glibc-langpack-en kbd langtable-data libreport-anaconda platform-python platform-python-coverage python3-blivet python3-blockdev python3-dbus python3-dnf python3-gobject-base python3-kickstart python3-langtable python3-libs python3-libselinux python3-meh python3-ntplib python3-ordered-set python3-pid python3-productmd python3-pwquality python3-pydbus python3-pyparted python3-pytz python3-requests python3-requests-file python3-requests-ftp python3-rpm python3-syspurpose python3-systemd subscription-manager systemd teamd util-linux"
RPM_SONAME_REQ_anaconda-dracut = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 librpm.so.8 librpmio.so.8"
RDEPENDS_anaconda-dracut = "bash dracut dracut-live dracut-network glibc libarchive platform-python python3-kickstart rpm-libs xz"
RDEPENDS_anaconda-gui = "NetworkManager-wifi adwaita-icon-theme anaconda-core anaconda-user-help anaconda-widgets centos-logos keybinder3 libgnomekbd libtimezonemap libxklavier nm-connection-editor platform-python python3-meh-gui tigervnc-server-minimal yelp"
RDEPENDS_anaconda-install-env-deps = "createrepo_c gdb isomd5sum kexec-tools libblockdev-plugins-all realmd rsync tmux udisks2-iscsi"
RDEPENDS_anaconda-tui = "anaconda-core platform-python python3-simpleline"
RPM_SONAME_PROV_anaconda-widgets = "libAnacondaWidgets.so.4"
RPM_SONAME_REQ_anaconda-widgets = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgladeui-2.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libxklavier.so.16 libxml2.so.2"
RDEPENDS_anaconda-widgets = "atk cairo cairo-gobject gdk-pixbuf2 glade-libs glib2 glibc gtk3 libxklavier libxml2 pango platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/anaconda-29.19.2.17-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/anaconda-core-29.19.2.17-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/anaconda-dracut-29.19.2.17-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/anaconda-gui-29.19.2.17-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/anaconda-install-env-deps-29.19.2.17-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/anaconda-tui-29.19.2.17-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/anaconda-widgets-29.19.2.17-1.el8.aarch64.rpm \
          "

SRC_URI[anaconda.sha256sum] = "4a645c0aa36336aca348c6c6d6cbca7a040ccf44a4909a8e84aef3f5dba2dd48"
SRC_URI[anaconda-core.sha256sum] = "d30eb22e88ac3a8f9bc9c9fc6257dc4e5a872cd14369a626250b6160ae4fd29d"
SRC_URI[anaconda-dracut.sha256sum] = "c4173f4e6305dff8a81f5f29590f5f61eb8eae54d4b63f48ec3214312a2c8090"
SRC_URI[anaconda-gui.sha256sum] = "e131f59108b0c9fa5d6145d299dbbb8f61ed536f364c53c7da3a9fa6d4ffe191"
SRC_URI[anaconda-install-env-deps.sha256sum] = "96cba7a558871beb5e367030fbc6dceb0a527a206d44ac08f1a7306999bc9874"
SRC_URI[anaconda-tui.sha256sum] = "5562e70a2e422e9a24b99eb3932fd5f4e6674cfcf9d810bfcdbe567a4528cc11"
SRC_URI[anaconda-widgets.sha256sum] = "e059031b4f83017129e0a976025132c077d99df5e37321802b1b0af973e79bb4"
