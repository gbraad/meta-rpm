SUMMARY = "generated recipe based on istack-commons srpm"
DESCRIPTION = "Description"
LICENSE = "CDDL-1.1 & GPL-2.0"
RPM_LICENSE = "CDDL-1.1 and GPLv2 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_istack-commons = "java-1.8.0-openjdk-headless javapackages-filesystem jvnet-parent maven-plugin-bundle"
RDEPENDS_istack-commons-runtime = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_istack-commons-tools = "ant-lib java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/istack-commons-runtime-2.21-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/istack-commons-tools-2.21-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/istack-commons-2.21-9.el8.noarch.rpm \
          "

SRC_URI[istack-commons.sha256sum] = "047f0ba7748500e175cff432642b4dc1535d49af7e1ee052883c9f9e6519d45d"
SRC_URI[istack-commons-runtime.sha256sum] = "f240fa208e1beb5d8ff2364587c50dd11f53be04ce6c0392ab85fe0c7d16ad98"
SRC_URI[istack-commons-tools.sha256sum] = "683cad36424a995beef9921804138baf2bc214f7e885bf02267a0ae00979555e"
