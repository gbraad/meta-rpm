SUMMARY = "generated recipe based on raptor2 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-2.0 | CLOSED"
RPM_LICENSE = "GPLv2+ or LGPLv2+ or ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "curl icu libxml2 libxslt pkgconfig-native xz zlib"
RPM_SONAME_PROV_raptor2 = "libraptor2.so.0"
RPM_SONAME_REQ_raptor2 = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libdl.so.2 libicuuc.so.60 liblzma.so.5 libm.so.6 libxml2.so.2 libxslt.so.1 libz.so.1"
RDEPENDS_raptor2 = "glibc libcurl libicu libxml2 libxslt xz-libs zlib"
RPM_SONAME_REQ_raptor2-devel = "libraptor2.so.0"
RPROVIDES_raptor2-devel = "raptor2-dev (= 2.0.15)"
RDEPENDS_raptor2-devel = "pkgconf-pkg-config raptor2"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/raptor2-2.0.15-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/raptor2-devel-2.0.15-13.el8.aarch64.rpm \
          "

SRC_URI[raptor2.sha256sum] = "4d6f750dbfd3c68af32199ac0580951c1255c3fe8847d104a5dbe20c27c6a21b"
SRC_URI[raptor2-devel.sha256sum] = "d6146bbf512e26aa0aeaecfd33f11ad8876e10635baf00915fd568ff1639d96f"
