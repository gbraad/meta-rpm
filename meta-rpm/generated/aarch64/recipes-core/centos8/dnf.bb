SUMMARY = "generated recipe based on dnf srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0 & GPL-2.0"
RPM_LICENSE = "GPLv2+ and GPLv2 and GPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gpgme libcomps libdnf librepo pkgconfig-native python3 python3-iniparse"
RDEPENDS_dnf = "bash python3-dnf"
RDEPENDS_dnf-automatic = "bash dnf platform-python systemd"
RDEPENDS_dnf-data = "libreport-filesystem"
RDEPENDS_python3-dnf = "dnf-data libmodulemd platform-python python3-gpg python3-hawkey python3-libcomps python3-libdnf python3-rpm"
RDEPENDS_yum = "dnf"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dnf-4.2.17-7.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dnf-automatic-4.2.17-7.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dnf-data-4.2.17-7.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-dnf-4.2.17-7.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/yum-4.2.17-7.el8_2.noarch.rpm \
          "
SRC_URI = "file://dnf-logdir-vs-installroot.patch"

SRC_URI[dnf.sha256sum] = "825d5e0379db26dbd52decc89615fed1ce7ebae5c8bc20abc8323fc51df5eb72"
SRC_URI[dnf-automatic.sha256sum] = "4f01e12693798f463093682b066dbc699c7bffce9c5045ae1ddda1941e4deef2"
SRC_URI[dnf-data.sha256sum] = "29cd4ba0de0e33ad0791c84a7ef10269aadd9d5fc6acd0fc981c9a8b7214f50a"
SRC_URI[python3-dnf.sha256sum] = "11271ccb1aa501763fb5e7f096c29e547d60a9a1a2132cf77a49f4722142d9f2"
SRC_URI[yum.sha256sum] = "298be0e532be703886fa5726f21a5a42344bd2933e03d031828912d368cf13ba"
