SUMMARY = "generated recipe based on sqlite srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native readline zlib"
RPM_SONAME_REQ_lemon = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lemon = "glibc"
RPM_SONAME_REQ_sqlite = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libncurses.so.6 libpthread.so.0 libreadline.so.7 libtinfo.so.6 libz.so.1"
RDEPENDS_sqlite = "glibc ncurses-libs readline sqlite-libs zlib"
RPM_SONAME_REQ_sqlite-devel = "libsqlite3.so.0"
RPROVIDES_sqlite-devel = "sqlite-dev (= 3.26.0)"
RDEPENDS_sqlite-devel = "pkgconf-pkg-config sqlite sqlite-libs"
RPM_SONAME_PROV_sqlite-libs = "libsqlite3.so.0"
RPM_SONAME_REQ_sqlite-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_sqlite-libs = "glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lemon-3.26.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sqlite-3.26.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sqlite-devel-3.26.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sqlite-doc-3.26.0-6.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sqlite-libs-3.26.0-6.el8.aarch64.rpm \
          "

SRC_URI[lemon.sha256sum] = "d31c8170af473bfc5bec2aadb7ea241d283a63e5eccc55fd556b486e9fcd3938"
SRC_URI[sqlite.sha256sum] = "653c8b31bab18bc3121665d5ea0034ade5d86e9d7c7c83db8f96f75db82085fa"
SRC_URI[sqlite-devel.sha256sum] = "b29ad5c1f99819372cb6972670d2ef47291f5b1c2082d3ef77d493fca0104a2d"
SRC_URI[sqlite-doc.sha256sum] = "c83764b7de6eac97675c533dd6220028ceed2d9ceb8f68da2999ad4a56afbb19"
SRC_URI[sqlite-libs.sha256sum] = "d8df344cd05e1622c404e7151caf64ba310227ddbe079aef838c0fe4743e30bf"
