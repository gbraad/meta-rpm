SUMMARY = "generated recipe based on hyphen-eu srpm"
DESCRIPTION = "Description"
LICENSE = "LPPL-1.0"
RPM_LICENSE = "LPPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-eu = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-eu-0.20110620-12.el8.noarch.rpm \
          "

SRC_URI[hyphen-eu.sha256sum] = "2da3397894af7e1cfd2f3c4adc5fed1d5753e6f81e94a6c1804cb3c4d6a54d00"
