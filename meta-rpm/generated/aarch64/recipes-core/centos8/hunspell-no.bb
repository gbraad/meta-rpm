SUMMARY = "generated recipe based on hunspell-no srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-nb = "hunspell"
RDEPENDS_hunspell-nn = "hunspell"
RDEPENDS_hyphen-nb = "hyphen"
RDEPENDS_hyphen-nn = "hyphen"
RDEPENDS_mythes-nb = "mythes"
RDEPENDS_mythes-nn = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-nb-2.0.10-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-nn-2.0.10-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-nb-2.0.10-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-nn-2.0.10-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-nb-2.0.10-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-nn-2.0.10-7.el8.noarch.rpm \
          "

SRC_URI[hunspell-nb.sha256sum] = "41b90cbfb8d5b63e41d6b1cebee8418f4a7c7acd2fdb7c3c89b010d550eb195d"
SRC_URI[hunspell-nn.sha256sum] = "0e07d73fb24eae0b8bc9d85b863e306c8f56a46938d46be64debcabf35e09c79"
SRC_URI[hyphen-nb.sha256sum] = "df75cf2305f8926ec1e81d277bc82cdd87641c3375005432aa9ea1aaa48c061c"
SRC_URI[hyphen-nn.sha256sum] = "ea4d415dc46aa04ba1647db496881a052a51575015241bddecdd402bb92edac9"
SRC_URI[mythes-nb.sha256sum] = "677056df98ae137e1fcaaa38ce224a195475d8d42b60951ef1ae281dc0f095ab"
SRC_URI[mythes-nn.sha256sum] = "5f1f112bf2dd0d8818f59c1cd4aee0b390bbd8d66428b5000fd36b4a80d4eb74"
