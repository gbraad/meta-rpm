SUMMARY = "generated recipe based on hunspell-da srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-da = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-da-1.7.42-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-da.sha256sum] = "11210372b4b97efb3348ecb1594453f4e4b254e0d0320d24ea02677c853d9e41"
