SUMMARY = "generated recipe based on bitmap-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Lucida"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_bitmap-console-fonts = "fontpackages-filesystem"
RDEPENDS_bitmap-fangsongti-fonts = "fontpackages-filesystem"
RDEPENDS_bitmap-fixed-fonts = "fontpackages-filesystem"
RDEPENDS_bitmap-fonts-compat = "bitmap-console-fonts bitmap-fangsongti-fonts bitmap-fixed-fonts bitmap-lucida-typewriter-fonts ucs-miscfixed-fonts"
RDEPENDS_bitmap-lucida-typewriter-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bitmap-console-fonts-0.3-28.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bitmap-fangsongti-fonts-0.3-28.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bitmap-fixed-fonts-0.3-28.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bitmap-fonts-compat-0.3-28.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bitmap-lucida-typewriter-fonts-0.3-28.el8.noarch.rpm \
          "

SRC_URI[bitmap-console-fonts.sha256sum] = "738d3664ceb31697b6bbba0aa12116d1444c0c2776fdf922ce6d90760b038875"
SRC_URI[bitmap-fangsongti-fonts.sha256sum] = "91020945300e2ac980f0eb1c4b361387fd6d65a2d755d35612675b5ec90080b5"
SRC_URI[bitmap-fixed-fonts.sha256sum] = "57fe90746efcfb707fd5e646876a857d9e8e2a4c539ed157a2b088e30d6733b0"
SRC_URI[bitmap-fonts-compat.sha256sum] = "5543784c934f83bce005cc6a8396de6fb0c901986f2199fd552022512e99be3e"
SRC_URI[bitmap-lucida-typewriter-fonts.sha256sum] = "8c977476965e090752181b7bc9c390de454ff42614e2e951a4d98b23f9946faf"
