SUMMARY = "generated recipe based on mariadb-java-client srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & LGPL-2.0"
RPM_LICENSE = "BSD and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mariadb-java-client = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mariadb-java-client-2.2.5-2.el8.noarch.rpm \
          "

SRC_URI[mariadb-java-client.sha256sum] = "bfb78f14a853d85c3a17d8eb3ca729c0dbeedcc2161e9b69a86b69fe5b664255"
