SUMMARY = "generated recipe based on hesiod srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libidn pkgconfig-native"
RPM_SONAME_PROV_hesiod = "libhesiod.so.0"
RPM_SONAME_REQ_hesiod = "ld-linux-aarch64.so.1 libc.so.6 libidn.so.11 libresolv.so.2"
RDEPENDS_hesiod = "glibc libidn"
RPM_SONAME_REQ_hesiod-devel = "libhesiod.so.0"
RPROVIDES_hesiod-devel = "hesiod-dev (= 3.2.1)"
RDEPENDS_hesiod-devel = "hesiod pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hesiod-3.2.1-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/hesiod-devel-3.2.1-11.el8.aarch64.rpm \
          "

SRC_URI[hesiod.sha256sum] = "0495788a005d6f77b218b30b7442cb582dab90a8dc727ed940dee1228cda9023"
SRC_URI[hesiod-devel.sha256sum] = "62200d51ff11f6657886a115048253276f9ca5d46a6b98d1cce7a1a3f9696195"
