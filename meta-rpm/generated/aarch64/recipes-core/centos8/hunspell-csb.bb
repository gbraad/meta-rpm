SUMMARY = "generated recipe based on hunspell-csb srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-csb = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-csb-0.20050311-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-csb.sha256sum] = "dc07c43440855bf9e53f99c2559224402e57bb15cdba5ee9378443ddf032f8c9"
