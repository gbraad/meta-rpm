SUMMARY = "generated recipe based on mozvoikko srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mozvoikko = "libvoikko mozilla-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mozvoikko-2.1-5.el8.noarch.rpm \
          "

SRC_URI[mozvoikko.sha256sum] = "e184b17a7e3bc2312dadc47a0dde80f3e53a5050d6762100230358e69f3f8ab0"
