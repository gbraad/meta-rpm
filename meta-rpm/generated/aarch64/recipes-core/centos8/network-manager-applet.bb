SUMMARY = "generated recipe based on network-manager-applet srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gcr gdk-pixbuf glib-2.0 gtk+3 jansson libgcc libnotify libsecret libselinux modemmanager networkmanager pango pkgconfig-native"
RPM_SONAME_PROV_libnma = "libnma.so.0"
RPM_SONAME_REQ_libnma = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libgcc_s.so.1 libgck-1.so.0 libgcr-base-3.so.1 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libnm.so.0 libpthread.so.0"
RDEPENDS_libnma = "NetworkManager-libnm cairo gcr glib2 glibc gtk3 libgcc mobile-broadband-provider-info"
RPM_SONAME_REQ_libnma-devel = "libnma.so.0"
RPROVIDES_libnma-devel = "libnma-dev (= 1.8.22)"
RDEPENDS_libnma-devel = "NetworkManager-libnm-devel gtk3-devel libnma pkgconf-pkg-config"
RPM_SONAME_REQ_network-manager-applet = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libmm-glib.so.0 libnm.so.0 libnma.so.0 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsecret-1.so.0"
RDEPENDS_network-manager-applet = "ModemManager-glib NetworkManager NetworkManager-libnm atk cairo gdk-pixbuf2 glib2 glibc gtk3 libgcc libnma libnotify libsecret nm-connection-editor pango"
RPM_SONAME_REQ_nm-connection-editor = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjansson.so.4 libnm.so.0 libnma.so.0 libpthread.so.0 libselinux.so.1"
RDEPENDS_nm-connection-editor = "NetworkManager-libnm glib2 glibc gtk3 jansson libgcc libnma libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libnma-1.8.22-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/network-manager-applet-1.8.22-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nm-connection-editor-1.8.22-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libnma-devel-1.8.22-2.el8.aarch64.rpm \
          "

SRC_URI[libnma.sha256sum] = "10b204dade632f398a715498c3aec5f6435cc9826edbd7a833eea350f6581533"
SRC_URI[libnma-devel.sha256sum] = "de8947e0aa8732977e073bc6dbf316630e6a0ce3e45cd679d499a076b011ab1f"
SRC_URI[network-manager-applet.sha256sum] = "7a0a462afa6b34419db5cd85514a6efc20430010687daeacb6d043e77143fab9"
SRC_URI[nm-connection-editor.sha256sum] = "4b1b6681ad29ec1a5350ce88d83c3e9508a21b5815a191976ce95e10c3c408bc"
