SUMMARY = "generated recipe based on util-linux srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libblkid libselinux libuuid pkgconfig-native"
RPM_SONAME_PROV_libmount = "libmount.so.1"
RPM_SONAME_REQ_libmount = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 librt.so.1 libselinux.so.1 libuuid.so.1"
RDEPENDS_libmount = "glibc libblkid libselinux libuuid"
RPM_SONAME_REQ_libmount-devel = "libmount.so.1"
RPROVIDES_libmount-devel = "libmount-dev (= 2.32.1)"
RDEPENDS_libmount-devel = "libblkid-devel libmount pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libmount-2.32.1-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmount-devel-2.32.1-22.el8.aarch64.rpm \
          "

SRC_URI[libmount.sha256sum] = "41b7e025c4d24fe389949fb13364c0704faff21c5ff3c913af5ec8da565455e3"
SRC_URI[libmount-devel.sha256sum] = "e746986c003d70620f2627e991f6ab00f40d7e4fc42e42075c1d137aa73d927e"
