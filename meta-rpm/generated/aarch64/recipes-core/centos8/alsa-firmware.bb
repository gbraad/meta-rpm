SUMMARY = "generated recipe based on alsa-firmware srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & BSD & GPL-2.0 & GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPL+ and BSD and GPLv2+ and GPLv2 and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_alsa-firmware = "alsa-tools-firmware systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-firmware-1.0.29-6.el8.noarch.rpm \
          "

SRC_URI[alsa-firmware.sha256sum] = "0f8996e38fa0ffbcb8e0feccf67065cb42f2676e6397987a899cbcdcd7b8ab41"
