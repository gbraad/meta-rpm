SUMMARY = "generated recipe based on perl-Package-Stash srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Package-Stash = "perl-Carp perl-Dist-CheckConflicts perl-Getopt-Long perl-Module-Implementation perl-Package-Stash-XS perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Package-Stash-0.37-9.el8.noarch.rpm \
          "

SRC_URI[perl-Package-Stash.sha256sum] = "b25e7097ccbc05c7354fe82feafd0dbed30d22ccf9a4d371e56d4be078c68886"
