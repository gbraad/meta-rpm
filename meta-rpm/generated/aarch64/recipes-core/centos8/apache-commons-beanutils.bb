SUMMARY = "generated recipe based on apache-commons-beanutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-beanutils = "apache-commons-collections apache-commons-logging java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-beanutils-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-beanutils-1.9.3-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-beanutils-javadoc-1.9.3-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-beanutils.sha256sum] = "5b600ce87533b133aec136e52f375f97271d15f2a41136863050a9418fe7619d"
SRC_URI[apache-commons-beanutils-javadoc.sha256sum] = "126832cf99d236b1c3eeadccdfb13c4ebcc1c9489172d7396934b36afc140bcf"
