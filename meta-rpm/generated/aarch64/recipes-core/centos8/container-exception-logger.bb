SUMMARY = "generated recipe based on container-exception-logger srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_container-exception-logger = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_container-exception-logger = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/container-exception-logger-1.0.2-3.el8.aarch64.rpm \
          "

SRC_URI[container-exception-logger.sha256sum] = "454b430abfd27c1aa494c7d5ec0582e61d8ad270d3f996f4e64e42b9b94e1f96"
