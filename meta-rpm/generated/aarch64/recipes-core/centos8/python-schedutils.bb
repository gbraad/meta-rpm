SUMMARY = "generated recipe based on python-schedutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-schedutils = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-schedutils = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-schedutils-0.6-6.el8.aarch64.rpm \
          "

SRC_URI[python3-schedutils.sha256sum] = "7d11819e0646e34f1489fbe9f6673a6cd701657d8ae1fe0ca370f7421150163f"
