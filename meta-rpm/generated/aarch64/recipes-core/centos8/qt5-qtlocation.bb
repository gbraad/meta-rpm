SUMMARY = "generated recipe based on qt5-qtlocation srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "icu libgcc libglvnd openssl pkgconfig-native qt5-qtbase qt5-qtdeclarative zlib"
RPM_SONAME_PROV_qt5-qtlocation = "libQt5Location.so.5 libQt5Positioning.so.5 libQt5PositioningQuick.so.5"
RPM_SONAME_REQ_qt5-qtlocation = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Sql.so.5 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_qt5-qtlocation = "glibc libgcc libglvnd-glx libicu libstdc++ openssl-libs qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative zlib"
RPM_SONAME_REQ_qt5-qtlocation-devel = "libQt5Location.so.5 libQt5Positioning.so.5 libQt5PositioningQuick.so.5"
RPROVIDES_qt5-qtlocation-devel = "qt5-qtlocation-dev (= 5.12.5)"
RDEPENDS_qt5-qtlocation-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtlocation"
RPM_SONAME_REQ_qt5-qtlocation-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5Gui.so.5 libQt5Location.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5Positioning.so.5 libQt5PositioningQuick.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Sql.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtlocation-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtlocation"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtlocation-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtlocation-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtlocation-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtlocation.sha256sum] = "b6df8f3aa74e382716658a604270f3c0ed810713e9246fe1323012a1e2efa8c1"
SRC_URI[qt5-qtlocation-devel.sha256sum] = "8294ab4a2cf514b7fd176fac9a4b85b209a9ac40ae8a91629a94c0292f778440"
SRC_URI[qt5-qtlocation-examples.sha256sum] = "6b682323f6cb6d0590e9ca1f8faeb9dc2eb7028d1ea053f97049a844eb739001"
