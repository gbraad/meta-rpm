SUMMARY = "generated recipe based on libspectre srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ghostscript pkgconfig-native"
RPM_SONAME_PROV_libspectre = "libspectre.so.1"
RPM_SONAME_REQ_libspectre = "ld-linux-aarch64.so.1 libc.so.6 libgs.so.9"
RDEPENDS_libspectre = "glibc libgs"
RPM_SONAME_REQ_libspectre-devel = "libspectre.so.1"
RPROVIDES_libspectre-devel = "libspectre-dev (= 0.2.8)"
RDEPENDS_libspectre-devel = "libspectre pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libspectre-0.2.8-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libspectre-devel-0.2.8-5.el8.aarch64.rpm \
          "

SRC_URI[libspectre.sha256sum] = "9133b14ef8a619bc818cd58f00dae66d3f43b2cc07ea40495d0769ba119aa641"
SRC_URI[libspectre-devel.sha256sum] = "ee405458efc6abce557ec85d40c8b1922769984d8e46d3352244b42db2ba00bd"
