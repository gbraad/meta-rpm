SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-mn = "locale-base-mn-mn (= 2.28) virtual-locale-mn (= 2.28) virtual-locale-mn-mn (= 2.28)"
RDEPENDS_glibc-langpack-mn = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-mn-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-mn.sha256sum] = "f7d78d31788477d6586aa54ec63d65c60e103eff97d5fe2636cbf3ff780717f6"
