SUMMARY = "generated recipe based on attr srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_attr = "ld-linux-aarch64.so.1 libattr.so.1 libc.so.6"
RDEPENDS_attr = "glibc libattr"
RPM_SONAME_PROV_libattr = "libattr.so.1"
RPM_SONAME_REQ_libattr = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libattr = "glibc"
RPM_SONAME_REQ_libattr-devel = "libattr.so.1"
RPROVIDES_libattr-devel = "libattr-dev (= 2.4.48)"
RDEPENDS_libattr-devel = "glibc-headers libattr pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/attr-2.4.48-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libattr-2.4.48-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libattr-devel-2.4.48-3.el8.aarch64.rpm \
          "

SRC_URI[attr.sha256sum] = "01a9043330dff57f7696f47f4d082ff6177f774759aea97c6714fb7f23085036"
SRC_URI[libattr.sha256sum] = "6a6db7eab6e53dccc54116d2ddf86b02db4cff332a58b868f7ba778a99666c58"
SRC_URI[libattr-devel.sha256sum] = "3ef56ebbdb89161f70e97b639eadcdf8bfeb7db1891c96b1569e629339f68ed8"
