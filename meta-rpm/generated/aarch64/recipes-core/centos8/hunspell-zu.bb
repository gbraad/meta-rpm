SUMMARY = "generated recipe based on hunspell-zu srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-zu = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-zu-0.20100126-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-zu.sha256sum] = "9e6a99f880e3b0c806ddf3e36517c5646111d8f627a1e2903651f86abf318efd"
