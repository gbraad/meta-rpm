SUMMARY = "generated recipe based on lksctp-tools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0 & LGPL-2.0 & MIT"
RPM_LICENSE = "GPLv2 and GPLv2+ and LGPLv2 and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_lksctp-tools = "libsctp.so.1 libwithsctp.so.1"
RPM_SONAME_REQ_lksctp-tools = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_lksctp-tools = "bash glibc"
RPM_SONAME_REQ_lksctp-tools-devel = "libsctp.so.1 libwithsctp.so.1"
RPROVIDES_lksctp-tools-devel = "lksctp-tools-dev (= 1.0.18)"
RDEPENDS_lksctp-tools-devel = "lksctp-tools pkgconf-pkg-config"
RDEPENDS_lksctp-tools-doc = "lksctp-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lksctp-tools-1.0.18-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lksctp-tools-devel-1.0.18-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lksctp-tools-doc-1.0.18-3.el8.aarch64.rpm \
          "

SRC_URI[lksctp-tools.sha256sum] = "822c0d90487e6d5d4b5c032189395c46dd9f2420664d59a44f8df4252d818459"
SRC_URI[lksctp-tools-devel.sha256sum] = "1810799324f36cee5c19d2371370bf733d19bf1d04590540219ba8b2382a1af4"
SRC_URI[lksctp-tools-doc.sha256sum] = "b90b4e446b7106e65c8099a61a1c22acde4d3837a3d557e0af233b5cc6a2dd5d"
