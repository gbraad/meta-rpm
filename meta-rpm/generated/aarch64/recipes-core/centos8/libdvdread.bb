SUMMARY = "generated recipe based on libdvdread srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdvdread = "libdvdread.so.4"
RPM_SONAME_REQ_libdvdread = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_libdvdread = "glibc"
RPM_SONAME_REQ_libdvdread-devel = "libdvdread.so.4"
RPROVIDES_libdvdread-devel = "libdvdread-dev (= 5.0.3)"
RDEPENDS_libdvdread-devel = "libdvdread pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdvdread-5.0.3-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdvdread-devel-5.0.3-9.el8.aarch64.rpm \
          "

SRC_URI[libdvdread.sha256sum] = "73ffa94a5cb89e0f86eb160c7e26c3044a8bb784026a2472a4d2ef815413ebe8"
SRC_URI[libdvdread-devel.sha256sum] = "27a310e8ff3a647599b4f894725742f644d251e9a456c316dcd7b4735e0b155f"
