SUMMARY = "generated recipe based on nftables srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gmp iptables jansson libmnl libnftnl pkgconfig-native readline"
RPM_SONAME_PROV_nftables = "libnftables.so.1"
RPM_SONAME_REQ_nftables = "ld-linux-aarch64.so.1 libc.so.6 libgmp.so.10 libjansson.so.4 libmnl.so.0 libnftnl.so.11 libreadline.so.7 libxtables.so.12"
RDEPENDS_nftables = "bash glibc gmp iptables-libs jansson libmnl libnftnl readline"
RDEPENDS_python3-nftables = "nftables platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nftables-0.9.3-12.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-nftables-0.9.3-12.el8_2.1.aarch64.rpm \
          "

SRC_URI[nftables.sha256sum] = "778576231a154b87bfd5444271900c0733b0b4faca733fd240c3e721104a69f5"
SRC_URI[python3-nftables.sha256sum] = "1ff272380e7e4c2d5796959a814d571294e3ad7673d0f1808591d0776f2fd756"
