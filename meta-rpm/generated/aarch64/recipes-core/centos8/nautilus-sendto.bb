SUMMARY = "generated recipe based on nautilus-sendto srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_nautilus-sendto = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_nautilus-sendto = "glib2 glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nautilus-sendto-3.8.6-2.el8.aarch64.rpm \
          "

SRC_URI[nautilus-sendto.sha256sum] = "af9533e33e6d623e11d355c62e80f6ba17f14ebb8178bfe4601015f37aed2815"
