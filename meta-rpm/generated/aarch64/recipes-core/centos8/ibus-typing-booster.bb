SUMMARY = "generated recipe based on ibus-typing-booster srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_ibus-typing-booster = "bash cldr-emoji-annotation ibus m17n-lib platform-python python3-dbus python3-enchant python3-pyxdg unicode-ucd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-typing-booster-2.1.0-3.el8.noarch.rpm \
          "

SRC_URI[ibus-typing-booster.sha256sum] = "cca7a46c2487b57b5a8dcfa58b1befdb887abcf4d6b8ebc9c42f142a87296cdb"
