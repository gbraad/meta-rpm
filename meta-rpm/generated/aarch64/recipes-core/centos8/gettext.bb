SUMMARY = "generated recipe based on gettext srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-3.0"
RPM_LICENSE = "LGPLv2+ and GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libcroco libgcc libunistring libxml2 ncurses pkgconfig-native"
RPM_SONAME_REQ_gettext = "ld-linux-aarch64.so.1 libc.so.6 libcroco-0.6.so.3 libgcc_s.so.1 libgettextlib-0.19.8.1.so libgettextsrc-0.19.8.1.so libglib-2.0.so.0 libgomp.so.1 libm.so.6 libncurses.so.6 libpthread.so.0 libtinfo.so.6 libunistring.so.2 libxml2.so.2"
RDEPENDS_gettext = "bash gettext-libs glib2 glibc info libcroco libgcc libgomp libunistring libxml2 ncurses-libs"
RPROVIDES_gettext-common-devel = "gettext-common-dev (= 0.19.8.1)"
RPM_SONAME_PROV_gettext-devel = "libgnuintl.so.8"
RPM_SONAME_REQ_gettext-devel = "ld-linux-aarch64.so.1 libasprintf.so.0 libc.so.6 libgettextpo.so.0"
RPROVIDES_gettext-devel = "gettext-dev (= 0.19.8.1)"
RDEPENDS_gettext-devel = "bash gettext gettext-common-devel gettext-libs glibc info xz"
RPM_SONAME_PROV_gettext-libs = "libasprintf.so.0 libgettextlib-0.19.8.1.so libgettextpo.so.0 libgettextsrc-0.19.8.1.so"
RPM_SONAME_REQ_gettext-libs = "ld-linux-aarch64.so.1 libc.so.6 libcroco-0.6.so.3 libgcc_s.so.1 libglib-2.0.so.0 libgomp.so.1 libm.so.6 libncurses.so.6 libpthread.so.0 libstdc++.so.6 libtinfo.so.6 libunistring.so.2 libxml2.so.2"
RDEPENDS_gettext-libs = "glib2 glibc libcroco libgcc libgomp libstdc++ libunistring libxml2 ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gettext-0.19.8.1-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gettext-common-devel-0.19.8.1-17.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gettext-devel-0.19.8.1-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gettext-libs-0.19.8.1-17.el8.aarch64.rpm \
          "
SRC_URI = "file://gettext-0.19.8.1-17.el8.patch"

SRC_URI[gettext.sha256sum] = "5f0c37488d3017b052039ddb8d9189a38c252af97884264959334237109c7e7c"
SRC_URI[gettext-common-devel.sha256sum] = "089ec84f7dae0bc5ea4fcc1f697797f433911eeaa50909a8d3d7c15863252359"
SRC_URI[gettext-devel.sha256sum] = "466aef406af736e74a8d273ae3277aae8b4c439d8d5fd14e438e0eb960685370"
SRC_URI[gettext-libs.sha256sum] = "882f23e0250a2d4aea49abb4ec8e11a9a3869ccdd812c796b6f85341ff9d30a2"
