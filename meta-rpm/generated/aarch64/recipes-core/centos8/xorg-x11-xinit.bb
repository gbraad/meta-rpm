SUMMARY = "generated recipe based on xorg-x11-xinit srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-xinit = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6"
RDEPENDS_xorg-x11-xinit = "bash coreutils glibc libX11 xorg-x11-server-utils xorg-x11-xauth"
RDEPENDS_xorg-x11-xinit-session = "bash"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-xinit-1.3.4-18.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-xinit-session-1.3.4-18.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-xinit.sha256sum] = "ff430fc634014a44d87707bd551262e2e94fa49861755a7dfab57f2117be8f2f"
SRC_URI[xorg-x11-xinit-session.sha256sum] = "3142f5bace2329ec6f5f4fdb528e661aa454d08630d282de4b7f427b9b5e3826"
