SUMMARY = "generated recipe based on libusbmuxd srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libplist pkgconfig-native"
RPM_SONAME_PROV_libusbmuxd = "libusbmuxd.so.4"
RPM_SONAME_REQ_libusbmuxd = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libplist.so.3 libpthread.so.0"
RDEPENDS_libusbmuxd = "glibc libgcc libplist"
RPM_SONAME_REQ_libusbmuxd-devel = "libusbmuxd.so.4"
RPROVIDES_libusbmuxd-devel = "libusbmuxd-dev (= 1.0.10)"
RDEPENDS_libusbmuxd-devel = "libusbmuxd pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libusbmuxd-1.0.10-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libusbmuxd-devel-1.0.10-9.el8.aarch64.rpm \
          "

SRC_URI[libusbmuxd.sha256sum] = "00259f37caabcfc2e6fd8b7457bc7f3370f4e9569042470f4e1288acc064f372"
SRC_URI[libusbmuxd-devel.sha256sum] = "4969404d9443f21f25cfb877df07a431be24c9a026e9ab1e3787a2bb9c6a29cc"
