SUMMARY = "generated recipe based on perl-File-MimeInfo srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-MimeInfo = "perl-Carp perl-Exporter perl-File-BaseDir perl-File-DesktopEntry perl-PathTools perl-interpreter perl-libs shared-mime-info"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-File-MimeInfo-0.28-7.el8.noarch.rpm \
          "

SRC_URI[perl-File-MimeInfo.sha256sum] = "4f4a4912fd8be7169563f8ccd197013c99bafea8f231d44afc34beed98d1c90f"
