SUMMARY = "generated recipe based on perl-Unicode-LineBreak srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libthai perl pkgconfig-native sombok"
RPM_SONAME_REQ_perl-Unicode-LineBreak = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0 libsombok.so.3 libthai.so.0"
RDEPENDS_perl-Unicode-LineBreak = "glibc libthai perl-Carp perl-Encode perl-Exporter perl-MIME-Charset perl-constant perl-interpreter perl-libs sombok"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Unicode-LineBreak-2017.004-6.el8.aarch64.rpm \
          "

SRC_URI[perl-Unicode-LineBreak.sha256sum] = "6791e2d3629947a897398d2765808e4f1cfb11692139d21d5b24fb47cc8c1c64"
