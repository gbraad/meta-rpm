SUMMARY = "generated recipe based on mythes-pl srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-pl = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-pl-1.5-20.el8.noarch.rpm \
          "

SRC_URI[mythes-pl.sha256sum] = "2cf60e7b23292f2d4d2033ea5f8ce6e6f91823aaa0ba1f191f9bdd872593438c"
