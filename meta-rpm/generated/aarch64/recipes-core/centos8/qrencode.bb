SUMMARY = "generated recipe based on qrencode srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libpng pkgconfig-native zlib"
RPM_SONAME_REQ_qrencode = "ld-linux-aarch64.so.1 libc.so.6 libpng16.so.16 libqrencode.so.3 libz.so.1"
RDEPENDS_qrencode = "glibc libpng qrencode-libs zlib"
RPM_SONAME_REQ_qrencode-devel = "libqrencode.so.3"
RPROVIDES_qrencode-devel = "qrencode-dev (= 3.4.4)"
RDEPENDS_qrencode-devel = "pkgconf-pkg-config qrencode-libs"
RPM_SONAME_PROV_qrencode-libs = "libqrencode.so.3"
RPM_SONAME_REQ_qrencode-libs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_qrencode-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qrencode-3.4.4-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qrencode-libs-3.4.4-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/qrencode-devel-3.4.4-5.el8.aarch64.rpm \
          "

SRC_URI[qrencode.sha256sum] = "bd4165c2004f63a3ac268c125689e28f8daa851d2e451c5bb7eaf30129da9365"
SRC_URI[qrencode-devel.sha256sum] = "bc7ce783b3885f9a0e6c7cf4e89536377912d23a825171f702c214fe731f4dd4"
SRC_URI[qrencode-libs.sha256sum] = "fbc7c768234854e9269d372fb682f5bed01572a9c3b523453585c619bd2a0308"
