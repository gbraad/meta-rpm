SUMMARY = "generated recipe based on gnome-screenshot srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo gdk-pixbuf glib-2.0 gtk+3 libcanberra libx11 libxext pkgconfig-native"
RPM_SONAME_REQ_gnome-screenshot = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6 libcairo.so.2 libcanberra-gtk3.so.0 libcanberra.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6"
RDEPENDS_gnome-screenshot = "cairo gdk-pixbuf2 glib2 glibc gtk3 libX11 libXext libcanberra libcanberra-gtk3"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-screenshot-3.26.0-3.el8.aarch64.rpm \
          "

SRC_URI[gnome-screenshot.sha256sum] = "86c12f6ba58b3e610a9c7431b11e916681778320ff40b82c5df5734ea1454e88"
