SUMMARY = "generated recipe based on shared-mime-info srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libxml2 pkgconfig-native"
RPM_SONAME_REQ_shared-mime-info = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libxml2.so.2"
RDEPENDS_shared-mime-info = "bash coreutils glib2 glibc libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/shared-mime-info-1.9-3.el8.aarch64.rpm \
          "

SRC_URI[shared-mime-info.sha256sum] = "955e50447ee8ba78996529e1f9192eab92b25251d236b6e24a61be9787fb7552"
