SUMMARY = "generated recipe based on hunspell-la srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-la = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-la-0.20130331-11.el8.noarch.rpm \
          "

SRC_URI[hunspell-la.sha256sum] = "82321509744f5f8b602d37ac1fff808c22989aa8b99a7270870c8e2307e18673"
