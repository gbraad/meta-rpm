SUMMARY = "generated recipe based on alsa-plugins srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib dbus-libs libsamplerate0 pkgconfig-native pulseaudio speexdsp"
RPM_SONAME_PROV_alsa-plugins-arcamav = "libasound_module_ctl_arcam_av.so"
RPM_SONAME_REQ_alsa-plugins-arcamav = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libpthread.so.0"
RDEPENDS_alsa-plugins-arcamav = "alsa-lib glibc"
RPM_SONAME_PROV_alsa-plugins-maemo = "libasound_module_ctl_dsp_ctl.so libasound_module_pcm_alsa_dsp.so"
RPM_SONAME_REQ_alsa-plugins-maemo = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libdbus-1.so.3 libpthread.so.0"
RDEPENDS_alsa-plugins-maemo = "alsa-lib dbus-libs glibc"
RPM_SONAME_PROV_alsa-plugins-oss = "libasound_module_ctl_oss.so libasound_module_pcm_oss.so"
RPM_SONAME_REQ_alsa-plugins-oss = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6"
RDEPENDS_alsa-plugins-oss = "alsa-lib alsa-utils glibc"
RPM_SONAME_PROV_alsa-plugins-pulseaudio = "libasound_module_conf_pulse.so libasound_module_ctl_pulse.so libasound_module_pcm_pulse.so"
RPM_SONAME_REQ_alsa-plugins-pulseaudio = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libpulse.so.0"
RDEPENDS_alsa-plugins-pulseaudio = "alsa-lib alsa-utils glibc pulseaudio pulseaudio-libs"
RPM_SONAME_PROV_alsa-plugins-samplerate = "libasound_module_rate_samplerate.so"
RPM_SONAME_REQ_alsa-plugins-samplerate = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libsamplerate.so.0"
RDEPENDS_alsa-plugins-samplerate = "alsa-lib alsa-utils glibc libsamplerate"
RPM_SONAME_PROV_alsa-plugins-speex = "libasound_module_pcm_speex.so libasound_module_rate_speexrate.so"
RPM_SONAME_REQ_alsa-plugins-speex = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libspeexdsp.so.1"
RDEPENDS_alsa-plugins-speex = "alsa-lib glibc speex speexdsp"
RPM_SONAME_PROV_alsa-plugins-upmix = "libasound_module_pcm_upmix.so"
RPM_SONAME_REQ_alsa-plugins-upmix = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6"
RDEPENDS_alsa-plugins-upmix = "alsa-lib alsa-utils glibc"
RPM_SONAME_PROV_alsa-plugins-usbstream = "libasound_module_pcm_usb_stream.so"
RPM_SONAME_REQ_alsa-plugins-usbstream = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libpthread.so.0"
RDEPENDS_alsa-plugins-usbstream = "alsa-lib glibc"
RPM_SONAME_PROV_alsa-plugins-vdownmix = "libasound_module_pcm_vdownmix.so"
RPM_SONAME_REQ_alsa-plugins-vdownmix = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6"
RDEPENDS_alsa-plugins-vdownmix = "alsa-lib alsa-utils glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-plugins-arcamav-1.1.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-plugins-maemo-1.1.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-plugins-oss-1.1.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-plugins-pulseaudio-1.1.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-plugins-samplerate-1.1.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-plugins-speex-1.1.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-plugins-upmix-1.1.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-plugins-usbstream-1.1.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/alsa-plugins-vdownmix-1.1.9-1.el8.aarch64.rpm \
          "

SRC_URI[alsa-plugins-arcamav.sha256sum] = "5524d56cf653eb7f418b2757aaaaa4680c4519db17a408d2986cc1f9ce450f9f"
SRC_URI[alsa-plugins-maemo.sha256sum] = "39b2946b08874c0f68b0776a9367fa240c76f3e8c3362ed86ee1483787aa38a0"
SRC_URI[alsa-plugins-oss.sha256sum] = "0d279331277c5fbc94a82eb39474c45f91d966bf9afec56cae817bab994b9c7a"
SRC_URI[alsa-plugins-pulseaudio.sha256sum] = "254a57b2bb68a0e35ecede6704930b93d00f168711e174728b3d800fde501e69"
SRC_URI[alsa-plugins-samplerate.sha256sum] = "c2dd14900918f8e483bc1f1632d3eb5a119e9174554dda0cfdc941adfd900d9f"
SRC_URI[alsa-plugins-speex.sha256sum] = "1daf8400f4c08c1376373cde5b1dad34c3f89e47e12734596ce5e79dcc455559"
SRC_URI[alsa-plugins-upmix.sha256sum] = "def149e555db8697518d7e98b4e7b508823b459e94827ce30ebf2279842b8b46"
SRC_URI[alsa-plugins-usbstream.sha256sum] = "be8dddc8b255fd0e9f64460fa0f201e1916bf3c3a5d2dc4f765887778a5d4f95"
SRC_URI[alsa-plugins-vdownmix.sha256sum] = "66525004eddd545ff1dd7f447c82467eb81df37893f6496815c791487d0c4c86"
