SUMMARY = "generated recipe based on hunspell-ht srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ht = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ht-0.06-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-ht.sha256sum] = "78e9ca3ae145615d453e25ca06c6dce30517f9b58c667009acf36a85c69ac57b"
