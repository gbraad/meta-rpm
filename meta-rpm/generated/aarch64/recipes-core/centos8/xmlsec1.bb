SUMMARY = "generated recipe based on xmlsec1 srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gnutls gnutls-libs libgcrypt libgpg-error libtool-ltdl libxml2 libxslt openssl pkgconfig-native"
RPM_SONAME_PROV_xmlsec1 = "libxmlsec1.so.1"
RPM_SONAME_REQ_xmlsec1 = "ld-linux-aarch64.so.1 libc.so.6 libltdl.so.7 libm.so.6 libxml2.so.2 libxslt.so.1"
RDEPENDS_xmlsec1 = "glibc libtool-ltdl libxml2 libxslt"
RPM_SONAME_REQ_xmlsec1-devel = "libxmlsec1.so.1"
RPROVIDES_xmlsec1-devel = "xmlsec1-dev (= 1.2.25)"
RDEPENDS_xmlsec1-devel = "bash libxml2-devel libxslt-devel openssl-devel pkgconf-pkg-config xmlsec1"
RPM_SONAME_PROV_xmlsec1-gcrypt = "libxmlsec1-gcrypt.so.1"
RPM_SONAME_REQ_xmlsec1-gcrypt = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0 libltdl.so.7 libm.so.6 libxml2.so.2 libxmlsec1.so.1 libxslt.so.1"
RDEPENDS_xmlsec1-gcrypt = "glibc libgcrypt libgpg-error libtool-ltdl libxml2 libxslt xmlsec1"
RPM_SONAME_PROV_xmlsec1-gnutls = "libxmlsec1-gnutls.so.1"
RPM_SONAME_REQ_xmlsec1-gnutls = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcrypt.so.20 libgnutls.so.30 libgpg-error.so.0 libltdl.so.7 libm.so.6 libxml2.so.2 libxmlsec1-gcrypt.so.1 libxmlsec1.so.1 libxslt.so.1"
RDEPENDS_xmlsec1-gnutls = "glibc gnutls libgcrypt libgpg-error libtool-ltdl libxml2 libxslt xmlsec1 xmlsec1-gcrypt"
RPROVIDES_xmlsec1-gnutls-devel = "xmlsec1-gnutls-dev (= 1.2.25)"
RDEPENDS_xmlsec1-gnutls-devel = "gnutls-devel libgcrypt-devel libxml2-devel libxslt-devel pkgconf-pkg-config xmlsec1-devel xmlsec1-openssl-devel"
RPM_SONAME_PROV_xmlsec1-openssl = "libxmlsec1-openssl.so.1"
RPM_SONAME_REQ_xmlsec1-openssl = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libltdl.so.7 libm.so.6 libssl.so.1.1 libxml2.so.2 libxmlsec1.so.1 libxslt.so.1"
RDEPENDS_xmlsec1-openssl = "glibc libtool-ltdl libxml2 libxslt openssl-libs xmlsec1"
RPROVIDES_xmlsec1-openssl-devel = "xmlsec1-openssl-dev (= 1.2.25)"
RDEPENDS_xmlsec1-openssl-devel = "libxml2-devel libxslt-devel pkgconf-pkg-config xmlsec1-devel xmlsec1-openssl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xmlsec1-1.2.25-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xmlsec1-openssl-1.2.25-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmlsec1-devel-1.2.25-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmlsec1-gcrypt-1.2.25-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmlsec1-gnutls-1.2.25-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmlsec1-gnutls-devel-1.2.25-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xmlsec1-openssl-devel-1.2.25-4.el8.aarch64.rpm \
          "

SRC_URI[xmlsec1.sha256sum] = "8d43325351b7a53aa7bcdd468da0378529c4766f24646d1e1944c225126e9db3"
SRC_URI[xmlsec1-devel.sha256sum] = "37aca8feab0c8b8366a420918edcbaa32ca4a22ac682eeb7374ddea9fdc24353"
SRC_URI[xmlsec1-gcrypt.sha256sum] = "ae928e7a3575aaa312c0dd2e58ede704dd4935a0bf2c8e78fbe3de3dbfdc1cf2"
SRC_URI[xmlsec1-gnutls.sha256sum] = "7b17445271ab7eadefacf1cf00a946b6a3333fabbf1f46d3c256ea26cad06a0e"
SRC_URI[xmlsec1-gnutls-devel.sha256sum] = "5e349991bd6c3c3ca2fa21d9ed05a76807ad5d5f5dc3e4abe5c0de9c2efc10c3"
SRC_URI[xmlsec1-openssl.sha256sum] = "680345884f87c34b376407534d4be686168e1ef16b3527d81d90b4e514bb4c8d"
SRC_URI[xmlsec1-openssl-devel.sha256sum] = "699c3f1b614e831e085e9ffb19debd7ea4e495839659d1bb0ca708a517bfbb6e"
