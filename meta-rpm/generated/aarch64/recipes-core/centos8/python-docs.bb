SUMMARY = "generated recipe based on python-docs srpm"
DESCRIPTION = "Description"
LICENSE = "Python-2.0"
RPM_LICENSE = "Python"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-docs-3.6.7-2.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python3-docs.sha256sum] = "cab19004bb040cdb1779249412157ba1a65fb55adb377626c796435932f4fb44"
