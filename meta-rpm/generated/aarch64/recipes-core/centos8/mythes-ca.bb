SUMMARY = "generated recipe based on mythes-ca srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-ca = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-ca-1.5.0-15.el8.noarch.rpm \
          "

SRC_URI[mythes-ca.sha256sum] = "459cbf7997a508e3556812fc875c3a65c8692808d78c059c03c5c0205c585d33"
