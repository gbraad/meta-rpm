SUMMARY = "generated recipe based on efi-rpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_efi-filesystem = "filesystem"
RDEPENDS_efi-srpm-macros = "rpm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/efi-srpm-macros-3-2.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/efi-filesystem-3-2.el8.noarch.rpm \
          "

SRC_URI[efi-filesystem.sha256sum] = "e5961ed94f6609f99d30aed65883764750b738ac448f7e1eca6e046743a7a5ef"
SRC_URI[efi-srpm-macros.sha256sum] = "dcd6713754e133c0b55c3a6c36d666c22c6b635a4f88d2e205e50154f33f8ee2"
