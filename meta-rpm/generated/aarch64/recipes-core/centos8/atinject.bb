SUMMARY = "generated recipe based on atinject srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_atinject = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_atinject-javadoc = "javapackages-filesystem"
RDEPENDS_atinject-tck = "atinject java-1.8.0-openjdk-headless javapackages-filesystem junit"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/atinject-1-28.20100611svn86.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/atinject-javadoc-1-28.20100611svn86.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/atinject-tck-1-28.20100611svn86.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[atinject.sha256sum] = "cf43732b3a2c3deb58d44f91b5a53dc214e9afab46d9e22d476a63e24b792152"
SRC_URI[atinject-javadoc.sha256sum] = "1546013a350d2be0b93340bf5e3212d1a732cfe9c1a3c67dd83b299356ae07d4"
SRC_URI[atinject-tck.sha256sum] = "831529246089169c50315a97291b2610bbf5e05f29dbd39760e50ffe8bef25fe"
