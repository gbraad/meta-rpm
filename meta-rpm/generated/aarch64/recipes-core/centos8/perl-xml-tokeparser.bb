SUMMARY = "generated recipe based on perl-XML-TokeParser srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-TokeParser = "perl-Carp perl-IO perl-XML-Catalog perl-XML-Parser perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-XML-TokeParser-0.05-25.el8.noarch.rpm \
          "

SRC_URI[perl-XML-TokeParser.sha256sum] = "20b768b2af76fb2a67b27cab8e56bbfabfc3f8a055886756e8af48a78c9fba81"
