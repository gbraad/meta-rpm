SUMMARY = "generated recipe based on kronosnet srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libknet1 = "libknet.so.1"
RPM_SONAME_REQ_libknet1 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0"
RDEPENDS_libknet1 = "glibc"
RPM_SONAME_REQ_libknet1-devel = "libknet.so.1"
RPROVIDES_libknet1-devel = "libknet1-dev (= 1.10)"
RDEPENDS_libknet1-devel = "libknet1 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libknet1-1.10-6.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libknet1-devel-1.10-6.el8_2.aarch64.rpm \
          "

SRC_URI[libknet1.sha256sum] = "7bd61c8f28c0b98bad92f77ea7cffc95eb3929e9ebf894d4be1b6484b1c41110"
SRC_URI[libknet1-devel.sha256sum] = "d14cc478a410cb725a1c7bec0c953434903678a4d7cb17699e83c60521797881"
