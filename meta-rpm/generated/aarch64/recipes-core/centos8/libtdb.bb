SUMMARY = "generated recipe based on libtdb srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxcrypt pkgconfig-native platform-python3"
RPM_SONAME_PROV_libtdb = "libtdb.so.1"
RPM_SONAME_REQ_libtdb = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libpthread.so.0"
RDEPENDS_libtdb = "glibc libxcrypt"
RPM_SONAME_REQ_libtdb-devel = "libtdb.so.1"
RPROVIDES_libtdb-devel = "libtdb-dev (= 1.4.2)"
RDEPENDS_libtdb-devel = "libtdb pkgconf-pkg-config"
RPM_SONAME_REQ_python3-tdb = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libtdb.so.1 libutil.so.1"
RDEPENDS_python3-tdb = "glibc libtdb libxcrypt platform-python python3-libs"
RPM_SONAME_REQ_tdb-tools = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libpthread.so.0 libtdb.so.1"
RDEPENDS_tdb-tools = "glibc libtdb libxcrypt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtdb-1.4.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtdb-devel-1.4.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-tdb-1.4.2-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tdb-tools-1.4.2-2.el8.aarch64.rpm \
          "

SRC_URI[libtdb.sha256sum] = "a5523684a410fb5de59ceaf7c2cf05d3b31d987a2bc6c1dadea216807ac14dc1"
SRC_URI[libtdb-devel.sha256sum] = "150730e1f108ac4b745de4105f24245a278006462eb6a64c4a6f2dd8eb3dd1c7"
SRC_URI[python3-tdb.sha256sum] = "0f10fc336411cc0799aa56f4956d44fe4e0d1703833a36f4ddf85db84bfbbef7"
SRC_URI[tdb-tools.sha256sum] = "6d6ff141cb4ccc146d308f5c93de0f63859717243447483bc55c83dbfb9e6557"
