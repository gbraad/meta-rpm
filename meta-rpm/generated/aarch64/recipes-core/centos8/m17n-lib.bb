SUMMARY = "generated recipe based on m17n-lib srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libthai libxml2 pkgconfig-native xz zlib"
RPM_SONAME_PROV_m17n-lib = "libm17n-core.so.0 libm17n-flt.so.0 libm17n.so.0"
RPM_SONAME_REQ_m17n-lib = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libthai.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_m17n-lib = "glibc libthai libxml2 m17n-db xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/m17n-lib-1.8.0-2.el8.aarch64.rpm \
          "

SRC_URI[m17n-lib.sha256sum] = "db1e57e1e6a9b5f2f4728aca309bbbb7b6d1c0b417fdafd5bb6ede6588842a07"
