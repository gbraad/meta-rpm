SUMMARY = "generated recipe based on mesa-libGLw srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libglvnd libx11 libxt mesa motif pkgconfig-native"
RPM_SONAME_PROV_mesa-libGLw = "libGLw.so.1"
RPM_SONAME_REQ_mesa-libGLw = "ld-linux-aarch64.so.1 libGL.so.1 libX11.so.6 libXm.so.4 libXt.so.6 libc.so.6"
RDEPENDS_mesa-libGLw = "glibc libX11 libXt libglvnd-glx motif"
RPM_SONAME_REQ_mesa-libGLw-devel = "libGLw.so.1"
RPROVIDES_mesa-libGLw-devel = "mesa-libGLw-dev (= 8.0.0)"
RDEPENDS_mesa-libGLw-devel = "libX11-devel libXt-devel libglvnd-devel mesa-libGL-devel mesa-libGLw motif-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libGLw-8.0.0-18.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libGLw-devel-8.0.0-18.el8.aarch64.rpm \
          "

SRC_URI[mesa-libGLw.sha256sum] = "33f3296a55afc43e117e604e92d395ffa6a35de6c28b64f0d5bba5ec9363d6ad"
SRC_URI[mesa-libGLw-devel.sha256sum] = "55df0d3c533eaf5e97a3f06ca28899c139f26a090d075d0e2f3c215e54971ef1"
