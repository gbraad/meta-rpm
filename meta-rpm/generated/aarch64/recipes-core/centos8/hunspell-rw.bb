SUMMARY = "generated recipe based on hunspell-rw srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-rw = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-rw-0.20050109-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-rw.sha256sum] = "b1490a22e17b1c6856b48b2fa2151c4526d958333dde625949dc419134a5aa85"
