SUMMARY = "generated recipe based on boom-boot srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_boom-boot = "boom-boot-conf platform-python python3-boom"
RDEPENDS_boom-boot-grub2 = "bash"
RDEPENDS_python3-boom = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/boom-boot-1.0-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/boom-boot-conf-1.0-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/boom-boot-grub2-1.0-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-boom-1.0-1.el8.noarch.rpm \
          "

SRC_URI[boom-boot.sha256sum] = "ff200a831d9b797f5017de4397c40b2874d895f3c5ff0b4c20ef39dc1959604f"
SRC_URI[boom-boot-conf.sha256sum] = "f1f29b50ba7ee5ce65d2fad48cc7f0e71e21b043dee81c6ac0ea7a6ea5e0cd1d"
SRC_URI[boom-boot-grub2.sha256sum] = "d5af3a9f72f0d928c46c0026bb92d4c48a20b3b0877b28eef9a7af748a03e712"
SRC_URI[python3-boom.sha256sum] = "ce890bfa9b44759112ec2ab904168b3994e567f69473ef7748ecf9ce86e257fc"
