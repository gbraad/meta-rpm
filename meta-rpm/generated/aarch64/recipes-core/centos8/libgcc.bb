SUMMARY = "generated recipe based on gcc srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GPL-3.0 & GPL-2.0 & LGPL-2.0 & BSD"
RPM_LICENSE = "GPLv3+ and GPLv3+ with exceptions and GPLv2+ with exceptions and LGPLv2+ and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libgcc = "libgcc_s.so.1"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgcc-8.3.1-5.el8.0.2.aarch64.rpm \
          "

SRC_URI[libgcc.sha256sum] = "9bb31907f761b2fb5eb908374c13aee5ae0b31e3abfba1e0e729e7c2acaf3a74"
