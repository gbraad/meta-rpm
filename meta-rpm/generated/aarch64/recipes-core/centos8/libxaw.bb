SUMMARY = "generated recipe based on libXaw srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 libxext libxmu libxpm libxt pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXaw = "libXaw.so.7"
RPM_SONAME_REQ_libXaw = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXmu.so.6 libXpm.so.4 libXt.so.6 libc.so.6"
RDEPENDS_libXaw = "glibc libX11 libXext libXmu libXpm libXt"
RPM_SONAME_REQ_libXaw-devel = "libXaw.so.7"
RPROVIDES_libXaw-devel = "libXaw-dev (= 1.0.13)"
RDEPENDS_libXaw-devel = "libX11-devel libXaw libXext-devel libXmu-devel libXpm-devel libXt-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXaw-1.0.13-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXaw-devel-1.0.13-10.el8.aarch64.rpm \
          "

SRC_URI[libXaw.sha256sum] = "e9587ec1756e54a046483051db133aeb01b053b5390730587b379f66fce9b43a"
SRC_URI[libXaw-devel.sha256sum] = "524be0918bfacdda9915a11338fceb0af60e8281ab1d0ace740fdbf9e9a78d62"
