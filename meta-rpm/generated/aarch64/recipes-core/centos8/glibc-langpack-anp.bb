SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-anp = "locale-base-anp-in (= 2.28) virtual-locale-anp (= 2.28) virtual-locale-anp-in (= 2.28)"
RDEPENDS_glibc-langpack-anp = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-anp-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-anp.sha256sum] = "22967e2202e7b679f9e1c357b82553e8b435d616cd6550dfa8f5c52c9e60edb6"
