SUMMARY = "generated recipe based on liboggz srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libogg pkgconfig-native"
RPM_SONAME_PROV_liboggz = "liboggz.so.2"
RPM_SONAME_REQ_liboggz = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libogg.so.0"
RDEPENDS_liboggz = "bash glibc libogg"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/liboggz-1.1.1-14.el8.aarch64.rpm \
          "

SRC_URI[liboggz.sha256sum] = "b8c5f3365ac144b28a3057a12978b4784d98e89ff986b8901a738b83a282001b"
