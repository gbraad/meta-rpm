SUMMARY = "generated recipe based on dnssec-trigger srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo fontconfig freetype gdk-pixbuf glib-2.0 gtk2 ldns openssl pango pkgconfig-native"
RPM_SONAME_REQ_dnssec-trigger = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libldns.so.2 libpthread.so.0 libssl.so.1.1"
RDEPENDS_dnssec-trigger = "NetworkManager NetworkManager-libnm bash e2fsprogs glibc ldns openssl openssl-libs platform-python systemd unbound"
RPM_SONAME_REQ_dnssec-trigger-panel = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libcrypto.so.1.1 libfontconfig.so.1 libfreetype.so.6 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libgtk-x11-2.0.so.0 libldns.so.2 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 libssl.so.1.1"
RDEPENDS_dnssec-trigger-panel = "atk cairo dnssec-trigger fontconfig freetype gdk-pixbuf2 glib2 glibc gtk2 ldns openssl-libs pango xdg-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dnssec-trigger-0.15-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dnssec-trigger-panel-0.15-4.el8.aarch64.rpm \
          "

SRC_URI[dnssec-trigger.sha256sum] = "37f09a73908dfefa23b30372c256ad294ee8f5c5b5c591fe313e143141e026ff"
SRC_URI[dnssec-trigger-panel.sha256sum] = "a30fb11d4ab9c7cc5feefac53a71fd0f3f05d024793cb6c8a693cf5c5ca2c6fd"
