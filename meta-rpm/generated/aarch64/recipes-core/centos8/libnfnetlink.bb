SUMMARY = "generated recipe based on libnfnetlink srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libnfnetlink = "libnfnetlink.so.0"
RPM_SONAME_REQ_libnfnetlink = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libnfnetlink = "glibc"
RPM_SONAME_REQ_libnfnetlink-devel = "libnfnetlink.so.0"
RPROVIDES_libnfnetlink-devel = "libnfnetlink-dev (= 1.0.1)"
RDEPENDS_libnfnetlink-devel = "kernel-headers libnfnetlink pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnfnetlink-1.0.1-13.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libnfnetlink-devel-1.0.1-13.el8.aarch64.rpm \
          "

SRC_URI[libnfnetlink.sha256sum] = "8422fbc84108abc9a89fe98cef9cd18ad1788b4dc6a9ec0bba1836b772fcaeda"
SRC_URI[libnfnetlink-devel.sha256sum] = "e1a2282b0a9744079b3f9af7aee6e795962bbcce184d77a28814b4929dbf1239"
