SUMMARY = "generated recipe based on python-networkx srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-networkx = "python3-networkx-core"
RDEPENDS_python3-networkx-core = "platform-python python3-decorator python3-pyparsing python3-pyyaml python3-scipy"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-networkx-1.11-16.1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-networkx-core-1.11-16.1.el8.noarch.rpm \
          "

SRC_URI[python3-networkx.sha256sum] = "e8349de078580500e7d1889dd3408fb0468946ca98258f10c2c53d6e7b65ea99"
SRC_URI[python3-networkx-core.sha256sum] = "823bb23f8e29c694ea476cc1cb570a3ae1c22451efedb4c6a8e9ac004adc786a"
