SUMMARY = "generated recipe based on frr srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "c-ares json-c libcap libgcc libxcrypt libyang net-snmp openssl pkgconfig-native readline rpm systemd-libs"
RPM_SONAME_PROV_frr = "libfrr.so.0 libfrrsnmp.so.0"
RPM_SONAME_REQ_frr = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcares.so.2 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libjson-c.so.4 libm.so.6 libnetsnmp.so.35 libnetsnmpagent.so.35 libnetsnmpmibs.so.35 libpthread.so.0 libreadline.so.7 librpm.so.8 librpmio.so.8 librt.so.1 libsystemd.so.0 libyang.so.0.16"
RDEPENDS_frr = "bash c-ares glibc info json-c libcap libgcc libxcrypt libyang ncurses net-snmp net-snmp-agent-libs net-snmp-libs openssl-libs platform-python readline rpm-libs systemd systemd-libs"
RDEPENDS_frr-contrib = "frr"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/frr-7.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/frr-contrib-7.0-5.el8.aarch64.rpm \
          "

SRC_URI[frr.sha256sum] = "5bdc5a3859c436b0b62f45c7d68566c2c2355518be4f5803fb06f10fde820990"
SRC_URI[frr-contrib.sha256sum] = "f4483b12b6737d799e0cc3950c09901da035f42c5ea6b0f10c68aa97619a4a21"
