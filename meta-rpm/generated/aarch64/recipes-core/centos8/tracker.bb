SUMMARY = "generated recipe based on tracker srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 icu json-glib libsoup-2.4 libuuid libxml2 networkmanager pkgconfig-native sqlite3 zlib"
RPM_SONAME_PROV_tracker = "libtracker-common.so.0 libtracker-control-2.0.so.0 libtracker-data.so.0 libtracker-miner-2.0.so.0 libtracker-sparql-2.0.so.0"
RPM_SONAME_REQ_tracker = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libjson-glib-1.0.so.0 libm.so.6 libnm.so.0 libpthread.so.0 libsoup-2.4.so.1 libsqlite3.so.0 libuuid.so.1 libxml2.so.2 libz.so.1"
RDEPENDS_tracker = "NetworkManager-libnm bash glib2 glibc json-glib libicu libsoup libuuid libxml2 sqlite-libs systemd zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tracker-2.1.5-1.el8.aarch64.rpm \
          "

SRC_URI[tracker.sha256sum] = "6a129561aa23a5480f50dada1109549b3dd925314a25097070313e9f0a3adb01"
