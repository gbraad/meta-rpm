SUMMARY = "generated recipe based on libgsf srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 gdk-pixbuf glib-2.0 libxml2 pkgconfig-native zlib"
RPM_SONAME_PROV_libgsf = "libgsf-1.so.114"
RPM_SONAME_REQ_libgsf = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_libgsf = "bzip2-libs gdk-pixbuf2 glib2 glibc libxml2 zlib"
RPM_SONAME_REQ_libgsf-devel = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgsf-1.so.114 libxml2.so.2"
RPROVIDES_libgsf-devel = "libgsf-dev (= 1.14.41)"
RDEPENDS_libgsf-devel = "glib2 glib2-devel glibc libgsf libxml2 libxml2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgsf-1.14.41-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libgsf-devel-1.14.41-5.el8.aarch64.rpm \
          "

SRC_URI[libgsf.sha256sum] = "23701dd0c3ef825a11d643d91710caba47fb07ecca1f2e9c5196387153e9a713"
SRC_URI[libgsf-devel.sha256sum] = "934065868966234adc16af2421581725a11f933f87d7da3823b802992a65ece4"
