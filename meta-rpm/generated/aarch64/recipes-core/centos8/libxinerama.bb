SUMMARY = "generated recipe based on libXinerama srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "xinerama"
DEPENDS = "libx11 libxext pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXinerama = "libXinerama.so.1"
RPM_SONAME_REQ_libXinerama = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXinerama = "glibc libX11 libXext"
RPM_SONAME_REQ_libXinerama-devel = "libXinerama.so.1"
RPROVIDES_libXinerama-devel = "libXinerama-dev (= 1.1.4)"
RDEPENDS_libXinerama-devel = "libX11-devel libXext-devel libXinerama pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXinerama-1.1.4-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXinerama-devel-1.1.4-1.el8.aarch64.rpm \
          "

SRC_URI[libXinerama.sha256sum] = "ff7e6d4e4a768e0209b0f505a56b0bcde1e3e3a56b32dee30fcc4ab9f2af7980"
SRC_URI[libXinerama-devel.sha256sum] = "95fdae6748bc4baaa30b8801073bc5c068f31c1cba88ba915a6d368d043404aa"
