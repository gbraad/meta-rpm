SUMMARY = "generated recipe based on rubygem-pg srpm"
DESCRIPTION = "Description"
LICENSE = "(BSD | Ruby) & PostgreSQL"
RPM_LICENSE = "(BSD or Ruby) and PostgreSQL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libpq pkgconfig-native ruby"
RPM_SONAME_REQ_rubygem-pg = "ld-linux-aarch64.so.1 libc.so.6 libpq.so.5 libruby.so.2.5"
RDEPENDS_rubygem-pg = "glibc libpq ruby-libs rubygems"
RDEPENDS_rubygem-pg-doc = "rubygem-pg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rubygem-pg-1.0.0-2.module_el8.1.0+214+9be47fd7.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rubygem-pg-doc-1.0.0-2.module_el8.1.0+214+9be47fd7.noarch.rpm \
          "

SRC_URI[rubygem-pg.sha256sum] = "822310ea794b3c9436e5aa3ed398b3922f37798a49196506e0400cf0b1b5ff25"
SRC_URI[rubygem-pg-doc.sha256sum] = "23409709eef9602d2f722cb41a5c26573bacb9366e8cde5a44d326a27daf7502"
