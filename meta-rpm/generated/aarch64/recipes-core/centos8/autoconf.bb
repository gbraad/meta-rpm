SUMMARY = "generated recipe based on autoconf srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GFDL-1.1"
RPM_LICENSE = "GPLv2+ and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_autoconf = "bash emacs-filesystem info m4 perl-Carp perl-Data-Dumper perl-Errno perl-Exporter perl-File-Path perl-Getopt-Long perl-IO perl-PathTools perl-Text-ParseWords perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/autoconf-2.69-27.el8.noarch.rpm \
          "
SRC_URI = "file://autoconf-2.69-27.el8.patch file://autoconf-2.69-27.el8-m4.patch"

SRC_URI[autoconf.sha256sum] = "8654abe1aef0a282022d4eb259c574b0ec6bf68c22542e1be83b8ef0008d218a"
