SUMMARY = "generated recipe based on glassfish-legal srpm"
DESCRIPTION = "Description"
LICENSE = "CDDL-1.0 | GPL-2.0"
RPM_LICENSE = "CDDL or GPLv2 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_glassfish-legal = "glassfish-master-pom java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glassfish-legal-1.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[glassfish-legal.sha256sum] = "1ef96f60c5321b49ff4d261d5770ec045f38ac85556928198c78e86a3921b045"
