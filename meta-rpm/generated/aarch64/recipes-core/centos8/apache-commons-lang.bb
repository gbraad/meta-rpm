SUMMARY = "generated recipe based on apache-commons-lang srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-lang = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-lang-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-lang-2.6-21.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-lang-javadoc-2.6-21.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-lang.sha256sum] = "4e1ac6555e16605940b6b0c63d64ed652bee0eb9a0c62276fb72c9a1de429388"
SRC_URI[apache-commons-lang-javadoc.sha256sum] = "b28ed260926b49913f973e7487f91eaea741cd587004b80dc0cc4f179ae54160"
