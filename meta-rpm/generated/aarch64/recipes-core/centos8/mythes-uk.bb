SUMMARY = "generated recipe based on mythes-uk srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | LGPL-2.0) & (GPL-2.0 | LGPL-2.0 | MPL-1.1) & GPL-2.0"
RPM_LICENSE = "(GPLv2+ or LGPLv2+) and (GPLv2+ or LGPLv2+ or MPLv1.1) and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-uk = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-uk-1.6.5-14.el8.noarch.rpm \
          "

SRC_URI[mythes-uk.sha256sum] = "fa24d199df55acb4e0259183ecdff532a7013e6e4904728344c6e539be598304"
