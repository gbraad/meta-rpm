SUMMARY = "generated recipe based on hyphen-mi srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-mi = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-mi-0.20080630-16.el8.noarch.rpm \
          "

SRC_URI[hyphen-mi.sha256sum] = "20640f0b73c079ea403b4967341dd9c3652df69cfc8b3d18a24638058dc89984"
