SUMMARY = "generated recipe based on xcb-util-wm srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxcb pkgconfig-native"
RPM_SONAME_PROV_xcb-util-wm = "libxcb-ewmh.so.2 libxcb-icccm.so.4"
RPM_SONAME_REQ_xcb-util-wm = "ld-linux-aarch64.so.1 libc.so.6 libxcb.so.1"
RDEPENDS_xcb-util-wm = "glibc libxcb"
RPM_SONAME_REQ_xcb-util-wm-devel = "libxcb-ewmh.so.2 libxcb-icccm.so.4"
RPROVIDES_xcb-util-wm-devel = "xcb-util-wm-dev (= 0.4.1)"
RDEPENDS_xcb-util-wm-devel = "libxcb-devel pkgconf-pkg-config xcb-util-wm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xcb-util-wm-0.4.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xcb-util-wm-devel-0.4.1-12.el8.aarch64.rpm \
          "

SRC_URI[xcb-util-wm.sha256sum] = "a323873530aa1c3297c284253ec1ff4d3592410bc87d0c254973ad12fa7f999d"
SRC_URI[xcb-util-wm-devel.sha256sum] = "23e1c33ea446df500a0635a8133234057f72b87e840ff212f6472fae0c844f7f"
