SUMMARY = "generated recipe based on rpm-ostree srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "curl glib-2.0 gpgme json-c json-glib libarchive libcap libgcc libgpg-error libmodulemd librepo libsolv openssl ostree pkgconfig-native polkit rpm sqlite3 systemd-libs util-linux"
RPM_SONAME_REQ_rpm-ostree = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 libcap.so.2 libcrypto.so.1.1 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libjson-c.so.4 libjson-glib-1.0.so.0 libm.so.6 libmodulemd.so.1 libostree-1.so.1 libpolkit-gobject-1.so.0 libpthread.so.0 librepo.so.0 librpm.so.8 librpmio.so.8 librpmostree-1.so.1 libsmartcols.so.1 libsolv.so.1 libsolvext.so.1 libsqlite3.so.0 libssl.so.1.1 libstdc++.so.6 libsystemd.so.0"
RDEPENDS_rpm-ostree = "bash bubblewrap fuse glib2 glibc gpgme json-c json-glib libarchive libcap libcurl libgcc libgpg-error libmodulemd1 librepo libsmartcols libsolv libstdc++ openssl-libs ostree ostree-libs polkit-libs rpm-libs rpm-ostree-libs sqlite-libs systemd-libs"
RPM_SONAME_PROV_rpm-ostree-libs = "librpmostree-1.so.1"
RPM_SONAME_REQ_rpm-ostree-libs = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 libcap.so.2 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libm.so.6 libostree-1.so.1 libpolkit-gobject-1.so.0 libpthread.so.0 librepo.so.0 librpm.so.8 librpmio.so.8 libsolv.so.1 libsystemd.so.0"
RDEPENDS_rpm-ostree-libs = "glib2 glibc json-glib libarchive libcap libgcc librepo libsolv ostree-libs polkit-libs rpm-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rpm-ostree-2019.6-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rpm-ostree-libs-2019.6-8.el8.aarch64.rpm \
          "

SRC_URI[rpm-ostree.sha256sum] = "7ba6ca90126ee55ea8c8e2ba269287d7a787b3ee5d20f4509ba8d4fd06d76478"
SRC_URI[rpm-ostree-libs.sha256sum] = "66d6990012229a7ab6ee565b563079d556a07d67efb063455ec20a75ed52e940"
