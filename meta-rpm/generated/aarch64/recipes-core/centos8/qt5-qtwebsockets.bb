SUMMARY = "generated recipe based on qt5-qtwebsockets srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtwebsockets = "libQt5WebSockets.so.5"
RPM_SONAME_REQ_qt5-qtwebsockets = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Network.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtwebsockets = "glibc libgcc libstdc++ qt5-qtbase"
RPM_SONAME_REQ_qt5-qtwebsockets-devel = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5WebSockets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RPROVIDES_qt5-qtwebsockets-devel = "qt5-qtwebsockets-dev (= 5.12.5)"
RDEPENDS_qt5-qtwebsockets-devel = "cmake-filesystem glibc libgcc libstdc++ pkgconf-pkg-config qt5-qtbase qt5-qtbase-devel qt5-qtdeclarative qt5-qtwebsockets"
RPM_SONAME_REQ_qt5-qtwebsockets-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5WebSockets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtwebsockets-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtwebsockets"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtwebsockets-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtwebsockets-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtwebsockets-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtwebsockets.sha256sum] = "438fa5e7bd024a7ab9e81762f5ab4625f593318d2d66c8d8f0da27ba0f4f03bb"
SRC_URI[qt5-qtwebsockets-devel.sha256sum] = "aa2714fc12237dbc88899ba0190b4a3656c52f175ab6c18d55ce99ef5398ef0e"
SRC_URI[qt5-qtwebsockets-examples.sha256sum] = "06a647723483da89b7b77d4ca6f75fa8fe7e3514d84e40838f1d0252aabfc61e"
