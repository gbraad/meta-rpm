SUMMARY = "generated recipe based on libpeas srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gobject-introspection gtk+3 pango pkgconfig-native platform-python3"
RPM_SONAME_PROV_libpeas = "libpeas-1.0.so.0"
RPM_SONAME_REQ_libpeas = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_libpeas = "glib2 glibc gobject-introspection"
RPM_SONAME_PROV_libpeas-devel = "libhelloworld.so libsecondtime.so"
RPM_SONAME_REQ_libpeas-devel = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpeas-gtk-1.0.so.0 libpthread.so.0"
RPROVIDES_libpeas-devel = "libpeas-dev (= 1.22.0)"
RDEPENDS_libpeas-devel = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glib2-devel glibc gobject-introspection gobject-introspection-devel gtk3 gtk3-devel libpeas libpeas-gtk pango pkgconf-pkg-config"
RPM_SONAME_PROV_libpeas-gtk = "libpeas-gtk-1.0.so.0"
RPM_SONAME_REQ_libpeas-gtk = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpeas-1.0.so.0 libpthread.so.0"
RDEPENDS_libpeas-gtk = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gobject-introspection gtk3 libpeas pango"
RPM_SONAME_PROV_libpeas-loader-python3 = "libpython3loader.so"
RPM_SONAME_REQ_libpeas-loader-python3 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgio-2.0.so.0 libgirepository-1.0.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpeas-1.0.so.0 libpthread.so.0 libpython3.6m.so.1.0 libutil.so.1"
RDEPENDS_libpeas-loader-python3 = "glib2 glibc gobject-introspection libpeas python3-gobject python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpeas-gtk-1.22.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpeas-loader-python3-1.22.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libpeas-1.22.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libpeas-devel-1.22.0-6.el8.aarch64.rpm \
          "

SRC_URI[libpeas.sha256sum] = "ca887fd6f18e8f3d23bb696dd6da327912277d8c9e9ea6e0e236f4647883e555"
SRC_URI[libpeas-devel.sha256sum] = "652ff768b5ce9ffe488a3f0677f2d8921b021f0e33664601a282f608a66d8029"
SRC_URI[libpeas-gtk.sha256sum] = "06deeb375332aa494e320c11f2cf875cba88c9885e2de6284419cba4d1397de5"
SRC_URI[libpeas-loader-python3.sha256sum] = "2543044bf3ed1fa4a5713ef2c2575ba5afff8857063530898d0eff5ff2ff632c"
