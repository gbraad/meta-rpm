SUMMARY = "generated recipe based on perl-B-Lint srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-B-Lint = "perl-Carp perl-Module-Pluggable perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-B-Lint-1.20-11.el8.noarch.rpm \
          "

SRC_URI[perl-B-Lint.sha256sum] = "9ae66704f7209e7b84d30055cc928f905bbfee24ee4997031fc8926abf0e7f5e"
