SUMMARY = "generated recipe based on mksh srpm"
DESCRIPTION = "Description"
LICENSE = "MirOS & ISC & BSD"
RPM_LICENSE = "MirOS and ISC and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mksh = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mksh = "bash chkconfig glibc grep"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mksh-56c-5.el8.aarch64.rpm \
          "

SRC_URI[mksh.sha256sum] = "68b091fbed40e5484345d88e2e353d8b0ed476f0c955d4308b2cec5bc871726b"
