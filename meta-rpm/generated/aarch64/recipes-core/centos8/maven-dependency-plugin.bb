SUMMARY = "generated recipe based on maven-dependency-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-dependency-plugin = "apache-commons-collections apache-commons-io apache-commons-lang java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact-transfer maven-common-artifact-filters maven-dependency-analyzer maven-dependency-tree maven-doxia-sink-api maven-doxia-sitetools maven-file-management maven-lib maven-reporting-api maven-reporting-impl maven-shared-utils plexus-archiver plexus-classworlds plexus-io plexus-utils"
RDEPENDS_maven-dependency-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-dependency-plugin-3.0.2-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-dependency-plugin-javadoc-3.0.2-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-dependency-plugin.sha256sum] = "7d1f1e0b33a7bf2f6d2cc3d1712e89066cb64dd0e8beabfd2b2f8b122dc013a6"
SRC_URI[maven-dependency-plugin-javadoc.sha256sum] = "790951a3578b6bcbffecfe835a37e318d6f3be88b0b244afd2359a8f4617529f"
