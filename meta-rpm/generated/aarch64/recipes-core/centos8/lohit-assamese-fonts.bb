SUMMARY = "generated recipe based on lohit-assamese-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-assamese-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lohit-assamese-fonts-2.91.5-3.el8.noarch.rpm \
          "

SRC_URI[lohit-assamese-fonts.sha256sum] = "e40f899f91aee1034bc62a63d9c412bc9dcc07b1e760387bb9c1c5d70c34c919"
