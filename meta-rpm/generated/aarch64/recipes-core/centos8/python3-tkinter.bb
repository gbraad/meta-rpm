SUMMARY = "generated recipe based on python3 srpm"
DESCRIPTION = "Description"
LICENSE = "Python-2.0"
RPM_LICENSE = "Python"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 openssl pkgconfig-native tcl tk"
RDEPENDS_python3-idle = "bash platform-python python3-tkinter python36"
RPM_SONAME_REQ_python3-tkinter = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libcrypto.so.1.1 libpthread.so.0 libpython3.6m.so.1.0 libtcl8.6.so libtk8.6.so"
RDEPENDS_python3-tkinter = "glibc libX11 openssl-libs platform-python python3-libs tcl tk"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-idle-3.6.8-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-tkinter-3.6.8-23.el8.aarch64.rpm \
          "

SRC_URI[python3-idle.sha256sum] = "edeb10db080be94ec24e70877a41f74a51ee20aa1c08b7c1213598b2d62f2fa7"
SRC_URI[python3-tkinter.sha256sum] = "9ca079b91e10eb90675186101b4faebb01873fec0bb32d6f0b7083925bbc644e"
