SUMMARY = "generated recipe based on perl-ExtUtils-Install srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-ExtUtils-Install = "perl-Carp perl-Data-Dumper perl-Exporter perl-ExtUtils-MakeMaker perl-File-Path perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-ExtUtils-Install-2.14-4.el8.noarch.rpm \
          "

SRC_URI[perl-ExtUtils-Install.sha256sum] = "9e1a31e0d2ac0c76dd33f7523f91dd7b8e109d999ad30ecd50adf4cdcebe3ed0"
