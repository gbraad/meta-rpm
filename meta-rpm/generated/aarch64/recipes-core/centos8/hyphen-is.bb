SUMMARY = "generated recipe based on hyphen-is srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | CLOSED"
RPM_LICENSE = "LGPLv2+ or SISSL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-is = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-is-0.20030920-19.el8.noarch.rpm \
          "

SRC_URI[hyphen-is.sha256sum] = "2edb61507c859fbc2e1c1cb5a4ced60790f52b876cecb6f89f02ca03dc2e20d3"
