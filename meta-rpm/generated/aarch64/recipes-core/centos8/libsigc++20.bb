SUMMARY = "generated recipe based on libsigc++20 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libsigc++20 = "libsigc-2.0.so.0"
RPM_SONAME_REQ_libsigc++20 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_libsigc++20 = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libsigc++20-devel = "libsigc-2.0.so.0"
RPROVIDES_libsigc++20-devel = "libsigc++20-dev (= 2.10.0)"
RDEPENDS_libsigc++20-devel = "libsigc++20 pkgconf-pkg-config"
RDEPENDS_libsigc++20-doc = "libsigc++20"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libsigc++20-2.10.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsigc++20-devel-2.10.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsigc++20-doc-2.10.0-5.el8.noarch.rpm \
          "

SRC_URI[libsigc++20.sha256sum] = "a60321ece827994b8cd88dcf54268b4a6d40cc7166fa3290ec9216e6ff272583"
SRC_URI[libsigc++20-devel.sha256sum] = "0790b1657ce81623d38eaf39df94442dc3f3d1bed89ede09433c612930b9b5ec"
SRC_URI[libsigc++20-doc.sha256sum] = "b6e7e85a879df543624300a0874cd357ba3bd69a2fb32ce7f5f9252987272b16"
