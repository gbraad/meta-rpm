SUMMARY = "generated recipe based on libgpg-error srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libgpg-error = "libgpg-error.so.0"
RPM_SONAME_REQ_libgpg-error = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libgpg-error = "glibc"
RPM_SONAME_REQ_libgpg-error-devel = "ld-linux-aarch64.so.1 libc.so.6 libgpg-error.so.0"
RPROVIDES_libgpg-error-devel = "libgpg-error-dev (= 1.31)"
RDEPENDS_libgpg-error-devel = "bash glibc info libgpg-error"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgpg-error-1.31-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgpg-error-devel-1.31-1.el8.aarch64.rpm \
          "

SRC_URI[libgpg-error.sha256sum] = "b953729a0a2be24749aeee9f00853fdc3227737971cf052a999a37ac36387cd9"
SRC_URI[libgpg-error-devel.sha256sum] = "42ee89e53166b98869a9f6ef0b9dca72ec116fd40bfe55a29bd8dfe8f9dbed4b"
