SUMMARY = "generated recipe based on libtool srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libtool-ltdl m4 pkgconfig-native"
RDEPENDS_libtool = "autoconf automake bash findutils gcc info sed tar"
RPM_SONAME_REQ_libtool-ltdl-devel = "libltdl.so.7"
RPROVIDES_libtool-ltdl-devel = "libtool-ltdl-dev (= 2.4.6)"
RDEPENDS_libtool-ltdl-devel = "automake bash libtool-ltdl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtool-2.4.6-25.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtool-ltdl-devel-2.4.6-25.el8.aarch64.rpm \
          "
SRC_URI = "file://libtool-2.4.6-25.el8.patch file://libtool-2.4.6-25.el8-sysroot.patch"

SRC_URI[libtool.sha256sum] = "27a9641276339d3659656957019ca7e072611d5d78bf7d2d223908b93e77b919"
SRC_URI[libtool-ltdl-devel.sha256sum] = "ff6081c88dd00e3cf89dbf13cf561020af8f6d0ec3dd25d944b1d0936ae970e4"
