SUMMARY = "generated recipe based on perl-Package-DeprecationManager srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0"
RPM_LICENSE = "Artistic 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Package-DeprecationManager = "perl-Carp perl-Package-Stash perl-Params-Util perl-Scalar-List-Utils perl-Sub-Install perl-Sub-Name perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Package-DeprecationManager-0.17-5.el8.noarch.rpm \
          "

SRC_URI[perl-Package-DeprecationManager.sha256sum] = "dcd63425e2a56f3fb174a9a7301e97a75c5ede03ad2a4295fcde6b87cec42946"
