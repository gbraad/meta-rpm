SUMMARY = "generated recipe based on bubblewrap srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libcap libgcc libselinux pkgconfig-native"
RPM_SONAME_REQ_bubblewrap = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libgcc_s.so.1 libselinux.so.1"
RDEPENDS_bubblewrap = "glibc libcap libgcc libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/bubblewrap-0.4.0-1.el8.aarch64.rpm \
          "

SRC_URI[bubblewrap.sha256sum] = "a445b27920d4bf9e6944a289c3e9cf69cb4396fe5f7f3383aab7dbe53ad6b39f"
