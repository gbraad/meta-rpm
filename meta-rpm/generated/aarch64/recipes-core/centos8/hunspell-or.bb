SUMMARY = "generated recipe based on hunspell-or srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-or = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-or-1.0.0-9.el8.noarch.rpm \
          "

SRC_URI[hunspell-or.sha256sum] = "24d9da2a1a628ce3fe5b255429653a141397329a6d8e785e0ef880e62dc027c1"
