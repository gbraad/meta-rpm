SUMMARY = "generated recipe based on python-pygments srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pygments = "platform-python python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pygments-2.2.0-20.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python3-pygments.sha256sum] = "23f415e293a19f54db9071ecf4e20685df0195c07b17549e96d17bf15f7f9455"
