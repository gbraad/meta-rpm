SUMMARY = "generated recipe based on yelp srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & CLOSED & GPL-2.0"
RPM_LICENSE = "LGPLv2+ and ASL 2.0 and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk bzip2 cairo gdk-pixbuf glib-2.0 gtk+3 libgcrypt libgpg-error libsoup-2.4 libxml2 libxslt pango pkgconfig-native sqlite3 webkit2gtk3 xz"
RPM_SONAME_REQ_yelp = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libbz2.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdl.so.2 libexslt.so.0 libgcrypt.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgtk-3.so.0 libjavascriptcoregtk-4.0.so.18 liblzma.so.5 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsoup-2.4.so.1 libsqlite3.so.0 libwebkit2gtk-4.0.so.37 libxml2.so.2 libxslt.so.1 libyelp.so.0"
RDEPENDS_yelp = "atk bzip2-libs cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libgcrypt libgpg-error libsoup libxml2 libxslt pango sqlite-libs webkit2gtk3 webkit2gtk3-jsc xz-libs yelp-libs yelp-xsl"
RPM_SONAME_REQ_yelp-devel = "libyelp.so.0"
RPROVIDES_yelp-devel = "yelp-dev (= 3.28.1)"
RDEPENDS_yelp-devel = "yelp-libs"
RPM_SONAME_PROV_yelp-libs = "libyelp.so.0 libyelpwebextension.so"
RPM_SONAME_REQ_yelp-libs = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libexslt.so.0 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 liblzma.so.5 libpthread.so.0 libsoup-2.4.so.1 libsqlite3.so.0 libwebkit2gtk-4.0.so.37 libxml2.so.2 libxslt.so.1"
RDEPENDS_yelp-libs = "bzip2-libs glib2 glibc gtk3 libsoup libxml2 libxslt sqlite-libs webkit2gtk3 xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/yelp-3.28.1-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/yelp-libs-3.28.1-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/yelp-devel-3.28.1-3.el8.aarch64.rpm \
          "

SRC_URI[yelp.sha256sum] = "662a9442d58f824d2b25ca3c173b03594e916cf775d7ed2e4e2ec1e2ce15cfbf"
SRC_URI[yelp-devel.sha256sum] = "486d9dea9b76c59b9c9114dae5323a579f78c6f12135a1edd0fe6863777417d5"
SRC_URI[yelp-libs.sha256sum] = "9884a4a614d3baca954a1edf9985ae591909747a314700e61842c68a2b524776"
