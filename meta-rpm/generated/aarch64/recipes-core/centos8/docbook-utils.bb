SUMMARY = "generated recipe based on docbook-utils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_docbook-utils = "bash docbook-dtds docbook-style-dsssl gawk grep lynx perl-Getopt-Long perl-SGMLSpm perl-interpreter perl-libs which"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/docbook-utils-0.6.14-44.el8.noarch.rpm \
          "

SRC_URI[docbook-utils.sha256sum] = "5fd5756bc19b0ca3061d4fb5b46031afe3e59d3bcc8ca181784377a1d3be8630"
