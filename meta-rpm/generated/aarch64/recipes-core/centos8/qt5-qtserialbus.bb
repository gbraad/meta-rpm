SUMMARY = "generated recipe based on qt5-qtserialbus srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtserialport"
RPM_SONAME_PROV_qt5-qtserialbus = "libQt5SerialBus.so.5 libqtpassthrucanbus.so libqtpeakcanbus.so libqtsocketcanbus.so libqttinycanbus.so libqtvirtualcanbus.so"
RPM_SONAME_REQ_qt5-qtserialbus = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Network.so.5 libQt5SerialPort.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtserialbus = "glibc libgcc libstdc++ qt5-qtbase qt5-qtserialport"
RPM_SONAME_REQ_qt5-qtserialbus-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5SerialBus.so.5 libQt5SerialPort.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtserialbus-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtserialbus qt5-qtserialport"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtserialbus-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtserialbus-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtserialbus.sha256sum] = "fae3e1ccd485354296319c6b79b2713874de9a610cdd129fc179264a2395c6fc"
SRC_URI[qt5-qtserialbus-examples.sha256sum] = "f9c41aedbe79d6c39d017b58c835bb0aa63a3ffa80fe940e3037fd0ac698479b"
