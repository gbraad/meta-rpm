SUMMARY = "generated recipe based on plexus-component-api srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-component-api = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-classworlds"
RDEPENDS_plexus-component-api-javadoc = "bash javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-component-api-1.0-0.24.alpha15.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-component-api-javadoc-1.0-0.24.alpha15.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-component-api.sha256sum] = "d8395546bb2da63175ded116a39df83d9dbf956a28c4bd46ef132eaf0fedec17"
SRC_URI[plexus-component-api-javadoc.sha256sum] = "f81158aaf3e4db703bc74b32cc21ba59a669dbd9f44795b5a665d6ce74010ed6"
