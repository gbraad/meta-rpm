SUMMARY = "generated recipe based on util-linux srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libuuid pkgconfig-native"
RPM_SONAME_PROV_libblkid = "libblkid.so.1"
RPM_SONAME_REQ_libblkid = "ld-linux-aarch64.so.1 libc.so.6 libuuid.so.1"
RDEPENDS_libblkid = "bash coreutils glibc libuuid"
RPM_SONAME_REQ_libblkid-devel = "libblkid.so.1"
RPROVIDES_libblkid-devel = "libblkid-dev (= 2.32.1)"
RDEPENDS_libblkid-devel = "libblkid libuuid-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libblkid-2.32.1-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libblkid-devel-2.32.1-22.el8.aarch64.rpm \
          "

SRC_URI[libblkid.sha256sum] = "d99066fcccb1f357e44c2abcb6445c52e7493589931cd0cfc3cf75a6e0208a88"
SRC_URI[libblkid-devel.sha256sum] = "83a74c8dc25768445968d2676b0485de2f709fd62e0f3f04a23918a9fa935ca7"
