SUMMARY = "generated recipe based on lvm2 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libblkid libselinux libsepol pkgconfig-native systemd-libs"
RPM_SONAME_PROV_device-mapper-libs = "libdevmapper.so.1.02"
RPM_SONAME_REQ_device-mapper-libs = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libm.so.6 libpthread.so.0 libselinux.so.1 libsepol.so.1 libudev.so.1"
RDEPENDS_device-mapper-libs = "device-mapper glibc libblkid libselinux libsepol systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/device-mapper-libs-1.02.169-3.el8.aarch64.rpm \
          "

SRC_URI[device-mapper-libs.sha256sum] = "26718ce8a44d105c0838c2ff81c3dce9337c74a0bf8d778f92feb84341654c02"
