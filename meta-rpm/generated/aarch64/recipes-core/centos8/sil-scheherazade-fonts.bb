SUMMARY = "generated recipe based on sil-scheherazade-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_sil-scheherazade-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sil-scheherazade-fonts-2.100-5.el8.noarch.rpm \
          "

SRC_URI[sil-scheherazade-fonts.sha256sum] = "12260b45041120845b0c2510febdc41a2031811e3e16e9e86492eb974c821b67"
