SUMMARY = "generated recipe based on modello srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & BSD & MIT"
RPM_LICENSE = "ASL 2.0 and BSD and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_modello = "bash java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools jsoup junit maven-lib plexus-build-api plexus-compiler plexus-containers-container-default plexus-utils"
RDEPENDS_modello-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/modello-1.9.1-7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/modello-javadoc-1.9.1-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[modello.sha256sum] = "c84779e0ebe4af0868ca397dd13974d33c416636b6ed6d64f547b578d83ac04f"
SRC_URI[modello-javadoc.sha256sum] = "4c555969e85513fb780c7bc3ffe296a338dac34b63cd3a6dc5154dcb47028562"
