SUMMARY = "generated recipe based on libisofs srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl pkgconfig-native zlib"
RPM_SONAME_PROV_libisofs = "libisofs.so.6"
RPM_SONAME_REQ_libisofs = "ld-linux-aarch64.so.1 libacl.so.1 libc.so.6 libpthread.so.0 libz.so.1"
RDEPENDS_libisofs = "glibc libacl zlib"
RPM_SONAME_REQ_libisofs-devel = "libisofs.so.6"
RPROVIDES_libisofs-devel = "libisofs-dev (= 1.4.8)"
RDEPENDS_libisofs-devel = "libisofs pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libisofs-1.4.8-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libisofs-devel-1.4.8-3.el8.aarch64.rpm \
          "

SRC_URI[libisofs.sha256sum] = "2e5435efba38348be8d33a43e5abbffc85f7c5a9504ebe6451b87c44006b3b4c"
SRC_URI[libisofs-devel.sha256sum] = "71f53b731561448a9445b49dbebcda15c80dc1927b85e4c2f37a3d6b0253d9c6"
