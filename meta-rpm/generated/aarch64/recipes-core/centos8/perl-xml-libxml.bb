SUMMARY = "generated recipe based on perl-XML-LibXML srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & MIT"
RPM_LICENSE = "(GPL+ or Artistic) and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxml2 perl pkgconfig-native xz zlib"
RPM_SONAME_REQ_perl-XML-LibXML = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 liblzma.so.5 libm.so.6 libperl.so.5.26 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_perl-XML-LibXML = "bash glibc libxml2 perl-Carp perl-Data-Dumper perl-Encode perl-Exporter perl-IO perl-Scalar-List-Utils perl-XML-NamespaceSupport perl-XML-SAX perl-XML-SAX-Base perl-constant perl-interpreter perl-libs perl-parent xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-XML-LibXML-2.0132-2.el8.aarch64.rpm \
          "

SRC_URI[perl-XML-LibXML.sha256sum] = "1079c7af1309a8778958e0f360b0539f78388651aa136cff2f4d680bab94a388"
