SUMMARY = "generated recipe based on c-ares srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_c-ares = "libcares.so.2"
RPM_SONAME_REQ_c-ares = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_c-ares = "glibc"
RPM_SONAME_REQ_c-ares-devel = "libcares.so.2"
RPROVIDES_c-ares-devel = "c-ares-dev (= 1.13.0)"
RDEPENDS_c-ares-devel = "c-ares pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/c-ares-1.13.0-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/c-ares-devel-1.13.0-5.el8.aarch64.rpm \
          "

SRC_URI[c-ares.sha256sum] = "4ae071ebcf06cec6e4e7e49a50d83464cb1f1f1c72449a02ba1dfc820dbf67c5"
SRC_URI[c-ares-devel.sha256sum] = "5b60050c3042dbcb04c8748043489cf1cd4bafed3d93ace1a80a04eb33eee87a"
