SUMMARY = "generated recipe based on acl srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "attr pkgconfig-native"
RPM_SONAME_REQ_acl = "ld-linux-aarch64.so.1 libacl.so.1 libattr.so.1 libc.so.6"
RDEPENDS_acl = "glibc libacl libattr"
RPM_SONAME_PROV_libacl = "libacl.so.1"
RPM_SONAME_REQ_libacl = "ld-linux-aarch64.so.1 libattr.so.1 libc.so.6"
RDEPENDS_libacl = "glibc libattr"
RPM_SONAME_REQ_libacl-devel = "libacl.so.1"
RPROVIDES_libacl-devel = "libacl-dev (= 2.2.53)"
RDEPENDS_libacl-devel = "libacl libattr-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/acl-2.2.53-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libacl-2.2.53-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libacl-devel-2.2.53-1.el8.aarch64.rpm \
          "

SRC_URI[acl.sha256sum] = "47c2cc5872174c548de1096dc5673ee91349209d89e0193a4793955d6865b3b1"
SRC_URI[libacl.sha256sum] = "c4cfed85e5a0db903ad134b4327b1714e5453fcf5c4348ec93ab344860a970ef"
SRC_URI[libacl-devel.sha256sum] = "d7ede6a08749a235627d8943d3a59b9f1eda01dfc04cc0986264d527db29f6e3"
