SUMMARY = "generated recipe based on lua-lunit srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_lua-lunit = "bash lua"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lua-lunit-0.5-13.el8.noarch.rpm \
          "

SRC_URI[lua-lunit.sha256sum] = "67ecaeb7467795f0e65736d6478ee2e0603ef61c092d1ef64d93ed61610ef7f4"
