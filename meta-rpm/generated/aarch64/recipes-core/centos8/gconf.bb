SUMMARY = "generated recipe based on GConf2 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "LGPLv2+ and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus dbus-glib dbus-libs glib-2.0 libxml2 pkgconfig-native polkit"
RPM_SONAME_PROV_GConf2 = "libgconf-2.so.4 libgconfbackend-oldxml.so libgconfbackend-xml.so libgsettingsgconfbackend.so"
RPM_SONAME_REQ_GConf2 = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libxml2.so.2"
RDEPENDS_GConf2 = "bash dbus dbus-glib dbus-libs glib2 glibc libxml2 polkit-libs psmisc"
RPM_SONAME_REQ_GConf2-devel = "libgconf-2.so.4"
RPROVIDES_GConf2-devel = "GConf2-dev (= 3.2.6)"
RDEPENDS_GConf2-devel = "GConf2 automake dbus-devel glib2-devel libxml2-devel pkgconf-pkg-config platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/GConf2-3.2.6-22.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/GConf2-devel-3.2.6-22.el8.aarch64.rpm \
          "

SRC_URI[GConf2.sha256sum] = "1010017725ec694bb975b7b269da253ca5fb70a3974bf8169f02181afdb37778"
SRC_URI[GConf2-devel.sha256sum] = "f3acf94e7209a4a8e6f6f3caff5a5f64c866c70d0ec061ac7c938db8edf06824"
