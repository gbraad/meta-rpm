SUMMARY = "generated recipe based on libvncserver srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gnutls-libs libgcrypt libjpeg-turbo libpng lzo pkgconfig-native zlib"
RPM_SONAME_PROV_libvncserver = "libvncclient.so.0 libvncserver.so.0"
RPM_SONAME_REQ_libvncserver = "ld-linux-aarch64.so.1 libc.so.6 libgcrypt.so.20 libgnutls.so.30 libjpeg.so.62 libminilzo.so.0 libpng16.so.16 libpthread.so.0 libresolv.so.2 libz.so.1"
RDEPENDS_libvncserver = "glibc gnutls libgcrypt libjpeg-turbo libpng lzo-minilzo zlib"
RPM_SONAME_REQ_libvncserver-devel = "libvncclient.so.0 libvncserver.so.0"
RPROVIDES_libvncserver-devel = "libvncserver-dev (= 0.9.11)"
RDEPENDS_libvncserver-devel = "bash coreutils libvncserver pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvncserver-0.9.11-15.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libvncserver-devel-0.9.11-15.el8_2.1.aarch64.rpm \
          "

SRC_URI[libvncserver.sha256sum] = "4bbd512e6e62f24749b8090b85c973eb48a3ba5b3caddfaab05b97441a2f5e01"
SRC_URI[libvncserver-devel.sha256sum] = "17584b9b9235152d105ebe9260a94ca72244fe4224cfef55e1109ff34067a4b8"
