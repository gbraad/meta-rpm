SUMMARY = "generated recipe based on pipewire srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib dbus-libs glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base pkgconfig-native sbc systemd-libs"
RPM_SONAME_PROV_pipewire = "libpipewire-module-audio-dsp.so libpipewire-module-autolink.so libpipewire-module-client-node.so libpipewire-module-link-factory.so libpipewire-module-mixer.so libpipewire-module-portal.so libpipewire-module-protocol-native.so libpipewire-module-rtkit.so libpipewire-module-spa-monitor.so libpipewire-module-spa-node-factory.so libpipewire-module-spa-node.so libpipewire-module-suspend-on-idle.so libspa-alsa.so libspa-audiomixer.so libspa-audiotestsrc.so libspa-bluez5.so libspa-dbus.so libspa-support.so libspa-test.so libspa-v4l2.so libspa-videotestsrc.so libspa-volume.so"
RPM_SONAME_REQ_pipewire = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libdbus-1.so.3 libdl.so.2 libm.so.6 libpipewire-0.2.so.1 libpthread.so.0 libsbc.so.1 libsystemd.so.0 libudev.so.1"
RDEPENDS_pipewire = "alsa-lib bash dbus-libs glibc pipewire-libs rtkit sbc shadow-utils systemd systemd-libs"
RPM_SONAME_PROV_pipewire-libs = "libgstpipewire.so libpipewire-0.2.so.1"
RPM_SONAME_REQ_pipewire-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libgstallocators-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libpthread.so.0"
RDEPENDS_pipewire-libs = "glib2 glibc gstreamer1 gstreamer1-plugins-base"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pipewire-0.2.7-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pipewire-libs-0.2.7-1.el8.aarch64.rpm \
          "

SRC_URI[pipewire.sha256sum] = "81a3629b02a83e42cf9c3a373eff9f2328ca7d8d10a82f39aee181799a1f4b81"
SRC_URI[pipewire-libs.sha256sum] = "87858639f32fb0faf3c5b44fe3520286a174a3a4b71d586f45f472a5c4e199b9"
