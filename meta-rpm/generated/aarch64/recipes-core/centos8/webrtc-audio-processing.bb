SUMMARY = "generated recipe based on webrtc-audio-processing srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & MIT"
RPM_LICENSE = "BSD and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_webrtc-audio-processing = "libwebrtc_audio_processing.so.1"
RPM_SONAME_REQ_webrtc-audio-processing = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 librt.so.1 libstdc++.so.6"
RDEPENDS_webrtc-audio-processing = "glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/webrtc-audio-processing-0.3-8.el8.aarch64.rpm \
          "

SRC_URI[webrtc-audio-processing.sha256sum] = "e27f8d634ed560ffc052af4e6087de7af5df2704cd47caf7d5696d684591a733"
