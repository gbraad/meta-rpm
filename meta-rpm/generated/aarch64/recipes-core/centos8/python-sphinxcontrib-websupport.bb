SUMMARY = "generated recipe based on python-sphinxcontrib-websupport srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-sphinxcontrib-websupport = "platform-python python3-docutils python3-jinja2 python3-six python3-sphinx python3-sqlalchemy python3-whoosh"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-sphinxcontrib-websupport-1.0.1-10.20180316git.el8.noarch.rpm \
          "

SRC_URI[python3-sphinxcontrib-websupport.sha256sum] = "04b27f705a419674d97709970fd5cd90e9df498ff2f45332859d16209b529229"
