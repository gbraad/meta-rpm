SUMMARY = "generated recipe based on motif srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libice libjpeg-turbo libpng libsm libx11 libxext libxft libxmu libxp libxt pkgconfig-native"
RPM_SONAME_PROV_motif = "libMrm.so.4 libUil.so.4 libXm.so.4"
RPM_SONAME_REQ_motif = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXext.so.6 libXft.so.2 libXmu.so.6 libXp.so.6 libXt.so.6 libc.so.6 libjpeg.so.62 libpng16.so.16"
RDEPENDS_motif = "bash glibc libICE libSM libX11 libXext libXft libXmu libXp libXt libjpeg-turbo libpng xorg-x11-xbitmaps xorg-x11-xinit"
RPM_SONAME_REQ_motif-devel = "libICE.so.6 libMrm.so.4 libSM.so.6 libUil.so.4 libX11.so.6 libXext.so.6 libXft.so.2 libXm.so.4 libXmu.so.6 libXp.so.6 libXt.so.6 libc.so.6 libjpeg.so.62 libpng16.so.16"
RPROVIDES_motif-devel = "motif-dev (= 2.3.4)"
RDEPENDS_motif-devel = "glibc libICE libSM libX11 libXext libXext-devel libXft libXft-devel libXmu libXmu-devel libXp libXp-devel libXt libXt-devel libjpeg-turbo libjpeg-turbo-devel libpng libpng-devel motif"
RDEPENDS_motif-static = "motif-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/motif-2.3.4-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/motif-devel-2.3.4-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/motif-static-2.3.4-16.el8.aarch64.rpm \
          "

SRC_URI[motif.sha256sum] = "9493f48899aafba226bdffdb4666195da77c115b6cc941d1b912ef39287f63cc"
SRC_URI[motif-devel.sha256sum] = "8714a71ee6d377443abffba3e1bc0aba02f89fef643476b7bf42e174548c793d"
SRC_URI[motif-static.sha256sum] = "13af1f6f18b7f6cc2d69a7c7c2b91d72ff8421bea1629a9f18cdecf6710c547e"
