SUMMARY = "generated recipe based on perl-Unix-Syslog srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0"
RPM_LICENSE = "Artistic 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Unix-Syslog = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Unix-Syslog = "glibc perl-Exporter perl-interpreter perl-libs rsyslog"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Unix-Syslog-1.1-29.el8.aarch64.rpm \
          "

SRC_URI[perl-Unix-Syslog.sha256sum] = "f5158f78ad3063160a905b345328ebf4f881eb4ea32206a8c72fa5586495dbd7"
