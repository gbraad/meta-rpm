SUMMARY = "generated recipe based on evolution-ews srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo evolution evolution-data-server gdk-pixbuf glib-2.0 gtk+3 libical libmspack libsecret libsoup-2.4 libxml2 nspr nss pango pkgconfig-native sqlite3 webkit2gtk3"
RPM_SONAME_PROV_evolution-ews = "libcamelews-priv.so libcamelews.so libebookbackendews.so libecalbackendews.so libevolution-ews.so"
RPM_SONAME_REQ_evolution-ews = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcamel-1.2.so.61 libdl.so.2 libebackend-1.2.so.10 libebook-1.2.so.19 libebook-contacts-1.2.so.2 libecal-1.2.so.19 libedata-book-1.2.so.25 libedata-cal-1.2.so.28 libedataserver-1.2.so.23 libedataserverui-1.2.so.2 libemail-engine.so libevolution-calendar.so libevolution-mail-composer.so libevolution-mail-formatter.so libevolution-mail.so libevolution-shell.so libevolution-util.so libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libical.so.3 libicalss.so.3 libicalvcal.so.3 libjavascriptcoregtk-4.0.so.18 libm.so.6 libmspack.so.0 libnspr4.so libnss3.so libnssutil3.so libpango-1.0.so.0 libpangocairo-1.0.so.0 libplc4.so libplds4.so libpthread.so.0 libsecret-1.so.0 libsmime3.so libsoup-2.4.so.1 libsqlite3.so.0 libssl3.so libwebkit2gtk-4.0.so.37 libxml2.so.2"
RDEPENDS_evolution-ews = "atk cairo cairo-gobject evolution evolution-data-server evolution-ews-langpacks gdk-pixbuf2 glib2 glibc gtk3 libical libmspack libsecret libsoup libxml2 nspr nss nss-util pango sqlite-libs webkit2gtk3 webkit2gtk3-jsc"
RDEPENDS_evolution-ews-langpacks = "evolution-ews"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/evolution-ews-3.28.5-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/evolution-ews-langpacks-3.28.5-9.el8.noarch.rpm \
          "

SRC_URI[evolution-ews.sha256sum] = "946b415a50f8a1fb066dfb00ce31c14e42c13a57c56fa63a906b453cfa40e95b"
SRC_URI[evolution-ews-langpacks.sha256sum] = "3d867ceea22fdc017657b7d34de3d64a115b96c687875e153f01966861e00f3b"
