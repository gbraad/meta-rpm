SUMMARY = "generated recipe based on tree srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_tree = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_tree = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tree-1.7.0-15.el8.aarch64.rpm \
          "

SRC_URI[tree.sha256sum] = "335c02d243e4c615564f6533a611f84f4874e2b06fc4155efafeb85f8b538636"
