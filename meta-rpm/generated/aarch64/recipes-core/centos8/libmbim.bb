SUMMARY = "generated recipe based on libmbim srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libgudev pkgconfig-native"
RPM_SONAME_PROV_libmbim = "libmbim-glib.so.4"
RPM_SONAME_REQ_libmbim = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0"
RDEPENDS_libmbim = "glib2 glibc libgudev"
RPM_SONAME_REQ_libmbim-utils = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgudev-1.0.so.0 libmbim-glib.so.4"
RDEPENDS_libmbim-utils = "bash glib2 glibc libgudev libmbim"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libmbim-1.20.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libmbim-utils-1.20.2-1.el8.aarch64.rpm \
          "

SRC_URI[libmbim.sha256sum] = "be00b7c6f61da34124858a5027f889c6ac47fc1e93b871e9c7c3be034f7c2137"
SRC_URI[libmbim-utils.sha256sum] = "62039f609990880d0466698bf166eb9b815a538e7c12b48a6a8aff35dca1fbc4"
