SUMMARY = "generated recipe based on systemd srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & MIT"
RPM_LICENSE = "LGPLv2+ and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libcap libgcc libgcrypt libmount lz4 pkgconfig-native xz"
RPM_SONAME_PROV_systemd-libs = "libnss_myhostname.so.2 libnss_resolve.so.2 libnss_systemd.so.2 libsystemd.so.0 libudev.so.1"
RPM_SONAME_REQ_systemd-libs = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libgcc_s.so.1 libgcrypt.so.20 liblz4.so.1 liblzma.so.5 libmount.so.1 libpthread.so.0 librt.so.1"
RPROVIDES_systemd-libs = "libsystemd (= 239) libudev (= 239)"
RDEPENDS_systemd-libs = "bash coreutils glibc glibc-common grep libcap libgcc libgcrypt libmount lz4-libs sed xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/systemd-libs-239-31.el8_2.2.aarch64.rpm \
          "

SRC_URI[systemd-libs.sha256sum] = "7807ee55897992f643d5a3fae8c1d5ed14678192b78bf99a04a41c7d295dd218"
