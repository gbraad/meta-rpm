SUMMARY = "generated recipe based on libpq srpm"
DESCRIPTION = "Description"
LICENSE = "PostgreSQL"
RPM_LICENSE = "PostgreSQL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "krb5-libs openldap openssl pkgconfig-native"
RPM_SONAME_PROV_libpq = "libpq.so.5"
RPM_SONAME_REQ_libpq = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgssapi_krb5.so.2 libldap_r-2.4.so.2 libpthread.so.0 libssl.so.1.1"
RDEPENDS_libpq = "glibc krb5-libs openldap openssl-libs"
RPM_SONAME_REQ_libpq-devel = "ld-linux-aarch64.so.1 libc.so.6 libpq.so.5 libpthread.so.0"
RPROVIDES_libpq-devel = "libpq-dev (= 12.4)"
RDEPENDS_libpq-devel = "glibc libpq pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpq-12.4-1.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libpq-devel-12.4-1.el8_2.aarch64.rpm \
          "

SRC_URI[libpq.sha256sum] = "6754272b318b763a58a4fef2ac4152eada514c6a0a9f64fcf29b1b5ca13171e1"
SRC_URI[libpq-devel.sha256sum] = "c8d8263679d9153a5b9abe1359230947063477b294ac098d633a24eddcfa26f2"
