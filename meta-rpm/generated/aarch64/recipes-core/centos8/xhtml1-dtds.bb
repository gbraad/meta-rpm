SUMMARY = "generated recipe based on xhtml1-dtds srpm"
DESCRIPTION = "Description"
LICENSE = "W3C"
RPM_LICENSE = "W3C"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_xhtml1-dtds = "bash libxml2 sgml-common xml-common"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xhtml1-dtds-1.0-20020801.13.el8.4.noarch.rpm \
          "

SRC_URI[xhtml1-dtds.sha256sum] = "bbfa02cfc9f376030a97ed25d4ddd67b84d2334a7b9ff43302686ed43f5931cb"
