SUMMARY = "generated recipe based on qt5-qtdoc srpm"
DESCRIPTION = "Description"
LICENSE = "GFDL-1.1"
RPM_LICENSE = "GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtdoc-5.12.5-1.el8.noarch.rpm \
          "

SRC_URI[qt5-qtdoc.sha256sum] = "e8e3d0bbc6d1bf37f3fc9591f7e0f5bf7854d63afa620ed3f39ac0149058eb8c"
