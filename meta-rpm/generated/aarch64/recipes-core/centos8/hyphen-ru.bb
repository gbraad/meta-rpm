SUMMARY = "generated recipe based on hyphen-ru srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-ru = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-ru-0.20020727-17.el8.noarch.rpm \
          "

SRC_URI[hyphen-ru.sha256sum] = "0dca0a7e95941fe733ba4fd78c63b2889e1816861539db311ddad42050cacbff"
