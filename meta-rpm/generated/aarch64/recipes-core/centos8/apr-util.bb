SUMMARY = "generated recipe based on apr-util srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "apr db expat libpq libuuid libxcrypt mariadb-connector-c openldap openssl pkgconfig-native sqlite3 unixodbc zlib"
RPM_SONAME_PROV_apr-util = "libaprutil-1.so.0"
RPM_SONAME_REQ_apr-util = "ld-linux-aarch64.so.1 libapr-1.so.0 libc.so.6 libcrypt.so.1 libdl.so.2 libexpat.so.1 libpthread.so.0 libuuid.so.1"
RDEPENDS_apr-util = "apr expat glibc libuuid libxcrypt"
RPM_SONAME_REQ_apr-util-bdb = "ld-linux-aarch64.so.1 libc.so.6 libdb-5.3.so libpthread.so.0"
RDEPENDS_apr-util-bdb = "apr-util glibc libdb"
RPM_SONAME_REQ_apr-util-devel = "libaprutil-1.so.0"
RPROVIDES_apr-util-devel = "apr-util-dev (= 1.6.1)"
RDEPENDS_apr-util-devel = "apr-devel apr-util bash expat-devel libdb-devel openldap-devel pkgconf-pkg-config"
RPM_SONAME_REQ_apr-util-ldap = "ld-linux-aarch64.so.1 libc.so.6 liblber-2.4.so.2 libldap_r-2.4.so.2 libpthread.so.0"
RDEPENDS_apr-util-ldap = "apr-util glibc openldap"
RPM_SONAME_REQ_apr-util-mysql = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libmariadb.so.3 libpthread.so.0 libssl.so.1.1 libz.so.1"
RDEPENDS_apr-util-mysql = "apr-util glibc mariadb-connector-c openssl-libs zlib"
RPM_SONAME_REQ_apr-util-odbc = "ld-linux-aarch64.so.1 libc.so.6 libodbc.so.2 libpthread.so.0"
RDEPENDS_apr-util-odbc = "apr-util glibc unixODBC"
RPM_SONAME_REQ_apr-util-openssl = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpthread.so.0 libssl.so.1.1"
RDEPENDS_apr-util-openssl = "apr-util glibc openssl-libs"
RPM_SONAME_REQ_apr-util-pgsql = "libc.so.6 libpq.so.5 libpthread.so.0"
RDEPENDS_apr-util-pgsql = "apr-util glibc libpq"
RPM_SONAME_REQ_apr-util-sqlite = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libsqlite3.so.0"
RDEPENDS_apr-util-sqlite = "apr-util glibc sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-util-1.6.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-util-bdb-1.6.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-util-devel-1.6.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-util-ldap-1.6.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-util-mysql-1.6.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-util-odbc-1.6.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-util-openssl-1.6.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-util-pgsql-1.6.1-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/apr-util-sqlite-1.6.1-6.el8.aarch64.rpm \
          "

SRC_URI[apr-util.sha256sum] = "fbbba416a566d7f08f5294ce6ddd94a13385e1c8ecd89ff4bc20733beb44490e"
SRC_URI[apr-util-bdb.sha256sum] = "74bab1b60aa30610bf7e3d2c01656019f2b1f4750af650e664acfff803603931"
SRC_URI[apr-util-devel.sha256sum] = "911a27304fe47035e9b47e5acd045e50ed0a85599df82b24cce141bbbce79de6"
SRC_URI[apr-util-ldap.sha256sum] = "fbfc97bc057b428e1d9f799c7441f59eaaf6694838c52d80d8e585ed163a4b89"
SRC_URI[apr-util-mysql.sha256sum] = "a6089b97b6a36e941cce4dc060821ed38a1ebcc2fd4b1a7fd523bb66ef12b084"
SRC_URI[apr-util-odbc.sha256sum] = "d4ea22cce2b43427353ceea3c3acfb446e75d9afed16183a36d4e59bfdaff949"
SRC_URI[apr-util-openssl.sha256sum] = "47e049b152f4060c07febb14f143d4f75b503d00f87cc2b4821e6a3190d2cc4a"
SRC_URI[apr-util-pgsql.sha256sum] = "dcafcde0234587a0e148300bdae1100fa572fb5d68f0cabff7e6dd5d5cd9795a"
SRC_URI[apr-util-sqlite.sha256sum] = "d7190388a46dbc3c1adecf848e072aff74bb4856e6f5d6caeba5a868376946ff"
