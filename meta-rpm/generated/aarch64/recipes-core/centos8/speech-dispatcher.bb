SUMMARY = "generated recipe based on speech-dispatcher srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0"
RPM_LICENSE = "GPLv2+ and GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib dotconf espeak-ng glib-2.0 libao libsndfile2 libtool-ltdl pkgconfig-native pulseaudio"
RDEPENDS_python3-speechd = "platform-python python3-pyxdg speech-dispatcher"
RPM_SONAME_PROV_speech-dispatcher = "libspeechd.so.2"
RPM_SONAME_REQ_speech-dispatcher = "ld-linux-aarch64.so.1 libao.so.4 libasound.so.2 libc.so.6 libdotconf.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgthread-2.0.so.0 libltdl.so.7 libm.so.6 libpthread.so.0 libpulse-simple.so.0 libpulse.so.0 libsndfile.so.1"
RDEPENDS_speech-dispatcher = "alsa-lib bash dotconf glib2 glibc libao libsndfile libtool-ltdl pulseaudio-libs speech-dispatcher-espeak-ng systemd"
RPM_SONAME_REQ_speech-dispatcher-devel = "libspeechd.so.2"
RPROVIDES_speech-dispatcher-devel = "speech-dispatcher-dev (= 0.8.8)"
RDEPENDS_speech-dispatcher-devel = "glib2-devel pkgconf-pkg-config speech-dispatcher"
RDEPENDS_speech-dispatcher-doc = "bash info speech-dispatcher"
RPM_SONAME_REQ_speech-dispatcher-espeak-ng = "ld-linux-aarch64.so.1 libc.so.6 libdotconf.so.0 libespeak-ng.so.1 libglib-2.0.so.0 libgthread-2.0.so.0 libltdl.so.7 libm.so.6 libpthread.so.0 libsndfile.so.1"
RDEPENDS_speech-dispatcher-espeak-ng = "dotconf espeak-ng glib2 glibc libsndfile libtool-ltdl speech-dispatcher"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-speechd-0.8.8-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/speech-dispatcher-0.8.8-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/speech-dispatcher-espeak-ng-0.8.8-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/speech-dispatcher-devel-0.8.8-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/speech-dispatcher-doc-0.8.8-6.el8.noarch.rpm \
          "

SRC_URI[python3-speechd.sha256sum] = "5e974f44a4b1a961f44e09524e5754ec45af85f4732cc4178c791248f1a8b6fd"
SRC_URI[speech-dispatcher.sha256sum] = "021d37e159e7cf9dbbbbe519388e370f308c21bee21c879a1913f400c0de48db"
SRC_URI[speech-dispatcher-devel.sha256sum] = "4f13fae4bfcbd6f956e644f698432138f25622f6e606b2b59d6b125f6bff3d5a"
SRC_URI[speech-dispatcher-doc.sha256sum] = "f457e2ad314f8b60b479f74de9b247ddeba4fb3ac5145fe6f9547c31e74df61c"
SRC_URI[speech-dispatcher-espeak-ng.sha256sum] = "ea6912f2df65e12cd2c480564603c07f4d9248c7e06b4dcf634c995e90133811"
