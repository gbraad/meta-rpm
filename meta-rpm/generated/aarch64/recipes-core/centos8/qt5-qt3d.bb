SUMMARY = "generated recipe based on qt5-qt3d srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative zlib"
RPM_SONAME_PROV_qt5-qt3d = "libQt53DAnimation.so.5 libQt53DCore.so.5 libQt53DExtras.so.5 libQt53DInput.so.5 libQt53DLogic.so.5 libQt53DQuick.so.5 libQt53DQuickAnimation.so.5 libQt53DQuickExtras.so.5 libQt53DQuickInput.so.5 libQt53DQuickRender.so.5 libQt53DQuickScene2D.so.5 libQt53DRender.so.5 libassimpsceneimport.so libdefaultgeometryloader.so libgltfgeometryloader.so libgltfsceneexport.so libgltfsceneimport.so libqtquickscene2dplugin.so libqtquickscene3dplugin.so libquick3danimationplugin.so libquick3dcoreplugin.so libquick3dextrasplugin.so libquick3dinputplugin.so libquick3dlogicplugin.so libquick3drenderplugin.so libscene2d.so"
RPM_SONAME_REQ_qt5-qt3d = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_qt5-qt3d = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtimageformats zlib"
RPM_SONAME_REQ_qt5-qt3d-devel = "ld-linux-aarch64.so.1 libQt53DAnimation.so.5 libQt53DCore.so.5 libQt53DExtras.so.5 libQt53DInput.so.5 libQt53DLogic.so.5 libQt53DQuick.so.5 libQt53DQuickAnimation.so.5 libQt53DQuickExtras.so.5 libQt53DQuickInput.so.5 libQt53DQuickRender.so.5 libQt53DQuickScene2D.so.5 libQt53DRender.so.5 libQt5Core.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RPROVIDES_qt5-qt3d-devel = "qt5-qt3d-dev (= 5.12.5)"
RDEPENDS_qt5-qt3d-devel = "cmake-filesystem glibc libgcc libstdc++ pkgconf-pkg-config qt5-qt3d qt5-qtbase qt5-qtbase-devel qt5-qtdeclarative-devel zlib"
RPM_SONAME_REQ_qt5-qt3d-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt53DCore.so.5 libQt53DExtras.so.5 libQt53DInput.so.5 libQt53DLogic.so.5 libQt53DQuick.so.5 libQt53DQuickExtras.so.5 libQt53DQuickInput.so.5 libQt53DQuickRender.so.5 libQt53DRender.so.5 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5QuickWidgets.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qt3d-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qt3d qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qt3d-5.12.5-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qt3d-devel-5.12.5-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qt3d-examples-5.12.5-2.el8.aarch64.rpm \
          "

SRC_URI[qt5-qt3d.sha256sum] = "73bdae25fd4a84f86b47fd7727e4393595414c9c69910c6c8b25e779c7119563"
SRC_URI[qt5-qt3d-devel.sha256sum] = "3f807fa2a80d60575e8e7ab4cbdb19ca46ec634ba5b45602eab8c5f2a7be241d"
SRC_URI[qt5-qt3d-examples.sha256sum] = "3afb82c3870da743b218201b466dac782e3286c97b92aa985ab1a2363fd4b13d"
