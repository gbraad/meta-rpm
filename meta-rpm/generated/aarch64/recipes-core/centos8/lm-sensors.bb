SUMMARY = "generated recipe based on lm_sensors srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & CLOSED & MIT"
RPM_LICENSE = "GPLv2+ and Verbatim and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native rrdtool"
RPM_SONAME_REQ_lm_sensors = "ld-linux-aarch64.so.1 libc.so.6 libsensors.so.4"
RDEPENDS_lm_sensors = "bash glibc kmod lm_sensors-libs perl-constant perl-interpreter perl-libs systemd"
RPM_SONAME_REQ_lm_sensors-devel = "libsensors.so.4"
RPROVIDES_lm_sensors-devel = "lm_sensors-dev (= 3.4.0)"
RDEPENDS_lm_sensors-devel = "lm_sensors-libs"
RPM_SONAME_PROV_lm_sensors-libs = "libsensors.so.4"
RPM_SONAME_REQ_lm_sensors-libs = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_lm_sensors-libs = "glibc"
RPM_SONAME_REQ_lm_sensors-sensord = "ld-linux-aarch64.so.1 libc.so.6 librrd.so.8 libsensors.so.4"
RDEPENDS_lm_sensors-sensord = "bash glibc lm_sensors lm_sensors-libs rrdtool"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lm_sensors-sensord-3.4.0-21.20180522git70f7e08.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lm_sensors-3.4.0-21.20180522git70f7e08.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lm_sensors-devel-3.4.0-21.20180522git70f7e08.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lm_sensors-libs-3.4.0-21.20180522git70f7e08.el8.aarch64.rpm \
          "

SRC_URI[lm_sensors.sha256sum] = "591b5b40892c304a8c171fffffcc403687eed68b22977c00bdcc498586341a46"
SRC_URI[lm_sensors-devel.sha256sum] = "d53d3d23aed34614d6a2ad81249c7bad3c3f59e47bc1bc3f88bdbad4d27d9672"
SRC_URI[lm_sensors-libs.sha256sum] = "ad3720f495a3d986f431c8b89f0a1374c65bdb3d908a90b21120d31527688bc4"
SRC_URI[lm_sensors-sensord.sha256sum] = "40e8bac33f322be38fa90e42d2614c7c2215d7694485e4f54233f87f47103e05"
