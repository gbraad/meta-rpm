SUMMARY = "generated recipe based on perl-Eval-Closure srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Eval-Closure = "perl-Carp perl-Devel-LexAlias perl-Exporter perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs perltidy"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Eval-Closure-0.14-5.el8.noarch.rpm \
          "

SRC_URI[perl-Eval-Closure.sha256sum] = "67648503ca2c0b7e5c9799a95c241ffe35e67af22127769f8e82ddfa46c3b345"
