SUMMARY = "generated recipe based on perl-JSON-PP srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-JSON-PP = "perl-Carp perl-Data-Dumper perl-Encode perl-Exporter perl-Getopt-Long perl-Math-BigInt perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-JSON-PP-2.97.001-3.el8.noarch.rpm \
          "

SRC_URI[perl-JSON-PP.sha256sum] = "e89e02e3cd85acac5fe917108156a61bc638ead21f012475e0c2a6691f9f67af"
