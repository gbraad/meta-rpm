SUMMARY = "generated recipe based on teckit srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | CPL-1.0"
RPM_LICENSE = "LGPLv2+ or CPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "expat libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_teckit = "libTECkit.so.0 libTECkit_Compiler.so.0"
RPM_SONAME_REQ_teckit = "ld-linux-aarch64.so.1 libc.so.6 libexpat.so.1 libgcc_s.so.1 libm.so.6 libstdc++.so.6 libz.so.1"
RDEPENDS_teckit = "expat glibc libgcc libstdc++ zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/teckit-2.5.8-1.el8.aarch64.rpm \
          "

SRC_URI[teckit.sha256sum] = "14f2c7b884417145b3ad26c6e6937b8b0172fddea56b11babb25593a7877b760"
