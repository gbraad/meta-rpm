SUMMARY = "generated recipe based on libunicap srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libraw1394 pkgconfig-native v4l-utils"
RPM_SONAME_PROV_libunicap = "libeuvccam_cpi.so libunicap.so.2"
RPM_SONAME_REQ_libunicap = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libraw1394.so.11 librt.so.1 libv4l2.so.0"
RDEPENDS_libunicap = "glibc libraw1394 libv4l"
RPM_SONAME_REQ_libunicap-devel = "libunicap.so.2"
RPROVIDES_libunicap-devel = "libunicap-dev (= 0.9.12)"
RDEPENDS_libunicap-devel = "libunicap pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libunicap-0.9.12-21.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libunicap-devel-0.9.12-21.el8.aarch64.rpm \
          "

SRC_URI[libunicap.sha256sum] = "2e4f1621f2f5b6b2a6709ece9ccf3171092cac462f5e10ed55d751059c0c5297"
SRC_URI[libunicap-devel.sha256sum] = "7105fca67e3e6f409990ad55fef1f6be54ed791fb204b06f3e7ef9ac2326be92"
