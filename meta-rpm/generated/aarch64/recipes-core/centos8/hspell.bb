SUMMARY = "generated recipe based on hspell srpm"
DESCRIPTION = "Description"
LICENSE = "AGPL-3.0"
RPM_LICENSE = "AGPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_hspell = "libhspell.so.0"
RPM_SONAME_REQ_hspell = "ld-linux-aarch64.so.1 libc.so.6 libz.so.1"
RDEPENDS_hspell = "glibc perl-Getopt-Long perl-IO perl-interpreter perl-libs zlib"
RDEPENDS_hunspell-he = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hspell-1.4-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-he-1.4-6.el8.aarch64.rpm \
          "

SRC_URI[hspell.sha256sum] = "8e11394971401a86fa35a9249089fe25530fdcbf3bf4feef5c9889479a808c1b"
SRC_URI[hunspell-he.sha256sum] = "c9f9f211c4ec6c5c2184eb86328784af9236ae8751c1720ad43db2e4149ed1f9"
