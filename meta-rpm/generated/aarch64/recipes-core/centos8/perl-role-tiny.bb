SUMMARY = "generated recipe based on perl-Role-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Role-Tiny = "perl-Class-Method-Modifiers perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Role-Tiny-2.000006-2.el8.noarch.rpm \
          "

SRC_URI[perl-Role-Tiny.sha256sum] = "54a3008c491f942db53e96630d9db9e42a85f03cfd295e75eb861007e3f29599"
