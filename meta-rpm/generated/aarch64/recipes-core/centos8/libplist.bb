SUMMARY = "generated recipe based on libplist srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libplist = "libplist++.so.3 libplist.so.3"
RPM_SONAME_REQ_libplist = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libplist = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libplist-devel = "libplist++.so.3 libplist.so.3"
RPROVIDES_libplist-devel = "libplist-dev (= 2.0.0)"
RDEPENDS_libplist-devel = "libplist pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libplist-2.0.0-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libplist-devel-2.0.0-10.el8.aarch64.rpm \
          "

SRC_URI[libplist.sha256sum] = "0d2c5c678b4b17c3427968e6e77e3466b7b6c5dc696c4fafe4e229e8fdcf6a6e"
SRC_URI[libplist-devel.sha256sum] = "9d9bca2eb21df39492f375220c881f0d25ac291b473381c14da8f32eeaa96d1c"
