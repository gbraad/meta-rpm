SUMMARY = "generated recipe based on javassist srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-1.1 | LGPL-2.0 | CLOSED"
RPM_LICENSE = "MPLv1.1 or LGPLv2+ or ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_javassist = "java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools"
RDEPENDS_javassist-javadoc = "javapackages-filesystem javapackages-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/javassist-3.18.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/javassist-javadoc-3.18.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[javassist.sha256sum] = "0f458788ee2a9961407e848c1741b14c7bb5046889178d5f9ebac0a68e93f5de"
SRC_URI[javassist-javadoc.sha256sum] = "f34574419c0aa8c770e41f0304ddf4f54f1750123cfccf4cb77a8ae432d37e7b"
