SUMMARY = "generated recipe based on hunspell-tl srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-tl = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-tl-0.20050109-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-tl.sha256sum] = "0292a2372658730113dc6e847e73766376b6d18a8f181e7f4c2dd2969b094368"
