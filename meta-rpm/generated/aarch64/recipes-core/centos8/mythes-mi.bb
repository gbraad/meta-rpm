SUMMARY = "generated recipe based on mythes-mi srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mythes-mi = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-mi-0.20080630-17.el8.noarch.rpm \
          "

SRC_URI[mythes-mi.sha256sum] = "8877d903f1de6f6a913206b5fc96c67a361f4fde128ddc1834a4b5720d98ad88"
