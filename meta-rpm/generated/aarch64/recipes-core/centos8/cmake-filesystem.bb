SUMMARY = "generated recipe based on cmake srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & MIT & Zlib"
RPM_LICENSE = "BSD and MIT and zlib"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cmake-filesystem-3.11.4-7.el8.aarch64.rpm \
          "

SRC_URI[cmake-filesystem.sha256sum] = "9238b95baac71661e9a305a4a7f0d680730c275cfc1da41fdb8f306ae7f421e0"
