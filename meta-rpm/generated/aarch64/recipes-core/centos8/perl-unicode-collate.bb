SUMMARY = "generated recipe based on perl-Unicode-Collate srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and Unicode"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Unicode-Collate = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Unicode-Collate = "glibc perl-Carp perl-PathTools perl-Unicode-Normalize perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Unicode-Collate-1.25-2.el8.aarch64.rpm \
          "

SRC_URI[perl-Unicode-Collate.sha256sum] = "0003e5cc5e9762d5fd2e788f2a3b58fbbfd550a2f1b572a2aba65c96f813a632"
