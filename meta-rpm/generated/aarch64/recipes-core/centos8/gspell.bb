SUMMARY = "generated recipe based on gspell srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo enchant2 gdk-pixbuf glib-2.0 gtk+3 pango pkgconfig-native"
RPM_SONAME_PROV_gspell = "libgspell-1.so.2"
RPM_SONAME_REQ_gspell = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libenchant-2.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0"
RDEPENDS_gspell = "atk cairo cairo-gobject enchant2 gdk-pixbuf2 glib2 glibc gtk3 iso-codes pango"
RPM_SONAME_REQ_gspell-devel = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libenchant-2.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgspell-1.so.2 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RPROVIDES_gspell-devel = "gspell-dev (= 1.8.1)"
RDEPENDS_gspell-devel = "atk cairo cairo-gobject enchant2 enchant2-devel gdk-pixbuf2 glib2 glib2-devel glibc gspell gtk3 gtk3-devel pango pkgconf-pkg-config"
RDEPENDS_gspell-doc = "gspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gspell-1.8.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gspell-devel-1.8.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gspell-doc-1.8.1-1.el8.noarch.rpm \
          "

SRC_URI[gspell.sha256sum] = "75d87e3fee400a464aeba1f2f505b94666cb26d049359dfbab70e4479ee46211"
SRC_URI[gspell-devel.sha256sum] = "0b5afd9a7c08ba3a43042012679e0944b8bf0981e6d82ed89831462cfb4b3563"
SRC_URI[gspell-doc.sha256sum] = "50ead194f720f93e068284445b07ea448a36a107953171af4d2df83eb13fa185"
