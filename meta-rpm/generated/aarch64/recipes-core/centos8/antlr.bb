SUMMARY = "generated recipe based on antlr srpm"
DESCRIPTION = "Description"
LICENSE = "ANTLR-PD"
RPM_LICENSE = "ANTLR-PD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_antlr-C++ = "bash"
RDEPENDS_antlr-javadoc = "javapackages-filesystem"
RDEPENDS_antlr-tool = "bash java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/antlr-C++-2.7.7-56.module_el8.0.0+30+832da3a1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/antlr-javadoc-2.7.7-56.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/antlr-manual-2.7.7-56.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/antlr-tool-2.7.7-56.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[antlr-C++.sha256sum] = "071cf3fac6f7cd360a266f3c15b9fda7c5d7b9ee19f27eba9081cc57e1b7cc76"
SRC_URI[antlr-javadoc.sha256sum] = "956bce8b350a2e7f125d6a57de28588b8d1d994e06922e6c28669a574a7f0d86"
SRC_URI[antlr-manual.sha256sum] = "0e9191d629d68590822ee5f736e14d5e906b232aff18fa976cfe6c04fe9703fc"
SRC_URI[antlr-tool.sha256sum] = "3f7964877e57d540a1208548b308d7ea569cdace9f429e903cf67c4585fbb50c"
