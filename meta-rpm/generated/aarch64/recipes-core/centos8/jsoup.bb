SUMMARY = "generated recipe based on jsoup srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jsoup = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jsoup-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jsoup-1.11.3-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jsoup-javadoc-1.11.3-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jsoup.sha256sum] = "4ecfc4f26e72cd65cc29ccf1bd708f5e65a7b5785021dd8ae61669c8a87b2e6c"
SRC_URI[jsoup-javadoc.sha256sum] = "cf220c0616dc4362c75a90aa2675f9add3adf08b970a5b33bd8550b88926d199"
