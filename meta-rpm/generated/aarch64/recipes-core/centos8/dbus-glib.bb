SUMMARY = "generated recipe based on dbus-glib srpm"
DESCRIPTION = "Description"
LICENSE = "AFL-1.2 & GPL-2.0"
RPM_LICENSE = "AFL and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-glib-devel dbus-libs expat glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_dbus-glib = "libdbus-glib-1.so.2"
RPM_SONAME_REQ_dbus-glib = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libexpat.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_dbus-glib = "dbus-libs expat glib2 glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/dbus-glib-0.110-2.el8.aarch64.rpm \
          "

SRC_URI[dbus-glib.sha256sum] = "eb5c0f62803580e76f09d53bec1fb4797f03846537fb0d050fd62045b7ce64ce"
