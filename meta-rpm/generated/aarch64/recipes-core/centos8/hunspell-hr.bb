SUMMARY = "generated recipe based on hunspell-hr srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | CLOSED"
RPM_LICENSE = "LGPLv2+ or SISSL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-hr = "hunspell"
RDEPENDS_hyphen-hr = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-hr-0.20040608-17.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-hr-0.20040608-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-hr.sha256sum] = "06c16d83e7bb04a7c2dd2a241bbde974329770c04a33b9e1d8a7a4dd094e08af"
SRC_URI[hyphen-hr.sha256sum] = "8a6fffde6e0fc17474ec1be976570c9a729ebd94cd7ea0a0fdf7814ac485f493"
