SUMMARY = "generated recipe based on pyxattr srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "attr pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-pyxattr = "ld-linux-aarch64.so.1 libattr.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-pyxattr = "glibc libattr platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-pyxattr-0.5.3-18.el8.aarch64.rpm \
          "

SRC_URI[python3-pyxattr.sha256sum] = "ebb5bde8f448098b07e785869bed673005b24b19704c819821477729b1f70476"
