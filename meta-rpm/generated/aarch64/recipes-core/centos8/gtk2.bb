SUMMARY = "generated recipe based on gtk2 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo cups-libs fontconfig freetype gdk-pixbuf glib-2.0 libx11 libxcomposite libxcursor libxdamage libxext libxfixes libxi libxinerama libxrandr libxrender pango pkgconfig-native"
RPM_SONAME_PROV_gtk2 = "libgailutil.so.18 libgdk-x11-2.0.so.0 libgtk-x11-2.0.so.0"
RPM_SONAME_REQ_gtk2 = "ld-linux-aarch64.so.1 libX11.so.6 libXcomposite.so.1 libXcursor.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libXrender.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libcups.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RDEPENDS_gtk2 = "atk bash cairo cups-libs fontconfig freetype gdk-pixbuf2 gdk-pixbuf2-modules glib2 glibc gtk-update-icon-cache hicolor-icon-theme libX11 libXcomposite libXcursor libXdamage libXext libXfixes libXi libXinerama libXrandr libXrender libtiff pango"
RPM_SONAME_REQ_gtk2-devel = "ld-linux-aarch64.so.1 libX11.so.6 libXcomposite.so.1 libXcursor.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libXrender.so.1 libatk-1.0.so.0 libc.so.6 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgailutil.so.18 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0"
RPROVIDES_gtk2-devel = "gtk2-dev (= 2.24.32)"
RDEPENDS_gtk2-devel = "atk atk-devel cairo cairo-devel fontconfig freetype gdk-pixbuf2 gdk-pixbuf2-devel glib2 glib2-devel glibc gtk2 libX11 libX11-devel libXcomposite libXcomposite-devel libXcursor libXcursor-devel libXdamage libXext libXext-devel libXfixes libXfixes-devel libXi libXi-devel libXinerama libXinerama-devel libXrandr libXrandr-devel libXrender pango pango-devel pkgconf-pkg-config platform-python"
RDEPENDS_gtk2-devel-docs = "gtk2"
RPM_SONAME_REQ_gtk2-immodule-xim = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libgdk-x11-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpthread.so.0"
RDEPENDS_gtk2-immodule-xim = "glib2 glibc gtk2 libX11 pango"
RPM_SONAME_REQ_gtk2-immodules = "ld-linux-aarch64.so.1 libc.so.6 libgdk-x11-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libpango-1.0.so.0 libpthread.so.0"
RDEPENDS_gtk2-immodules = "glib2 glibc gtk2 pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtk2-2.24.32-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtk2-devel-2.24.32-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtk2-devel-docs-2.24.32-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtk2-immodule-xim-2.24.32-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtk2-immodules-2.24.32-4.el8.aarch64.rpm \
          "

SRC_URI[gtk2.sha256sum] = "0ee88ea6a469064535555296c9d538f87f9f847c774a73a3b4a9992f4a000ec0"
SRC_URI[gtk2-devel.sha256sum] = "2817080de6ff8638e268f8488a8fc31d267ef082140911397cc73b3f95ea910f"
SRC_URI[gtk2-devel-docs.sha256sum] = "c799dc76e627e1f05932e21efead2d199719cc6d3ca73651bf4f25d604743f2d"
SRC_URI[gtk2-immodule-xim.sha256sum] = "6dbf07a3f2ad81174e04e5a490e5d2916848b2e2a9a6f9d784e2c9932e60878b"
SRC_URI[gtk2-immodules.sha256sum] = "ed4e7f2021e42b3f5ed890e0c1376989c69cac246ddb8a85587cc67fb1768942"
