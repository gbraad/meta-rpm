SUMMARY = "generated recipe based on fence-agents srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_fence-agents-all = "fence-agents-amt-ws fence-agents-apc fence-agents-apc-snmp fence-agents-bladecenter fence-agents-brocade fence-agents-cisco-mds fence-agents-cisco-ucs fence-agents-compute fence-agents-drac5 fence-agents-eaton-snmp fence-agents-emerson fence-agents-eps fence-agents-heuristics-ping fence-agents-hpblade fence-agents-ibmblade fence-agents-ifmib fence-agents-ilo-moonshot fence-agents-ilo-mp fence-agents-ilo-ssh fence-agents-ilo2 fence-agents-intelmodular fence-agents-ipdu fence-agents-ipmilan fence-agents-kdump fence-agents-mpath fence-agents-redfish fence-agents-rhevm fence-agents-rsa fence-agents-rsb fence-agents-sbd fence-agents-scsi fence-agents-vmware-rest fence-agents-vmware-soap fence-agents-wti"
RDEPENDS_fence-agents-amt-ws = "fence-agents-common openwsman-python3 platform-python"
RDEPENDS_fence-agents-apc = "fence-agents-common openssh-clients platform-python telnet"
RDEPENDS_fence-agents-apc-snmp = "fence-agents-common net-snmp-utils platform-python"
RDEPENDS_fence-agents-bladecenter = "fence-agents-common openssh-clients platform-python telnet"
RDEPENDS_fence-agents-brocade = "fence-agents-common openssh-clients platform-python telnet"
RDEPENDS_fence-agents-cisco-mds = "fence-agents-common net-snmp-utils platform-python"
RDEPENDS_fence-agents-cisco-ucs = "fence-agents-common platform-python python3-pycurl"
RDEPENDS_fence-agents-common = "platform-python python3-pexpect python3-pycurl"
RDEPENDS_fence-agents-compute = "fence-agents-common platform-python python3-requests"
RDEPENDS_fence-agents-drac5 = "fence-agents-common openssh-clients platform-python telnet"
RDEPENDS_fence-agents-eaton-snmp = "fence-agents-common net-snmp-utils platform-python"
RDEPENDS_fence-agents-emerson = "fence-agents-common platform-python"
RDEPENDS_fence-agents-eps = "fence-agents-common platform-python"
RDEPENDS_fence-agents-heuristics-ping = "fence-agents-common platform-python"
RDEPENDS_fence-agents-hpblade = "fence-agents-common openssh-clients platform-python telnet"
RDEPENDS_fence-agents-ibmblade = "fence-agents-common net-snmp-utils platform-python"
RDEPENDS_fence-agents-ifmib = "fence-agents-common net-snmp-utils platform-python"
RDEPENDS_fence-agents-ilo-moonshot = "fence-agents-common openssh-clients platform-python telnet"
RDEPENDS_fence-agents-ilo-mp = "fence-agents-common openssh-clients platform-python telnet"
RDEPENDS_fence-agents-ilo-ssh = "fence-agents-common openssh-clients platform-python"
RDEPENDS_fence-agents-ilo2 = "fence-agents-common gnutls-utils platform-python"
RDEPENDS_fence-agents-intelmodular = "fence-agents-common net-snmp-utils platform-python"
RDEPENDS_fence-agents-ipdu = "fence-agents-common net-snmp-utils platform-python"
RDEPENDS_fence-agents-ipmilan = "fence-agents-common ipmitool platform-python"
RPM_SONAME_REQ_fence-agents-kdump = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_fence-agents-kdump = "fence-agents-common glibc"
RDEPENDS_fence-agents-mpath = "device-mapper-multipath fence-agents-common platform-python"
RDEPENDS_fence-agents-redfish = "fence-agents-common platform-python python3-requests"
RDEPENDS_fence-agents-rhevm = "fence-agents-common platform-python"
RDEPENDS_fence-agents-rsa = "fence-agents-common openssh-clients platform-python telnet"
RDEPENDS_fence-agents-rsb = "fence-agents-common openssh-clients platform-python telnet"
RDEPENDS_fence-agents-sbd = "fence-agents-common platform-python sbd"
RDEPENDS_fence-agents-scsi = "fence-agents-common platform-python sg3_utils"
RDEPENDS_fence-agents-virsh = "fence-agents-common libvirt-client openssh-clients platform-python"
RDEPENDS_fence-agents-vmware-rest = "fence-agents-common platform-python"
RDEPENDS_fence-agents-vmware-soap = "fence-agents-common platform-python python3-requests python3-suds"
RDEPENDS_fence-agents-wti = "fence-agents-common openssh-clients platform-python telnet"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-all-4.2.1-41.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-amt-ws-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-apc-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-apc-snmp-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-bladecenter-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-brocade-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-cisco-mds-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-cisco-ucs-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-common-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-compute-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-drac5-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-eaton-snmp-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-emerson-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-eps-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-heuristics-ping-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-hpblade-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-ibmblade-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-ifmib-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-ilo-moonshot-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-ilo-mp-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-ilo-ssh-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-ilo2-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-intelmodular-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-ipdu-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-ipmilan-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-kdump-4.2.1-41.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-mpath-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-redfish-4.2.1-41.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-rhevm-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-rsa-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-rsb-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-sbd-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-scsi-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-virsh-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-vmware-rest-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-vmware-soap-4.2.1-41.el8_2.2.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fence-agents-wti-4.2.1-41.el8_2.2.noarch.rpm \
          "

SRC_URI[fence-agents-all.sha256sum] = "55bba870b0db191cf93dd3cf8fb55040ed6913e44b553ce8e496c9afa1b71b5b"
SRC_URI[fence-agents-amt-ws.sha256sum] = "68aa4e25853237b2cd2d6cfa2668335afdbdb38e695c18c6fa279ee280dcc366"
SRC_URI[fence-agents-apc.sha256sum] = "ea87751bfc03198aa9bd31846035a60e2269d6bc6e2889ccfed56584d7b75066"
SRC_URI[fence-agents-apc-snmp.sha256sum] = "0e8f47d431cad8b8ad7c75d41be784eb6ad14dde53a79c1faa5da4973910a4fb"
SRC_URI[fence-agents-bladecenter.sha256sum] = "bc86bfe12ac4f008bc564eb71512b0c4be7dbc10d17d1d8f94495063e9f82ece"
SRC_URI[fence-agents-brocade.sha256sum] = "85df9cde81b2a04a4dc460e8f7a6729457f22c9e44adc16c258d62d00434bad6"
SRC_URI[fence-agents-cisco-mds.sha256sum] = "ec0caa4d12ae315ef28afe18650608d41e2be4a74771eea359f0d39fa60bc730"
SRC_URI[fence-agents-cisco-ucs.sha256sum] = "7a34cc393508b39a2e4547df11552a68f38865ac5c2b769b046c77f9ee6d4c35"
SRC_URI[fence-agents-common.sha256sum] = "3d30137e03fd7557344ab4878c4815c1b538768a8afd25e9ca70755bf1e32828"
SRC_URI[fence-agents-compute.sha256sum] = "6160d1ec5e06c9eb46a1df629ecaec01d46cfc7ff31dfbbae7775dffd39cd566"
SRC_URI[fence-agents-drac5.sha256sum] = "06360f56f2cfd0f01a51dc12d46e7a64f50115bfedbd39e8d64e3b19ecebf0ad"
SRC_URI[fence-agents-eaton-snmp.sha256sum] = "5dcba81a5e81ecb07ed8332a35480e6d08fce5a400559cadf2a2001e8dec4f4f"
SRC_URI[fence-agents-emerson.sha256sum] = "d94721c174567c1a311b5f44a0581c03dfdf6fc90421adbdb675a22aef1bd450"
SRC_URI[fence-agents-eps.sha256sum] = "a21ed17bb1edde3f54f4fef1261721c7d7887bc50aa37dbeed1342e4948b8a11"
SRC_URI[fence-agents-heuristics-ping.sha256sum] = "b8d6ba40fda1aa363637680bb5c17b3f1beac193e1edbda0f09300af45ab320c"
SRC_URI[fence-agents-hpblade.sha256sum] = "9a679a7316a529ac4eee7e175a85622e084d300a20d11cf69af64360fa9b0545"
SRC_URI[fence-agents-ibmblade.sha256sum] = "046d4d6cec737bcc110563926f5b8255245466bc80a83263038785571fb0575d"
SRC_URI[fence-agents-ifmib.sha256sum] = "4248455b09e65dc0baa4a5680e96c59b317eba1274a3e8f1d56c71b4c2b0a1e8"
SRC_URI[fence-agents-ilo-moonshot.sha256sum] = "08ca08e16ffc29988f70ba2867a06acd44da82b4b2889383245ec49022c67012"
SRC_URI[fence-agents-ilo-mp.sha256sum] = "1b65d0e09ec7a5260c500b63a122cc7edacd042e01a81efcb802ba77359c01d9"
SRC_URI[fence-agents-ilo-ssh.sha256sum] = "9ce54b2a00e1ba0676c74d936018661243c3dde47b3b00d368512396ef6a0f5c"
SRC_URI[fence-agents-ilo2.sha256sum] = "c055c84193da2312f301f693d5156328af66cd509e9fe9875c66a6a22d784179"
SRC_URI[fence-agents-intelmodular.sha256sum] = "6480f48fa497e84331ecf0fac3a7150b69de39820b940a31f5310c8b094b0382"
SRC_URI[fence-agents-ipdu.sha256sum] = "cd3a1d46c5be736f5836699b74f3b93ae117e7dc334dd2ce94667a5b23bcf315"
SRC_URI[fence-agents-ipmilan.sha256sum] = "779ad92897f4d06f96f4fd57a360cd742a006db4350e0a6f9bf75fb8b5074e38"
SRC_URI[fence-agents-kdump.sha256sum] = "d8b233bb67f81e2d66e9b4b0648e91ba79c4fb4d0e84953d88c6ab82419f1b2f"
SRC_URI[fence-agents-mpath.sha256sum] = "ab4ae0d501bdd3915630620e3e4ef4ecf77c4241c2f4ec9699621950a4d6d3e2"
SRC_URI[fence-agents-redfish.sha256sum] = "071f60f2343e47cf15caa76cae0ad14b5349d1665856135e114b3a888c427ec2"
SRC_URI[fence-agents-rhevm.sha256sum] = "dd66758bffe5678bd92b10c7e6d9fac93c5a369ce381d5745fa75c2726303ee6"
SRC_URI[fence-agents-rsa.sha256sum] = "089650c9538c2e0fae8aa09c47d41b3cba44f38f1c1cc5d3542044e66732c26a"
SRC_URI[fence-agents-rsb.sha256sum] = "0e995b933d20eb299634070a7a7cbfeefeb0bd7d310447d6cbb270554e073354"
SRC_URI[fence-agents-sbd.sha256sum] = "ae7ced4533f114fc25f93dd460770e858eaf68858f6167485f0393a08149da4e"
SRC_URI[fence-agents-scsi.sha256sum] = "c5cf75c9e10b81789829ac48d081bf35a2f840015e42d7ba87580e8db0e9bb65"
SRC_URI[fence-agents-virsh.sha256sum] = "01f960c7bffaa7ed634810d3e736a33365b3cfaf281cd4154d9a10c263a8927e"
SRC_URI[fence-agents-vmware-rest.sha256sum] = "9cc2a3c0ea47b9999a93106dbf60af78814e85b60dc4e4314c719b45c3e90362"
SRC_URI[fence-agents-vmware-soap.sha256sum] = "1f4d9ed5ee98a5557cf072cb5563465f02a7d77d8deeaec34fa8cbf7bba4865d"
SRC_URI[fence-agents-wti.sha256sum] = "2f0dbe94e042f1aeb5b4f2088b9dac2463a9db7041b5ca1f997639830ea7de11"
