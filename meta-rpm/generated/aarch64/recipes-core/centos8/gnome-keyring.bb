SUMMARY = "generated recipe based on gnome-keyring srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gcr gdk-pixbuf glib-2.0 gtk+3 libcap-ng libgcrypt libgpg-error libselinux p11-kit pam pango pkgconfig-native"
RPM_SONAME_REQ_gnome-keyring = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcap-ng.so.0 libdl.so.2 libgck-1.so.0 libgcr-base-3.so.1 libgcr-ui-3.so.1 libgcrypt.so.20 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgpg-error.so.0 libgtk-3.so.0 libp11-kit.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_gnome-keyring = "atk cairo cairo-gobject gcr gdk-pixbuf2 glib2 glibc gtk3 libcap-ng libgcrypt libgpg-error openssh-clients p11-kit pango"
RPM_SONAME_REQ_gnome-keyring-pam = "ld-linux-aarch64.so.1 libc.so.6 libpam.so.0 libselinux.so.1"
RDEPENDS_gnome-keyring-pam = "glibc gnome-keyring libselinux pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-keyring-3.28.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-keyring-pam-3.28.2-1.el8.aarch64.rpm \
          "

SRC_URI[gnome-keyring.sha256sum] = "3eb0cc048b7353e246e2330c4817503029f48bf2a64033948ba73b2ddefc090f"
SRC_URI[gnome-keyring-pam.sha256sum] = "b3d5dd39406b1217b33e4310305bf55c14eea6ae6aee0407ccfec750230d3677"
