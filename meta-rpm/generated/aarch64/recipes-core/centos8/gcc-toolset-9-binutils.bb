SUMMARY = "generated recipe based on gcc-toolset-9-binutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_gcc-toolset-9-binutils = "libbfd-2.32-17.el8_1.so libopcodes-2.32-17.el8_1.so"
RPM_SONAME_REQ_gcc-toolset-9-binutils = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-binutils = "bash chkconfig coreutils gcc-toolset-9-runtime glibc info libgcc libstdc++"
RPROVIDES_gcc-toolset-9-binutils-devel = "gcc-toolset-9-binutils-dev (= 2.32)"
RDEPENDS_gcc-toolset-9-binutils-devel = "coreutils gcc-toolset-9-binutils gcc-toolset-9-runtime zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-binutils-2.32-17.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-binutils-devel-2.32-17.el8_1.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-binutils.sha256sum] = "feeb643b0938e8ff30e9e04132fd59da7c314bbf2f7fc177b475de49e9d25c94"
SRC_URI[gcc-toolset-9-binutils-devel.sha256sum] = "9a85fdabb9d85207043a292c097fe1fd655227f427874f116690626bcd453315"
