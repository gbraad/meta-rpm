SUMMARY = "generated recipe based on perl-IPC-Cmd srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IPC-Cmd = "perl-Carp perl-Exporter perl-ExtUtils-MM-Utils perl-IO perl-Locale-Maketext-Simple perl-Module-Load-Conditional perl-Params-Check perl-PathTools perl-Socket perl-Text-ParseWords perl-Time-HiRes perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-IPC-Cmd-1.02-1.el8.noarch.rpm \
          "

SRC_URI[perl-IPC-Cmd.sha256sum] = "1ef902fb094010887ae54c94ab93a894b383edd635e8303a4435c9dd8502feae"
