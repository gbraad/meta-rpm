SUMMARY = "generated recipe based on libeasyfc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "fontconfig freetype glib-2.0 harfbuzz libxml2 pkgconfig-native"
RPM_SONAME_PROV_libeasyfc = "libeasyfc.so.0"
RPM_SONAME_REQ_libeasyfc = "ld-linux-aarch64.so.1 libc.so.6 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libharfbuzz.so.0 libxml2.so.2"
RDEPENDS_libeasyfc = "fontconfig freetype glib2 glibc harfbuzz libxml2"
RPM_SONAME_PROV_libeasyfc-gobject = "libeasyfc-gobject.so.0"
RPM_SONAME_REQ_libeasyfc-gobject = "libc.so.6 libeasyfc.so.0 libfontconfig.so.1 libfreetype.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libharfbuzz.so.0 libxml2.so.2"
RDEPENDS_libeasyfc-gobject = "fontconfig freetype glib2 glibc harfbuzz libeasyfc libxml2"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libeasyfc-0.14.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libeasyfc-gobject-0.14.0-1.el8.aarch64.rpm \
          "

SRC_URI[libeasyfc.sha256sum] = "30c1a911056d70916c7b449c89291779ac1e5b9f3cca688c35859fdddc4c3356"
SRC_URI[libeasyfc-gobject.sha256sum] = "59e8a77f88be918c433ff5bd4b4ba1a0ddadb5ec39faf1979f85fdebb8e456b3"
