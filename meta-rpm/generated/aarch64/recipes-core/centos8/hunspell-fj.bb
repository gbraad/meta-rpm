SUMMARY = "generated recipe based on hunspell-fj srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-2.0 | MPL-1.1"
RPM_LICENSE = "LGPLv2+ or GPLv2+ or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-fj = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-fj-1.2-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-fj.sha256sum] = "2cc8bc3bce946c7584af3f777440ff727d224520c0ba2f0bd9d21e0524acbbf5"
