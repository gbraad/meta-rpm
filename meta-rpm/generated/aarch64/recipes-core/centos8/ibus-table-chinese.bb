SUMMARY = "generated recipe based on ibus-table-chinese srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_ibus-table-chinese = "ibus-table"
RDEPENDS_ibus-table-chinese-array = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-cangjie = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-cantonese = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-easy = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-erbi = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-quick = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-scj = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-stroke5 = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-wu = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-wubi-haifeng = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-wubi-jidian = "bash ibus-table-chinese"
RDEPENDS_ibus-table-chinese-yong = "bash ibus-table-chinese"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-array-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-cangjie-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-cantonese-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-easy-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-erbi-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-quick-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-scj-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-stroke5-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-wu-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-wubi-haifeng-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-wubi-jidian-1.8.2-9.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ibus-table-chinese-yong-1.8.2-9.el8.noarch.rpm \
          "

SRC_URI[ibus-table-chinese.sha256sum] = "cf4b9a259ff6ce27b0b7885f527867edf4087ba24b52adc3cc50bd7da7b4a0d3"
SRC_URI[ibus-table-chinese-array.sha256sum] = "2259c462d52b66f9ee0a7203abad0ebea19bef69c88d8c251770c915a9bcfbf4"
SRC_URI[ibus-table-chinese-cangjie.sha256sum] = "37f8befe5d5c21d8b5d2e826f1e0efa347a182b04d8bd9cf31fc7049607b6675"
SRC_URI[ibus-table-chinese-cantonese.sha256sum] = "ea18c179c3cfee2938ed7d547237df5713b1711d18dc184ee5cfbd0226618a59"
SRC_URI[ibus-table-chinese-easy.sha256sum] = "da82fc34fbdf9e11ecfcbb6a68c05b04e5cb4f40499c63c45645f45d86601172"
SRC_URI[ibus-table-chinese-erbi.sha256sum] = "9b37fcff1fa3b4ad1e27f764c7b46cb1da0005799a407bc905d975efad00abc8"
SRC_URI[ibus-table-chinese-quick.sha256sum] = "5fd8926f18c0ab6c26aa2d8550034a1db83daea71bd1490360a5bc855b61859e"
SRC_URI[ibus-table-chinese-scj.sha256sum] = "cc0967f43335468d5c45e5a8fee59cc5a0fee3fec5537e45ba4d207dbfe03389"
SRC_URI[ibus-table-chinese-stroke5.sha256sum] = "06a050a835659c53a77edd2592b2dc4ea86e2c84f91629865b74174b8506a88a"
SRC_URI[ibus-table-chinese-wu.sha256sum] = "e0d3b680ea7627f72e542f1ec728b69af13b25b03efe83aa6af9a34151866e5b"
SRC_URI[ibus-table-chinese-wubi-haifeng.sha256sum] = "9c798cb4d4548aabfe24362b936b78a8de64ae4163957d7612a1669f76ce1dc6"
SRC_URI[ibus-table-chinese-wubi-jidian.sha256sum] = "66b699102355847c6ad3bbde5d8f9a20a3dbda1e8360df96d9d0f82e551af7c5"
SRC_URI[ibus-table-chinese-yong.sha256sum] = "3ed615a01c4a9ef5ec6d91b04a20f13176554728f47c39cb5a283f688555cec2"
