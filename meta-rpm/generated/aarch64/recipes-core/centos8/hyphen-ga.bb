SUMMARY = "generated recipe based on hyphen-ga srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-ga = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-ga-0.20040220-16.el8.noarch.rpm \
          "

SRC_URI[hyphen-ga.sha256sum] = "795f1a050461f8d3f311c712774bad2dd090ad8db0f1f1d0694cba2217b93f19"
