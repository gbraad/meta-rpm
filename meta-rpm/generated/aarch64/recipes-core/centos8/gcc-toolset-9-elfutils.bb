SUMMARY = "generated recipe based on gcc-toolset-9-elfutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-3.0"
RPM_LICENSE = "GPLv2+ or LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 elfutils pkgconfig-native xz zlib"
RPM_SONAME_REQ_gcc-toolset-9-elfutils = "ld-linux-aarch64.so.1 libasm.so.dts.1 libc.so.6 libdl.so.2 libdw.so.dts.1 libelf.so.dts.1 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-elfutils = "bash gcc-toolset-9-elfutils-libelf gcc-toolset-9-elfutils-libs gcc-toolset-9-runtime glibc libstdc++"
RPROVIDES_gcc-toolset-9-elfutils-devel = "gcc-toolset-9-elfutils-dev (= 0.176)"
RDEPENDS_gcc-toolset-9-elfutils-devel = "bzip2-devel elfutils-libelf-devel gcc-toolset-9-elfutils-libelf-devel gcc-toolset-9-runtime pkgconf-pkg-config xz-devel zlib-devel"
RPM_SONAME_PROV_gcc-toolset-9-elfutils-libelf = "libelf.so.dts.1"
RPM_SONAME_REQ_gcc-toolset-9-elfutils-libelf = "ld-linux-aarch64.so.1 libc.so.6 libz.so.1"
RDEPENDS_gcc-toolset-9-elfutils-libelf = "gcc-toolset-9-runtime glibc zlib"
RPROVIDES_gcc-toolset-9-elfutils-libelf-devel = "gcc-toolset-9-elfutils-libelf-dev (= 0.176)"
RDEPENDS_gcc-toolset-9-elfutils-libelf-devel = "gcc-toolset-9-runtime pkgconf-pkg-config zlib-devel"
RPM_SONAME_PROV_gcc-toolset-9-elfutils-libs = "libasm.so.dts.1 libdw.so.dts.1"
RPM_SONAME_REQ_gcc-toolset-9-elfutils-libs = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libdl.so.2 libelf.so.dts.1 liblzma.so.5 libz.so.1"
RDEPENDS_gcc-toolset-9-elfutils-libs = "bzip2-libs gcc-toolset-9-elfutils-libelf gcc-toolset-9-runtime glibc xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-elfutils-0.176-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-elfutils-devel-0.176-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-elfutils-libelf-0.176-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-elfutils-libelf-devel-0.176-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-elfutils-libs-0.176-5.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-elfutils.sha256sum] = "18f0312269dd23039c49347105d0bac726b2fe094f68c9640c1b6076ee762188"
SRC_URI[gcc-toolset-9-elfutils-devel.sha256sum] = "d8eba7a72041e11215bbda4e2e6c76fb26658b6da72c8976da51344083480236"
SRC_URI[gcc-toolset-9-elfutils-libelf.sha256sum] = "ef634bfdb7c77b697c439972077b69c42ba3ec683d12d745611117b44050ada1"
SRC_URI[gcc-toolset-9-elfutils-libelf-devel.sha256sum] = "49e1e35c1f2160cda595299d9195684f549f113584d7c1995a0fc826e002f2a5"
SRC_URI[gcc-toolset-9-elfutils-libs.sha256sum] = "a72be666b7f0ac652a4a4ba057fbcd2187a67177e44c748a6da582f7c9ef9e9c"
