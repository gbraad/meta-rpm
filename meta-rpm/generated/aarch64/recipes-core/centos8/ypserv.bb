SUMMARY = "generated recipe based on ypserv srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libnsl2 libtirpc libxcrypt pkgconfig-native systemd-libs tokyocabinet"
RPM_SONAME_REQ_ypserv = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libnsl.so.2 libsystemd.so.0 libtirpc.so.3 libtokyocabinet.so.9"
RDEPENDS_ypserv = "bash gawk glibc hostname libnsl2 libtirpc libxcrypt make rpcbind systemd systemd-libs tokyocabinet"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ypserv-4.0-6.20170331git5bfba76.el8.aarch64.rpm \
          "

SRC_URI[ypserv.sha256sum] = "2a42d752c1c0df9b28d75ce5054602fd0ad06f18c7345d4d87e509e5260ef003"
