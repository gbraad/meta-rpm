SUMMARY = "generated recipe based on libXcomposite srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "xcomposite"
DEPENDS = "libx11 libxfixes pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXcomposite = "libXcomposite.so.1"
RPM_SONAME_REQ_libXcomposite = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6"
RDEPENDS_libXcomposite = "glibc libX11"
RPM_SONAME_REQ_libXcomposite-devel = "libXcomposite.so.1"
RPROVIDES_libXcomposite-devel = "libXcomposite-dev (= 0.4.4)"
RDEPENDS_libXcomposite-devel = "libX11-devel libXcomposite libXfixes-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXcomposite-0.4.4-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXcomposite-devel-0.4.4-14.el8.aarch64.rpm \
          "

SRC_URI[libXcomposite.sha256sum] = "79249bbbcf7b1d52fb6595171c090805d0826ea4eb626354c70b0afd80ccfe20"
SRC_URI[libXcomposite-devel.sha256sum] = "2e4cb4c18f0ec60ad95897385d0e1b0a33391b13ca7cc5fa763df350b7ce41ce"
