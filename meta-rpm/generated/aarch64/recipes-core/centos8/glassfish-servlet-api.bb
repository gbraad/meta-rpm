SUMMARY = "generated recipe based on glassfish-servlet-api srpm"
DESCRIPTION = "Description"
LICENSE = "(CDDL-1.0 | GPL-2.0) & CLOSED"
RPM_LICENSE = "(CDDL or GPLv2 with exceptions) and ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_glassfish-servlet-api = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_glassfish-servlet-api-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glassfish-servlet-api-3.1.0-14.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glassfish-servlet-api-javadoc-3.1.0-14.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[glassfish-servlet-api.sha256sum] = "889a67908c2bf3f3887adf1d8a4a43a0797ca4d6c39262c3e6083331c2f18b92"
SRC_URI[glassfish-servlet-api-javadoc.sha256sum] = "5d996432c5f167d18e38fd8bcfd8af3a97d37177a7cd85db101738d493649e41"
