SUMMARY = "generated recipe based on libXfixes srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXfixes = "libXfixes.so.3"
RPM_SONAME_REQ_libXfixes = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6"
RDEPENDS_libXfixes = "glibc libX11"
RPM_SONAME_REQ_libXfixes-devel = "libXfixes.so.3"
RPROVIDES_libXfixes-devel = "libXfixes-dev (= 5.0.3)"
RDEPENDS_libXfixes-devel = "libX11-devel libXfixes pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXfixes-5.0.3-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXfixes-devel-5.0.3-7.el8.aarch64.rpm \
          "

SRC_URI[libXfixes.sha256sum] = "6f8762daaee4811e5f25c36437ccde57f12ad655801fcbf307d6722a98133d57"
SRC_URI[libXfixes-devel.sha256sum] = "1ac0a22c899ffac27ef541136122d4e3b8137b447b71d42ce9768a6ccb95a8da"
