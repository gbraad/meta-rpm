SUMMARY = "generated recipe based on ant-contrib srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & CLOSED"
RPM_LICENSE = "ASL 2.0 and ASL 1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_ant-contrib = "ant java-1.8.0-openjdk-headless javapackages-filesystem junit xerces-j2"
RDEPENDS_ant-contrib-javadoc = "javapackages-filesystem javapackages-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-contrib-1.0-0.32.b3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ant-contrib-javadoc-1.0-0.32.b3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[ant-contrib.sha256sum] = "c1573dd4c468279b5f57b150ab785b97ef027bc262f0984ce59d0b0721d76bc6"
SRC_URI[ant-contrib-javadoc.sha256sum] = "64bf5c1ba9bf1f246b4dba663a34ea019c2d25305e9f5e01910593ff103f2e91"
