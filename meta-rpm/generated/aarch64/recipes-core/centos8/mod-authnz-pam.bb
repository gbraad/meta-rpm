SUMMARY = "generated recipe based on mod_authnz_pam srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pam pkgconfig-native"
RPM_SONAME_REQ_mod_authnz_pam = "ld-linux-aarch64.so.1 libc.so.6 libpam.so.0"
RDEPENDS_mod_authnz_pam = "glibc httpd pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_authnz_pam-1.1.0-7.el8.aarch64.rpm \
          "

SRC_URI[mod_authnz_pam.sha256sum] = "83a2df3639aa07e19ed8b069da23f66f9226ceb19076f408aebda4fcd86ae0da"
