SUMMARY = "generated recipe based on po4a srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_po4a = "bash gettext libxslt opensp perl-Carp perl-Encode perl-Exporter perl-File-Path perl-File-Temp perl-Getopt-Long perl-IO perl-Locale-gettext perl-PathTools perl-Pod-Parser perl-Pod-Usage perl-TermReadKey perl-Text-WrapI18N perl-Time-Local perl-Unicode-LineBreak perl-interpreter perl-libs texlive-kpathsea"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/po4a-0.52-4.el8.noarch.rpm \
          "

SRC_URI[po4a.sha256sum] = "baf2b145e5d7852beb1de157e1cdf3d084e33a5e93ae2f9eaac43a265efec64a"
