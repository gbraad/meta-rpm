SUMMARY = "generated recipe based on lasso srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libtool-ltdl libxml2 libxslt openssl pkgconfig-native xmlsec1 zlib"
RPM_SONAME_PROV_lasso = "liblasso.so.3"
RPM_SONAME_REQ_lasso = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libglib-2.0.so.0 libgobject-2.0.so.0 libltdl.so.7 libm.so.6 libssl.so.1.1 libxml2.so.2 libxmlsec1-openssl.so.1 libxmlsec1.so.1 libxslt.so.1 libz.so.1"
RDEPENDS_lasso = "glib2 glibc libtool-ltdl libxml2 libxslt openssl-libs xmlsec1 xmlsec1-openssl zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lasso-2.6.0-8.el8.aarch64.rpm \
          "

SRC_URI[lasso.sha256sum] = "86444f9f8f1364e726985231dd79c7cfc82fe34150d8a1ff82ad10d213e3a6de"
