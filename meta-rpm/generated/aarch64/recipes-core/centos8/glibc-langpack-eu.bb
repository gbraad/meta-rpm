SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-eu = "locale-base-eu-es (= 2.28) locale-base-eu-es.utf8 (= 2.28) locale-base-eu-es@euro (= 2.28) virtual-locale-eu (= 2.28) virtual-locale-eu (= 2.28) virtual-locale-eu (= 2.28) virtual-locale-eu-es (= 2.28) virtual-locale-eu-es.utf8 (= 2.28) virtual-locale-eu-es@euro (= 2.28)"
RDEPENDS_glibc-langpack-eu = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-eu-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-eu.sha256sum] = "edfa61b32e00efe9bc187503ec4670001143bd202be18c947ee0ce08ad6c06e9"
