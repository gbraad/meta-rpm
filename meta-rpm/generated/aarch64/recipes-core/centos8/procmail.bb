SUMMARY = "generated recipe based on procmail srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPLv2+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_procmail = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_procmail = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/procmail-3.22-47.el8.aarch64.rpm \
          "

SRC_URI[procmail.sha256sum] = "8ca7a88cf2da08a7224aec8bc41ed672e7476a66e963f9d10dbaf98d9204a010"
