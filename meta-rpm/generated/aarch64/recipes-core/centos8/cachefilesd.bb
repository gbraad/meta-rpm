SUMMARY = "generated recipe based on cachefilesd srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_cachefilesd = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_cachefilesd = "bash glibc selinux-policy-minimum systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cachefilesd-0.10.10-4.el8.aarch64.rpm \
          "

SRC_URI[cachefilesd.sha256sum] = "6cd24a9c5fb1faad6bad59187819f126eb1ba746ea17cd34c3a1593a6cf3de6f"
