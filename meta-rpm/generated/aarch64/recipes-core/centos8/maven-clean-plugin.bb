SUMMARY = "generated recipe based on maven-clean-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-clean-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-shared-utils"
RDEPENDS_maven-clean-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-clean-plugin-3.0.0-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-clean-plugin-javadoc-3.0.0-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-clean-plugin.sha256sum] = "128030ab2f7945b4f48e5e652966b00c13e05230445b6272909cc8a50cd52423"
SRC_URI[maven-clean-plugin-javadoc.sha256sum] = "64403651bb0f12b02a9a6d1d04d5a28ba264d3ce1ab71421c60fddf7cac4989d"
