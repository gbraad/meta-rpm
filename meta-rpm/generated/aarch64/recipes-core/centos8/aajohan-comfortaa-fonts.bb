SUMMARY = "generated recipe based on aajohan-comfortaa-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_aajohan-comfortaa-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/aajohan-comfortaa-fonts-3.001-2.el8.noarch.rpm \
          "

SRC_URI[aajohan-comfortaa-fonts.sha256sum] = "9c760ee8bc6bcde40a8c51cc4c9255b0b950420f9cdb7c15883377bc3558bf21"
