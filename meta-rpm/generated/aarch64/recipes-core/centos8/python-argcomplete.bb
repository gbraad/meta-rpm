SUMMARY = "generated recipe based on python-argcomplete srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-argcomplete = "bash platform-python platform-python-setuptools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-argcomplete-1.9.3-6.el8.noarch.rpm \
          "

SRC_URI[python3-argcomplete.sha256sum] = "0f9ceef94199fd255715284626d4124e6b99bd0d4941506e7df02f74583ae540"
