SUMMARY = "generated recipe based on xdg-desktop-portal-gtk srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libgcc pango pkgconfig-native"
RPM_SONAME_REQ_xdg-desktop-portal-gtk = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_xdg-desktop-portal-gtk = "atk bash cairo cairo-gobject dbus gdk-pixbuf2 glib2 glibc gtk3 libgcc pango systemd xdg-desktop-portal"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xdg-desktop-portal-gtk-1.4.0-1.el8.aarch64.rpm \
          "

SRC_URI[xdg-desktop-portal-gtk.sha256sum] = "e3dc7938e61e55ea8686444c1ed5c7797829d8e8cd61c6e598c5e272ab0d8fed"
