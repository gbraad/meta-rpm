SUMMARY = "generated recipe based on sblim-cmpi-base srpm"
DESCRIPTION = "Description"
LICENSE = "EPL-1.0"
RPM_LICENSE = "EPL-1.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native sblim-indication-helper"
RPM_SONAME_PROV_sblim-cmpi-base = "libcmpiOSBase_BaseBoardProvider.so libcmpiOSBase_CSBaseBoardProvider.so libcmpiOSBase_CSProcessorProvider.so libcmpiOSBase_Common.so.0 libcmpiOSBase_ComputerSystemProvider.so libcmpiOSBase_OSProcessProvider.so libcmpiOSBase_OperatingSystemProvider.so libcmpiOSBase_OperatingSystemStatisticalDataProvider.so libcmpiOSBase_OperatingSystemStatisticsProvider.so libcmpiOSBase_ProcessorProvider.so libcmpiOSBase_RunningOSProvider.so libcmpiOSBase_UnixProcessProvider.so libdmiinfo.so.0"
RPM_SONAME_REQ_sblim-cmpi-base = "ld-linux-aarch64.so.1 libc.so.6 libind_helper.so.0 libpthread.so.0"
RDEPENDS_sblim-cmpi-base = "bash glibc sblim-indication_helper sblim-sfcb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sblim-cmpi-base-1.6.4-14.el8.aarch64.rpm \
          "

SRC_URI[sblim-cmpi-base.sha256sum] = "591796b1e35f12ade4d10eca24d8a4bb61236544f14e0a71979a8c4a7d1a9515"
