SUMMARY = "generated recipe based on saab-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+ with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_saab-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/saab-fonts-0.91-16.el8.noarch.rpm \
          "

SRC_URI[saab-fonts.sha256sum] = "ccc5f8c3892ccee17f4401bdf786adeac83ee4d4d11bc940cc5be15fce1856bf"
