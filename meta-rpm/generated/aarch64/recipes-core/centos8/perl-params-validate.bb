SUMMARY = "generated recipe based on perl-Params-Validate srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0 & (GPL-2.0 | Artistic-1.0)"
RPM_LICENSE = "Artistic 2.0 and (GPL+ or Artistic)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Params-Validate = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Params-Validate = "glibc perl-Carp perl-Exporter perl-Module-Implementation perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Params-Validate-1.29-5.el8.aarch64.rpm \
          "

SRC_URI[perl-Params-Validate.sha256sum] = "2804c950875cf7fc7c0a951cadb7bae3be486589aba08f6627836fe1cb56ea5f"
