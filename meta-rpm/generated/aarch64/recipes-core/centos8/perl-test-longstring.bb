SUMMARY = "generated recipe based on perl-Test-LongString srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-LongString = "perl-Exporter perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Test-LongString-0.17-10.el8.noarch.rpm \
          "

SRC_URI[perl-Test-LongString.sha256sum] = "a78f7f542690e94bdd287e9b51f7d2325b573dbe6164c8435efc383ba97ec422"
