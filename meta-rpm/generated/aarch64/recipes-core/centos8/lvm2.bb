SUMMARY = "generated recipe based on lvm2 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "device-mapper device-mapper-event libaio libblkid libselinux libsepol pkgconfig-native readline sanlock systemd-libs"
RPM_SONAME_REQ_lvm2 = "ld-linux-aarch64.so.1 libaio.so.1 libblkid.so.1 libc.so.6 libdevmapper-event.so.1.02 libdevmapper.so.1.02 libm.so.6 libpthread.so.0 libreadline.so.7 libselinux.so.1 libsepol.so.1 libsystemd.so.0 libudev.so.1"
RDEPENDS_lvm2 = "bash device-mapper-event-libs device-mapper-libs device-mapper-persistent-data glibc kmod libaio libblkid libselinux libsepol lvm2-libs readline systemd systemd-libs"
RDEPENDS_lvm2-dbusd = "bash dbus lvm2 platform-python python3-dbus python3-gobject-base python3-pyudev systemd"
RPM_SONAME_REQ_lvm2-devel = "libdevmapper-event-lvm2.so.2.03 liblvm2cmd.so.2.03"
RPROVIDES_lvm2-devel = "lvm2-dev (= 2.03.08)"
RDEPENDS_lvm2-devel = "device-mapper-devel device-mapper-event-devel lvm2 lvm2-libs pkgconf-pkg-config"
RPM_SONAME_PROV_lvm2-libs = "libdevmapper-event-lvm2.so.2.03 libdevmapper-event-lvm2mirror.so.2.03 libdevmapper-event-lvm2raid.so.2.03 libdevmapper-event-lvm2snapshot.so.2.03 libdevmapper-event-lvm2thin.so.2.03 libdevmapper-event-lvm2vdo.so.2.03 liblvm2cmd.so.2.03"
RPM_SONAME_REQ_lvm2-libs = "ld-linux-aarch64.so.1 libaio.so.1 libblkid.so.1 libc.so.6 libdevmapper-event.so.1.02 libdevmapper.so.1.02 libm.so.6 libpthread.so.0 libselinux.so.1 libsepol.so.1 libsystemd.so.0 libudev.so.1"
RDEPENDS_lvm2-libs = "device-mapper-event device-mapper-event-libs device-mapper-libs glibc libaio libblkid libselinux libsepol systemd-libs"
RPM_SONAME_REQ_lvm2-lockd = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libm.so.6 libpthread.so.0 libsanlock_client.so.1 libselinux.so.1 libsepol.so.1 libsystemd.so.0 libudev.so.1"
RDEPENDS_lvm2-lockd = "bash glibc libblkid libselinux libsepol lvm2 sanlock-lib systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lvm2-2.03.08-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lvm2-dbusd-2.03.08-3.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lvm2-libs-2.03.08-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lvm2-lockd-2.03.08-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lvm2-devel-2.03.08-3.el8.aarch64.rpm \
          "

SRC_URI[lvm2.sha256sum] = "ca27af9f66f139e12d4ebeacaeaa40628ba9c9c2f1d9f8b2a8719d6be3e866d2"
SRC_URI[lvm2-dbusd.sha256sum] = "251d3a67117d570a1ff6b939dac0f70f6780bc4477a8d4b2921aff195663fbaf"
SRC_URI[lvm2-devel.sha256sum] = "9ebfc0bdfbd2449552a98f6c6b8d516c3c0d1716692f82ee4f69f7428d8a66a3"
SRC_URI[lvm2-libs.sha256sum] = "b2c5e8eca111a5d3c26354373a6c1ccdb8bb7c1ed6a795792368d2faabdcb678"
SRC_URI[lvm2-lockd.sha256sum] = "562a28575859e8f92d13e9e6e51e0f8a9c72a1e79df5030188e42a4eea6478cc"
