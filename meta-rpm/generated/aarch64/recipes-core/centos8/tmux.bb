SUMMARY = "generated recipe based on tmux srpm"
DESCRIPTION = "Description"
LICENSE = "ISC & BSD"
RPM_LICENSE = "ISC and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libevent ncurses pkgconfig-native"
RPM_SONAME_REQ_tmux = "ld-linux-aarch64.so.1 libc.so.6 libevent-2.1.so.6 libresolv.so.2 libtinfo.so.6 libutil.so.1"
RDEPENDS_tmux = "bash glibc libevent ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tmux-2.7-1.el8.aarch64.rpm \
          "

SRC_URI[tmux.sha256sum] = "670476d9cd73fa1b6b3a3b132def41a4d3cc872a1876434ee8ee79c626a5e3c4"
