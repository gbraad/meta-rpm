SUMMARY = "generated recipe based on fftw srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_fftw = "ld-linux-aarch64.so.1 libc.so.6 libfftw3.so.3 libfftw3_threads.so.3 libfftw3f.so.3 libfftw3f_threads.so.3 libfftw3l.so.3 libfftw3l_threads.so.3 libm.so.6 libpthread.so.0"
RDEPENDS_fftw = "bash fftw-libs-double fftw-libs-long fftw-libs-single glibc info"
RPM_SONAME_REQ_fftw-devel = "libfftw3.so.3 libfftw3_omp.so.3 libfftw3_threads.so.3 libfftw3f.so.3 libfftw3f_omp.so.3 libfftw3f_threads.so.3 libfftw3l.so.3 libfftw3l_omp.so.3 libfftw3l_threads.so.3"
RPROVIDES_fftw-devel = "fftw-dev (= 3.3.5)"
RDEPENDS_fftw-devel = "bash fftw fftw-libs fftw-libs-double fftw-libs-long fftw-libs-single pkgconf-pkg-config"
RDEPENDS_fftw-libs = "fftw-libs-double fftw-libs-long fftw-libs-single"
RPM_SONAME_PROV_fftw-libs-double = "libfftw3.so.3 libfftw3_omp.so.3 libfftw3_threads.so.3"
RPM_SONAME_REQ_fftw-libs-double = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_fftw-libs-double = "glibc libgcc libgomp"
RPM_SONAME_PROV_fftw-libs-long = "libfftw3l.so.3 libfftw3l_omp.so.3 libfftw3l_threads.so.3"
RPM_SONAME_REQ_fftw-libs-long = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_fftw-libs-long = "glibc libgcc libgomp"
RPM_SONAME_PROV_fftw-libs-single = "libfftw3f.so.3 libfftw3f_omp.so.3 libfftw3f_threads.so.3"
RPM_SONAME_REQ_fftw-libs-single = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgomp.so.1 libm.so.6 libpthread.so.0"
RDEPENDS_fftw-libs-single = "glibc libgcc libgomp"
RDEPENDS_fftw-static = "fftw-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fftw-3.3.5-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fftw-devel-3.3.5-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fftw-libs-3.3.5-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fftw-libs-double-3.3.5-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fftw-libs-long-3.3.5-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fftw-libs-single-3.3.5-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fftw-static-3.3.5-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/fftw-doc-3.3.5-11.el8.noarch.rpm \
          "

SRC_URI[fftw.sha256sum] = "0617d5382839c29b86677c8e438cc6489c49477f45f1ab4287947cb97aefe30d"
SRC_URI[fftw-devel.sha256sum] = "40e7ac032188d43b55da903a5bafc6410f0f327fe5fbec36ec7e59fa0a9f5cfc"
SRC_URI[fftw-doc.sha256sum] = "d20b4d8314ec79369bac8e9fb624666dcd995b40b89a8b1f48a6c26b8135135c"
SRC_URI[fftw-libs.sha256sum] = "03a3cd2641d8bdba18e4d9a057a648f813d8fd2a4f2f3059f03fe4ad5ed70f5d"
SRC_URI[fftw-libs-double.sha256sum] = "deea3d817171c7a33600e3f572ce647462c109f4e8ad177c979242c874657ca7"
SRC_URI[fftw-libs-long.sha256sum] = "3584b022bbedab440d8bf60c5dc3c6714ea5d584d9218a5f55257b8e2c5230e0"
SRC_URI[fftw-libs-single.sha256sum] = "711c9693ca1e90b3cb655d5e6fe2d20d84c12a242001d1d0599ccfc312e34a3e"
SRC_URI[fftw-static.sha256sum] = "7bcb459cec6c33d969da4adf35bdde96ee3b742eda18ff4e2daf9ee1bfafb778"
