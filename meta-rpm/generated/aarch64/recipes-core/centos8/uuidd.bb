SUMMARY = "generated recipe based on util-linux srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libuuid pkgconfig-native systemd-libs"
RPM_SONAME_REQ_uuidd = "ld-linux-aarch64.so.1 libc.so.6 librt.so.1 libsystemd.so.0 libuuid.so.1"
RDEPENDS_uuidd = "bash glibc libuuid shadow-utils systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/uuidd-2.32.1-22.el8.aarch64.rpm \
          "

SRC_URI[uuidd.sha256sum] = "88745f9148250c0718dce5f19711222064eff0e1d69feea7aa6dab16e6f45445"
