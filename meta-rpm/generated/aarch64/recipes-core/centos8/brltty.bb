SUMMARY = "generated recipe based on brltty srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib at-spi2-core bluez dbus-libs espeak-ng glib-2.0 libx11 libxaw libxext libxt libxtst pkgconfig-native platform-python3 tcl"
RPM_SONAME_PROV_brlapi = "libbrlapi.so.0.6"
RPM_SONAME_REQ_brlapi = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXtst.so.6 libbluetooth.so.3 libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_brlapi = "bash bluez-libs brltty coreutils glibc glibc-common libX11 libXext libXtst shadow-utils util-linux"
RPM_SONAME_REQ_brlapi-devel = "libbrlapi.so.0.6"
RPROVIDES_brlapi-devel = "brlapi-dev (= 0.6.7)"
RDEPENDS_brlapi-devel = "brlapi"
RPM_SONAME_REQ_brlapi-java = "ld-linux-aarch64.so.1 libbrlapi.so.0.6 libc.so.6"
RDEPENDS_brlapi-java = "brlapi glibc"
RPM_SONAME_REQ_brltty = "ld-linux-aarch64.so.1 libasound.so.2 libbluetooth.so.3 libbrlapi.so.0.6 libc.so.6 libdbus-1.so.3 libdl.so.2 libpthread.so.0"
RDEPENDS_brltty = "alsa-lib bash bluez-libs brlapi coreutils dbus-libs glibc platform-python systemd"
RPM_SONAME_REQ_brltty-at-spi2 = "ld-linux-aarch64.so.1 libatspi.so.0 libc.so.6 libdbus-1.so.3 libglib-2.0.so.0"
RDEPENDS_brltty-at-spi2 = "at-spi2-core brltty dbus-libs glib2 glibc"
RDEPENDS_brltty-docs = "brltty"
RDEPENDS_brltty-dracut = "bash brltty dracut"
RPM_SONAME_REQ_brltty-espeak-ng = "ld-linux-aarch64.so.1 libc.so.6 libespeak-ng.so.1"
RDEPENDS_brltty-espeak-ng = "brltty espeak-ng glibc"
RPM_SONAME_REQ_brltty-xw = "ld-linux-aarch64.so.1 libX11.so.6 libXaw.so.7 libXt.so.6 libc.so.6"
RDEPENDS_brltty-xw = "brltty glibc libX11 libXaw libXt ucs-miscfixed-fonts xorg-x11-fonts-misc"
RPM_SONAME_REQ_python3-brlapi = "ld-linux-aarch64.so.1 libbrlapi.so.0.6 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-brlapi = "brlapi glibc platform-python python3-libs"
RPM_SONAME_REQ_tcl-brlapi = "ld-linux-aarch64.so.1 libbrlapi.so.0.6 libc.so.6 libtcl8.6.so"
RDEPENDS_tcl-brlapi = "brlapi glibc tcl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/brlapi-0.6.7-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/brlapi-java-0.6.7-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/brltty-5.6-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/brltty-at-spi2-5.6-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/brltty-docs-5.6-28.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/brltty-dracut-5.6-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/brltty-espeak-ng-5.6-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/brltty-xw-5.6-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-brlapi-0.6.7-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tcl-brlapi-0.6.7-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/brlapi-devel-0.6.7-28.el8.aarch64.rpm \
          "

SRC_URI[brlapi.sha256sum] = "25eba55b4ca6e840c29042db2dbb1da82b9e1167e0a2ae7584e3e3645e56d403"
SRC_URI[brlapi-devel.sha256sum] = "d12801fc8898ee83fbef7067a75b380797a42634535873c0b536e227199626b2"
SRC_URI[brlapi-java.sha256sum] = "7d949019ef5c23be5a46bf6012a0a69949a765a4a2b17829352a220a78f81bd1"
SRC_URI[brltty.sha256sum] = "2477b6ef1bf49358d4eb7e66c2e25f03ee1749e0e64e9b6ffe7c9181d6cd9724"
SRC_URI[brltty-at-spi2.sha256sum] = "3c38b37a9e9a99e2bd40866261486824c8048b6250c058a4344a8820c77c031a"
SRC_URI[brltty-docs.sha256sum] = "3444eabb78ee71f992fde755b8a5d94c62844075723b755787976eecd753d535"
SRC_URI[brltty-dracut.sha256sum] = "300c9b02b05a603e2ba009a01103d54fed37b2594d8bb593c7a7da4d6f45b6fc"
SRC_URI[brltty-espeak-ng.sha256sum] = "7cc2856e1ebbc01aa5ccdd1e640457a17b9d1cbf540a9ab6211eef733d96dd17"
SRC_URI[brltty-xw.sha256sum] = "3eca71742cc20a69fcfe795d332ed518dba31a37de4ea4f722747f5c4e46504d"
SRC_URI[python3-brlapi.sha256sum] = "9c14b391a5e3f801bd829d5a3ea231554c02629fc6451302c2ff6df380f48f52"
SRC_URI[tcl-brlapi.sha256sum] = "b4a3c8e349cebb06d3772ef687251dadf75573a8db65b672f96162d639ae758d"
