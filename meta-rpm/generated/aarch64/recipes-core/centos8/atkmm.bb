SUMMARY = "generated recipe based on atkmm srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk glib-2.0 glibmm24 libgcc libsigc++20 pkgconfig-native"
RPM_SONAME_PROV_atkmm = "libatkmm-1.6.so.1"
RPM_SONAME_REQ_atkmm = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libglibmm-2.4.so.1 libgobject-2.0.so.0 libm.so.6 libsigc-2.0.so.0 libstdc++.so.6"
RDEPENDS_atkmm = "atk glib2 glibc glibmm24 libgcc libsigc++20 libstdc++"
RPM_SONAME_REQ_atkmm-devel = "libatkmm-1.6.so.1"
RPROVIDES_atkmm-devel = "atkmm-dev (= 2.24.2)"
RDEPENDS_atkmm-devel = "atk-devel atkmm glibmm24-devel pkgconf-pkg-config"
RDEPENDS_atkmm-doc = "atkmm glibmm24-doc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/atkmm-2.24.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/atkmm-devel-2.24.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/atkmm-doc-2.24.2-6.el8.noarch.rpm \
          "

SRC_URI[atkmm.sha256sum] = "812b2698bde10c0f5f70c0806c4951c364bba7bc773ad097671973924ca0ea76"
SRC_URI[atkmm-devel.sha256sum] = "0122dd61bb651078e0c85e6a81e6838e87dcd32c7a8c09070a0ee48b4ea9995f"
SRC_URI[atkmm-doc.sha256sum] = "b5d885e85fc899b5cf4ca1eddea143afeb6db471bbaf3360d68abcc9d4005437"
