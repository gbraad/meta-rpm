SUMMARY = "generated recipe based on iptables srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & Artistic-2.0 & ISC"
RPM_LICENSE = "GPLv2 and Artistic 2.0 and ISC"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libmnl libnetfilter-conntrack libnfnetlink libnftnl libpcap pkgconfig-native"
RPM_SONAME_PROV_iptables = "libarpt_mangle.so libebt_802_3.so libebt_among.so libebt_arp.so libebt_arpreply.so libebt_dnat.so libebt_ip.so libebt_ip6.so libebt_log.so libebt_mark.so libebt_mark_m.so libebt_nflog.so libebt_pkttype.so libebt_redirect.so libebt_snat.so libebt_stp.so libebt_vlan.so libip6t_DNAT.so libip6t_DNPT.so libip6t_HL.so libip6t_LOG.so libip6t_MASQUERADE.so libip6t_NETMAP.so libip6t_REDIRECT.so libip6t_REJECT.so libip6t_SNAT.so libip6t_SNPT.so libip6t_ah.so libip6t_dst.so libip6t_eui64.so libip6t_frag.so libip6t_hbh.so libip6t_hl.so libip6t_icmp6.so libip6t_ipv6header.so libip6t_mh.so libip6t_rt.so libip6t_srh.so libipt_CLUSTERIP.so libipt_DNAT.so libipt_ECN.so libipt_LOG.so libipt_MASQUERADE.so libipt_NETMAP.so libipt_REDIRECT.so libipt_REJECT.so libipt_SNAT.so libipt_TTL.so libipt_ULOG.so libipt_ah.so libipt_icmp.so libipt_realm.so libipt_ttl.so libxt_AUDIT.so libxt_CHECKSUM.so libxt_CLASSIFY.so libxt_CONNMARK.so libxt_CONNSECMARK.so libxt_CT.so libxt_DSCP.so libxt_HMARK.so libxt_IDLETIMER.so libxt_LED.so libxt_MARK.so libxt_NFLOG.so libxt_NFQUEUE.so libxt_RATEEST.so libxt_SECMARK.so libxt_SET.so libxt_SYNPROXY.so libxt_TCPMSS.so libxt_TCPOPTSTRIP.so libxt_TEE.so libxt_TOS.so libxt_TPROXY.so libxt_TRACE.so libxt_addrtype.so libxt_bpf.so libxt_cgroup.so libxt_cluster.so libxt_comment.so libxt_connbytes.so libxt_connlabel.so libxt_connlimit.so libxt_connmark.so libxt_conntrack.so libxt_cpu.so libxt_dccp.so libxt_devgroup.so libxt_dscp.so libxt_ecn.so libxt_esp.so libxt_hashlimit.so libxt_helper.so libxt_ipcomp.so libxt_iprange.so libxt_ipvs.so libxt_length.so libxt_limit.so libxt_mac.so libxt_mark.so libxt_multiport.so libxt_nfacct.so libxt_osf.so libxt_owner.so libxt_physdev.so libxt_pkttype.so libxt_policy.so libxt_quota.so libxt_rateest.so libxt_recent.so libxt_rpfilter.so libxt_sctp.so libxt_set.so libxt_socket.so libxt_standard.so libxt_statistic.so libxt_string.so libxt_tcp.so libxt_tcpmss.so libxt_time.so libxt_tos.so libxt_u32.so libxt_udp.so"
RPM_SONAME_REQ_iptables = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libmnl.so.0 libnetfilter_conntrack.so.3 libnfnetlink.so.0 libnftnl.so.11 libpcap.so.1 libxtables.so.12"
RDEPENDS_iptables = "bash glibc iptables-libs libmnl libnetfilter_conntrack libnfnetlink libnftnl libpcap"
RDEPENDS_iptables-arptables = "bash iptables"
RPM_SONAME_REQ_iptables-devel = "libip4tc.so.2 libip6tc.so.2 libxtables.so.12"
RPROVIDES_iptables-devel = "iptables-dev (= 1.8.4)"
RDEPENDS_iptables-devel = "iptables iptables-libs pkgconf-pkg-config"
RDEPENDS_iptables-ebtables = "bash iptables"
RPM_SONAME_PROV_iptables-libs = "libip4tc.so.0 libip4tc.so.2 libip6tc.so.0 libip6tc.so.2 libiptc.so.0 libxtables.so.12"
RPM_SONAME_REQ_iptables-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpcap.so.1"
RDEPENDS_iptables-libs = "glibc libpcap"
RDEPENDS_iptables-services = "bash iptables systemd"
RPM_SONAME_REQ_iptables-utils = "ld-linux-aarch64.so.1 libc.so.6 libnfnetlink.so.0 libpcap.so.1"
RDEPENDS_iptables-utils = "glibc iptables libnfnetlink libpcap"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iptables-1.8.4-10.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iptables-arptables-1.8.4-10.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iptables-devel-1.8.4-10.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iptables-ebtables-1.8.4-10.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iptables-libs-1.8.4-10.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iptables-services-1.8.4-10.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iptables-utils-1.8.4-10.el8_2.1.aarch64.rpm \
          "

SRC_URI[iptables.sha256sum] = "086a81e5baeadef520b3ec9f0345a3cc75a8f390598452098fdeeee303be366a"
SRC_URI[iptables-arptables.sha256sum] = "c42fd5194ffecd32c183e57c9d9951dc9a8e7018db974e80fd5398793d6725c4"
SRC_URI[iptables-devel.sha256sum] = "0cc2cc57de2c0e64502383f6bed701e055a1500348402a29b095bc5e082e376b"
SRC_URI[iptables-ebtables.sha256sum] = "4dd31beb51daa767202fc8cd451565e1165af821c465489b560d49cbe2682529"
SRC_URI[iptables-libs.sha256sum] = "5bcaa67966390cdccc6aa1600e7ddff7f610917b8b3eff42c2f641b1bfdb5d19"
SRC_URI[iptables-services.sha256sum] = "8c4056c4fc20de4053ed928b9b90048fb5e00bc3a85c58e181c72a510575d27e"
SRC_URI[iptables-utils.sha256sum] = "9bb4cedb01b18fdbbf00d1ce42913112992ee81fa2ca42eb7ad7b59cc3b00151"
