SUMMARY = "generated recipe based on python3 srpm"
DESCRIPTION = "Description"
LICENSE = "Python-2.0"
RPM_LICENSE = "Python"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 expat gdbm json-c libffi libnsl2 libtirpc libxcrypt ncurses openssl pkgconfig-native readline sqlite3 xz zlib"
RPM_SONAME_REQ_platform-python = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libutil.so.1"
RDEPENDS_platform-python = "bash chkconfig glibc openssl-libs platform-python-setuptools python3-libs python3-pip-wheel python3-setuptools-wheel"
RPM_SONAME_REQ_platform-python-devel = "libpython3.6m.so.1.0"
RPROVIDES_platform-python-devel = "platform-python-dev (= 3.6.8)"
RDEPENDS_platform-python-devel = "bash pkgconf-pkg-config platform-python platform-python-setuptools python-rpm-macros python3-libs python3-rpm-generators python3-rpm-macros"
RPM_SONAME_PROV_python3-libs = "libpython3.6m.so.1.0 libpython3.so"
RPM_SONAME_REQ_python3-libs = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libexpat.so.1 libffi.so.6 libgdbm.so.6 libgdbm_compat.so.4 liblzma.so.5 libm.so.6 libncursesw.so.6 libnsl.so.2 libpanelw.so.6 libpthread.so.0 libreadline.so.7 libsqlite3.so.0 libssl.so.1.1 libtinfo.so.6 libtirpc.so.3 libutil.so.1 libz.so.1"
RPROVIDES_python3-libs = "libpython3 (= 3.6.8) python3-2to3 (= 3.6.8) python3-asyncio (= 3.6.8) python3-audio (= 3.6.8) python3-codecs (= 3.6.8) python3-compile (= 3.6.8) python3-compression (= 3.6.8) python3-core (= 3.6.8) python3-crypt (= 3.6.8) python3-ctypes (= 3.6.8) python3-curses (= 3.6.8) python3-datetime (= 3.6.8) python3-db (= 3.6.8) python3-debugger (= 3.6.8) python3-difflib (= 3.6.8) python3-distutils (= 3.6.8) python3-distutils-windows (= 3.6.8) python3-doc (= 3.6.8) python3-doctest (= 3.6.8) python3-email (= 3.6.8) python3-fcntl (= 3.6.8) python3-gdbm (= 3.6.8) python3-html (= 3.6.8) python3-image (= 3.6.8) python3-io (= 3.6.8) python3-json (= 3.6.8) python3-locale (= 3.6.8) python3-logging (= 3.6.8) python3-mailbox (= 3.6.8) python3-math (= 3.6.8) python3-mime (= 3.6.8) python3-misc (= 3.6.8) python3-mmap (= 3.6.8) python3-modules (= 3.6.8) python3-multiprocessing (= 3.6.8) python3-netclient (= 3.6.8) python3-netserver (= 3.6.8) python3-numbers (= 3.6.8) python3-pickle (= 3.6.8) python3-pkgutil (= 3.6.8) python3-plistlib (= 3.6.8) python3-pprint (= 3.6.8) python3-profile (= 3.6.8) python3-ptest (= 3.6.8) python3-pydoc (= 3.6.8) python3-resource (= 3.6.8) python3-shell (= 3.6.8) python3-smtpd (= 3.6.8) python3-sqlite3 (= 3.6.8) python3-stringold (= 3.6.8) python3-syslog (= 3.6.8) python3-terminal (= 3.6.8) python3-tests (= 3.6.8) python3-threading (= 3.6.8) python3-typing (= 3.6.8) python3-unittest (= 3.6.8) python3-unixadmin (= 3.6.8) python3-venv (= 3.6.8) python3-xml (= 3.6.8) python3-xmlrpc (= 3.6.8)"
RDEPENDS_python3-libs = "bash bzip2-libs chkconfig expat gdbm gdbm-libs glibc libffi libnsl2 libtirpc libxcrypt ncurses-libs openssl-libs platform-python python3-pip-wheel python3-setuptools-wheel readline sqlite-libs xz-libs zlib"
RPM_SONAME_REQ_python3-test = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-test = "glibc openssl-libs platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/platform-python-devel-3.6.8-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/platform-python-3.6.8-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-libs-3.6.8-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-test-3.6.8-23.el8.aarch64.rpm \
          "

SRC_URI[platform-python.sha256sum] = "36a6e19838bbca46a7990ffd59f47f61a6d7c76f87623189bc67a980ab87ed2f"
SRC_URI[platform-python-devel.sha256sum] = "e2c510115112c5f1c8f0168f1a696f6bab88074d04aaf9357d9fc768df760dbc"
SRC_URI[python3-libs.sha256sum] = "5852272ea37a5ea21ce867664dd410cb8e062f8bf4adc5ff4b901e9fcf4305de"
SRC_URI[python3-test.sha256sum] = "80d4846bed3c273a9eac4a56b371a146f01ef3494c988e53f39043777eb615d9"
