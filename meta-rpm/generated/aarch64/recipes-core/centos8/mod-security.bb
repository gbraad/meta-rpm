SUMMARY = "generated recipe based on mod_security srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "apr apr-util curl db expat libpcre libxml2 lua openldap pkgconfig-native xz yajl zlib"
RPM_SONAME_REQ_mod_security = "ld-linux-aarch64.so.1 libapr-1.so.0 libaprutil-1.so.0 libc.so.6 libcurl.so.4 libdb-5.3.so libdl.so.2 libexpat.so.1 liblber-2.4.so.2 libldap_r-2.4.so.2 liblua-5.3.so liblzma.so.5 libm.so.6 libpcre.so.1 libpthread.so.0 libxml2.so.2 libyajl.so.2 libz.so.1"
RDEPENDS_mod_security = "apr apr-util expat glibc httpd httpd-filesystem libcurl libdb libxml2 lua-libs openldap pcre xz-libs yajl zlib"
RPM_SONAME_REQ_mod_security-mlogc = "ld-linux-aarch64.so.1 libapr-1.so.0 libc.so.6 libcurl.so.4 libdl.so.2 libpcre.so.1 libpthread.so.0"
RDEPENDS_mod_security-mlogc = "apr glibc httpd-filesystem libcurl mod_security pcre perl-Digest-MD5 perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_security-2.9.2-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_security-mlogc-2.9.2-8.el8.aarch64.rpm \
          "

SRC_URI[mod_security.sha256sum] = "d587388cfaecd60465272c105d94a5f68cfde3f7eea7d1ee66eb1f6cc67e5ebf"
SRC_URI[mod_security-mlogc.sha256sum] = "5f60f5754355c5332f24b8b6d9781851e2533c8487ea41eb1b7d3d779187eb1a"
