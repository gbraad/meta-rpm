SUMMARY = "generated recipe based on maven-wagon srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-wagon = "java-1.8.0-openjdk-headless javapackages-filesystem maven-parent plexus-containers-component-metadata"
RDEPENDS_maven-wagon-file = "java-1.8.0-openjdk-headless javapackages-filesystem maven-wagon-provider-api plexus-utils"
RDEPENDS_maven-wagon-ftp = "apache-commons-io apache-commons-net java-1.8.0-openjdk-headless javapackages-filesystem maven-wagon-provider-api slf4j"
RDEPENDS_maven-wagon-http = "httpcomponents-client httpcomponents-core java-1.8.0-openjdk-headless javapackages-filesystem jcl-over-slf4j maven-wagon-http-shared maven-wagon-provider-api plexus-utils"
RDEPENDS_maven-wagon-http-lightweight = "apache-commons-io java-1.8.0-openjdk-headless javapackages-filesystem maven-wagon-http-shared maven-wagon-provider-api plexus-utils"
RDEPENDS_maven-wagon-http-shared = "apache-commons-io httpcomponents-client httpcomponents-core java-1.8.0-openjdk-headless javapackages-filesystem jsoup maven-wagon-provider-api slf4j"
RDEPENDS_maven-wagon-javadoc = "javapackages-filesystem"
RDEPENDS_maven-wagon-provider-api = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-utils"
RDEPENDS_maven-wagon-providers = "java-1.8.0-openjdk-headless javapackages-filesystem maven-wagon maven-wagon-provider-api"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-wagon-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-wagon-file-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-wagon-ftp-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-wagon-http-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-wagon-http-lightweight-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-wagon-http-shared-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-wagon-javadoc-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-wagon-provider-api-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-wagon-providers-3.1.0-1.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-wagon.sha256sum] = "8a2e40a4e21fc982b3946353f4a73749d269863cc4503f492f96cdf304ade72d"
SRC_URI[maven-wagon-file.sha256sum] = "a3d4cbf2ce88fb648255bb3e8ded1546925ab289560b3a939d17b0c95ae3ed6c"
SRC_URI[maven-wagon-ftp.sha256sum] = "8387d720936b1c9bcc9cbf7eb2208c02efaca58c0f882d16223b300f239ee8fc"
SRC_URI[maven-wagon-http.sha256sum] = "a4a4921b1616dc77f737b399713842127032c33d3388e8f9a43a95459316553a"
SRC_URI[maven-wagon-http-lightweight.sha256sum] = "8f260f0a48f1a8c7e3907445e8b619fb59bf621f3363656bc39dc6d89e16c2a6"
SRC_URI[maven-wagon-http-shared.sha256sum] = "8e70bd19596e08deb48d151bf1a1325c1cd2dcbce422f55f625351d4032f7b63"
SRC_URI[maven-wagon-javadoc.sha256sum] = "9b7f8e810e1e538d58227a0a11001e97aa7bfe9050c980659ba59857f6025919"
SRC_URI[maven-wagon-provider-api.sha256sum] = "dade2cd0cb7857a2c61ff04015f631e1890c60d54f27976c1a8567e682802c54"
SRC_URI[maven-wagon-providers.sha256sum] = "a79f0c3b0e29bbd6e8389b78c50c52c9c1e28957aa2b15dfb9a374d98154352d"
