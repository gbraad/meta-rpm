SUMMARY = "generated recipe based on libverto srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libevent pkgconfig-native"
RPM_SONAME_PROV_libverto-libevent = "libverto-libevent.so.1"
RPM_SONAME_REQ_libverto-libevent = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libevent-2.1.so.6 libpthread.so.0 libverto.so.1"
RDEPENDS_libverto-libevent = "glibc libevent libverto"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libverto-libevent-0.3.0-5.el8.aarch64.rpm \
          "

SRC_URI[libverto-libevent.sha256sum] = "c32f372529ad09dea40f1262ceec38464f1bd8aa8acb2ba1358e0d1742743261"
