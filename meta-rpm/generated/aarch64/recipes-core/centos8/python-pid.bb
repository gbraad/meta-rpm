SUMMARY = "generated recipe based on python-pid srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pid = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pid-2.1.1-7.el8.noarch.rpm \
          "

SRC_URI[python3-pid.sha256sum] = "475cf09e782ff885b9a7dcfd660106871e5bcc3c31cbe02eb19aaecb1f27d1e1"
