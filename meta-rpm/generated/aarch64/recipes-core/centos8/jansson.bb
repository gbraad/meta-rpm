SUMMARY = "generated recipe based on jansson srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_jansson = "libjansson.so.4"
RPM_SONAME_REQ_jansson = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_jansson = "glibc"
RPM_SONAME_REQ_jansson-devel = "libjansson.so.4"
RPROVIDES_jansson-devel = "jansson-dev (= 2.11)"
RDEPENDS_jansson-devel = "jansson pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jansson-devel-2.11-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/jansson-2.11-3.el8.aarch64.rpm \
          "

SRC_URI[jansson.sha256sum] = "b8bd21e036c68bb8fbb9f21e6b5f6998fc3558f55a4b902d5d85664d5929134a"
SRC_URI[jansson-devel.sha256sum] = "718a57a2742964d3c27cec1b87235eb3dda7b37dea1c7b612f1c8c9f8c439f23"
