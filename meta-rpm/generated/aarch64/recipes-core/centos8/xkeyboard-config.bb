SUMMARY = "generated recipe based on xkeyboard-config srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_xkeyboard-config-devel = "xkeyboard-config-dev (= 2.28)"
RDEPENDS_xkeyboard-config-devel = "pkgconf-pkg-config xkeyboard-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xkeyboard-config-2.28-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xkeyboard-config-devel-2.28-1.el8.noarch.rpm \
          "

SRC_URI[xkeyboard-config.sha256sum] = "a2aeabb3962859069a78acc288bc3bffb35485428e162caafec8134f5ce6ca67"
SRC_URI[xkeyboard-config-devel.sha256sum] = "d4b8a544c1eceed61c79b36d5cd944feb34e0ec4714df47dedfb5da6343a8140"
