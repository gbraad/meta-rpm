SUMMARY = "generated recipe based on qt5-qtscript srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase"
RPM_SONAME_PROV_qt5-qtscript = "libQt5Script.so.5 libQt5ScriptTools.so.5"
RPM_SONAME_REQ_qt5-qtscript = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtscript = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_REQ_qt5-qtscript-devel = "libQt5Script.so.5 libQt5ScriptTools.so.5"
RPROVIDES_qt5-qtscript-devel = "qt5-qtscript-dev (= 5.12.5)"
RDEPENDS_qt5-qtscript-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtscript"
RPM_SONAME_REQ_qt5-qtscript-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Script.so.5 libQt5ScriptTools.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtscript-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtscript"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtscript-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtscript-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtscript-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtscript.sha256sum] = "caf76d955ea78dbd3e57f9f8ada04f41db57f85030d9159887b3e4f71603c2a1"
SRC_URI[qt5-qtscript-devel.sha256sum] = "ab63db21d0fd9a44d865c91620d14131752c6b50a388e197f2f83e44ce41b883"
SRC_URI[qt5-qtscript-examples.sha256sum] = "c1112d4d96e30c8412b7cd034de577035730ec4522724251540750fc13bed616"
