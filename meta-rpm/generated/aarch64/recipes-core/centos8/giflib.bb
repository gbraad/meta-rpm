SUMMARY = "generated recipe based on giflib srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_giflib = "libgif.so.7"
RPM_SONAME_REQ_giflib = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_giflib = "glibc"
RPM_SONAME_REQ_giflib-devel = "libgif.so.7"
RPROVIDES_giflib-devel = "giflib-dev (= 5.1.4)"
RDEPENDS_giflib-devel = "giflib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/giflib-5.1.4-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/giflib-devel-5.1.4-3.el8.aarch64.rpm \
          "

SRC_URI[giflib.sha256sum] = "8700e4ac6c522ee43c8fcbc41ffde490cd5590d05efcb24843bb49fbf1084f4e"
SRC_URI[giflib-devel.sha256sum] = "c86cf9e1dcc6e48840f009fccaf450d632f78eb117f6b9cba6e779ebcff4160f"
