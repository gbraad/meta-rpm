SUMMARY = "generated recipe based on perl-Filter-Simple srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Filter-Simple = "perl-Carp perl-Filter perl-Text-Balanced perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Filter-Simple-0.94-2.el8.noarch.rpm \
          "

SRC_URI[perl-Filter-Simple.sha256sum] = "3d0bda5a56dbf4a7daa92948b9f176bb779ccc82fc1edb85c1f1973016a4727e"
