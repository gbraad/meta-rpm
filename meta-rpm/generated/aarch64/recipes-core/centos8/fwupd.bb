SUMMARY = "generated recipe based on fwupd srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "efivar elfutils gcab glib-2.0 gnutls-libs gpgme json-glib libappstream-glib libarchive libgcc libgpg-error libgudev libgusb libsoup-2.4 libuuid pkgconfig-native polkit sqlite3"
RPM_SONAME_PROV_fwupd = "libfu_plugin_altos.so libfu_plugin_amt.so libfu_plugin_colorhug.so libfu_plugin_csr.so libfu_plugin_dell_dock.so libfu_plugin_dfu.so libfu_plugin_ebitdo.so libfu_plugin_flashrom.so libfu_plugin_nitrokey.so libfu_plugin_nvme.so libfu_plugin_rts54hid.so libfu_plugin_rts54hub.so libfu_plugin_steelseries.so libfu_plugin_superio.so libfu_plugin_test.so libfu_plugin_thunderbolt.so libfu_plugin_thunderbolt_power.so libfu_plugin_udev.so libfu_plugin_uefi.so libfu_plugin_unifying.so libfu_plugin_upower.so libfu_plugin_wacomhid.so libfwupd.so.2"
RPM_SONAME_REQ_fwupd = "ld-linux-aarch64.so.1 libappstream-glib.so.8 libarchive.so.13 libc.so.6 libefiboot.so.1 libefivar.so.1 libelf.so.1 libgcab-1.0.so.0 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgpg-error.so.0 libgpgme.so.11 libgudev-1.0.so.0 libgusb.so.2 libjson-glib-1.0.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libsoup-2.4.so.1 libsqlite3.so.0 libuuid.so.1"
RDEPENDS_fwupd = "bash bubblewrap efivar-libs elfutils-libelf glib2 glibc gnutls gpgme json-glib libappstream-glib libarchive libgcab1 libgcc libgpg-error libgudev libgusb libsoup libuuid platform-python polkit-libs sqlite-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fwupd-1.1.4-7.el8.aarch64.rpm \
          "

SRC_URI[fwupd.sha256sum] = "ff53ff5a71d1e39a9ea0a7fd729f1eee0f2eda7a84f04e58134b31bfb9e0e35e"
