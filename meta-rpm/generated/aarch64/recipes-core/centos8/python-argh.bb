SUMMARY = "generated recipe based on python-argh srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-argh = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-argh-0.26.1-8.el8.noarch.rpm \
          "

SRC_URI[python3-argh.sha256sum] = "3aeeafbdb7771f12d5823dc7e0bbb7400fd0c6527a2945afb31c25cbd6d1cd39"
