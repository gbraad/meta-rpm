SUMMARY = "generated recipe based on plexus-languages srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-languages = "java-1.8.0-openjdk-headless javapackages-filesystem objectweb-asm qdox"
RDEPENDS_plexus-languages-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-languages-0.9.10-3.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-languages-javadoc-0.9.10-3.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-languages.sha256sum] = "8b8f309683afaaebde72790a877684253f2142d56334e3b823fc7e54d32c78ac"
SRC_URI[plexus-languages-javadoc.sha256sum] = "cc83d9b716f22c95021de8765be03cec32aba127258c4d862a7bbbe369053492"
