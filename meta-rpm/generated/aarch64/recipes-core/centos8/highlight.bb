SUMMARY = "generated recipe based on highlight srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc lua pkgconfig-native"
RPM_SONAME_REQ_highlight = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 liblua-5.3.so libm.so.6 libstdc++.so.6"
RDEPENDS_highlight = "glibc libgcc libstdc++ lua-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/highlight-3.42-3.el8.aarch64.rpm \
          "

SRC_URI[highlight.sha256sum] = "105e3c0cdecca54681691ff2da83e85690b7b211329ab2b7a0ba302baaff5c54"
