SUMMARY = "generated recipe based on ocaml-ocamlbuild srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+ with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_ocaml-ocamlbuild = "libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_ocaml-ocamlbuild = "glibc ocaml-runtime"
RPROVIDES_ocaml-ocamlbuild-devel = "ocaml-ocamlbuild-dev (= 0.12.0)"
RDEPENDS_ocaml-ocamlbuild-devel = "ocaml-ocamlbuild"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-ocamlbuild-0.12.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ocaml-ocamlbuild-devel-0.12.0-6.el8.aarch64.rpm \
          "

SRC_URI[ocaml-ocamlbuild.sha256sum] = "0acdce3666c5f31283060a37127b3896a40a068d86dc1d967a9809f92604c88c"
SRC_URI[ocaml-ocamlbuild-devel.sha256sum] = "af58e44ca57e7195c47bcd57cfffbf0f2125ff5253212d2dcbfe8618f1a4ef4b"
