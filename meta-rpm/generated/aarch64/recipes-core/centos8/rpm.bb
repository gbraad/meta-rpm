SUMMARY = "generated recipe based on rpm srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
DEPENDS = "acl audit-libs bzip2 db elfutils file ima-evm-utils libarchive libcap lua openssl pkgconfig-native platform-python3 popt xz zlib zstd"
RPM_SONAME_REQ_python3-rpm = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libdw.so.1 libelf.so.1 libimaevm.so.0 liblua-5.3.so liblzma.so.5 libm.so.6 libmagic.so.1 libpopt.so.0 libpthread.so.0 libpython3.6m.so.1.0 librpm.so.8 librpmbuild.so.8 librpmio.so.8 librpmsign.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_python3-rpm = "audit-libs bzip2-libs elfutils-libelf elfutils-libs file-libs glibc ima-evm-utils libacl libcap libdb libzstd lua-libs openssl-libs platform-python popt python3-libs rpm-build-libs rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm = "ld-linux-aarch64.so.1 libacl.so.1 libarchive.so.13 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm = "audit-libs bash bzip2-libs coreutils curl elfutils-libelf glibc libacl libarchive libcap libdb libdb-utils libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_PROV_rpm-build-libs = "librpmbuild.so.8 librpmsign.so.8"
RPM_SONAME_REQ_rpm-build-libs = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libdw.so.1 libelf.so.1 libimaevm.so.0 liblua-5.3.so liblzma.so.5 libm.so.6 libmagic.so.1 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-build-libs = "audit-libs bzip2-libs elfutils-libelf elfutils-libs file-libs glibc gnupg2 ima-evm-utils libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RDEPENDS_rpm-cron = "bash crontabs logrotate rpm"
RPM_SONAME_PROV_rpm-libs = "librpm.so.8 librpmio.so.8"
RPM_SONAME_REQ_rpm-libs = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-libs = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm xz-libs zlib"
RPM_SONAME_REQ_rpm-sign = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 libimaevm.so.0 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 librpmsign.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-sign = "audit-libs bzip2-libs elfutils-libelf glibc ima-evm-utils libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-build-libs rpm-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-rpm-4.14.2-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-4.14.2-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-apidocs-4.14.2-37.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-build-libs-4.14.2-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-cron-4.14.2-37.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-libs-4.14.2-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-sign-4.14.2-37.el8.aarch64.rpm \
          "

SRC_URI[python3-rpm.sha256sum] = "e5097ca3ebf36672ef8564533a3bc58cda017e7a9f2bd0ab7f11aea20b02536b"
SRC_URI[rpm.sha256sum] = "82031bf6997df8aa47d37fade9059bf15b6231d648f4736e0adf48a98b3b00b2"
SRC_URI[rpm-apidocs.sha256sum] = "f36fd2c3094c35fe1f75e2991d33e598991810aea9135d60e32b264c17b34739"
SRC_URI[rpm-build-libs.sha256sum] = "cec1c79822eef26072ba43b358f3cb5012b0fbe146265eed4f107e6f2967b116"
SRC_URI[rpm-cron.sha256sum] = "601f9565125f201503dbba97accc4215c157b29ecf8087124b02a7b1e4cf073d"
SRC_URI[rpm-libs.sha256sum] = "8d04c1710eae2652511f1f762e5f555da0fbb7a7c7795e43d42cf32810f2e67d"
SRC_URI[rpm-sign.sha256sum] = "dc48ce48eaca56f68aa2a2c9097055a722af9ddd2764cdf714cbbea8f5b54fb9"
