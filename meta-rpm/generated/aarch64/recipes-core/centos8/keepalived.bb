SUMMARY = "generated recipe based on keepalived srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libnl net-snmp openssl pkgconfig-native"
RPM_SONAME_REQ_keepalived = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libm.so.6 libnetsnmp.so.35 libnetsnmpagent.so.35 libnetsnmpmibs.so.35 libnl-3.so.200 libnl-genl-3.so.200 libssl.so.1.1"
RDEPENDS_keepalived = "bash glibc libnl3 net-snmp-agent-libs net-snmp-libs openssl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/keepalived-2.0.10-10.el8.aarch64.rpm \
          "

SRC_URI[keepalived.sha256sum] = "d65626e0632d120acc35b1db05a0d53f1f10275e64278e1cad5cc068a9ebfafc"
