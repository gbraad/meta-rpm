SUMMARY = "generated recipe based on pyserial srpm"
DESCRIPTION = "Description"
LICENSE = "Python-2.0"
RPM_LICENSE = "Python"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pyserial = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-pyserial-3.1.1-8.el8.noarch.rpm \
          "

SRC_URI[python3-pyserial.sha256sum] = "82ce159a9e4e4b6b4fdce9ad954aa507f67108430bd261a4dcd826e44d0152a4"
