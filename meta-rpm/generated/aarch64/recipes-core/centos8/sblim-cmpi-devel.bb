SUMMARY = "generated recipe based on sblim-cmpi-devel srpm"
DESCRIPTION = "Description"
LICENSE = "EPL"
RPM_LICENSE = "EPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libcmpiCppImpl0 = "libcmpiCppImpl.so.0"
RPM_SONAME_REQ_libcmpiCppImpl0 = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libcmpiCppImpl0 = "glibc libgcc libstdc++"
RPROVIDES_sblim-cmpi-devel = "sblim-cmpi-dev (= 2.0.3)"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libcmpiCppImpl0-2.0.3-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sblim-cmpi-devel-2.0.3-15.el8.aarch64.rpm \
          "

SRC_URI[libcmpiCppImpl0.sha256sum] = "3b0b845bf36b069fe9d261cfa729f24593d1f147501b68b820aa2deb7a31eced"
SRC_URI[sblim-cmpi-devel.sha256sum] = "18ad05437dd09857098314d9da210a218067339d16676f09131824062f7bc460"
