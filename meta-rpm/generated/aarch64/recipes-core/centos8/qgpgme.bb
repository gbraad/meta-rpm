SUMMARY = "generated recipe based on gpgme srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libassuan libgcc libgpg-error pkgconfig-native qt5-qtbase"
RPM_SONAME_PROV_qgpgme = "libqgpgme.so.7"
RPM_SONAME_REQ_qgpgme = "ld-linux-aarch64.so.1 libQt5Core.so.5 libassuan.so.0 libc.so.6 libgcc_s.so.1 libgpg-error.so.0 libgpgme.so.11 libgpgmepp.so.6 libm.so.6 libstdc++.so.6"
RDEPENDS_qgpgme = "glibc gpgme gpgmepp libassuan libgcc libgpg-error libstdc++ qt5-qtbase"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qgpgme-1.10.0-6.el8.0.1.aarch64.rpm \
          "

SRC_URI[qgpgme.sha256sum] = "126dadfe6a42023dcdf2d5821e765c0302e8d261db6d8e1c69ead5134afda63d"
