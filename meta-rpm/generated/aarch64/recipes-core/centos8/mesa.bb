SUMMARY = "generated recipe based on mesa srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "libegl libgles3-mesa-dev mesa-dev mesa-megadriver virtual/egl virtual/libgbm virtual/libgl virtual/libgles2 virtual/mesa"
DEPENDS = "elfutils expat libdrm libgcc libglvnd libselinux libx11 libxcb libxdamage libxext libxfixes libxshmfence libxxf86vm llvm pkgconfig-native wayland zlib"
RPM_SONAME_PROV_mesa-dri-drivers = "libgallium_dri.so"
RPM_SONAME_REQ_mesa-dri-drivers = "ld-linux-aarch64.so.1 libLLVM-9.so libc.so.6 libdl.so.2 libdrm.so.2 libdrm_amdgpu.so.1 libdrm_nouveau.so.2 libdrm_radeon.so.1 libelf.so.1 libexpat.so.1 libgcc_s.so.1 libglapi.so.0 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_mesa-dri-drivers = "elfutils-libelf expat glibc libdrm libgcc libstdc++ llvm-libs mesa-filesystem mesa-libglapi zlib"
RPM_SONAME_PROV_mesa-libEGL = "libEGL_mesa.so.0"
RPM_SONAME_REQ_mesa-libEGL = "ld-linux-aarch64.so.1 libX11-xcb.so.1 libc.so.6 libdl.so.2 libdrm.so.2 libexpat.so.1 libgbm.so.1 libglapi.so.0 libm.so.6 libpthread.so.0 libwayland-client.so.0 libwayland-server.so.0 libxcb-dri2.so.0 libxcb-dri3.so.0 libxcb-present.so.0 libxcb-sync.so.1 libxcb-xfixes.so.0 libxcb.so.1 libxshmfence.so.1"
RPROVIDES_mesa-libEGL = "libegl (= 19.3.4)"
RDEPENDS_mesa-libEGL = "expat glibc libX11-xcb libdrm libglvnd-egl libwayland-client libwayland-server libxcb libxshmfence mesa-libgbm mesa-libglapi"
RPROVIDES_mesa-libEGL-devel = "mesa-libEGL-dev (= 19.3.4)"
RDEPENDS_mesa-libEGL-devel = "libglvnd-devel mesa-libEGL"
RPM_SONAME_PROV_mesa-libGL = "libGLX_mesa.so.0"
RPM_SONAME_REQ_mesa-libGL = "ld-linux-aarch64.so.1 libX11-xcb.so.1 libX11.so.6 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXxf86vm.so.1 libc.so.6 libdl.so.2 libdrm.so.2 libexpat.so.1 libglapi.so.0 libm.so.6 libpthread.so.0 libxcb-dri2.so.0 libxcb-dri3.so.0 libxcb-glx.so.0 libxcb-present.so.0 libxcb-sync.so.1 libxcb.so.1 libxshmfence.so.1"
RDEPENDS_mesa-libGL = "expat glibc libX11 libX11-xcb libXdamage libXext libXfixes libXxf86vm libdrm libglvnd-glx libxcb libxshmfence mesa-libglapi"
RPM_SONAME_REQ_mesa-libGL-devel = "libglapi.so.0"
RPROVIDES_mesa-libGL-devel = "mesa-libGL-dev (= 19.3.4)"
RDEPENDS_mesa-libGL-devel = "libdrm-devel libglvnd-devel mesa-libGL mesa-libglapi pkgconf-pkg-config"
RPM_SONAME_PROV_mesa-libOSMesa = "libOSMesa.so.8"
RPM_SONAME_REQ_mesa-libOSMesa = "ld-linux-aarch64.so.1 libLLVM-9.so libc.so.6 libgcc_s.so.1 libglapi.so.0 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_mesa-libOSMesa = "glibc libgcc libstdc++ llvm-libs mesa-libglapi zlib"
RPM_SONAME_REQ_mesa-libOSMesa-devel = "libOSMesa.so.8"
RPROVIDES_mesa-libOSMesa-devel = "mesa-libOSMesa-dev (= 19.3.4)"
RDEPENDS_mesa-libOSMesa-devel = "mesa-libOSMesa pkgconf-pkg-config"
RPM_SONAME_PROV_mesa-libgbm = "libgbm.so.1"
RPM_SONAME_REQ_mesa-libgbm = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libdrm.so.2 libexpat.so.1 libm.so.6 libpthread.so.0 libwayland-server.so.0"
RDEPENDS_mesa-libgbm = "expat glibc libdrm libwayland-server"
RPM_SONAME_REQ_mesa-libgbm-devel = "libgbm.so.1"
RPROVIDES_mesa-libgbm-devel = "mesa-libgbm-dev (= 19.3.4)"
RDEPENDS_mesa-libgbm-devel = "mesa-libgbm pkgconf-pkg-config"
RPM_SONAME_PROV_mesa-libglapi = "libglapi.so.0"
RPM_SONAME_REQ_mesa-libglapi = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libselinux.so.1"
RDEPENDS_mesa-libglapi = "glibc libselinux"
RPM_SONAME_PROV_mesa-libxatracker = "libxatracker.so.2"
RPM_SONAME_REQ_mesa-libxatracker = "ld-linux-aarch64.so.1 libLLVM-9.so libc.so.6 libdl.so.2 libdrm.so.2 libdrm_nouveau.so.2 libexpat.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_mesa-libxatracker = "expat glibc libdrm libgcc libstdc++ llvm-libs zlib"
RPM_SONAME_PROV_mesa-vdpau-drivers = "libvdpau_gallium.so.1.0.0"
RPM_SONAME_REQ_mesa-vdpau-drivers = "ld-linux-aarch64.so.1 libLLVM-9.so libX11-xcb.so.1 libc.so.6 libdl.so.2 libdrm.so.2 libdrm_amdgpu.so.1 libdrm_nouveau.so.2 libdrm_radeon.so.1 libelf.so.1 libexpat.so.1 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libxcb-dri2.so.0 libxcb-dri3.so.0 libxcb-present.so.0 libxcb-sync.so.1 libxcb-xfixes.so.0 libxcb.so.1 libxshmfence.so.1 libz.so.1"
RDEPENDS_mesa-vdpau-drivers = "elfutils-libelf expat glibc libX11-xcb libdrm libgcc libstdc++ libxcb libxshmfence llvm-libs mesa-filesystem zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-dri-drivers-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-filesystem-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libEGL-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libEGL-devel-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libGL-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libGL-devel-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libOSMesa-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libgbm-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libglapi-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-libxatracker-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mesa-vdpau-drivers-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mesa-libOSMesa-devel-19.3.4-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mesa-libgbm-devel-19.3.4-2.el8.aarch64.rpm \
          "

SRC_URI[mesa-dri-drivers.sha256sum] = "36c7dfe48609f618302f62031fd1beb393eaf3e09c26248643851d24f1090685"
SRC_URI[mesa-filesystem.sha256sum] = "b5e6006352e17fafab9bef06cc47f020304c2951a86c84efe3d502eb348fb45d"
SRC_URI[mesa-libEGL.sha256sum] = "b4707484a07ba873b32df1a1c46599376e56d2b3bffd19cd005ed52abc7cf6e3"
SRC_URI[mesa-libEGL-devel.sha256sum] = "a7aa9ec1b5297b9f02fde9fe4cdc1781e1cb290122b8d1c586763970164c3c66"
SRC_URI[mesa-libGL.sha256sum] = "78454d813c530924a99cca59c2388b32ab2d1b12300f44658d772f1603843438"
SRC_URI[mesa-libGL-devel.sha256sum] = "e86c991037db8dc278eca0f8b30991bff4d3db46005948b421cc538aa132ef71"
SRC_URI[mesa-libOSMesa.sha256sum] = "75ef04a5f1b300b8eecccfe5b3173cc0f488e4c278056f819247cba9eece2201"
SRC_URI[mesa-libOSMesa-devel.sha256sum] = "795fd2df3df7332bfe94e0b20a75e321cfcaf14961743d7285feac8aae18550b"
SRC_URI[mesa-libgbm.sha256sum] = "5857573dc480c5b0844d81846ac4b6e29d30555e068de34595ff50958b303bf6"
SRC_URI[mesa-libgbm-devel.sha256sum] = "9ff335d874712c75b507d3915f2047ff62ff3020e5f8dff47cd3a25b2238f5d6"
SRC_URI[mesa-libglapi.sha256sum] = "ceca5b7a789153120301196c2f2e2b0c9b0ff7cf6c809c2ce2a8fc990ac40ad0"
SRC_URI[mesa-libxatracker.sha256sum] = "c8d61a3a7bc8e7fdadca7aa1134849d4fc9484f4548bf46ba064f5e786f25be9"
SRC_URI[mesa-vdpau-drivers.sha256sum] = "4558d62e5a18de84c2df387b6ee329b8c16f6777aaa14a3325f9bb0c0c655539"
