SUMMARY = "generated recipe based on jline srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jline = "jansi java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jline-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jline-2.14.6-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jline-javadoc-2.14.6-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jline.sha256sum] = "7ddb6191f7c35197bfcc2adb47d7b4a73a3f51dbaf71e31dc6b3ca4ac5a7f6cc"
SRC_URI[jline-javadoc.sha256sum] = "a674a956bad495372b4c168bcef405afb5a9ae1b9e8ece238bce01a420708efc"
