SUMMARY = "generated recipe based on python-requests-file srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-requests-file = "platform-python python3-requests python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-requests-file-1.4.3-5.el8.noarch.rpm \
          "

SRC_URI[python3-requests-file.sha256sum] = "a344d36cc3077db9b22452308595455f02d192b30e14a4ed7e7d827e1d6a84aa"
