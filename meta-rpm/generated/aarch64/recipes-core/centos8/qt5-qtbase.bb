SUMMARY = "generated recipe based on qt5-qtbase srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cups-libs dbus-libs fontconfig freetype glib-2.0 harfbuzz icu libdrm libgcc libglvnd libice libinput libjpeg-turbo libpcre2 libpng libpq libsm libx11 libxcb libxext libxkbcommon libxrender mariadb-connector-c mesa openssl pkgconfig-native sqlite3 systemd-libs unixodbc xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm zlib"
RPM_SONAME_PROV_qt5-qtbase = "libQt5Concurrent.so.5 libQt5Core.so.5 libQt5DBus.so.5 libQt5Network.so.5 libQt5Sql.so.5 libQt5Test.so.5 libQt5Xml.so.5"
RPM_SONAME_REQ_qt5-qtbase = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdbus-1.so.3 libdl.so.2 libgcc_s.so.1 libglib-2.0.so.0 libgthread-2.0.so.0 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libm.so.6 libpcre2-16.so.0 libpthread.so.0 libsqlite3.so.0 libssl.so.1.1 libstdc++.so.6 libsystemd.so.0 libz.so.1"
RDEPENDS_qt5-qtbase = "bash chkconfig dbus-libs glib2 glibc libgcc libicu libstdc++ openssl-libs pcre2-utf16 qt5-qtbase-common sqlite-libs systemd-libs zlib"
RDEPENDS_qt5-qtbase-common = "qt5-qtbase"
RPM_SONAME_REQ_qt5-qtbase-devel = "ld-linux-aarch64.so.1 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5DBus.so.5 libQt5EglFSDeviceIntegration.so.5 libQt5EglFsKmsSupport.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5PrintSupport.so.5 libQt5Sql.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libQt5XcbQpa.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libz.so.1"
RPROVIDES_qt5-qtbase-devel = "qt5-qtbase-dev (= 5.12.5)"
RDEPENDS_qt5-qtbase-devel = "bash cmake-filesystem glibc libgcc libglvnd-devel libstdc++ perl-File-Path perl-Getopt-Long perl-IO perl-PathTools perl-interpreter perl-libs pkgconf-pkg-config platform-python qt5-qtbase qt5-qtbase-gui qt5-rpm-macros zlib"
RPM_SONAME_PROV_qt5-qtbase-examples = "libechoplugin.so libpnp_extrafilters.so libsimplestyleplugin.so"
RPM_SONAME_REQ_qt5-qtbase-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Concurrent.so.5 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5PrintSupport.so.5 libQt5Sql.so.5 libQt5Test.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtbase-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui"
RPM_SONAME_PROV_qt5-qtbase-gui = "libQt5EglFSDeviceIntegration.so.5 libQt5EglFsKmsSupport.so.5 libQt5Gui.so.5 libQt5OpenGL.so.5 libQt5PrintSupport.so.5 libQt5Widgets.so.5 libQt5XcbQpa.so.5"
RPM_SONAME_REQ_qt5-qtbase-gui = "ld-linux-aarch64.so.1 libEGL.so.1 libGL.so.1 libICE.so.6 libQt5Core.so.5 libQt5DBus.so.5 libQt5Network.so.5 libSM.so.6 libX11-xcb.so.1 libX11.so.6 libXext.so.6 libXrender.so.1 libc.so.6 libcups.so.2 libdl.so.2 libdrm.so.2 libfontconfig.so.1 libfreetype.so.6 libgbm.so.1 libgcc_s.so.1 libglib-2.0.so.0 libgthread-2.0.so.0 libharfbuzz.so.0 libjpeg.so.62 libm.so.6 libpng16.so.16 libpthread.so.0 libstdc++.so.6 libudev.so.1 libxcb-glx.so.0 libxcb-icccm.so.4 libxcb-image.so.0 libxcb-keysyms.so.1 libxcb-randr.so.0 libxcb-render-util.so.0 libxcb-render.so.0 libxcb-shape.so.0 libxcb-shm.so.0 libxcb-sync.so.1 libxcb-xfixes.so.0 libxcb-xinerama.so.0 libxcb-xinput.so.0 libxcb.so.1 libxkbcommon.so.0 libz.so.1"
RDEPENDS_qt5-qtbase-gui = "bash cups-libs fontconfig freetype glib2 glibc glx-utils harfbuzz libICE libSM libX11 libX11-xcb libXext libXrender libdrm libgcc libglvnd-egl libglvnd-glx libjpeg-turbo libpng libstdc++ libxcb libxkbcommon mesa-libgbm qt5-qtbase systemd-libs xcb-util-image xcb-util-keysyms xcb-util-renderutil xcb-util-wm zlib"
RPM_SONAME_REQ_qt5-qtbase-mysql = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Sql.so.5 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libm.so.6 libmariadb.so.3 libpthread.so.0 libssl.so.1.1 libstdc++.so.6 libz.so.1"
RDEPENDS_qt5-qtbase-mysql = "glibc libgcc libstdc++ mariadb-connector-c openssl-libs qt5-qtbase zlib"
RPM_SONAME_REQ_qt5-qtbase-odbc = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Sql.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libodbc.so.2 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtbase-odbc = "glibc libgcc libstdc++ qt5-qtbase unixODBC"
RPM_SONAME_REQ_qt5-qtbase-postgresql = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Sql.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpq.so.5 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtbase-postgresql = "glibc libgcc libpq libstdc++ qt5-qtbase"
RPROVIDES_qt5-qtbase-private-devel = "qt5-qtbase-private-dev (= 5.12.5)"
RDEPENDS_qt5-qtbase-private-devel = "cups-devel qt5-qtbase-devel"
RDEPENDS_qt5-qtbase-static = "cmake-filesystem fontconfig-devel glib2-devel libinput-devel libxkbcommon-devel pkgconf-pkg-config qt5-qtbase-devel zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtbase-5.12.5-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtbase-common-5.12.5-4.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtbase-devel-5.12.5-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtbase-examples-5.12.5-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtbase-gui-5.12.5-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtbase-mysql-5.12.5-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtbase-odbc-5.12.5-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtbase-postgresql-5.12.5-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtbase-private-devel-5.12.5-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/qt5-qtbase-static-5.12.5-4.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtbase.sha256sum] = "ea8dc63c69f15c8c74ab5df49259accb911a0bcecf55ebad2fd2cf3b09305bd7"
SRC_URI[qt5-qtbase-common.sha256sum] = "50cfe2978887454e23fdf0fdb296c13eb9792d5bc48ebe664a5b74e7c9f32c3e"
SRC_URI[qt5-qtbase-devel.sha256sum] = "42e6c8dbd11182ba85b88848d902c6a2b0d1a9fd300a1d8c72f45f3f544173cf"
SRC_URI[qt5-qtbase-examples.sha256sum] = "09d5e952ebd32da534acc7ef286e41e0f0945e1d416a51fd6c350cfa38c42159"
SRC_URI[qt5-qtbase-gui.sha256sum] = "889c3b64dbccb8eccf9133cdee62d89c3615ba9c78af95913548b8728140ca8c"
SRC_URI[qt5-qtbase-mysql.sha256sum] = "4a382421e80f0a3b96212484066237f4842c5e6f24106cb894ba699d13797885"
SRC_URI[qt5-qtbase-odbc.sha256sum] = "b527578248a1f0800c60b26621604e4ed748b20d8a240cfd4c973da7f45ab223"
SRC_URI[qt5-qtbase-postgresql.sha256sum] = "c872201fc5df83f9959c5771b024a9b97e41009807e44c91ddeafe36e5fbe6c8"
SRC_URI[qt5-qtbase-private-devel.sha256sum] = "642517654fe4442bd7c62524add46c777e8b63cb4c680cb2e228acc67cab05d2"
SRC_URI[qt5-qtbase-static.sha256sum] = "4a964b7db07d59d3507bfb0b253f7d5144bb7ba0a1d426951979384264585e2f"
