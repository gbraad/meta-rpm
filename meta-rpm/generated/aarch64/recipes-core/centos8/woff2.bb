SUMMARY = "generated recipe based on woff2 srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "brotli libgcc pkgconfig-native"
RPM_SONAME_PROV_woff2 = "libwoff2common.so.1.0.2 libwoff2dec.so.1.0.2 libwoff2enc.so.1.0.2"
RPM_SONAME_REQ_woff2 = "ld-linux-aarch64.so.1 libbrotlidec.so.1 libbrotlienc.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_woff2 = "brotli glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/woff2-1.0.2-4.el8.aarch64.rpm \
          "

SRC_URI[woff2.sha256sum] = "94f017ed6ea700321669622feb7e540a010cea88a2840c4ffe4e44574a720967"
