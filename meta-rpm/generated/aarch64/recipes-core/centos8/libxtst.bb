SUMMARY = "generated recipe based on libXtst srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "xtst"
DEPENDS = "libx11 libxext libxi pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXtst = "libXtst.so.6"
RPM_SONAME_REQ_libXtst = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXi.so.6 libc.so.6"
RDEPENDS_libXtst = "glibc libX11 libXext libXi"
RPM_SONAME_REQ_libXtst-devel = "libXtst.so.6"
RPROVIDES_libXtst-devel = "libXtst-dev (= 1.2.3)"
RDEPENDS_libXtst-devel = "libX11-devel libXext-devel libXi-devel libXtst pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXtst-1.2.3-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXtst-devel-1.2.3-7.el8.aarch64.rpm \
          "

SRC_URI[libXtst.sha256sum] = "fcc94e8d6c48b1f6b7c41663e3de7c63027f0207742fa4ffa962cd75ddd1bf6f"
SRC_URI[libXtst-devel.sha256sum] = "6ebad329b05d0925776b5622e3f6f511628d16d27259f3ada5d7a7b16e968bba"
