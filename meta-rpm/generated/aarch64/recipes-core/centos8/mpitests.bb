SUMMARY = "generated recipe based on mpitests srpm"
DESCRIPTION = "Description"
LICENSE = "CPL-1.0 & BSD"
RPM_LICENSE = "CPL and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc mpich mvapich2 openmpi pkgconfig-native"
RPM_SONAME_REQ_mpitests-mpich = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 libstdc++.so.6"
RDEPENDS_mpitests-mpich = "glibc libgcc libstdc++ mpich"
RPM_SONAME_REQ_mpitests-mvapich2 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.12 libmpicxx.so.12 libpthread.so.0 libstdc++.so.6"
RDEPENDS_mpitests-mvapich2 = "glibc libgcc libstdc++ mvapich2"
RPM_SONAME_REQ_mpitests-openmpi = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libmpi.so.40 libmpi_cxx.so.40 libpthread.so.0 libstdc++.so.6"
RDEPENDS_mpitests-openmpi = "glibc libgcc libstdc++ openmpi"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mpitests-mpich-5.4.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mpitests-mvapich2-5.4.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mpitests-openmpi-5.4.2-4.el8.aarch64.rpm \
          "

SRC_URI[mpitests-mpich.sha256sum] = "42eb091279d8480edc16a11562185a39821cff9fa7a325067ff7f0e9689c7e9b"
SRC_URI[mpitests-mvapich2.sha256sum] = "567424582343cab699d051fce0ddde1a3b28debd07cd66e40cc5791e4943582e"
SRC_URI[mpitests-openmpi.sha256sum] = "07938e55018265864d1d150546385499cec683b483049ad9bad3c2289af668d4"
