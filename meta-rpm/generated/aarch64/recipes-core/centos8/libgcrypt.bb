SUMMARY = "generated recipe based on libgcrypt srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "LGPLv2+ and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgpg-error pkgconfig-native"
RPM_SONAME_PROV_libgcrypt = "libgcrypt.so.20"
RPM_SONAME_REQ_libgcrypt = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgpg-error.so.0"
RDEPENDS_libgcrypt = "glibc libgpg-error"
RPM_SONAME_REQ_libgcrypt-devel = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0"
RPROVIDES_libgcrypt-devel = "libgcrypt-dev (= 1.8.3)"
RDEPENDS_libgcrypt-devel = "bash glibc info libgcrypt libgpg-error libgpg-error-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgcrypt-1.8.3-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libgcrypt-devel-1.8.3-4.el8.aarch64.rpm \
          "

SRC_URI[libgcrypt.sha256sum] = "349fb3fe51236f9c0608cc94c035853292fed724bcc4a37f7f7edd3523e1099d"
SRC_URI[libgcrypt-devel.sha256sum] = "71a2b752a5f91a14fa2b3436b2c4afeeccf6b97aa0426c1081b37b0260064e52"
