SUMMARY = "generated recipe based on perl-ExtUtils-CBuilder srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-ExtUtils-CBuilder = "perl-ExtUtils-MakeMaker perl-File-Path perl-File-Temp perl-IO perl-IPC-Cmd perl-PathTools perl-Perl-OSType perl-Text-ParseWords perl-devel perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-ExtUtils-CBuilder-0.280230-2.el8.noarch.rpm \
          "

SRC_URI[perl-ExtUtils-CBuilder.sha256sum] = "018f7a5093eb4f9c55b919696dc631098eb0e0a66d1c6b74f8f732e41bad766c"
