SUMMARY = "generated recipe based on pinfo srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_pinfo = "ld-linux-aarch64.so.1 libc.so.6 libncursesw.so.6 libtinfo.so.6"
RDEPENDS_pinfo = "bash glibc info ncurses-libs xdg-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/pinfo-0.6.10-18.el8.aarch64.rpm \
          "

SRC_URI[pinfo.sha256sum] = "52d542c49ded4c9aed644e3161417749928b67ae6a9b0813d52117db4053e542"
