SUMMARY = "generated recipe based on maven-invoker srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-invoker = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-invoker-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-invoker-2.2-7.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-invoker-javadoc-2.2-7.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-invoker.sha256sum] = "704a22dbd975956ed587688b97c65c2c2a58e3e46eb7ac6b474b06213ba33be3"
SRC_URI[maven-invoker-javadoc.sha256sum] = "f15e5c8b1dc048bc8a72880766674cfd291be6011d71781e5ccba042ab3b9dbe"
