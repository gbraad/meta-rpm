SUMMARY = "generated recipe based on rrdtool srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+ with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo glib-2.0 libpng libxml2 lua pango perl pkgconfig-native platform-python3 ruby"
RPM_SONAME_REQ_python3-rrdtool = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 librrd.so.8"
RDEPENDS_python3-rrdtool = "glibc platform-python python3-libs rrdtool"
RPM_SONAME_PROV_rrdtool = "librrd.so.8"
RPM_SONAME_REQ_rrdtool = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libpthread.so.0 libxml2.so.2"
RDEPENDS_rrdtool = "bash cairo dejavu-sans-mono-fonts glib2 glibc libpng libxml2 pango systemd"
RPM_SONAME_REQ_rrdtool-devel = "librrd.so.8"
RPROVIDES_rrdtool-devel = "rrdtool-dev (= 1.7.0)"
RDEPENDS_rrdtool-devel = "pkgconf-pkg-config rrdtool"
RPM_SONAME_REQ_rrdtool-lua = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libglib-2.0.so.0 libgobject-2.0.so.0 liblua-5.3.so libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpng16.so.16 libpthread.so.0 librrd.so.8 libxml2.so.2"
RDEPENDS_rrdtool-lua = "cairo glib2 glibc libpng libxml2 lua-libs pango rrdtool"
RPM_SONAME_REQ_rrdtool-perl = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0 librrd.so.8"
RDEPENDS_rrdtool-perl = "glibc perl-Carp perl-IO perl-interpreter perl-libs rrdtool"
RPM_SONAME_REQ_rrdtool-ruby = "ld-linux-aarch64.so.1 libc.so.6 libruby.so.2.5"
RDEPENDS_rrdtool-ruby = "glibc rrdtool ruby-libs"
RPM_SONAME_REQ_rrdtool-tcl = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 librrd.so.8"
RDEPENDS_rrdtool-tcl = "glibc rrdtool tcl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rrdtool-1.7.0-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rrdtool-perl-1.7.0-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-rrdtool-1.7.0-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rrdtool-devel-1.7.0-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rrdtool-doc-1.7.0-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rrdtool-lua-1.7.0-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rrdtool-ruby-1.7.0-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rrdtool-tcl-1.7.0-16.el8.aarch64.rpm \
          "

SRC_URI[python3-rrdtool.sha256sum] = "eff4314f7e48294df00c895089705cfba8ea49a3d4e7269252f7492ebc699b8a"
SRC_URI[rrdtool.sha256sum] = "5274bf1fec447d49472d6d9e08e1b3fa095a6abae9998c99b2cad18389b29ecf"
SRC_URI[rrdtool-devel.sha256sum] = "1115edd196a7c5a1b1ab417d562e4529a11052dd8a018ddf945daf714ccf6d04"
SRC_URI[rrdtool-doc.sha256sum] = "61785e2f5128427f3ecc05de193cfd945422e8e58a9b83aeb157e6b473075156"
SRC_URI[rrdtool-lua.sha256sum] = "088365ce42ff0991e38f391c72a824ad4b43a7e749c5452ed88e70040ff4cef7"
SRC_URI[rrdtool-perl.sha256sum] = "f739b2509e14559f7f8bf3b1d6bcfd747e7a47ebbe44da45d7c04957b1d65ccf"
SRC_URI[rrdtool-ruby.sha256sum] = "edede7e91521981105a8b5fff11b46559f56913132aca1d7520a357d9c175a60"
SRC_URI[rrdtool-tcl.sha256sum] = "eb1040188b1953259736108ff0a3a7528c1f93b9fd5316cf49ccc1b4f7265a04"
