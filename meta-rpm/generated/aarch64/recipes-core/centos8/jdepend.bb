SUMMARY = "generated recipe based on jdepend srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jdepend = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jdepend-demo = "jdepend"
RDEPENDS_jdepend-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jdepend-2.9.1-18.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jdepend-demo-2.9.1-18.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jdepend-javadoc-2.9.1-18.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jdepend.sha256sum] = "f80da98d1747cf98d5c5a5850e9f91bb51ea190a5285bbf165cfd4583262fce6"
SRC_URI[jdepend-demo.sha256sum] = "a5f0e8c015743943862393e01035d3b346b3528094d8dc3bbef759d87fef7271"
SRC_URI[jdepend-javadoc.sha256sum] = "39db9e67833b366455c7e2a6f9189b8d2be4288f633fa34eb0bbcd16da9c6c81"
