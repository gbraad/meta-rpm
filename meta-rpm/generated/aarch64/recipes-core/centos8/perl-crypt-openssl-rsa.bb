SUMMARY = "generated recipe based on perl-Crypt-OpenSSL-RSA srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl perl pkgconfig-native"
RPM_SONAME_REQ_perl-Crypt-OpenSSL-RSA = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libperl.so.5.26 libpthread.so.0 libssl.so.1.1"
RDEPENDS_perl-Crypt-OpenSSL-RSA = "glibc openssl-libs perl-Carp perl-Crypt-OpenSSL-Bignum perl-Crypt-OpenSSL-Random perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Crypt-OpenSSL-RSA-0.31-1.el8.aarch64.rpm \
          "

SRC_URI[perl-Crypt-OpenSSL-RSA.sha256sum] = "02abf783dd8a0775553c7a074c713b246f1a643d1d06222add9a7c343097d4f2"
