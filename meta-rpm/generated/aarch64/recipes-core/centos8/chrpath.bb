SUMMARY = "generated recipe based on chrpath srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_chrpath = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_chrpath = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/chrpath-0.16-7.el8.aarch64.rpm \
          "

SRC_URI[chrpath.sha256sum] = "1607f738abc6ac4b5973c63097711b3c08bc0f844e3c7f44867f11124c0547aa"
