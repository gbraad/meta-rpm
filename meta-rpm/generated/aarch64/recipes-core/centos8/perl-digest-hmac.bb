SUMMARY = "generated recipe based on perl-Digest-HMAC srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Digest-HMAC = "perl-Digest-MD5 perl-Digest-SHA perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Digest-HMAC-1.03-17.el8.noarch.rpm \
          "

SRC_URI[perl-Digest-HMAC.sha256sum] = "3452f53c4c9b161fa240a70037ac76961b4b176903ec7dd633bcc90c61c7e88c"
