SUMMARY = "generated recipe based on stunnel srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_stunnel = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libpthread.so.0 libssl.so.1.1 libutil.so.1"
RDEPENDS_stunnel = "bash glibc openssl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/stunnel-5.48-5.el8.0.1.aarch64.rpm \
          "

SRC_URI[stunnel.sha256sum] = "a5c5ba7a8cac13604a249f940b2a5f6a8514a739bbbfea13750ecfb74bad4f85"
