SUMMARY = "generated recipe based on mockito srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_mockito = "cglib hamcrest hamcrest-core java-1.8.0-openjdk-headless javapackages-filesystem junit objenesis"
RDEPENDS_mockito-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mockito-1.10.19-17.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mockito-javadoc-1.10.19-17.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[mockito.sha256sum] = "d8a81cadbdad5313f06b9851f8c88f86618d86888d8860f2cf9c81c873fc730c"
SRC_URI[mockito-javadoc.sha256sum] = "ccbae589f889a790e3c2746ef1ec7c772f4a7aa8810a5409c4b377d3fdebce40"
