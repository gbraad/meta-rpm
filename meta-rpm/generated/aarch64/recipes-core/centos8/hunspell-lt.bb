SUMMARY = "generated recipe based on hunspell-lt srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-lt = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-lt-1.2.1-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-lt.sha256sum] = "63b072990a6eb6aa9d119129599ed89d6234d64d28eb2e5ed8e5593725a92363"
