SUMMARY = "generated recipe based on maven-reporting-impl srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-reporting-impl = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-core maven-doxia-sink-api maven-doxia-sitetools maven-lib maven-reporting-api maven-shared-utils plexus-utils"
RDEPENDS_maven-reporting-impl-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-reporting-impl-3.0.0-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-reporting-impl-javadoc-3.0.0-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-reporting-impl.sha256sum] = "1c048e9ccae5fc4c5dc21b58f46a5a23238f1b877e2ccd5e84c66f6395d8a7a4"
SRC_URI[maven-reporting-impl-javadoc.sha256sum] = "f27806f2a781205fd8b527efd136af93546a5e0cfb4f2505b01e6471dce2f0b5"
