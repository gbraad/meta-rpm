SUMMARY = "generated recipe based on apache-commons-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-parent = "apache-parent java-1.8.0-openjdk-headless javapackages-filesystem maven-antrun-plugin maven-assembly-plugin maven-compiler-plugin maven-jar-plugin maven-plugin-build-helper maven-plugin-bundle maven-surefire-plugin"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-parent-43-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-parent.sha256sum] = "f2aa1cca676bee256d6e2631d023d0c991fd951386fffaba5b67b2a712ddfe45"
