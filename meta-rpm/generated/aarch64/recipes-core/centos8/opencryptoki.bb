SUMMARY = "generated recipe based on opencryptoki srpm"
DESCRIPTION = "Description"
LICENSE = "CPL-1.0"
RPM_LICENSE = "CPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openldap openssl pkgconfig-native trousers"
RPM_SONAME_REQ_opencryptoki = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libitm.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libpthread.so.0"
RDEPENDS_opencryptoki = "bash coreutils glibc libitm opencryptoki-libs opencryptoki-swtok openldap openssl-libs systemd"
RPROVIDES_opencryptoki-devel = "opencryptoki-dev (= 3.12.1)"
RDEPENDS_opencryptoki-devel = "opencryptoki-libs"
RPM_SONAME_PROV_opencryptoki-icsftok = "libpkcs11_icsf.so.0"
RPM_SONAME_REQ_opencryptoki-icsftok = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libitm.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libpthread.so.0 librt.so.1 libssl.so.1.1"
RDEPENDS_opencryptoki-icsftok = "glibc libitm opencryptoki-libs openldap openssl-libs"
RPM_SONAME_PROV_opencryptoki-libs = "libopencryptoki.so.0"
RPM_SONAME_REQ_opencryptoki-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libitm.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libpthread.so.0"
RDEPENDS_opencryptoki-libs = "bash glibc libitm openldap shadow-utils"
RPM_SONAME_PROV_opencryptoki-swtok = "libpkcs11_sw.so.0"
RPM_SONAME_REQ_opencryptoki-swtok = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libitm.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libpthread.so.0 librt.so.1"
RDEPENDS_opencryptoki-swtok = "glibc libitm opencryptoki-libs openldap openssl-libs"
RPM_SONAME_PROV_opencryptoki-tpmtok = "libpkcs11_tpm.so.0"
RPM_SONAME_REQ_opencryptoki-tpmtok = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libitm.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libpthread.so.0 librt.so.1 libtspi.so.1"
RDEPENDS_opencryptoki-tpmtok = "glibc libitm opencryptoki-libs openldap openssl-libs trousers-lib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/opencryptoki-3.12.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/opencryptoki-icsftok-3.12.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/opencryptoki-libs-3.12.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/opencryptoki-swtok-3.12.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/opencryptoki-tpmtok-3.12.1-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/opencryptoki-devel-3.12.1-2.el8.aarch64.rpm \
          "

SRC_URI[opencryptoki.sha256sum] = "0f7ebdfe14860f23d8fefac19d2e9b162881b76b21888b3fc930027b59b90cc8"
SRC_URI[opencryptoki-devel.sha256sum] = "9d4b25be0014050ea0115c84dfe316ba2b46507daca5cea1d6ca8db2fce23c5b"
SRC_URI[opencryptoki-icsftok.sha256sum] = "26a159be779de591b7b860c72556f2d8ece29a555e20399f0c6ead102f7800a0"
SRC_URI[opencryptoki-libs.sha256sum] = "e1924c32a82910cb5f5536824294929d7eeba261a6a5bc8e9fad89326806e23c"
SRC_URI[opencryptoki-swtok.sha256sum] = "70f00aa3feb2c7450b89356638cdcb23f1001297b39da459066168d4ced04801"
SRC_URI[opencryptoki-tpmtok.sha256sum] = "b759d4545138e8f49ef4173d65a86c90ea995fd587d60ed228919fad3eb17112"
