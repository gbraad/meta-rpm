SUMMARY = "generated recipe based on perl-Sub-Install srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Sub-Install = "perl-Carp perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Sub-Install-0.928-14.el8.noarch.rpm \
          "

SRC_URI[perl-Sub-Install.sha256sum] = "37c6272d20e1fa67ea3414d13db66854b052b0e619aab9f8db4f6ee32448dd1c"
