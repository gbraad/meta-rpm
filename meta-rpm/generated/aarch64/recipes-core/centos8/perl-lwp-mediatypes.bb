SUMMARY = "generated recipe based on perl-LWP-MediaTypes srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-LWP-MediaTypes = "mailcap perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-LWP-MediaTypes-6.02-14.el8.noarch.rpm \
          "

SRC_URI[perl-LWP-MediaTypes.sha256sum] = "46beca0a72ea64cafee2dc6c620422a79422aa4662fd5f76f00c62f939c5b704"
