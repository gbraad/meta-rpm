SUMMARY = "generated recipe based on lvm2 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libblkid libgcc libselinux libsepol pkgconfig-native systemd-libs"
RPM_SONAME_REQ_device-mapper-event = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libdevmapper-event.so.1.02 libdevmapper.so.1.02 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libselinux.so.1 libsepol.so.1 libudev.so.1"
RDEPENDS_device-mapper-event = "bash device-mapper device-mapper-event-libs device-mapper-libs glibc libblkid libgcc libselinux libsepol systemd systemd-libs"
RPM_SONAME_REQ_device-mapper-event-devel = "libdevmapper-event.so.1.02"
RPROVIDES_device-mapper-event-devel = "device-mapper-event-dev (= 1.02.169)"
RDEPENDS_device-mapper-event-devel = "device-mapper-event device-mapper-event-libs pkgconf-pkg-config"
RPM_SONAME_PROV_device-mapper-event-libs = "libdevmapper-event.so.1.02"
RPM_SONAME_REQ_device-mapper-event-libs = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libdevmapper.so.1.02 libm.so.6 libpthread.so.0 libselinux.so.1 libsepol.so.1 libudev.so.1"
RDEPENDS_device-mapper-event-libs = "device-mapper-libs glibc libblkid libselinux libsepol systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/device-mapper-event-1.02.169-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/device-mapper-event-libs-1.02.169-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/device-mapper-event-devel-1.02.169-3.el8.aarch64.rpm \
          "

SRC_URI[device-mapper-event.sha256sum] = "3d0231f5fa4bafd53984d5d34ba0029bb3b6fad8da3beb51bbf01aae2feb1857"
SRC_URI[device-mapper-event-devel.sha256sum] = "9eaac86d94a0a1715155a525460dde2d925e17966b4118128d691c77ab6943f2"
SRC_URI[device-mapper-event-libs.sha256sum] = "bb72de961d637a73c8a9f7bb819b7d525930713d9c0b87f2ec40c29fe8a4b65b"
