SUMMARY = "generated recipe based on python-sqlalchemy srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-sqlalchemy = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_python3-sqlalchemy = "glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python-sqlalchemy-doc-1.3.2-1.module_el8.1.0+245+c39af44f.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-sqlalchemy-1.3.2-1.module_el8.1.0+245+c39af44f.aarch64.rpm \
          "

SRC_URI[python-sqlalchemy-doc.sha256sum] = "74574cbe3ed07b4d456c681715744e796b0703304ed4b9e8f59ea5843a10bc65"
SRC_URI[python3-sqlalchemy.sha256sum] = "aa4438e174dcb3f11b0c142b7e64cf8db401bc939412075de32fb4123f83bd42"
