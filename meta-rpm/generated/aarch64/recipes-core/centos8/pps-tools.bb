SUMMARY = "generated recipe based on pps-tools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_pps-tools-devel = "pps-tools-dev (= 1.0.2)"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/pps-tools-devel-1.0.2-1.el8.aarch64.rpm \
          "

SRC_URI[pps-tools-devel.sha256sum] = "eca085cf6f5ffd1178e08693f9c0228d9f58bce10f2f80e4baa73ef0ff4ff479"
