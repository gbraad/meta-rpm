SUMMARY = "generated recipe based on libetonyek srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-2.0"
RPM_LICENSE = "MPLv2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc liblangtag librevenge libxml2 pkgconfig-native zlib"
RPM_SONAME_PROV_libetonyek = "libetonyek-0.1.so.1"
RPM_SONAME_REQ_libetonyek = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 liblangtag.so.1 libm.so.6 librevenge-0.0.so.0 libstdc++.so.6 libxml2.so.2 libz.so.1"
RDEPENDS_libetonyek = "glibc libgcc liblangtag librevenge libstdc++ libxml2 zlib"
RPM_SONAME_REQ_libetonyek-devel = "libetonyek-0.1.so.1"
RPROVIDES_libetonyek-devel = "libetonyek-dev (= 0.1.8)"
RDEPENDS_libetonyek-devel = "libetonyek liblangtag-devel librevenge-devel libxml2-devel pkgconf-pkg-config zlib-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libetonyek-0.1.8-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libetonyek-devel-0.1.8-1.el8.aarch64.rpm \
          "

SRC_URI[libetonyek.sha256sum] = "b912e14ffa911cc1fd5ed96d522a57d8242de728e1f92b9ed00e83ec80276ddc"
SRC_URI[libetonyek-devel.sha256sum] = "9f68a4c690de1a532de34bdd08f35b93d8cfef27ee821df5e23828d336c9d0d9"
