SUMMARY = "generated recipe based on fwupdate srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "efivar pkgconfig-native popt"
RPM_SONAME_REQ_fwupdate = "ld-linux-aarch64.so.1 libc.so.6 libefiboot.so.1 libefivar.so.1 libfwup.so.1 libpopt.so.0 libpthread.so.0"
RDEPENDS_fwupdate = "bash efivar-libs fwupdate-libs glibc popt"
RDEPENDS_fwupdate-efi = "fwupdate-libs"
RPM_SONAME_PROV_fwupdate-libs = "libfwup.so.1"
RPM_SONAME_REQ_fwupdate-libs = "ld-linux-aarch64.so.1 libc.so.6 libefiboot.so.1 libefivar.so.1 libpthread.so.0"
RDEPENDS_fwupdate-libs = "bash efivar-libs fwupdate-efi glibc shim-aa64"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fwupdate-11-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fwupdate-efi-11-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/fwupdate-libs-11-3.el8.aarch64.rpm \
          "

SRC_URI[fwupdate.sha256sum] = "130ad9ff5b259fd1d080c7b5d7d9ea3d9407f1c21271f5c681912ccc5188e4d0"
SRC_URI[fwupdate-efi.sha256sum] = "74be1ef6329d9926108809bddaa30a209d898ae321062abc053ebd89cd3f76a7"
SRC_URI[fwupdate-libs.sha256sum] = "40de8057892e061bf1907443ea5ae11cc2d52bf85e15b86c7c54ad0f692bc2c8"
