SUMMARY = "generated recipe based on gnome-settings-daemon srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib cairo colord cups-libs fontconfig gdk-pixbuf geoclue2 geocode-glib glib-2.0 gnome-desktop3 gtk+3 lcms2 libcanberra libgcc libgudev libgweather libnotify libwacom libx11 libxext libxi networkmanager nspr nss pango pkgconfig-native polkit pulseaudio upower wayland"
RPM_SONAME_PROV_gnome-settings-daemon = "libgsd.so"
RPM_SONAME_REQ_gnome-settings-daemon = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXi.so.6 libasound.so.2 libc.so.6 libcairo.so.2 libcanberra-gtk3.so.0 libcanberra.so.0 libcolord.so.2 libcups.so.2 libdl.so.2 libfontconfig.so.1 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgeoclue-2.so.0 libgeocode-glib.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgtk-3.so.0 libgudev-1.0.so.0 libgweather-3.so.15 liblcms2.so.2 libm.so.6 libnm.so.0 libnotify.so.4 libnspr4.so libnss3.so libpango-1.0.so.0 libpangocairo-1.0.so.0 libpolkit-gobject-1.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libupower-glib.so.3 libwacom.so.2 libwayland-client.so.0"
RDEPENDS_gnome-settings-daemon = "NetworkManager-libnm alsa-lib cairo colord colord-libs cups-libs fontconfig gdk-pixbuf2 geoclue2 geoclue2-libs geocode-glib glib2 glibc gnome-desktop3 gsettings-desktop-schemas gtk3 iio-sensor-proxy lcms2 libX11 libXext libXi libcanberra libcanberra-gtk3 libgcc libgudev libgweather libnotify libwacom libwayland-client nspr nss pango polkit-libs pulseaudio-libs pulseaudio-libs-glib2 upower"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-settings-daemon-3.32.0-9.el8.0.1.aarch64.rpm \
          "

SRC_URI[gnome-settings-daemon.sha256sum] = "d29816a8aca15e892778fe85f254d279eee4f263d37e60b3e8808d769ca6a738"
