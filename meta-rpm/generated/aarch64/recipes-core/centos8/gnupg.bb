SUMMARY = "generated recipe based on gnupg2 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 gnutls-libs libassuan libgcrypt libgpg-error libksba libusb1 npth openldap pkgconfig-native readline sqlite3 zlib"
RPM_SONAME_REQ_gnupg2 = "ld-linux-aarch64.so.1 libassuan.so.0 libbz2.so.1 libc.so.6 libdl.so.2 libgcrypt.so.20 libgnutls.so.30 libgpg-error.so.0 libksba.so.8 liblber-2.4.so.2 libldap-2.4.so.2 libnpth.so.0 libpthread.so.0 libreadline.so.7 libresolv.so.2 libsqlite3.so.0 libusb-1.0.so.0 libz.so.1"
RDEPENDS_gnupg2 = "bash bzip2-libs glibc gnutls libassuan libgcrypt libgpg-error libksba libusbx npth openldap readline sqlite-libs zlib"
RPM_SONAME_REQ_gnupg2-smime = "ld-linux-aarch64.so.1 libassuan.so.0 libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0 libksba.so.8 libreadline.so.7"
RDEPENDS_gnupg2-smime = "glibc gnupg2 libassuan libgcrypt libgpg-error libksba readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gnupg2-2.2.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gnupg2-smime-2.2.9-1.el8.aarch64.rpm \
          "

SRC_URI[gnupg2.sha256sum] = "cdaad1e2c2321bd400740c140f595c03baee66a4d3af42a97808202c2a047614"
SRC_URI[gnupg2-smime.sha256sum] = "caa079a4e2ee59c9fb062c2fee2087e360863249d820eecb4c21f12a106c352c"
