SUMMARY = "generated recipe based on devhelp srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPL2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gsettings-desktop-schemas gtk+3 libsoup-2.4 pango pkgconfig-native webkit2gtk3"
RPM_SONAME_REQ_devhelp = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libdevhelp-3.so.5 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjavascriptcoregtk-4.0.so.18 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsoup-2.4.so.1 libwebkit2gtk-4.0.so.37"
RDEPENDS_devhelp = "atk cairo cairo-gobject devhelp-libs gdk-pixbuf2 glib2 glibc gtk3 libsoup pango webkit2gtk3 webkit2gtk3-jsc"
RPM_SONAME_REQ_devhelp-devel = "libdevhelp-3.so.5"
RPROVIDES_devhelp-devel = "devhelp-dev (= 3.28.1)"
RDEPENDS_devhelp-devel = "devhelp devhelp-libs glib2-devel gsettings-desktop-schemas-devel gtk3-devel pkgconf-pkg-config webkit2gtk3-devel"
RPM_SONAME_PROV_devhelp-libs = "libdevhelp-3.so.5"
RPM_SONAME_REQ_devhelp-libs = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libjavascriptcoregtk-4.0.so.18 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libsoup-2.4.so.1 libwebkit2gtk-4.0.so.37"
RDEPENDS_devhelp-libs = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libsoup pango webkit2gtk3 webkit2gtk3-jsc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/devhelp-3.28.1-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/devhelp-libs-3.28.1-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/devhelp-devel-3.28.1-5.el8.aarch64.rpm \
          "

SRC_URI[devhelp.sha256sum] = "697c8ac461b15cf9058480913fe25b8de026c904fd69e01fd9c27e8f6f9c6eab"
SRC_URI[devhelp-devel.sha256sum] = "abb21c041e16dc5e0c466d0fdfb92eca3a43757b336c3ac1c347c95eee0b4b2e"
SRC_URI[devhelp-libs.sha256sum] = "b0e2b3f9c97bc6b554263de6e4c63a4fe2b313ed051e4d540f10ad4e490cc2c4"
