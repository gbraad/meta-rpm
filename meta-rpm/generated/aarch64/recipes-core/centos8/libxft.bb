SUMMARY = "generated recipe based on libXft srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "xft"
DEPENDS = "fontconfig freetype libx11 libxrender pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXft = "libXft.so.2"
RPM_SONAME_REQ_libXft = "ld-linux-aarch64.so.1 libX11.so.6 libXrender.so.1 libc.so.6 libfontconfig.so.1 libfreetype.so.6"
RDEPENDS_libXft = "fontconfig freetype glibc libX11 libXrender"
RPM_SONAME_REQ_libXft-devel = "libXft.so.2"
RPROVIDES_libXft-devel = "libXft-dev (= 2.3.2)"
RDEPENDS_libXft-devel = "fontconfig-devel freetype-devel libXft libXrender-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXft-2.3.2-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXft-devel-2.3.2-10.el8.aarch64.rpm \
          "

SRC_URI[libXft.sha256sum] = "fdba543bd1567d1a57518d410cbd37d76009fbd07cfad6ea76d37f2582658441"
SRC_URI[libXft-devel.sha256sum] = "6441a9981fbd87cb98c88899b7256ec1bb23e1aaf59edec8afe368e35f865c2a"
