SUMMARY = "generated recipe based on graphite2 srpm"
DESCRIPTION = "Description"
LICENSE = "(LGPL-2.0 | GPL-2.0 | MPL-1.0) & (CLOSED | GPL-2.0 | LGPL-2.0)"
RPM_LICENSE = "(LGPLv2+ or GPLv2+ or MPL) and (Netscape or GPLv2+ or LGPLv2+)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_graphite2 = "libgraphite2.so.3"
RPM_SONAME_REQ_graphite2 = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_graphite2 = "glibc libgcc libstdc++"
RPM_SONAME_REQ_graphite2-devel = "libgraphite2.so.3"
RPROVIDES_graphite2-devel = "graphite2-dev (= 1.3.10)"
RDEPENDS_graphite2-devel = "graphite2 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/graphite2-1.3.10-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/graphite2-devel-1.3.10-10.el8.aarch64.rpm \
          "

SRC_URI[graphite2.sha256sum] = "661c6e578d9e15b509ab31ad04ffcb6fcb583d1ccbd560fe66f83ea421841117"
SRC_URI[graphite2-devel.sha256sum] = "48d2b8206d9cc863093c3d9100caa0c9357beb6ffad9338c0c05e9dec0856c1b"
