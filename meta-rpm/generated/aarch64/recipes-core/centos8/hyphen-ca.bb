SUMMARY = "generated recipe based on hyphen-ca srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-ca = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-ca-0.9.3-14.el8.noarch.rpm \
          "

SRC_URI[hyphen-ca.sha256sum] = "39462358d4b9e50a1b8a189666f59f6c759b5d401c68651bf49f9fd1d36f0382"
