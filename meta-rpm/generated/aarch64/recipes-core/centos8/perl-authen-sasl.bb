SUMMARY = "generated recipe based on perl-Authen-SASL srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Authen-SASL = "perl-Carp perl-Digest-HMAC perl-Digest-MD5 perl-GSSAPI perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Authen-SASL-2.16-13.el8.noarch.rpm \
          "

SRC_URI[perl-Authen-SASL.sha256sum] = "b2b2069808580f87725fb52d8e11b28aa6d2e44280fb13aca213c0fac62e4f0b"
