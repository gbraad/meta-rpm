SUMMARY = "generated recipe based on perl-IO-stringy srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IO-stringy = "perl-Carp perl-Exporter perl-IO perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-IO-stringy-2.111-9.el8.noarch.rpm \
          "

SRC_URI[perl-IO-stringy.sha256sum] = "1541a76c75250f7ba7ae7c1782f6f505b37e622c54193f6e0c83d96de868e453"
