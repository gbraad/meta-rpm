SUMMARY = "generated recipe based on soundtouch srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_soundtouch = "libSoundTouch.so.2"
RPM_SONAME_REQ_soundtouch = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_soundtouch = "glibc libgcc libstdc++"
RPM_SONAME_REQ_soundtouch-devel = "libSoundTouch.so.2"
RPROVIDES_soundtouch-devel = "soundtouch-dev (= 2.0.0)"
RDEPENDS_soundtouch-devel = "pkgconf-pkg-config soundtouch"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/soundtouch-2.0.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/soundtouch-devel-2.0.0-2.el8.aarch64.rpm \
          "

SRC_URI[soundtouch.sha256sum] = "4f7629f212f46f61197471fc5c485b4d40440ef25be274295e3117f6531e4452"
SRC_URI[soundtouch-devel.sha256sum] = "2b7a94ba3af7ac9d61778a12f1162cfc64b3973c96703fe44b8977a6ceccab85"
