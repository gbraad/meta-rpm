SUMMARY = "generated recipe based on perl-Capture-Tiny srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Capture-Tiny = "perl-Carp perl-Exporter perl-File-Temp perl-IO perl-PathTools perl-Scalar-List-Utils perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Capture-Tiny-0.46-4.el8.noarch.rpm \
          "

SRC_URI[perl-Capture-Tiny.sha256sum] = "493567e2313214d40d6d68efab195c6c2a715414db92334c5de85c887f931a8f"
