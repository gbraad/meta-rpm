SUMMARY = "generated recipe based on man-pages srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0 & BSD & MIT & CLOSED & CLOSED"
RPM_LICENSE = "GPL+ and GPLv2+ and BSD and MIT and Copyright only and IEEE"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/man-pages-4.15-6.el8.aarch64.rpm \
          "

SRC_URI[man-pages.sha256sum] = "78c7341f17373cb484469f97e9cd8e49599fab7e4c21d5deb18d86f971183c8a"
