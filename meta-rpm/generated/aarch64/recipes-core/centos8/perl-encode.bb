SUMMARY = "generated recipe based on perl-Encode srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Encode = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Encode = "glibc perl-Carp perl-Exporter perl-Getopt-Long perl-MIME-Base64 perl-Storable perl-constant perl-interpreter perl-libs perl-parent"
RPROVIDES_perl-Encode-devel = "perl-Encode-dev (= 2.97)"
RDEPENDS_perl-Encode-devel = "perl-Encode perl-constant perl-interpreter perl-libs"
RDEPENDS_perl-encoding = "perl-Carp perl-Encode perl-Filter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Encode-devel-2.97-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-encoding-2.22-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Encode-2.97-3.el8.aarch64.rpm \
          "

SRC_URI[perl-Encode.sha256sum] = "3102c49c7aca506d4d2808479dd3beff029bc8eb5c0df22d6238f164e85cc349"
SRC_URI[perl-Encode-devel.sha256sum] = "ae3143ecd152649136467b4054e0a235f349353b3d32c362824c6ff601cc543f"
SRC_URI[perl-encoding.sha256sum] = "10243d9d08df3157e709beec0c72ed260bb1afca6009082ea4eb373bcc56f732"
