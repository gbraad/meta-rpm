SUMMARY = "generated recipe based on kpatch srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_kpatch = "bash binutils kmod"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kpatch-0.6.1-6.el8.noarch.rpm \
          "

SRC_URI[kpatch.sha256sum] = "3248ffb70a779613dd3ec2a3bbb5051c77a9798d1d897653a15e3663ad3ec2fc"
