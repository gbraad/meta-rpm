SUMMARY = "generated recipe based on selinux-policy srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
RDEPENDS_selinux-policy = "bash coreutils gawk policycoreutils rpm-plugin-selinux"
RDEPENDS_selinux-policy-doc = "selinux-policy"
RDEPENDS_selinux-policy-targeted = "bash coreutils policycoreutils selinux-policy"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/selinux-policy-3.14.3-41.el8_2.8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/selinux-policy-doc-3.14.3-41.el8_2.8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/selinux-policy-targeted-3.14.3-41.el8_2.8.noarch.rpm \
          "

SRC_URI[selinux-policy.sha256sum] = "318bb66d340b4fc4e0e27ab96f5b652ad75064834dc5b6f81248a2b4c3379b1f"
SRC_URI[selinux-policy-doc.sha256sum] = "16e51e509804a5c08729ab790563de6408654cf2e75292ceb0fa4c6d6fe7b0c4"
SRC_URI[selinux-policy-targeted.sha256sum] = "9d71f83a538fbc93c26cdb088e7fe4aa3f7c50a041b50c368719b30bf50411c4"
