SUMMARY = "generated recipe based on libpsl srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libidn2 libunistring pkgconfig-native"
RPM_SONAME_PROV_libpsl = "libpsl.so.5"
RPM_SONAME_REQ_libpsl = "ld-linux-aarch64.so.1 libc.so.6 libidn2.so.0 libunistring.so.2"
RDEPENDS_libpsl = "glibc libidn2 libunistring publicsuffix-list-dafsa"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libpsl-0.20.2-5.el8.aarch64.rpm \
          "

SRC_URI[libpsl.sha256sum] = "8743b29596353e56b8f8c5c946f89a1d2edf698f17a7c4166c57366e01be68d4"
