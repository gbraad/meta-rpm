SUMMARY = "generated recipe based on multilib-rpm-config srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_multilib-rpm-config = "bash redhat-rpm-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/multilib-rpm-config-1-10.el8.noarch.rpm \
          "

SRC_URI[multilib-rpm-config.sha256sum] = "937bdbba99e119d5f8d0c6d252f058565539a552ecce68a348fb84286c674f01"
