SUMMARY = "generated recipe based on perl-generators srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-generators = "perl-Fedora-VSP perl-interpreter perl-libs perl-macros"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-generators-1.10-9.el8.noarch.rpm \
          "

SRC_URI[perl-generators.sha256sum] = "3aad9996ecb4de9508c858fe255c9aa40b309bd48cb6947d363bf0e0658b267b"
