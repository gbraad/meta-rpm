SUMMARY = "generated recipe based on perl-Devel-GlobalDestruction srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Devel-GlobalDestruction = "perl-Sub-Exporter-Progressive perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Devel-GlobalDestruction-0.14-5.el8.noarch.rpm \
          "

SRC_URI[perl-Devel-GlobalDestruction.sha256sum] = "a247e8f1643b42c9ef6ceb7160c58d67663fe0fc1d18a2bfc5cdc86226f89402"
