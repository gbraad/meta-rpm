SUMMARY = "generated recipe based on mtx srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mtx = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mtx = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mtx-1.3.12-17.el8.aarch64.rpm \
          "

SRC_URI[mtx.sha256sum] = "4d4b2045bd11b3d018922dcbcc65b9ba16e6150ed516fedeb0c07a75e5cf9d73"
