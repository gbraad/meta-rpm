SUMMARY = "generated recipe based on qt5 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-srpm-macros-5.12.5-3.el8.noarch.rpm \
          "

SRC_URI[qt5-srpm-macros.sha256sum] = "bd7d4cc40a56d9dc00308bdb093f540fe344ba1b215abfb0c7613f16ee379d13"
