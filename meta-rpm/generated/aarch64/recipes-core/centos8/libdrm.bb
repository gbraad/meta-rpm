SUMMARY = "generated recipe based on libdrm srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "drm"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdrm = "libdrm.so.2 libdrm_amdgpu.so.1 libdrm_etnaviv.so.1 libdrm_exynos.so.1 libdrm_freedreno.so.1 libdrm_nouveau.so.2 libdrm_radeon.so.1 libdrm_tegra.so.0 libkms.so.1"
RPM_SONAME_REQ_libdrm = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpthread.so.0"
RDEPENDS_libdrm = "glibc"
RPM_SONAME_REQ_libdrm-devel = "libdrm.so.2 libdrm_amdgpu.so.1 libdrm_etnaviv.so.1 libdrm_exynos.so.1 libdrm_freedreno.so.1 libdrm_nouveau.so.2 libdrm_radeon.so.1 libdrm_tegra.so.0 libkms.so.1"
RPROVIDES_libdrm-devel = "libdrm-dev (= 2.4.100)"
RDEPENDS_libdrm-devel = "kernel-headers libdrm pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdrm-2.4.100-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdrm-devel-2.4.100-1.el8.aarch64.rpm \
          "

SRC_URI[libdrm.sha256sum] = "db83b2a1fa8e1f04599cc53cf650f8ca3114c022813dc92d9b7ea758a12fa560"
SRC_URI[libdrm-devel.sha256sum] = "a34d4648f1dee063be68258aa65ec9aeeaeb957afce667e6582ab36561d4bd9a"
