SUMMARY = "generated recipe based on perl-XML-SAX-Base srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-SAX-Base = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-XML-SAX-Base-1.09-4.el8.noarch.rpm \
          "

SRC_URI[perl-XML-SAX-Base.sha256sum] = "96c87413205259b2b5eeb6e322b1d43e744e547441b7a57e94df3af78ec9acb7"
