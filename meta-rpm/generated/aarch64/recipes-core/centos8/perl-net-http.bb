SUMMARY = "generated recipe based on perl-Net-HTTP srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Net-HTTP = "perl-Carp perl-Compress-Raw-Zlib perl-IO-Compress perl-IO-Socket-IP perl-IO-Socket-SSL perl-URI perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Net-HTTP-6.17-2.el8.noarch.rpm \
          "

SRC_URI[perl-Net-HTTP.sha256sum] = "aa9761cf2a7069b89252449e679b291a83e2da960d3a95bc9a118e8a22ba7277"
