SUMMARY = "generated recipe based on systemtap srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "avahi-libs boost dyninst elfutils json-c libgcc libselinux ncurses nspr nss pkgconfig-native platform-python3 readline rpm sqlite3"
RDEPENDS_systemtap = "bash systemtap-client systemtap-devel"
RPM_SONAME_REQ_systemtap-client = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libreadline.so.7 librpm.so.8 librpmio.so.8 libsmime3.so libsqlite3.so.0 libssl3.so libstdc++.so.6"
RDEPENDS_systemtap-client = "avahi-libs bash coreutils elfutils-libelf elfutils-libs glibc grep libgcc libstdc++ nspr nss nss-util openssh-clients perl-interpreter readline rpm-libs sed sqlite-libs systemtap-runtime unzip zip"
RPM_SONAME_PROV_systemtap-devel = "libHelperSDT_aarch64.so"
RPM_SONAME_REQ_systemtap-devel = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libreadline.so.7 librpm.so.8 librpmio.so.8 libsmime3.so libsqlite3.so.0 libssl3.so libstdc++.so.6"
RPROVIDES_systemtap-devel = "systemtap-dev (= 4.2)"
RDEPENDS_systemtap-devel = "avahi-libs bash elfutils-libelf elfutils-libs gcc glibc libgcc libstdc++ make nspr nss nss-util readline rpm-libs sqlite-libs"
RDEPENDS_systemtap-exporter = "bash platform-python systemtap-runtime"
RDEPENDS_systemtap-initscript = "bash systemd systemtap"
RPM_SONAME_REQ_systemtap-runtime = "ld-linux-aarch64.so.1 libboost_system.so.1.66.0 libc.so.6 libdl.so.2 libdyninstAPI.so.10.1 libelf.so.1 libgcc_s.so.1 libjson-c.so.4 libm.so.6 libncurses.so.6 libnspr4.so libnss3.so libnssutil3.so libpanel.so.6 libplc4.so libplds4.so libpthread.so.0 libselinux.so.1 libsmime3.so libssl3.so libstdc++.so.6 libsymtabAPI.so.10.1 libtinfo.so.6"
RDEPENDS_systemtap-runtime = "bash boost-system dyninst elfutils-libelf glibc json-c libgcc libselinux libstdc++ ncurses-libs nspr nss nss-util shadow-utils"
RPM_SONAME_PROV_systemtap-runtime-java = "libHelperSDT_aarch64.so"
RPM_SONAME_REQ_systemtap-runtime-java = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_systemtap-runtime-java = "bash byteman glibc iproute java-1.8.0-openjdk-devel systemtap-runtime"
RPM_SONAME_REQ_systemtap-runtime-python3 = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0"
RDEPENDS_systemtap-runtime-python3 = "glibc platform-python python3-libs systemtap-runtime"
RDEPENDS_systemtap-runtime-virtguest = "bash coreutils findutils grep systemtap-runtime"
RPM_SONAME_REQ_systemtap-server = "ld-linux-aarch64.so.1 libavahi-client.so.3 libavahi-common.so.3 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libstdc++.so.6"
RDEPENDS_systemtap-server = "avahi-libs bash coreutils glibc libgcc libstdc++ nspr nss nss-util shadow-utils systemd systemtap-devel unzip zip"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-4.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-client-4.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-devel-4.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-exporter-4.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-initscript-4.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-runtime-4.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-runtime-java-4.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-runtime-python3-4.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-runtime-virtguest-4.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/systemtap-server-4.2-6.el8.aarch64.rpm \
          "

SRC_URI[systemtap.sha256sum] = "ff92d9e2d067afc59d4f10cda1e75d37091f2de06a014f78102a6c9e6069e580"
SRC_URI[systemtap-client.sha256sum] = "ce739de9f962c30714517f6175248f39b624e414013784d9b802a2a683515d0e"
SRC_URI[systemtap-devel.sha256sum] = "09e5c71a5b076d4f3c74ea7aa2991225eb1ded291e01b14f2836cd657a1a8cb3"
SRC_URI[systemtap-exporter.sha256sum] = "9bbe04901e9d885b91cc4f3cd87f1d8e1d2e34fb03eaae3519800a72d46e85bc"
SRC_URI[systemtap-initscript.sha256sum] = "2d880d9899d790e8673a7c3b5d124690bec5c7e99634e40cc3a75a558dec1563"
SRC_URI[systemtap-runtime.sha256sum] = "2ef66fcf593808e74907d7fa2efa89b4578a1a665f669e2399022f25874eae8b"
SRC_URI[systemtap-runtime-java.sha256sum] = "cca38500f7232672b7b0f99df4cbfef211ef2c5f9c674d0393e5fc64d42351b4"
SRC_URI[systemtap-runtime-python3.sha256sum] = "fc333525183e065b38b5b0249984bcd27cd1fc95cf5010750aee146a8eb83f9c"
SRC_URI[systemtap-runtime-virtguest.sha256sum] = "af90e3ee763af00590becbb7409ade1d1e9e158b06aa17e69a81224942b70ee5"
SRC_URI[systemtap-server.sha256sum] = "7007688f0704dd885a2d4be2b3c46d28d482cb9a573d2252be53e5b6f8a92322"
