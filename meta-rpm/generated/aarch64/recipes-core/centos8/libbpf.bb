SUMMARY = "generated recipe based on libbpf srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | BSD"
RPM_LICENSE = "LGPLv2 or BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_PROV_libbpf = "libbpf.so.0"
RPM_SONAME_REQ_libbpf = "ld-linux-aarch64.so.1 libc.so.6 libelf.so.1"
RDEPENDS_libbpf = "elfutils-libelf glibc"
RPM_SONAME_REQ_libbpf-devel = "libbpf.so.0"
RPROVIDES_libbpf-devel = "libbpf-dev (= 0.0.4)"
RDEPENDS_libbpf-devel = "elfutils-libelf-devel kernel-headers libbpf pkgconf-pkg-config"
RDEPENDS_libbpf-static = "libbpf-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libbpf-0.0.4-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libbpf-devel-0.0.4-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libbpf-static-0.0.4-5.el8.aarch64.rpm \
          "

SRC_URI[libbpf.sha256sum] = "893fbc5054750a8e1f4ea5de2ec2b0472492a67e0578b56466ec2a987ab4b350"
SRC_URI[libbpf-devel.sha256sum] = "8815ebe3fe87d07ab0c8133d58d29c72dacd6b66b12be2733001ddad07d49aa5"
SRC_URI[libbpf-static.sha256sum] = "fb6248364c24e29ed4c01b437fea2b4e839035be3b6f943b8b63bc326bb99f97"
