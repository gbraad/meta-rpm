SUMMARY = "generated recipe based on copy-jdk-configs srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_copy-jdk-configs = "bash lua"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/copy-jdk-configs-3.7-1.el8.noarch.rpm \
          "

SRC_URI[copy-jdk-configs.sha256sum] = "6bbe9bd9f64aa9464db4040857e8408cf02e2b5527ae0aef42919f865ae2621a"
