SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-ia = "locale-base-ia-fr (= 2.28) virtual-locale-ia (= 2.28) virtual-locale-ia-fr (= 2.28)"
RDEPENDS_glibc-langpack-ia = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ia-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-ia.sha256sum] = "c6a0bc21c646a0eab74f207026ab2d49d4ce96cb3ee0ffbf3691af5b947d264e"
