SUMMARY = "generated recipe based on cldr-emoji-annotation srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & CLOSED"
RPM_LICENSE = "LGPLv2+ and Unicode"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cldr-emoji-annotation-33.1.0_0-1.el8.noarch.rpm \
          "

SRC_URI[cldr-emoji-annotation.sha256sum] = "a055ed7669856cacf31c2c3d57d1500076b8e95de008854d8878c9afbcd458c1"
