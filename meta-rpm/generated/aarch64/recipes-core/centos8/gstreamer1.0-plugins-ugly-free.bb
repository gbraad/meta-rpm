SUMMARY = "generated recipe based on gstreamer1-plugins-ugly-free srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0"
RPM_LICENSE = "LGPLv2+ and LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "a52dec glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base libcdio libdvdread orc pkgconfig-native"
RPM_SONAME_PROV_gstreamer1-plugins-ugly-free = "libgsta52dec.so libgstcdio.so libgstdvdread.so libgstxingmux.so"
RPM_SONAME_REQ_gstreamer1-plugins-ugly-free = "ld-linux-aarch64.so.1 liba52.so.0 libc.so.6 libcdio.so.18 libdvdread.so.4 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgsttag-1.0.so.0 libm.so.6 liborc-0.4.so.0 libpthread.so.0"
RDEPENDS_gstreamer1-plugins-ugly-free = "glib2 glibc gstreamer1 gstreamer1-plugins-base liba52 libcdio libdvdread orc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gstreamer1-plugins-ugly-free-1.16.1-1.el8.aarch64.rpm \
          "

SRC_URI[gstreamer1-plugins-ugly-free.sha256sum] = "b8c80d93ec29577ff65f97698bff178af55743f94b8b4f88df847ed8e40af86b"
