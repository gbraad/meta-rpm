SUMMARY = "generated recipe based on mailcap srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & MIT"
RPM_LICENSE = "Public Domain and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mailcap-2.1.48-3.el8.noarch.rpm \
          "

SRC_URI[mailcap.sha256sum] = "6b3dd4e265ebb23b3d5532653f74da0ad896ee7e1dd925b524e471c34ee645f0"
