SUMMARY = "generated recipe based on ldns srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_PROV_ldns = "libldns.so.2"
RPM_SONAME_REQ_ldns = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libssl.so.1.1"
RDEPENDS_ldns = "ca-certificates glibc openssl-libs"
RPM_SONAME_REQ_ldns-devel = "libldns.so.2"
RPROVIDES_ldns-devel = "ldns-dev (= 1.7.0)"
RDEPENDS_ldns-devel = "bash ldns openssl-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/ldns-1.7.0-21.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ldns-devel-1.7.0-21.el8.aarch64.rpm \
          "

SRC_URI[ldns.sha256sum] = "580fe2fd4663a6814b2b550caec7efa4d5669ea91ea29aafa965cdd7a40f23a9"
SRC_URI[ldns-devel.sha256sum] = "edde9645544c4d9d279db8c72e30b496cd2f89125e2a723d39295e2221580207"
