SUMMARY = "generated recipe based on cronie srpm"
DESCRIPTION = "Description"
LICENSE = "MIT & BSD & ISC & GPL-2.0"
RPM_LICENSE = "MIT and BSD and ISC and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs libselinux pam pkgconfig-native"
RPM_SONAME_REQ_cronie = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libdl.so.2 libpam.so.0 libselinux.so.1"
RDEPENDS_cronie = "audit-libs bash coreutils cronie-anacron glibc libselinux pam sed systemd"
RPM_SONAME_REQ_cronie-anacron = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libdl.so.2 libpam.so.0 libselinux.so.1"
RDEPENDS_cronie-anacron = "audit-libs bash coreutils cronie crontabs glibc libselinux pam"
RDEPENDS_cronie-noanacron = "cronie crontabs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cronie-1.5.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cronie-anacron-1.5.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cronie-noanacron-1.5.2-4.el8.aarch64.rpm \
          "

SRC_URI[cronie.sha256sum] = "d95e7d75d39bdec2742f406a12027605612e8abc38a9bcac5c9b632611313020"
SRC_URI[cronie-anacron.sha256sum] = "b729404e9d1195caf72cf793331886708fdf8d807b74575871b5aa8ae8d726b4"
SRC_URI[cronie-noanacron.sha256sum] = "ba4d3b6cb3b760d8b04fb38b3d999779e417932d2c923af345353ff1ffd33d27"
