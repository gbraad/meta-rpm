SUMMARY = "generated recipe based on hunspell-be srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPL+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-be = "hunspell"
RDEPENDS_hyphen-be = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-be-1.1-15.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-be-1.1-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-be.sha256sum] = "5c09dcee7ff72fac0d361f808dba054edca0da28d7aa1fcf3880b5dba5212ac0"
SRC_URI[hyphen-be.sha256sum] = "caea865e5076a39aacbf98d1d6effa540e518e96ebc8596bc4d6143444db98e0"
