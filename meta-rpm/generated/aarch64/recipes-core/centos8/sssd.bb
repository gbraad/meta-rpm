SUMMARY = "generated recipe based on sssd srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & LGPL-3.0"
RPM_LICENSE = "GPLv3+ and LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "c-ares cyrus-sasl-lib dbus-libs ding-libs e2fsprogs glib-2.0 jansson keyutils krb5-libs libldb libnl libpcre libselinux libsemanage libtalloc libtdb libtevent libuuid nfs-utils openldap openssl p11-kit pam pkgconfig-native platform-python3 popt samba systemd-libs"
RPM_SONAME_PROV_libipa_hbac = "libipa_hbac.so.0"
RPM_SONAME_REQ_libipa_hbac = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0"
RDEPENDS_libipa_hbac = "glib2 glibc"
RPM_SONAME_PROV_libsss_autofs = "libsss_autofs.so"
RPM_SONAME_REQ_libsss_autofs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libsss_autofs = "glibc"
RPM_SONAME_PROV_libsss_certmap = "libsss_certmap.so.0"
RPM_SONAME_REQ_libsss_certmap = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libtalloc.so.2"
RDEPENDS_libsss_certmap = "glibc libtalloc openssl-libs"
RPM_SONAME_PROV_libsss_idmap = "libsss_idmap.so.0"
RPM_SONAME_REQ_libsss_idmap = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libsss_idmap = "glibc"
RPM_SONAME_PROV_libsss_nss_idmap = "libsss_nss_idmap.so.0"
RPM_SONAME_REQ_libsss_nss_idmap = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_libsss_nss_idmap = "glibc"
RPM_SONAME_REQ_libsss_nss_idmap-devel = "libsss_nss_idmap.so.0"
RPROVIDES_libsss_nss_idmap-devel = "libsss_nss_idmap-dev (= 2.2.3)"
RDEPENDS_libsss_nss_idmap-devel = "libsss_nss_idmap pkgconf-pkg-config"
RPM_SONAME_PROV_libsss_simpleifp = "libsss_simpleifp.so.0"
RPM_SONAME_REQ_libsss_simpleifp = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libdhash.so.1"
RDEPENDS_libsss_simpleifp = "dbus-libs glibc libdhash sssd-dbus"
RPM_SONAME_PROV_libsss_sudo = "libsss_sudo.so"
RPM_SONAME_REQ_libsss_sudo = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libsss_sudo = "glibc"
RPM_SONAME_REQ_python3-libipa_hbac = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libglib-2.0.so.0 libipa_hbac.so.0 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libutil.so.1"
RDEPENDS_python3-libipa_hbac = "glib2 glibc libipa_hbac platform-python python3-libs"
RPM_SONAME_REQ_python3-libsss_nss_idmap = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libsss_nss_idmap.so.0 libutil.so.1"
RDEPENDS_python3-libsss_nss_idmap = "glibc libsss_nss_idmap platform-python python3-libs"
RPM_SONAME_REQ_python3-sss = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libini_config.so.5 libldb.so.2 libm.so.6 libpcre.so.1 libpopt.so.0 libpthread.so.0 libpython3.6m.so.1.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0 libutil.so.1"
RDEPENDS_python3-sss = "dbus-libs glib2 glibc libbasicobjects libcollection libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent openssl-libs pcre platform-python popt python3-libs sssd-common systemd-libs"
RPM_SONAME_REQ_python3-sss-murmur = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libutil.so.1"
RDEPENDS_python3-sss-murmur = "glibc platform-python python3-libs"
RDEPENDS_python3-sssdconfig = "platform-python"
RDEPENDS_sssd = "python3-sssdconfig sssd-ad sssd-common sssd-ipa sssd-krb5 sssd-ldap"
RPM_SONAME_PROV_sssd-ad = "libsss_ad.so"
RPM_SONAME_REQ_sssd-ad = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libglib-2.0.so.0 libini_config.so.5 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libldb.so.2 libndr-krb5pac.so.0 libndr-nbt.so.0 libndr-standard.so.0 libndr.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libsamba-util.so.0 libsasl2.so.3 libselinux.so.1 libsmbclient.so.0 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_idmap.so.0 libsss_krb5_common.so libsss_ldap_common.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-ad = "cyrus-sasl-lib dbus-libs glib2 glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libldb libref_array libselinux libsmbclient libsss_certmap libsss_idmap libtalloc libtdb libtevent openldap openssl-libs pcre popt samba-client-libs sssd-common sssd-common-pac sssd-krb5-common systemd-libs"
RPM_SONAME_PROV_sssd-client = "libnss_sss.so.2"
RPM_SONAME_REQ_sssd-client = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libk5crypto.so.3 libkrb5.so.3 libpam.so.0 libpthread.so.0 libsss_idmap.so.0 libsss_nss_idmap.so.0"
RDEPENDS_sssd-client = "bash chkconfig glibc krb5-libs libcom_err libsss_idmap libsss_nss_idmap pam"
RPM_SONAME_PROV_sssd-common = "libifp_iface.so libifp_iface_sync.so libsss_cert.so libsss_child.so libsss_crypt.so libsss_debug.so libsss_files.so libsss_iface.so libsss_iface_sync.so libsss_krb5_common.so libsss_ldap_common.so libsss_sbus.so libsss_sbus_sync.so libsss_semanage.so libsss_simple.so libsss_util.so"
RPM_SONAME_REQ_sssd-common = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcares.so.2 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libini_config.so.5 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libldb.so.2 libnl-3.so.200 libnl-route-3.so.200 libp11-kit.so.0 libpam.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsemanage.so.1 libssl.so.1.1 libsss_certmap.so.0 libsss_idmap.so.0 libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-common = "bash c-ares dbus-libs glib2 glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libldb libnl3 libref_array libselinux libsemanage libsss_certmap libsss_idmap libtalloc libtdb libtevent openldap openssl-libs p11-kit pam pcre popt shadow-utils sssd-client systemd systemd-libs"
RPM_SONAME_REQ_sssd-common-pac = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libini_config.so.5 libldb.so.2 libndr-krb5pac.so.0 libndr-standard.so.0 libndr.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libsamba-util.so.0 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_idmap.so.0 libsss_iface.so libsss_sbus.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-common-pac = "dbus-libs glib2 glibc libbasicobjects libcollection libdhash libini_config libldb libref_array libselinux libsss_certmap libsss_idmap libtalloc libtdb libtevent openssl-libs pcre popt samba-client-libs sssd-common systemd-libs"
RPM_SONAME_REQ_sssd-dbus = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libifp_iface.so libini_config.so.5 libldb.so.2 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_iface.so libsss_sbus.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-dbus = "bash dbus-libs glib2 glibc libbasicobjects libcollection libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent openssl-libs pcre popt sssd-common systemd systemd-libs"
RPM_SONAME_PROV_sssd-ipa = "libsss_ipa.so"
RPM_SONAME_REQ_sssd-ipa = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libglib-2.0.so.0 libini_config.so.5 libipa_hbac.so.0 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libldb.so.2 libndr-krb5pac.so.0 libndr-nbt.so.0 libndr-standard.so.0 libndr.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libsamba-util.so.0 libselinux.so.1 libsemanage.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_idmap.so.0 libsss_krb5_common.so libsss_ldap_common.so libsss_semanage.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-ipa = "bash dbus-libs glib2 glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libipa_hbac libldb libref_array libselinux libsemanage libsss_certmap libsss_idmap libtalloc libtdb libtevent openldap openssl-libs pcre popt samba-client-libs shadow-utils sssd-common sssd-common-pac sssd-krb5-common systemd-libs"
RPM_SONAME_PROV_sssd-kcm = "libsss_secrets.so"
RPM_SONAME_REQ_sssd-kcm = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libini_config.so.5 libjansson.so.4 libk5crypto.so.3 libkrb5.so.3 libldb.so.2 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_iface.so libsss_sbus.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0 libuuid.so.1"
RDEPENDS_sssd-kcm = "bash dbus-libs glib2 glibc jansson krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent libuuid openssl-libs pcre popt sssd-common systemd systemd-libs"
RPM_SONAME_PROV_sssd-krb5 = "libsss_krb5.so"
RPM_SONAME_REQ_sssd-krb5 = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libglib-2.0.so.0 libini_config.so.5 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 libldb.so.2 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_krb5_common.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-krb5 = "dbus-libs glib2 glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent openssl-libs pcre popt sssd-common sssd-krb5-common systemd-libs"
RPM_SONAME_REQ_sssd-krb5-common = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libdhash.so.1 libk5crypto.so.3 libkrb5.so.3 libpopt.so.0 libsss_debug.so libsystemd.so.0 libtalloc.so.2"
RDEPENDS_sssd-krb5-common = "bash cyrus-sasl-gssapi glibc krb5-libs libcom_err libdhash libtalloc popt shadow-utils sssd-common systemd-libs"
RPM_SONAME_PROV_sssd-ldap = "libsss_ldap.so"
RPM_SONAME_REQ_sssd-ldap = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcom_err.so.2 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libglib-2.0.so.0 libini_config.so.5 libk5crypto.so.3 libkeyutils.so.1 libkrb5.so.3 liblber-2.4.so.2 libldap-2.4.so.2 libldb.so.2 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_idmap.so.0 libsss_krb5_common.so libsss_ldap_common.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-ldap = "dbus-libs glib2 glibc keyutils-libs krb5-libs libbasicobjects libcollection libcom_err libdhash libini_config libldb libref_array libselinux libsss_certmap libsss_idmap libtalloc libtdb libtevent openldap openssl-libs pcre popt sssd-common sssd-krb5-common systemd-libs"
RPM_SONAME_REQ_sssd-libwbclient = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0 libsss_nss_idmap.so.0"
RDEPENDS_sssd-libwbclient = "bash glibc libsss_nss_idmap"
RPM_SONAME_REQ_sssd-nfs-idmap = "ld-linux-aarch64.so.1 libc.so.6 libnfsidmap.so.1"
RDEPENDS_sssd-nfs-idmap = "glibc libnfsidmap"
RDEPENDS_sssd-polkit-rules = "polkit sssd-common"
RPM_SONAME_PROV_sssd-proxy = "libsss_proxy.so"
RPM_SONAME_REQ_sssd-proxy = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libini_config.so.5 libldb.so.2 libpam.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_iface.so libsss_sbus.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-proxy = "bash dbus-libs glib2 glibc libbasicobjects libcollection libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent openssl-libs pam pcre popt shadow-utils sssd-common systemd-libs"
RPM_SONAME_REQ_sssd-tools = "ld-linux-aarch64.so.1 libbasicobjects.so.0 libc.so.6 libcollection.so.4 libcrypto.so.1.1 libdbus-1.so.3 libdhash.so.1 libdl.so.2 libglib-2.0.so.0 libifp_iface_sync.so libini_config.so.5 libldb.so.2 libpam.so.0 libpam_misc.so.0 libpcre.so.1 libpopt.so.0 libref_array.so.1 librt.so.1 libselinux.so.1 libsss_cert.so libsss_certmap.so.0 libsss_child.so libsss_crypt.so libsss_debug.so libsss_iface_sync.so libsss_sbus_sync.so libsss_util.so libsystemd.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_sssd-tools = "bash dbus-libs glib2 glibc libbasicobjects libcollection libdhash libini_config libldb libref_array libselinux libsss_certmap libtalloc libtdb libtevent openssl-libs pam pcre platform-python popt python3-sss python3-sssdconfig sssd-common systemd-libs"
RPM_SONAME_REQ_sssd-winbind-idmap = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libsss_idmap.so.0 libsss_nss_idmap.so.0 libtalloc.so.2"
RDEPENDS_sssd-winbind-idmap = "glibc libsss_idmap libsss_nss_idmap libtalloc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libipa_hbac-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsss_autofs-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsss_certmap-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsss_idmap-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsss_nss_idmap-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsss_simpleifp-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsss_sudo-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-libipa_hbac-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-libsss_nss_idmap-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-sss-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-sss-murmur-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-sssdconfig-2.2.3-20.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-ad-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-client-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-common-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-common-pac-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-dbus-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-ipa-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-kcm-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-krb5-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-krb5-common-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-ldap-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-libwbclient-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-nfs-idmap-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-polkit-rules-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-proxy-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-tools-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sssd-winbind-idmap-2.2.3-20.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsss_nss_idmap-devel-2.2.3-20.el8.aarch64.rpm \
          "

SRC_URI[libipa_hbac.sha256sum] = "209f97a497c56cd6d64dc3308940f97b9f0574be87ac51752e094aebc30ab9a0"
SRC_URI[libsss_autofs.sha256sum] = "09eff051d435251035b8195647c8b0d67ca6abfd44bce760ba50f25f32917009"
SRC_URI[libsss_certmap.sha256sum] = "72ada739cc290fdd8a0af7fc1ca2517feae37581f6b293953f34f66a1f0d257b"
SRC_URI[libsss_idmap.sha256sum] = "9cfc239e24aba04ca0b23a4deb1013059d1fcc6b2c37953219763bdb4d481b4c"
SRC_URI[libsss_nss_idmap.sha256sum] = "e1b2f0b88480c9e2d62a1c3631d0ec0396cb896ff0e988318b529b707e89bf1a"
SRC_URI[libsss_nss_idmap-devel.sha256sum] = "61f1582119fac65f359d8f1770e1f77158d5b855dc73a91e4086349145c07996"
SRC_URI[libsss_simpleifp.sha256sum] = "f330aa3f5ea175cf90767ad148e08f81158ff9cdd4420dae0d08264dd1a508a8"
SRC_URI[libsss_sudo.sha256sum] = "98a1e5343b21a96271b23a07e9a07e802beca735d7c9e1e7ce5e08eb3985f2fd"
SRC_URI[python3-libipa_hbac.sha256sum] = "db4e566959cbad25cdbb00cf2dd6cd2457207a864444b86b248f9fe36b92dbff"
SRC_URI[python3-libsss_nss_idmap.sha256sum] = "88ef623eca3a785e02576c8c70f0764d36ce1df1b9570190251acafe79775e07"
SRC_URI[python3-sss.sha256sum] = "b153ce4414216a1bf58bee5abb28f626ddf4a1f61080038914c8d9786826b1b7"
SRC_URI[python3-sss-murmur.sha256sum] = "d1d5db033887f4b66f29cde3e5ac6e06d4a14a3338b94dd94297e6b91f4cfac1"
SRC_URI[python3-sssdconfig.sha256sum] = "e26bbd19357780bfb4c70403c7bf722d24f16a5fe330dd074da86542c57e6200"
SRC_URI[sssd.sha256sum] = "c1224e6ef99d7bdd4f8258f010fb0467d3d36eb81afcb1faba134514c68f03bc"
SRC_URI[sssd-ad.sha256sum] = "627219cead84663596029ae63cb97b785f6762499bc8921ccd2ff7ff5e26c10f"
SRC_URI[sssd-client.sha256sum] = "ea8c84bf695e0f60c9a6bb07f69c10e99da83647c39f66485a1f95a59b93108e"
SRC_URI[sssd-common.sha256sum] = "cffb3bdf59d486d7900324515ef6546ae54d07fb566e0e55393164900e529b66"
SRC_URI[sssd-common-pac.sha256sum] = "e930ae8dc5194a2a87e086536a5284957064509fa84608a8fa90939580114d5b"
SRC_URI[sssd-dbus.sha256sum] = "8598a2697c0715cc6ced1691d506861e19a7fd11c71562d0e17600f2e72ff7ea"
SRC_URI[sssd-ipa.sha256sum] = "d5fd959a03cb54fa9efae04bc8665ed8f2670cbf83b5f813df48856544d5f5a0"
SRC_URI[sssd-kcm.sha256sum] = "0763e7ebbbcade79163ccd2a9d2fa61eaa2115f24e08e14347d5ebe86d999b45"
SRC_URI[sssd-krb5.sha256sum] = "a2ffbb5c018e8e086d563ba90eda9e6ad6723d2a9b64df0b0ccd1fd54109b225"
SRC_URI[sssd-krb5-common.sha256sum] = "cd7666dc17345a839b4088978c9fb16933fb38996d6b41dbb9753ef9358cb659"
SRC_URI[sssd-ldap.sha256sum] = "569097ef8af5eba19dd8c85a6b6527a6704d8a30c4c467c84cad24a1187c77c0"
SRC_URI[sssd-libwbclient.sha256sum] = "b170c780b11f48bc235b788743625a0ebf933c6b00bd1b7a009fb70d97981547"
SRC_URI[sssd-nfs-idmap.sha256sum] = "32007802c657f71723034e4dfcee06305e6a74051987dce0fa1efd7fefa8632c"
SRC_URI[sssd-polkit-rules.sha256sum] = "12e452c421d9a2e3eb78c97c7cd873dc8f6de6178d9c42ea5ce464b3828b6011"
SRC_URI[sssd-proxy.sha256sum] = "e566a9f1e5512cb6e1f1b85bb61e98daec9bc52c4200e1ab316bb4b744c95bb2"
SRC_URI[sssd-tools.sha256sum] = "c571163dbb5ad64ec7aab3bef593fad8b181a5556bbc13dd9d7ead1da0960a9f"
SRC_URI[sssd-winbind-idmap.sha256sum] = "b8a2c1620ae18e573f0584a305c9f93cccba6cc0321a24c32abe31168579836f"
