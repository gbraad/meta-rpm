SUMMARY = "generated recipe based on libXdamage srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "xdamage"
DEPENDS = "libx11 libxfixes pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXdamage = "libXdamage.so.1"
RPM_SONAME_REQ_libXdamage = "ld-linux-aarch64.so.1 libX11.so.6 libXfixes.so.3 libc.so.6"
RDEPENDS_libXdamage = "glibc libX11 libXfixes"
RPM_SONAME_REQ_libXdamage-devel = "libXdamage.so.1"
RPROVIDES_libXdamage-devel = "libXdamage-dev (= 1.1.4)"
RDEPENDS_libXdamage-devel = "libX11-devel libXdamage libXfixes-devel pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXdamage-1.1.4-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXdamage-devel-1.1.4-14.el8.aarch64.rpm \
          "

SRC_URI[libXdamage.sha256sum] = "e94899df13e35b3076a9ef254e78469908c3f7c570b7b8b81a0f42062fbc0d0c"
SRC_URI[libXdamage-devel.sha256sum] = "225edc313567fc812b088f2d327d22e66717c1ad5dd234c2a122574b7dfc37a8"
