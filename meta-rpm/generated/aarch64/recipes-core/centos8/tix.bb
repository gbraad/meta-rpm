SUMMARY = "generated recipe based on tix srpm"
DESCRIPTION = "Description"
LICENSE = "tcl"
RPM_LICENSE = "TCL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 pkgconfig-native"
RPM_SONAME_PROV_tix = "libTix.so"
RPM_SONAME_REQ_tix = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6"
RDEPENDS_tix = "glibc libX11 tcl tk"
RPROVIDES_tix-devel = "tix-dev (= 8.4.3)"
RDEPENDS_tix-devel = "tix"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/tix-8.4.3-23.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/tix-devel-8.4.3-23.el8.aarch64.rpm \
          "

SRC_URI[tix.sha256sum] = "6ec0b3560c08e59a87042eb83b9967af173a0dfcec3109ca9556bf0a9b1c6d34"
SRC_URI[tix-devel.sha256sum] = "f013dc48a39b7b7e509c9012b4ec9cf9a8ac02b6183f797ce9c0c4e6fa804859"
