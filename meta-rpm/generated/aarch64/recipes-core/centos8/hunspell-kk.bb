SUMMARY = "generated recipe based on hunspell-kk srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-2.0 | MPL-1.1"
RPM_LICENSE = "GPLv2+ or LGPLv2+ or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-kk = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-kk-1.1-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-kk.sha256sum] = "a6c68941c8aa9242aa9271e9c95c689391c16809c576504127b106208d11d28a"
