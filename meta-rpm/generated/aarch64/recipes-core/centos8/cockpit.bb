SUMMARY = "generated recipe based on cockpit srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 gnutls-libs json-glib krb5-libs libgcc libssh libxcrypt pam pkgconfig-native polkit systemd-libs"
RDEPENDS_cockpit = "cockpit-bridge cockpit-system cockpit-ws"
RPM_SONAME_REQ_cockpit-bridge = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libssh.so.4 libsystemd.so.0 libutil.so.1"
RDEPENDS_cockpit-bridge = "glib-networking glib2 glibc json-glib libgcc libssh polkit-libs systemd-libs"
RDEPENDS_cockpit-system = "NetworkManager cockpit-bridge coreutils grep kexec-tools libpwquality shadow-utils"
RPM_SONAME_REQ_cockpit-ws = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgssapi_krb5.so.2 libjson-glib-1.0.so.0 libkrb5.so.3 libpam.so.0 libpthread.so.0 libsystemd.so.0 libutil.so.1"
RDEPENDS_cockpit-ws = "bash glib-networking glib2 glibc gnutls json-glib krb5-libs libgcc libxcrypt openssl pam systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cockpit-211.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cockpit-bridge-211.3-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cockpit-doc-211.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cockpit-system-211.3-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cockpit-ws-211.3-1.el8.aarch64.rpm \
          "

SRC_URI[cockpit.sha256sum] = "e0894c36b5e2e65ccb9f45f62111afadfb436edeb45345a4c1feb5b9ba0f9005"
SRC_URI[cockpit-bridge.sha256sum] = "8f66e4adccfc8b3174653b35f61fa401f5722500823f40c9f731332d84c30903"
SRC_URI[cockpit-doc.sha256sum] = "11de5ccb7445b90701ace0572e42a757449611b21398f0c31b52088f1f14d8fd"
SRC_URI[cockpit-system.sha256sum] = "6d9d7511800071c47b2b5d4f4b25f17661259e0503ef9bafcf3b7ea05c4f71a3"
SRC_URI[cockpit-ws.sha256sum] = "0393518cc2af0c4d58e1cdcb236ec756c88e0c272ab669d50b8751609f411782"
