SUMMARY = "generated recipe based on sendmail srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Sendmail"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cyrus-sasl-lib db libnsl2 libxcrypt openldap openssl pkgconfig-native"
RPM_SONAME_REQ_sendmail = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdb-5.3.so liblber-2.4.so.2 libldap-2.4.so.2 libnsl.so.2 libresolv.so.2 libsasl2.so.3 libssl.so.1.1"
RDEPENDS_sendmail = "bash chkconfig coreutils cyrus-sasl cyrus-sasl-lib glibc libdb libnsl2 libxcrypt openldap openssl openssl-libs procmail setup shadow-utils systemd"
RDEPENDS_sendmail-cf = "bash m4 sendmail"
RDEPENDS_sendmail-doc = "sendmail"
RPM_SONAME_PROV_sendmail-milter = "libmilter.so.1.0"
RPM_SONAME_REQ_sendmail-milter = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_sendmail-milter = "glibc"
RPM_SONAME_REQ_sendmail-milter-devel = "libmilter.so.1.0"
RPROVIDES_sendmail-milter-devel = "sendmail-milter-dev (= 8.15.2)"
RDEPENDS_sendmail-milter-devel = "sendmail-milter"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sendmail-8.15.2-32.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sendmail-cf-8.15.2-32.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sendmail-doc-8.15.2-32.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sendmail-milter-8.15.2-32.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sendmail-milter-devel-8.15.2-32.el8.aarch64.rpm \
          "

SRC_URI[sendmail.sha256sum] = "000643435699517b31e973674576832812fd2afae3928e71fd6495ca9a14a137"
SRC_URI[sendmail-cf.sha256sum] = "c9e1de4de52b731052c58ad3e53f22f5e7b2cb73d57bb75cb00922413466b008"
SRC_URI[sendmail-doc.sha256sum] = "c2ffadb95511127412ef9ed0754daa07a1eca6775a66fbbd39a02e09d08d3110"
SRC_URI[sendmail-milter.sha256sum] = "dd4fe3903cad96268b08912a3b92e1fe5a40169b1a37cbf69899a9ede98b0e8f"
SRC_URI[sendmail-milter-devel.sha256sum] = "897a7f9833e80f234eaef096beecfe16ef00b8403c7d5e8e9e6b64bd3e11222c"
