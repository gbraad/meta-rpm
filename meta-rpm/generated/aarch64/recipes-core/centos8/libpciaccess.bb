SUMMARY = "generated recipe based on libpciaccess srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libpciaccess = "libpciaccess.so.0"
RPM_SONAME_REQ_libpciaccess = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libpciaccess = "glibc hwdata"
RPM_SONAME_REQ_libpciaccess-devel = "libpciaccess.so.0"
RPROVIDES_libpciaccess-devel = "libpciaccess-dev (= 0.14)"
RDEPENDS_libpciaccess-devel = "libpciaccess pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libpciaccess-0.14-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libpciaccess-devel-0.14-1.el8.aarch64.rpm \
          "

SRC_URI[libpciaccess.sha256sum] = "a13ddb43ac2c840959f0fcbc62cf3ef4045ee8e878f59063de4eaa553bc07435"
SRC_URI[libpciaccess-devel.sha256sum] = "c0e63eb615d5194f8267a34360c21a07f32c77a827e44ef346bb6e6ba066848d"
