SUMMARY = "generated recipe based on python-virtualenv srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-virtualenv = "bash platform-python python3-pip-wheel python3-setuptools python3-setuptools-wheel python3-wheel-wheel python36 python36-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python-virtualenv-doc-15.1.0-19.module_el8.1.0+245+c39af44f.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-virtualenv-15.1.0-19.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python-virtualenv-doc.sha256sum] = "43074e87f6dfafe186db2fa71dadcd5e0cdb3864f0a07242c09e820711155d1b"
SRC_URI[python3-virtualenv.sha256sum] = "1078010de59381ad3df979e8435246a11d19c6361bcf9af46749e6624ad84610"
