SUMMARY = "generated recipe based on subscription-manager-migration-data srpm"
DESCRIPTION = "Description"
LICENSE = "CC0-1.0"
RPM_LICENSE = "CC0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/subscription-manager-migration-data-2.0.51-1.noarch.rpm \
          "

SRC_URI[subscription-manager-migration-data.sha256sum] = "29fa79ada0b8b660ab2933d7030fc5a9b25976e6609de1d583f0f13e0cdba0d1"
