SUMMARY = "generated recipe based on grafana srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_grafana = "libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_grafana = "bash glibc shadow-utils systemd"
RDEPENDS_grafana-azure-monitor = "grafana"
RDEPENDS_grafana-cloudwatch = "grafana"
RDEPENDS_grafana-elasticsearch = "grafana"
RDEPENDS_grafana-graphite = "grafana"
RDEPENDS_grafana-influxdb = "grafana"
RDEPENDS_grafana-loki = "grafana"
RDEPENDS_grafana-mssql = "grafana"
RDEPENDS_grafana-mysql = "grafana"
RDEPENDS_grafana-opentsdb = "grafana"
RDEPENDS_grafana-postgres = "grafana"
RDEPENDS_grafana-prometheus = "grafana"
RDEPENDS_grafana-stackdriver = "grafana"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-azure-monitor-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-cloudwatch-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-elasticsearch-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-graphite-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-influxdb-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-loki-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-mssql-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-mysql-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-opentsdb-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-postgres-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-prometheus-6.3.6-2.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/grafana-stackdriver-6.3.6-2.el8_2.aarch64.rpm \
          "

SRC_URI[grafana.sha256sum] = "f5e8c947a07cf9e8d4b55ec11428afdf07818492b0fe9266e0669937e1010be2"
SRC_URI[grafana-azure-monitor.sha256sum] = "c2e3fb57d81d63282803a92220e088781b94abdfaedabc3996c5d2221445e896"
SRC_URI[grafana-cloudwatch.sha256sum] = "0d7fbacce26a0e1955465bab4c818ba13067055fd439d64a844a2773a626b380"
SRC_URI[grafana-elasticsearch.sha256sum] = "c55614763ed98ee54a2e9180cb738500464bd0cd4434283561fb877f36eec3b7"
SRC_URI[grafana-graphite.sha256sum] = "f6668e461a84c25ff530bfcc8fb70cad05802cb81b63665d09bf30e690eafcb8"
SRC_URI[grafana-influxdb.sha256sum] = "cb4bf08df6c2f7a9cab724fdeac73cdef5742d7412e7ff729ec2985cf64b40a5"
SRC_URI[grafana-loki.sha256sum] = "29104285d05924529efde47b82809abc46d9287a3d0250edab14691ff2244d29"
SRC_URI[grafana-mssql.sha256sum] = "294bc395f2e881089eb40cf624b9248a2c187f2cba09b9ee7cc968523e8e29bb"
SRC_URI[grafana-mysql.sha256sum] = "a4e3a485a15305c2f7d5ab87cf5a21fc3ce92c060b5c3179bdc8fc429e010e7c"
SRC_URI[grafana-opentsdb.sha256sum] = "86901c66f469a619c06f8778df08411b56adf64c18ab14b1c3ddcf778ec69e05"
SRC_URI[grafana-postgres.sha256sum] = "1a59a0862412b57729aebaf2d0166ea6cda61fd6474cce633183266d721b9f66"
SRC_URI[grafana-prometheus.sha256sum] = "5231207796fe6c16f7cc2a27bcb0c9dba3dd24c3653cc7a7ccf0f59bda023fa4"
SRC_URI[grafana-stackdriver.sha256sum] = "4def972b6ecc49e7954c510f8737270c0897507e35375ac1ef0f804ebb8a3c92"
