SUMMARY = "generated recipe based on gsound srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libcanberra pkgconfig-native"
RPM_SONAME_PROV_gsound = "libgsound.so.0"
RPM_SONAME_REQ_gsound = "ld-linux-aarch64.so.1 libc.so.6 libcanberra.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_gsound = "glib2 glibc libcanberra"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gsound-1.0.2-6.el8.aarch64.rpm \
          "

SRC_URI[gsound.sha256sum] = "c856d2d02bc2f0cdeaa47831aa19d5d88c53c8f8bc8ecff48f2ececcf0ac4971"
