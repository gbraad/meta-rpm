SUMMARY = "generated recipe based on texlive srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo fontconfig freetype gd ghostscript gmp graphite2 harfbuzz icu libgcc libice libpaper libpng libsm libx11 libxaw libxext libxi libxmu libxpm libxt mpfr pixman pkgconfig-native poppler potrace teckit zlib zziplib"
RDEPENDS_texlive = "texlive-collection-latexrecommended texlive-kpathsea texlive-scheme-basic texlive-tetex"
RDEPENDS_texlive-adjustbox = "texlive-base texlive-collectbox texlive-graphics texlive-ifoddpage texlive-kpathsea texlive-pgf texlive-tools texlive-varwidth texlive-xkeyval"
RDEPENDS_texlive-ae = "texlive-base texlive-kpathsea texlive-latex"
RDEPENDS_texlive-algorithms = "texlive-base texlive-float texlive-graphics texlive-kpathsea texlive-latex"
RDEPENDS_texlive-amscls = "texlive-amsfonts texlive-amsmath texlive-base texlive-kpathsea texlive-latex texlive-url"
RDEPENDS_texlive-amsfonts = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-amsmath = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-anyfontsize = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-anysize = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-appendix = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-arabxetex = "texlive-amsmath texlive-base texlive-bidi texlive-fontspec texlive-kpathsea"
RDEPENDS_texlive-arphic = "texlive-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-attachfile = "texlive-base texlive-graphics texlive-hyperref texlive-kpathsea texlive-oberdiek texlive-tools"
RDEPENDS_texlive-avantgar = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-awesomebox = "texlive-base texlive-fontawesome texlive-ifluatex texlive-ifxetex texlive-tools texlive-xcolor texlive-xltxtra"
RDEPENDS_texlive-babel = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-babel-english = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-babelbib = "texlive-babel texlive-base texlive-kpathsea"
RDEPENDS_texlive-base = "bash coreutils"
RDEPENDS_texlive-beamer = "texlive-amscls texlive-amsfonts texlive-amsmath texlive-base texlive-geometry texlive-graphics texlive-hyperref texlive-kpathsea texlive-latex texlive-oberdiek texlive-pgf texlive-tools texlive-ucs texlive-xcolor"
RDEPENDS_texlive-bera = "texlive-base texlive-graphics texlive-kpathsea texlive-latex texlive-tetex"
RDEPENDS_texlive-beton = "texlive-base texlive-kpathsea"
RPM_SONAME_REQ_texlive-bibtex = "ld-linux-aarch64.so.1 libc.so.6 libkpathsea.so.6 libm.so.6"
RDEPENDS_texlive-bibtex = "glibc texlive-base texlive-kpathsea texlive-lib"
RDEPENDS_texlive-bibtopic = "texlive-base texlive-kpathsea texlive-latex"
RDEPENDS_texlive-bidi = "texlive-base texlive-changepage texlive-etoolbox texlive-fancyhdr texlive-geometry texlive-graphics texlive-hyperref texlive-iftex texlive-kpathsea texlive-latex texlive-marvosym texlive-ms texlive-natbib texlive-oberdiek texlive-paralist texlive-placeins texlive-sauerj texlive-setspace texlive-showexpl texlive-titlesec texlive-tools texlive-url texlive-xcolor texlive-xifthen"
RDEPENDS_texlive-bigfoot = "texlive-base texlive-etex-pkg texlive-kpathsea texlive-ncctools"
RDEPENDS_texlive-bookman = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-booktabs = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-breakurl = "texlive-base texlive-kpathsea texlive-oberdiek texlive-xkeyval"
RDEPENDS_texlive-breqn = "texlive-base texlive-graphics texlive-kpathsea texlive-l3kernel texlive-tools"
RDEPENDS_texlive-capt-of = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-caption = "texlive-base texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-carlisle = "texlive-base texlive-graphics texlive-kpathsea texlive-tools"
RDEPENDS_texlive-changebar = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-changepage = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-charter = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-chngcntr = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-cite = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-cjk = "perl-interpreter texlive-arphic texlive-base texlive-cns texlive-garuda-c90 texlive-graphics texlive-kpathsea texlive-latex texlive-norasi-c90 texlive-oberdiek texlive-uhc texlive-ulem texlive-wadalab"
RDEPENDS_texlive-classpack = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-cm = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-cm-lgc = "texlive-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-cm-super = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-latex texlive-tetex"
RDEPENDS_texlive-cmap = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-cmextra = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-cns = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-collectbox = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-collection-basic = "texlive-amsfonts texlive-base texlive-bibtex texlive-cm texlive-dvipdfmx texlive-dvips texlive-enctex texlive-etex texlive-etex-pkg texlive-glyphlist texlive-graphics-def texlive-gsftopk texlive-hyph-utf8 texlive-hyphen-base texlive-ifluatex texlive-ifxetex texlive-knuth-lib texlive-knuth-local texlive-kpathsea texlive-lua-alt-getopt texlive-luatex texlive-makeindex texlive-metafont texlive-mflogo texlive-mfware texlive-pdftex texlive-plain texlive-tetex texlive-tex texlive-tex-ini-files texlive-texconfig texlive-texlive-common-doc texlive-texlive-docindex texlive-texlive-en texlive-texlive-msg-translations texlive-texlive-scripts texlive-texlive.infra texlive-unicode-data texlive-updmap-map texlive-xdvi"
RDEPENDS_texlive-collection-fontsrecommended = "texlive-avantgar texlive-base texlive-bookman texlive-charter texlive-cm-super texlive-cmextra texlive-collection-basic texlive-courier texlive-ec texlive-euro texlive-eurosym texlive-fpl texlive-helvetic texlive-lm texlive-lm-math texlive-manfnt-font texlive-marvosym texlive-mathpazo texlive-mflogo-font texlive-ncntrsbk texlive-palatino texlive-pxfonts texlive-rsfs texlive-symbol texlive-tex-gyre texlive-tex-gyre-math texlive-times texlive-tipa texlive-txfonts texlive-utopia texlive-wasy texlive-wasy2-ps texlive-wasysym texlive-zapfchan texlive-zapfding"
RDEPENDS_texlive-collection-htmlxml = "texlive-base texlive-classpack texlive-collection-basic texlive-collection-fontsrecommended texlive-collection-latex texlive-jadetex texlive-passivetex texlive-tex4ht texlive-xmltex texlive-xmltexconfig"
RDEPENDS_texlive-collection-latex = "texlive-ae texlive-amscls texlive-amsmath texlive-babel texlive-babel-english texlive-babelbib texlive-base texlive-carlisle texlive-collection-basic texlive-colortbl texlive-fancyhdr texlive-fix2col texlive-geometry texlive-graphics texlive-graphics-cfg texlive-graphics-def texlive-hyperref texlive-latex texlive-latex-fonts texlive-latexconfig texlive-ltxmisc texlive-mfnfss texlive-mptopdf texlive-natbib texlive-oberdiek texlive-pslatex texlive-psnfss texlive-pspicture texlive-tools texlive-url"
RDEPENDS_texlive-collection-latexrecommended = "texlive-anysize texlive-base texlive-beamer texlive-booktabs texlive-breqn texlive-caption texlive-cite texlive-cmap texlive-collection-fontsrecommended texlive-collection-latex texlive-crop texlive-ctable texlive-eso-pic texlive-euenc texlive-euler texlive-extsizes texlive-fancybox texlive-fancyref texlive-fancyvrb texlive-float texlive-fontspec texlive-fp texlive-graphics texlive-index texlive-jknapltx texlive-koma-script texlive-l3experimental texlive-l3kernel texlive-l3packages texlive-lineno texlive-listings texlive-mathtools texlive-mdwtools texlive-memoir texlive-metalogo texlive-microtype texlive-ms texlive-ntgclass texlive-parskip texlive-pdfpages texlive-powerdot texlive-psfrag texlive-rcs texlive-sansmath texlive-section texlive-seminar texlive-sepnum texlive-setspace texlive-subfig texlive-textcase texlive-thumbpdf texlive-typehtml texlive-underscore texlive-xcolor texlive-xkeyval"
RDEPENDS_texlive-collection-xetex = "texlive-arabxetex texlive-awesomebox texlive-base texlive-collection-basic texlive-fixlatvian texlive-fontbook texlive-fontwrap texlive-philokalia texlive-ptext texlive-realscripts texlive-ucharclasses texlive-unisugar texlive-xecjk texlive-xecolor texlive-xecyr texlive-xeindex texlive-xesearch texlive-xetex texlive-xetex-itrans texlive-xetex-pstricks texlive-xetex-tibetan texlive-xetexconfig texlive-xetexfontinfo"
RDEPENDS_texlive-colortbl = "texlive-base texlive-graphics texlive-kpathsea texlive-tools"
RDEPENDS_texlive-courier = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-crop = "texlive-base texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-csquotes = "texlive-base texlive-etoolbox texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-ctable = "bash texlive-base texlive-booktabs texlive-etoolbox texlive-graphics texlive-kpathsea texlive-tools texlive-xkeyval"
RDEPENDS_texlive-ctablestack = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-currfile = "texlive-base texlive-filehook texlive-kpathsea texlive-oberdiek"
RDEPENDS_texlive-datetime = "texlive-base texlive-fmtcount texlive-kpathsea texlive-latex"
RDEPENDS_texlive-dvipdfmx = "bash texlive-base texlive-glyphlist texlive-graphics-def texlive-kpathsea"
RPM_SONAME_REQ_texlive-dvipng = "ld-linux-aarch64.so.1 libc.so.6 libfreetype.so.6 libgd.so.3 libkpathsea.so.6 libm.so.6 libpng16.so.16 libz.so.1"
RDEPENDS_texlive-dvipng = "bash freetype gd glibc info libpng texlive-base texlive-kpathsea texlive-lib zlib"
RPM_SONAME_REQ_texlive-dvips = "ld-linux-aarch64.so.1 libc.so.6 libkpathsea.so.6 libm.so.6"
RDEPENDS_texlive-dvips = "bash glibc info texlive-base texlive-kpathsea texlive-latex-fonts texlive-lib"
RPM_SONAME_REQ_texlive-dvisvgm = "ld-linux-aarch64.so.1 libc.so.6 libfreetype.so.6 libgcc_s.so.1 libgs.so.9 libkpathsea.so.6 libm.so.6 libpotrace.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_texlive-dvisvgm = "freetype glibc libgcc libgs libstdc++ potrace texlive-base texlive-kpathsea texlive-lib zlib"
RDEPENDS_texlive-ec = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-eepic = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-enctex = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-enumitem = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-environ = "texlive-base texlive-kpathsea texlive-trimspaces"
RDEPENDS_texlive-epsf = "bash texlive-base texlive-kpathsea"
RDEPENDS_texlive-epstopdf = "perl-File-Temp perl-Getopt-Long perl-interpreter texlive-base texlive-kpathsea"
RDEPENDS_texlive-eqparbox = "texlive-base texlive-environ texlive-kpathsea texlive-tools"
RDEPENDS_texlive-eso-pic = "texlive-base texlive-graphics texlive-kpathsea texlive-oberdiek texlive-xcolor"
RDEPENDS_texlive-etex = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-etex-pkg = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-etoolbox = "texlive-base texlive-etex-pkg texlive-kpathsea"
RDEPENDS_texlive-euenc = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-euler = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-euro = "texlive-base texlive-fp texlive-kpathsea"
RDEPENDS_texlive-eurosym = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-extsizes = "texlive-base texlive-kpathsea texlive-latex"
RDEPENDS_texlive-fancybox = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-fancyhdr = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-fancyref = "texlive-base texlive-kpathsea texlive-tools"
RDEPENDS_texlive-fancyvrb = "texlive-base texlive-graphics texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-filecontents = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-filehook = "texlive-base texlive-currfile texlive-kpathsea texlive-oberdiek texlive-pgf"
RDEPENDS_texlive-finstrut = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-fix2col = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-fixlatvian = "texlive-base texlive-bigfoot texlive-caption texlive-etoolbox texlive-kpathsea texlive-polyglossia texlive-svn-prov texlive-tools texlive-was texlive-xstring"
RDEPENDS_texlive-float = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-fmtcount = "texlive-amsmath texlive-base texlive-etoolbox texlive-graphics texlive-kpathsea texlive-latex"
RDEPENDS_texlive-fncychap = "texlive-base texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-fontawesome = "fontawesome-fonts texlive-base texlive-ifluatex texlive-ifxetex texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-fontbook = "texlive-base texlive-etoolbox texlive-fontspec texlive-kpathsea texlive-oberdiek texlive-xunicode"
RDEPENDS_texlive-fonts-tlwg = "texlive-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-fontspec = "texlive-base texlive-kastrup texlive-kpathsea texlive-l3kernel texlive-l3packages texlive-latex texlive-lm texlive-luaotfload texlive-xunicode"
RPM_SONAME_REQ_texlive-fontware = "ld-linux-aarch64.so.1 libc.so.6 libkpathsea.so.6 libm.so.6"
RDEPENDS_texlive-fontware = "glibc texlive-base texlive-kpathsea texlive-lib"
RDEPENDS_texlive-fontwrap = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-footmisc = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-fp = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-fpl = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-framed = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-garuda-c90 = "texlive-base texlive-fonts-tlwg texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-geometry = "texlive-base texlive-graphics texlive-ifxetex texlive-kpathsea texlive-oberdiek"
RDEPENDS_texlive-glyphlist = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-graphics = "texlive-base texlive-graphics-cfg texlive-kpathsea"
RDEPENDS_texlive-graphics-cfg = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-graphics-def = "texlive-base texlive-kpathsea"
RPM_SONAME_REQ_texlive-gsftopk = "ld-linux-aarch64.so.1 libc.so.6 libkpathsea.so.6"
RDEPENDS_texlive-gsftopk = "glibc texlive-base texlive-kpathsea texlive-lib"
RDEPENDS_texlive-helvetic = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-hyperref = "texlive-base texlive-graphics texlive-ifxetex texlive-kpathsea texlive-memoir texlive-oberdiek texlive-url texlive-zapfding"
RDEPENDS_texlive-hyph-utf8 = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-hyphen-base = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-hyphenat = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-ifetex = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-ifluatex = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-ifmtarg = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-ifoddpage = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-iftex = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-ifxetex = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-import = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-index = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-jadetex = "bash coreutils texlive-base texlive-kpathsea texlive-latex texlive-passivetex texlive-pdftex texlive-tetex texlive-tex"
RDEPENDS_texlive-jknapltx = "texlive-base texlive-graphics texlive-kpathsea texlive-latex"
RDEPENDS_texlive-kastrup = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-kerkis = "texlive-base texlive-kpathsea texlive-tetex texlive-txfonts"
RDEPENDS_texlive-knuth-lib = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-knuth-local = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-koma-script = "texlive-amsmath texlive-babelbib texlive-base texlive-booktabs texlive-etoolbox texlive-geometry texlive-graphics texlive-hyperref texlive-kpathsea texlive-latex texlive-listings texlive-marginnote texlive-mparhack texlive-oberdiek texlive-tools texlive-xcolor"
RPM_SONAME_REQ_texlive-kpathsea = "ld-linux-aarch64.so.1 libc.so.6 libkpathsea.so.6"
RDEPENDS_texlive-kpathsea = "bash coreutils glibc grep info texlive-lib"
RDEPENDS_texlive-l3experimental = "texlive-base texlive-kpathsea texlive-l3kernel texlive-l3packages"
RDEPENDS_texlive-l3kernel = "texlive-amsmath texlive-base texlive-booktabs texlive-colortbl texlive-csquotes texlive-enumitem texlive-etex-pkg texlive-fancyvrb texlive-graphics texlive-kpathsea texlive-latex texlive-lm texlive-oberdiek texlive-psnfss texlive-tools texlive-underscore"
RDEPENDS_texlive-l3packages = "texlive-amsmath texlive-base texlive-graphics texlive-kpathsea texlive-l3kernel"
RDEPENDS_texlive-lastpage = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-latex = "bash coreutils texlive-base texlive-hyperref texlive-kpathsea texlive-latex-fonts texlive-latexconfig texlive-luatex texlive-pdftex texlive-tetex texlive-tools texlive-url"
RDEPENDS_texlive-latex-fonts = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-latex2man = "bash info perl-interpreter perl-libs texlive-base texlive-fancyhdr texlive-kpathsea"
RDEPENDS_texlive-latexconfig = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-lettrine = "texlive-base texlive-graphics texlive-kpathsea"
RPM_SONAME_PROV_texlive-lib = "libkpathsea.so.6 libptexenc.so.1 libsynctex.so.1 libtexlua52.so.5 libtexlua53.so.5"
RPM_SONAME_REQ_texlive-lib = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libz.so.1"
RDEPENDS_texlive-lib = "glibc zlib"
RPM_SONAME_REQ_texlive-lib-devel = "libkpathsea.so.6 libptexenc.so.1 libsynctex.so.1 libtexlua52.so.5 libtexlua53.so.5"
RPROVIDES_texlive-lib-devel = "texlive-lib-dev (= 20180414)"
RDEPENDS_texlive-lib-devel = "pkgconf-pkg-config texlive-lib zlib-devel"
RDEPENDS_texlive-linegoal = "texlive-base texlive-etex-pkg texlive-kpathsea texlive-oberdiek"
RDEPENDS_texlive-lineno = "texlive-base texlive-finstrut texlive-kpathsea texlive-ltabptch texlive-tools"
RDEPENDS_texlive-listings = "texlive-algorithms texlive-base texlive-fancyvrb texlive-graphics texlive-hyperref texlive-kpathsea texlive-latex texlive-url"
RDEPENDS_texlive-lm = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-lm-math = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-ltabptch = "texlive-base texlive-kpathsea texlive-tools"
RDEPENDS_texlive-ltxmisc = "texlive-base texlive-beton texlive-euler texlive-graphics texlive-hyperref texlive-kpathsea texlive-latex texlive-natbib texlive-psnfss texlive-tools"
RDEPENDS_texlive-lua-alt-getopt = "perl-interpreter texlive-base texlive-kpathsea"
RDEPENDS_texlive-lualatex-math = "texlive-base texlive-etoolbox texlive-filehook texlive-kpathsea texlive-l3kernel texlive-luatexbase"
RDEPENDS_texlive-lualibs = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-luaotfload = "texlive-base texlive-kpathsea texlive-lua-alt-getopt texlive-lualibs texlive-luatex texlive-luatexbase"
RPM_SONAME_REQ_texlive-luatex = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgmp.so.10 libkpathsea.so.6 libm.so.6 libmpfr.so.4 libpng16.so.16 libpoppler.so.78 libstdc++.so.6 libtexlua52.so.5 libtexlua53.so.5 libz.so.1 libzzip-0.so.13"
RDEPENDS_texlive-luatex = "bash coreutils glibc gmp libgcc libpng libstdc++ mpfr poppler texlive-base texlive-cm texlive-etex texlive-graphics-def texlive-hyphen-base texlive-knuth-lib texlive-kpathsea texlive-lib texlive-plain texlive-tetex texlive-tex-ini-files texlive-unicode-data zlib zziplib"
RDEPENDS_texlive-luatex85 = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-luatexbase = "texlive-base texlive-etex-pkg texlive-ifluatex texlive-kpathsea"
RDEPENDS_texlive-makecmds = "texlive-base texlive-kpathsea"
RPM_SONAME_REQ_texlive-makeindex = "ld-linux-aarch64.so.1 libc.so.6 libkpathsea.so.6"
RDEPENDS_texlive-makeindex = "bash glibc texlive-base texlive-kpathsea texlive-lib"
RDEPENDS_texlive-manfnt-font = "texlive-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-marginnote = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-marvosym = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-mathpazo = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-mathspec = "texlive-amsmath texlive-base texlive-etoolbox texlive-fontspec texlive-ifxetex texlive-kpathsea texlive-mnsymbol texlive-xkeyval"
RDEPENDS_texlive-mathtools = "texlive-amsmath texlive-base texlive-graphics texlive-kpathsea texlive-tools"
RDEPENDS_texlive-mdwtools = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-memoir = "texlive-base texlive-etex-pkg texlive-ifetex texlive-ifluatex texlive-ifxetex texlive-kpathsea texlive-oberdiek"
RPM_SONAME_REQ_texlive-metafont = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXext.so.6 libc.so.6 libkpathsea.so.6 libm.so.6"
RDEPENDS_texlive-metafont = "bash coreutils glibc libICE libSM libX11 libXext texlive-base texlive-kpathsea texlive-lib texlive-tetex"
RDEPENDS_texlive-metalogo = "texlive-base texlive-fontspec texlive-graphics texlive-ifxetex texlive-kpathsea"
RPM_SONAME_REQ_texlive-metapost = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libgmp.so.10 libkpathsea.so.6 libm.so.6 libmpfr.so.4 libpixman-1.so.0 libpng16.so.16 libz.so.1"
RDEPENDS_texlive-metapost = "cairo glibc gmp libpng mpfr pixman texlive-base texlive-kpathsea texlive-lib texlive-tetex zlib"
RDEPENDS_texlive-mflogo = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-mflogo-font = "texlive-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-mfnfss = "texlive-base texlive-kpathsea"
RPM_SONAME_REQ_texlive-mfware = "ld-linux-aarch64.so.1 libc.so.6 libkpathsea.so.6 libm.so.6"
RDEPENDS_texlive-mfware = "glibc texlive-base texlive-kpathsea texlive-lib"
RDEPENDS_texlive-microtype = "texlive-base texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-mnsymbol = "texlive-amsfonts texlive-amsmath texlive-base texlive-kpathsea texlive-latex texlive-tetex"
RDEPENDS_texlive-mparhack = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-mptopdf = "bash coreutils perl-Getopt-Long perl-interpreter perl-libs texlive-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-ms = "texlive-base texlive-footmisc texlive-koma-script texlive-kpathsea texlive-latex texlive-tools"
RDEPENDS_texlive-multido = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-multirow = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-natbib = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-ncctools = "texlive-amsmath texlive-base texlive-bigfoot texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-ncntrsbk = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-needspace = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-norasi-c90 = "texlive-base texlive-fonts-tlwg texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-ntgclass = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-oberdiek = "perl-Digest-MD5 perl-Getopt-Long perl-interpreter perl-libs texlive-amsmath texlive-base texlive-carlisle texlive-etex-pkg texlive-fontspec texlive-fp texlive-graphics texlive-hyperref texlive-ifxetex texlive-index texlive-kpathsea texlive-latex texlive-listings texlive-ms texlive-parallel texlive-pgf texlive-qstest texlive-sauerj texlive-soul texlive-thumbpdf texlive-tools texlive-unicode-math"
RDEPENDS_texlive-overpic = "texlive-base texlive-eepic texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-palatino = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-paralist = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-parallel = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-parskip = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-passivetex = "texlive-amsfonts texlive-amsmath texlive-base texlive-graphics texlive-hyperref texlive-kpathsea texlive-latex texlive-marvosym texlive-psnfss texlive-tipa texlive-tools texlive-ulem texlive-url"
RDEPENDS_texlive-pdfpages = "texlive-base texlive-eso-pic texlive-graphics texlive-kpathsea texlive-latex texlive-ms texlive-tools"
RPM_SONAME_REQ_texlive-pdftex = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libkpathsea.so.6 libm.so.6 libpng16.so.16 libpoppler.so.78 libstdc++.so.6 libz.so.1"
RDEPENDS_texlive-pdftex = "bash coreutils glibc libgcc libpng libstdc++ perl-interpreter perl-libs poppler texlive-base texlive-cm texlive-etex texlive-graphics-def texlive-hyphen-base texlive-knuth-lib texlive-kpathsea texlive-lib texlive-plain texlive-tetex texlive-tex-ini-files zlib"
RDEPENDS_texlive-pgf = "texlive-base texlive-graphics texlive-kpathsea texlive-xcolor texlive-xkeyval"
RDEPENDS_texlive-philokalia = "texlive-base texlive-fontspec texlive-kpathsea texlive-lettrine texlive-xltxtra texlive-xunicode"
RDEPENDS_texlive-placeins = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-plain = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-polyglossia = "texlive-base texlive-bidi texlive-etoolbox texlive-fontspec texlive-kpathsea texlive-makecmds texlive-tools texlive-xkeyval"
RDEPENDS_texlive-powerdot = "texlive-amsfonts texlive-base texlive-enumitem texlive-geometry texlive-graphics texlive-hyperref texlive-ifxetex texlive-kpathsea texlive-psnfss texlive-pst-blur texlive-pst-grad texlive-pst-slpe texlive-pst-text texlive-pstricks texlive-tools texlive-type1cm texlive-xcolor texlive-xkeyval"
RDEPENDS_texlive-preprint = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-psfrag = "texlive-base texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-pslatex = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-psnfss = "texlive-base texlive-graphics texlive-hyphen-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-pspicture = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-pst-3d = "texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-pst-blur = "texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-pst-coil = "texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-pst-eps = "texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-pst-fill = "texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-pst-grad = "texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-pst-math = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-pst-node = "texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-pst-plot = "texlive-base texlive-kpathsea texlive-multido texlive-pstricks texlive-xkeyval"
RDEPENDS_texlive-pst-slpe = "texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-pst-text = "texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-pst-tree = "texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-pstricks = "texlive-amsfonts texlive-amsmath texlive-babel texlive-base texlive-bera texlive-booktabs texlive-breakurl texlive-caption texlive-chngcntr texlive-eso-pic texlive-fancyvrb texlive-filecontents texlive-footmisc texlive-graphics texlive-hyperref texlive-koma-script texlive-kpathsea texlive-latex texlive-ms texlive-multido texlive-oberdiek texlive-paralist texlive-pst-3d texlive-pst-coil texlive-pst-eps texlive-pst-fill texlive-pst-grad texlive-pst-node texlive-pst-plot texlive-pst-text texlive-pst-tree texlive-pstricks-add texlive-setspace texlive-showexpl texlive-subfig texlive-tools texlive-xcolor texlive-xkeyval"
RDEPENDS_texlive-pstricks-add = "texlive-base texlive-kpathsea texlive-multido texlive-pst-3d texlive-pst-math texlive-pst-node texlive-pst-plot texlive-pstricks"
RDEPENDS_texlive-ptext = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-pxfonts = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-qstest = "texlive-base texlive-kpathsea texlive-tools"
RDEPENDS_texlive-rcs = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-realscripts = "texlive-base texlive-fontspec texlive-kpathsea"
RDEPENDS_texlive-rsfs = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-sansmath = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-sauerj = "texlive-base texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-scheme-basic = "coreutils texlive-base texlive-collection-basic texlive-collection-latex"
RDEPENDS_texlive-section = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-sectsty = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-seminar = "bash texlive-base texlive-kpathsea texlive-pstricks"
RDEPENDS_texlive-sepnum = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-setspace = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-showexpl = "texlive-attachfile texlive-base texlive-graphics texlive-kpathsea texlive-latex texlive-listings texlive-tools texlive-varwidth"
RDEPENDS_texlive-soul = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-stmaryrd = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-subfig = "texlive-base texlive-caption texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-subfigure = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-svn-prov = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-symbol = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-t2 = "bash texlive-amsfonts texlive-base texlive-kpathsea texlive-tetex texlive-tools"
RDEPENDS_texlive-tabu = "texlive-base texlive-kpathsea texlive-linegoal texlive-tools texlive-varwidth"
RDEPENDS_texlive-tabulary = "texlive-base texlive-kpathsea texlive-tools"
RDEPENDS_texlive-tetex = "bash perl-Getopt-Long perl-PathTools perl-Pod-Usage perl-interpreter perl-libs texlive-base texlive-kpathsea texlive-texlive.infra"
RPM_SONAME_REQ_texlive-tex = "ld-linux-aarch64.so.1 libc.so.6 libkpathsea.so.6 libm.so.6"
RDEPENDS_texlive-tex = "bash coreutils glibc texlive-base texlive-cm texlive-hyphen-base texlive-knuth-lib texlive-kpathsea texlive-lib texlive-plain texlive-tetex"
RDEPENDS_texlive-tex-gyre = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-oberdiek texlive-tetex"
RDEPENDS_texlive-tex-gyre-math = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-tex-ini-files = "texlive-base texlive-kpathsea"
RPM_SONAME_REQ_texlive-tex4ht = "ld-linux-aarch64.so.1 libc.so.6 libkpathsea.so.6"
RDEPENDS_texlive-tex4ht = "bash glibc perl-interpreter perl-libs texlive-base texlive-kpathsea texlive-lib"
RDEPENDS_texlive-texconfig = "bash texlive-base texlive-kpathsea"
RDEPENDS_texlive-texlive-docindex = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-texlive-en = "bash info texlive-base texlive-kpathsea"
RDEPENDS_texlive-texlive-msg-translations = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-texlive-scripts = "texlive-base texlive-kpathsea texlive-luatex texlive-texlive.infra"
RDEPENDS_texlive-texlive.infra = "perl-Digest-MD5 perl-Exporter perl-File-Temp perl-Getopt-Long perl-PathTools perl-Text-Unidecode perl-XML-Parser perl-XML-XPath perl-constant perl-interpreter texlive-base texlive-kpathsea"
RDEPENDS_texlive-textcase = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-textpos = "texlive-base texlive-kpathsea texlive-ms"
RDEPENDS_texlive-threeparttable = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-thumbpdf = "ghostscript perl-Getopt-Long perl-interpreter perl-libs texlive-base texlive-ifluatex texlive-kpathsea"
RDEPENDS_texlive-times = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-tipa = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-latex texlive-tetex"
RDEPENDS_texlive-titlesec = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-titling = "coreutils texlive-base texlive-kpathsea"
RDEPENDS_texlive-tocloft = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-tools = "texlive-base texlive-graphics texlive-kpathsea"
RDEPENDS_texlive-trimspaces = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-txfonts = "texlive-base texlive-hyphen-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-type1cm = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-typehtml = "texlive-base texlive-kpathsea texlive-latex"
RDEPENDS_texlive-ucharclasses = "texlive-base texlive-ifxetex texlive-kpathsea"
RDEPENDS_texlive-ucs = "perl-Carp perl-Data-Dumper perl-Getopt-Long perl-IO perl-interpreter perl-libs texlive-base texlive-graphics texlive-hyperref texlive-kpathsea texlive-latex"
RDEPENDS_texlive-uhc = "texlive-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-ulem = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-underscore = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-unicode-data = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-unicode-math = "texlive-base texlive-filehook texlive-fontspec texlive-kpathsea texlive-l3kernel texlive-l3packages texlive-latex texlive-lualatex-math texlive-luaotfload texlive-luatexbase texlive-oberdiek"
RDEPENDS_texlive-unisugar = "texlive-base texlive-ifxetex texlive-kpathsea"
RDEPENDS_texlive-updmap-map = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-upquote = "texlive-base texlive-kpathsea texlive-latex"
RDEPENDS_texlive-url = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-utopia = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-varwidth = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-wadalab = "texlive-base texlive-kpathsea texlive-tetex"
RDEPENDS_texlive-was = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-wasy = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-wasy2-ps = "texlive-base texlive-kpathsea texlive-tetex texlive-wasy"
RDEPENDS_texlive-wasysym = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-wrapfig = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-xcolor = "texlive-base texlive-colortbl texlive-kpathsea texlive-oberdiek"
RPM_SONAME_REQ_texlive-xdvi = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXaw.so.7 libXext.so.6 libXi.so.6 libXmu.so.6 libXpm.so.4 libXt.so.6 libc.so.6 libfreetype.so.6 libkpathsea.so.6 libm.so.6"
RDEPENDS_texlive-xdvi = "bash freetype glibc libICE libSM libX11 libXaw libXext libXi libXmu libXpm libXt texlive-base texlive-kpathsea texlive-lib"
RDEPENDS_texlive-xecjk = "texlive-base texlive-cjk texlive-environ texlive-fontspec texlive-kpathsea texlive-l3kernel texlive-l3packages texlive-listings texlive-tools texlive-ulem texlive-xunicode"
RDEPENDS_texlive-xecolor = "texlive-base texlive-fontspec texlive-iftex texlive-kpathsea"
RDEPENDS_texlive-xecyr = "texlive-base texlive-kpathsea texlive-t2 texlive-xltxtra texlive-xunicode"
RDEPENDS_texlive-xeindex = "texlive-base texlive-kpathsea texlive-latex texlive-xesearch"
RDEPENDS_texlive-xepersian = "texlive-base texlive-bidi texlive-datetime texlive-fancybox texlive-fancyhdr texlive-fontspec texlive-geometry texlive-hyphenat texlive-ifxetex texlive-kpathsea texlive-lastpage texlive-latex texlive-ms texlive-multido texlive-preprint texlive-psnfss texlive-setspace texlive-textpos texlive-tools"
RDEPENDS_texlive-xesearch = "texlive-base texlive-kpathsea"
RPM_SONAME_REQ_texlive-xetex = "ld-linux-aarch64.so.1 libTECkit.so.0 libc.so.6 libdl.so.2 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgraphite2.so.3 libharfbuzz-icu.so.0 libharfbuzz.so.0 libicudata.so.60 libicui18n.so.60 libicuuc.so.60 libkpathsea.so.6 libm.so.6 libpaper.so.1 libpng16.so.16 libpoppler.so.78 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_texlive-xetex = "bash coreutils fontconfig freetype glibc graphite2 harfbuzz harfbuzz-icu libgcc libicu libpaper libpng libstdc++ perl-Digest-MD5 perl-Exporter perl-SelfLoader perl-constant perl-interpreter perl-libs poppler teckit texlive-base texlive-graphics-def texlive-kpathsea texlive-lib texlive-tetex texlive-texconfig texlive-xetexconfig zlib"
RDEPENDS_texlive-xetex-itrans = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-xetex-pstricks = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-xetex-tibetan = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-xetexconfig = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-xetexfontinfo = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-xifthen = "texlive-base texlive-etex-pkg texlive-ifmtarg texlive-kpathsea texlive-latex texlive-tools"
RDEPENDS_texlive-xkeyval = "texlive-base texlive-kpathsea texlive-tools"
RDEPENDS_texlive-xltxtra = "texlive-base texlive-fontspec texlive-ifluatex texlive-ifxetex texlive-kpathsea texlive-metalogo texlive-realscripts"
RDEPENDS_texlive-xmltex = "bash texlive-base texlive-kpathsea texlive-latex texlive-pdftex texlive-tetex texlive-tex texlive-xmltexconfig"
RDEPENDS_texlive-xmltexconfig = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-xstring = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-xtab = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-xunicode = "texlive-base texlive-graphics texlive-kpathsea texlive-tipa"
RDEPENDS_texlive-zapfchan = "texlive-base texlive-kpathsea"
RDEPENDS_texlive-zapfding = "texlive-base texlive-kpathsea"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-adjustbox-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ae-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-algorithms-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-amscls-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-amsfonts-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-amsmath-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-anyfontsize-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-anysize-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-appendix-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-arabxetex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-arphic-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-attachfile-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-avantgar-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-awesomebox-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-babel-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-babel-english-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-babelbib-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-base-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-beamer-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-bera-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-beton-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-bibtex-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-bibtopic-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-bidi-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-bigfoot-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-bookman-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-booktabs-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-breakurl-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-breqn-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-capt-of-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-caption-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-carlisle-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-changebar-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-changepage-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-charter-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-chngcntr-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-cite-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-cjk-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-classpack-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-cm-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-cm-lgc-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-cm-super-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-cmap-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-cmextra-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-cns-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-collectbox-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-collection-basic-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-collection-fontsrecommended-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-collection-htmlxml-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-collection-latex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-collection-latexrecommended-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-collection-xetex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-colortbl-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-courier-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-crop-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-csquotes-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ctable-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ctablestack-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-currfile-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-datetime-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-dvipdfmx-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-dvipng-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-dvips-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-dvisvgm-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ec-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-eepic-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-enctex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-enumitem-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-environ-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-epsf-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-epstopdf-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-eqparbox-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-eso-pic-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-etex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-etex-pkg-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-etoolbox-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-euenc-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-euler-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-euro-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-eurosym-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-extsizes-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fancybox-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fancyhdr-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fancyref-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fancyvrb-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-filecontents-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-filehook-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-finstrut-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fix2col-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fixlatvian-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-float-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fmtcount-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fncychap-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fontawesome-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fontbook-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fonts-tlwg-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fontspec-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fontware-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fontwrap-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-footmisc-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fp-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-fpl-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-framed-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-garuda-c90-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-geometry-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-glyphlist-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-graphics-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-graphics-cfg-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-graphics-def-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-gsftopk-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-helvetic-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-hyperref-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-hyph-utf8-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-hyphen-base-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-hyphenat-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ifetex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ifluatex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ifmtarg-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ifoddpage-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-iftex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ifxetex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-import-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-index-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-jadetex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-jknapltx-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-kastrup-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-kerkis-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-knuth-lib-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-knuth-local-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-koma-script-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-kpathsea-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-l3experimental-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-l3kernel-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-l3packages-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-lastpage-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-latex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-latex-fonts-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-latex2man-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-latexconfig-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-lettrine-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-lib-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-linegoal-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-lineno-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-listings-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-lm-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-lm-math-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ltabptch-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ltxmisc-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-lua-alt-getopt-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-lualatex-math-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-lualibs-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-luaotfload-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-luatex-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-luatex85-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-luatexbase-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-makecmds-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-makeindex-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-manfnt-font-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-marginnote-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-marvosym-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mathpazo-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mathspec-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mathtools-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mdwtools-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-memoir-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-metafont-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-metalogo-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-metapost-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mflogo-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mflogo-font-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mfnfss-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mfware-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-microtype-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mnsymbol-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mparhack-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-mptopdf-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ms-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-multido-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-multirow-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-natbib-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ncctools-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ncntrsbk-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-needspace-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-norasi-c90-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ntgclass-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-oberdiek-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-overpic-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-palatino-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-paralist-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-parallel-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-parskip-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-passivetex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pdfpages-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pdftex-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pgf-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-philokalia-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-placeins-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-plain-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-polyglossia-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-powerdot-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-preprint-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-psfrag-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pslatex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-psnfss-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pspicture-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-3d-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-blur-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-coil-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-eps-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-fill-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-grad-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-math-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-node-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-plot-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-slpe-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-text-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pst-tree-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pstricks-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pstricks-add-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ptext-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-pxfonts-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-qstest-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-rcs-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-realscripts-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-rsfs-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-sansmath-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-sauerj-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-scheme-basic-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-section-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-sectsty-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-seminar-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-sepnum-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-setspace-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-showexpl-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-soul-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-stmaryrd-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-subfig-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-subfigure-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-svn-prov-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-symbol-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-t2-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tabu-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tabulary-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tetex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tex-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tex-gyre-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tex-gyre-math-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tex-ini-files-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tex4ht-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-texconfig-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-texlive-common-doc-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-texlive-docindex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-texlive-en-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-texlive-msg-translations-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-texlive-scripts-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-texlive.infra-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-textcase-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-textpos-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-threeparttable-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-thumbpdf-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-times-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tipa-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-titlesec-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-titling-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tocloft-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-tools-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-trimspaces-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-txfonts-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-type1cm-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-typehtml-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ucharclasses-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ucs-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-uhc-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-ulem-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-underscore-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-unicode-data-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-unicode-math-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-unisugar-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-updmap-map-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-upquote-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-url-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-utopia-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-varwidth-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-wadalab-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-was-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-wasy-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-wasy2-ps-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-wasysym-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-wrapfig-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xcolor-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xdvi-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xecjk-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xecolor-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xecyr-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xeindex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xepersian-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xesearch-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xetex-20180414-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xetex-itrans-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xetex-pstricks-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xetex-tibetan-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xetexconfig-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xetexfontinfo-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xifthen-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xkeyval-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xltxtra-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xmltex-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xmltexconfig-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xstring-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xtab-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-xunicode-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-zapfchan-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/texlive-zapfding-20180414-14.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/texlive-lib-devel-20180414-14.el8.aarch64.rpm \
          "

SRC_URI[texlive.sha256sum] = "42a9fb283f066592074ce3503ad87ddf7c4aee7cde2119bf817d52d36a829751"
SRC_URI[texlive-adjustbox.sha256sum] = "d70b8ff11207208e7eec69e4a977b24ddec5db0651ba7a7c94796fc367179535"
SRC_URI[texlive-ae.sha256sum] = "5e8a26c9d4d9b82450f772691a2c6136b57da43171f4a282952b78a9b46b4a44"
SRC_URI[texlive-algorithms.sha256sum] = "2de74a0de496d3647d3b674c1297d882232690ae319b8a9668b060c9009ceefa"
SRC_URI[texlive-amscls.sha256sum] = "40a1902f161f2a8406995e65b02ee0e4ff20118e6dd9cd24658e3fa73263f9cb"
SRC_URI[texlive-amsfonts.sha256sum] = "47b78bf91ac4505a1a9472fdcef845c46d2513a242afe63709b06d89e8ea4ffa"
SRC_URI[texlive-amsmath.sha256sum] = "23f4db20641d25e0414ea19957b3b76182e64c4c25ec048702f371dc975d45cf"
SRC_URI[texlive-anyfontsize.sha256sum] = "45fb8f4005f182e102cdcfaae3155d44979ed06a86f694c4e4e36ca83127c9ac"
SRC_URI[texlive-anysize.sha256sum] = "c0c22fb3c5545d905a8819f8e8f0c1a788ad2fd39c0ae347eb5c3d0dffcf5599"
SRC_URI[texlive-appendix.sha256sum] = "728d449fdc777cffd564afdfec59c52b956972a237f581c803a1bf1cbcd64017"
SRC_URI[texlive-arabxetex.sha256sum] = "608bb220b978a00a8ddb086af675f3e42c52fa0b70a07134661d4e67a5c9e357"
SRC_URI[texlive-arphic.sha256sum] = "1b13736de9e3ea494cbc258d2f90b23eef62771e795cf15e2019e205a882f863"
SRC_URI[texlive-attachfile.sha256sum] = "1a652b1321569eeb86770c47c7ab136a463e4e24a5144c56c5fbf1668c441fab"
SRC_URI[texlive-avantgar.sha256sum] = "b2d70859dc1f923595ff842cc5b36009ef2d4aa8e8b070c0b485e636e4ffeaf8"
SRC_URI[texlive-awesomebox.sha256sum] = "0bf77ccb47dc58bc0ca2dff106ece055766fafc4f17813d27b168fc47601f79b"
SRC_URI[texlive-babel.sha256sum] = "9131f2ff1c7d0450c0132b161abce117c2df00776d911776a22132398a8c816f"
SRC_URI[texlive-babel-english.sha256sum] = "0c20a814a19e48fb5aeb218d6ea9c223a4111c93175e778650707856eedc1940"
SRC_URI[texlive-babelbib.sha256sum] = "f7e7ab2ae9edc62bdd66403e44dab4502612f8a8a5fc74a5dbc24645d6a46c0b"
SRC_URI[texlive-base.sha256sum] = "de37f040fcabed84b8999e88b4de6a2aaae59fdab7cc0fce3fc41c5938438e56"
SRC_URI[texlive-beamer.sha256sum] = "3c3721fb207858f0d32b41ae50eef9f9bb22d47882b7275ff0068abdc1bbde6e"
SRC_URI[texlive-bera.sha256sum] = "c8204aa9b12d532983ece069c6064cfe283d8f1ef2d0c89f2a0a4600294dbf86"
SRC_URI[texlive-beton.sha256sum] = "91357be4fbbeb3922df61edcfe7df87bc1505e01eb73b16538e2be1543a97325"
SRC_URI[texlive-bibtex.sha256sum] = "7efb56a63a2b9161e344a961ebe639d39fea730f52ef285ee68b9b5207156b8a"
SRC_URI[texlive-bibtopic.sha256sum] = "c48cb48232a9bb1cb4e13f6fd37f1f005a8855da4228f107f5afa68e51cdd5aa"
SRC_URI[texlive-bidi.sha256sum] = "d3e03dab185725bfc61b747e6f9db1d157162d74421613c58aa25ca29aceca7a"
SRC_URI[texlive-bigfoot.sha256sum] = "5c43db2d1e3be96f544c672ccd3fa88a14f9057f53809b3ec165310158c7ced1"
SRC_URI[texlive-bookman.sha256sum] = "8734d007be763b85429572d3095017962dee8fe17abec9a445a24965d8ee37bc"
SRC_URI[texlive-booktabs.sha256sum] = "6cae3f23774ab0b703dad44573ff23863ad5dc42ab1be1238bb8577d1b9cbcf5"
SRC_URI[texlive-breakurl.sha256sum] = "2d6ab0113636e6403c2a8e97223275cb2668be5afbf7b8523928fdb68e3cecc7"
SRC_URI[texlive-breqn.sha256sum] = "53b156af2b3d94cf907c230fc72f678d2c09435d4d6c4f661388d1014563bb5d"
SRC_URI[texlive-capt-of.sha256sum] = "0971e6a17d3dae04eb0ff26e385bd71665f96b0542b60d3c5cb4b6e55abd2f2f"
SRC_URI[texlive-caption.sha256sum] = "4008dffa4b1fde2293812c2a6df56d21d893f9927d14ba893ff7ff6c3e6fb245"
SRC_URI[texlive-carlisle.sha256sum] = "4a3360282b7273c8985ae6f9ed48bed0d5038d74790daedb47eb4de5e1bd63e0"
SRC_URI[texlive-changebar.sha256sum] = "c851b838dd58a71a2171ce163fa0e6201c9d31d70fac2854808362e9347655fa"
SRC_URI[texlive-changepage.sha256sum] = "739a013f34e1775252a8fa89342727bb6e4469c8d240650cd93b0ef406436ebd"
SRC_URI[texlive-charter.sha256sum] = "6af4f77eae59f65ddcc20340751a37fd427f39720978033caa714f756b95fcbf"
SRC_URI[texlive-chngcntr.sha256sum] = "6e9f301d159a49662c93ef2cdfc62dec19a5970a485cdfeddb6513f19f6fc53f"
SRC_URI[texlive-cite.sha256sum] = "dfa33d9720598ded06a89d55681eb802884c08facdec86e8637d4a5feb2dc1a3"
SRC_URI[texlive-cjk.sha256sum] = "067c562afa616d3a5b8f7b2652d9a1dc0ea860885bb6660e006cbc7f1462baab"
SRC_URI[texlive-classpack.sha256sum] = "e6670da70c07b9103be64491c6c5eb7be243b75719a5b57d55b7e4b90f6527ef"
SRC_URI[texlive-cm.sha256sum] = "954c1c47eb10b3079f9897ec160dbfda89b4b3204fd989a985de01e9fdc9e282"
SRC_URI[texlive-cm-lgc.sha256sum] = "1799fb87ea19db78ec4219d7cce0a4c680bc4e6704c4955ecffb72555ba64765"
SRC_URI[texlive-cm-super.sha256sum] = "373b468039b9f8ceed6b87c8ca8c23337dcd0684f55bcde26fbe813c27cbb656"
SRC_URI[texlive-cmap.sha256sum] = "3d470a07ad5a03452cd032932c1547be35693058b8ce7c116959740e64a597bd"
SRC_URI[texlive-cmextra.sha256sum] = "f91a0fa2786c0ab32f963b76ab9f4e0c0ec1c5c8f3829da13f54cf0ca44030b1"
SRC_URI[texlive-cns.sha256sum] = "69d8462373b06348a66e8f93a5c59d816dade5419333f11d67e6d374aa06ae43"
SRC_URI[texlive-collectbox.sha256sum] = "8c975c13638e49c68c04e6645f63cb759edc1e96bc23a1bd221a1d2e4c703a14"
SRC_URI[texlive-collection-basic.sha256sum] = "4d7ef381bb1afb9fe77ca7d77fcfe1ee1c5bdcff2f07a518a1cdb3da926df723"
SRC_URI[texlive-collection-fontsrecommended.sha256sum] = "b3a9ae4a90c529eb0332c963e2b573d0b82fc1c4477df1308a4cfc10050f8be5"
SRC_URI[texlive-collection-htmlxml.sha256sum] = "203264013af841031143be79c4fd85f9e1b25481a04aacc30006adb35b550ee5"
SRC_URI[texlive-collection-latex.sha256sum] = "f5aa7a3887d5a20b498622e1d2e48fd3ebea8fc4364560d12efce6f380513454"
SRC_URI[texlive-collection-latexrecommended.sha256sum] = "8c68edf2629d7a774e6b3b28c3486d6ea3640a476b2358e3786302e351867f27"
SRC_URI[texlive-collection-xetex.sha256sum] = "d01161959b229cacccc9e555723df750155f2f1bbccc551dc4696442ac4f582e"
SRC_URI[texlive-colortbl.sha256sum] = "24fd9851b055a117b157e0b3ce3b46550b1a828b33e598d41402e71161ae53c0"
SRC_URI[texlive-courier.sha256sum] = "81276390537a8211b14dd3e5fdd3b60e979021364743d393cef4aa5fc523f006"
SRC_URI[texlive-crop.sha256sum] = "4b261366626a8f4118e8f44d2d864e3049b831e7c5d326407215aecaaec0d2b7"
SRC_URI[texlive-csquotes.sha256sum] = "3f3504cd60562e00c0e410a1c01624a123a3383908ff7a425c1b61dde180c8bd"
SRC_URI[texlive-ctable.sha256sum] = "0c72f3b26223e0308577330649b7f5d8adf967dd35db3655f3f1e350bdd59df8"
SRC_URI[texlive-ctablestack.sha256sum] = "c071c645e686d7576011574b61cbbef588486f04b21290cc57200eada952a6b0"
SRC_URI[texlive-currfile.sha256sum] = "16afa54a91acfeb30a5742bed8b3e9f96bc7972fedb38e8fd43048f8a19075c7"
SRC_URI[texlive-datetime.sha256sum] = "73cf740e66f45a0e4fb0e07f5729855a44845a8f81725418e889eec7e1ca7aa8"
SRC_URI[texlive-dvipdfmx.sha256sum] = "5ae6175686d4038f384d74b115d12ce5fe64a930847de4692d2e691a11b30416"
SRC_URI[texlive-dvipng.sha256sum] = "c9c1dd54c792bb133b22d585981334ddb461a97cdf54d2011795994f340f355f"
SRC_URI[texlive-dvips.sha256sum] = "a3e9d20838371fc673a349706fdb6acac512168792f2d8022ffc2d653c7e8d53"
SRC_URI[texlive-dvisvgm.sha256sum] = "2282ac329e950236b1a0c7186c9999cd3864fcfa552274fc26be41bf60ce4520"
SRC_URI[texlive-ec.sha256sum] = "94f7e687f0ead309d20a7a7841eef4fbe87ee236118e6bd1c4bbfae8b18a8a66"
SRC_URI[texlive-eepic.sha256sum] = "60728f155c6fb8475be5db52d38dc1d3a5e7226c436e0c7235c6bdc14f901333"
SRC_URI[texlive-enctex.sha256sum] = "217987d4bcb6587fa3e43e825d2ea41d5cf4d3819e6dc184feb50564d81f082c"
SRC_URI[texlive-enumitem.sha256sum] = "870f9035d94025870585470c2499996f103ee4720f7f42d2dcb82860d66351eb"
SRC_URI[texlive-environ.sha256sum] = "a5f7d527b1fb494c6e80bc4815481b5658c02ab1afd5cfe3a5099939efccf694"
SRC_URI[texlive-epsf.sha256sum] = "ee3b82af2574b082cc5ac35e2f14cd3fbb13305d83bdda16d0a76a59bf76035d"
SRC_URI[texlive-epstopdf.sha256sum] = "ae091686513c559befd05012c257cad63c4efaa373dc9204207ac1b563f7ea07"
SRC_URI[texlive-eqparbox.sha256sum] = "b60bbbcda795ac0c7dae7df9963ec8ed0b8f5e19dc72b332f2296acf3f40d9bc"
SRC_URI[texlive-eso-pic.sha256sum] = "be5330b2220698ef0ccb9485cc94c29594e69bb67b3303153bc8eefa8cda7c6d"
SRC_URI[texlive-etex.sha256sum] = "876372b902529634921da5ec8956c7c2531c719b983a9c99b4f21d2bf184ff85"
SRC_URI[texlive-etex-pkg.sha256sum] = "477f9f52cd1ba0f6fc0868c14875ab65faf87f379e3d9ef6321d9a30682d4e36"
SRC_URI[texlive-etoolbox.sha256sum] = "840124b4e390709a0a1627baf787001e330de9145c24fa8244a7322adc7608fa"
SRC_URI[texlive-euenc.sha256sum] = "3334f94d81b97a09ca1ea3407399c9902c9c13eca404382b05c534f12a7c1774"
SRC_URI[texlive-euler.sha256sum] = "14eda368ec3f9d90d66d0264d159ea99462f37c2505566cfbedf409ddd87925b"
SRC_URI[texlive-euro.sha256sum] = "a0b1557f05bdd2ec35ec03bf9d6a50754f93d4b539417d29526a5227e44c2b17"
SRC_URI[texlive-eurosym.sha256sum] = "28e575accb324ee00b2596d8fdf0b91ba969f2d7ebc51b9936514b6af772d247"
SRC_URI[texlive-extsizes.sha256sum] = "7182ef16352af37e913dbee35ecd13115be035863f5d1be409d01d347081216c"
SRC_URI[texlive-fancybox.sha256sum] = "4e3600880f27dde36e16964950d1a2a1e4ab8ca59b99ed5d779eb0e1bcb4d306"
SRC_URI[texlive-fancyhdr.sha256sum] = "d9ccb4d7dc09acff9216ab057aea3efd30c84050cdc401ad659e8ef9f93536ad"
SRC_URI[texlive-fancyref.sha256sum] = "3ece4a959a6f377d9700288686a304556def91192f48fc59f19cf9bfb486154f"
SRC_URI[texlive-fancyvrb.sha256sum] = "fa9a332dcfa92cca2f9605263b618c8ee43e0188ccb544d0a8ac507f1f90694f"
SRC_URI[texlive-filecontents.sha256sum] = "d7cc3cf8a746dd8cacebb08c4a0d0d3358fb4aab58c55725c4f603ecc349246e"
SRC_URI[texlive-filehook.sha256sum] = "a6d476e338bfa2ee339074bf8ab4f9106ea335f8bea1493ae6a8f1cc1e95fbb9"
SRC_URI[texlive-finstrut.sha256sum] = "103bfdf0b4f7d728d0ce22184c55f6d622775edd4459f07bb0545ff1b5ddc5b7"
SRC_URI[texlive-fix2col.sha256sum] = "bc3643bfc3e02183c1610ddfec045fc8e4f38879f80d1649e28391b4a23489cf"
SRC_URI[texlive-fixlatvian.sha256sum] = "7430ddb0145642a257d2dbd42fe1654ee83063708501a5761ad13bbd6000a7e0"
SRC_URI[texlive-float.sha256sum] = "d4f9f71f4948c2e8ed58815a8416319273541a2a9cd3f1488abb5b44df8497c1"
SRC_URI[texlive-fmtcount.sha256sum] = "984f6bc924000f8bcf48403c3b488dcc1ea4bbeaef4fa863121031ceae2ffea9"
SRC_URI[texlive-fncychap.sha256sum] = "40dc1aa114af8f1f0a0bc6c4fd42d48147cfaf7a4efd38d266272f112fe6af62"
SRC_URI[texlive-fontawesome.sha256sum] = "0544adad1c4b33c40ae47d535c6e8846aeb462dd7e558b0972a3ff0314578352"
SRC_URI[texlive-fontbook.sha256sum] = "7a6c412699228cde9f78f02b063b87682dc68d733314a3616ac5dc82ee02cccc"
SRC_URI[texlive-fonts-tlwg.sha256sum] = "91974e911448def584c050e19c169bf1038d079c0409754e6b0ae918a94715f0"
SRC_URI[texlive-fontspec.sha256sum] = "7249c18e8fd5cd620df696abaaa8475043d72dbea6d43f9001ea4a70a5ddb605"
SRC_URI[texlive-fontware.sha256sum] = "001fa79e5147372d2b15e96897b77a92b9e0737e8fe458aa4c051a27cae60ce4"
SRC_URI[texlive-fontwrap.sha256sum] = "3ecac98365fbd2f488d897ed2f9ee23b995cfc50955d1ba6487bf5886ddf7831"
SRC_URI[texlive-footmisc.sha256sum] = "bc96e70c78e71337d32ddbc08794e6508f4206974973ab443dfb258c9fd67cca"
SRC_URI[texlive-fp.sha256sum] = "24a121971c86d548734a3cbacc7f2db76bff4f3698387f9543b285123154e4b6"
SRC_URI[texlive-fpl.sha256sum] = "9766f09024f86b84ae1f59b03fbfddbc54bb3da65e1911af22f0233f11adf472"
SRC_URI[texlive-framed.sha256sum] = "08823363b5643943b473f61e19bd47ef8c2c082533b73a1f5f311c630d5f8e50"
SRC_URI[texlive-garuda-c90.sha256sum] = "c5bfffb8f0441c45bb850e80f73198baf58da388fe55d9d6e4af26ed162ae5db"
SRC_URI[texlive-geometry.sha256sum] = "76a21e99e4b096873765c93a978eace59402cc4762221dc2978954eb9eccaba4"
SRC_URI[texlive-glyphlist.sha256sum] = "42d6b4ecc2f6e84ac622065ef952ac313acea27733add3bf56bff7e312062821"
SRC_URI[texlive-graphics.sha256sum] = "351897769a2444df470778904f3438c5cfab428dbabed03ea0c5308c8812c832"
SRC_URI[texlive-graphics-cfg.sha256sum] = "35dd371703af8b043e6a311254dc5e0f31258421a94b620be4df575b23af027d"
SRC_URI[texlive-graphics-def.sha256sum] = "8cb915dd98079d9ffb6613cb8cb378b4eb7ae7baefbc018bea1d7d5d2c240b62"
SRC_URI[texlive-gsftopk.sha256sum] = "554b730037c15789f7bca9cf6284678b3af7aacb788b835d109597f70f4b1fd5"
SRC_URI[texlive-helvetic.sha256sum] = "9017ed8d7a81c78392de6c1eebe6c8cd3d21ef9f2af628606edb0bb9161726da"
SRC_URI[texlive-hyperref.sha256sum] = "a24872947c1425979d4fe21db24dc59797294eafc2d1aff5336bf52f9e6ddc4c"
SRC_URI[texlive-hyph-utf8.sha256sum] = "a13672564ba077379ae693f93eae76e1d864657d6975e0ed0275acccb7e53ad2"
SRC_URI[texlive-hyphen-base.sha256sum] = "85db01817bdb15afafadeaaf966c5896676e35d09fe8d80acae6a22b1020c2ca"
SRC_URI[texlive-hyphenat.sha256sum] = "ac7c2a5a71bcba96bc4f95109cc58e292764f14d5b1113a6c15f36fa0d69098c"
SRC_URI[texlive-ifetex.sha256sum] = "8dd622496a5f44195818383a9cf6ff334d9af99d6e483358b3b0f8195ebc2e0f"
SRC_URI[texlive-ifluatex.sha256sum] = "3fd8dca933d195dc2f52bcd3ddbd184489d727951f220ad8cb8e9583114fb7f3"
SRC_URI[texlive-ifmtarg.sha256sum] = "557bcc598324e6be84b13d1aaaa60ce761d077ecf51b5bcd898900dd69888686"
SRC_URI[texlive-ifoddpage.sha256sum] = "923cfd74e26797e5d27abc5ce25a3e57909bedf21847bc71c02ffa4741087b90"
SRC_URI[texlive-iftex.sha256sum] = "2de3630e29eb7fd24812802837a896f9e13b9b21e122846e579c584dcf01e4cb"
SRC_URI[texlive-ifxetex.sha256sum] = "ee10b88d87bfc0f161a03eecc0d66a6fe0e9813884bf5c9c900e152f5f35a751"
SRC_URI[texlive-import.sha256sum] = "87af7ec2b823d0e442e61d737caff8a99e7c1f7ca4d5240b8500bd678ae04454"
SRC_URI[texlive-index.sha256sum] = "804308d81832fd71a9ffd846b426b2c686e5f3bb7b53f3aa01d5d47670af48ed"
SRC_URI[texlive-jadetex.sha256sum] = "774752d3f1912d7b82e6ce6848fa276b7b936404045275462382ce5178cee3d3"
SRC_URI[texlive-jknapltx.sha256sum] = "4d932a1b2b97d94362ad333e87f723caa3ecb653a2a16dd97f0ebf1b5e34e32e"
SRC_URI[texlive-kastrup.sha256sum] = "86e77caf6d14a3b10003337303b649bb335c1cedb308dd0b33b3702e46cf7612"
SRC_URI[texlive-kerkis.sha256sum] = "6064dd4d7a179129a0e04ab8fc1b6a886e4fbd43cfe22f27f4e1cd5dcb1e9df0"
SRC_URI[texlive-knuth-lib.sha256sum] = "4c04f32737935ff172d8f38d3cda5613213f46769f401e226296b6c7c82c0f1f"
SRC_URI[texlive-knuth-local.sha256sum] = "27943f9922d0c7f3acf6ece6f649f4a615c0cc4be86653cd6535468c0272d752"
SRC_URI[texlive-koma-script.sha256sum] = "b84c646cb6cd6e0152df641976de29b29e1798c26b1a9fec77442d23c8388ebe"
SRC_URI[texlive-kpathsea.sha256sum] = "f34d5a897cb314701db9033e4fbea62ad17af9cf0c072119a16d83f12a16bfbc"
SRC_URI[texlive-l3experimental.sha256sum] = "2686b0a34e08a669df871027373ee1680c6ea66d592abd632b4d7f34587338bc"
SRC_URI[texlive-l3kernel.sha256sum] = "37d93e40452ac699ab4774713238e3c125e2bb452b7ecc52b8acaf836d79e3b5"
SRC_URI[texlive-l3packages.sha256sum] = "a267f9619f7df767c8040f5314fe49ed6df7448af0baaeb01af30c938fd341ff"
SRC_URI[texlive-lastpage.sha256sum] = "f206097713b41c370b7c5c45c8ffa3691a819636d8e727867cbee5c7c2125dbe"
SRC_URI[texlive-latex.sha256sum] = "7fc24ff137d3d1a1799832db9a9f81a16abf64d0ace80b153bfad6f8bb20c57a"
SRC_URI[texlive-latex-fonts.sha256sum] = "bd56d362abf41f458c9140a1e9a664ba3a40a684d6867a30e742e4883408a8ac"
SRC_URI[texlive-latex2man.sha256sum] = "45eee10218e342d4ee8bce20119f4c74fe2a31af3ba075e41410d35dc4c339df"
SRC_URI[texlive-latexconfig.sha256sum] = "0fed743fb8b38e2325501395135c36a253bbd8f298636d67fe10f06f22499ffa"
SRC_URI[texlive-lettrine.sha256sum] = "e96abab2eb9406276bcc3b56acf74a35f76e3090861cdbb0dacac232422bfae6"
SRC_URI[texlive-lib.sha256sum] = "6298d52c07653407ee2470ccbce0b73eff3e2b050f5bacc9c628f8638217e661"
SRC_URI[texlive-lib-devel.sha256sum] = "7fb2c17296268b9e89284a213b4aadfdc7fd4a932de9ccf51643f2b0a75835bc"
SRC_URI[texlive-linegoal.sha256sum] = "dfa1b69b3801e65b40d52de42752c01e2f7f814d784f6f2d82d0a2b315b57016"
SRC_URI[texlive-lineno.sha256sum] = "6b406a94e55c42861a766e8364aa4d6d9f4cc700a50ca68dfff660cd1d4ece99"
SRC_URI[texlive-listings.sha256sum] = "0c4a211f926df5da5a80a4f8d26a0d09c9c583425f0df4f298a751b6f4511019"
SRC_URI[texlive-lm.sha256sum] = "28a536be11bc5c8b438703cdb30642d7e142e1bd497985e7e2b50c377c94c320"
SRC_URI[texlive-lm-math.sha256sum] = "d82cb5c7b919c150391d030aa991c873513854a160f819dbdbdb9e8e0ba1a0ec"
SRC_URI[texlive-ltabptch.sha256sum] = "e977f1fa65cde0c4e394b1ee21c31aea3a70dba9b5760e838d77ba5ae0118793"
SRC_URI[texlive-ltxmisc.sha256sum] = "b5bf15b81559e6fc3932fa5f00093a16175f12c4fbad72e165c6fe2fe723b3f8"
SRC_URI[texlive-lua-alt-getopt.sha256sum] = "636d09333cbe3a7c815719ad6699553230e76611fa7778d9fe2e75eb62fa9f51"
SRC_URI[texlive-lualatex-math.sha256sum] = "9d018af86b02259df4cd45534360005f78320481cd4af57bd7ff09aa441f9801"
SRC_URI[texlive-lualibs.sha256sum] = "31e30f6229b083e0be475d3cdc15e39cf543a46d5c8ca50f2f2cfbf867b6f6e8"
SRC_URI[texlive-luaotfload.sha256sum] = "ec61c4ec6ee710fd3919b2fbd60d1975d7df54e69601fa0a089732189688eece"
SRC_URI[texlive-luatex.sha256sum] = "b1b801908e7d0829da28a6cfa220cee8cc577b370ea8a382eb80145f7fa2a939"
SRC_URI[texlive-luatex85.sha256sum] = "301027959e52de38c500741a92df2c58dee3600827357421e8fab11f5e8eb889"
SRC_URI[texlive-luatexbase.sha256sum] = "46e3174214f6f2bdcc90d811af5be64bfc2d7f7e6649fada47a4dba53340cab0"
SRC_URI[texlive-makecmds.sha256sum] = "ec8fb954feca06368deb665542109e7a1f6f41e5973225c11011fd05e8c145ca"
SRC_URI[texlive-makeindex.sha256sum] = "a8214498ff6fbd7d6b28e26e7de9141c286307eb6a3114c38fc49498f009a403"
SRC_URI[texlive-manfnt-font.sha256sum] = "0e4da5d6f64a04ec38830318d7df0c34caae480be97791d1e58573d2210897ab"
SRC_URI[texlive-marginnote.sha256sum] = "ead74cf39f4b8bdded067dc3d58f8ccd966910341b0b69ab1e45e886092baadd"
SRC_URI[texlive-marvosym.sha256sum] = "16580ad8886faec47f4a2498f289eba60960d099bdf0e50fdc5fa5fe40297d89"
SRC_URI[texlive-mathpazo.sha256sum] = "eaa8fad908263ee83e0495a76c18b68574edcd13649a18d37c61cd8b30d8fe0e"
SRC_URI[texlive-mathspec.sha256sum] = "432e16410881af9e309164a0c100ced97384719bdedc85686985b29df62743d8"
SRC_URI[texlive-mathtools.sha256sum] = "66e185467cf0e9609c6933ce7a83cec5a5d197f95fb9607d4e69db9aae2529ba"
SRC_URI[texlive-mdwtools.sha256sum] = "b1eff3cbcfa0370601418e6721a5f7b8e79f45a43dfa5f92585ee63a0d6f9cba"
SRC_URI[texlive-memoir.sha256sum] = "f2a704da1c7102b798cb4403a3d0c4357a35e0cc47be6a6802d81b4fcb7500dc"
SRC_URI[texlive-metafont.sha256sum] = "8badc0af49dc0da72c169055965ee3bf06704b130a55536133f39460337288b4"
SRC_URI[texlive-metalogo.sha256sum] = "7e956ca819c00f8efba632e7f773da3c6291967dfaea2850dbedf46ab7c6677e"
SRC_URI[texlive-metapost.sha256sum] = "243fe7230741c18085f6d2b9e89843a22a3314d08dc99afe080ac5d72c792d2f"
SRC_URI[texlive-mflogo.sha256sum] = "1e0c7a851740784f760d63bfb934185eb804ce38286747548dfeaa7eba131e99"
SRC_URI[texlive-mflogo-font.sha256sum] = "ab82cecc0720391a0544ee983289a4a65dd60c820e1b4d86d8f850d15516f68f"
SRC_URI[texlive-mfnfss.sha256sum] = "b247478627b917407bb7d1400b267ea88ecbc78bbaec871745858cbddbdced89"
SRC_URI[texlive-mfware.sha256sum] = "42664b92008f4f9cbd879a42595c7976dd07f049ab4e5a8e312b57020d76cdaf"
SRC_URI[texlive-microtype.sha256sum] = "9d6e9a29802b9d667253ca913dbc4b5abd42278f351a40db1a96a4fe8f60d965"
SRC_URI[texlive-mnsymbol.sha256sum] = "1f39c76bc60a6ae7a61b9ebe133e74d944b1b88c41d0a9096ede90e86f3c908b"
SRC_URI[texlive-mparhack.sha256sum] = "0881d243af9f8e6439efce8e023e529b191b2a8e78ff4ac998ea764437ad7a09"
SRC_URI[texlive-mptopdf.sha256sum] = "7c11d0ee689cd10e43b9499cc3261e4395b8666ee2af38c11444a27984e279d0"
SRC_URI[texlive-ms.sha256sum] = "a8e4c5436c67946d2cecff1f2487eb7942f5e7f5ef21ff4bd17e899f6ba8acbd"
SRC_URI[texlive-multido.sha256sum] = "ab69622fc469e0bfac642d6df85c774c4ecef761cee175dd060ef284fcc4f6fb"
SRC_URI[texlive-multirow.sha256sum] = "83063a1d820c3222498e42fde2332fffd07cb21cccfc996153c0d13953ca2b67"
SRC_URI[texlive-natbib.sha256sum] = "1f21b3b20d7147b366f9b69d39ff8faa45806f461353cf5fc2d1ee344d4cb5bb"
SRC_URI[texlive-ncctools.sha256sum] = "42cdf81afb35e017ce403371d690166678eed0ff2573ba27404f44600af8edde"
SRC_URI[texlive-ncntrsbk.sha256sum] = "5b87d45a2698a745de201d4eb0c24d8d0ca2ba8e9a3ca86af6f495d003913741"
SRC_URI[texlive-needspace.sha256sum] = "1d6ae791724ceb85c19757c28e7ddfc649bff633ff19a5c14cdd6de18622898c"
SRC_URI[texlive-norasi-c90.sha256sum] = "3709a00a02c1ab1f7bbbf5a04765fd676a9272e2da475601c96d368aa963054c"
SRC_URI[texlive-ntgclass.sha256sum] = "2a7f4684467d0eacc675f5103f462ff043980d5f7302dc53852746ba5cce4086"
SRC_URI[texlive-oberdiek.sha256sum] = "28d8411dbbaabe01a2655f3d633acb82ec685ae932d4682a1ca8b7eb44df65ac"
SRC_URI[texlive-overpic.sha256sum] = "a9c64637b9f1fde4d609fba9d9810ca49875b478bf11b5588f17cd505d1bcc8e"
SRC_URI[texlive-palatino.sha256sum] = "7ef215df7ea5fc6e6a6936bfb939218836c0aa9c4b09ea0455aff322b26ab29a"
SRC_URI[texlive-paralist.sha256sum] = "d4c20a3b694908774b6c65341092b7230d9f3812c7f36435402e254cfb01ae10"
SRC_URI[texlive-parallel.sha256sum] = "6bb6f44cae4175e9f969d4e3d6de76e6e406a2c34e78deeaec83548f263a8342"
SRC_URI[texlive-parskip.sha256sum] = "cfe70ec7869baa21d04a5b121f0ace8e3d6b220b8697f4b43cf5d24705ee344b"
SRC_URI[texlive-passivetex.sha256sum] = "6739c9795052d9120464c4259697e36c8619c287f72b68d8cdc5951980b86dd0"
SRC_URI[texlive-pdfpages.sha256sum] = "d882b669e92d031c54f6a6fb2c22325bf7e6e3103e117358ef84504194ef06a2"
SRC_URI[texlive-pdftex.sha256sum] = "debdbda9bae0f36ce1d529c0a71b89221d7f897b9ad7581e244a32500f978025"
SRC_URI[texlive-pgf.sha256sum] = "3e9c42d1f8e96f954481a2b5df44e31e9bfb5708077f44b388cee26d61132f4b"
SRC_URI[texlive-philokalia.sha256sum] = "10f65f55b76cbdbb2254b3c95bef885432790168ed5207805293e16e13594174"
SRC_URI[texlive-placeins.sha256sum] = "09836d4ffed003f127a768bcb2a09600ff7fc649b0b8a38a015600a12b9431ee"
SRC_URI[texlive-plain.sha256sum] = "ee2fecdfe68f180564ec4a1bb4d11d5aeacf42c8381aeb9516b25146f31ac656"
SRC_URI[texlive-polyglossia.sha256sum] = "6746a8522fcbf89a39819c55c75309ec367e53d90e2b0b0415b6ba42fb7301bd"
SRC_URI[texlive-powerdot.sha256sum] = "18ab9c0f6d81e7d553550e71fe76316ae67dda3244518ff9d26c9ea98b0733c2"
SRC_URI[texlive-preprint.sha256sum] = "3f35aedfa1be9509f231b005b83555d49331298d1eb2445ae6a1cdefffce6901"
SRC_URI[texlive-psfrag.sha256sum] = "35f87748e41aca4f352bfe875db239d2c49376d06c6d3cfb266f5b5606b9254c"
SRC_URI[texlive-pslatex.sha256sum] = "4dd9c8287bfa10a70d709a67a265f256ef22fce050d6f641adfbabdc8da9fe0a"
SRC_URI[texlive-psnfss.sha256sum] = "e8d1546768f3462bdce561cdd34252a986d9bddfb50cb28fe011f7a43cfb7f7e"
SRC_URI[texlive-pspicture.sha256sum] = "594a39bcd66a7421c2221aaf9218a6367e385246440f530d5359afbad52891ad"
SRC_URI[texlive-pst-3d.sha256sum] = "d65b3ab23895eb6b86ade6c77dd081cf79a783d7b94a99330754c1dba081075c"
SRC_URI[texlive-pst-blur.sha256sum] = "d164fbbe0307a513e9b61aaff343f5c8aaa5e38c9bd8ed7718cd49b1be550a5a"
SRC_URI[texlive-pst-coil.sha256sum] = "71f98ddb21cb1b0e5b3bd37781a2f850efbe0e5cd307a56957080f313a634e5c"
SRC_URI[texlive-pst-eps.sha256sum] = "65540e0bf768e6826cac6f846b6e954dad4af7eb2f1ea20e27b9414863d5f89d"
SRC_URI[texlive-pst-fill.sha256sum] = "4d45ef0773fddc3d4bc9d3eb592eecea89808ee3ddf6e5235a468f1ecd4233b5"
SRC_URI[texlive-pst-grad.sha256sum] = "4fec4e5ec09c197f7a4b3a6c3f2900cc386f8ec5f6b67a0fec991000e90f8f1c"
SRC_URI[texlive-pst-math.sha256sum] = "39f95426cef8381b8235af2f7c0823140bcfe22453eb643acf3d5a60d7ee57e1"
SRC_URI[texlive-pst-node.sha256sum] = "d8b633cd53736758008b68bd9b7e8afe3ac2c21389edd61404d14bee4f883b40"
SRC_URI[texlive-pst-plot.sha256sum] = "c328d27b6ed29b439b7387d222dc6300d12018099ccba580a68b46469bcaf006"
SRC_URI[texlive-pst-slpe.sha256sum] = "cfd03e86ea7e9e7da0782d5dad43d17dd2187484e90c21486a4f3ee23d4a8e08"
SRC_URI[texlive-pst-text.sha256sum] = "a13bbb669da61146c7f0031cfde04ef616aa8b1cc7608d521c49cc5747c309f6"
SRC_URI[texlive-pst-tree.sha256sum] = "0f4da44c987c77ce77cc75a3b2fcdf74f93d98a4a29533427a671c7735867104"
SRC_URI[texlive-pstricks.sha256sum] = "824878d05e414ca3998a4e5d9bbe102c9b16b11c1c3c5cacea10ec6ffb87632c"
SRC_URI[texlive-pstricks-add.sha256sum] = "67e2b13c1c4a173076009b349772acb2118e12e131c81023b74ebfc194db829f"
SRC_URI[texlive-ptext.sha256sum] = "b3b253b7581c9942566ac0c87758e070e65dfa4c903c031b2a2df16383fb0130"
SRC_URI[texlive-pxfonts.sha256sum] = "1c5cd12c6348d44608e81242572804e43037e3f211d8d3cde676b92b5d61de65"
SRC_URI[texlive-qstest.sha256sum] = "68a6d3524a333c931b01a7681ba2571f135460b2a85a61b01823e6ed761db044"
SRC_URI[texlive-rcs.sha256sum] = "1c0965f7501c4f6a6c4616a4e68bf541043b5d0fc8088258ee6ca65ed9758bea"
SRC_URI[texlive-realscripts.sha256sum] = "b241663d81808afec3021c9e4c2abd6332f0019abd51a245efa2209cbd34da3f"
SRC_URI[texlive-rsfs.sha256sum] = "f2e746a25de7bb1684e4311e09f53085350255771a5fb288150911a457149e42"
SRC_URI[texlive-sansmath.sha256sum] = "88d21ad90dbe4effe66553bc826a259134567af3de9966a06adaa088762c1c90"
SRC_URI[texlive-sauerj.sha256sum] = "30ca40f852420f3f95849e56391542f5368e5315dcf8d312ce8b15ccddbb133b"
SRC_URI[texlive-scheme-basic.sha256sum] = "e6e6061b6afe0b6fcbd324a6f172c586f9e3407e28a5eea05a56d4ce9783376a"
SRC_URI[texlive-section.sha256sum] = "42b581c88fb7d04f43845074a8674fe427b3726cda660065025059824afe9dc6"
SRC_URI[texlive-sectsty.sha256sum] = "b6881b70a08e33c83fe7d82765c90369e298c1d6b0c893d0b7ba7d0bce68304b"
SRC_URI[texlive-seminar.sha256sum] = "2bd288cfcc5ba9667317265c2abcc5ebde7bb17ac47d2ccc2fa5231ae0bfa0da"
SRC_URI[texlive-sepnum.sha256sum] = "31536bd2a9fbfed57030af8b3e3b71b8bb54fd0f6d26180c5f7e5e6549c33834"
SRC_URI[texlive-setspace.sha256sum] = "afd4610823026cd641cd577bf2076c1015ddef0c3b6693cc10adf0e91abc269d"
SRC_URI[texlive-showexpl.sha256sum] = "9679fbd02c05c0aece33feef888c07f0426c2dd9a7e49bc64bfda2905806d36d"
SRC_URI[texlive-soul.sha256sum] = "ed68c0a8095951268c2e6d660ce9ab952c8f700e935c052ba8089f44a89f869e"
SRC_URI[texlive-stmaryrd.sha256sum] = "7de401af9cfa41473ccd14a6c4b03066383c3a4d68da734f542e144cd8aed76f"
SRC_URI[texlive-subfig.sha256sum] = "e731d955093332906358dec230f2986cc4b7486f8e95379b4a3ad7270568f930"
SRC_URI[texlive-subfigure.sha256sum] = "b15b1eb64863d3b5bf158711620eb7c1dd2786734f41b163b19737de68b6b10e"
SRC_URI[texlive-svn-prov.sha256sum] = "e972c5bb7a4bce76ab994844864935f5c8bb3cf3ca65aaa5165ebf3288fff0ad"
SRC_URI[texlive-symbol.sha256sum] = "983fd9dbb56b9c8f36209ed8d6a0f7147c3bd051e4f678a005841f7aef6b07d0"
SRC_URI[texlive-t2.sha256sum] = "2b007aeaac82a884857b35e97a306bdeb793e59f9d8357e24772cb3bb2b3edbb"
SRC_URI[texlive-tabu.sha256sum] = "5671e3cfe51896a320ff28f582413da03d438e15a023c943cec372e181f1a089"
SRC_URI[texlive-tabulary.sha256sum] = "b43e8aa438de5da798d5f9792b75e5e9b7a447078fe4a5309483a3b810bcef8e"
SRC_URI[texlive-tetex.sha256sum] = "e1b3aab8e4c412c8a049ea26fe94dcc9d98ebba62a1adbe8b33a51bbab87ff72"
SRC_URI[texlive-tex.sha256sum] = "dd5f94aa7b5024d843d23a70c98e5e0d091a11a53ab8766033d18edb156880ea"
SRC_URI[texlive-tex-gyre.sha256sum] = "20e4660a271752f1dee7622e905cf61f67d063d001252a0d563eb57c3d7e899d"
SRC_URI[texlive-tex-gyre-math.sha256sum] = "dc67c127471142f83ba5d8faac4a757519161408098e044e1a0258831a241f36"
SRC_URI[texlive-tex-ini-files.sha256sum] = "8d72d1d2eaeacfba06c11f8d65d3e230f0652f2b541a03722d90c03fbfc1e568"
SRC_URI[texlive-tex4ht.sha256sum] = "9a5be30647455e697a321d5d7bed1f2caa707c388be37df8e9156a72a27fa08d"
SRC_URI[texlive-texconfig.sha256sum] = "63919ec38d7f5a1e909597a4691e1a93f16607df04cabb362f19ea91b9efcb9b"
SRC_URI[texlive-texlive-common-doc.sha256sum] = "3bb056e6b91b89b931aa13ff7bc47735bbb9661719f12d003a44839829f9b625"
SRC_URI[texlive-texlive-docindex.sha256sum] = "74bd472bd65888caf6e1ab280fbb4efa09b559d3ff4af79121bd96c71621a970"
SRC_URI[texlive-texlive-en.sha256sum] = "57b2bfe693ad6b13a66ebef3e959bc7214bbaf8cacf048dff92e704909d1fde5"
SRC_URI[texlive-texlive-msg-translations.sha256sum] = "f4d35477f12019421d48651207136ae77b35acf48b0cf362e9726f676f5e98c7"
SRC_URI[texlive-texlive-scripts.sha256sum] = "cf31c8f280021b4b986b20582281f14726e68144ad3310a0289ed82124dd8017"
SRC_URI[texlive-texlive.infra.sha256sum] = "4b9e70e250ae15913d385bab30fe41dc262deb93fed0fd3c65649c2ed528e5ca"
SRC_URI[texlive-textcase.sha256sum] = "cd9fdd8f5bf104c38d1eb461ada018163577c219c222bd0a7cff0214e3896f87"
SRC_URI[texlive-textpos.sha256sum] = "7533e777a61911074646470a54f3acc1310e54cfe0bace4d6dbf08b39087a42d"
SRC_URI[texlive-threeparttable.sha256sum] = "627eda67bd428c317cedb86201aaf461373085832c5ad01f2d8baee64ce529ee"
SRC_URI[texlive-thumbpdf.sha256sum] = "462135919584ae075e12d5c195df2a90621291d61c41c209b8d2eff77353397a"
SRC_URI[texlive-times.sha256sum] = "bcc59cbdb7beb8eb8fab15c313cacd33a325687dcf994d42dfed83fafcc4fb45"
SRC_URI[texlive-tipa.sha256sum] = "ba1445462efbeb5714dfa55025cea43846e79c9fac2a5b6bfb90bcade346d0c0"
SRC_URI[texlive-titlesec.sha256sum] = "42d50cd73e5247f192715542bf9ccd19338b9c8371ca03fbe3e9eafd3e01f9d0"
SRC_URI[texlive-titling.sha256sum] = "fb82fdb5f9033492e8c8c95a628e7ffe9ffb2ccf6a9eec628b94eb119ba51c80"
SRC_URI[texlive-tocloft.sha256sum] = "572e42ba448e58473f1904c23fb791680817d41d2c39ebb5b76d3fcb24f33657"
SRC_URI[texlive-tools.sha256sum] = "26a834d5ad6a0a1ca41ef8361c72ad201b2aa78b1a51b1f37db431057a92c70f"
SRC_URI[texlive-trimspaces.sha256sum] = "9415b91921627553a6e079d36f7ba8a7cf98a315d5cefbbd53fe208871410392"
SRC_URI[texlive-txfonts.sha256sum] = "5b96d0803f1fd415fe7c73e959c61d4dca82dd1c7b3b196c30a1c4a074bee45c"
SRC_URI[texlive-type1cm.sha256sum] = "0f325fb56cbc7a4775b748a6b1984152737bd804797343ac9fc6854de08f74ad"
SRC_URI[texlive-typehtml.sha256sum] = "20252ac7e39f0075066bbc28af565e5448373727abfbbcee55c3b2dfd0a6cc1c"
SRC_URI[texlive-ucharclasses.sha256sum] = "53132228c13692653bf9b718dcdc9389b4eae9e34acc89f791a5f89fd3cebb0b"
SRC_URI[texlive-ucs.sha256sum] = "3ce3bc887c9706ca09b683fad08297f16aea179bf485eb66ea05d2cfce970947"
SRC_URI[texlive-uhc.sha256sum] = "6696ecd73449c9fb4c4aad95eb4696337db8bca094ff6f6f8b2de281a5ff3c5e"
SRC_URI[texlive-ulem.sha256sum] = "227136dbbda8832abea6df624e99ce5e410660a0a9418b103644913a1dbccf7c"
SRC_URI[texlive-underscore.sha256sum] = "4cf2af1ad1c54b932f9289d8f394ed438143658eab1813656425d3677d88d556"
SRC_URI[texlive-unicode-data.sha256sum] = "571ecf4dff73c3bae8e11e3efeddd3fdfc64c54b20d742eec0bceb4b09ccc5d1"
SRC_URI[texlive-unicode-math.sha256sum] = "b3d655a8b7727def997895db8e7b7a07564f908fe705a0eb65466ecaa82268c1"
SRC_URI[texlive-unisugar.sha256sum] = "716f9967f3c7367dd8c86a2a4e19b6290edb1a0895d678430c1ad7d2370f6202"
SRC_URI[texlive-updmap-map.sha256sum] = "52358dd97e40055179a1ed972ba8ac711b5c9ec83a4050eddefb9dac954df6c4"
SRC_URI[texlive-upquote.sha256sum] = "d03445c53744ab510ae0e2cc7e2a4ec7137ef8915a6b16c1506f5a46b6d4bcdd"
SRC_URI[texlive-url.sha256sum] = "0c8f57c3978beae64ae59e205858f9ac8c9e51a0737ff19c73593ca34a1b5c50"
SRC_URI[texlive-utopia.sha256sum] = "14407dd8169b33ce64d1037dbcd831ae830d727072ffbf65a98d1dcc69b6c4be"
SRC_URI[texlive-varwidth.sha256sum] = "6936666324734684ff9322db83512ca8e8d64b74e414920bad30fff8931149e2"
SRC_URI[texlive-wadalab.sha256sum] = "3e550a3a8a22b7b4e259c7bc1078d5c4e9b604cbcde8a6cf97294034726673f2"
SRC_URI[texlive-was.sha256sum] = "81a1b9d8840836d912ac5598c6ec17fe77465a7a882c2e8284e841b161b2c074"
SRC_URI[texlive-wasy.sha256sum] = "aa7525c8aad73da4ee715b76cc89457b4a6edbd6dc70cb6e4c4ea9ef8741d3aa"
SRC_URI[texlive-wasy2-ps.sha256sum] = "af784acad2f78e91115d639d845fffcbe767dfc15996c95ca9028e3d8d31883d"
SRC_URI[texlive-wasysym.sha256sum] = "fa4e0afef0088ac671b99329821f1bcde1351f717ff91c049467287f6d42825e"
SRC_URI[texlive-wrapfig.sha256sum] = "91c16a0bff3ffafbad0c0d519b95e337dff92727d8aa88108611123ed796a61d"
SRC_URI[texlive-xcolor.sha256sum] = "cf916c73f413694e8325b92918e1243cec859f7d409ac903ed876f54b953d2ac"
SRC_URI[texlive-xdvi.sha256sum] = "3ee6137d031eee7a72d9ac4b9a3ed7072ed7ea6c62e34aea35f90410823f81da"
SRC_URI[texlive-xecjk.sha256sum] = "fdafab1265a65eb8bf553043bcb81e25e09474128b69ecf6a3c88245703a00a0"
SRC_URI[texlive-xecolor.sha256sum] = "53d99f37185cc234a026ec5addd489cafbf4cc44583cfa69835b4a1cdf607497"
SRC_URI[texlive-xecyr.sha256sum] = "f0745851b98619eac668b00baa416c8dfdc5d39bffe5d0666e8434da89501e49"
SRC_URI[texlive-xeindex.sha256sum] = "dcbee7f0059849f85cef26b1c6ad0ef7e3b97f5fb243e64c7678d74208181a45"
SRC_URI[texlive-xepersian.sha256sum] = "19ecd865818ecc7e06318b50fe9395cd5d1e9a4b7f51130acb903411009b8d5b"
SRC_URI[texlive-xesearch.sha256sum] = "53b33134ce34cfe82d36b7942bf17fcaa023ac86c8bfce4ce6111ce888658d70"
SRC_URI[texlive-xetex.sha256sum] = "2cbaca071838cb0078862c2016eeb9084dda0363ed6b2046ac8d5ffa50f412cd"
SRC_URI[texlive-xetex-itrans.sha256sum] = "e83bb8e48b771e0052601f6265cb6091fc01fe849087eed14d35f31b0dbc030d"
SRC_URI[texlive-xetex-pstricks.sha256sum] = "8e7259748352896771e9f1c065e604b83845326fa731edf08213a02c9be8b9c6"
SRC_URI[texlive-xetex-tibetan.sha256sum] = "3647beb73e5676c4eb39c5d0003ecdd348a70704ecacf106783026345654f279"
SRC_URI[texlive-xetexconfig.sha256sum] = "5868cc47b0461610751ecd514c154a3734eb82a93dc35be273167999f3df4c69"
SRC_URI[texlive-xetexfontinfo.sha256sum] = "d5d14a599eb92c0f7d5adfd3d1d6c5d85575234584744fa3af7b03f9988faabf"
SRC_URI[texlive-xifthen.sha256sum] = "f2e95bd0ddf5ceb4382d64ee1529daceb59728b4bb165f6f3dfcb0c6c355f012"
SRC_URI[texlive-xkeyval.sha256sum] = "8fbdc6ecd0dcfa82f8dce449ffb035184e2e9940a1698dcf8a289827b3b8e5c8"
SRC_URI[texlive-xltxtra.sha256sum] = "8c2c52408a1952116e99a69e392b94c85ce079cbf9396b6e873b198bdbcfd586"
SRC_URI[texlive-xmltex.sha256sum] = "8b15792292d9c0f3d716cea08529d16c6fef66f50c06aa694be55b74b54e2175"
SRC_URI[texlive-xmltexconfig.sha256sum] = "6734e54dde8d743c9e31a43a9d60646a3abf7c4e21a9cec4d3ab716e2c8b870c"
SRC_URI[texlive-xstring.sha256sum] = "cd07d00a28e3cc9418daf53c8d75ec2119882aa6b2a07b1c21d607c3a113fd59"
SRC_URI[texlive-xtab.sha256sum] = "d54172526b2a8148f33286e6e1aea7443ec5ad418f73f333420f9d954d527e69"
SRC_URI[texlive-xunicode.sha256sum] = "003d0faf1341c621ad3cccc6f0e5446e45cbb64a1f4717ed62cf5ec043501884"
SRC_URI[texlive-zapfchan.sha256sum] = "2bb0c3334af4d2a67753bfe4743f81357c05771cf5aa88ebfede66a55556337e"
SRC_URI[texlive-zapfding.sha256sum] = "3c48a2abd8680ba9ee70705602e86a3c826913e7df01d51d89003c38679876ce"
