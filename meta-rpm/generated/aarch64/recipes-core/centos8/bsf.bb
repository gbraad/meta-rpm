SUMMARY = "generated recipe based on bsf srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_bsf = "apache-commons-logging java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_bsf-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/bsf-2.4.0-30.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/bsf-javadoc-2.4.0-30.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[bsf.sha256sum] = "427c063fe363d345982d3cc9dcac26bba26e857eec7ea5609b884b8c82704ad6"
SRC_URI[bsf-javadoc.sha256sum] = "b7cddbf20a49d1ba0c3c83eaad44c5fca9b618fcfcb0d77638254b1c657b281d"
