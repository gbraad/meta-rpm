SUMMARY = "generated recipe based on cryptsetup srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cryptsetup-libs libblkid libpwquality libuuid pkgconfig-native popt"
RPM_SONAME_REQ_cryptsetup = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libcryptsetup.so.12 libm.so.6 libpopt.so.0 libpthread.so.0 libpwquality.so.1 libuuid.so.1"
RDEPENDS_cryptsetup = "cryptsetup-libs glibc libblkid libpwquality libuuid popt"
RPM_SONAME_REQ_cryptsetup-devel = "libcryptsetup.so.12"
RPROVIDES_cryptsetup-devel = "cryptsetup-dev (= 2.2.2)"
RDEPENDS_cryptsetup-devel = "cryptsetup cryptsetup-libs pkgconf-pkg-config"
RPM_SONAME_REQ_cryptsetup-reencrypt = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libcryptsetup.so.12 libm.so.6 libpopt.so.0 libpthread.so.0 libpwquality.so.1 libuuid.so.1"
RDEPENDS_cryptsetup-reencrypt = "cryptsetup-libs glibc libblkid libpwquality libuuid popt"
RPM_SONAME_REQ_integritysetup = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libcryptsetup.so.12 libm.so.6 libpopt.so.0 libpthread.so.0 libuuid.so.1"
RDEPENDS_integritysetup = "cryptsetup-libs glibc libblkid libuuid popt"
RPM_SONAME_REQ_veritysetup = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libcryptsetup.so.12 libm.so.6 libpopt.so.0 libpthread.so.0"
RDEPENDS_veritysetup = "cryptsetup-libs glibc libblkid popt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cryptsetup-devel-2.2.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cryptsetup-2.2.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/cryptsetup-reencrypt-2.2.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/integritysetup-2.2.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/veritysetup-2.2.2-1.el8.aarch64.rpm \
          "

SRC_URI[cryptsetup.sha256sum] = "e0567ca3b4d7bcfca29d82b1f1e66d70ead68fc042b7ff390f0535cc43051302"
SRC_URI[cryptsetup-devel.sha256sum] = "f78447ccb103694a4e4e643c4842964c3fab3122f769a00734b5d14dc048add9"
SRC_URI[cryptsetup-reencrypt.sha256sum] = "835d6ef48dfbf784979ea6638dd7cbd8ca99629574b24190f38bebc2537d9576"
SRC_URI[integritysetup.sha256sum] = "73e1fe56a2495b40fa943ebfab500227f8f0afd00a8e286e504744b013b72ba3"
SRC_URI[veritysetup.sha256sum] = "5e22e913c03cc758823a685960e411fa80e34ddae5b1d3bbeb490c0da80f3814"
