SUMMARY = "generated recipe based on minicom srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0 & CLOSED"
RPM_LICENSE = "GPLv2+ and LGPLv2+ and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "lockdev ncurses pkgconfig-native"
RPM_SONAME_REQ_minicom = "ld-linux-aarch64.so.1 libc.so.6 liblockdev.so.1 libtinfo.so.6"
RDEPENDS_minicom = "bash glibc lockdev lrzsz ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/minicom-2.7.1-9.el8.aarch64.rpm \
          "

SRC_URI[minicom.sha256sum] = "8c50b373448c3b1211214abb5d961b2e0a4fb3c512da11e044623d2483764bcc"
