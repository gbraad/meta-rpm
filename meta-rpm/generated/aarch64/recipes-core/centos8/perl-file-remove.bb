SUMMARY = "generated recipe based on perl-File-Remove srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-Remove = "perl-File-Path perl-PathTools perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-File-Remove-1.57-6.el8.noarch.rpm \
          "

SRC_URI[perl-File-Remove.sha256sum] = "a84314eca1a7525597f126ac5add3bdb846e899f8161f226eed349713386645a"
