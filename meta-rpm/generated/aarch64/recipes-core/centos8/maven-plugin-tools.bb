SUMMARY = "generated recipe based on maven-plugin-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-plugin-annotations = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-plugin-registry"
RDEPENDS_maven-plugin-plugin = "java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-doxia-sink-api maven-doxia-sitetools maven-lib maven-model maven-plugin-annotations maven-plugin-registry maven-plugin-tools-annotations maven-plugin-tools-api maven-plugin-tools-generators maven-plugin-tools-java maven-reporting-api maven-reporting-impl maven-surefire plexus-utils plexus-velocity velocity"
RDEPENDS_maven-plugin-tools = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-parent maven-plugin-registry"
RDEPENDS_maven-plugin-tools-annotations = "easymock java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-model maven-plugin-annotations maven-plugin-registry maven-plugin-tools-api objectweb-asm plexus-archiver plexus-containers-component-annotations plexus-utils qdox"
RDEPENDS_maven-plugin-tools-ant = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-plugin-registry maven-plugin-tools-api maven-plugin-tools-model plexus-containers-component-annotations plexus-utils"
RDEPENDS_maven-plugin-tools-api = "java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-lib maven-model maven-plugin-registry plexus-utils"
RDEPENDS_maven-plugin-tools-beanshell = "bsh java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-plugin-registry maven-plugin-tools-api plexus-containers-component-annotations"
RDEPENDS_maven-plugin-tools-generators = "java-1.8.0-openjdk-headless javapackages-filesystem jtidy maven-lib maven-model maven-plugin-registry maven-plugin-tools-api maven-reporting-api objectweb-asm plexus-utils plexus-velocity velocity"
RDEPENDS_maven-plugin-tools-java = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-model maven-plugin-registry maven-plugin-tools-api plexus-containers-component-annotations plexus-utils qdox"
RDEPENDS_maven-plugin-tools-javadoc = "java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools maven-lib maven-plugin-registry maven-plugin-tools-java"
RDEPENDS_maven-plugin-tools-javadocs = "javapackages-filesystem"
RDEPENDS_maven-plugin-tools-model = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-plugin-registry plexus-utils"
RDEPENDS_maven-script = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-plugin-registry maven-plugin-tools"
RDEPENDS_maven-script-ant = "ant-lib java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-plugin-registry plexus-ant-factory plexus-archiver"
RDEPENDS_maven-script-beanshell = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-plugin-registry plexus-bsh-factory"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-annotations-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-plugin-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-tools-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-tools-annotations-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-tools-ant-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-tools-api-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-tools-beanshell-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-tools-generators-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-tools-java-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-tools-javadoc-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-tools-javadocs-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-tools-model-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-script-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-script-ant-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-script-beanshell-3.5.1-4.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-plugin-annotations.sha256sum] = "eb1197137defd015e3c8f393eb72c3bd45057554cdddb3f8dc193121cdce98e8"
SRC_URI[maven-plugin-plugin.sha256sum] = "5e8219f42db31dc67839e4b97ab8625b132e1fc7f6d7d7aff2ea46fb83a4a7e2"
SRC_URI[maven-plugin-tools.sha256sum] = "3ee676eb0802e29a91888f3c55569b152b9fa7d726234ba96c52339ddae11ec3"
SRC_URI[maven-plugin-tools-annotations.sha256sum] = "6afbc6a992b4c864652dc237a9306a0eb58857d9cead3a5d516ecee81724aa11"
SRC_URI[maven-plugin-tools-ant.sha256sum] = "6a19f7097fbb0c03f6aac820170ae2da5742852917561ca3750e6736f49385ef"
SRC_URI[maven-plugin-tools-api.sha256sum] = "1aeae10d7513d308ef7c807ac1c3ea63a7d8fba5045cbcbf48dd7c6bbc445224"
SRC_URI[maven-plugin-tools-beanshell.sha256sum] = "819845cbb47d225b2660511f8110604f62433595e85431475cc75a7a373ef4ae"
SRC_URI[maven-plugin-tools-generators.sha256sum] = "7b0fd764caa563f41fc347dd331a2f69be5d3a35548cff2732cb356f48d362f3"
SRC_URI[maven-plugin-tools-java.sha256sum] = "aa84952e17a8dbec42d6a8e0cf40d722e2784321ccc1f415a0d21f9a8156bb7e"
SRC_URI[maven-plugin-tools-javadoc.sha256sum] = "549be423fb3fadbf704f051b0fb8f24530129ad84909a968572b2731b5a4ed49"
SRC_URI[maven-plugin-tools-javadocs.sha256sum] = "ddaa2cc661d04f3775279cc8538b4bedc7e24729c613e019741eb424eb7b39a8"
SRC_URI[maven-plugin-tools-model.sha256sum] = "4422f929732adf20e5ad3aa19ce967a2b3edc6e41968febb0036ef7a2d1ad65b"
SRC_URI[maven-script.sha256sum] = "b008eab0dfcb002e2ed3b85967b0e42ea245e31e3f6d4b6812980d41981d8cbb"
SRC_URI[maven-script-ant.sha256sum] = "e095fd3f96b60fa1221555018de4aef76502c71212282d8fc0abf4a6e3cadb77"
SRC_URI[maven-script-beanshell.sha256sum] = "c031d65e399110258179c628bf60afeb0b718f4e449b7a12f24ef99084d109cc"
