SUMMARY = "generated recipe based on gnome-system-monitor srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk atkmm cairo cairomm gdk-pixbuf glib-2.0 glibmm24 gtk+3 gtkmm30 libgcc libgtop2 librsvg libsigc++20 libxml2 pango pangomm pkgconfig-native systemd-libs"
RPM_SONAME_REQ_gnome-system-monitor = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libatkmm-1.6.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcairomm-1.0.so.1 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgdkmm-3.0.so.1 libgio-2.0.so.0 libgiomm-2.4.so.1 libglib-2.0.so.0 libglibmm-2.4.so.1 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtkmm-3.0.so.1 libgtop-2.0.so.11 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangomm-1.4.so.1 libpthread.so.0 librsvg-2.so.2 libsigc-2.0.so.0 libstdc++.so.6 libsystemd.so.0 libxml2.so.2"
RDEPENDS_gnome-system-monitor = "atk atkmm cairo cairo-gobject cairomm gdk-pixbuf2 glib2 glibc glibmm24 gtk3 gtkmm30 libgcc libgtop2 librsvg2 libsigc++20 libstdc++ libxml2 pango pangomm systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-system-monitor-3.28.2-1.el8.aarch64.rpm \
          "

SRC_URI[gnome-system-monitor.sha256sum] = "8d322d9390b875bbb8f9baacf590de638ffa8dfbe2f4d833b4396af35eec10e1"
