SUMMARY = "generated recipe based on ima-evm-utils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "keyutils openssl pkgconfig-native"
RPM_SONAME_PROV_ima-evm-utils = "libimaevm.so.0"
RPM_SONAME_REQ_ima-evm-utils = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libkeyutils.so.1 libssl.so.1.1"
RDEPENDS_ima-evm-utils = "glibc keyutils-libs openssl-libs"
RPM_SONAME_REQ_ima-evm-utils-devel = "libimaevm.so.0"
RPROVIDES_ima-evm-utils-devel = "ima-evm-utils-dev (= 1.1)"
RDEPENDS_ima-evm-utils-devel = "ima-evm-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ima-evm-utils-1.1-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ima-evm-utils-devel-1.1-5.el8.aarch64.rpm \
          "

SRC_URI[ima-evm-utils.sha256sum] = "b58f45e55741bb835ec566af2afc36cd97d8fc354b3aad96a85980cadefa968c"
SRC_URI[ima-evm-utils-devel.sha256sum] = "7eabc4c3ee5c2aaa70db08bf2e6255da071d444f82ea8b65d727537c8b81fceb"
