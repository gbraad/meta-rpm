SUMMARY = "generated recipe based on perl-GSSAPI srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "e2fsprogs krb5-libs perl pkgconfig-native"
RPM_SONAME_REQ_perl-GSSAPI = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libgssapi_krb5.so.2 libk5crypto.so.3 libkrb5.so.3 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-GSSAPI = "glibc krb5-libs libcom_err perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-GSSAPI-0.28-23.el8.aarch64.rpm \
          "

SRC_URI[perl-GSSAPI.sha256sum] = "d3dc23b5ef296ff05a9f5d006ffa35bcadf23f0d046bdd351f379af57d38cc15"
