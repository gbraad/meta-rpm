SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-pt = "locale-base-pt-br (= 2.28) locale-base-pt-br.utf8 (= 2.28) locale-base-pt-pt (= 2.28) locale-base-pt-pt.utf8 (= 2.28) locale-base-pt-pt@euro (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt (= 2.28) virtual-locale-pt-br (= 2.28) virtual-locale-pt-br.utf8 (= 2.28) virtual-locale-pt-pt (= 2.28) virtual-locale-pt-pt.utf8 (= 2.28) virtual-locale-pt-pt@euro (= 2.28)"
RDEPENDS_glibc-langpack-pt = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-pt-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-pt.sha256sum] = "7344e284deb957e6b0f7d66a38679a9d2b85751255a69d3af77cdbcad545adc8"
