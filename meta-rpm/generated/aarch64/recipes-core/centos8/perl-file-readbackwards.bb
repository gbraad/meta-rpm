SUMMARY = "generated recipe based on perl-File-ReadBackwards srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-ReadBackwards = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-File-ReadBackwards-1.05-11.el8.noarch.rpm \
          "

SRC_URI[perl-File-ReadBackwards.sha256sum] = "b8083272492f3f84526da5d1b421649efccfd6304c0f42d12ef124fab68fb585"
