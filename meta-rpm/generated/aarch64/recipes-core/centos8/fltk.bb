SUMMARY = "generated recipe based on fltk srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+ with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "fontconfig libgcc libglvnd libice libjpeg-turbo libpng libsm libx11 libxcursor libxext libxfixes libxft libxinerama libxrender libxt mesa-libglu pkgconfig-native zlib"
RPM_SONAME_PROV_fltk = "libfltk.so.1.3 libfltk_forms.so.1.3 libfltk_gl.so.1.3 libfltk_images.so.1.3"
RPM_SONAME_REQ_fltk = "ld-linux-aarch64.so.1 libGL.so.1 libGLU.so.1 libX11.so.6 libXcursor.so.1 libXext.so.6 libXfixes.so.3 libXft.so.2 libXinerama.so.1 libXrender.so.1 libc.so.6 libdl.so.2 libfontconfig.so.1 libgcc_s.so.1 libjpeg.so.62 libm.so.6 libpng16.so.16 libpthread.so.0 libstdc++.so.6 libz.so.1"
RDEPENDS_fltk = "fontconfig glibc libX11 libXcursor libXext libXfixes libXft libXinerama libXrender libgcc libglvnd-glx libjpeg-turbo libpng libstdc++ mesa-libGLU zlib"
RPM_SONAME_REQ_fltk-devel = "libfltk.so.1.3 libfltk_forms.so.1.3 libfltk_gl.so.1.3 libfltk_images.so.1.3"
RPROVIDES_fltk-devel = "fltk-dev (= 1.3.4)"
RDEPENDS_fltk-devel = "bash fltk libICE-devel libSM-devel libX11-devel libXft-devel libXt-devel libglvnd-devel libstdc++-devel mesa-libGLU-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/fltk-1.3.4-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/fltk-devel-1.3.4-5.el8.aarch64.rpm \
          "

SRC_URI[fltk.sha256sum] = "5f7ac9a8ffdb2273535cd6f07bd8b03bf936905611728c4ec9355db7c4ec65e6"
SRC_URI[fltk-devel.sha256sum] = "169fa5307c3080ffc96c379895f582d07cdd10abef99331746e0f028a1f21653"
