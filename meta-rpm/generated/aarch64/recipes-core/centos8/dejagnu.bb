SUMMARY = "generated recipe based on dejagnu srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_dejagnu = "bash expect"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dejagnu-1.6.1-2.el8.noarch.rpm \
          "

SRC_URI[dejagnu.sha256sum] = "de70e28feda6e550d2d80524cad4c95bd4ab1037fb890bfa26cd8f505f34cbca"
