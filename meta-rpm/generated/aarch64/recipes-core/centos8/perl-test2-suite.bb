SUMMARY = "generated recipe based on perl-Test2-Suite srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test2-Suite = "perl-Carp perl-Data-Dumper perl-Exporter perl-File-Temp perl-Importer perl-Module-Pluggable perl-PathTools perl-Scalar-List-Utils perl-Scope-Guard perl-Sub-Info perl-Term-Table perl-Test-Simple perl-Time-HiRes perl-interpreter perl-libs perl-threads"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Test2-Suite-0.000111-1.el8.noarch.rpm \
          "

SRC_URI[perl-Test2-Suite.sha256sum] = "90f3f06871c0608fcb8df548ebb63c39412f4a4ad1b2235d5611db09835d9406"
