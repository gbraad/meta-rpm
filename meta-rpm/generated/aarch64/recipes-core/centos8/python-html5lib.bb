SUMMARY = "generated recipe based on python-html5lib srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-html5lib = "platform-python python3-six python3-webencodings"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-html5lib-0.999999999-6.el8.noarch.rpm \
          "

SRC_URI[python3-html5lib.sha256sum] = "f3db954bc4b3dc3e8ce91a4377701215df88c68ac95d72c45362dc22142c31fc"
