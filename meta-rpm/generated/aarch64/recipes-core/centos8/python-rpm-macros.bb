SUMMARY = "generated recipe based on python-rpm-macros srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python-rpm-macros = "python-srpm-macros"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python-rpm-macros-3-38.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python-srpm-macros-3-38.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-rpm-macros-3-38.el8.noarch.rpm \
          "

SRC_URI[python-rpm-macros.sha256sum] = "575dd8bc701aefceb76b1dcce3a3c6f2497525f8a15f8301ffadca9e2f0a9d2f"
SRC_URI[python-srpm-macros.sha256sum] = "2656d8c0b6f7befde9c7df222f919893111293125b72c827875dd45f78d9d316"
SRC_URI[python3-rpm-macros.sha256sum] = "3d0f79bdef96445d3b7be53c8b29e113d7511111420095281f73abf927378be3"
