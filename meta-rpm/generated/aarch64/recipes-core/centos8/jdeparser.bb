SUMMARY = "generated recipe based on jdeparser srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jdeparser = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jdeparser-2.0.0-5.el8.noarch.rpm \
          "

SRC_URI[jdeparser.sha256sum] = "84b40f4e8e0d1b92155e626e501932d1f6fcce3314e4a0d56c1579783002ea06"
