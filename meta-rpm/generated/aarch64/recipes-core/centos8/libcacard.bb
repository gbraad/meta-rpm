SUMMARY = "generated recipe based on libcacard srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 nspr nss pkgconfig-native"
RPM_SONAME_PROV_libcacard = "libcacard.so.0"
RPM_SONAME_REQ_libcacard = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libnspr4.so libnss3.so"
RDEPENDS_libcacard = "glib2 glibc nspr nss"
RPM_SONAME_REQ_libcacard-devel = "libcacard.so.0"
RPROVIDES_libcacard-devel = "libcacard-dev (= 2.7.0)"
RDEPENDS_libcacard-devel = "glib2-devel libcacard nss-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libcacard-2.7.0-2.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libcacard-devel-2.7.0-2.el8_1.aarch64.rpm \
          "

SRC_URI[libcacard.sha256sum] = "866b76aee5e52687566181039a66aee71ae7c0b6f3383ae8e5ca247c73a9d85f"
SRC_URI[libcacard-devel.sha256sum] = "42a0f90e804226b9d3741641917cf365111cffa753330b32b40c67b15d37aa30"
