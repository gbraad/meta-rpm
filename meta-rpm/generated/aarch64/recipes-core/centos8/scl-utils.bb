SUMMARY = "generated recipe based on scl-utils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native rpm"
RPM_SONAME_REQ_scl-utils = "ld-linux-aarch64.so.1 libc.so.6 librpm.so.8 librpmio.so.8"
RDEPENDS_scl-utils = "bash environment-modules glibc rpm-libs"
RDEPENDS_scl-utils-build = "bash iso-codes redhat-rpm-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/scl-utils-2.0.2-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/scl-utils-build-2.0.2-12.el8.aarch64.rpm \
          "

SRC_URI[scl-utils.sha256sum] = "2ae5c1ee1412628a530f0b62528a5ee1954ee16af9b862d4589205d18b63dfaa"
SRC_URI[scl-utils-build.sha256sum] = "6280d1c6a7fcbdc6d2bfc975b48f26be10799264c8abdb2637ab049a81a90089"
