SUMMARY = "generated recipe based on isorelax srpm"
DESCRIPTION = "Description"
LICENSE = "MIT & CLOSED"
RPM_LICENSE = "MIT and ASL 1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_isorelax = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_isorelax-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/isorelax-0-0.23.release20050331.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/isorelax-javadoc-0-0.23.release20050331.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[isorelax.sha256sum] = "bb067e3bbbf2b36af7a944258c6158d57cd2d6b191cbc3f4d66425331b4ff4a2"
SRC_URI[isorelax-javadoc.sha256sum] = "322f1a13fe8be619596a9aa2f421c3e4612b525578bd14373d69f67be99b91fe"
