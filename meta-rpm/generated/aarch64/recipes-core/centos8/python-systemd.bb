SUMMARY = "generated recipe based on python-systemd srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native platform-python3 systemd-libs"
RPM_SONAME_REQ_python3-systemd = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libpthread.so.0 libpython3.6m.so.1.0 libsystemd.so.0"
RDEPENDS_python3-systemd = "glibc libgcc platform-python python3-libs systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-systemd-234-8.el8.aarch64.rpm \
          "

SRC_URI[python3-systemd.sha256sum] = "b44932e4383ed1a197cacb9af673eb527ad5903202db0c573aeac879f42d7a9a"
