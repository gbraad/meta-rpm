SUMMARY = "generated recipe based on hunspell-ti srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ti = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ti-0.20090911-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-ti.sha256sum] = "5ebec6fd3e705d5f7ff38d214671ef2892c9cf5abed367f8030964d7222759ca"
