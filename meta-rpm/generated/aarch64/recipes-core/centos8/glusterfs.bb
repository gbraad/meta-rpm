SUMMARY = "generated recipe based on glusterfs srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-3.0"
RPM_LICENSE = "GPLv2 or LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl libgcc libtirpc libuuid libxml2 ncurses openssl pkgconfig-native rdma-core readline zlib"
RPM_SONAME_REQ_glusterfs = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgcc_s.so.1 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libpthread.so.0 librt.so.1 libssl.so.1.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs = "bash glibc glusterfs-libs libgcc libtirpc libuuid openssl-libs shadow-utils systemd zlib"
RPM_SONAME_PROV_glusterfs-api = "libgfapi.so.0"
RPM_SONAME_REQ_glusterfs-api = "ld-linux-aarch64.so.1 libacl.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libpthread.so.0 librt.so.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs-api = "bash glibc glusterfs glusterfs-client-xlators glusterfs-libs libacl libtirpc libuuid openssl-libs zlib"
RPM_SONAME_REQ_glusterfs-api-devel = "libgfapi.so.0"
RPROVIDES_glusterfs-api-devel = "glusterfs-api-dev (= 6.0)"
RDEPENDS_glusterfs-api-devel = "glusterfs glusterfs-api glusterfs-devel libacl-devel libuuid-devel pkgconf-pkg-config"
RPM_SONAME_REQ_glusterfs-cli = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libncurses.so.6 libpthread.so.0 libreadline.so.7 librt.so.1 libtinfo.so.6 libtirpc.so.3 libuuid.so.1 libxml2.so.2 libz.so.1"
RDEPENDS_glusterfs-cli = "bash glibc glusterfs-libs libtirpc libuuid libxml2 ncurses-libs openssl-libs readline zlib"
RPM_SONAME_REQ_glusterfs-client-xlators = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libpthread.so.0 librt.so.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs-client-xlators = "glibc glusterfs-libs libtirpc libuuid openssl-libs zlib"
RPM_SONAME_REQ_glusterfs-devel = "libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0"
RPROVIDES_glusterfs-devel = "glusterfs-dev (= 6.0)"
RDEPENDS_glusterfs-devel = "glusterfs glusterfs-libs"
RPM_SONAME_REQ_glusterfs-fuse = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libm.so.6 libpthread.so.0 librt.so.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs-fuse = "attr bash glibc glusterfs glusterfs-client-xlators glusterfs-libs libtirpc libuuid openssl-libs psmisc zlib"
RPM_SONAME_PROV_glusterfs-libs = "libgfchangelog.so.0 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0"
RPM_SONAME_REQ_glusterfs-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpthread.so.0 librt.so.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs-libs = "bash glibc libtirpc libuuid openssl-libs zlib"
RPM_SONAME_REQ_glusterfs-rdma = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libgfrpc.so.0 libgfxdr.so.0 libglusterfs.so.0 libibverbs.so.1 libm.so.6 libpthread.so.0 librdmacm.so.1 librt.so.1 libtirpc.so.3 libuuid.so.1 libz.so.1"
RDEPENDS_glusterfs-rdma = "glibc glusterfs glusterfs-libs libibverbs librdmacm libtirpc libuuid openssl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/glusterfs-api-6.0-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/glusterfs-cli-6.0-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glusterfs-6.0-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glusterfs-client-xlators-6.0-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glusterfs-fuse-6.0-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glusterfs-libs-6.0-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glusterfs-rdma-6.0-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glusterfs-api-devel-6.0-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glusterfs-devel-6.0-37.el8.aarch64.rpm \
          "

SRC_URI[glusterfs.sha256sum] = "5c74167be6d10d6e31c4370b83f99e0c553b0c6616650564499d6c3428377aec"
SRC_URI[glusterfs-api.sha256sum] = "ba09195dc24849f3b82872480c65f22d6ce54143bcdbd84f3f0b2909243a7978"
SRC_URI[glusterfs-api-devel.sha256sum] = "a8a90502a86830bf8dae2180973cfb12278eb2f7da36ae84ad8e8186f38b26b1"
SRC_URI[glusterfs-cli.sha256sum] = "b7a9794275ce3d1e9357579c7a1e400ec0e2ba4d8bb5f518cf6f4e2497cfb37f"
SRC_URI[glusterfs-client-xlators.sha256sum] = "34b45da4fb0a4aed7ba33590e4033f6283a8ca64564a9da92e014ed519aa21dc"
SRC_URI[glusterfs-devel.sha256sum] = "878eed582a9cd3aef1e18046f9a0f4adb69eb46474431d1e31c01bca7e1b2bc7"
SRC_URI[glusterfs-fuse.sha256sum] = "65f13516a1fb7be7398136c66b0f6a265a572c905e276450145c130710cd8b5e"
SRC_URI[glusterfs-libs.sha256sum] = "73e6c83458471dc5f0da83859470fcc4f0459e90e38eb7929237192bcfd7bb96"
SRC_URI[glusterfs-rdma.sha256sum] = "3e2abb91c09a82026c4673320e9c2b75b6f5020964db80c6a07335b45aed3291"
