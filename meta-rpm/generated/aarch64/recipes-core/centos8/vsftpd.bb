SUMMARY = "generated recipe based on vsftpd srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libcap openssl pam pkgconfig-native"
RPM_SONAME_REQ_vsftpd = "ld-linux-aarch64.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdl.so.2 libpam.so.0 libssl.so.1.1"
RDEPENDS_vsftpd = "bash glibc libcap logrotate openssl-libs pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/vsftpd-3.0.3-31.el8.aarch64.rpm \
          "

SRC_URI[vsftpd.sha256sum] = "42b3d2853e3b9cdbc7de8b86ddc0eb1af2a9c03b8405864c8607fc124f9b4084"
