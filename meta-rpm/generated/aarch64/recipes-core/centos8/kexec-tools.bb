SUMMARY = "generated recipe based on kexec-tools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 elfutils libgcc lzo ncurses pkgconfig-native snappy xz zlib"
RPM_SONAME_REQ_kexec-tools = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 libgcc_s.so.1 liblzma.so.5 liblzo2.so.2 libpthread.so.0 libsnappy.so.1 libtinfo.so.6 libz.so.1"
RDEPENDS_kexec-tools = "bash bzip2-libs coreutils dracut dracut-network dracut-squash elfutils-libelf elfutils-libs ethtool glibc libgcc lzo ncurses-libs sed snappy systemd xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kexec-tools-2.0.20-14.el8.aarch64.rpm \
          "

SRC_URI[kexec-tools.sha256sum] = "da70033e8c8b8f9d4f987963fafd39d9710539ad2c28881731d2fbd9ef7090b0"
