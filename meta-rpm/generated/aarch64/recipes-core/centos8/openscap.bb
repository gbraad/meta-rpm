SUMMARY = "generated recipe based on openscap srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl bzip2 curl dbus-libs gconf glib-2.0 libblkid libcap libgcc libgcrypt libpcre libselinux libxml2 libxslt pkgconfig-native platform-python3 rpm"
RPM_SONAME_PROV_openscap = "libopenscap.so.25"
RPM_SONAME_REQ_openscap = "ld-linux-aarch64.so.1 libacl.so.1 libblkid.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcurl.so.4 libdbus-1.so.3 libexslt.so.0 libgcc_s.so.1 libgconf-2.so.4 libgcrypt.so.20 libglib-2.0.so.0 libgobject-2.0.so.0 libpcre.so.1 libpthread.so.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libxml2.so.2 libxslt.so.1"
RDEPENDS_openscap = "GConf2 bash bzip2-libs dbus dbus-libs glib2 glibc libacl libblkid libcap libcurl libgcc libgcrypt libselinux libxml2 libxslt openldap pcre popt procps-ng rpm-libs"
RPM_SONAME_REQ_openscap-devel = "libopenscap.so.25"
RPROVIDES_openscap-devel = "openscap-dev (= 1.3.2)"
RDEPENDS_openscap-devel = "libxml2-devel openscap pkgconf-pkg-config"
RPM_SONAME_PROV_openscap-engine-sce = "libopenscap_sce.so.25"
RPM_SONAME_REQ_openscap-engine-sce = "ld-linux-aarch64.so.1 libacl.so.1 libblkid.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcurl.so.4 libdbus-1.so.3 libexslt.so.0 libgconf-2.so.4 libgcrypt.so.20 libglib-2.0.so.0 libgobject-2.0.so.0 libopenscap.so.25 libpcre.so.1 libpthread.so.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libxml2.so.2 libxslt.so.1"
RDEPENDS_openscap-engine-sce = "GConf2 bash bzip2-libs dbus-libs glib2 glibc libacl libblkid libcap libcurl libgcrypt libselinux libxml2 libxslt openscap pcre rpm-libs"
RPM_SONAME_REQ_openscap-engine-sce-devel = "libopenscap_sce.so.25"
RPROVIDES_openscap-engine-sce-devel = "openscap-engine-sce-dev (= 1.3.2)"
RDEPENDS_openscap-engine-sce-devel = "openscap-devel openscap-engine-sce pkgconf-pkg-config"
RPM_SONAME_REQ_openscap-python3 = "ld-linux-aarch64.so.1 libacl.so.1 libblkid.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcurl.so.4 libdbus-1.so.3 libexslt.so.0 libgconf-2.so.4 libgcrypt.so.20 libglib-2.0.so.0 libgobject-2.0.so.0 libopenscap.so.25 libpcre.so.1 libpthread.so.0 libpython3.6m.so.1.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libxml2.so.2 libxslt.so.1"
RDEPENDS_openscap-python3 = "GConf2 bzip2-libs dbus-libs glib2 glibc libacl libblkid libcap libcurl libgcrypt libselinux libxml2 libxslt openscap pcre platform-python python3-libs rpm-libs"
RPM_SONAME_REQ_openscap-scanner = "ld-linux-aarch64.so.1 libacl.so.1 libblkid.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcurl.so.4 libdbus-1.so.3 libexslt.so.0 libgconf-2.so.4 libgcrypt.so.20 libglib-2.0.so.0 libgobject-2.0.so.0 libopenscap.so.25 libpcre.so.1 libpthread.so.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libxml2.so.2 libxslt.so.1"
RDEPENDS_openscap-scanner = "GConf2 bash bzip2-libs dbus-libs glib2 glibc libacl libblkid libcap libcurl libgcrypt libselinux libxml2 libxslt openscap pcre rpm-libs"
RDEPENDS_openscap-utils = "bash openscap openscap-scanner platform-python rpm-build rpmdevtools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openscap-1.3.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openscap-devel-1.3.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openscap-engine-sce-1.3.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openscap-python3-1.3.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openscap-scanner-1.3.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openscap-utils-1.3.2-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/openscap-engine-sce-devel-1.3.2-6.el8.aarch64.rpm \
          "

SRC_URI[openscap.sha256sum] = "f6fd9ff6e87dd839e35207a779b117bf7201d3f656c8827f3cb4f40263054cb9"
SRC_URI[openscap-devel.sha256sum] = "4f360090bf2b6457e425fc8498310a178bac557d0501569fa028a494f4f06c8c"
SRC_URI[openscap-engine-sce.sha256sum] = "b4324b8110df977d31e568f91205d8f535cc1b1aab57e81487d995c2e3d81568"
SRC_URI[openscap-engine-sce-devel.sha256sum] = "38590332389ee5d40ac16515e1866aed4db800778041056655f0826dcd562eb6"
SRC_URI[openscap-python3.sha256sum] = "ebfccb06115362d94c5dd206d9e45b323592ff21d1b826ebab73e69490937e8e"
SRC_URI[openscap-scanner.sha256sum] = "1177abf6b375cde783e8e7e980461aa613201bcd87082bae6679781178fee7cc"
SRC_URI[openscap-utils.sha256sum] = "8b3b3e3a90aeb2997628948ad33b5cb634369b49909b21ed6da0ceeb27520c2e"
