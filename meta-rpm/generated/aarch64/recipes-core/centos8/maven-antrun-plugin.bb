SUMMARY = "generated recipe based on maven-antrun-plugin srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-antrun-plugin = "ant-lib java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-lib maven-project plexus-utils"
RDEPENDS_maven-antrun-plugin-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-antrun-plugin-1.8-6.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-antrun-plugin-javadoc-1.8-6.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-antrun-plugin.sha256sum] = "9d6faa80241274c36d0205501ded9f667b05fcc4eef14d1d1ba7b3f367d753a4"
SRC_URI[maven-antrun-plugin-javadoc.sha256sum] = "ee5d16783e2c12f1dc1f0b46d73c24914aa59d45c0dc89308d603cc40b9b6aaf"
