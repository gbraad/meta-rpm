SUMMARY = "generated recipe based on libevent srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & MIT"
RPM_LICENSE = "BSD and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_PROV_libevent = "libevent-2.1.so.6 libevent_core-2.1.so.6 libevent_extra-2.1.so.6 libevent_openssl-2.1.so.6 libevent_pthreads-2.1.so.6"
RPM_SONAME_REQ_libevent = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libpthread.so.0 libssl.so.1.1"
RDEPENDS_libevent = "glibc openssl-libs"
RPM_SONAME_REQ_libevent-devel = "libevent-2.1.so.6 libevent_core-2.1.so.6 libevent_extra-2.1.so.6 libevent_openssl-2.1.so.6 libevent_pthreads-2.1.so.6"
RPROVIDES_libevent-devel = "libevent-dev (= 2.1.8)"
RDEPENDS_libevent-devel = "libevent pkgconf-pkg-config platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libevent-devel-2.1.8-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libevent-2.1.8-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libevent-doc-2.1.8-5.el8.noarch.rpm \
          "

SRC_URI[libevent.sha256sum] = "a7fed3b521d23e60539dcbd548bda2a62f0d745a99dd5feeb43b6539f7f88232"
SRC_URI[libevent-devel.sha256sum] = "5037529a57eae4d371ddf73b197c2e522082c65792f0c760e3dabafdc34655bb"
SRC_URI[libevent-doc.sha256sum] = "7444164b9d09e77d738327cb8b5044c13a1a33f0d88154f28768c2a7694c83ee"
