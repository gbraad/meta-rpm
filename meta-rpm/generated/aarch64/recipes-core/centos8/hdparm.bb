SUMMARY = "generated recipe based on hdparm srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_hdparm = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_hdparm = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/hdparm-9.54-2.el8.aarch64.rpm \
          "

SRC_URI[hdparm.sha256sum] = "7fccc75d00a8f0223b01776dd79a22822291e4b8bb41e38ec483a556b2b7884d"
