SUMMARY = "generated recipe based on jasper srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "JasPer"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libjpeg-turbo pkgconfig-native"
RPM_SONAME_REQ_jasper-devel = "libjasper.so.4"
RPROVIDES_jasper-devel = "jasper-dev (= 2.0.14)"
RDEPENDS_jasper-devel = "jasper-libs libjpeg-turbo-devel pkgconf-pkg-config"
RPM_SONAME_PROV_jasper-libs = "libjasper.so.4"
RPM_SONAME_REQ_jasper-libs = "ld-linux-aarch64.so.1 libc.so.6 libjpeg.so.62 libm.so.6"
RDEPENDS_jasper-libs = "glibc libjpeg-turbo"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jasper-libs-2.0.14-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jasper-devel-2.0.14-4.el8.aarch64.rpm \
          "

SRC_URI[jasper-devel.sha256sum] = "dd43c0c77253d79d85bf89e03fa4778c82d72c2b085f78aa7c05d8199f9050de"
SRC_URI[jasper-libs.sha256sum] = "45dc7fce69a8534d7eaf8f261b68c102a6b9a0c17cf418c9992e041af447b2a6"
