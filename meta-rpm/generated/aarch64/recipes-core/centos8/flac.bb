SUMMARY = "generated recipe based on flac srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & GPL-2.0 & GFDL-1.1"
RPM_LICENSE = "BSD and GPLv2+ and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libogg pkgconfig-native"
RPM_SONAME_REQ_flac = "ld-linux-aarch64.so.1 libFLAC.so.8 libc.so.6 libm.so.6 libogg.so.0"
RDEPENDS_flac = "flac-libs glibc libogg"
RPM_SONAME_REQ_flac-devel = "libFLAC++.so.6 libFLAC.so.8"
RPROVIDES_flac-devel = "flac-dev (= 1.3.2)"
RDEPENDS_flac-devel = "flac-libs libogg-devel pkgconf-pkg-config"
RPM_SONAME_PROV_flac-libs = "libFLAC++.so.6 libFLAC.so.8"
RPM_SONAME_REQ_flac-libs = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libogg.so.0 libstdc++.so.6"
RDEPENDS_flac-libs = "glibc libgcc libogg libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/flac-libs-1.3.2-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/flac-1.3.2-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/flac-devel-1.3.2-9.el8.aarch64.rpm \
          "

SRC_URI[flac.sha256sum] = "0ad67e3d57e17aa57ee4cc03298cd1dc5861243f25978cfe8bae4d8257f02155"
SRC_URI[flac-devel.sha256sum] = "27faaa4cd256b6e23d44b2908f553bd71b74e8ebd267460d742143a1b6b2b97c"
SRC_URI[flac-libs.sha256sum] = "9e562e46c34c516de1da356d3596295c62449827095cb712a1f09cd8f91e8324"
