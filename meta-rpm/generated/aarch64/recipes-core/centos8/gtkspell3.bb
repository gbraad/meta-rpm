SUMMARY = "generated recipe based on gtkspell3 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo enchant gdk-pixbuf glib-2.0 gtk+3 pango pkgconfig-native"
RPM_SONAME_PROV_gtkspell3 = "libgtkspell3-3.so.0"
RPM_SONAME_REQ_gtkspell3 = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libenchant.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0"
RDEPENDS_gtkspell3 = "atk cairo cairo-gobject enchant gdk-pixbuf2 glib2 glibc gtk3 iso-codes pango"
RPM_SONAME_REQ_gtkspell3-devel = "libgtkspell3-3.so.0"
RPROVIDES_gtkspell3-devel = "gtkspell3-dev (= 3.0.9)"
RDEPENDS_gtkspell3-devel = "enchant-devel glib2-devel gtk3-devel gtkspell3 pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtkspell3-3.0.9-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gtkspell3-devel-3.0.9-5.el8.aarch64.rpm \
          "

SRC_URI[gtkspell3.sha256sum] = "ff3f083b7d64b4867714969644b151accd3827c1281c75072be6f113003fa1d7"
SRC_URI[gtkspell3-devel.sha256sum] = "df34ec91cd7de0e44236b53a3726a3df0907c76236c168e6f2f40b5b2233fc7b"
