SUMMARY = "generated recipe based on lohit-telugu-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_lohit-telugu-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lohit-telugu-fonts-2.5.5-3.el8.noarch.rpm \
          "

SRC_URI[lohit-telugu-fonts.sha256sum] = "ff29144a302bff8939e77ee9d489d32ed97819df92188106f3e7cd0cccc1a17a"
