SUMMARY = "generated recipe based on perl-Term-Size-Any srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Term-Size-Any = "perl-Term-Size-Perl perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Term-Size-Any-0.002-23.el8.noarch.rpm \
          "

SRC_URI[perl-Term-Size-Any.sha256sum] = "8ccd1e57f1a97fb5a7bf43a4d7cacc72912af2cfd38344191970ed43ae41bd55"
