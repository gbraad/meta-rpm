SUMMARY = "generated recipe based on icoutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libpng pkgconfig-native zlib"
RPM_SONAME_REQ_icoutils = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpng16.so.16 libz.so.1"
RDEPENDS_icoutils = "glibc libpng perl-Getopt-Long perl-HTTP-Message perl-PathTools perl-interpreter perl-libwww-perl zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/icoutils-0.32.3-2.el8.aarch64.rpm \
          "

SRC_URI[icoutils.sha256sum] = "677c0dd13d6c860e7a523cf9b3d6202edad9dc96ab346a02cfd7143fb96b497b"
