SUMMARY = "generated recipe based on hunspell-fo srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-fo = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-fo-0.4.2-8.el8.noarch.rpm \
          "

SRC_URI[hunspell-fo.sha256sum] = "fefbd5a4e535990ef29a806590211a8f2676879f061f64e11193448c55a7c7d4"
