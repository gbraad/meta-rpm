SUMMARY = "generated recipe based on microdnf srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libdnf libgcc libpeas pkgconfig-native util-linux"
RPM_SONAME_REQ_microdnf = "ld-linux-aarch64.so.1 libc.so.6 libdnf.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libpeas-1.0.so.0 libpthread.so.0 libsmartcols.so.1"
RDEPENDS_microdnf = "glib2 glibc libdnf libgcc libpeas libsmartcols"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/microdnf-3.0.1-8.el8.aarch64.rpm \
          "

SRC_URI[microdnf.sha256sum] = "44b534ca3c076d779584e53ef24718df8542780fe9ecb5c16cc65bf0b14559f5"
