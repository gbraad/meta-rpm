SUMMARY = "generated recipe based on unixODBC srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libtool-ltdl pkgconfig-native readline"
RPM_SONAME_PROV_unixODBC = "libesoobS.so.2 libmimerS.so.2 libnn.so.2 libodbc.so.2 libodbccr.so.2 libodbcdrvcfg1S.so.2 libodbcdrvcfg2S.so.2 libodbcinst.so.2 libodbcminiS.so.2 libodbcmyS.so.2 libodbcnnS.so.2 libodbcpsqlS.so.2 libodbctxtS.so.2 liboplodbcS.so.2 liboraodbcS.so.2 libsapdbS.so.2 libtdsS.so.2 libtemplate.so.2"
RPM_SONAME_REQ_unixODBC = "ld-linux-aarch64.so.1 libc.so.6 libltdl.so.7 libpthread.so.0 libreadline.so.7"
RDEPENDS_unixODBC = "glibc libtool-ltdl readline"
RPM_SONAME_REQ_unixODBC-devel = "libesoobS.so.2 libmimerS.so.2 libnn.so.2 libodbccr.so.2 libodbcdrvcfg1S.so.2 libodbcdrvcfg2S.so.2 libodbcminiS.so.2 libodbcnnS.so.2 libodbctxtS.so.2 liboplodbcS.so.2 liboraodbcS.so.2 libsapdbS.so.2 libtemplate.so.2"
RPROVIDES_unixODBC-devel = "unixODBC-dev (= 2.3.7)"
RDEPENDS_unixODBC-devel = "pkgconf-pkg-config unixODBC"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/unixODBC-2.3.7-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/unixODBC-devel-2.3.7-1.el8.aarch64.rpm \
          "

SRC_URI[unixODBC.sha256sum] = "410a8dd291aca53e141bb6911e529468551cadc719ba6d87e086bb419536513a"
SRC_URI[unixODBC-devel.sha256sum] = "ac66dc77d53a006d5619fc58f40269f022a5c4d1a28461ba60e462504fdd1e77"
