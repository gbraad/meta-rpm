SUMMARY = "generated recipe based on jzlib srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jzlib = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_jzlib-demo = "jzlib"
RDEPENDS_jzlib-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jzlib-1.1.3-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jzlib-demo-1.1.3-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/jzlib-javadoc-1.1.3-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[jzlib.sha256sum] = "1ad4a726585210d80562fe39c63a1cbded0b02fc1070dfd2fd6b1197c4581fec"
SRC_URI[jzlib-demo.sha256sum] = "fa24b2fcd027da060b989e8b53632039e608493c565bfa654c8361bd9e9d229c"
SRC_URI[jzlib-javadoc.sha256sum] = "d40fe7a89029611bb4259cb65362baef6a8b7d2ba815d3d78b188b028cf2c693"
