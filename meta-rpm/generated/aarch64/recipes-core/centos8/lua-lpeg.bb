SUMMARY = "generated recipe based on lua-lpeg srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_lua-lpeg = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_lua-lpeg = "glibc lua-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/lua-lpeg-1.0.1-6.el8.aarch64.rpm \
          "

SRC_URI[lua-lpeg.sha256sum] = "9bcc43fd46651faeedd60b1e4506f0bd75309bdcf18595213ff2d52e28327c56"
