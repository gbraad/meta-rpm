SUMMARY = "generated recipe based on gcc-toolset-9-dwz srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-3.0"
RPM_LICENSE = "GPLv2+ and GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "elfutils pkgconfig-native"
RPM_SONAME_REQ_gcc-toolset-9-dwz = "ld-linux-aarch64.so.1 libc.so.6 libelf.so.1"
RDEPENDS_gcc-toolset-9-dwz = "elfutils-libelf gcc-toolset-9-runtime glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-dwz-0.12-1.1.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-dwz.sha256sum] = "f1fe6ee1cdefd30db9e03e807c6737052a708d39179da307187981271ce399bc"
