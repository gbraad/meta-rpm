SUMMARY = "generated recipe based on perl-Test-Taint srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Test-Taint = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Test-Taint = "glibc perl-Scalar-List-Utils perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Test-Taint-1.06-19.el8.aarch64.rpm \
          "

SRC_URI[perl-Test-Taint.sha256sum] = "1e691b37dd057be1a22da49189687eaac8e4006eddb86b0721416e14ba74bda3"
