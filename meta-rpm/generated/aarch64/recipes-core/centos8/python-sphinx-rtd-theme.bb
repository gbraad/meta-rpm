SUMMARY = "generated recipe based on python-sphinx_rtd_theme srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-sphinx_rtd_theme = "fontawesome-fonts-web platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-sphinx_rtd_theme-0.3.1-3.el8.noarch.rpm \
          "

SRC_URI[python3-sphinx_rtd_theme.sha256sum] = "959196e92eb709631628319b5f247bdd93fe914e866f2b0d4c81258f21a48208"
