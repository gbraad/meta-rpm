SUMMARY = "generated recipe based on pciutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "kmod pkgconfig-native zlib"
RPM_SONAME_REQ_pciutils = "ld-linux-aarch64.so.1 libc.so.6 libkmod.so.2 libpci.so.3"
RDEPENDS_pciutils = "bash glibc hwdata kmod-libs pciutils-libs"
RPM_SONAME_REQ_pciutils-devel = "libpci.so.3"
RPROVIDES_pciutils-devel = "pciutils-dev (= 3.5.6)"
RDEPENDS_pciutils-devel = "pciutils pciutils-libs pkgconf-pkg-config zlib-devel"
RPM_SONAME_PROV_pciutils-libs = "libpci.so.3"
RPM_SONAME_REQ_pciutils-libs = "ld-linux-aarch64.so.1 libc.so.6 libresolv.so.2"
RDEPENDS_pciutils-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pciutils-3.5.6-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pciutils-devel-3.5.6-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/pciutils-libs-3.5.6-4.el8.aarch64.rpm \
          "

SRC_URI[pciutils.sha256sum] = "d3a55ad8156e7fddd7b0bb9a301ac998c360a09cbccdcdf1c5195e9513b8fec8"
SRC_URI[pciutils-devel.sha256sum] = "bdff577a4420333c1ff88ce7070de5ffd5569a4b9af83e123a04ee5839bb6e7a"
SRC_URI[pciutils-libs.sha256sum] = "0318d07dbb89a81453621124207764f2b9eb3a12597a19e4630d3d963fb1ba19"
