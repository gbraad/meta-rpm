SUMMARY = "generated recipe based on mingw-binutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0 & GPL-3.0 & LGPL-3.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+ and GPLv3+ and LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mingw-binutils-generic = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_mingw-binutils-generic = "glibc"
RPM_SONAME_REQ_mingw32-binutils = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_mingw32-binutils = "glibc mingw-binutils-generic mingw32-filesystem"
RPM_SONAME_REQ_mingw64-binutils = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6"
RDEPENDS_mingw64-binutils = "glibc mingw-binutils-generic mingw64-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw-binutils-generic-2.30-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw32-binutils-2.30-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/mingw64-binutils-2.30-1.el8.aarch64.rpm \
          "

SRC_URI[mingw-binutils-generic.sha256sum] = "fa687182345095bbb894e9283c07ec51d68b6c03e517cc7d6104c912c7458ea0"
SRC_URI[mingw32-binutils.sha256sum] = "3f6b23d089587370da1b2899c05de119de8435cee50614250c760ae5c25710c2"
SRC_URI[mingw64-binutils.sha256sum] = "afe6dcc59792b1f3c5eb383ded752b36a507553d06e42ec229ea155286825ccc"
