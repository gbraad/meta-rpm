SUMMARY = "generated recipe based on libieee1284 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libieee1284 = "libieee1284.so.3"
RPM_SONAME_REQ_libieee1284 = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libieee1284 = "glibc"
RPM_SONAME_REQ_libieee1284-devel = "libieee1284.so.3"
RPROVIDES_libieee1284-devel = "libieee1284-dev (= 0.2.11)"
RDEPENDS_libieee1284-devel = "libieee1284"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libieee1284-0.2.11-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libieee1284-devel-0.2.11-28.el8.aarch64.rpm \
          "

SRC_URI[libieee1284.sha256sum] = "010fca2ec5dee81da5ad1d117e0c9f64521d628ce7c03424198cd9d97401f1bd"
SRC_URI[libieee1284-devel.sha256sum] = "d4f34513ad4e423b034169833605d32948ca15c449028e22cef67f6d9650edd3"
