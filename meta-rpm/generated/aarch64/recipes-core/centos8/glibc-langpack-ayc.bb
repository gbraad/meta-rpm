SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-ayc = "locale-base-ayc-pe (= 2.28) virtual-locale-ayc (= 2.28) virtual-locale-ayc-pe (= 2.28)"
RDEPENDS_glibc-langpack-ayc = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ayc-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-ayc.sha256sum] = "868895bf6faf219cde0f302807970064a7f79f747cc1255e356b04a3e6e14790"
