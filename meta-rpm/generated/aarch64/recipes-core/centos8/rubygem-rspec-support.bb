SUMMARY = "generated recipe based on rubygem-rspec-support srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-rspec-support = "rubygems"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/rubygem-rspec-support-3.7.1-2.el8.noarch.rpm \
          "

SRC_URI[rubygem-rspec-support.sha256sum] = "ce2123d27870f6c0121a275dc250fe85f2d63486a68104c22246589f5a5aa692"
