SUMMARY = "generated recipe based on dyninst srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "boost elfutils libgcc pkgconfig-native tbb"
RPM_SONAME_PROV_dyninst = "libcommon.so.10.1 libdynC_API.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libdyninstAPI_RT.so.10.1 libinstructionAPI.so.10.1 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libstackwalk.so.10.1 libsymLite.so.10.1 libsymtabAPI.so.10.1"
RPM_SONAME_REQ_dyninst = "ld-linux-aarch64.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libboost_timer.so.1.66.0 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 libgcc_s.so.1 libgomp.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RDEPENDS_dyninst = "boost-atomic boost-chrono boost-date-time boost-filesystem boost-system boost-thread boost-timer elfutils-libelf elfutils-libs glibc libgcc libgomp libstdc++ tbb"
RPM_SONAME_PROV_dyninst-devel = "libInst.so"
RPM_SONAME_REQ_dyninst-devel = "ld-linux-aarch64.so.1 libc.so.6 libcommon.so.10.1 libdynC_API.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libdyninstAPI_RT.so.10.1 libgcc_s.so.1 libinstructionAPI.so.10.1 libm.so.6 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libstackwalk.so.10.1 libstdc++.so.6 libsymLite.so.10.1 libsymtabAPI.so.10.1"
RPROVIDES_dyninst-devel = "dyninst-dev (= 10.1.0)"
RDEPENDS_dyninst-devel = "boost-devel cmake-filesystem dyninst glibc libgcc libstdc++ tbb-devel"
RDEPENDS_dyninst-static = "dyninst-devel"
RPM_SONAME_REQ_dyninst-testsuite = "ld-linux-aarch64.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libboost_timer.so.1.66.0 libc.so.6 libcommon.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libgcc_s.so.1 libgomp.so.1 libinstructionAPI.so.10.1 libm.so.6 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libpthread.so.0 libstackwalk.so.10.1 libstdc++.so.6 libsymtabAPI.so.10.1 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RDEPENDS_dyninst-testsuite = "boost-atomic boost-chrono boost-date-time boost-filesystem boost-system boost-thread boost-timer dyninst dyninst-devel dyninst-static glibc libgcc libgomp libstdc++ tbb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/dyninst-10.1.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dyninst-devel-10.1.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dyninst-doc-10.1.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dyninst-static-10.1.0-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dyninst-testsuite-10.1.0-4.el8.aarch64.rpm \
          "

SRC_URI[dyninst.sha256sum] = "5ac84ad36f733f5080a01fd32d0a88322f46abf0f22a031f0ba0a921ecac6799"
SRC_URI[dyninst-devel.sha256sum] = "de280d65d148714fd1d72e92d656949a03c26d99040451bec98a48e8bf859e97"
SRC_URI[dyninst-doc.sha256sum] = "a9f60575680b069c6807f11455bca16a54f9946cc89a62b1b6aa5607b6110db8"
SRC_URI[dyninst-static.sha256sum] = "16c8d42f864e9b080276e1e52b6fbd993cc2c568969549e417331f1770111420"
SRC_URI[dyninst-testsuite.sha256sum] = "067ba6eced681fa9fa0a4a31a44fc1c7f2f78dab8355b58f069894e50cf2e32b"
