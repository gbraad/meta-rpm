SUMMARY = "generated recipe based on libomp srpm"
DESCRIPTION = "Description"
LICENSE = "NCSA"
RPM_LICENSE = "NCSA"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "clang elfutils libffi libgcc pkgconfig-native"
RPM_SONAME_PROV_libomp = "libomp.so libomptarget.rtl.aarch64.so libomptarget.so"
RPM_SONAME_REQ_libomp = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libelf.so.1 libffi.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libomp = "elfutils-libelf glibc libffi libgcc libstdc++"
RPROVIDES_libomp-devel = "libomp-dev (= 9.0.1)"
RDEPENDS_libomp-devel = "clang-devel"
RDEPENDS_libomp-test = "bash clang gcc gcc-c++ libomp libomp-devel llvm python3-lit"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libomp-9.0.1-1.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libomp-devel-9.0.1-1.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libomp-test-9.0.1-1.module_el8.2.0+309+0c7b6b03.aarch64.rpm \
          "

SRC_URI[libomp.sha256sum] = "18433a75e4e819ec544d2a2f81dd31f1f97bd2d5b1a25cfdda335be53885a00d"
SRC_URI[libomp-devel.sha256sum] = "7dd36e94426eabc061bbc5a61458c853c8fce646be5659ccd06e83c6b6ee08c0"
SRC_URI[libomp-test.sha256sum] = "26372693f315dd4f9a295a42e675f31c791df9dc692958f727a56241f53c997d"
