SUMMARY = "generated recipe based on python-setuptools srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_platform-python-setuptools = "platform-python"
RDEPENDS_python3-setuptools = "platform-python platform-python-setuptools"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/platform-python-setuptools-39.2.0-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-setuptools-39.2.0-5.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-setuptools-wheel-39.2.0-5.el8.noarch.rpm \
          "

SRC_URI[platform-python-setuptools.sha256sum] = "d300ce0fa3a0c6a91d50c5f6a8f5988c014b07b431267b857fcd057b8bf10301"
SRC_URI[python3-setuptools.sha256sum] = "57df4bd86adfff3124c8cbb345ad73f6063f6be5629db123210918a2f37835f7"
SRC_URI[python3-setuptools-wheel.sha256sum] = "fe32f87d975ed2bd5f1c854961caa05fec47f6a5f9bd2a28610be907aeb71f4e"
