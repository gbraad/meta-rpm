SUMMARY = "generated recipe based on perl-HTTP-Date srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-HTTP-Date = "perl-Exporter perl-Time-Local perl-TimeDate perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-HTTP-Date-6.02-18.el8.noarch.rpm \
          "

SRC_URI[perl-HTTP-Date.sha256sum] = "70e1e04c0d46fa657a03b052c0b1676dca009d2a5ea2c80be3ef91e5dc1ab3eb"
