SUMMARY = "generated recipe based on apache-commons-jxpath srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-jxpath = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-jxpath-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-jxpath-1.3-29.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-jxpath-javadoc-1.3-29.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-jxpath.sha256sum] = "2b5c8ea1b8341d4fbfdb1b9f41acd2b374977c56f295651245d61fc0d6d205ab"
SRC_URI[apache-commons-jxpath-javadoc.sha256sum] = "b497e46a56d482eec4264feefb7d3f3db0d007b36959167a6036bbfe32dc9716"
