SUMMARY = "generated recipe based on sombok srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | ClArtistic"
RPM_LICENSE = "GPLv2+ or Artistic clarified"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libthai pkgconfig-native"
RPM_SONAME_PROV_sombok = "libsombok.so.3"
RPM_SONAME_REQ_sombok = "ld-linux-aarch64.so.1 libc.so.6 libthai.so.0"
RDEPENDS_sombok = "glibc libthai"
RPM_SONAME_REQ_sombok-devel = "libsombok.so.3"
RPROVIDES_sombok-devel = "sombok-dev (= 2.4.0)"
RDEPENDS_sombok-devel = "pkgconf-pkg-config sombok"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sombok-2.4.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sombok-devel-2.4.0-6.el8.aarch64.rpm \
          "

SRC_URI[sombok.sha256sum] = "ae17d7acc7b8c3d30c95305d9689d61ca596f842ba8f377d573e696f6817db9b"
SRC_URI[sombok-devel.sha256sum] = "690ee380deac8ff14b3313c26e524ec7515dcd04c24f5a7205e7066a9a41f410"
