SUMMARY = "generated recipe based on libnice srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & MPL-1.1"
RPM_LICENSE = "LGPLv2 and MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 gnutls gnutls-libs gstreamer1.0 gupnp gupnp-igd pkgconfig-native"
RPM_SONAME_PROV_libnice = "libnice.so.10"
RPM_SONAME_REQ_libnice = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgthread-2.0.so.0 libgupnp-1.0.so.4 libgupnp-igd-1.0.so.4 libpthread.so.0 librt.so.1"
RDEPENDS_libnice = "glib2 glibc gnutls gupnp gupnp-igd"
RPM_SONAME_REQ_libnice-devel = "libnice.so.10"
RPROVIDES_libnice-devel = "libnice-dev (= 0.1.14)"
RDEPENDS_libnice-devel = "glib2-devel gnutls-devel gupnp-igd-devel libnice pkgconf-pkg-config"
RPM_SONAME_PROV_libnice-gstreamer1 = "libgstnice.so"
RPM_SONAME_REQ_libnice-gstreamer1 = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libgstbase-1.0.so.0 libgstreamer-1.0.so.0 libgthread-2.0.so.0 libgupnp-1.0.so.4 libgupnp-igd-1.0.so.4 libnice.so.10 libpthread.so.0 librt.so.1"
RDEPENDS_libnice-gstreamer1 = "glib2 glibc gnutls gstreamer1 gupnp gupnp-igd libnice"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libnice-0.1.14-7.20180504git34d6044.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libnice-gstreamer1-0.1.14-7.20180504git34d6044.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libnice-devel-0.1.14-7.20180504git34d6044.el8.aarch64.rpm \
          "

SRC_URI[libnice.sha256sum] = "4f630e3de1d89f73903962e20ac29de4cf1a5f4f3d3eebce5a9c3e244996730f"
SRC_URI[libnice-devel.sha256sum] = "c6a4dad4304a0f19a5fb7381efe1120d9258c21f5653388db77b8b1d1d343887"
SRC_URI[libnice-gstreamer1.sha256sum] = "ba7fd3dc68295e57adfc21c71e65c81ea7e31fe55166929d1c31e287220fc160"
