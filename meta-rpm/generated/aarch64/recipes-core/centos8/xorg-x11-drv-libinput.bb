SUMMARY = "generated recipe based on xorg-x11-drv-libinput srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libinput pkgconfig-native"
RPM_SONAME_PROV_xorg-x11-drv-libinput = "libinput_drv.so"
RPM_SONAME_REQ_xorg-x11-drv-libinput = "ld-linux-aarch64.so.1 libc.so.6 libinput.so.10 libm.so.6"
RDEPENDS_xorg-x11-drv-libinput = "glibc libinput xkeyboard-config xorg-x11-server-Xorg"
RPROVIDES_xorg-x11-drv-libinput-devel = "xorg-x11-drv-libinput-dev (= 0.29.0)"
RDEPENDS_xorg-x11-drv-libinput-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-libinput-0.29.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xorg-x11-drv-libinput-devel-0.29.0-1.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-drv-libinput.sha256sum] = "f7dae248b6842ea6286c35d588bdac5d8ec436e088a8e7072ef7f339d311e54e"
SRC_URI[xorg-x11-drv-libinput-devel.sha256sum] = "2d21470e58739092c7e49ae3f69ef43871f6cad5a4a957c329ae37807de471e8"
