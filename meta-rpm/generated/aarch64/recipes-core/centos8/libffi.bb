SUMMARY = "generated recipe based on libffi srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libffi = "libffi.so.6"
RPM_SONAME_REQ_libffi = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libffi = "glibc"
RPM_SONAME_REQ_libffi-devel = "libffi.so.6"
RPROVIDES_libffi-devel = "libffi-dev (= 3.1)"
RDEPENDS_libffi-devel = "bash info libffi pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libffi-3.1-21.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libffi-devel-3.1-21.el8.aarch64.rpm \
          "

SRC_URI[libffi.sha256sum] = "bc2fc704cf93c3d23b5dd08190504888c5c5de9e36ad5503bca83918c9a0f0eb"
SRC_URI[libffi-devel.sha256sum] = "a8cec93da6703aa3a2df71216ad516bdf1d8fb6d5ea49ec8677d28d44de228e8"
