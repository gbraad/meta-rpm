SUMMARY = "generated recipe based on libedit srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_PROV_libedit = "libedit.so.0"
RPM_SONAME_REQ_libedit = "ld-linux-aarch64.so.1 libc.so.6 libtinfo.so.6"
RDEPENDS_libedit = "glibc ncurses-libs"
RPM_SONAME_REQ_libedit-devel = "libedit.so.0"
RPROVIDES_libedit-devel = "libedit-dev (= 3.1)"
RDEPENDS_libedit-devel = "libedit ncurses-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libedit-3.1-23.20170329cvs.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libedit-devel-3.1-23.20170329cvs.el8.aarch64.rpm \
          "

SRC_URI[libedit.sha256sum] = "ed1bf4961e5dd6e7c813655a18059f9a854d6512a6f7b3ed3077c280e8d6c13c"
SRC_URI[libedit-devel.sha256sum] = "d4a5f4f5448e5f72d3bfc75c67687f55ddfa6ecfb143f7ddc6f2eace57d9e86f"
