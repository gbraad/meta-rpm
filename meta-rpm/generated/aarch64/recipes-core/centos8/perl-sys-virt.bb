SUMMARY = "generated recipe based on perl-Sys-Virt srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPLv2+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libvirt perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sys-Virt = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0 libvirt.so.0"
RDEPENDS_perl-Sys-Virt = "glibc libvirt-libs perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Sys-Virt-4.5.0-5.module_el8.2.0+320+13f867d7.aarch64.rpm \
          "

SRC_URI[perl-Sys-Virt.sha256sum] = "7c0079b3947156a50366db932450754279c0fe8ec25ab69a4e9dcb023cad0f57"
