SUMMARY = "generated recipe based on xorg-x11-drv-ati srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libdrm libpciaccess mesa pkgconfig-native systemd-libs"
RPM_SONAME_REQ_xorg-x11-drv-ati = "ld-linux-aarch64.so.1 libc.so.6 libdrm_radeon.so.1 libgbm.so.1 libpciaccess.so.0 libudev.so.1"
RDEPENDS_xorg-x11-drv-ati = "glibc libdrm libpciaccess mesa-libgbm systemd-libs xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-ati-19.0.1-2.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-drv-ati.sha256sum] = "bc0e38a9f2c5587ffd01ec15aff5a16c5ceb9d5324480ecf63d69283b2ab1f7c"
