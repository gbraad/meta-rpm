SUMMARY = "generated recipe based on libthai srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libdatrie pkgconfig-native"
RPM_SONAME_PROV_libthai = "libthai.so.0"
RPM_SONAME_REQ_libthai = "ld-linux-aarch64.so.1 libc.so.6 libdatrie.so.1"
RDEPENDS_libthai = "glibc libdatrie"
RPM_SONAME_REQ_libthai-devel = "libthai.so.0"
RPROVIDES_libthai-devel = "libthai-dev (= 0.1.27)"
RDEPENDS_libthai-devel = "libdatrie-devel libthai pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libthai-0.1.27-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libthai-devel-0.1.27-2.el8.aarch64.rpm \
          "

SRC_URI[libthai.sha256sum] = "23c85217f87a49bdb57b2f2d379856b4036b46d31fd1902f3c8d8c0fa932177e"
SRC_URI[libthai-devel.sha256sum] = "4828a07e5fdd93490c8e7b38a53040ed81ad288691b182de655ff3fb2022d5ac"
