SUMMARY = "generated recipe based on perl-Compress-Raw-Zlib srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & Zlib"
RPM_LICENSE = "(GPL+ or Artistic) and zlib"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native zlib"
RPM_SONAME_REQ_perl-Compress-Raw-Zlib = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0 libz.so.1"
RDEPENDS_perl-Compress-Raw-Zlib = "glibc perl-Carp perl-Exporter perl-constant perl-interpreter perl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Compress-Raw-Zlib-2.081-1.el8.aarch64.rpm \
          "

SRC_URI[perl-Compress-Raw-Zlib.sha256sum] = "94efbec00799a7a36fea4a972a1438c22b29ce0e8997e0d6413225ff03fdefa9"
