SUMMARY = "generated recipe based on cmake srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & MIT & Zlib"
RPM_LICENSE = "BSD and MIT and zlib"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "curl expat libarchive libgcc libuv ncurses pkgconfig-native zlib"
RPM_SONAME_REQ_cmake = "ld-linux-aarch64.so.1 libarchive.so.13 libc.so.6 libcurl.so.4 libdl.so.2 libexpat.so.1 libform.so.6 libgcc_s.so.1 libm.so.6 libncurses.so.6 libpthread.so.0 libstdc++.so.6 libtinfo.so.6 libuv.so.1 libz.so.1"
RDEPENDS_cmake = "cmake-data cmake-filesystem cmake-rpm-macros expat glibc libarchive libcurl libgcc libstdc++ libuv ncurses-libs zlib"
RDEPENDS_cmake-data = "cmake cmake-filesystem cmake-rpm-macros emacs-filesystem"
RDEPENDS_cmake-rpm-macros = "platform-python rpm"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cmake-3.11.4-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cmake-data-3.11.4-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cmake-doc-3.11.4-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cmake-rpm-macros-3.11.4-7.el8.noarch.rpm \
          "

SRC_URI[cmake.sha256sum] = "32312b3f68af605d7358b3d9d8670f084c823def26eb7adbcf5219f3498778a0"
SRC_URI[cmake-data.sha256sum] = "bda84c7ee0f954807792cc8dbf63cebe62a300d4c4660d3895a761e2dd486d9d"
SRC_URI[cmake-doc.sha256sum] = "2af71538be9a5c9ef4bf53622d69d22862a84f63234241c45083ff6207b3b1b9"
SRC_URI[cmake-rpm-macros.sha256sum] = "4da0924e3a6582b3b63a0201facdb3e38ada928098d3c29ca24d342930e069de"
