SUMMARY = "generated recipe based on python-slip srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-slip = "platform-python python3-libselinux"
RDEPENDS_python3-slip-dbus = "platform-python python3-dbus python3-decorator python3-six python3-slip"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-slip-0.6.4-11.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-slip-dbus-0.6.4-11.el8.noarch.rpm \
          "

SRC_URI[python3-slip.sha256sum] = "233e5f78027b684e5aaac1395cc574aea3039059bf86eb4494422bc74216a396"
SRC_URI[python3-slip-dbus.sha256sum] = "563b3741d26b6af963fdb7b0634e0791445a707d0b6093ac71c3224299241639"
