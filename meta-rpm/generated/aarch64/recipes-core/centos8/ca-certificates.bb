SUMMARY = "generated recipe based on ca-certificates srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_ca-certificates = "bash coreutils grep p11-kit p11-kit-trust sed"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ca-certificates-2020.2.41-80.0.el8_2.noarch.rpm \
          "

SRC_URI[ca-certificates.sha256sum] = "dc984aefb28c2d11ff6f6f1a794d04d300744ea0cfc9b869368f2f1acfc419be"
