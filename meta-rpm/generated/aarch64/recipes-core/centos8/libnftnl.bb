SUMMARY = "generated recipe based on libnftnl srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libmnl pkgconfig-native"
RPM_SONAME_PROV_libnftnl = "libnftnl.so.11"
RPM_SONAME_REQ_libnftnl = "ld-linux-aarch64.so.1 libc.so.6 libmnl.so.0"
RDEPENDS_libnftnl = "glibc libmnl"
RPM_SONAME_REQ_libnftnl-devel = "libnftnl.so.11"
RPROVIDES_libnftnl-devel = "libnftnl-dev (= 1.1.5)"
RDEPENDS_libnftnl-devel = "libmnl-devel libnftnl pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnftnl-1.1.5-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libnftnl-devel-1.1.5-4.el8.aarch64.rpm \
          "

SRC_URI[libnftnl.sha256sum] = "c85fbf0045e810a8a7df257799a82e32fee141db8119e9f1eb7abdb96553127f"
SRC_URI[libnftnl-devel.sha256sum] = "d3813a90a4e81959f2af4b567e9b9c3f2b49cb8a0e8d78d8259d0dcaf9ee7369"
