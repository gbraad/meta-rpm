SUMMARY = "generated recipe based on scap-workbench srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc openscap pkgconfig-native qt5-qtbase qt5-qtxmlpatterns"
RPM_SONAME_REQ_scap-workbench = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Widgets.so.5 libQt5XmlPatterns.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libopenscap.so.25 libstdc++.so.6"
RDEPENDS_scap-workbench = "bash glibc libgcc libstdc++ openscap openscap-utils openssh-askpass openssh-clients polkit qt5-qtbase qt5-qtbase-gui qt5-qtxmlpatterns scap-security-guide util-linux"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/scap-workbench-1.2.0-4.el8.aarch64.rpm \
          "

SRC_URI[scap-workbench.sha256sum] = "90c88903d3eb69de7f610b21f5d6c1e51fe3d628531b6bcf1f65dca55d5550bc"
