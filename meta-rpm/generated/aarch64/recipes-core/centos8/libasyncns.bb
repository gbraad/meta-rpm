SUMMARY = "generated recipe based on libasyncns srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libasyncns = "libasyncns.so.0"
RPM_SONAME_REQ_libasyncns = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libresolv.so.2"
RDEPENDS_libasyncns = "glibc"
RPM_SONAME_REQ_libasyncns-devel = "libasyncns.so.0"
RPROVIDES_libasyncns-devel = "libasyncns-dev (= 0.8)"
RDEPENDS_libasyncns-devel = "libasyncns pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libasyncns-0.8-14.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libasyncns-devel-0.8-14.el8.aarch64.rpm \
          "

SRC_URI[libasyncns.sha256sum] = "794b2fe3dc26b77b4f76b9f3bbef85bb0e6a026e255fe9f76a31ab53abb699e4"
SRC_URI[libasyncns-devel.sha256sum] = "b8cf6db48c028ce410d66ce3b8fda85b6f79443d290f7d62b925bfe01527bc12"
