SUMMARY = "generated recipe based on perl-Test-Pod-Coverage srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0"
RPM_LICENSE = "Artistic 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Pod-Coverage = "perl-Pod-Coverage perl-Test-Simple perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Test-Pod-Coverage-1.10-10.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Pod-Coverage.sha256sum] = "da62c9a87e54d20cc0c20839aafbe7e455f93c54942013cb700e88a63370f4fe"
