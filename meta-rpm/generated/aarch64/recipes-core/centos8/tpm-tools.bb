SUMMARY = "generated recipe based on tpm-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CPL-1.0"
RPM_LICENSE = "CPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native trousers"
RPM_SONAME_PROV_tpm-tools = "libtpm_unseal.so.1"
RPM_SONAME_REQ_tpm-tools = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libtspi.so.1"
RDEPENDS_tpm-tools = "glibc openssl-libs trousers-lib"
RPM_SONAME_REQ_tpm-tools-devel = "libtpm_unseal.so.1"
RPROVIDES_tpm-tools-devel = "tpm-tools-dev (= 1.3.9)"
RDEPENDS_tpm-tools-devel = "tpm-tools"
RPM_SONAME_REQ_tpm-tools-pkcs11 = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libtspi.so.1"
RDEPENDS_tpm-tools-pkcs11 = "glibc opencryptoki-libs openssl-libs trousers-lib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tpm-tools-1.3.9-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tpm-tools-pkcs11-1.3.9-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/tpm-tools-devel-1.3.9-7.el8.aarch64.rpm \
          "

SRC_URI[tpm-tools.sha256sum] = "4888c9d4517fd2e93bdc74cc9e359cfcf17c47469c1b9085f52dcdcaab6d75a2"
SRC_URI[tpm-tools-devel.sha256sum] = "bd4ef9a0720282c739b19150be4fa6108aa029acf60dbbd1979af2574c21097e"
SRC_URI[tpm-tools-pkcs11.sha256sum] = "28311b0ca7b6543d811082e9435290a79a7a4afa98bda7b750b0becc67ec5cf6"
