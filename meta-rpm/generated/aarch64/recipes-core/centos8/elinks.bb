SUMMARY = "generated recipe based on elinks srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 e2fsprogs expat gpm krb5-libs libidn2 lua openssl pkgconfig-native zlib"
RPM_SONAME_REQ_elinks = "ld-linux-aarch64.so.1 libbz2.so.1 libc.so.6 libcom_err.so.2 libcrypto.so.1.1 libdl.so.2 libexpat.so.1 libgpm.so.2 libgssapi_krb5.so.2 libidn2.so.0 libk5crypto.so.3 libkrb5.so.3 liblua-5.3.so libm.so.6 libssl.so.1.1 libz.so.1"
RDEPENDS_elinks = "bash bzip2-libs chkconfig coreutils expat glibc gpm-libs krb5-libs libcom_err libidn2 lua-libs openssl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/elinks-0.12-0.58.pre6.el8.aarch64.rpm \
          "

SRC_URI[elinks.sha256sum] = "a873dd1c7a0c3f35bb8e16e13853552c91093e3e25bbe5144c57735882f6a3c2"
