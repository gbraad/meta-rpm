SUMMARY = "generated recipe based on opensp srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_opensp = "libosp.so.5"
RPM_SONAME_REQ_opensp = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_opensp = "glibc libgcc libstdc++ sgml-common"
RPM_SONAME_REQ_opensp-devel = "libosp.so.5"
RPROVIDES_opensp-devel = "opensp-dev (= 1.5.2)"
RDEPENDS_opensp-devel = "opensp"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/opensp-1.5.2-28.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/opensp-devel-1.5.2-28.el8.aarch64.rpm \
          "

SRC_URI[opensp.sha256sum] = "f47416694121c8f58facedcf1d20f84dd9b3e0c07480ebbf517f772007df532e"
SRC_URI[opensp-devel.sha256sum] = "ca264d6d53b4ba90c6ce5b60def52161708ab76c429dcd960b862e804f6d20fd"
