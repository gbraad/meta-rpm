SUMMARY = "generated recipe based on maven-shared-incremental srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-shared-incremental = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-shared-utils plexus-containers-component-annotations"
RDEPENDS_maven-shared-incremental-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-shared-incremental-1.1-14.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-shared-incremental-javadoc-1.1-14.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-shared-incremental.sha256sum] = "8eff74a8c3e522f7b9815710cbd09300ae21453209da75adb8f4908343eca23e"
SRC_URI[maven-shared-incremental-javadoc.sha256sum] = "17e22af746cdff388e5fbad44500ef0c729e7835e2902bd22f33b7d496382d9b"
