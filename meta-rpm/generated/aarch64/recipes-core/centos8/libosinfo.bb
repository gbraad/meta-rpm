SUMMARY = "generated recipe based on libosinfo srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libxml2 libxslt pkgconfig-native"
RPM_SONAME_PROV_libosinfo = "libosinfo-1.0.so.0"
RPM_SONAME_REQ_libosinfo = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libm.so.6 libpthread.so.0 libxml2.so.2 libxslt.so.1"
RDEPENDS_libosinfo = "glib2 glibc gvfs hwdata libxml2 libxslt osinfo-db osinfo-db-tools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libosinfo-1.5.0-3.el8.aarch64.rpm \
          "

SRC_URI[libosinfo.sha256sum] = "ea29113def55ae2669d0a30623b13004c3dc7084f3862c686f6ad79098f33c3a"
