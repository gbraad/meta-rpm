SUMMARY = "generated recipe based on dwarves srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "elfutils pkgconfig-native zlib"
RPM_SONAME_REQ_dwarves = "ld-linux-aarch64.so.1 libc.so.6 libdw.so.1 libdwarves.so.1 libdwarves_emit.so.1 libdwarves_reorganize.so.1 libelf.so.1 libz.so.1"
RDEPENDS_dwarves = "bash elfutils-libelf elfutils-libs glibc libdwarves1 platform-python zlib"
RPM_SONAME_PROV_libdwarves1 = "libdwarves.so.1 libdwarves_emit.so.1 libdwarves_reorganize.so.1"
RPM_SONAME_REQ_libdwarves1 = "ld-linux-aarch64.so.1 libc.so.6 libdw.so.1 libelf.so.1 libz.so.1"
RDEPENDS_libdwarves1 = "elfutils-libelf elfutils-libs glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dwarves-1.15-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdwarves1-1.15-5.el8.aarch64.rpm \
          "

SRC_URI[dwarves.sha256sum] = "76b78002014e35852b1e072fd3bcd14cce69063850cbc6b7cdf947b3604e5380"
SRC_URI[libdwarves1.sha256sum] = "47e69c310ea48c53159c1f9f2f301261c78bd762959885edfe9fb4935ec049f7"
