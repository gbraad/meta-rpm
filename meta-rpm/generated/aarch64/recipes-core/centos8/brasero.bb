SUMMARY = "generated recipe based on brasero srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo gdk-pixbuf glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base gtk+3 libcanberra libnotify pango pkgconfig-native totem-pl-parser"
RPM_SONAME_REQ_brasero-devel = "libbrasero-burn3.so.1 libbrasero-media3.so.1 libbrasero-utils3.so.1"
RPROVIDES_brasero-devel = "brasero-dev (= 3.12.2)"
RDEPENDS_brasero-devel = "brasero-libs glib2-devel gtk3-devel pkgconf-pkg-config"
RPM_SONAME_PROV_brasero-libs = "libbrasero-burn3.so.1 libbrasero-media3.so.1 libbrasero-utils3.so.1"
RPM_SONAME_REQ_brasero-libs = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libcanberra-gtk3.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstpbutils-1.0.so.0 libgstreamer-1.0.so.0 libgtk-3.so.0 libnotify.so.4 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libtotem-plparser.so.18"
RDEPENDS_brasero-libs = "cairo gdk-pixbuf2 glib2 glibc gstreamer1 gstreamer1-plugins-base gtk3 libcanberra-gtk3 libnotify pango totem-pl-parser"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/brasero-devel-3.12.2-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/brasero-libs-3.12.2-4.el8.aarch64.rpm \
          "

SRC_URI[brasero-devel.sha256sum] = "ee57cc22fd1843caad312432ac8f15c8fc509d251883da70a701d93d9218d865"
SRC_URI[brasero-libs.sha256sum] = "c8562a018c1b824023ab7f7fa856fbb9a1579db34c5e3c1b77676031802cf62c"
