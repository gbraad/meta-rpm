SUMMARY = "generated recipe based on glade srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo gdk-pixbuf glib-2.0 gtk+3 libxml2 pango pkgconfig-native platform-python3 webkit2gtk3"
RPM_SONAME_REQ_glade-devel = "libgladeui-2.so.6"
RPROVIDES_glade-devel = "glade-dev (= 3.22.1)"
RDEPENDS_glade-devel = "glade-libs gtk3-devel libxml2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_glade-libs = "libgladegtk.so libgladepython.so libgladeui-2.so.6 libgladewebkit2gtk.so"
RPM_SONAME_REQ_glade-libs = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libpython3.6m.so.1.0 libwebkit2gtk-4.0.so.37 libxml2.so.2"
RDEPENDS_glade-libs = "cairo gdk-pixbuf2 glib2 glibc gtk3 libxml2 pango python3-libs webkit2gtk3"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/glade-libs-3.22.1-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/glade-devel-3.22.1-1.el8.aarch64.rpm \
          "

SRC_URI[glade-devel.sha256sum] = "0539b1d9298f7260628504e4004d59a6fcadf547fde1e4c12ce84728299cb9ed"
SRC_URI[glade-libs.sha256sum] = "8ed6098aaedde34960c2ec9bd4d9d76ce6d9c9327a4e0a1e9bc4f12cc9275a0e"
