SUMMARY = "generated recipe based on exempi srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "expat libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_exempi = "libexempi.so.3"
RPM_SONAME_REQ_exempi = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libexpat.so.1 libgcc_s.so.1 libm.so.6 librt.so.1 libstdc++.so.6 libz.so.1"
RDEPENDS_exempi = "expat glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_exempi-devel = "libexempi.so.3"
RPROVIDES_exempi-devel = "exempi-dev (= 2.4.5)"
RDEPENDS_exempi-devel = "exempi pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/exempi-2.4.5-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/exempi-devel-2.4.5-2.el8.aarch64.rpm \
          "

SRC_URI[exempi.sha256sum] = "c6aea1ec2ec00973de037bb74c4d45a34bdc30d501c0829591e5013da75e3842"
SRC_URI[exempi-devel.sha256sum] = "7915f79ab03cc4e5ce2872badf01d6b8e688fe7a9a357aff2ccead8b171536d9"
