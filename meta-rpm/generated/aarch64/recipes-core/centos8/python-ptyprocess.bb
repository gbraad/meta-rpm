SUMMARY = "generated recipe based on python-ptyprocess srpm"
DESCRIPTION = "Description"
LICENSE = "ISC"
RPM_LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-ptyprocess = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-ptyprocess-0.5.2-4.el8.noarch.rpm \
          "

SRC_URI[python3-ptyprocess.sha256sum] = "499e48b35f3b5f5da45031fa78fba559fee6a480ecb106e6c300eb8344510958"
