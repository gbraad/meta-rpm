SUMMARY = "generated recipe based on perl-NetAddr-IP srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & (GPL-2.0 | ClArtistic)"
RPM_LICENSE = "GPLv2+ and (GPLv2+ or Artistic clarified)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-NetAddr-IP = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-NetAddr-IP = "glibc perl-Carp perl-Exporter perl-Math-BigInt perl-Socket perl-Socket6 perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-NetAddr-IP-4.079-7.el8.aarch64.rpm \
          "

SRC_URI[perl-NetAddr-IP.sha256sum] = "27fe8650b7ed11f87b2d55bc76c75108e845bfb13c513ed1738268ddcbac182e"
