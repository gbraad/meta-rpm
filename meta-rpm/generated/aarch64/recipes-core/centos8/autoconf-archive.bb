SUMMARY = "generated recipe based on autoconf-archive srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+ with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_autoconf-archive = "autoconf bash info"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/autoconf-archive-2018.03.13-1.el8.noarch.rpm \
          "

SRC_URI[autoconf-archive.sha256sum] = "b18a037d3df87d1ec80249fe1ba7d2d2c62aba1dbbbc69f30ca7845706ed55b0"
