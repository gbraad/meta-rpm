SUMMARY = "generated recipe based on hunspell-fr srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-2.0"
RPM_LICENSE = "MPLv2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-fr = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-fr-6.2-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-fr.sha256sum] = "28caf7c516341e351c8888e80489b2dfd4f524ef603b2201897060e8e5661175"
