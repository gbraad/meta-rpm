SUMMARY = "generated recipe based on lshw srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native sqlite3"
RPM_SONAME_REQ_lshw = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libresolv.so.2 libsqlite3.so.0 libstdc++.so.6"
RDEPENDS_lshw = "glibc hwdata libgcc libstdc++ sqlite-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lshw-B.02.18-23.el8.aarch64.rpm \
          "

SRC_URI[lshw.sha256sum] = "23699bbb8707b00766e15a9c8bbaf1d0fc763be3b857d044b74b599f92a96ec5"
