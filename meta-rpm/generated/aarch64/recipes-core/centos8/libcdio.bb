SUMMARY = "generated recipe based on libcdio srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc ncurses pkgconfig-native"
RPM_SONAME_PROV_libcdio = "libcdio++.so.1 libcdio.so.18 libiso9660++.so.0 libiso9660.so.11 libudf.so.0"
RPM_SONAME_REQ_libcdio = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libncurses.so.6 libstdc++.so.6 libtinfo.so.6"
RDEPENDS_libcdio = "bash glibc info libgcc libstdc++ ncurses-libs"
RPM_SONAME_REQ_libcdio-devel = "libcdio++.so.1 libcdio.so.18 libiso9660++.so.0 libiso9660.so.11 libudf.so.0"
RPROVIDES_libcdio-devel = "libcdio-dev (= 2.0.0)"
RDEPENDS_libcdio-devel = "libcdio pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libcdio-2.0.0-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libcdio-devel-2.0.0-3.el8.aarch64.rpm \
          "

SRC_URI[libcdio.sha256sum] = "152229730f4a31e97cbf966c4cf03d6976ce6356a3dc779b68845fb8ec3d32fe"
SRC_URI[libcdio-devel.sha256sum] = "f0c881ca9557cfa79f706476ed36752fc601b75ffc29025457db78aebe6a1df5"
