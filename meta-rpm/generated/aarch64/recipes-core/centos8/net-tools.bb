SUMMARY = "generated recipe based on net-tools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libselinux pkgconfig-native"
RPM_SONAME_REQ_net-tools = "ld-linux-aarch64.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_net-tools = "bash glibc libselinux systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/net-tools-2.0-0.51.20160912git.el8.aarch64.rpm \
          "

SRC_URI[net-tools.sha256sum] = "cb5a8b40db52e72cf0ac4470af5861fcc5fc55656553ff299acb1c11aee6dd5c"
