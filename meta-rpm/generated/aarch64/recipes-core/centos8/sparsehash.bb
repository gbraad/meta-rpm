SUMMARY = "generated recipe based on sparsehash srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_sparsehash-devel = "sparsehash-dev (= 2.0.2)"
RDEPENDS_sparsehash-devel = "pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sparsehash-devel-2.0.2-8.el8.aarch64.rpm \
          "

SRC_URI[sparsehash-devel.sha256sum] = "c47b468295190afce2a5746852ec5234f5f1f69ce76858674386aac412884e8e"
