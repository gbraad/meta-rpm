SUMMARY = "generated recipe based on apache-commons-collections srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-commons-collections = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_apache-commons-collections-javadoc = "javapackages-filesystem"
RDEPENDS_apache-commons-collections-testframework = "apache-commons-collections java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-collections-3.2.2-10.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-collections-javadoc-3.2.2-10.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/apache-commons-collections-testframework-3.2.2-10.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-commons-collections.sha256sum] = "abb41ba48ed880483d26f1d254027789b83ac23ddae83cc91ef22acabfbb272f"
SRC_URI[apache-commons-collections-javadoc.sha256sum] = "74f36f7bc20bb9203ce80ed41896add83f640f25169cc946a3acfce50a642e16"
SRC_URI[apache-commons-collections-testframework.sha256sum] = "0987646dccab07377b0ae2c11c07276c68b9adb2f738ae8e21a10ab0ef376ef5"
