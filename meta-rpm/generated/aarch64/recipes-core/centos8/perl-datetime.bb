SUMMARY = "generated recipe based on perl-DateTime srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0"
RPM_LICENSE = "Artistic 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-DateTime = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-DateTime = "glibc perl-Carp perl-DateTime-Locale perl-DateTime-TimeZone perl-Dist-CheckConflicts perl-Params-ValidationCompiler perl-Scalar-List-Utils perl-Specio perl-Try-Tiny perl-interpreter perl-libs perl-namespace-autoclean perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-DateTime-1.50-1.el8.aarch64.rpm \
          "

SRC_URI[perl-DateTime.sha256sum] = "422a837dd83ca56fddb2caa7b0a8abd150b9fda5ee5a9f38f7240940f104e7b8"
