SUMMARY = "generated recipe based on libgovirt srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libsoup-2.4 libxml2 pkgconfig-native rest"
RPM_SONAME_PROV_libgovirt = "libgovirt.so.2"
RPM_SONAME_REQ_libgovirt = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgthread-2.0.so.0 libpthread.so.0 librest-0.7.so.0 libsoup-2.4.so.1 libxml2.so.2"
RDEPENDS_libgovirt = "glib2 glibc libsoup libxml2 rest"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libgovirt-0.3.4-12.el8_2.aarch64.rpm \
          "

SRC_URI[libgovirt.sha256sum] = "b552ceb094333fcce7ec5df64be8b9b1b01b03ffa58d37bddb6ca54e3c012eeb"
