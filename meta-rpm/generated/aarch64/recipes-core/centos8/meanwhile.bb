SUMMARY = "generated recipe based on meanwhile srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 pkgconfig-native"
RPM_SONAME_PROV_meanwhile = "libmeanwhile.so.1"
RPM_SONAME_REQ_meanwhile = "ld-linux-aarch64.so.1 libc.so.6 libglib-2.0.so.0 libm.so.6"
RDEPENDS_meanwhile = "glib2 glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/meanwhile-1.1.0-23.el8.aarch64.rpm \
          "

SRC_URI[meanwhile.sha256sum] = "f3ec442405f1dd249a482f486ad2aeea79774bcc41b2348149d64daf1c634890"
