SUMMARY = "generated recipe based on audit srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libcap-ng pkgconfig-native"
RPM_SONAME_PROV_audit-libs = "libaudit.so.1 libauparse.so.0"
RPM_SONAME_REQ_audit-libs = "ld-linux-aarch64.so.1 libc.so.6 libcap-ng.so.0 libpthread.so.0"
RDEPENDS_audit-libs = "glibc libcap-ng"
RPM_SONAME_REQ_audit-libs-devel = "libaudit.so.1 libauparse.so.0"
RPROVIDES_audit-libs-devel = "audit-libs-dev (= 3.0)"
RDEPENDS_audit-libs-devel = "audit-libs kernel-headers pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/audit-libs-3.0-0.17.20191104git1c2f876.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/audit-libs-devel-3.0-0.17.20191104git1c2f876.el8.aarch64.rpm \
          "

SRC_URI[audit-libs.sha256sum] = "11811c556a3bdc9c572c0ab67d3106bd1de3406c9d471de03e028f041b5785c3"
SRC_URI[audit-libs-devel.sha256sum] = "0bdc44f5e68de41c88d4d687af86a92693ce02e62e6335869b610b24cf769fea"
