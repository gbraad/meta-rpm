SUMMARY = "generated recipe based on dbus-c++ srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus dbus-libs expat glib-2.0 libgcc pkgconfig-native"
RPM_SONAME_PROV_dbus-c++ = "libdbus-c++-1.so.0"
RPM_SONAME_REQ_dbus-c++ = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libexpat.so.1 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_dbus-c++ = "dbus-libs expat glibc libgcc libstdc++"
RPM_SONAME_REQ_dbus-c++-devel = "libdbus-c++-1.so.0 libdbus-c++-glib-1.so.0"
RPROVIDES_dbus-c++-devel = "dbus-c++-dev (= 0.9.0)"
RDEPENDS_dbus-c++-devel = "dbus-c++ dbus-c++-glib dbus-devel pkgconf-pkg-config"
RPM_SONAME_PROV_dbus-c++-glib = "libdbus-c++-glib-1.so.0"
RPM_SONAME_REQ_dbus-c++-glib = "libc.so.6 libdbus-1.so.3 libgcc_s.so.1 libglib-2.0.so.0 libm.so.6 libstdc++.so.6"
RDEPENDS_dbus-c++-glib = "dbus-c++ dbus-libs glib2 glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dbus-c++-0.9.0-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dbus-c++-devel-0.9.0-17.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/dbus-c++-glib-0.9.0-17.el8.aarch64.rpm \
          "

SRC_URI[dbus-c++.sha256sum] = "d7729e7c097fa6f6c77181466f482b8c0319d28d80c5b97cf8ce43e3f4c25ac9"
SRC_URI[dbus-c++-devel.sha256sum] = "55101acf67e1135ec5b210ffccd5dba0b9c813fd396f0eb7f16b26bdbef79ef6"
SRC_URI[dbus-c++-glib.sha256sum] = "6081819dfaf066ab3c8f3d39aecd33af9d137786218421c51c8ea9027c450cb8"
