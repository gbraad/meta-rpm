SUMMARY = "generated recipe based on sg3_utils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & BSD"
RPM_LICENSE = "GPLv2+ and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_sg3_utils = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libpthread.so.0 libsgutils2.so.2"
RDEPENDS_sg3_utils = "bash glibc libgcc sg3_utils-libs"
RPM_SONAME_REQ_sg3_utils-devel = "libsgutils2.so.2"
RPROVIDES_sg3_utils-devel = "sg3_utils-dev (= 1.44)"
RDEPENDS_sg3_utils-devel = "sg3_utils-libs"
RPM_SONAME_PROV_sg3_utils-libs = "libsgutils2.so.2"
RPM_SONAME_REQ_sg3_utils-libs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_sg3_utils-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sg3_utils-1.44-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sg3_utils-libs-1.44-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sg3_utils-devel-1.44-5.el8.aarch64.rpm \
          "

SRC_URI[sg3_utils.sha256sum] = "de0cec19f3194af0aff5ee5f133f370dca5cb1d3d6826c24e30c001cec779c36"
SRC_URI[sg3_utils-devel.sha256sum] = "79a66f89d163db00a2af49ee0a75f4bc1ee8e33acec262386a86cd8b9b46ac6a"
SRC_URI[sg3_utils-libs.sha256sum] = "7b958cc0e9eb01f18ba771b1b5cb382dd7be5e066e42c7dcc1073e9534679e9b"
