SUMMARY = "generated recipe based on nss_nis srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libnsl2 libtirpc pkgconfig-native"
RPM_SONAME_PROV_nss_nis = "libnss_nis.so.2"
RPM_SONAME_REQ_nss_nis = "ld-linux-aarch64.so.1 libc.so.6 libnsl.so.2 libtirpc.so.3"
RDEPENDS_nss_nis = "glibc libnsl2 libtirpc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/nss_nis-3.0-8.el8.aarch64.rpm \
          "

SRC_URI[nss_nis.sha256sum] = "86348ab1eb454b7a595dedb33d090ef1b7763d8113563cb52b87af5674fcc2b0"
