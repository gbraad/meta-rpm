SUMMARY = "generated recipe based on qhull srpm"
DESCRIPTION = "Description"
LICENSE = "Qhull"
RPM_LICENSE = "Qhull"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libqhull = "libqhull.so.7"
RPM_SONAME_REQ_libqhull = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_libqhull = "glibc"
RPM_SONAME_PROV_libqhull_p = "libqhull_p.so.7"
RPM_SONAME_REQ_libqhull_p = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_libqhull_p = "glibc"
RPM_SONAME_PROV_libqhull_r = "libqhull_r.so.7"
RPM_SONAME_REQ_libqhull_r = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_libqhull_r = "glibc"
RPM_SONAME_REQ_qhull-devel = "libqhull.so.7 libqhull_p.so.7 libqhull_r.so.7"
RPROVIDES_qhull-devel = "qhull-dev (= 2015.2)"
RDEPENDS_qhull-devel = "libqhull libqhull_p libqhull_r"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libqhull-2015.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libqhull_p-2015.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libqhull_r-2015.2-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/qhull-devel-2015.2-5.el8.aarch64.rpm \
          "

SRC_URI[libqhull.sha256sum] = "e8e2b87cf4af93269781749393d3c4ccc331c9893e89740e35e8c6f8863c1785"
SRC_URI[libqhull_p.sha256sum] = "caf1fe982ff154dce85b0b9ceddfbdfccc9eceb331d418768742742b0d298c56"
SRC_URI[libqhull_r.sha256sum] = "85f5c4395f22488ede27c64239690160ab02d87bf6096b30916ed6f7a8fc8a4f"
SRC_URI[qhull-devel.sha256sum] = "fcbdbc0a41186133f3615f4dea47e1b6fb5eb7e500a99790ebc5bf8096265773"
