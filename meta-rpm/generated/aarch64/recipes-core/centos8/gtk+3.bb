SUMMARY = "generated recipe based on gtk3 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "at-spi2-atk atk cairo colord cups-libs fontconfig freetype gdk-pixbuf glib-2.0 harfbuzz json-glib libepoxy libx11 libxcomposite libxcursor libxdamage libxext libxfixes libxi libxinerama libxkbcommon libxrandr pango pkgconfig-native rest wayland wayland-protocols"
RPM_SONAME_REQ_gtk-update-icon-cache = "ld-linux-aarch64.so.1 libc.so.6 libgdk_pixbuf-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_gtk-update-icon-cache = "gdk-pixbuf2 glib2 glibc"
RPM_SONAME_PROV_gtk3 = "libgailutil-3.so.0 libgdk-3.so.0 libgtk-3.so.0"
RPM_SONAME_REQ_gtk3 = "ld-linux-aarch64.so.1 libX11.so.6 libXcomposite.so.1 libXcursor.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libatk-1.0.so.0 libatk-bridge-2.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libcolord.so.2 libcups.so.2 libepoxy.so.0 libfontconfig.so.1 libfreetype.so.6 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libjson-glib-1.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 librest-0.7.so.0 librt.so.1 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libxkbcommon.so.0"
RDEPENDS_gtk3 = "adwaita-icon-theme at-spi2-atk atk bash cairo cairo-gobject colord-libs cups-libs fontconfig freetype gdk-pixbuf2 gdk-pixbuf2-modules glib2 glibc gtk-update-icon-cache hicolor-icon-theme json-glib libX11 libXcomposite libXcursor libXdamage libXext libXfixes libXi libXinerama libXrandr libepoxy libwayland-client libwayland-cursor libwayland-egl libxkbcommon pango rest"
RPM_SONAME_REQ_gtk3-devel = "ld-linux-aarch64.so.1 libX11.so.6 libXcomposite.so.1 libXcursor.so.1 libXdamage.so.1 libXext.so.6 libXfixes.so.3 libXi.so.6 libXinerama.so.1 libXrandr.so.2 libatk-1.0.so.0 libatk-bridge-2.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libepoxy.so.0 libfontconfig.so.1 libfreetype.so.6 libgailutil-3.so.0 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libharfbuzz.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpthread.so.0 librt.so.1 libwayland-client.so.0 libwayland-cursor.so.0 libwayland-egl.so.1 libxkbcommon.so.0"
RPROVIDES_gtk3-devel = "gtk3-dev (= 3.22.30)"
RDEPENDS_gtk3-devel = "at-spi2-atk at-spi2-atk-devel atk atk-devel cairo cairo-devel cairo-gobject cairo-gobject-devel fontconfig fontconfig-devel freetype gdk-pixbuf2 gdk-pixbuf2-devel glib2 glib2-devel glibc gtk3 harfbuzz libX11 libX11-devel libXcomposite libXcomposite-devel libXcursor libXcursor-devel libXdamage libXdamage-devel libXext libXext-devel libXfixes libXfixes-devel libXi libXi-devel libXinerama libXinerama-devel libXrandr libXrandr-devel libepoxy libepoxy-devel libwayland-client libwayland-cursor libwayland-egl libxkbcommon libxkbcommon-devel pango pango-devel pkgconf-pkg-config wayland-devel wayland-protocols-devel"
RPM_SONAME_REQ_gtk3-immodule-xim = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libgdk-3.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libpango-1.0.so.0 libpthread.so.0"
RDEPENDS_gtk3-immodule-xim = "glib2 glibc gtk3 libX11 pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtk-update-icon-cache-3.22.30-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtk3-3.22.30-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtk3-devel-3.22.30-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtk3-immodule-xim-3.22.30-5.el8.aarch64.rpm \
          "

SRC_URI[gtk-update-icon-cache.sha256sum] = "c8af00c5a9ae31a0a5629616563231aabe4e9100d0693edec5362798f158eda0"
SRC_URI[gtk3.sha256sum] = "89c80185ba67b7146647f65b9ad9fe5f873e95b7592e9c082f992e73111c7ada"
SRC_URI[gtk3-devel.sha256sum] = "8f7518817fd9e7f8fc10aeecdfbffd89a689b85ea361b85695da139f2262c4a0"
SRC_URI[gtk3-immodule-xim.sha256sum] = "bef67e8dbcd067f4b60cb9690b0e479d92928deefdc5f9040c72d6240250b25f"
