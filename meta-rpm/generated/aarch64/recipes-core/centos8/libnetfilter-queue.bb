SUMMARY = "generated recipe based on libnetfilter_queue srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libmnl libnfnetlink pkgconfig-native"
RPM_SONAME_PROV_libnetfilter_queue = "libnetfilter_queue.so.1"
RPM_SONAME_REQ_libnetfilter_queue = "ld-linux-aarch64.so.1 libc.so.6 libmnl.so.0 libnfnetlink.so.0"
RDEPENDS_libnetfilter_queue = "glibc libmnl libnfnetlink"
RPM_SONAME_REQ_libnetfilter_queue-devel = "libnetfilter_queue.so.1"
RPROVIDES_libnetfilter_queue-devel = "libnetfilter_queue-dev (= 1.0.2)"
RDEPENDS_libnetfilter_queue-devel = "kernel-headers libnetfilter_queue libnfnetlink-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libnetfilter_queue-1.0.2-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libnetfilter_queue-devel-1.0.2-11.el8.aarch64.rpm \
          "

SRC_URI[libnetfilter_queue.sha256sum] = "e1c106ce918f17ed74211ce2ca60102d45ad774c84897911377a05d9f5999fdd"
SRC_URI[libnetfilter_queue-devel.sha256sum] = "99582fd122d794df5835c0eaec62b1c79f74460ad4fb79625c75df37a0db6bcf"
