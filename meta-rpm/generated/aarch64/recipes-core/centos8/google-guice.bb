SUMMARY = "generated recipe based on google-guice srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_google-guice = "aopalliance atinject guava20 java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_google-guice-javadoc = "javapackages-filesystem"
RDEPENDS_guice-assistedinject = "google-guice java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_guice-bom = "guice-parent java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_guice-extensions = "google-guice guice-parent java-1.8.0-openjdk-headless javapackages-filesystem maven-plugin-bundle maven-remote-resources-plugin maven-source-plugin"
RDEPENDS_guice-grapher = "google-guice guice-assistedinject guice-multibindings java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_guice-jmx = "google-guice java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_guice-jndi = "google-guice java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_guice-multibindings = "google-guice java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_guice-parent = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_guice-servlet = "google-guice java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_guice-testlib = "google-guice java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_guice-throwingproviders = "google-guice java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/google-guice-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/google-guice-javadoc-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-assistedinject-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-bom-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-extensions-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-grapher-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-jmx-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-jndi-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-multibindings-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-parent-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-servlet-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-testlib-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/guice-throwingproviders-4.1-11.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[google-guice.sha256sum] = "e1e7f5ceba4f6f07b8d28252a9c5975c9bcb17b719d211ab932bf368f94b1a23"
SRC_URI[google-guice-javadoc.sha256sum] = "9e5507878ce5d674ef0e7998869e35d0cb5f9ad44147ae018470e3a40c4a6dfb"
SRC_URI[guice-assistedinject.sha256sum] = "e9598d0d9f8eaebef055fb9d0d339ef2ca6f3f9676c9e9b7d5e78ee13081c39c"
SRC_URI[guice-bom.sha256sum] = "967ac47966d979b563e48d09eeb2f16ab803d3a4d4b701b1bd0170a9de190ad9"
SRC_URI[guice-extensions.sha256sum] = "c8e1d4ff239a40d398ff5e5c9d5be3cfcbade1d8ff40240fca534be79284cd7c"
SRC_URI[guice-grapher.sha256sum] = "f949a92c9253164bc0a0aa690da5fe587583e291c884a545b391a5b4b9ddb7a1"
SRC_URI[guice-jmx.sha256sum] = "df22f6a69e215c357075ffd1f30b0fc2f6a84ca930f5700bf2153f69ce72b4fb"
SRC_URI[guice-jndi.sha256sum] = "839ba10b2f29297c28d95502e4ff9ea80fb44873cdf33c33726edc240b98bc3d"
SRC_URI[guice-multibindings.sha256sum] = "c6b52cae4d69ceb9275c6b5a903eb82d062bea5ab0b83d9ac8a2f5d1e69f1a47"
SRC_URI[guice-parent.sha256sum] = "50f2d5b6cc14aa300c6666647a830b64770fe0b398d5e0c9a915727984fcc702"
SRC_URI[guice-servlet.sha256sum] = "b66eafebf15dd679b4b25db860bb00e2e257844c171a3c07595acb5f5bd38230"
SRC_URI[guice-testlib.sha256sum] = "4ed9476b5dace80853808e3b1a3a067ee3d9aa1d1082aef0922d49138d70e5a5"
SRC_URI[guice-throwingproviders.sha256sum] = "a3e12f17598d36526eb530de6afbd9172049b969bc3e67e4b5dd18d1ef173cbf"
