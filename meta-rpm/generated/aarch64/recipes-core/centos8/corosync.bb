SUMMARY = "generated recipe based on corosync srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libqb pkgconfig-native readline"
RPM_SONAME_REQ_corosync-vqsim = "ld-linux-aarch64.so.1 libc.so.6 libcorosync_common.so.4 libpthread.so.0 libqb.so.0 libreadline.so.7"
RDEPENDS_corosync-vqsim = "corosynclib glibc libqb pkgconf-pkg-config readline"
RPM_SONAME_PROV_corosynclib = "libcfg.so.7 libcmap.so.4 libcorosync_common.so.4 libcpg.so.4 libquorum.so.5 libsam.so.4 libvotequorum.so.8"
RPM_SONAME_REQ_corosynclib = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libqb.so.0 librt.so.1"
RDEPENDS_corosynclib = "glibc libqb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/corosynclib-3.0.3-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/corosync-vqsim-3.0.3-2.el8.aarch64.rpm \
          "

SRC_URI[corosync-vqsim.sha256sum] = "7a3d3b5be87e1303618d85c57c27c1936d491cdf6cbb42c242c8db7289e1bb79"
SRC_URI[corosynclib.sha256sum] = "2949ed037e3d85d23bba9113d4080afd82824ed3fbe44783526acfb093da4c23"
