SUMMARY = "generated recipe based on sanlock srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2 and GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libaio libblkid libuuid pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-sanlock = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 libsanlock.so.1"
RDEPENDS_python3-sanlock = "glibc platform-python python3-libs sanlock-lib"
RPM_SONAME_REQ_sanlk-reset = "ld-linux-aarch64.so.1 libc.so.6 libsanlock.so.1 libwdmd.so.1"
RDEPENDS_sanlk-reset = "glibc sanlock sanlock-lib"
RPM_SONAME_REQ_sanlock = "ld-linux-aarch64.so.1 libaio.so.1 libblkid.so.1 libc.so.6 libpthread.so.0 librt.so.1 libsanlock.so.1 libuuid.so.1 libwdmd.so.1"
RDEPENDS_sanlock = "bash glibc libaio libblkid libuuid sanlock-lib shadow-utils systemd"
RPM_SONAME_REQ_sanlock-devel = "libsanlock.so.1 libsanlock_client.so.1 libwdmd.so.1"
RPROVIDES_sanlock-devel = "sanlock-dev (= 3.8.0)"
RDEPENDS_sanlock-devel = "pkgconf-pkg-config sanlock-lib"
RPM_SONAME_PROV_sanlock-lib = "libsanlock.so.1 libsanlock_client.so.1 libwdmd.so.1"
RPM_SONAME_REQ_sanlock-lib = "ld-linux-aarch64.so.1 libaio.so.1 libblkid.so.1 libc.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_sanlock-lib = "glibc libaio libblkid"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-sanlock-3.8.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sanlk-reset-3.8.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sanlock-3.8.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/sanlock-lib-3.8.0-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/sanlock-devel-3.8.0-2.el8.aarch64.rpm \
          "

SRC_URI[python3-sanlock.sha256sum] = "011bbd2bbdc65eb8b46d984a71c380e0a67251ffa2da3adcf7a0cfba29a92c99"
SRC_URI[sanlk-reset.sha256sum] = "e90a913bd2a6608dbb30b3d5ab911605144e771004d8b7cb27b3bb41ff8db254"
SRC_URI[sanlock.sha256sum] = "6e96145ec3376417e09536eb21d24d66f59e54f36a4884feb6dd22aea09c0854"
SRC_URI[sanlock-devel.sha256sum] = "42efe296ac88e1e8e6d04079606f5df70c85ff84e27863b1cc60fcf621fedaf1"
SRC_URI[sanlock-lib.sha256sum] = "3c86e9aec7420d5cc8ab162253b7d7d58f61ed926d3f8f886e69b141ab6e0ea9"
