SUMMARY = "generated recipe based on maven-shared-io srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-shared-io = "java-1.8.0-openjdk-headless javapackages-filesystem maven-lib maven-shared-utils maven-wagon-provider-api plexus-utils"
RDEPENDS_maven-shared-io-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-shared-io-3.0.0-5.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-shared-io-javadoc-3.0.0-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-shared-io.sha256sum] = "7f1cd3a10345f1c6fea2b2488b9f5c578312937e94c8fa79fcbf6176c996f0dc"
SRC_URI[maven-shared-io-javadoc.sha256sum] = "ebc6cec06dd0f36395e084f3a32aaf454db33d4022e42e2f0208bd5f776f2fe6"
