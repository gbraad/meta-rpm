SUMMARY = "generated recipe based on volume_key srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & (MPL-1.1 | GPL-2.0 | LGPL-2.0)"
RPM_LICENSE = "GPLv2 and (MPLv1.1 or GPLv2 or LGPLv2)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cryptsetup-libs glib-2.0 gpgme libblkid nspr nss pkgconfig-native"
RPM_SONAME_REQ_volume_key = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libcryptsetup.so.12 libdl.so.2 libglib-2.0.so.0 libgpgme.so.11 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so libvolume_key.so.1"
RDEPENDS_volume_key = "cryptsetup-libs glib2 glibc gpgme libblkid nspr nss nss-util volume_key-libs"
RPM_SONAME_REQ_volume_key-devel = "libvolume_key.so.1"
RPROVIDES_volume_key-devel = "volume_key-dev (= 0.3.11)"
RDEPENDS_volume_key-devel = "volume_key-libs"
RPM_SONAME_PROV_volume_key-libs = "libvolume_key.so.1"
RPM_SONAME_REQ_volume_key-libs = "ld-linux-aarch64.so.1 libblkid.so.1 libc.so.6 libcryptsetup.so.12 libdl.so.2 libglib-2.0.so.0 libgpgme.so.11 libnspr4.so libnss3.so libnssutil3.so libplc4.so libplds4.so libpthread.so.0 libsmime3.so libssl3.so"
RDEPENDS_volume_key-libs = "cryptsetup-libs glib2 glibc gnupg2 gpgme libblkid nspr nss nss-util"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/volume_key-0.3.11-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/volume_key-devel-0.3.11-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/volume_key-libs-0.3.11-5.el8.aarch64.rpm \
          "

SRC_URI[volume_key.sha256sum] = "a559f9543daf4126368a664d9e48102a84764eb96f2a5e04d8dd02fd1a4779e4"
SRC_URI[volume_key-devel.sha256sum] = "d215581cace82cbbfece14f83fe2b4ba056290753924021160feda5a7a48795b"
SRC_URI[volume_key-libs.sha256sum] = "3ec4f0a88c90f2021507016d369c7a77aac7c3d120ab35a30532235b4eb524f5"
