SUMMARY = "generated recipe based on maven-plugin-build-helper srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-plugin-build-helper = "bsh java-1.8.0-openjdk-headless javapackages-filesystem maven-artifact maven-lib maven-model maven-project plexus-utils"
RDEPENDS_maven-plugin-build-helper-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-build-helper-1.9.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/maven-plugin-build-helper-javadoc-1.9.1-8.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-plugin-build-helper.sha256sum] = "d8d344dd303e84e3290665f8a847b9fbfd68723545f6eb2838fb6cb0fc67d455"
SRC_URI[maven-plugin-build-helper-javadoc.sha256sum] = "955dbc7c72aed9f9968241623e5a1d2bd841cfaa5c66a18481e3cb4f1243ba53"
