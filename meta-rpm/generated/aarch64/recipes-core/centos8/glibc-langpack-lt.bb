SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-lt = "locale-base-lt-lt (= 2.28) locale-base-lt-lt.utf8 (= 2.28) virtual-locale-lt (= 2.28) virtual-locale-lt (= 2.28) virtual-locale-lt-lt (= 2.28) virtual-locale-lt-lt.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-lt = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-lt-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-lt.sha256sum] = "51f81cb6616990f22da7acf0a6fa4485d4e581fccd9123302eb81be7c661c563"
