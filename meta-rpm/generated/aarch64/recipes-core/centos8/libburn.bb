SUMMARY = "generated recipe based on libburn srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libburn = "libburn.so.4"
RPM_SONAME_REQ_libburn = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_libburn = "glibc"
RPM_SONAME_REQ_libburn-devel = "libburn.so.4"
RPROVIDES_libburn-devel = "libburn-dev (= 1.4.8)"
RDEPENDS_libburn-devel = "libburn pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libburn-1.4.8-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libburn-devel-1.4.8-3.el8.aarch64.rpm \
          "

SRC_URI[libburn.sha256sum] = "5ae88291a28b2a86efb6cdc8ff67baaf73dad1428c858c8b0fa9e8df0f0f041c"
SRC_URI[libburn-devel.sha256sum] = "f0f10bffb2e3231d02f7e713d7c6c27b05e14bc513566a63cbcddd83f8f3fd36"
