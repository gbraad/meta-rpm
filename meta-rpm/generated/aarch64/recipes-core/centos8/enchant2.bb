SUMMARY = "generated recipe based on enchant2 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "aspell glib-2.0 hunspell libgcc pkgconfig-native"
RPM_SONAME_PROV_enchant2 = "libenchant-2.so.2"
RPM_SONAME_REQ_enchant2 = "ld-linux-aarch64.so.1 libaspell.so.15 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgmodule-2.0.so.0 libhunspell-1.6.so.0 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_enchant2 = "aspell glib2 glibc hunspell libgcc libstdc++"
RPM_SONAME_REQ_enchant2-devel = "libenchant-2.so.2"
RPROVIDES_enchant2-devel = "enchant2-dev (= 2.2.3)"
RDEPENDS_enchant2-devel = "enchant2 glib2-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/enchant2-2.2.3-2.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/enchant2-devel-2.2.3-2.el8.aarch64.rpm \
          "

SRC_URI[enchant2.sha256sum] = "3cd64c4a430ce2c006d91e21d90abeee0f40caacfc137bb5dfbebeb2772ece25"
SRC_URI[enchant2-devel.sha256sum] = "337fcfd074ca3bd2cb19b936ed340c617913b2fbf2b55a29b572c297b4f4951b"
