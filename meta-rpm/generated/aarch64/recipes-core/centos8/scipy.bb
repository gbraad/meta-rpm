SUMMARY = "generated recipe based on scipy srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & LGPL-2.0"
RPM_LICENSE = "BSD and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gcc libgcc openblas pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-scipy = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgfortran.so.5 libm.so.6 libopenblas.so.0 libopenblasp.so.0 libpthread.so.0 libpython3.6m.so.1.0 libstdc++.so.6"
RDEPENDS_python3-scipy = "glibc libgcc libgfortran libstdc++ openblas openblas-threads platform-python python3-libs python3-numpy python3-numpy-f2py python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-scipy-1.0.0-20.module_el8.1.0+245+c39af44f.aarch64.rpm \
          "

SRC_URI[python3-scipy.sha256sum] = "48214931578071a1dd184230c1ea419006c692defaee73a6c3d9f9254207cd1e"
