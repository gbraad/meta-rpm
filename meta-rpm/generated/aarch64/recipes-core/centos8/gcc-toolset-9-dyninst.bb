SUMMARY = "generated recipe based on gcc-toolset-9-dyninst srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "boost dyninst elfutils libgcc pkgconfig-native tbb"
RPM_SONAME_PROV_gcc-toolset-9-dyninst = "libcommon.so.10.1 libdynC_API.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libdyninstAPI_RT.so.10.1 libinstructionAPI.so.10.1 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libstackwalk.so.10.1 libsymLite.so.10.1 libsymtabAPI.so.10.1"
RPM_SONAME_REQ_gcc-toolset-9-dyninst = "ld-linux-aarch64.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libboost_timer.so.1.66.0 libc.so.6 libdl.so.2 libdw.so.1 libelf.so.1 libgcc_s.so.1 libgomp.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RDEPENDS_gcc-toolset-9-dyninst = "boost-atomic boost-chrono boost-date-time boost-filesystem boost-system boost-thread boost-timer elfutils-libelf elfutils-libs gcc-toolset-9-runtime glibc libgcc libgomp libstdc++ tbb"
RPM_SONAME_PROV_gcc-toolset-9-dyninst-devel = "libInst.so"
RPM_SONAME_REQ_gcc-toolset-9-dyninst-devel = "ld-linux-aarch64.so.1 libc.so.6 libcommon.so.10.1 libdynC_API.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libdyninstAPI_RT.so.10.1 libgcc_s.so.1 libinstructionAPI.so.10.1 libm.so.6 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libstackwalk.so.10.1 libstdc++.so.6 libsymLite.so.10.1 libsymtabAPI.so.10.1"
RPROVIDES_gcc-toolset-9-dyninst-devel = "gcc-toolset-9-dyninst-dev (= 10.1.0)"
RDEPENDS_gcc-toolset-9-dyninst-devel = "boost-devel dyninst gcc-toolset-9-dyninst glibc libgcc libstdc++ tbb-devel"
RDEPENDS_gcc-toolset-9-dyninst-static = "gcc-toolset-9-dyninst-devel"
RPM_SONAME_REQ_gcc-toolset-9-dyninst-testsuite = "ld-linux-aarch64.so.1 libboost_atomic.so.1.66.0 libboost_chrono.so.1.66.0 libboost_date_time.so.1.66.0 libboost_filesystem.so.1.66.0 libboost_system.so.1.66.0 libboost_thread.so.1.66.0 libboost_timer.so.1.66.0 libc.so.6 libcommon.so.10.1 libdynDwarf.so.10.1 libdynElf.so.10.1 libdyninstAPI.so.10.1 libgcc_s.so.1 libgomp.so.1 libinstructionAPI.so.10.1 libm.so.6 libparseAPI.so.10.1 libpatchAPI.so.10.1 libpcontrol.so.10.1 libpthread.so.0 libstackwalk.so.10.1 libstdc++.so.6 libsymtabAPI.so.10.1 libtbb.so.2 libtbbmalloc.so.2 libtbbmalloc_proxy.so.2"
RDEPENDS_gcc-toolset-9-dyninst-testsuite = "boost-atomic boost-chrono boost-date-time boost-filesystem boost-system boost-thread boost-timer dyninst gcc-toolset-9-dyninst gcc-toolset-9-dyninst-devel gcc-toolset-9-dyninst-static glibc glibc-static libgcc libgomp libstdc++ tbb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-dyninst-10.1.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gcc-toolset-9-dyninst-devel-10.1.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gcc-toolset-9-dyninst-doc-10.1.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gcc-toolset-9-dyninst-static-10.1.0-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gcc-toolset-9-dyninst-testsuite-10.1.0-1.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-dyninst.sha256sum] = "d9582af0aabf9b7cf2999b5471c86859fc728df831d4adae67d477dec117db99"
SRC_URI[gcc-toolset-9-dyninst-devel.sha256sum] = "0e25bb0d5f917f4edc8f2aad9783c39f5b20478bfa57101f997915fbdf8f3bf6"
SRC_URI[gcc-toolset-9-dyninst-doc.sha256sum] = "2ea5266d38d7847b633b7e4091aace34a33ed9a8713e2f8cd8ebb9abea276fac"
SRC_URI[gcc-toolset-9-dyninst-static.sha256sum] = "0efa50da69837c3f770e973a7cd8be3a7686a03b812de1ea34abd551f192d0ba"
SRC_URI[gcc-toolset-9-dyninst-testsuite.sha256sum] = "c28a0fce17d2dcb0b29cda8ca6364371dd7c90c1333ab159aaacfb2a146ec709"
