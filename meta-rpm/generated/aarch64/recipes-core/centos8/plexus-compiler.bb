SUMMARY = "generated recipe based on plexus-compiler srpm"
DESCRIPTION = "Description"
LICENSE = "MIT & CLOSED"
RPM_LICENSE = "MIT and ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-compiler = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-utils"
RDEPENDS_plexus-compiler-extras = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-compiler plexus-utils"
RDEPENDS_plexus-compiler-javadoc = "javapackages-filesystem"
RDEPENDS_plexus-compiler-pom = "java-1.8.0-openjdk-headless javapackages-filesystem plexus-compiler plexus-components-pom plexus-containers-component-metadata"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-compiler-2.8.2-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-compiler-extras-2.8.2-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-compiler-javadoc-2.8.2-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-compiler-pom-2.8.2-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-compiler.sha256sum] = "8c6dcd0d338ff884a44f35f6d1ee6aef805bf4b39efd68a49e9da15b7ce81fd0"
SRC_URI[plexus-compiler-extras.sha256sum] = "f4f06cfce3b383b60a56490e4a4c748f7adc9c9a0b6f11be1d19f10b39d187f8"
SRC_URI[plexus-compiler-javadoc.sha256sum] = "21b8d620d641406c6f76cf640db19f61c15240dfa989fdfb953c8f46af866c2f"
SRC_URI[plexus-compiler-pom.sha256sum] = "e22b99859840cb987d101c9a64b7339fad2dd7d0fd32ee210977a21bae7049ad"
