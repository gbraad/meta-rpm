SUMMARY = "generated recipe based on gnome-font-viewer srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo fontconfig freetype gdk-pixbuf glib-2.0 gnome-desktop3 gtk+3 harfbuzz pango pkgconfig-native"
RPM_SONAME_REQ_gnome-font-viewer = "ld-linux-aarch64.so.1 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libfontconfig.so.1 libfreetype.so.6 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgtk-3.so.0 libharfbuzz.so.0 libpango-1.0.so.0 libpangocairo-1.0.so.0"
RDEPENDS_gnome-font-viewer = "bash cairo cairo-gobject fontconfig freetype gdk-pixbuf2 glib2 glibc gnome-desktop3 gtk3 harfbuzz pango"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-font-viewer-3.28.0-1.el8.aarch64.rpm \
          "

SRC_URI[gnome-font-viewer.sha256sum] = "fa72c75c2e42cf61d1c60df788bda292f18216ca362638c03d7aac96594eb7ca"
