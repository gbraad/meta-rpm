SUMMARY = "generated recipe based on libmtp srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcrypt libusb1 pkgconfig-native"
RPM_SONAME_PROV_libmtp = "libmtp.so.9"
RPM_SONAME_REQ_libmtp = "ld-linux-aarch64.so.1 libc.so.6 libgcrypt.so.20 libusb-1.0.so.0"
RDEPENDS_libmtp = "glibc libgcrypt libusbx systemd-udev"
RPM_SONAME_REQ_libmtp-devel = "libmtp.so.9"
RPROVIDES_libmtp-devel = "libmtp-dev (= 1.1.14)"
RDEPENDS_libmtp-devel = "libgcrypt-devel libmtp libusbx-devel pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libmtp-1.1.14-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libmtp-devel-1.1.14-3.el8.aarch64.rpm \
          "

SRC_URI[libmtp.sha256sum] = "3951fc1bcdd2c798d0427597c44ca88041044489f969cb92369fe7d8e3a6fdb7"
SRC_URI[libmtp-devel.sha256sum] = "2b464100329bd6a58b99ba6f224ec2a898e8ad5fdd3b97c24b387d735d43acc7"
