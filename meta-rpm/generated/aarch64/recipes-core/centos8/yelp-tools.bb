SUMMARY = "generated recipe based on yelp-tools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_yelp-tools = "bash itstool libxml2 mallard-rng yelp-xsl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/yelp-tools-3.28.0-3.el8.noarch.rpm \
          "

SRC_URI[yelp-tools.sha256sum] = "f9f8b8b133359ec59708e445d45da28192318529df380a55ed7ae49284bc312a"
