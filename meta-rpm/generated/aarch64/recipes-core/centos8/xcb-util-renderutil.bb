SUMMARY = "generated recipe based on xcb-util-renderutil srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxcb pkgconfig-native"
RPM_SONAME_PROV_xcb-util-renderutil = "libxcb-render-util.so.0"
RPM_SONAME_REQ_xcb-util-renderutil = "ld-linux-aarch64.so.1 libc.so.6 libxcb-render.so.0 libxcb.so.1"
RDEPENDS_xcb-util-renderutil = "glibc libxcb"
RPM_SONAME_REQ_xcb-util-renderutil-devel = "libxcb-render-util.so.0"
RPROVIDES_xcb-util-renderutil-devel = "xcb-util-renderutil-dev (= 0.3.9)"
RDEPENDS_xcb-util-renderutil-devel = "libxcb-devel pkgconf-pkg-config xcb-util-renderutil"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xcb-util-renderutil-0.3.9-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xcb-util-renderutil-devel-0.3.9-10.el8.aarch64.rpm \
          "

SRC_URI[xcb-util-renderutil.sha256sum] = "ee28c5066cbf17e7eafda04a1443bed5064afcbd35416411f75a389018d41e13"
SRC_URI[xcb-util-renderutil-devel.sha256sum] = "23af3aa583f21f0b8460d24b2353436a085d225e98c968b5d9225d2b9e16141f"
