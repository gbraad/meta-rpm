SUMMARY = "generated recipe based on libsamplerate srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib libsndfile2 pkgconfig-native"
RPM_SONAME_PROV_libsamplerate = "libsamplerate.so.0"
RPM_SONAME_REQ_libsamplerate = "ld-linux-aarch64.so.1 libasound.so.2 libc.so.6 libm.so.6 libsndfile.so.1"
RDEPENDS_libsamplerate = "alsa-lib glibc libsndfile"
RPM_SONAME_REQ_libsamplerate-devel = "libsamplerate.so.0"
RPROVIDES_libsamplerate-devel = "libsamplerate-dev (= 0.1.9)"
RDEPENDS_libsamplerate-devel = "libsamplerate pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libsamplerate-0.1.9-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsamplerate-devel-0.1.9-1.el8.aarch64.rpm \
          "

SRC_URI[libsamplerate.sha256sum] = "48268625d6a9463fc77e7e51cde8a31960c4ea03c948760575ada14ce794731a"
SRC_URI[libsamplerate-devel.sha256sum] = "080260e4188c361c4e12f223b07214fbb76e489a3f4865edf02d54e78394323c"
