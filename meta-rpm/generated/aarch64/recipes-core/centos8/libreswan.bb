SUMMARY = "generated recipe based on libreswan srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs curl fipscheck ldns libcap-ng libevent libseccomp libselinux libxcrypt nspr nss openldap pam pkgconfig-native systemd-libs unbound"
RPM_SONAME_REQ_libreswan = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libcap-ng.so.0 libcrypt.so.1 libcurl.so.4 libevent-2.1.so.6 libevent_pthreads-2.1.so.6 libfipscheck.so.1 liblber-2.4.so.2 libldap-2.4.so.2 libldns.so.2 libnspr4.so libnss3.so libnssutil3.so libpam.so.0 libpthread.so.0 librt.so.1 libseccomp.so.2 libselinux.so.1 libsmime3.so libsystemd.so.0 libunbound.so.2"
RDEPENDS_libreswan = "audit-libs bash coreutils fipscheck fipscheck-lib glibc iproute ldns libcap-ng libcurl libevent libseccomp libselinux libxcrypt nspr nss nss-softokn nss-tools nss-util openldap pam platform-python systemd systemd-libs unbound-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libreswan-3.29-7.el8_2.aarch64.rpm \
          "

SRC_URI[libreswan.sha256sum] = "c8302139ab17f21aa13f27669b03e5c419427ca1d61d5a23af382b2f3fc820b7"
