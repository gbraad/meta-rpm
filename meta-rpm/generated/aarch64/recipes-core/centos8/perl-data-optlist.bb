SUMMARY = "generated recipe based on perl-Data-OptList srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Data-OptList = "perl-Params-Util perl-Scalar-List-Utils perl-Sub-Install perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Data-OptList-0.110-6.el8.noarch.rpm \
          "

SRC_URI[perl-Data-OptList.sha256sum] = "59a8dc1f9c4e129fac3b62c9fe0ed95bdb1d98480444015c9bc5da5ee1cc9733"
