SUMMARY = "generated recipe based on hunspell-si srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-si = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-si-0.2.1-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-si.sha256sum] = "9e35d040f67d8ace7dc973eb765673502919783fed21c7add92cd84c8fbded55"
