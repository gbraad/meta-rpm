SUMMARY = "generated recipe based on perl-Date-Manip srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Date-Manip = "perl-Carp perl-Data-Dumper perl-Encode perl-Exporter perl-IO perl-PathTools perl-Storable perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-Date-Manip-6.60-2.el8.noarch.rpm \
          "

SRC_URI[perl-Date-Manip.sha256sum] = "756d8c63b30999c8f661e3b845b3b1e4ebd145371af861d59cea02a85fc51e19"
