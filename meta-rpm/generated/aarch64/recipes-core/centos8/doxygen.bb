SUMMARY = "generated recipe based on doxygen srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native qt5-qtbase"
RPM_SONAME_REQ_doxygen = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_doxygen = "glibc libgcc libstdc++ perl-interpreter platform-python"
RPM_SONAME_REQ_doxygen-doxywizard = "ld-linux-aarch64.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_doxygen-doxywizard = "doxygen glibc libgcc libstdc++ qt5-qtbase qt5-qtbase-gui"
RDEPENDS_doxygen-latex = "doxygen texlive-appendix texlive-collection-latexrecommended texlive-epstopdf texlive-import texlive-multirow texlive-sectsty texlive-tabu texlive-tocloft texlive-xtab"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/doxygen-1.8.14-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/doxygen-doxywizard-1.8.14-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/doxygen-latex-1.8.14-12.el8.aarch64.rpm \
          "

SRC_URI[doxygen.sha256sum] = "96b9865ee58f0235d2469fa003f8bf8fca5dd049bc221cd4508fff30138ad4c8"
SRC_URI[doxygen-doxywizard.sha256sum] = "580610b4c61893719d213cf01c5178f3df83915476faa961eb13c3c299dc2e26"
SRC_URI[doxygen-latex.sha256sum] = "f6769f54cd929cbfab34d75749e9bab204922e610ce99dffe1910e4bc82cc891"
