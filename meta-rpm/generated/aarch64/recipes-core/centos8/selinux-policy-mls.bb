SUMMARY = "generated recipe based on selinux-policy srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_selinux-policy-mls = "bash coreutils mcstrans policycoreutils policycoreutils-newrole selinux-policy"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/selinux-policy-mls-3.14.3-41.el8_2.8.noarch.rpm \
          "

SRC_URI[selinux-policy-mls.sha256sum] = "1ab26980eae3621622d6d500120f04b548d9f451c7b5cc1eadf0e6a56b4be867"
