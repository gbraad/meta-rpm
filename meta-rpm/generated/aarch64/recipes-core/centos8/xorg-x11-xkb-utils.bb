SUMMARY = "generated recipe based on xorg-x11-xkb-utils srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 libxkbfile pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_REQ_xorg-x11-xkb-utils = "ld-linux-aarch64.so.1 libX11.so.6 libc.so.6 libxkbfile.so.1"
RDEPENDS_xorg-x11-xkb-utils = "glibc libX11 libxkbfile"
RPROVIDES_xorg-x11-xkb-utils-devel = "xorg-x11-xkb-utils-dev (= 7.7)"
RDEPENDS_xorg-x11-xkb-utils-devel = "libX11-devel libxkbfile-devel pkgconf-pkg-config xorg-x11-proto-devel xorg-x11-xkb-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-xkb-utils-7.7-27.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xorg-x11-xkb-utils-devel-7.7-27.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-xkb-utils.sha256sum] = "00f676a55d922b0ed80698d79d25e28f1cd5cf740c3c3e02db045c19e4bf5c1a"
SRC_URI[xorg-x11-xkb-utils-devel.sha256sum] = "7366250d258c60ebefdf3cda5fc906dcad70d531095d940bcc6db754e02f28a5"
