SUMMARY = "generated recipe based on spice-client-win srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-client-win-x64-8.0-1.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/spice-client-win-x86-8.0-1.el8.noarch.rpm \
          "

SRC_URI[spice-client-win-x64.sha256sum] = "966644557fdb0bc3937f912bcfee2ee59645a11412f9d0a8191e947a19f7f1a6"
SRC_URI[spice-client-win-x86.sha256sum] = "89c64628a05acfabe4d7c73bd60a02fd9f20bca2814d41b7ecab2d1b89252d59"
