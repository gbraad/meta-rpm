SUMMARY = "generated recipe based on NetworkManager srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2+ and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs bluez curl glib-2.0 gnutls-libs jansson libgcc libndp libnewt libselinux libteam libuuid modemmanager pkgconfig-native polkit readline systemd-libs"
RPM_SONAME_PROV_NetworkManager = "libnm-settings-plugin-ifcfg-rh.so"
RPM_SONAME_REQ_NetworkManager = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libndp.so.0 libnm.so.0 libpolkit-agent-1.so.0 libpolkit-gobject-1.so.0 libpthread.so.0 libreadline.so.7 libselinux.so.1 libsystemd.so.0 libudev.so.1 libuuid.so.1"
RDEPENDS_NetworkManager = "NetworkManager-libnm audit-libs bash chkconfig dbus glib2 glibc gnutls libcurl libgcc libndp libselinux libuuid polkit-libs readline systemd systemd-libs"
RPM_SONAME_PROV_NetworkManager-adsl = "libnm-device-plugin-adsl.so"
RPM_SONAME_REQ_NetworkManager-adsl = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libudev.so.1"
RDEPENDS_NetworkManager-adsl = "NetworkManager glibc libgcc systemd-libs"
RPM_SONAME_PROV_NetworkManager-bluetooth = "libnm-device-plugin-bluetooth.so"
RPM_SONAME_REQ_NetworkManager-bluetooth = "ld-linux-aarch64.so.1 libbluetooth.so.3 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libmm-glib.so.0 libnm-wwan.so libpthread.so.0 libsystemd.so.0"
RDEPENDS_NetworkManager-bluetooth = "ModemManager-glib NetworkManager NetworkManager-wwan bluez bluez-libs glib2 glibc libgcc systemd-libs"
RPM_SONAME_REQ_NetworkManager-cloud-setup = "ld-linux-aarch64.so.1 libc.so.6 libcurl.so.4 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libnm.so.0 libpthread.so.0 libudev.so.1 libuuid.so.1"
RDEPENDS_NetworkManager-cloud-setup = "NetworkManager NetworkManager-libnm bash glib2 glibc gnutls libcurl libgcc libuuid systemd-libs"
RDEPENDS_NetworkManager-dispatcher-routing-rules = "bash"
RPM_SONAME_PROV_NetworkManager-libnm = "libnm.so.0"
RPM_SONAME_REQ_NetworkManager-libnm = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libpthread.so.0 libudev.so.1 libuuid.so.1"
RDEPENDS_NetworkManager-libnm = "glib2 glibc gnutls libgcc libuuid systemd-libs"
RPM_SONAME_REQ_NetworkManager-libnm-devel = "libnm.so.0"
RPROVIDES_NetworkManager-libnm-devel = "NetworkManager-libnm-dev (= 1.22.8)"
RDEPENDS_NetworkManager-libnm-devel = "NetworkManager-libnm glib2-devel pkgconf-pkg-config"
RPM_SONAME_PROV_NetworkManager-ovs = "libnm-device-plugin-ovs.so"
RPM_SONAME_REQ_NetworkManager-ovs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libjansson.so.4 libpthread.so.0"
RDEPENDS_NetworkManager-ovs = "NetworkManager glib2 glibc jansson libgcc"
RPM_SONAME_PROV_NetworkManager-ppp = "libnm-ppp-plugin.so"
RPM_SONAME_REQ_NetworkManager-ppp = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_NetworkManager-ppp = "NetworkManager glib2 glibc libgcc ppp"
RPM_SONAME_PROV_NetworkManager-team = "libnm-device-plugin-team.so"
RPM_SONAME_REQ_NetworkManager-team = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libjansson.so.4 libpthread.so.0 libteamdctl.so.0"
RDEPENDS_NetworkManager-team = "NetworkManager glib2 glibc jansson libgcc teamd"
RPM_SONAME_REQ_NetworkManager-tui = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnutls.so.30 libgobject-2.0.so.0 libnewt.so.0.52 libnm.so.0 libpthread.so.0 libudev.so.1 libuuid.so.1"
RDEPENDS_NetworkManager-tui = "NetworkManager NetworkManager-libnm glib2 glibc gnutls libgcc libuuid newt systemd-libs"
RPM_SONAME_PROV_NetworkManager-wifi = "libnm-device-plugin-wifi.so"
RPM_SONAME_REQ_NetworkManager-wifi = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libpthread.so.0"
RDEPENDS_NetworkManager-wifi = "NetworkManager glib2 glibc libgcc wpa_supplicant"
RPM_SONAME_PROV_NetworkManager-wwan = "libnm-device-plugin-wwan.so libnm-wwan.so"
RPM_SONAME_REQ_NetworkManager-wwan = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libmm-glib.so.0 libpthread.so.0 libsystemd.so.0"
RDEPENDS_NetworkManager-wwan = "ModemManager ModemManager-glib NetworkManager glib2 glibc libgcc systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/NetworkManager-cloud-setup-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-adsl-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-bluetooth-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-config-connectivity-redhat-1.22.8-5.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-config-server-1.22.8-5.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-dispatcher-routing-rules-1.22.8-5.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-libnm-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-ovs-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-ppp-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-team-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-tui-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-wifi-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/NetworkManager-wwan-1.22.8-5.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/NetworkManager-libnm-devel-1.22.8-5.el8_2.aarch64.rpm \
          "

SRC_URI[NetworkManager.sha256sum] = "83d1165ed0f1de35413287a3f59452103cb5725a3460e5f3cd05b3cdfa4f6f22"
SRC_URI[NetworkManager-adsl.sha256sum] = "b47d657dca7d2a3f8a3597d7e2367de1ec2936fb40e934d6b6608ceb56a363bc"
SRC_URI[NetworkManager-bluetooth.sha256sum] = "dc76dd115484826221f4f9d506cfb10bdc3c90cdf42c3bdab9786e6c75756ed1"
SRC_URI[NetworkManager-cloud-setup.sha256sum] = "f93a9d78a5b7286fd29003b74f683519c6f683c334d6d83baf6714233b01caa7"
SRC_URI[NetworkManager-config-connectivity-redhat.sha256sum] = "631c75aac48e9932bb715a2577ab2b8cab6a86d8f795eabd31cf706a4a17de19"
SRC_URI[NetworkManager-config-server.sha256sum] = "0c0e3a1251185fc0418360105454f437617e15bd5c1b37f0b43a8861402bfb4c"
SRC_URI[NetworkManager-dispatcher-routing-rules.sha256sum] = "3f24eb848d077955dbad5570da37132b27e7b916241783f195afedf52e190001"
SRC_URI[NetworkManager-libnm.sha256sum] = "7ea985facfba47d08dd521f43b045c7be5d3889baaa637f850d053fb88bdae76"
SRC_URI[NetworkManager-libnm-devel.sha256sum] = "7109f319c849214d3bed0eff68c55cef96e602125d6bc9c64c562ce587c2a8a5"
SRC_URI[NetworkManager-ovs.sha256sum] = "67d0bfe51e8af6ada5a0eb0a85cd98c1bdea581a58dbd4d241fa46b56569949b"
SRC_URI[NetworkManager-ppp.sha256sum] = "8a2cb7edd965876ec88f608131ab91a1300e915b6f3b4c1f57df1016cc311487"
SRC_URI[NetworkManager-team.sha256sum] = "f90763efbd16b1bc8f485341ddbe8d36c8c3497a5dada91fcc163490b6c08e9a"
SRC_URI[NetworkManager-tui.sha256sum] = "4a4711631b3a8f81b98292c0cff5f219f240dc15ea4e13a5fd06cec96e6968f7"
SRC_URI[NetworkManager-wifi.sha256sum] = "1ea173aac01b5f46d85d26271afb54123b4327724160b6782d3b8c03143de8b5"
SRC_URI[NetworkManager-wwan.sha256sum] = "98514480cadd0c79d70b2e68e42e9ae80c78dca1e2d031a976ed17fa565a403a"
