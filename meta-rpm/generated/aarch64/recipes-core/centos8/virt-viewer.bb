SUMMARY = "generated recipe based on virt-viewer srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 gtk-vnc libgovirt libsoup-2.4 libvirt libvirt-glib libxml2 pango pkgconfig-native rest spice-gtk"
RPM_SONAME_REQ_virt-viewer = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgovirt.so.2 libgthread-2.0.so.0 libgtk-3.so.0 libgtk-vnc-2.0.so.0 libgvnc-1.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 librest-0.7.so.0 libsoup-2.4.so.1 libspice-client-glib-2.0.so.8 libspice-client-gtk-3.0.so.5 libvirt-glib-1.0.so.0 libvirt.so.0 libxml2.so.2"
RDEPENDS_virt-viewer = "atk cairo cairo-gobject chkconfig desktop-file-utils gdk-pixbuf2 glib2 glibc gtk-vnc2 gtk3 gvnc libgovirt libsoup libvirt-glib libvirt-libs libxml2 openssh-clients pango rest spice-glib spice-gtk3"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/virt-viewer-7.0-9.el8.aarch64.rpm \
          "

SRC_URI[virt-viewer.sha256sum] = "92c9a89b841331f08f89b57772a2216d30c356fb31395689f667be4a8be86402"
