SUMMARY = "generated recipe based on nautilus srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gnome-autoar gnome-desktop3 gtk+3 libgcc libgexiv2 libselinux pango pkgconfig-native tracker"
RPM_SONAME_PROV_nautilus = "libnautilus-image-properties.so libnautilus-sendto.so"
RPM_SONAME_REQ_nautilus = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgailutil-3.so.0 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgexiv2.so.2 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgnome-autoar-0.so.0 libgnome-desktop-3.so.17 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libnautilus-extension.so.1 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libselinux.so.1 libtracker-sparql-2.0.so.0"
RDEPENDS_nautilus = "atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gnome-autoar gnome-desktop3 gsettings-desktop-schemas gtk3 gvfs libexif libgcc libgexiv2 libselinux nautilus-extensions pango tracker"
RPM_SONAME_REQ_nautilus-devel = "libnautilus-extension.so.1"
RPROVIDES_nautilus-devel = "nautilus-dev (= 3.28.1)"
RDEPENDS_nautilus-devel = "glib2-devel gtk3-devel nautilus nautilus-extensions pkgconf-pkg-config"
RPM_SONAME_PROV_nautilus-extensions = "libnautilus-extension.so.1"
RPM_SONAME_REQ_nautilus-extensions = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0"
RDEPENDS_nautilus-extensions = "glib2 glibc gtk3 libgcc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nautilus-3.28.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/nautilus-extensions-3.28.1-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/nautilus-devel-3.28.1-12.el8.aarch64.rpm \
          "

SRC_URI[nautilus.sha256sum] = "b510f49b1181da338f3c69fed65d64a9b48182c045985a1db0a32f084fb9edc5"
SRC_URI[nautilus-devel.sha256sum] = "ccd13ff3dc61152b074010aa0f46a754eb17f388eb0f06cbcc02a52be7f9703f"
SRC_URI[nautilus-extensions.sha256sum] = "6152a1ad8b5f13e16671a5ff676ad3e94360e78c555457cb4a681ba5027638bb"
