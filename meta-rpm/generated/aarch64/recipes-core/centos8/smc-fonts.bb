SUMMARY = "generated recipe based on smc-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_smc-anjalioldlipi-fonts = "smc-fonts-common"
RDEPENDS_smc-dyuthi-fonts = "smc-fonts-common"
RDEPENDS_smc-fonts-common = "fontpackages-filesystem"
RDEPENDS_smc-kalyani-fonts = "smc-fonts-common"
RDEPENDS_smc-meera-fonts = "smc-fonts-common"
RDEPENDS_smc-rachana-fonts = "smc-fonts-common"
RDEPENDS_smc-raghumalayalam-fonts = "smc-fonts-common"
RDEPENDS_smc-suruma-fonts = "smc-fonts-common"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/smc-anjalioldlipi-fonts-6.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/smc-dyuthi-fonts-6.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/smc-fonts-common-6.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/smc-kalyani-fonts-6.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/smc-meera-fonts-6.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/smc-rachana-fonts-6.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/smc-raghumalayalam-fonts-6.1-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/smc-suruma-fonts-6.1-10.el8.noarch.rpm \
          "

SRC_URI[smc-anjalioldlipi-fonts.sha256sum] = "8ddb1042417569b75317c08da1b554a4f86b1fad8944d6902a9da308ceb5e015"
SRC_URI[smc-dyuthi-fonts.sha256sum] = "ce9ec1a8a1a7038a8edf35913077301d202de9082d26061c700d4c933d0c10e7"
SRC_URI[smc-fonts-common.sha256sum] = "15eb2bb09cdec658970f81dfe0011e9ba0b996cdbe285eba4f39ff00e1fc9c40"
SRC_URI[smc-kalyani-fonts.sha256sum] = "687f33caa9517d4bd6e4297f3dcdab093ce9ef863f6a7d3fde096f1efd93b663"
SRC_URI[smc-meera-fonts.sha256sum] = "dafdf0f851a97f1012eaa8d535e5981fe10885a5af2c8bfb7844bd662da1ee18"
SRC_URI[smc-rachana-fonts.sha256sum] = "f19b12a2298046204636afdebe5f6dced1d3bdf16ac7b53684977bc6d84ecbda"
SRC_URI[smc-raghumalayalam-fonts.sha256sum] = "07b5a963b0bfbc0641c2cd94a43722df59b5bc19e6eba9569717f4e2a5d682c6"
SRC_URI[smc-suruma-fonts.sha256sum] = "97820cd13870fef1c35d7a59892c4efcf40dc06c4fef969b094797bd681df32a"
