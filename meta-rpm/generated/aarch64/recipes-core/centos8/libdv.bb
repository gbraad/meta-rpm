SUMMARY = "generated recipe based on libdv srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libdv = "libdv.so.4"
RPM_SONAME_REQ_libdv = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_libdv = "glibc"
RPM_SONAME_REQ_libdv-devel = "libdv.so.4"
RPROVIDES_libdv-devel = "libdv-dev (= 1.0.0)"
RDEPENDS_libdv-devel = "libdv pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libdv-1.0.0-27.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libdv-devel-1.0.0-27.el8.aarch64.rpm \
          "

SRC_URI[libdv.sha256sum] = "7ed999c03d233b8c9cf21a33555db761e0a98d722989af02d9fa6b90d930fc6c"
SRC_URI[libdv-devel.sha256sum] = "3662ce84e538d41772ab9a531b7ae1185a61f1b13c3a246c68f69ff7263403f8"
