SUMMARY = "generated recipe based on hyphen-gl srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-gl = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-gl-0.99-15.el8.noarch.rpm \
          "

SRC_URI[hyphen-gl.sha256sum] = "6e1399375c40a63268552cbacd907a2ae7a5fbd1a78dd6ebaca051715975a981"
