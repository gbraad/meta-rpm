SUMMARY = "generated recipe based on gdisk srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libuuid ncurses pkgconfig-native popt"
RPM_SONAME_REQ_gdisk = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libncursesw.so.6 libpopt.so.0 libstdc++.so.6 libtinfo.so.6 libuuid.so.1"
RDEPENDS_gdisk = "glibc libgcc libstdc++ libuuid ncurses-libs popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gdisk-1.0.3-6.el8.aarch64.rpm \
          "

SRC_URI[gdisk.sha256sum] = "2d727c662854847b3bd6c68b15609acc29195a9778ad81307a8e1d857006ae60"
