SUMMARY = "generated recipe based on snappy srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_snappy = "libsnappy.so.1"
RPM_SONAME_REQ_snappy = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_snappy = "glibc libgcc libstdc++"
RPM_SONAME_REQ_snappy-devel = "libsnappy.so.1"
RPROVIDES_snappy-devel = "snappy-dev (= 1.1.7)"
RDEPENDS_snappy-devel = "cmake-filesystem pkgconf-pkg-config snappy"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/snappy-1.1.7-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/snappy-devel-1.1.7-5.el8.aarch64.rpm \
          "

SRC_URI[snappy.sha256sum] = "62df1e376189aab65976262deb5f4fb1ede20d16ce55082918c716f35572667d"
SRC_URI[snappy-devel.sha256sum] = "bae1d001ec2f37dd4c5dc15ccffd4614bf42fd73dfa9988ddf2cd4bd879d2f32"
