SUMMARY = "generated recipe based on jboss-logging srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jboss-logging = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jboss-logging-3.3.0-5.el8.noarch.rpm \
          "

SRC_URI[jboss-logging.sha256sum] = "758d18a73df25c0f527996e9e2ec5602a6b348e79fa158c11e8f8750dc62b308"
