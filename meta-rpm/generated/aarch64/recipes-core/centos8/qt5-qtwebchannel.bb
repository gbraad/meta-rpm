SUMMARY = "generated recipe based on qt5-qtwebchannel srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc libglvnd pkgconfig-native qt5-qtbase qt5-qtdeclarative qt5-qtwebsockets"
RPM_SONAME_PROV_qt5-qtwebchannel = "libQt5WebChannel.so.5 libdeclarative_webchannel.so"
RPM_SONAME_REQ_qt5-qtwebchannel = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtwebchannel = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtwebchannel-devel = "libQt5WebChannel.so.5"
RPROVIDES_qt5-qtwebchannel-devel = "qt5-qtwebchannel-dev (= 5.12.5)"
RDEPENDS_qt5-qtwebchannel-devel = "cmake-filesystem pkgconf-pkg-config qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtwebchannel"
RPM_SONAME_REQ_qt5-qtwebchannel-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5Qml.so.5 libQt5WebChannel.so.5 libQt5WebSockets.so.5 libQt5Widgets.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtwebchannel-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtwebchannel qt5-qtwebsockets"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtwebchannel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtwebchannel-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtwebchannel-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtwebchannel.sha256sum] = "777846cff5c4ca6b82925374ccc1d12c2aeae3db1a5d00243260e30069c47469"
SRC_URI[qt5-qtwebchannel-devel.sha256sum] = "39841b08d09b702f889491f376e247cd87ce097b4b8a5e3dd0a76e021bafb48d"
SRC_URI[qt5-qtwebchannel-examples.sha256sum] = "650050e8b1660433970ed7f08d76fa69df777365b481acff651e6c52bccf5c9f"
