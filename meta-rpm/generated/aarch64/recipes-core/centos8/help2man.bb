SUMMARY = "generated recipe based on help2man srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_help2man = "bash info perl-Getopt-Long perl-Text-ParseWords perl-Text-Tabs+Wrap perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/help2man-1.47.6-1.el8.noarch.rpm \
          "

SRC_URI[help2man.sha256sum] = "fbc7636c37487bd7ffc2c6a466322cd5529c79b6116df4b736cce5e10ab7c717"
