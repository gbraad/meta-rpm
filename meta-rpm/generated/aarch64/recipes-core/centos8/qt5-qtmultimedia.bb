SUMMARY = "generated recipe based on qt5-qtmultimedia srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-3.0"
RPM_LICENSE = "LGPLv2 with exceptions or GPLv3 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "alsa-lib glib-2.0 gstreamer1.0 gstreamer1.0-plugins-bad gstreamer1.0-plugins-base libgcc libglvnd openal-soft pkgconfig-native pulseaudio qt5-qtbase qt5-qtdeclarative"
RPM_SONAME_PROV_qt5-qtmultimedia = "libQt5Multimedia.so.5 libQt5MultimediaGstTools.so.5 libQt5MultimediaQuick.so.5 libQt5MultimediaWidgets.so.5"
RPM_SONAME_REQ_qt5-qtmultimedia = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Widgets.so.5 libasound.so.2 libc.so.6 libgcc_s.so.1 libglib-2.0.so.0 libgobject-2.0.so.0 libgstapp-1.0.so.0 libgstaudio-1.0.so.0 libgstbase-1.0.so.0 libgstpbutils-1.0.so.0 libgstphotography-1.0.so.0 libgstreamer-1.0.so.0 libgstvideo-1.0.so.0 libm.so.6 libopenal.so.1 libpthread.so.0 libpulse-mainloop-glib.so.0 libpulse.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtmultimedia = "alsa-lib glib2 glibc gstreamer1 gstreamer1-plugins-bad-free gstreamer1-plugins-base libgcc libglvnd-glx libstdc++ openal-soft pulseaudio-libs pulseaudio-libs-glib2 qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative"
RPM_SONAME_REQ_qt5-qtmultimedia-devel = "libQt5Multimedia.so.5 libQt5MultimediaGstTools.so.5 libQt5MultimediaQuick.so.5 libQt5MultimediaWidgets.so.5"
RPROVIDES_qt5-qtmultimedia-devel = "qt5-qtmultimedia-dev (= 5.12.5)"
RDEPENDS_qt5-qtmultimedia-devel = "cmake-filesystem pkgconf-pkg-config pulseaudio-libs-devel qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtmultimedia"
RPM_SONAME_PROV_qt5-qtmultimedia-examples = "libfftreal.so.1"
RPM_SONAME_REQ_qt5-qtmultimedia-examples = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5Gui.so.5 libQt5Multimedia.so.5 libQt5MultimediaWidgets.so.5 libQt5Network.so.5 libQt5OpenGL.so.5 libQt5Qml.so.5 libQt5Quick.so.5 libQt5Widgets.so.5 libQt5Xml.so.5 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qt5-qtmultimedia-examples = "glibc libgcc libglvnd-glx libstdc++ qt5-qtbase qt5-qtbase-gui qt5-qtdeclarative qt5-qtmultimedia"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtmultimedia-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtmultimedia-devel-5.12.5-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qt5-qtmultimedia-examples-5.12.5-1.el8.aarch64.rpm \
          "

SRC_URI[qt5-qtmultimedia.sha256sum] = "d9a84525cfee5b0a05f2736c555d0c9effb2b875667cce6d3e5d6b7ffc82dfbb"
SRC_URI[qt5-qtmultimedia-devel.sha256sum] = "a63aa76870b809b8a5ac1f49839d75e0efc68aa74f93cfd920b6c35011fc5c29"
SRC_URI[qt5-qtmultimedia-examples.sha256sum] = "3a15ccdb350711ca8625a9bedbf7793bc055da6db2064036988022f0d24a4824"
