SUMMARY = "generated recipe based on gnome-user-docs srpm"
DESCRIPTION = "Description"
LICENSE = "CC-BY-SA-1.0"
RPM_LICENSE = "CC-BY-SA"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-user-docs-3.28.2-1.el8.noarch.rpm \
          "

SRC_URI[gnome-user-docs.sha256sum] = "32de5a3562bae9bd962601e2cdb740042ac2584dcbeebe40db98d3cc1aab8a33"
