SUMMARY = "generated recipe based on plexus-cipher srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_plexus-cipher = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_plexus-cipher-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-cipher-1.7-14.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/plexus-cipher-javadoc-1.7-14.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[plexus-cipher.sha256sum] = "ca4d39dc79df535144d6d5ae78808bd7587b82beb70359537ae1e0b3a9314656"
SRC_URI[plexus-cipher-javadoc.sha256sum] = "6bf0d79136a9f496039d8a69c3dfd0ce5ac585e6adfddf6a935a972c28b4d022"
