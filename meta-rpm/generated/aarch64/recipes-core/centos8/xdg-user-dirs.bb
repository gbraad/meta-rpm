SUMMARY = "generated recipe based on xdg-user-dirs srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & MIT"
RPM_LICENSE = "GPLv2+ and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xdg-user-dirs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_xdg-user-dirs = "bash filesystem glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xdg-user-dirs-0.17-1.el8.aarch64.rpm \
          "

SRC_URI[xdg-user-dirs.sha256sum] = "d95f18da6f8fae59d46228c8ee778e4da66ecbab5da31276006477cf63fbc747"
