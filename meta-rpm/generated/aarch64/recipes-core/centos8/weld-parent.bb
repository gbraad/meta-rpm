SUMMARY = "generated recipe based on weld-parent srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_weld-parent = "java-1.8.0-openjdk-headless javapackages-filesystem maven-plugin-build-helper maven-source-plugin"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/weld-parent-34-5.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[weld-parent.sha256sum] = "5747d16d4c7903bca652142ce10f0c5a277645c9b48fa65d674cb41a49025dda"
