SUMMARY = "generated recipe based on python-unittest2 srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-unittest2 = "platform-python platform-python-setuptools python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/python3-unittest2-1.1.0-16.el8.noarch.rpm \
          "

SRC_URI[python3-unittest2.sha256sum] = "af73af8e3851cacefdb6b6ccde14d7bda06f7993b219b070469755cf404c81e0"
