SUMMARY = "generated recipe based on hyphen-ro srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-ro = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-ro-3.3.6-13.el8.noarch.rpm \
          "

SRC_URI[hyphen-ro.sha256sum] = "8f5f85899e1d312cfd2c02aa6e0121b8a27da45d1ef7fcb612a16e9fb17b3663"
