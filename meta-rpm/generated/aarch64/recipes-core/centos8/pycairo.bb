SUMMARY = "generated recipe based on pycairo srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-1.1 | LGPL-2.0"
RPM_LICENSE = "MPLv1.1 or LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cairo pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-cairo = "ld-linux-aarch64.so.1 libc.so.6 libcairo.so.2 libpthread.so.0 libpython3.6m.so.1.0"
RPROVIDES_python3-cairo = "python3-pycairo (= 1.16.3)"
RDEPENDS_python3-cairo = "cairo glibc platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-cairo-1.16.3-6.el8.aarch64.rpm \
          "

SRC_URI[python3-cairo.sha256sum] = "84ee9acac31c9363ccca04a9fab7358b9ed979f41faa7742f96b45a37df56663"
