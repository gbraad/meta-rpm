SUMMARY = "generated recipe based on python-dbus-python-client-gen srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-2.0"
RPM_LICENSE = "MPLv2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-dbus-python-client-gen = "platform-python python3-dbus python3-into-dbus-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-dbus-python-client-gen-0.7-3.el8.noarch.rpm \
          "

SRC_URI[python3-dbus-python-client-gen.sha256sum] = "6838d47e2ed1baf5743228a7f98ab4e62e70815509128aa69bb93417a5e77c3c"
