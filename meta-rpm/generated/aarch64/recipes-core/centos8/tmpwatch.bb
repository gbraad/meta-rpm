SUMMARY = "generated recipe based on tmpwatch srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_tmpwatch = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_tmpwatch = "glibc psmisc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tmpwatch-2.11-14.el8.aarch64.rpm \
          "

SRC_URI[tmpwatch.sha256sum] = "b19d7ef7cac48df94677e48b4a3c0b7ee46b378f8b12518d83543f652af3c836"
