SUMMARY = "generated recipe based on bison srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_bison = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_bison = "bash glibc info m4"
RPROVIDES_bison-devel = "bison-dev (= 3.0.4)"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bison-3.0.4-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bison-runtime-3.0.4-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/bison-devel-3.0.4-10.el8.aarch64.rpm \
          "

SRC_URI[bison.sha256sum] = "31a8e833732f84ca07d7477628596f7c3dd1571f87dbb6e429c729359a474c97"
SRC_URI[bison-devel.sha256sum] = "5a4fe5aca9ad8d99fa32cb8c45472bb978beb5928b315532eef2832c907edc31"
SRC_URI[bison-runtime.sha256sum] = "6f1180b159341fd496cebac5176fe809d1bb9dbec1db754689a3ea33ded37dd7"
