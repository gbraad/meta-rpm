SUMMARY = "generated recipe based on libtheora srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libogg libpng libvorbis pkgconfig-native sdl zlib"
RPM_SONAME_PROV_libtheora = "libtheora.so.0 libtheoradec.so.1 libtheoraenc.so.1"
RPM_SONAME_REQ_libtheora = "ld-linux-aarch64.so.1 libc.so.6 libogg.so.0"
RDEPENDS_libtheora = "glibc libogg"
RPM_SONAME_REQ_libtheora-devel = "libtheora.so.0 libtheoradec.so.1 libtheoraenc.so.1"
RPROVIDES_libtheora-devel = "libtheora-dev (= 1.1.1)"
RDEPENDS_libtheora-devel = "libogg-devel libtheora pkgconf-pkg-config"
RPM_SONAME_REQ_theora-tools = "ld-linux-aarch64.so.1 libSDL-1.2.so.0 libc.so.6 libm.so.6 libogg.so.0 libpng16.so.16 libpthread.so.0 libtheoradec.so.1 libtheoraenc.so.1 libvorbis.so.0 libvorbisenc.so.2 libz.so.1"
RDEPENDS_theora-tools = "SDL glibc libogg libpng libtheora libvorbis zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libtheora-1.1.1-21.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/theora-tools-1.1.1-21.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libtheora-devel-1.1.1-21.el8.aarch64.rpm \
          "

SRC_URI[libtheora.sha256sum] = "9cb271338e138ca74e2486c9a256de43d3e10980d825433bb2f8930eaf49198b"
SRC_URI[libtheora-devel.sha256sum] = "e3ab63bf760681aa8d8033c59479cd90026a036c8b1e60455b34cbc8f07a2568"
SRC_URI[theora-tools.sha256sum] = "f974a825fc7d869f2a426a0476009c7f7d60c281bf56611434483b96ad308b6f"
