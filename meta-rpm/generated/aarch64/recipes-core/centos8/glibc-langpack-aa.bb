SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-aa = "locale-base-aa-dj (= 2.28) locale-base-aa-dj.utf8 (= 2.28) locale-base-aa-er (= 2.28) locale-base-aa-er@saaho (= 2.28) locale-base-aa-et (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa (= 2.28) virtual-locale-aa-dj (= 2.28) virtual-locale-aa-dj.utf8 (= 2.28) virtual-locale-aa-er (= 2.28) virtual-locale-aa-er@saaho (= 2.28) virtual-locale-aa-et (= 2.28)"
RDEPENDS_glibc-langpack-aa = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-aa-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-aa.sha256sum] = "f43a065c8849c731fbad46bb46b12d33857143a0e7034b5b1ecac13c8340c7a8"
