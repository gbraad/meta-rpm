SUMMARY = "generated recipe based on urw-base35-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "AGPL-3.0"
RPM_LICENSE = "AGPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_urw-base35-bookman-fonts = "bash fontconfig urw-base35-fonts-common xorg-x11-font-utils xorg-x11-server-utils"
RDEPENDS_urw-base35-c059-fonts = "bash fontconfig urw-base35-fonts-common xorg-x11-font-utils xorg-x11-server-utils"
RDEPENDS_urw-base35-d050000l-fonts = "bash fontconfig urw-base35-fonts-common xorg-x11-font-utils xorg-x11-server-utils"
RDEPENDS_urw-base35-fonts = "urw-base35-bookman-fonts urw-base35-c059-fonts urw-base35-d050000l-fonts urw-base35-fonts-common urw-base35-gothic-fonts urw-base35-nimbus-mono-ps-fonts urw-base35-nimbus-roman-fonts urw-base35-nimbus-sans-fonts urw-base35-p052-fonts urw-base35-standard-symbols-ps-fonts urw-base35-z003-fonts"
RDEPENDS_urw-base35-fonts-common = "filesystem fontpackages-filesystem"
RPROVIDES_urw-base35-fonts-devel = "urw-base35-fonts-dev (= 20170801)"
RDEPENDS_urw-base35-fonts-devel = "urw-base35-fonts"
RDEPENDS_urw-base35-gothic-fonts = "bash fontconfig urw-base35-fonts-common xorg-x11-font-utils xorg-x11-server-utils"
RDEPENDS_urw-base35-nimbus-mono-ps-fonts = "bash fontconfig urw-base35-fonts-common xorg-x11-font-utils xorg-x11-server-utils"
RDEPENDS_urw-base35-nimbus-roman-fonts = "bash fontconfig urw-base35-fonts-common xorg-x11-font-utils xorg-x11-server-utils"
RDEPENDS_urw-base35-nimbus-sans-fonts = "bash fontconfig urw-base35-fonts-common xorg-x11-font-utils xorg-x11-server-utils"
RDEPENDS_urw-base35-p052-fonts = "bash fontconfig urw-base35-fonts-common xorg-x11-font-utils xorg-x11-server-utils"
RDEPENDS_urw-base35-standard-symbols-ps-fonts = "bash fontconfig urw-base35-fonts-common xorg-x11-font-utils xorg-x11-server-utils"
RDEPENDS_urw-base35-z003-fonts = "bash fontconfig urw-base35-fonts-common xorg-x11-font-utils xorg-x11-server-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-bookman-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-c059-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-d050000l-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-fonts-common-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-gothic-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-nimbus-mono-ps-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-nimbus-roman-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-nimbus-sans-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-p052-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-standard-symbols-ps-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/urw-base35-z003-fonts-20170801-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/urw-base35-fonts-devel-20170801-10.el8.noarch.rpm \
          "

SRC_URI[urw-base35-bookman-fonts.sha256sum] = "3e2a3f4f59afbebd49e4140ba96059df7482e3086b7a6003241b994f981ad404"
SRC_URI[urw-base35-c059-fonts.sha256sum] = "951853a4db3f15b4af83fc2968234f3a33906f0c20691a5f79a5dc14ec917622"
SRC_URI[urw-base35-d050000l-fonts.sha256sum] = "68369bfb7cb11f156acf54512d78dd89e7042f1b9d1cb28982c469338d0215f6"
SRC_URI[urw-base35-fonts.sha256sum] = "1b638dbee35e28810111c98c7700171ed8cce0057c3fad747049d52d53c19aa6"
SRC_URI[urw-base35-fonts-common.sha256sum] = "a94ed72fc0e1bc688e2017b955020faf90978bde4c4ca781519cdfcc75a23471"
SRC_URI[urw-base35-fonts-devel.sha256sum] = "f39eb38d043f62d59d4154e2b450a6cd4e8c8549ebb4f83116d99c43bb27e1a1"
SRC_URI[urw-base35-gothic-fonts.sha256sum] = "913c906b755a61d296ad346c20e1fbca14ecf5dd7f3278939869a2cfc003b220"
SRC_URI[urw-base35-nimbus-mono-ps-fonts.sha256sum] = "64f0224b305e4bc0e4dd338d8de1e17b0fdfee08810c997d76a857dfe0f6758d"
SRC_URI[urw-base35-nimbus-roman-fonts.sha256sum] = "d4d260496bf2d9e64938f8172bc00c9834722c8fcd8fef21483802b08fa4121f"
SRC_URI[urw-base35-nimbus-sans-fonts.sha256sum] = "8652726acef6956b52fd32b4e692e24c4a02b0cdc2ce3c2dd26f0e80aa5dee06"
SRC_URI[urw-base35-p052-fonts.sha256sum] = "6d173b483be3e0d853fe9a3abe2baf8ce0dd03e5c53542b44918b8a3ef8ffb4c"
SRC_URI[urw-base35-standard-symbols-ps-fonts.sha256sum] = "f47536da37e7de48e9f247d8651a226fa69a826f4dcd8d91d2f1b81ef33380c6"
SRC_URI[urw-base35-z003-fonts.sha256sum] = "dce64b3cc8c4ca6b30b5a88d958ed28d9bb9e8a4bb6f33d116ca13eff7caae2f"
