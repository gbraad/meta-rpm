SUMMARY = "generated recipe based on libteam srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-libs jansson libdaemon libnl pkgconfig-native"
RPM_SONAME_PROV_libteam = "libteam.so.5"
RPM_SONAME_REQ_libteam = "ld-linux-aarch64.so.1 libc.so.6 libnl-3.so.200 libnl-cli-3.so.200 libnl-genl-3.so.200 libnl-nf-3.so.200 libnl-route-3.so.200"
RDEPENDS_libteam = "glibc libnl3 libnl3-cli"
RDEPENDS_libteam-doc = "libteam"
RDEPENDS_network-scripts-team = "bash network-scripts"
RPM_SONAME_PROV_teamd = "libteamdctl.so.0"
RPM_SONAME_REQ_teamd = "ld-linux-aarch64.so.1 libc.so.6 libdaemon.so.0 libdbus-1.so.3 libjansson.so.4 libnl-3.so.200 libnl-cli-3.so.200 libnl-genl-3.so.200 libnl-nf-3.so.200 libnl-route-3.so.200 libteam.so.5"
RDEPENDS_teamd = "bash dbus-libs glibc jansson libdaemon libnl3 libnl3-cli libteam"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libteam-1.29-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libteam-doc-1.29-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/network-scripts-team-1.29-1.el8_2.2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/teamd-1.29-1.el8_2.2.aarch64.rpm \
          "

SRC_URI[libteam.sha256sum] = "ea3b1d9c329b41339ecb0a0e235c088b1a9fe2f998fadc9398ac546800a3d416"
SRC_URI[libteam-doc.sha256sum] = "7c26be27b879bd8768880549ab68adde7459819c7b778e6e806d8e8472fa1188"
SRC_URI[network-scripts-team.sha256sum] = "d593ae632d203d7e7335d9ad5fb370fe609802eeb033e8b7a3e91ab923006af6"
SRC_URI[teamd.sha256sum] = "5f5aa8900c1a5b01271fefd57df5375e40c095f2f5d4fc6ef330404d430cc94f"
