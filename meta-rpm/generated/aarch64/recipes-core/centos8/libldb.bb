SUMMARY = "generated recipe based on libldb srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libtalloc libtdb libtevent libxcrypt openldap pkgconfig-native platform-python3 popt"
RPM_SONAME_PROV_ldb-tools = "libldb-cmdline.so"
RPM_SONAME_REQ_ldb-tools = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libldb.so.2 libpopt.so.0 libpthread.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_ldb-tools = "glibc libldb libtalloc libtdb libtevent libxcrypt popt"
RPM_SONAME_PROV_libldb = "libldb-key-value.so libldb-tdb-err-map.so libldb-tdb-int.so libldb.so.2"
RPM_SONAME_REQ_libldb = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 liblber-2.4.so.2 libldap-2.4.so.2 libpthread.so.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0"
RDEPENDS_libldb = "glibc libtalloc libtdb libtevent libxcrypt openldap"
RPM_SONAME_REQ_libldb-devel = "libldb.so.2"
RPROVIDES_libldb-devel = "libldb-dev (= 2.0.7)"
RDEPENDS_libldb-devel = "libldb libtalloc-devel libtdb-devel libtevent-devel pkgconf-pkg-config"
RPM_SONAME_PROV_python3-ldb = "libpyldb-util.cpython-36m-aarch64-linux-gnu.so.2"
RPM_SONAME_REQ_python3-ldb = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libdl.so.2 libldb.so.2 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 libtalloc.so.2 libtdb.so.1 libtevent.so.0 libutil.so.1"
RDEPENDS_python3-ldb = "glibc libldb libtalloc libtdb libtevent libxcrypt platform-python python3-libs python3-tdb"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ldb-tools-2.0.7-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libldb-2.0.7-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libldb-devel-2.0.7-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-ldb-2.0.7-3.el8.aarch64.rpm \
          "

SRC_URI[ldb-tools.sha256sum] = "be44ad6bf53278253526d43897de492355094581c123769c3cc789c1e8af70c2"
SRC_URI[libldb.sha256sum] = "48dcc824d7ef09b06a600af0022451f953632e0c5a673d219bb077fafdab81b7"
SRC_URI[libldb-devel.sha256sum] = "31bd4eb1cc46ddbf9ec2356159c50daf48c2c8f9cb87cf825218eccde08a3953"
SRC_URI[python3-ldb.sha256sum] = "61c6e747ee84c15e593a879e1667fe31ee7307a1613edd5e46b3ed9ffd414556"
