SUMMARY = "generated recipe based on perl-podlators srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and FSFAP"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-podlators = "perl-Carp perl-Encode perl-Exporter perl-Getopt-Long perl-PathTools perl-Pod-Simple perl-Pod-Usage perl-Term-ANSIColor perl-Term-Cap perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-podlators-4.11-1.el8.noarch.rpm \
          "

SRC_URI[perl-podlators.sha256sum] = "78d17ed089151e7fa3d1a3cdbbac8ca3b1b5c484fae5ba025642cc9107991037"
