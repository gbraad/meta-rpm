SUMMARY = "generated recipe based on gtkmm24 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk atkmm cairo cairomm fontconfig freetype gdk-pixbuf glib-2.0 glibmm24 gtk2 libgcc libsigc++20 pango pangomm pkgconfig-native"
RPM_SONAME_PROV_gtkmm24 = "libgdkmm-2.4.so.1 libgtkmm-2.4.so.1"
RPM_SONAME_REQ_gtkmm24 = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libatkmm-1.6.so.1 libc.so.6 libcairo.so.2 libcairomm-1.0.so.1 libfontconfig.so.1 libfreetype.so.6 libgcc_s.so.1 libgdk-x11-2.0.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libgiomm-2.4.so.1 libglib-2.0.so.0 libglibmm-2.4.so.1 libgobject-2.0.so.0 libgtk-x11-2.0.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpangoft2-1.0.so.0 libpangomm-1.4.so.1 libsigc-2.0.so.0 libstdc++.so.6"
RDEPENDS_gtkmm24 = "atk atkmm cairo cairomm fontconfig freetype gdk-pixbuf2 glib2 glibc glibmm24 gtk2 libgcc libsigc++20 libstdc++ pango pangomm"
RPM_SONAME_REQ_gtkmm24-devel = "libgdkmm-2.4.so.1 libgtkmm-2.4.so.1"
RPROVIDES_gtkmm24-devel = "gtkmm24-dev (= 2.24.5)"
RDEPENDS_gtkmm24-devel = "atkmm-devel cairomm-devel glibmm24-devel gtk2-devel gtkmm24 pangomm-devel pkgconf-pkg-config"
RDEPENDS_gtkmm24-docs = "glibmm24-doc gtkmm24"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gtkmm24-2.24.5-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gtkmm24-devel-2.24.5-5.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/gtkmm24-docs-2.24.5-5.el8.noarch.rpm \
          "

SRC_URI[gtkmm24.sha256sum] = "c36530f21336bd7281740efa7725a98d3b0ec26a228bae1a359aeb9230ee8086"
SRC_URI[gtkmm24-devel.sha256sum] = "3db75743620998b8a71054f2f4ff727e5a3d7e3eca581986f0eb00c877aaae28"
SRC_URI[gtkmm24-docs.sha256sum] = "65ca96b37f1d10de8720c91d8cb5c82fe050d26eae423e301faf4fd9b9aba497"
