SUMMARY = "generated recipe based on qgnomeplatform srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk cairo gdk-pixbuf glib-2.0 gtk+3 libgcc libglvnd pango pkgconfig-native qt5-qtbase"
RPM_SONAME_PROV_qgnomeplatform = "libqgnomeplatform.so"
RPM_SONAME_REQ_qgnomeplatform = "ld-linux-aarch64.so.1 libGL.so.1 libQt5Core.so.5 libQt5DBus.so.5 libQt5Gui.so.5 libQt5Widgets.so.5 libatk-1.0.so.0 libc.so.6 libcairo-gobject.so.2 libcairo.so.2 libgcc_s.so.1 libgdk-3.so.0 libgdk_pixbuf-2.0.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libm.so.6 libpango-1.0.so.0 libpangocairo-1.0.so.0 libpthread.so.0 libstdc++.so.6"
RDEPENDS_qgnomeplatform = "adwaita-qt atk cairo cairo-gobject gdk-pixbuf2 glib2 glibc gtk3 libgcc libglvnd-glx libstdc++ pango qt5-qtbase qt5-qtbase-gui"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/qgnomeplatform-0.4-3.el8.aarch64.rpm \
          "

SRC_URI[qgnomeplatform.sha256sum] = "59b4b47575f63fc3ab46b5e9a4d0129caaf50a8aa05952ba83b7904ddd10d8a6"
