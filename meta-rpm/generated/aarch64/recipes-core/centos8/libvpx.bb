SUMMARY = "generated recipe based on libvpx srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_libvpx = "libvpx.so.5"
RPM_SONAME_REQ_libvpx = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libpthread.so.0 libstdc++.so.6"
RDEPENDS_libvpx = "glibc libgcc libstdc++"
RPM_SONAME_REQ_libvpx-devel = "libvpx.so.5"
RPROVIDES_libvpx-devel = "libvpx-dev (= 1.7.0)"
RDEPENDS_libvpx-devel = "libvpx pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libvpx-1.7.0-6.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libvpx-devel-1.7.0-6.el8.aarch64.rpm \
          "

SRC_URI[libvpx.sha256sum] = "806cea53189fd0adf86810da6c37bc1a9b4507f076a6af71ed90fcc53ae720fa"
SRC_URI[libvpx-devel.sha256sum] = "5213628a6253ff35b4e84aa7597a791e683006bafb65af6c746fa340cd067900"
