SUMMARY = "generated recipe based on farstream02 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "LGPLv2+ and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 gstreamer1.0 gstreamer1.0-plugins-base gupnp gupnp-igd libnice pkgconfig-native"
RPM_SONAME_PROV_farstream02 = "libfarstream-0.2.so.5 libfsmsnconference.so libfsrawconference.so libfsrtpconference.so libfsrtpxdata.so libfsvideoanyrate.so libmulticast-transmitter.so libnice-transmitter.so librawudp-transmitter.so libshm-transmitter.so"
RPM_SONAME_REQ_farstream02 = "ld-linux-aarch64.so.1 libc.so.6 libgio-2.0.so.0 libglib-2.0.so.0 libgmodule-2.0.so.0 libgobject-2.0.so.0 libgstbase-1.0.so.0 libgstnet-1.0.so.0 libgstreamer-1.0.so.0 libgstrtp-1.0.so.0 libgthread-2.0.so.0 libgupnp-1.0.so.4 libgupnp-igd-1.0.so.4 libm.so.6 libnice.so.10 libpthread.so.0"
RDEPENDS_farstream02 = "glib2 glibc gstreamer1 gstreamer1-plugins-bad-free gstreamer1-plugins-base gstreamer1-plugins-good gupnp gupnp-igd libnice libnice-gstreamer1"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/farstream02-0.2.8-2.el8.aarch64.rpm \
          "

SRC_URI[farstream02.sha256sum] = "085b3eed3e015a807a10736e4a905da4975765db9d29622df9268e05ad427812"
