SUMMARY = "generated recipe based on sane-backends srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0 & CLOSED & CLOSED & LGPL-2.0 & MIT"
RPM_LICENSE = "GPLv2+ and GPLv2+ with exceptions and Public Domain and IJG and LGPLv2+ and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgphoto2 libieee1284 libjpeg-turbo libpng libusb1 pkgconfig-native systemd-libs tiff v4l-utils"
RPM_SONAME_REQ_sane-backends = "ld-linux-aarch64.so.1 libc.so.6 libieee1284.so.3 libjpeg.so.62 libm.so.6 libpng16.so.16 libsane.so.1 libusb-1.0.so.0"
RDEPENDS_sane-backends = "bash glibc grep libieee1284 libjpeg-turbo libpng libusbx sane-backends-libs sed systemd systemd-udev"
RPM_SONAME_REQ_sane-backends-daemon = "ld-linux-aarch64.so.1 libc.so.6 libsane.so.1 libsystemd.so.0"
RDEPENDS_sane-backends-daemon = "bash glibc sane-backends sane-backends-libs shadow-utils systemd systemd-libs"
RPM_SONAME_REQ_sane-backends-devel = "libsane.so.1"
RPROVIDES_sane-backends-devel = "sane-backends-dev (= 1.0.27)"
RDEPENDS_sane-backends-devel = "bash libieee1284-devel libjpeg-turbo-devel libtiff-devel libusbx-devel pkgconf-pkg-config sane-backends sane-backends-drivers-cameras sane-backends-drivers-scanners sane-backends-libs"
RPM_SONAME_REQ_sane-backends-drivers-cameras = "ld-linux-aarch64.so.1 libc.so.6 libgphoto2.so.6 libgphoto2_port.so.12 libjpeg.so.62 libm.so.6"
RDEPENDS_sane-backends-drivers-cameras = "glibc libgphoto2 libjpeg-turbo sane-backends sane-backends-libs"
RPM_SONAME_REQ_sane-backends-drivers-scanners = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libieee1284.so.3 libjpeg.so.62 libm.so.6 libpthread.so.0 libtiff.so.5 libusb-1.0.so.0 libv4l1.so.0"
RDEPENDS_sane-backends-drivers-scanners = "glibc libieee1284 libjpeg-turbo libtiff libusbx libv4l sane-backends sane-backends-libs"
RPM_SONAME_PROV_sane-backends-libs = "libsane.so.1"
RPM_SONAME_REQ_sane-backends-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgphoto2.so.6 libgphoto2_port.so.12 libieee1284.so.3 libjpeg.so.62 libm.so.6 libpthread.so.0 libtiff.so.5 libusb-1.0.so.0 libv4l1.so.0"
RDEPENDS_sane-backends-libs = "glibc libgphoto2 libieee1284 libjpeg-turbo libtiff libusbx libv4l"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sane-backends-1.0.27-19.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sane-backends-daemon-1.0.27-19.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sane-backends-devel-1.0.27-19.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sane-backends-doc-1.0.27-19.el8_2.1.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sane-backends-drivers-cameras-1.0.27-19.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sane-backends-drivers-scanners-1.0.27-19.el8_2.1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sane-backends-libs-1.0.27-19.el8_2.1.aarch64.rpm \
          "

SRC_URI[sane-backends.sha256sum] = "f52da823be8899682b53cdb41917738a3821f0d4233259aa3fb54d611f64e2cb"
SRC_URI[sane-backends-daemon.sha256sum] = "420b0fde202fd94c72c4b3d7eaf92be0e88e76cb21a81a013a5659a276228823"
SRC_URI[sane-backends-devel.sha256sum] = "351282e765e9b25dd54021ba19b9ef1b6f2d302b4071c288be558baaec1dcf1c"
SRC_URI[sane-backends-doc.sha256sum] = "850627ee2067694b915dcdd042651e65f2b047523bb9a039c50966f66aec06b5"
SRC_URI[sane-backends-drivers-cameras.sha256sum] = "dcc55c7659c6b5b224bf16526d65470a8c6400a4570c77ab2b285c597bcdfee2"
SRC_URI[sane-backends-drivers-scanners.sha256sum] = "f954caefbc326cb5296abaff0f5a05ebcd703c81085f86a7ad186319ee135934"
SRC_URI[sane-backends-libs.sha256sum] = "23ff85507be04c8f4fc234e02a19d9f23c13e8c10f68611663f6a7366064abb8"
