SUMMARY = "generated recipe based on kmod srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc openssl pkgconfig-native xz zlib"
RPM_SONAME_REQ_kmod = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 liblzma.so.5 libz.so.1"
RDEPENDS_kmod = "bash glibc libgcc openssl-libs xz-libs zlib"
RPM_SONAME_REQ_kmod-devel = "libkmod.so.2"
RPROVIDES_kmod-devel = "kmod-dev (= 25)"
RDEPENDS_kmod-devel = "kmod-libs pkgconf-pkg-config"
RPM_SONAME_PROV_kmod-libs = "libkmod.so.2"
RPM_SONAME_REQ_kmod-libs = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libgcc_s.so.1 liblzma.so.5 libssl.so.1.1 libz.so.1"
RDEPENDS_kmod-libs = "glibc libgcc openssl-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kmod-25-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/kmod-libs-25-16.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/kmod-devel-25-16.el8.aarch64.rpm \
          "

SRC_URI[kmod.sha256sum] = "7bcba5de3c015d001ef55a6b9e29f95b5b5ef680f3a0ad9c9938ed771cbb9fd1"
SRC_URI[kmod-devel.sha256sum] = "72f988e9d834a66aa6004cd88496f83d814110a8c264cf7d5969d3241a59086b"
SRC_URI[kmod-libs.sha256sum] = "1f1151d90463a1c3ab2ce2be40402585263150a3de0f6beb75224da9ded2fb9e"
