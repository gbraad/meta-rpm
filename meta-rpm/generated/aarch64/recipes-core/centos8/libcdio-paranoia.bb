SUMMARY = "generated recipe based on libcdio-paranoia srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libcdio pkgconfig-native"
RPM_SONAME_PROV_libcdio-paranoia = "libcdio_cdda.so.2 libcdio_paranoia.so.2"
RPM_SONAME_REQ_libcdio-paranoia = "ld-linux-aarch64.so.1 libc.so.6 libcdio.so.18 libm.so.6 librt.so.1"
RDEPENDS_libcdio-paranoia = "glibc libcdio"
RPM_SONAME_REQ_libcdio-paranoia-devel = "libcdio_cdda.so.2 libcdio_paranoia.so.2"
RPROVIDES_libcdio-paranoia-devel = "libcdio-paranoia-dev (= 10.2+0.94+2)"
RDEPENDS_libcdio-paranoia-devel = "libcdio-devel libcdio-paranoia pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libcdio-paranoia-10.2+0.94+2-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libcdio-paranoia-devel-10.2+0.94+2-3.el8.aarch64.rpm \
          "

SRC_URI[libcdio-paranoia.sha256sum] = "9190b11680e4252ca43f6f8ad0e5dcb002c3b2e987b67d57e7e564ba1dc58b6d"
SRC_URI[libcdio-paranoia-devel.sha256sum] = "f42ce513c405a1d98d486750ab1789fef09fe2ebb94d369cb2d73f3db627a48b"
