SUMMARY = "generated recipe based on libnet srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libnet = "libnet.so.1"
RPM_SONAME_REQ_libnet = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libnet = "glibc"
RPM_SONAME_REQ_libnet-devel = "libnet.so.1"
RPROVIDES_libnet-devel = "libnet-dev (= 1.1.6)"
RDEPENDS_libnet-devel = "bash libnet"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libnet-1.1.6-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libnet-devel-1.1.6-15.el8.aarch64.rpm \
          "

SRC_URI[libnet.sha256sum] = "a589ad38b2fb4739302b0dbc8171f9036e97398a11d83c5fd380dc26a1c9aab9"
SRC_URI[libnet-devel.sha256sum] = "087030afcf9be3ca31efcc6ef0b7d08e023496cb9de17dd97b0fd4a65508e7be"
