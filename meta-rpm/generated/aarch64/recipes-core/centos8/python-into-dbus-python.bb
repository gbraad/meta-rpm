SUMMARY = "generated recipe based on python-into-dbus-python srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-into-dbus-python = "platform-python python3-dbus python3-dbus-signature-pyparsing"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-into-dbus-python-0.06-2.el8.noarch.rpm \
          "

SRC_URI[python3-into-dbus-python.sha256sum] = "fb861d5c01b708d059e334b8f308aa56673d902b735282e1f7afa108fdc81f14"
