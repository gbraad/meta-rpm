SUMMARY = "generated recipe based on setools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libselinux libsepol pkgconfig-native platform-python3"
RPM_SONAME_REQ_python3-setools = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpython3.6m.so.1.0 libselinux.so.1 libsepol.so.1"
RDEPENDS_python3-setools = "glibc libselinux libsepol platform-python platform-python-setuptools python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-setools-4.2.2-2.el8.aarch64.rpm \
          "

SRC_URI[python3-setools.sha256sum] = "5ef7e8472bf2606233aeed0a7c713c068ada9221a5feb226b16d48a32a5f6511"
