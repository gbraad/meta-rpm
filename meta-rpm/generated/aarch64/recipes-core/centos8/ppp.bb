SUMMARY = "generated recipe based on ppp srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & LGPL-2.0 & GPL-2.0 & CLOSED"
RPM_LICENSE = "BSD and LGPLv2+ and GPLv2+ and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "glib-2.0 libpcap libxcrypt openssl pam pkgconfig-native systemd-libs"
RPM_SONAME_REQ_ppp = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libcrypto.so.1.1 libdl.so.2 libglib-2.0.so.0 libpam.so.0 libpcap.so.1 libresolv.so.2 libssl.so.1.1 libudev.so.1 libutil.so.1"
RDEPENDS_ppp = "bash glib2 glibc glibc-common libpcap libxcrypt openssl-libs pam shadow-utils systemd systemd-libs"
RPROVIDES_ppp-devel = "ppp-dev (= 2.4.7)"
RDEPENDS_ppp-devel = "ppp"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/ppp-2.4.7-26.el8_1.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/ppp-devel-2.4.7-26.el8_1.aarch64.rpm \
          "

SRC_URI[ppp.sha256sum] = "c5e96a442288b657c718878fb96ba7ac1bd8dbe6e809931ec78756b1c675b7b8"
SRC_URI[ppp-devel.sha256sum] = "980fb8273be2adb371d91fe958ca2d066a6e86d0a2b060b4784d6c28621ed004"
