SUMMARY = "generated recipe based on libcgroup srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pam pkgconfig-native"
RPM_SONAME_PROV_libcgroup = "libcgroup.so.1"
RPM_SONAME_REQ_libcgroup = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0"
RDEPENDS_libcgroup = "bash glibc shadow-utils systemd"
RPM_SONAME_REQ_libcgroup-pam = "ld-linux-aarch64.so.1 libc.so.6 libcgroup.so.1 libpam.so.0 libpthread.so.0"
RDEPENDS_libcgroup-pam = "glibc libcgroup pam"
RPM_SONAME_REQ_libcgroup-tools = "ld-linux-aarch64.so.1 libc.so.6 libcgroup.so.1 libpthread.so.0"
RDEPENDS_libcgroup-tools = "bash glibc libcgroup systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcgroup-0.41-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcgroup-pam-0.41-19.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libcgroup-tools-0.41-19.el8.aarch64.rpm \
          "

SRC_URI[libcgroup.sha256sum] = "5dd1a8af0bc7ae3efb14f4da841d3e11024396d712e2bc139c4b7b75026bdd6f"
SRC_URI[libcgroup-pam.sha256sum] = "04af20bbff8e16773ee9273c7b538cbce1a05e59ed16def9b579484694581fb0"
SRC_URI[libcgroup-tools.sha256sum] = "3777af8c73ec82db5b8bbd48e87d7e3d2ad5cdeffb3e29d2289f5761f693d6b6"
