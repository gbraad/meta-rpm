SUMMARY = "generated recipe based on sbc srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2 and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_sbc = "libsbc.so.1"
RPM_SONAME_REQ_sbc = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_sbc = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sbc-1.3-9.el8.aarch64.rpm \
          "

SRC_URI[sbc.sha256sum] = "5b38727d8d18aae3d6e5be51f67f527ba3dc99bc6188927e1e18128cebb2f909"
