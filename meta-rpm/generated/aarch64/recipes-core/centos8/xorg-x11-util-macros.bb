SUMMARY = "generated recipe based on xorg-x11-util-macros srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_xorg-x11-util-macros = "autoconf automake libtool pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/xorg-x11-util-macros-1.19.0-8.el8.noarch.rpm \
          "

SRC_URI[xorg-x11-util-macros.sha256sum] = "8ee2cc14e34e9c4824f790da0fa011983d78a1d366c8a37781b7ccadbc5be3fb"
