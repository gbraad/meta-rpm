SUMMARY = "generated recipe based on python-itsdangerous srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-itsdangerous = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-itsdangerous-0.24-14.el8.noarch.rpm \
          "

SRC_URI[python3-itsdangerous.sha256sum] = "2233cf3923634b19fd04e98d991abd9ffd8cec367a0fad110f57d70365df64a1"
