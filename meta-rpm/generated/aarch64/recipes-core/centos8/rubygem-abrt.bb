SUMMARY = "generated recipe based on rubygem-abrt srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_rubygem-abrt = "libreport-filesystem rubygems"
RDEPENDS_rubygem-abrt-doc = "rubygem-abrt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rubygem-abrt-0.3.0-4.module_el8.1.0+214+9be47fd7.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rubygem-abrt-doc-0.3.0-4.module_el8.1.0+214+9be47fd7.noarch.rpm \
          "

SRC_URI[rubygem-abrt.sha256sum] = "008ac92fbfbdce4140708ee8fc62f3e84ee639fd85de45709853f3e2cfe38e10"
SRC_URI[rubygem-abrt-doc.sha256sum] = "ea7314b600b9958ad00c9fe1c7aa5b8ff43ef4c9c50deed27cc942a8af7cd3bf"
