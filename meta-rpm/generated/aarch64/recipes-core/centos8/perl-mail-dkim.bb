SUMMARY = "generated recipe based on perl-Mail-DKIM srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Mail-DKIM = "perl-Carp perl-Crypt-OpenSSL-RSA perl-Digest-SHA perl-MIME-Base64 perl-MailTools perl-Net-DNS perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Mail-DKIM-0.54-1.el8.noarch.rpm \
          "

SRC_URI[perl-Mail-DKIM.sha256sum] = "3f4dda55681fc7abe5a2d8362945bd290c3bb47b7fef169f84a78e58e7a5bb0f"
