SUMMARY = "generated recipe based on sysfsutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libsysfs = "libsysfs.so.2"
RPM_SONAME_REQ_libsysfs = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libsysfs = "glibc"
RPM_SONAME_REQ_libsysfs-devel = "libsysfs.so.2"
RPROVIDES_libsysfs-devel = "libsysfs-dev (= 2.1.0)"
RDEPENDS_libsysfs-devel = "libsysfs"
RPM_SONAME_REQ_sysfsutils = "ld-linux-aarch64.so.1 libc.so.6 libsysfs.so.2"
RDEPENDS_sysfsutils = "glibc libsysfs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/sysfsutils-2.1.0-24.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libsysfs-2.1.0-24.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libsysfs-devel-2.1.0-24.el8.aarch64.rpm \
          "

SRC_URI[libsysfs.sha256sum] = "c31bbacfb56aa3e6eaaba32c7e2a186bdedcd768a5d12bfeaeacada350afb4b1"
SRC_URI[libsysfs-devel.sha256sum] = "66ef2d4e9bbf6f03fbf7cab76a7b1568e8fb7ca167a6123f294ea12233eb83e0"
SRC_URI[sysfsutils.sha256sum] = "777e9f5492257ef76a7c768c9def87a246516ae4fbbea9c5225403e624df38b6"
