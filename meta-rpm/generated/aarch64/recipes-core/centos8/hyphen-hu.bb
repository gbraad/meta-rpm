SUMMARY = "generated recipe based on hyphen-hu srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-hu = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-hu-0.20090612-19.el8.noarch.rpm \
          "

SRC_URI[hyphen-hu.sha256sum] = "341c9619989a2a137fb2b60d727f72cfed8705ecece201dca6460f84caf7f9ad"
