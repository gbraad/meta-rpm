SUMMARY = "generated recipe based on passwd srpm"
DESCRIPTION = "Description"
LICENSE = "BSD | GPL-2.0"
RPM_LICENSE = "BSD or GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs glib-2.0 libselinux libuser pam pkgconfig-native popt"
RPM_SONAME_REQ_passwd = "ld-linux-aarch64.so.1 libaudit.so.1 libc.so.6 libglib-2.0.so.0 libgobject-2.0.so.0 libpam.so.0 libpam_misc.so.0 libpopt.so.0 libpthread.so.0 libselinux.so.1 libuser.so.1"
RDEPENDS_passwd = "audit-libs glib2 glibc libselinux libuser pam popt"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/passwd-0.80-3.el8.aarch64.rpm \
          "

SRC_URI[passwd.sha256sum] = "5175b8c44052fe8a2cfa001f3937072190218cd432d4070ca64a3883c638f0e7"
