SUMMARY = "generated recipe based on perl-HTTP-Message srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-HTTP-Message = "perl-Carp perl-Compress-Raw-Zlib perl-Encode perl-Encode-Locale perl-Exporter perl-HTTP-Date perl-IO-Compress perl-IO-HTML perl-LWP-MediaTypes perl-MIME-Base64 perl-Storable perl-URI perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-HTTP-Message-6.18-1.el8.noarch.rpm \
          "

SRC_URI[perl-HTTP-Message.sha256sum] = "4612f36cb313af6bc956d6aab65808d63050e5c3090ac7b04602d56ac8cf377f"
