SUMMARY = "generated recipe based on papi srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gcc libgcc libpfm pkgconfig-native"
RPM_SONAME_REQ_papi = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libm.so.6 libpapi.so.5"
RDEPENDS_papi = "glibc papi-libs"
RPM_SONAME_REQ_papi-devel = "libpapi.so.5"
RPROVIDES_papi-devel = "papi-dev (= 5.6.0)"
RDEPENDS_papi-devel = "papi papi-libs pkgconf-pkg-config"
RPM_SONAME_PROV_papi-libs = "libpapi.so.5"
RPM_SONAME_REQ_papi-libs = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpfm.so.4"
RDEPENDS_papi-libs = "glibc libpfm"
RPM_SONAME_REQ_papi-testsuite = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libgcc_s.so.1 libgfortran.so.5 libgomp.so.1 libm.so.6 libpapi.so.5 libpthread.so.0 librt.so.1"
RDEPENDS_papi-testsuite = "bash glibc libgcc libgfortran libgomp papi papi-libs perl-Getopt-Long perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/papi-5.6.0-9.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/papi-devel-5.6.0-9.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/papi-libs-5.6.0-9.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/papi-testsuite-5.6.0-9.el8_2.aarch64.rpm \
          "

SRC_URI[papi.sha256sum] = "7b5e83649775bfc4d6f7877f45dd3c66e601e348849ede40b43afac7057822d4"
SRC_URI[papi-devel.sha256sum] = "c421e6422fa5d4882b4464cdcf76ea67ae6b19de2a7e3fcbbe6a9715d3269e50"
SRC_URI[papi-libs.sha256sum] = "2ed52ca1c7c20f55b13bb46be43cd7d24cbfc14604f5bbce47c74ae9e3cc6248"
SRC_URI[papi-testsuite.sha256sum] = "2f85fa82b7c5976f13ae088629b50e3e7486661b73cf65c0925113e77c62d74d"
