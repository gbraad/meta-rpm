SUMMARY = "generated recipe based on libogg srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libogg = "libogg.so.0"
RPM_SONAME_REQ_libogg = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libogg = "glibc"
RPM_SONAME_REQ_libogg-devel = "libogg.so.0"
RPROVIDES_libogg-devel = "libogg-dev (= 1.3.2)"
RDEPENDS_libogg-devel = "automake libogg pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libogg-1.3.2-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libogg-devel-1.3.2-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libogg-devel-docs-1.3.2-10.el8.noarch.rpm \
          "

SRC_URI[libogg.sha256sum] = "b81877b7cc7bc181ff52cc65fc1ed88ccbde11c403a664ac2496d3b8eb45e4bb"
SRC_URI[libogg-devel.sha256sum] = "f2f1528b2e0cd8608f03bbb685c942ccb25e456d5eb0fb5973c2894b5045ce93"
SRC_URI[libogg-devel-docs.sha256sum] = "0cd3850a60057258e06c4c10a6c4a05d22b31741dc472e043d2ecdd9b2d8b35a"
