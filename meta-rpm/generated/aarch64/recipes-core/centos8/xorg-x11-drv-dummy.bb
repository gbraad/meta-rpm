SUMMARY = "generated recipe based on xorg-x11-drv-dummy srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-drv-dummy = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_xorg-x11-drv-dummy = "glibc xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-dummy-0.3.7-6.el8.1.aarch64.rpm \
          "

SRC_URI[xorg-x11-drv-dummy.sha256sum] = "b11b75f3b4df1ecd7b87dc29fbefc37b0347222d5c0aa842851c826bc10434db"
