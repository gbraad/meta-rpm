SUMMARY = "generated recipe based on hamcrest srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hamcrest = "easymock hamcrest-core java-1.8.0-openjdk-headless javapackages-filesystem qdox"
RDEPENDS_hamcrest-core = "java-1.8.0-openjdk-headless javapackages-filesystem"
RDEPENDS_hamcrest-demo = "hamcrest junit testng"
RDEPENDS_hamcrest-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/hamcrest-1.3-23.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/hamcrest-core-1.3-23.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/hamcrest-demo-1.3-23.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/hamcrest-javadoc-1.3-23.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[hamcrest.sha256sum] = "d71454e36b9cf394c3c39b92e46bd0c6cff146b96a848bd14754008b78ac5253"
SRC_URI[hamcrest-core.sha256sum] = "0f9d8e057e64bdf23d5d31644e6039298394f4442d0d9b5413a072c589783404"
SRC_URI[hamcrest-demo.sha256sum] = "a04ed762300ff8af8e646ed49cd6bfe061ed43307b16d8d34eccaf691722f522"
SRC_URI[hamcrest-javadoc.sha256sum] = "4d81385df919c3dceec0b60da15e74045931c250dae39ad6fe3fbf4bb13a0a50"
