SUMMARY = "generated recipe based on openoffice-lv srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-lv = "hunspell"
RDEPENDS_hyphen-lv = "hyphen"
RDEPENDS_mythes-lv = "mythes"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-lv-1.0.0-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-lv-1.0.0-7.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mythes-lv-1.0.0-7.el8.noarch.rpm \
          "

SRC_URI[hunspell-lv.sha256sum] = "9f072b1a02162a5ef501b3ed9bf94ef3358ffb86c4766d161b502f92f827cea2"
SRC_URI[hyphen-lv.sha256sum] = "28575c8c606c2006ede78a4c2fe50d58ce524854dc356773c053d15e79fe917a"
SRC_URI[mythes-lv.sha256sum] = "85890d2a6fb4d7ebdc27df858692753dc1bf0fba4ae65b37424ca71502ec263d"
