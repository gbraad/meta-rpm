SUMMARY = "generated recipe based on vdo srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "device-mapper-event device-mapper-libs libuuid pkgconfig-native zlib"
RPM_SONAME_REQ_vdo = "ld-linux-aarch64.so.1 libc.so.6 libdevmapper-event.so.1.02 libdevmapper.so.1.02 libdl.so.2 libm.so.6 libpthread.so.0 librt.so.1 libuuid.so.1 libz.so.1"
RDEPENDS_vdo = "bash device-mapper-event-libs device-mapper-libs glibc kmod-kvdo libuuid lvm2 platform-python python3-pyyaml systemd util-linux zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/vdo-6.2.2.117-13.el8.aarch64.rpm \
          "

SRC_URI[vdo.sha256sum] = "4ead4458f6e94c5d87f89c4a0195ddab702d6b754c43b139277d78283d0a683c"
