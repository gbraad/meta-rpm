SUMMARY = "generated recipe based on pandoc srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gmp libffi libpcre libyaml lua pkgconfig-native zlib"
RPM_SONAME_REQ_pandoc = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libffi.so.6 libgmp.so.10 liblua-5.3.so libm.so.6 libpcre.so.1 libpthread.so.0 librt.so.1 libutil.so.1 libyaml-0.so.2 libz.so.1"
RDEPENDS_pandoc = "glibc gmp libffi libyaml lua-libs pandoc-common pcre zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/pandoc-2.0.6-4.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/pandoc-common-2.0.6-4.el8.noarch.rpm \
          "

SRC_URI[pandoc.sha256sum] = "f1a18151cdbf1c7e5273232ebb3aae5b4663f572238d18893a3223a16bcb45b9"
SRC_URI[pandoc-common.sha256sum] = "2dca2036f2a05ceb53136c5e60099a58bb92087320b8e8b7730a7747d23fcc81"
