SUMMARY = "generated recipe based on hunspell-st srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-st = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-st-0.20091030-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-st.sha256sum] = "2366a921b78e39f96876f9681cdf721885674997f1da079043b22e977d8c4e7d"
