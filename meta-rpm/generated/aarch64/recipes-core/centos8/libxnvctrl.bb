SUMMARY = "generated recipe based on libXNVCtrl srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 libxext pkgconfig-native"
RPM_SONAME_PROV_libXNVCtrl = "libXNVCtrl.so.0"
RPM_SONAME_REQ_libXNVCtrl = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libc.so.6"
RDEPENDS_libXNVCtrl = "glibc libX11 libXext"
RPM_SONAME_REQ_libXNVCtrl-devel = "libXNVCtrl.so.0"
RPROVIDES_libXNVCtrl-devel = "libXNVCtrl-dev (= 352.21)"
RDEPENDS_libXNVCtrl-devel = "libX11-devel libXNVCtrl"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXNVCtrl-352.21-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libXNVCtrl-devel-352.21-9.el8.aarch64.rpm \
          "

SRC_URI[libXNVCtrl.sha256sum] = "f572e9ea5fe79f75ae4f72dbffb91c1f64df18d59c1f744c2f96f89282b2ec41"
SRC_URI[libXNVCtrl-devel.sha256sum] = "bcac695f69cd45ada092778f4daac45019891ab131e64dad631ab24dc1c6f3f2"
