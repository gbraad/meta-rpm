SUMMARY = "generated recipe based on libXt srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
PROVIDES = "xt"
DEPENDS = "libice libsm libx11 pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXt = "libXt.so.6"
RPM_SONAME_REQ_libXt = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libc.so.6"
RDEPENDS_libXt = "glibc libICE libSM libX11"
RPM_SONAME_REQ_libXt-devel = "libXt.so.6"
RPROVIDES_libXt-devel = "libXt-dev (= 1.1.5)"
RDEPENDS_libXt-devel = "libICE-devel libSM-devel libX11-devel libXt pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXt-1.1.5-12.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXt-devel-1.1.5-12.el8.aarch64.rpm \
          "

SRC_URI[libXt.sha256sum] = "2e4efbbee20cb51b326319e616e50d725f20f0476e7ed5b4b24e74ca09e513f8"
SRC_URI[libXt-devel.sha256sum] = "d005b1700b4dbbd8d2030e2b4e2f9fd6138dcf58be92c6260eddd6a65ffd66aa"
