SUMMARY = "generated recipe based on gcc-toolset-9-annobin srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_gcc-toolset-9-annobin = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_gcc-toolset-9-annobin = "bash gcc-toolset-9-gcc gcc-toolset-9-runtime glibc libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gcc-toolset-9-annobin-9.08-4.el8.aarch64.rpm \
          "

SRC_URI[gcc-toolset-9-annobin.sha256sum] = "ade6274dec497982bbf9f8a35bda57aaa1ac10cc7be707aca99ea34503be0b8a"
