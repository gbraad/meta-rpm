SUMMARY = "generated recipe based on mrtg srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gd pkgconfig-native"
RPM_SONAME_REQ_mrtg = "ld-linux-aarch64.so.1 libc.so.6 libgd.so.3 libm.so.6"
RDEPENDS_mrtg = "bash gd glibc perl-CGI perl-Carp perl-Exporter perl-Getopt-Long perl-IO perl-IO-Socket-INET6 perl-Math-BigInt perl-Pod-Usage perl-SNMP_Session perl-Socket perl-Socket6 perl-interpreter perl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mrtg-2.17.7-1.el8.aarch64.rpm \
          "

SRC_URI[mrtg.sha256sum] = "b81222b4e8b3dbf29e5d3605dee333d8beceff77333ba01f3c7c955a1fffad35"
