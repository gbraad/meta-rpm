SUMMARY = "generated recipe based on jna srpm"
DESCRIPTION = "Description"
LICENSE = "(LGPL-2.0 | CLOSED) & CLOSED"
RPM_LICENSE = "(LGPLv2 or ASL 2.0) and ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libffi pkgconfig-native"
RPM_SONAME_REQ_jna = "ld-linux-aarch64.so.1 libc.so.6 libffi.so.6"
RDEPENDS_jna = "glibc java-1.8.0-openjdk-headless javapackages-filesystem libffi"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jna-4.5.1-5.el8.aarch64.rpm \
          "

SRC_URI[jna.sha256sum] = "69722a6801493c670b277b7935b9d819a30edc6d89443c7ecb4cca205e00b441"
