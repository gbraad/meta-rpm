SUMMARY = "generated recipe based on convmv srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | GPL-3.0"
RPM_LICENSE = "GPLv2 or GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_convmv = "perl-Encode perl-Getopt-Long perl-PathTools perl-Unicode-Normalize perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/convmv-2.01-3.el8.noarch.rpm \
          "

SRC_URI[convmv.sha256sum] = "f0613ed41cfd0575b8b97abb0497a168614fc5b94872df4dcf37d946e7068487"
