SUMMARY = "generated recipe based on quota srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & GPL-2.0 & GPL-2.0"
RPM_LICENSE = "BSD and GPLv2 and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-libs e2fsprogs libnl libtirpc openldap pkgconfig-native"
RPM_SONAME_REQ_quota = "ld-linux-aarch64.so.1 libc.so.6 libcom_err.so.2 libext2fs.so.2 libtirpc.so.3"
RDEPENDS_quota = "e2fsprogs-libs glibc libcom_err libtirpc quota-nls"
RDEPENDS_quota-doc = "quota"
RPM_SONAME_REQ_quota-nld = "ld-linux-aarch64.so.1 libc.so.6 libdbus-1.so.3 libnl-3.so.200 libnl-genl-3.so.200 libtirpc.so.3"
RDEPENDS_quota-nld = "bash dbus-libs glibc libnl3 libtirpc quota-nls systemd"
RPM_SONAME_REQ_quota-rpc = "ld-linux-aarch64.so.1 libc.so.6 libtirpc.so.3"
RDEPENDS_quota-rpc = "bash glibc libtirpc quota-nls rpcbind systemd"
RPM_SONAME_REQ_quota-warnquota = "ld-linux-aarch64.so.1 libc.so.6 liblber-2.4.so.2 libldap-2.4.so.2 libtirpc.so.3"
RDEPENDS_quota-warnquota = "glibc libtirpc openldap quota-nls"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/quota-4.04-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/quota-doc-4.04-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/quota-nld-4.04-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/quota-nls-4.04-10.el8.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/quota-rpc-4.04-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/quota-warnquota-4.04-10.el8.aarch64.rpm \
          "

SRC_URI[quota.sha256sum] = "e63088cb4953eeddaf321aa25b33701c44e70180697b7af54011cd193ffe0c48"
SRC_URI[quota-doc.sha256sum] = "ef0775544e977ba8747d1c4354f69cf69261b82c6cbe4e229c9fe6e3c8855edf"
SRC_URI[quota-nld.sha256sum] = "cb8320596cf381774300659d099f025fcdb936a69e5cdab2ebc2538b2c9a454c"
SRC_URI[quota-nls.sha256sum] = "35f810182b9605123bb81da116e441c254c01726607477188f18e07bf0582ee1"
SRC_URI[quota-rpc.sha256sum] = "358a68d0a9e4058b91ecb565535c0442d3f75044c605dbb1557352cf29150a9a"
SRC_URI[quota-warnquota.sha256sum] = "4dfc316d91c59ee0a09d20b8edde525d09faa887149aa1c65ebd90ec21e141a0"
