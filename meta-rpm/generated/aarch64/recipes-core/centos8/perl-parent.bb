SUMMARY = "generated recipe based on perl-parent srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-parent = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perl-parent-0.237-1.el8.noarch.rpm \
          "

SRC_URI[perl-parent.sha256sum] = "f5e73bbd776a2426a796971d8d38664f2e94898479fb76947dccdd28cf9fe1d0"
