SUMMARY = "generated recipe based on perl-List-MoreUtils-XS srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-List-MoreUtils-XS = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-List-MoreUtils-XS = "glibc perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-List-MoreUtils-XS-0.428-3.el8.aarch64.rpm \
          "

SRC_URI[perl-List-MoreUtils-XS.sha256sum] = "53a0e24a2e4df4ab71b21358155ed396930c382f111bcb6555fe4d0843e6f0b9"
