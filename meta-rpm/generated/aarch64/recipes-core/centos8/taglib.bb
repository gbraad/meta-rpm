SUMMARY = "generated recipe based on taglib srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | MPL-1.1"
RPM_LICENSE = "LGPLv2 or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native zlib"
RPM_SONAME_PROV_taglib = "libtag.so.1 libtag_c.so.0"
RPM_SONAME_REQ_taglib = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6 libz.so.1"
RDEPENDS_taglib = "glibc libgcc libstdc++ zlib"
RPM_SONAME_REQ_taglib-devel = "libtag.so.1 libtag_c.so.0"
RPROVIDES_taglib-devel = "taglib-dev (= 1.11.1)"
RDEPENDS_taglib-devel = "bash pkgconf-pkg-config taglib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/taglib-1.11.1-8.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/taglib-devel-1.11.1-8.el8.aarch64.rpm \
          "

SRC_URI[taglib.sha256sum] = "17355aaa279f4952bf6ff436315386bb6242ef5f31494f6162c95fcd4aec728a"
SRC_URI[taglib-devel.sha256sum] = "408d7400568c82e7be176ba11bd243aa262148de2bfc01428d2c906d1615bbe0"
