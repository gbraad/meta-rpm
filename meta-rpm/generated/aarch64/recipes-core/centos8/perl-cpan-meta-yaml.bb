SUMMARY = "generated recipe based on perl-CPAN-Meta-YAML srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-CPAN-Meta-YAML = "perl-Carp perl-Exporter perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-CPAN-Meta-YAML-0.018-397.el8.noarch.rpm \
          "

SRC_URI[perl-CPAN-Meta-YAML.sha256sum] = "ef154f0036e427fced39334b3ae846dedcc0957a5ccdeac32301b51b5699c5cc"
