SUMMARY = "generated recipe based on device-mapper-persistent-data srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "expat libaio libgcc pkgconfig-native"
RPM_SONAME_REQ_device-mapper-persistent-data = "ld-linux-aarch64.so.1 libaio.so.1 libc.so.6 libdl.so.2 libexpat.so.1 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_device-mapper-persistent-data = "expat glibc libaio libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/device-mapper-persistent-data-0.8.5-3.el8.aarch64.rpm \
          "

SRC_URI[device-mapper-persistent-data.sha256sum] = "a39dc747d1fb53cc0ca075e59e0440f9619dc310436e6bf38c18cbaf274c0f54"
