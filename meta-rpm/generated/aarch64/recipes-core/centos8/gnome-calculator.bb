SUMMARY = "generated recipe based on gnome-calculator srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atk glib-2.0 gtk+3 gtksourceview3 libmpc libsoup-2.4 libxml2 mpfr pkgconfig-native"
RPM_SONAME_REQ_gnome-calculator = "ld-linux-aarch64.so.1 libatk-1.0.so.0 libc.so.6 libgdk-3.so.0 libgio-2.0.so.0 libglib-2.0.so.0 libgobject-2.0.so.0 libgtk-3.so.0 libgtksourceview-3.0.so.1 libmpc.so.3 libmpfr.so.4 libsoup-2.4.so.1 libxml2.so.2"
RDEPENDS_gnome-calculator = "atk glib2 glibc gtk3 gtksourceview3 libmpc libsoup libxml2 mpfr"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/gnome-calculator-3.28.2-1.el8.aarch64.rpm \
          "

SRC_URI[gnome-calculator.sha256sum] = "78cf3faa1d67d446a0d7667e06c9c6a7782a24cd202f452bb2824ecc5741ee88"
