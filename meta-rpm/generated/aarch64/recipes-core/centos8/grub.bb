SUMMARY = "generated recipe based on grub2 srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "device-mapper-libs freetype pkgconfig-native rpm xz"
RDEPENDS_grub2-common = "bash"
RDEPENDS_grub2-efi-aa64 = "efi-filesystem grub2-common grub2-tools grub2-tools-extra grub2-tools-minimal"
RDEPENDS_grub2-efi-aa64-cdboot = "grub2-common"
RDEPENDS_grub2-efi-aa64-modules = "grub2-common"
RDEPENDS_grub2-efi-ia32-modules = "bash grub2-common"
RDEPENDS_grub2-efi-x64-modules = "grub2-common"
RDEPENDS_grub2-pc-modules = "grub2-common"
RDEPENDS_grub2-ppc64le-modules = "grub2-common"
RPM_SONAME_REQ_grub2-tools = "libc.so.6 libdevmapper.so.1.02 liblzma.so.5 librpm.so.8"
RDEPENDS_grub2-tools = "bash device-mapper-libs dracut file gettext glibc grub2-common os-prober rpm-libs which xz-libs"
RPM_SONAME_REQ_grub2-tools-extra = "libc.so.6 libdevmapper.so.1.02 libfreetype.so.6 liblzma.so.5"
RDEPENDS_grub2-tools-extra = "bash device-mapper-libs file freetype gettext glibc grub2-common grub2-tools-minimal os-prober which xz-libs"
RPM_SONAME_REQ_grub2-tools-minimal = "libc.so.6 libdevmapper.so.1.02 liblzma.so.5"
RDEPENDS_grub2-tools-minimal = "bash device-mapper-libs gettext glibc grub2-common xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-common-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-efi-aa64-2.02-87.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-efi-aa64-cdboot-2.02-87.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-efi-aa64-modules-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-efi-ia32-modules-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-efi-x64-modules-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-pc-modules-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-ppc64le-modules-2.02-87.el8_2.noarch.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-tools-2.02-87.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-tools-extra-2.02-87.el8_2.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/grub2-tools-minimal-2.02-87.el8_2.aarch64.rpm \
          "

SRC_URI[grub2-common.sha256sum] = "886fe30d2f7ab40f96a8b28a96b86079feab5f45f8fac818d02c17af03b5af05"
SRC_URI[grub2-efi-aa64.sha256sum] = "46a6c3ea32b268df8cee79db988f6ea7c6a42f5aa1fcdaf8d699b891ee19c14b"
SRC_URI[grub2-efi-aa64-cdboot.sha256sum] = "3c8916c7ff1990868a5bcde7ec6f342ca1e936cfda771947d8d39befc8fece33"
SRC_URI[grub2-efi-aa64-modules.sha256sum] = "7f209b336386a89100061a63fda2ddbf653052d050fb73caac97c9272a9f4f05"
SRC_URI[grub2-efi-ia32-modules.sha256sum] = "230456edb514dee5f19d5e20b997c1e6c9536f60b3686b8d63d5dd4da75ffb15"
SRC_URI[grub2-efi-x64-modules.sha256sum] = "140e94fa5b2ce4f95f6caffb2ee25444f83e33cfd6d0690702ff2790fb35234c"
SRC_URI[grub2-pc-modules.sha256sum] = "8023853c359646ab9df45fdef5217ab842195626006e4eb6e8e026089d66ddb7"
SRC_URI[grub2-ppc64le-modules.sha256sum] = "266388983e0b88df3e47a856bbdb39e5ff2c0275b60d6691e89df79fa4625183"
SRC_URI[grub2-tools.sha256sum] = "8c18271235faa65eaed1dcdcf21b38b49b55b1bf6e52fb337069f4c0d435d13d"
SRC_URI[grub2-tools-extra.sha256sum] = "104151e503c14571fa63caaac6ded5fce6105cb9edf264df6ef2c862cc7e9af8"
SRC_URI[grub2-tools-minimal.sha256sum] = "5a0ab31bb5da992db4af366f09da9499d575d3eef48432112c61ccd90068d136"
