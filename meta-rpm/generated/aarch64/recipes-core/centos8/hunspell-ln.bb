SUMMARY = "generated recipe based on hunspell-ln srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ln = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ln-0.02-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-ln.sha256sum] = "d03ee61986b9d6decc824ea1bce79f48c1b089e3b15b24fb7626142c2c0f1332"
