SUMMARY = "generated recipe based on libXvMC srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 libxext libxv pkgconfig-native xorg-x11-proto-devel"
RPM_SONAME_PROV_libXvMC = "libXvMC.so.1 libXvMCW.so.1"
RPM_SONAME_REQ_libXvMC = "ld-linux-aarch64.so.1 libX11.so.6 libXext.so.6 libXv.so.1 libc.so.6 libdl.so.2"
RDEPENDS_libXvMC = "glibc libX11 libXext libXv"
RPM_SONAME_REQ_libXvMC-devel = "libXvMC.so.1 libXvMCW.so.1"
RPROVIDES_libXvMC-devel = "libXvMC-dev (= 1.0.10)"
RDEPENDS_libXvMC-devel = "libX11-devel libXext-devel libXv-devel libXvMC pkgconf-pkg-config xorg-x11-proto-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libXvMC-1.0.10-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libXvMC-devel-1.0.10-7.el8.aarch64.rpm \
          "

SRC_URI[libXvMC.sha256sum] = "25ac1b09b15702debdd0b3ba0482656b8c958ba7cd2c52c604831a1665968459"
SRC_URI[libXvMC-devel.sha256sum] = "857ce94fc18a911d167d96eb86bacbc42b1933bee721a0a083f0ea99858b519f"
