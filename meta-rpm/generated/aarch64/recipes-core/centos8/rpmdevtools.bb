SUMMARY = "generated recipe based on rpmdevtools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & GPL-2.0"
RPM_LICENSE = "GPLv2+ and GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_rpmdevtools = "bash curl diffutils emacs-filesystem file findutils gawk grep perl-File-Temp perl-Getopt-Long perl-PathTools perl-interpreter perl-libs platform-python python3-rpm rpm-build sed"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/rpmdevtools-8.10-7.el8.noarch.rpm \
          "

SRC_URI[rpmdevtools.sha256sum] = "8aa04a2007fa2b1d58221d71967f43648deeda0d6acb51e76fc5dc941688914a"
