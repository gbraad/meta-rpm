SUMMARY = "Groot gives fake root capabilities to a normal user"
HOMEPAGE = "https://github.com/alexlarsson/groot"
LIC_FILES_CHKSUM = "file://COPYING;md5=5f30f0716dfdd0d91eb439ebec522ec2"
SECTION = "base"
LICENSE = "LGPL2.1"
DEPENDS = "fuse libcap"

FILES_${PN} = "${bindir}/groot"

#PROVIDES += "virtual/fakeroot"

MAKEOPTS = ""

do_configure () {
	:
}

# Compile for the local machine arch...
do_compile () {
	oe_runmake ${MAKEOPTS}
}

do_install () {
	oe_runmake 'DESTDIR=${D}' 'PREFIX=${exec_prefix}' LIBDIR="${libdir}" ${MAKEOPTS} install
}

BBCLASSEXTEND = "native"
