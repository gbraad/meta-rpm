require groot.inc

SRC_URI = "git://github.com/alexlarsson/groot.git;branch=main"

SRCREV = "3dbae6acbbea1585cf3d2507523f0c91af897447"

S = "${WORKDIR}/git"
PV = "0.1+git${SRCPV}"
