inherit core-image

# We need stateless-rootfs, otherwise the systemd distro features calls out to sytemctl during image build which fails
IMAGE_FEATURES = "stateless-rootfs"
EXTRA_IMAGE_FEATURES = ""

# Don't want locale-base-* (for now at least)
IMAGE_LINGUAS = ""

# Don't try to talk to pid 1 in rpm scripts
export SYSTEMD_OFFLINE = "1"

# Avoid accidentally talking to host
export DBUS_SYSTEM_BUS_ADDRESS = "unix:path=/does/not/exist"

CORE_IMAGE_BASE_INSTALL = "bash coreutils"
CORE_IMAGE_EXTRA_INSTALL = ""

ROOTFS_BOOTSTRAP_INSTALL = ""

# We don't do SDKs (yet), so drop all those deps
SDK_DEPENDS = ""


IMAGE_LOG_CHECK_EXCLUDES = "dbus-org.freedesktop.resolve1.service"

# The regular do_rootfs can't remove existing rootfs because the groot fuse mount blocks rm with EBUSY.
# We run the pre as a python function so it runs without groot in the shell and then disable groot for the rm
python rpm_image_pre_do_rootfs () {
  import subprocess

  rootfs = d.getVar('IMAGE_ROOTFS')
  inc_rpm_image_gen = d.getVar('INC_RPM_IMAGE_GEN')

  if inc_rpm_image_gen != "1" and os.path.isdir(rootfs):
    env = os.environ.copy()
    env["GROOT_DISABLED"] = "1"
    subprocess.check_call(['rm', '-rf', rootfs], env=env)
}

do_rootfs[prefuncs] += "rpm_image_pre_do_rootfs"

do_pre_setup_rootfs () {
  # Fix some ordering issue betweend filesystem creating the sbin symlink and something owning a file in /sbin
  mkdir -p ${IMAGE_ROOTFS}/usr/sbin
  ln -s usr/sbin ${IMAGE_ROOTFS}/sbin
  mkdir -p ${IMAGE_ROOTFS}/usr/lib64
  ln -s usr/lib64 ${IMAGE_ROOTFS}/lib64
}

RPM_PREPROCESS_COMMANDS = "do_pre_setup_rootfs"

# The built-in version of this doesn't work as it doesn't run in groot thus not setting the right permissions on e.g. gshadow
SORT_PASSWD_POSTPROCESS_COMMAND = ""

# Disable pseudo-based fakeroot:
do_flush_pseudodb[noexec] = "1"
python () {
       d.delVarFlag("do_rootfs", "fakeroot")
       d.delVarFlag("do_image", "fakeroot")
       d.delVarFlag("do_image_complete", "fakeroot")
       d.delVarFlag("do_image_qa", "fakeroot")
       d.delVarFlag("do_image_tar", "fakeroot")
}

# Use groot instead to do fakeroot, as it allows proper support for chroot, including with exec()

DEPENDS += "groot-native"

def getusername():
    import pwd
    return pwd.getpwuid(os.getuid())[0]

export GROOT_WRAPFS = "${IMAGE_ROOTFS}"
export LD_PRELOAD = "${STAGING_DIR_NATIVE}${libdir_native}/libgroot.so"
# Avoid doing nss lookups in preload:
export GROOT_USER = "${@getusername()}"
