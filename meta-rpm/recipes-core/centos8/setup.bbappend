post_rpm_install_append_class-native() {
  # This conflicts with the file from pseudo-native, so remove
  rm ${D}${sysconfdir}/group
  rm ${D}${sysconfdir}/passwd
}
