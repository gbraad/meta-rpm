update: update-centos8

update-centos8:
	$(MAKE) -C meta-rpm/recipes-core/centos8 update


# Warning, this is slow and can download a lot of data
update-centos8-data:
	$(MAKE) -C package-data centos8-x86_64.json
